package cz.zcu.kiv.efps.efpportal.data.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class represents data EFP.
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class EFPData {
    
    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(EFPData.class);
    /**
     * EFP name.
     */
    private String name;
    /**
     * Result of EFP comparison.
     */
    private String result;
    /**
     * EFP type.
     */
    private String type;
    /**
     * EFP value.
     */
    private String value;

    
    /**
     * Constructor.
     * 
     * @param name EFP name
     * @param result result of EFP comparison
     * @param type EFP type
     * @param value EFP value
     */
    public EFPData(String name, String result, String type, String value) {
        this.name = name;
        this.result = result;
        this.type = type;
        this.value = value;
    }
    
    /**
     * Returns EFP name.
     * @return EFP name
     */
    public String getName() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return name;
    }

    /**
     * Returns result of EFP comparison.
     * @return result of EFP comparison
     */
    public String getResult() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return result;
    }

    /**
     * Returns EFP type.
     * @return EFP type 
     */
    public String getType() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return type;
    }

    /**
     * Returns EFP value.
     * @return EFP value
     */
    public String getValue() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return value;
    }

    @Override
    public String toString() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return this.name + " " + this.result + " " + this.type + " " + this.value;
    }
}
