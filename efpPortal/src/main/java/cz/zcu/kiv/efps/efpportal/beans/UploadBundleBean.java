package cz.zcu.kiv.efps.efpportal.beans;

import cz.zcu.kiv.efps.efpportal.service.BundleLRService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class is bean for upload bundle.
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class UploadBundleBean {

    /**
     * Uploaded bundles
     */
    private MultipartFile[] bundles;
    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(UploadBundleBean.class);

    public MultipartFile[] getBundles() 
    {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return bundles;
    }

    public void setBundles(MultipartFile[] bundles) {
        logger.trace("ENTRY");
        this.bundles = bundles;
        logger.trace("EXIT");
    }
}
