package cz.zcu.kiv.efps.efpportal.service;

import cz.zcu.kiv.efps.efpportal.service.storage.StorageManager;
import java.io.IOException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import cz.zcu.kiv.efps.efpportal.service.storage.StorageManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is implementation HttpSessionListener.
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class SessionHandler implements HttpSessionListener {

    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(SessionHandler.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        logger.trace("ENTRY");
        HttpSession session = se.getSession();
        logger.info("Session " + session.getId()
                + " was created with timeout " + session.getMaxInactiveInterval());
        logger.trace("EXIT");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        logger.trace("ENTRY");
        HttpSession session = se.getSession();
        logger.info("Session " + session.getId() + " was invalidated.");
        StorageManager storage = new StorageManagerImpl();
        try {
            storage.deleteDirectory(session.getId());
        } catch (IOException ex) {
            logger.error("Directory " + session.getId() + " was not deleted. " + ex.getMessage());
        }
        logger.trace("EXIT");
    }
}
