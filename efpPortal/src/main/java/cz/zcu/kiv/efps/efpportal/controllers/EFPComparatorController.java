package cz.zcu.kiv.efps.efpportal.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;

import javax.annotation.Resource;

import cz.zcu.kiv.efps.efpportal.service.CocaexDataCreator;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cz.zcu.kiv.efps.comparator.api.EfpComparator;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.efpportal.beans.UploadBundleBean;
import cz.zcu.kiv.efps.efpportal.service.DataSetCreator;
import cz.zcu.kiv.efps.efpportal.service.BundleLRService;
import cz.zcu.kiv.efps.efpportal.service.storage.StorageManager;
import cz.zcu.kiv.efps.efpportal.utils.FrameworkType;
import cz.zcu.kiv.efps.efpportal.validators.UploadedBundlesValidator;

/**
 * Controller for EFP Comparator.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
@Controller
@RequestMapping(value = "/efpcomparator")
public class EFPComparatorController {

	@Resource(name="cocaexUrl")
	private String cocaexUrl;

    @Autowired
    private BundleLRService bundleLRService;

    @Autowired
    private CocaexDataCreator resultsToCocaex;

    @Autowired
    private StorageManager storage;

    /**
     * Validator of uploaded bundles.
     */
    @Autowired
    private UploadedBundlesValidator bundlesValidator;
    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(EFPComparatorController.class);


	/**
     * Action for showing choose framework.
     *
     * @return path for view
     */
    @RequestMapping(value = {"/", "" }, method = RequestMethod.GET)
    public String stepFramework() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return "efpcomparator/index";
    }

    /**
     * Action for showing choose framework with using AJAX.
     *
     * @return path for view
     */
    @RequestMapping(value = {"/", "" }, method = RequestMethod.POST)
    public String stepBackToFramework() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return "efpcomparator/back";
    }

    /**
     * Action for showing upload bundles.
     *
     * @param jSessionId session was sent by client
     * @param model model passing to template
     * @return path for view
     */
    @RequestMapping(value = "/upload_bundles", method = RequestMethod.POST)
    public String stepUploadBundles(
            @CookieValue("JSESSIONID") final String jSessionId,
            final Model model) {

        logger.trace("ENTRY");

        if (storage.isExist(jSessionId)) {
            model.addAttribute("bundleNames", storage.getUploadedBundleNames(jSessionId));
        }

        model.addAttribute("uploadBundleBean", new UploadBundleBean());

        logger.trace("EXIT");
        return "efpcomparator/upload_bundles";

    }

    /**
     * Action for upload bundles.
     *
     * @param jSessionId session was sent by client
     * @param upload bean with uploaded files
     * @param model data for view
     * @param result binding result
     * @return path for view
     */
    @RequestMapping(value = "/upload_bundlesAjax", method = RequestMethod.POST)
    public String uploadBundles(
            @ModelAttribute("uploadBundleBean")
            final UploadBundleBean upload,
            final BindingResult result,
            @CookieValue("JSESSIONID") final String jSessionId,
            final Model model) {

        logger.trace("ENTRY");
        List<String> errors = new LinkedList<String>();
        this.bundlesValidator.validate(upload, result);

        if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                errors.add(error.getCode());
            }
        }

        if (!storage.isExist(jSessionId)) {
            try {
                storage.createStorage(jSessionId);
            } catch (IOException ex) {
                logger.error(ex.getMessage());
                errors.add("storage.errors.create.directory");
            }
        }
        try {
            storage.saveFiles(upload.getBundles(), jSessionId);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
            errors.add("storage.errors.save.bundles");
        }

        model.addAttribute("errors", errors);
        model.addAttribute("bundleNames", storage.getUploadedBundleNames(jSessionId));

        logger.trace("EXIT");
        return "efpcomparator/upload_bundles";

    }

    /**
     * Action for delete bundle.
     *
     * @param jSessionId session was sent by client
     * @param bundleToBeDeleted name of bundle which will be deleted
     * @param model a model from MVC
     * @return path for view
     */
    @RequestMapping(value = "/delete_bundle_ajax/", method = RequestMethod.POST)
    public String deleteBundle(
             @RequestParam("delete_name") final String bundleToBeDeleted,
             @CookieValue("JSESSIONID") final String jSessionId,
            final Model model) {

        logger.trace("ENTRY");

        List<String> errors = new LinkedList<String>();
        try {
            storage.deleteFile(bundleToBeDeleted, jSessionId);
        } catch (RuntimeException ex) {
            logger.error(ex.getMessage());
            errors.add("storage.errors.delete.bundle");

        }

        model.addAttribute("errors", errors);
        model.addAttribute("bundleNames", storage.getUploadedBundleNames(jSessionId));

        model.addAttribute("uploadBundleBean", new UploadBundleBean());
        logger.trace("EXIT");
        return "efpcomparator/upload_bundles";
    }

    /**
     * Action for delete all bundle.
     *
     * @param jSessionId session was sent by client
     * @param model data for view
     * @return path for view
     */
    @RequestMapping(value = "/delete_all_bundles_ajax/", method = RequestMethod.POST)
    public String deleteAllBundles(
            @CookieValue("JSESSIONID") final String jSessionId,
            final Model model) {

        logger.trace("ENTRY");
        List<String> errors = new LinkedList<String>();

        try {
            storage.deleteAllFiles(jSessionId);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
            errors.add("storage.errors.delete.bundle.all");
        }

        model.addAttribute("errors", errors);
        model.addAttribute("uploadBundleBean", new UploadBundleBean());
        logger.trace("EXIT");
        return "efpcomparator/upload_bundles";
    }

    /**
     * Action for showing choose LR (local registry).
     *
     * @param jSessionId session was sent by client
     * @param framework framework name
     * @param model data for view
     * @return path for view
     * @throws IOException error to read components
     */
    @RequestMapping(value = "/choose_lr", method = RequestMethod.POST)
    public String stepChooseLR(@CookieValue("JSESSIONID") final String jSessionId,
            @CookieValue("framework") final String framework,
            final Model model) throws IOException {

        logger.trace("ENTRY");
        logger.debug("COOKIE: " + framework);
        //pass bundle names into model
        model.addAttribute("bundleNames", storage.getUploadedBundleNames(jSessionId));

        Map<String, Set<String>> map = bundleLRService.getLRsAndCorrespondingBundles(
                storage.getUploadedBundles(jSessionId), framework);

        if (map.isEmpty()) {
            map.put("0", new HashSet<String>());
        }

        Set<String> set = map.keySet();
        model.addAttribute("localRegistryIDs", set);
        model.addAttribute("jsonData", JSONObject.fromObject(map));
        logger.debug(map.toString());

        logger.trace("EXIT");
        return "efpcomparator/choose_lr";
    }

    /**
     * Action for showing compared results.
     *
     * @param jSessionId session was sent by client
     * @param framework framework name
     * @param localRegister chosen local register
     * @param model data for view
     * @return path for view
     * @throws IOException error to read components
     */
    @RequestMapping(value = "/results", method = RequestMethod.POST)
    public String stepResult(@CookieValue("JSESSIONID") final String jSessionId,
            @CookieValue("framework") final String framework,
            @CookieValue("localRegister") final String localRegister,
            final Model model) throws IOException {

        logger.trace("ENTRY");

        List<EfpEvalResult> efpResults = EfpComparator.evaluate(storage.getAbsolutePathUploadedBundles(jSessionId),
                FrameworkType.getClassNameBySelectedFramework(framework),
                Integer.valueOf(localRegister));

        DataSetCreator creator = new DataSetCreator(efpResults);

        GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		
		String json = gson.toJson(resultsToCocaex.create(efpResults));
		
		model.addAttribute("leftColumn", creator.getWholeStructureComparison());
		model.addAttribute("rightColumn", JSONObject.fromObject(creator.getComparisonResults()));
		model.addAttribute("url", this.cocaexUrl);
		model.addAttribute("data", compress(json));
        
        logger.trace("EXIT");
        return "efpcomparator/results";
    }

    /**
     * Converts an input string into GZIP Base64 encoded string
     * 
     * @param input
     * @return
     * @throws IOException
     */
    private String compress(String input) throws IOException {
    	ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(rstBao);
        zos.write(input.getBytes());
        IOUtils.closeQuietly(zos);
         
        byte[] bytes = rstBao.toByteArray();
        return Base64.encodeBase64String(bytes);
	}


}
