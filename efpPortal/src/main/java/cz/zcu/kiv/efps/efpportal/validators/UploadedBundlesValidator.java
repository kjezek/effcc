package cz.zcu.kiv.efps.efpportal.validators;

import cz.zcu.kiv.efps.efpportal.beans.UploadBundleBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * This class provides methods for testing validity of uploaded files.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public final class UploadedBundlesValidator implements Validator {

    /**
     * Logging.
     */
    private static Logger logger = LoggerFactory.getLogger(UploadedBundlesValidator.class);
    private static final String BUNDLE_TYPE = "jar";

    /**
     * Checks if there are uploaded files.
     *
     * @param files uploaded files
     * @return true if there are uploaded files, otherwise false.
     */
    private boolean isUploaded(MultipartFile[] files) {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return (files != null && files.length > 0 && !"".equals(files[0].getOriginalFilename()));
    }

    /**
     * Checks if uploaded files are supported (file extension).
     *
     * @param files uploaded files
     * @return true if uploaded files are supported, otherwise false.
     */
    private boolean isSupportedFiles(MultipartFile[] files) {
        logger.trace("ENTRY");
        if (!this.isUploaded(files)) {
            throw new IllegalArgumentException("Inappropriate parameter value files!");
        }

        for (int i = 0; i < files.length; i++) {
            if (!this.isCorrectFileExtension(files[i].getOriginalFilename())) {
                logger.trace("EXIT");
                return false;
            }
        }
        logger.trace("EXIT");
        return true;
    }

    /**
     * Checks if file has correct file extension.
     *
     * @param fileName
     * @return true if file has correct file extension, otherwise false.
     */
    private boolean isCorrectFileExtension(String fileName) {
        logger.trace("ENTRY");
        //minus one is meaned as length character "."
        int typeLength = fileName.length() - BUNDLE_TYPE.length() - 1;
        logger.trace("EXIT");
        return fileName.substring(typeLength).endsWith("." + BUNDLE_TYPE);
    }

    @Override
    public boolean supports(Class<?> clazz) {
        logger.debug("ENTRY");
        logger.debug("EXIT");
        return UploadBundleBean.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        logger.debug("ENTRY");

        UploadBundleBean bean = (UploadBundleBean) object;

        if (!this.isUploaded(bean.getBundles())) {
            errors.rejectValue("bundles", "bundles.required");
        }
        if (!this.isSupportedFiles(bean.getBundles())) {
            errors.rejectValue("bundles", "bundles.unsupported.files");
        }

        logger.debug("EXIT");
    }
}
