package cz.zcu.kiv.efps.efpportal.service;

import java.util.List;

import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.efpportal.data.entity.CocaexWrapper;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface CocaexDataCreator {

    public CocaexWrapper create(List<EfpEvalResult> efpResults);

}
