package cz.zcu.kiv.efps.efpportal.service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.client.EfpAssignmentClient;
import cz.zcu.kiv.efps.efpportal.utils.FrameworkType;
import cz.zcu.kiv.efps.types.lr.LR;
import org.springframework.stereotype.Service;

/**
 * The class for obtaining local registries from uploaded bundles.
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
@Service
public class BundleLRServiceImpl implements BundleLRService {

    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(BundleLRServiceImpl.class);

    /**
     * Gets local registries and corresponding bundles.
     * @param uploadedBundles list of uploaded bundles
     * @param framework framework
     * @return local registries and corresponding bundles
     * @throws IOException {@inheritDoc}
     */
    @Override
    public Map<String, Set<String>> getLRsAndCorrespondingBundles(
            final List<File> uploadedBundles,
            final String framework) throws IOException {
        logger.debug("ENTRY");
        String className = FrameworkType.getClassNameBySelectedFramework(framework);
        logger.debug("class name: " + className);

        Map<String, Set<String>> bundleLRsMap = new HashMap<String, Set<String>>();

        EfpAwareComponentLoader loader;
        loader = EfpAssignmentClient.initialiseComponentLoader(className);
        ComponentEfpAccessor accessor;
        String keyLR;

        for (File file : uploadedBundles) {
            accessor = loader.loadForRead(file.getCanonicalPath());
            Set<LR> localRegistrySet = accessor.getLRs();


            for (LR lr : localRegistrySet) {
                keyLR = String.valueOf(lr.getId());

                if (!bundleLRsMap.containsKey(keyLR)) {
                    bundleLRsMap.put(keyLR, new HashSet<String>());
                }
                bundleLRsMap.get(keyLR).add(file.getName());
            }
            //deletes all temporary files
            accessor.close();
        }
        logger.debug("EXIT");
        return bundleLRsMap;
    }
}
