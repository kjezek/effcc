package cz.zcu.kiv.efps.efpportal.service.storage;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface StorageManager {

    /**
     * Creates directory for given session.
     *
     * @throws IOException
     */
    void createStorage(String jSessionId) throws IOException;

    /**
     * Saves uploaded bundle to designated directory for current session.
     *
     * @param files uploaded bundles
     * @throws IOException when IO error occurs
     */
    void saveFiles(MultipartFile[] files, String jSessionId) throws IOException;

    /**
     * Gets list of uploded bundles file names in designated directory for
     * current session.
     *
     * @return list of filenames
     */
    List<String> getUploadedBundleNames(String jSessionId);

    /**
     * Gets list of absolute path of uploded bundles inclusive of bundle names
     * in designated directory for current session.
     *
     * @return list of absolute path of uploded bundles inclusive of bundle names
     */
    List<String> getAbsolutePathUploadedBundles(String jSessionId) throws IOException;

    /**
     * Gets list of absolute path uploaded bundles in designated directory for
     * current session.
     *
     * @return list of absolute path uploaded bundles
     */
    List<File> getUploadedBundles(String jSessionId);

    /**
     * Deletes file from storage.
     *
     * @param fileName
     */
    void deleteFile(String fileName, String jSessionId);

    /**
     * Deletes all files from storage.
     *
     * @throws IOException
     */
    void deleteAllFiles(String jSessionId) throws IOException;

    /**
     *  Deletes a directory recursively from storage.
     *
     * @throws IOException in case deletion is unsuccessful
     */
    void deleteDirectory(String jSessionId) throws IOException;

    /**
     * Checks if storage directory exist.
     *
     * @return true if storage directory exist, otherwise false.
     */
    public boolean isExist(String jSessionId);
}
