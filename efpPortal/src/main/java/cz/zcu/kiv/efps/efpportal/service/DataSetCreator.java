package cz.zcu.kiv.efps.efpportal.service;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;
import cz.zcu.kiv.efps.efpportal.data.entity.BundleData;
import cz.zcu.kiv.efps.efpportal.data.entity.EFPData;
import cz.zcu.kiv.efps.efpportal.data.entity.FeatureData;
import cz.zcu.kiv.efps.types.properties.EFP;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides methods for creation structures which represent result.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class DataSetCreator {

    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(DataSetCreator.class);
    /**
     * Structure for left tree of results.
     */
    private Map<String, BundleData> wholeStructureComparison;
    /**
     * Structure for left tree of results. <bundle name, <bundle name of
     * neighbour, object Bundle>>
     */
    private Map<String, Map<String, BundleData>> comparisonResults;
    /**
     * View on first bundle in the comparison result.
     */
    private Feature.AssignmentSide bundleSide;
    /**
     * Regual expression for parsing string containing version with others data
     */
    private static final String REGEX_FOR_VERSION = "\\d\\.\\d\\.\\d";
    /**
     * Constant indicates that comparison result could not be specified.
     */
    private static final String COULD_NOT_BE_COMPARED = "COULD_NOT_BE_COMPARED";
    /**
     * Constant indicates that comparison result is ok.
     */
    private static final String COMPARISON_RESULT_OK = "OK";
    /**
     * Constant indicates that comparison result is error.
     */
    private static final String COMPARISON_RESULT_ERROR = "ERROR";
    /**
     * Enum indicates which bundle will be saved into result structure.
     */
    private static enum BundlesOrder {
        FIRST,
        SECOND
    }
    /**
     * Signals which bundle will be saved EFP value.
     */
    private BundlesOrder bundleOrder;

    /**
     * Constructor.
     *
     * @param efpResults list of efp comparison results
     */
    public DataSetCreator(List<EfpEvalResult> efpResults) {
        logger.trace("ENTRY");
        this.wholeStructureComparison = new HashMap<String, BundleData>();
        this.comparisonResults = new HashMap<String, Map<String, BundleData>>();
        this.bundleSide = null;
        this.createCompleteData(efpResults);
        logger.trace("EXIT");
    }

    /**
     * Creates complete structure of results. In results are all bundles and
     * their all features with all EFPs.
     *
     * @param efpResults list of efp comparison results
     */
    private void createCompleteData(List<EfpEvalResult> efpResults) {
        logger.trace("ENTRY");
        for (EfpEvalResult item : efpResults) {
            String firstBundleName = convertPathToBundleName(item.getFirstComponent());
            String secondBundleName = convertPathToBundleName(item.getSecondComponent());
            Feature feature = item.getFeature();
            if (feature == null) {
                continue;
            }
            this.bundleSide = feature.getSide();
            Feature.AssignmentSide featureSide = feature.getSide();
            this.bundleSide = Feature.AssignmentSide.REQUIRED;
            if (firstBundleName != null && !"".equals(firstBundleName)) {
                String requiredBundle = "";
                if (featureSide == Feature.AssignmentSide.REQUIRED) {
                    this.bundleOrder = bundleOrder.FIRST;
                    requiredBundle = firstBundleName;
                } else {
                    this.bundleOrder = bundleOrder.SECOND;
                    requiredBundle = secondBundleName;
                }
                this.fillCompleteBundle(requiredBundle, item);
            }
            this.bundleSide = Feature.AssignmentSide.PROVIDED;
            if (firstBundleName != null && !"".equals(firstBundleName)
                    && secondBundleName != null && !"".equals(secondBundleName)) {
                if (featureSide == Feature.AssignmentSide.REQUIRED) {
                    this.bundleOrder = bundleOrder.SECOND;
                    this.createComparisonResults(firstBundleName, secondBundleName, item);
                } else {
                    this.bundleOrder = bundleOrder.FIRST;
                    this.createComparisonResults(secondBundleName, firstBundleName, item);
                }
            }
        }
        logger.trace("EXIT");
    }

    /**
     * Creates complete sub-structures one bundle. In result is one bundle and
     * its all features with all EFPs.
     *
     * @param bundleName bundle name for which are created sub-structures
     * @param item efp comparison result
     */
    private void fillCompleteBundle(String bundleName, EfpEvalResult item) {
        logger.trace("ENTRY");

        BundleData bundleData = this.wholeStructureComparison.get(bundleName);

        Map<String, FeatureData> featureMap;

        if (bundleData == null) {
            bundleData = new BundleData(bundleName, COMPARISON_RESULT_OK);
            featureMap = new HashMap<String, FeatureData>();
            bundleData.setFeatureMap(featureMap);
            this.addFeatureToMap(item, featureMap);
            this.wholeStructureComparison.put(bundleName, bundleData);
        } else {
            featureMap = bundleData.getFeatureMap();
            this.addFeatureToMap(item, featureMap);
        }
        this.setEfpResultsParents(bundleData, item);
        logger.trace("EXIT");
    }

    /**
     * Make short name of component from path.
     *
     * @param path name with path
     * @return bundle name
     */
    private String convertPathToBundleName(String path) {
        logger.trace("ENTRY");
        if (path == null) {
            return null;
        }
        int lastSeparator = path.lastIndexOf(File.separator);
        logger.trace("EXIT");
        return path.substring(lastSeparator + 1);
    }

    /**
     * Make short name of feature from full name of feature.
     *
     * @param featureFullName full name of feature
     * @return only name of feature
     */
    private String convertFeatureFullNameToFeatureName(final String featureFullName) {
        logger.trace("ENTRY");
        int lastSeparator = featureFullName.lastIndexOf(".");
        logger.trace("EXIT");
        return featureFullName.substring(lastSeparator + 1);
    }

    /**
     * Creates structure of results right tree relating with left tree. In
     * results relating with left tree are all bundles and their all features
     * with all EFPs.
     *
     * @param firstBundleName first bundle name
     * @param secondBundleName second bundle name neighbouring with first
     * @param item efp comparison result
     */
    private void createComparisonResults(String firstBundleName, String secondBundleName, EfpEvalResult item) {
        logger.trace("ENTRY");
        Map<String, BundleData> neighbours = this.comparisonResults.get(firstBundleName);
        BundleData bundleData;
        Map<String, FeatureData> featuresMap;

        if (neighbours == null) {
            neighbours = new HashMap<String, BundleData>();
            this.comparisonResults.put(firstBundleName, neighbours);
        }
        bundleData = neighbours.get(secondBundleName);

        if (bundleData == null) {
            bundleData = new BundleData(secondBundleName);
            neighbours.put(secondBundleName, bundleData);
            featuresMap = new HashMap<String, FeatureData>();
            bundleData.setFeatureMap(featuresMap);
        } else {
            featuresMap = bundleData.getFeatureMap();
        }
        this.addFeatureToMap(item, featuresMap);
        logger.trace("EXIT");
    }

    /**
     * Creates EFP and adds to feature.
     *
     * @param item efp comparison result
     * @param feature feature where EFP will be added
     */
    private void createEFPAndAddToFeature(EfpEvalResult item, FeatureData feature) {
        logger.trace("ENTRY");
        EFP efp = item.getEfp();
        if (efp == null) {
            return;
        }
        EFPData newEFP;
        if ((item.getTypeError() == EfpEvalResult.MatchingResult.MISSING
                && item.getSide() != this.bundleSide)) {
            newEFP = new EFPData(
                    efp.getName(),
                    COULD_NOT_BE_COMPARED,
                    efp.getType().toString(),
                    this.getEfpValue(item));
        } else {
            newEFP = new EFPData(
                    efp.getName(),
                    item.getTypeError().toString(),
                    efp.getType().toString(),
                    this.getEfpValue(item));
        }
        feature.addEFP(newEFP);
        logger.trace("EXIT");
    }

    /**
     * Creates new feature and adds to map.
     *
     * @param item efp comparison result
     * @param featuresMap map of feature where new feature will be added
     */
    private void addNewFeatureToMap(EfpEvalResult item, Map<String, FeatureData> featuresMap) {
        logger.trace("ENTRY");
        Feature feature = item.getFeature();
        String featureId = feature.getIdentifier();
        String featureName = convertFeatureFullNameToFeatureName(feature.getName());
        FeatureData newFeature = new FeatureData(featureId,
                featureName,
                this.getOnlyVersion(feature.getVersion().getIdentifier()),
                feature.getRepresentElement(),
                feature.getName(),
                this.bundleSide.toString(),
                COMPARISON_RESULT_OK);

        this.createEFPAndAddToFeature(item, newFeature);
        featuresMap.put(featureId, newFeature);
        logger.trace("EXIT");
    }

    /**
     * Add feature to map.
     *
     * @param item efp comparison result
     * @param featuresMap map of feature where new feature will be added
     */
    private void addFeatureToMap(EfpEvalResult item, Map<String, FeatureData> featuresMap) {
        logger.trace("ENTRY");
        FeatureData oldFeature = featuresMap.get(item.getFeature().getIdentifier());
        if (oldFeature != null) {
            this.createEFPAndAddToFeature(item, oldFeature);
        } else {
            this.addNewFeatureToMap(item, featuresMap);
        }
        logger.trace("EXIT");
    }

    /**
     * Gets structure for left tree of results.
     *
     * @return structure for left tree of results
     */
    public Map<String, Map<String, BundleData>> getComparisonResults() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return comparisonResults;
    }

    /**
     * Gets structure for left tree of results.
     *
     * @return structure for left tree of results
     */
    public Map<String, BundleData> getWholeStructureComparison() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return wholeStructureComparison;
    }

    /**
     * Gets EFP value.
     *
     * @param item EFP comparison result
     * @return EFP value
     */
    private String getEfpValue(EfpEvalResult item) {
        logger.trace("ENTRY");
        String efpValue = "no value";
        if (item.getTypeError() == EfpEvalResult.MatchingResult.MISSING) {
            logger.trace("EXIT");
            return efpValue;
        }
        if (this.bundleOrder == BundlesOrder.FIRST) {
            logger.trace("EXIT");
            return item.getFirstEfpValue() == null ? efpValue : item.getFirstEfpValue().getLabel();
        }
        logger.trace("EXIT");
        return item.getSecondEfpValue() == null ? efpValue : item.getSecondEfpValue().getLabel();
    }

    /**
     * Parses version with others data to version in format "x.x.x".
     *
     * @param str version with others data
     * @return version in format "x.x.x"
     */
    private String getOnlyVersion(String str) {
        logger.trace("ENTRY");
        Pattern pattern = Pattern.compile(REGEX_FOR_VERSION);
        Matcher matcher = pattern.matcher(str);
        String result = "";
        while (matcher.find()) {
            result = matcher.group();
        }
        logger.trace("EXIT");
        return result;
    }

    /**
     * Sets EFP comparison result superior elements.
     *
     * @param bundleData bundle with features which will be set EFP comparison
     * result
     * @param item EFP comparison result
     */
    private void setEfpResultsParents(BundleData bundleData, EfpEvalResult item) {
        logger.trace("ENTRY");
        if (item.getTypeError() == null || item.getFeature() == null) {
            return;
        }
        MatchingResult efpResult = item.getTypeError();
        if (efpResult != EfpEvalResult.MatchingResult.OK) {
            bundleData.setEfpResult(COMPARISON_RESULT_ERROR);
            bundleData.getFeatureMap().get(item.getFeature().getIdentifier()).setEfpResult(COMPARISON_RESULT_ERROR);
        }
        logger.trace("EXIT");
    }
}
