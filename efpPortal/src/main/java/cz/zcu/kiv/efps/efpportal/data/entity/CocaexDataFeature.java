package cz.zcu.kiv.efps.efpportal.data.entity;

import java.util.ArrayList;
import java.util.List;

public class CocaexDataFeature {
	private List<CocaexDataEfp> efps;

	public CocaexDataFeature() {
		this.efps = new ArrayList<CocaexDataEfp>();
	}

	/**
	 * @return the efps
	 */
	public List<CocaexDataEfp> getEfps() {
		return efps;
	}

	/**
	 * @param efps the efps to set
	 */
	public void setEfps(List<CocaexDataEfp> efps) {
		this.efps = efps;
	}

}