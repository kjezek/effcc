/**
 * 
 */
package cz.zcu.kiv.efps.efpportal.data.entity;

import java.util.HashMap;
import java.util.Map;

public class CocaexData {
	private Map<String, CocaexDataFeature> providedFeatures;
	private Map<String, CocaexDataFeature> requiredFeatures;
	
	public CocaexData() {
		this.providedFeatures = new HashMap<String, CocaexDataFeature>();
		this.requiredFeatures = new HashMap<String, CocaexDataFeature>();
	}

	/**
	 * @return the providedFeatures
	 */
	public Map<String, CocaexDataFeature> getProvidedFeatures() {
		return providedFeatures;
	}

	/**
	 * @return the importedFeatures
	 */
	public Map<String, CocaexDataFeature> getRequiredFeatures() {
		return requiredFeatures;
	}

}
