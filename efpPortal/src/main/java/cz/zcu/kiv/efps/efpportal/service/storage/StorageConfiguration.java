package cz.zcu.kiv.efps.efpportal.service.storage;

import org.slf4j.LoggerFactory;

/**
 * This class is used to configuration storage.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public final class StorageConfiguration {

    /**
     * Logging.
     */
    private org.slf4j.Logger logger = LoggerFactory.getLogger(StorageConfiguration.class);
    /**
     * Path to storage.
     */
    private String storageLocation;
    /**
     * Bundle extension.
     */
    private String bundleExtension;

    public StorageConfiguration(final String storageLocation, final String bundleExtension) {
        logger.trace("ENTRY");
        this.storageLocation = storageLocation;
        this.bundleExtension = bundleExtension;
        logger.trace("EXIT");
    }

    /**
     * Sets bundle extension
     *
     * @param bundleExtension bundle extension
     */
    public void setBundleExtension(String bundleExtension) {
        logger.trace("ENTRY");
        this.bundleExtension = bundleExtension;
        logger.trace("EXIT");
    }

    /**
     * Sets storage location
     *
     * @param storageLocation path to storage
     */
    public void setStorageLocation(String storageLocation) {
        logger.trace("ENTRY");
        this.storageLocation = storageLocation;
        logger.trace("EXIT");
    }

    /**
     * Gets bundle extension.
     *
     * @return bundle extension.
     */
    public String getBundleExtension() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return bundleExtension;
    }

    /**
     * Gets storage location.
     *
     * @return storage location.
     */
    public String getStorageLocation() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return storageLocation;
    }
}
