package cz.zcu.kiv.efps.efpportal.data.entity;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class represents data bundle.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class BundleData {

    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(BundleData.class);
    /**
     * Bundle name.
     */
    private String name;
    /**
     * Result comparison of EFP.
     */
    private String efpResult;
    /**
     * Map with features which belongs this bundle.
     */
    private Map<String, FeatureData> featureMap;

    /**
     * Constructor.
     * @param name bundle name
     * @param efpResult result comparison of EFP
     */
    public BundleData(String name, String efpResult) {
        logger.trace("ENTRY");
        this.name = name;
        this.efpResult = efpResult;
        logger.trace("EXIT");
    }

    /**
     * Constructor.
     * @param name bundle name
     */
    public BundleData(String name) {
        logger.trace("ENTRY");
        this.name = name;
        logger.trace("EXIT");
    }

    /**
     * Returns result comparison of EFP.
     * @return result comparison of EFP.
     */
    public String getEfpResult() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return efpResult;
    }

    /**
     * Returns bundle name.
     * @return bundle name
     */
    public String getName() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return name;
    }

    /**
     * Returns map with features which belongs this bundle.
     * @return map with features which belongs this bundle.
     */
    public Map<String, FeatureData> getFeatureMap() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return featureMap;
    }

    /**
     * Sets map with features which belongs this bundle.
     */
    public void setFeatureMap(Map<String, FeatureData> featureMap) {
        logger.trace("ENTRY");
        this.featureMap = featureMap;
        logger.trace("EXIT");
    }

    /**
     * Sets result comparison of EFP.
     */
    public void setEfpResult(String efpResult) {
        logger.trace("ENTRY");
        this.efpResult = efpResult;
        logger.trace("EXIT");
    }
}
