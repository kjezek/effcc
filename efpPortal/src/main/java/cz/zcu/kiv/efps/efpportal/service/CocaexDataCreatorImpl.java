package cz.zcu.kiv.efps.efpportal.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.efpportal.data.entity.CocaexData;
import cz.zcu.kiv.efps.efpportal.data.entity.CocaexDataEfp;
import cz.zcu.kiv.efps.efpportal.data.entity.CocaexDataFeature;
import cz.zcu.kiv.efps.efpportal.data.entity.CocaexWrapper;
import cz.zcu.kiv.efps.types.properties.EFP;

@Service
public class CocaexDataCreatorImpl implements CocaexDataCreator {

	private static final String DELIMITER = ";";
	private Map<String, String> featureMappings = new HashMap<String, String>();
	private int featureCounter = 1;
	
	private Map<String, String> efpMappings = new HashMap<String, String>();
	private int efpCounter = 1;
	
	/**
	 * Logging.
	 */
	private Logger logger = LoggerFactory.getLogger(CocaexDataCreator.class);
	

	public CocaexWrapper create(List<EfpEvalResult> efpResults) {
		logger.trace("ENTRY");
		
		CocaexWrapper wrapper = new CocaexWrapper();
		
		Map<String, CocaexData> components = new HashMap<String, CocaexData>();
		wrapper.setData(components);
		
		for (EfpEvalResult efpEvalResult : efpResults) {
			String[] tempComponents = { getFilenameFromPath(efpEvalResult.getFirstComponent()),
					getFilenameFromPath(efpEvalResult.getSecondComponent()) };

			setComponentOrder(efpEvalResult, tempComponents);
			
			CocaexData providingComponent = getComponent(components, tempComponents[0]);
			CocaexData importingComponent = getComponent(components, tempComponents[1]);
			
			if (providingComponent != null) {
				addToFeatures(tempComponents[1] ,efpEvalResult, providingComponent.getProvidedFeatures(), true);
			}
			
			if (importingComponent != null) {
				addToFeatures(tempComponents[0], efpEvalResult, importingComponent.getRequiredFeatures(), false);
			}
			
		}
		
		switchKeyAndValuesInMappings(wrapper);
		
		logger.trace("EXIT");
		return wrapper;
	}
	
	private void switchKeyAndValuesInMappings(CocaexWrapper wrapper) {
		Map<String, String> featureMappingsSwitched = new HashMap<String, String>();
		
		for (Map.Entry<String, String> entry : featureMappings.entrySet()) {
			featureMappingsSwitched.put(entry.getValue(), entry.getKey());
		}
		
		Map<String, String> efpMappingsSwitched = new HashMap<String, String>();
		
		for (Map.Entry<String, String> entry : efpMappings.entrySet()) {
			efpMappingsSwitched.put(entry.getValue(), entry.getKey());
		}
		
		wrapper.setFeatureMappings(featureMappingsSwitched);
		wrapper.setEfpMappings(efpMappingsSwitched);
		
	}

	private void addToFeatures(String endComponent, EfpEvalResult feature, Map<String, CocaexDataFeature> cocaexFeatures, boolean shouldTakeFirstEfpValue) {
		String featureName = endComponent + DELIMITER + feature.getFeature().getName();
		String featureNameMapped = featureMappings.get(featureName);
		
		if (featureNameMapped == null) {
			featureNameMapped = String.valueOf(featureCounter);
			featureCounter++;
			featureMappings.put(featureName, featureNameMapped);
		}
		
		CocaexDataFeature cocaexFeature = cocaexFeatures.get(featureNameMapped);
		
		if (cocaexFeature == null) {
			cocaexFeature = new CocaexDataFeature();
			cocaexFeatures.put(featureNameMapped, cocaexFeature);
		}
		
		EFP efp = feature.getEfp();
		
		if (efp == null) {
			return;
		}
		
		CocaexDataEfp cocaexEfp = new CocaexDataEfp();
		
		String efpName = efp.getName();
		String efpNameMapped = efpMappings.get(efp.getName());
		
		if (efpNameMapped == null) {
			efpNameMapped = String.valueOf(efpCounter);
			efpCounter++;
			efpMappings.put(efpName, efpNameMapped);
		}
		
		cocaexEfp.setEfpName(efpNameMapped);
		cocaexEfp.setEfpValueTypeName(efp.getValueType().getName());
		cocaexEfp.setEfpValue((shouldTakeFirstEfpValue) ? feature.getFirstEfpValue() : feature.getSecondEfpValue());
		cocaexEfp.setTypeError(feature.getTypeError());
		
		cocaexFeature.getEfps().add(cocaexEfp);
	}

	private CocaexData getComponent(Map<String, CocaexData> components, String componentName) {
		CocaexData component = components.get(componentName);
		
		if (component == null && componentName != null && !componentName.isEmpty()) {
			component = new CocaexData();
			components.put(componentName, component);
		}
		
		return component;
	}

	/**
	 * Switches the order of elements so the first element is always providing and the second requiring 
	 * 
	 * @param efpEvalResult
	 *            Results needed for arbitration.
	 * @param components
	 *            Two components to check.
	 * @return True if there was a switching needed. False otherwise.
	 */
	private boolean setComponentOrder(EfpEvalResult efpEvalResult,
			String[] components) {
		logger.trace("ENTRY");

		if (efpEvalResult.getFeature() == null) {
			logger.trace("EXIT");
			return false;
		}

		// switch sides according to the signature
		if (!efpEvalResult.getFeature().getSide().toString().equals(Feature.AssignmentSide.PROVIDED.toString())) {
			// switch the first and the second components
			String tmp = components[0];
			components[0] = components[1];
			components[1] = tmp;
			
			logger.trace("EXIT");
			return true;
		}

		logger.trace("EXIT");
		return false;
	}
	
	/**
     * Extracts filename from path
     * 
     * @param fullName
     * @return
     */
    private String getFilenameFromPath(String fullName) {
         logger.trace("ENTRY");
         File path = new File(fullName);
         logger.trace("EXIT");

         return path.getName();
    }
	
}
