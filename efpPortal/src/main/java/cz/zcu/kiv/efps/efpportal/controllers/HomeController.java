package cz.zcu.kiv.efps.efpportal.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for home page.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
@Controller
@RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
public class HomeController {
    /** logging. */
    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    /**
     * Action for showing home page with information about EFP.
     *
     * @return path for view
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return "home/index";
    }
}
