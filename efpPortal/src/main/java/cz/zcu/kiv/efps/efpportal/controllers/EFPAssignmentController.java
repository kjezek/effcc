package cz.zcu.kiv.efps.efpportal.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *  Controller for application EFPAssignment.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
@Controller
@RequestMapping(value = "/efpassignment", method = RequestMethod.GET)
public class EFPAssignmentController {
    /** Logging.*/
    private Logger logger = LoggerFactory.getLogger(EFPAssignmentController.class);

    /**
     * Action for showing page with EFPAssignment.
     * 
     * @return path for view
     */
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String index() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
		return "efpassignment/index";
	}
}
