package cz.zcu.kiv.efps.efpportal.service.storage;


import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class storage manager storage for uploaded bundle files. Files are managed
 * for each session.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
@Service
public class StorageManagerImpl implements StorageManager {

    @Autowired
    private StorageConfiguration config;
    /**
     * Logging.
     */
    private org.slf4j.Logger logger = LoggerFactory.getLogger(StorageManagerImpl.class);


    /**
     * Creates directory for given session.
     *
     * @throws IOException
     */
    @Override
    public void createStorage(String jSessionId) throws IOException {
        logger.trace("ENTRY");
        FileUtils.forceMkdir(new File(this.config.getStorageLocation() + jSessionId));
        logger.trace("EXIT");
    }

    /**
     * Saves uploaded bundle to designated directory for current session.
     *
     * @param files uploaded bundles
     * @throws IOException when IO error occurs
     */
    @Override
    public void saveFiles(MultipartFile[] files, String jSessionId) throws IOException {
        logger.trace("ENTRY");

        logger.debug("Path to destination directory: " + this.config.getStorageLocation() + jSessionId);
        for (int i = 0; i < files.length; i++) {
            files[i].transferTo(new File(this.config.getStorageLocation() + jSessionId + File.separator + files[i].getOriginalFilename()));
        }
        logger.trace("EXIT");
    }

    /**
     * Gets list of uploded bundles file names in designated directory for
     * current session.
     *
     * @return list of filenames
     */
    @Override
    public List<String> getUploadedBundleNames(String jSessionId) {
        logger.trace("ENTRY");
        String[] extension = {this.config.getBundleExtension()};
        logger.info(this.config.getStorageLocation() + jSessionId);
        LinkedList<File> fileNames = (LinkedList<File>) FileUtils.listFiles(
                new File(this.config.getStorageLocation() + jSessionId), extension, false);
        List<String> bundleNames = new LinkedList<String>();
        for (File file : fileNames) {
            bundleNames.add(file.getName());
        }
        logger.trace("EXIT");
        return bundleNames;
    }
    
    /**
     * Gets list of absolute path of uploded bundles inclusive of bundle names 
     * in designated directory for current session.
     *
     * @return list of absolute path of uploded bundles inclusive of bundle names
     */
    @Override
    public List<String> getAbsolutePathUploadedBundles(String jSessionId) throws IOException {
        logger.trace("ENTRY");
        String[] extension = {this.config.getBundleExtension()};
        logger.info(this.config.getStorageLocation() + jSessionId);
        LinkedList<File> fileNames = (LinkedList<File>) FileUtils.listFiles(
                new File(this.config.getStorageLocation() + jSessionId), extension, false);
        List<String> bundleNames = new LinkedList<String>();
        for (File file : fileNames) {
            bundleNames.add(file.getCanonicalPath());
        }
        logger.trace("EXIT");
        return bundleNames;
    }
    
    /**
     * Gets list of absolute path uploaded bundles in designated directory for
     * current session.
     *
     * @return list of absolute path uploaded bundles
     */
    @Override
    public List<File> getUploadedBundles(String jSessionId) {
        logger.trace("ENTRY");
        String[] extension = {this.config.getBundleExtension()};
        logger.info(this.config.getStorageLocation() + jSessionId);
        LinkedList<File> fileNames = (LinkedList<File>) FileUtils.listFiles(
                new File(this.config.getStorageLocation() + jSessionId), extension, false);
        logger.trace("EXIT");
        return fileNames;
    }

    /**
     * Deletes file from storage.
     *
     * @param fileName
     */
    @Override
    public void deleteFile(String fileName, String jSessionId) {
        logger.trace("ENTRY");
        if (fileName == null) {
            throw new NullPointerException("The parameter fileName is null!");
        }
        String path = this.config.getStorageLocation() + jSessionId + File.separator + fileName;
        logger.debug("All path to delete file: " + path);
        File filePath = new File(path);

        if (filePath.isDirectory()) {
            throw new IllegalArgumentException("The parameter fileName " + fileName + " can not be directory!");
        }

        if (!FileDeleteStrategy.NORMAL.deleteQuietly(filePath)) {
            throw new IllegalStateException("The file " + fileName + " can not be deleted!");
        }
        logger.debug("File " + fileName + " was deleted.");

        logger.trace("EXIT");
    }

    /**
     * Deletes all files from storage.
     *
     * @throws IOException
     */
    @Override
    public void deleteAllFiles(String jSessionId) throws IOException {
        logger.trace("ENTRY");

        FileUtils.cleanDirectory(new File(this.config.getStorageLocation() + jSessionId));

        logger.trace("EXIT");
    }
    
    /**
     *  Deletes a directory recursively from storage.
     *
     * @throws IOException in case deletion is unsuccessful
     */
    @Override
    public void deleteDirectory(String jSessionId) throws IOException {
        logger.trace("ENTRY");

        FileUtils.deleteDirectory(new File(this.config.getStorageLocation() + jSessionId));

        logger.trace("EXIT");
    }

    /**
     * Checks if storage directory exist.
     *
     * @return true if storage directory exist, otherwise false.
     */
    @Override
    public boolean isExist(String jSessionId) {
        File file = new File(this.config.getStorageLocation() + jSessionId);
        return file.isDirectory();
    }
}
