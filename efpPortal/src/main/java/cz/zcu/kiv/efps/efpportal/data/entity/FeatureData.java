package cz.zcu.kiv.efps.efpportal.data.entity;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class represents data feature.
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public class FeatureData {

    /**
     * Logging.
     */
    private Logger logger = LoggerFactory.getLogger(FeatureData.class);
    /**
     * A literal indentifier of this object.
     */
    private String identificator;
    /**
     * Map with EFP which belongs this feature.
     */
    private Map<String, EFPData> efpMap;
    /**
     * Feature name.
     */
    private String name;
    /**
     * Name which is used for identifying this feature in the system.
     */
    private String fullName;
    /**
     * Version of this feature.
     */
    private String version;
    /**
     * Type this feature represents in the system.
     */
    private String type;
    /**
     * Side of the component this feature is.
     */
    private String requiredOrProvided;
    /**
     * Result comparison of EFP.
     */
    private String efpResult;

    /**
     * Constructor.
     *
     * @param identificator A literal indentifier of this object.
     * @param name Feature name.
     * @param version Version of this feature.
     * @param type Type this feature represents in the system.
     * @param fullName Name which is used for identifying this feature in the
     * system.
     * @param requiredOrProvided Side of the component this feature.
     * @param efpResult Result comparison of EFP.
     */
    public FeatureData(String identificator, String name, String version,
            String type, String fullName, String requiredOrProvided, String efpResult) {
        logger.trace("ENTRY");
        this.identificator = identificator;
        this.name = name;
        this.version = version;
        this.type = type;
        this.efpMap = new HashMap<String, EFPData>();
        this.fullName = fullName;
        this.requiredOrProvided = requiredOrProvided;
        this.efpResult = efpResult;
        logger.trace("ENTRY");
    }

    /**
     * Returns result comparison of EFP.
     *
     * @return result comparison of EFP.
     */
    public String getEfpResult() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return efpResult;
    }

    /**
     * Sets result comparison of EFP.
     */
    public void setEfpResult(String efpResult) {
        logger.trace("ENTRY");
        this.efpResult = efpResult;
        logger.trace("EXIT");
    }

    /**
     * Returns map with EFP which belongs this feature.
     * @return EFP map
     */
    public Map<String, EFPData> getEfpMap() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return efpMap;
    }

    /**
     * Adds EFPData to EFP map.
     */
    public void addEFP(EFPData efp) {
        logger.trace("ENTRY");
        this.efpMap.put(efp.getName(), efp);
        logger.trace("EXIT");
    }

    /**
     * Returns a literal indentifier of this object.
     * @return a literal indentifier of this object.
     */
    public String getIdentificator() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return identificator;
    }

    /**
     * Returns feature name.
     * @return feature name
     */
    public String getName() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return name;
    }

    /**
     * Returns type this feature represents in the system.
     * @return type this feature represents in the system
     */
    public String getType() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return type;
    }

    /**
     * Returns a version of this feature.
     * @return a version of this feature
     */
    public String getVersion() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return version;
    }

    /**
     * Returns a name which is used for identifying this feature in the
     * system.
     * @return the name
     */
    public String getFullName() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return fullName;
    }

    /**
     * Returns side of the component this feature.
     * @return the side of the component
     */
    public String getRequiredOrProvided() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return requiredOrProvided;
    }

    @Override
    public String toString() {
        logger.trace("ENTRY");
        logger.trace("EXIT");
        return this.identificator.toString() + "\n EFPs: {" + this.efpMap.toString() + "}\n";
    }
}
