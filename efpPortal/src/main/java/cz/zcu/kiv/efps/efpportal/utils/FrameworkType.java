package cz.zcu.kiv.efps.efpportal.utils;

import cz.zcu.kiv.efps.assignment.cosi.CosiAssignmentImpl;
import cz.zcu.kiv.efps.assignment.osgi.OSGiAssignmentImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class which offering methods for determine framework type.
 *
 * @author Daniel Kuneš <dkunes@students.zcu.cz>
 */
public final class FrameworkType {
    /**
     * Constant for OSGi framework.
     */
    private static final String OSGI = "osgi";
    /**
     * Constant for CoSi framework.
     */
    private static final String COSI = "cosi";
    /**
     * Logging.
     */
    private static Logger logger = LoggerFactory.getLogger(FrameworkType.class);

    /**
     * Private constructor.
     */
    private FrameworkType() { }

    /**
     * Tests if framework parameter is OSGi framework.
     * @param framework framework
     * @return true if framework parameter is OSGi framework, otherwise false.
     */
    public static boolean isOSGi(final String framework) {
        FrameworkType.logger.trace("ENTRY");
        FrameworkType.logger.trace("EXIT");
        return FrameworkType.OSGI.equalsIgnoreCase(framework);
    }
    /**
     * Tests if framework parameter is CoSi framework.
     * @param framework framework
     * @return true if framework parameter is CoSi framework, otherwise false.
     */
    public static boolean isCoSi(final String framework) {
        FrameworkType.logger.trace("ENTRY");
        FrameworkType.logger.trace("EXIT");
        return FrameworkType.COSI.equalsIgnoreCase(framework);
    }

    /**
     * Gets class name by selected framework.
     * @param framework framework
     * @return class name
     */
    public static String getClassNameBySelectedFramework(final String framework) {
        logger.debug("ENTRY");
        String className = "";
        if (FrameworkType.isOSGi(framework)) {
            className = OSGiAssignmentImpl.class.getName();
        } else if (FrameworkType.isCoSi(framework)) {
            className = CosiAssignmentImpl.class.getName();
        } else {
            throw new IllegalArgumentException("The parameter framework must "
                    + "be chosen from enumeration of class BundleLRService.");
        }
        logger.debug("EXIT");
        return className;
    }
}
