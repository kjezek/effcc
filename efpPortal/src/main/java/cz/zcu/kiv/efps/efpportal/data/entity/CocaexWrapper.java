package cz.zcu.kiv.efps.efpportal.data.entity;

import java.util.Map;

public class CocaexWrapper {
	Map<String, String> featureMappings;
	Map<String, String> efpMappings;
	Map<String, CocaexData> data;
	/**
	 * @return the featureMappings
	 */
	public Map<String, String> getFeatureMappings() {
		return featureMappings;
	}
	/**
	 * @param featureMappings the featureMappings to set
	 */
	public void setFeatureMappings(Map<String, String> featureMappings) {
		this.featureMappings = featureMappings;
	}
	/**
	 * @return the efpMappings
	 */
	public Map<String, String> getEfpMappings() {
		return efpMappings;
	}
	/**
	 * @param efpMappings the efpMappings to set
	 */
	public void setEfpMappings(Map<String, String> efpMappings) {
		this.efpMappings = efpMappings;
	}
	/**
	 * @return the data
	 */
	public Map<String, CocaexData> getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Map<String, CocaexData> data) {
		this.data = data;
	}
	
	
}
