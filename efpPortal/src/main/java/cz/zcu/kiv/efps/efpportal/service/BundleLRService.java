package cz.zcu.kiv.efps.efpportal.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface BundleLRService {
    Map<String, Set<String>> getLRsAndCorrespondingBundles(
            List<File> uploadedBundles,
            String framework) throws IOException;
}
