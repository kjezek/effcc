<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1>EFP Comparator</h1>
<hr/>
<nav id="wizard">
    <ul>
        <li class="left_inactive">Choose framework</li>
        <li class="middle_inactive">Upload bundles</li>
        <li class="middle_inactive">Choose LR ID</li>
        <li class="right_active">Results</li>
    </ul>
</nav>
<div id="window">
    <h2>Results of comparison all bundles</h2>
    <spring:url var="iconsUrl" value="/resources/images/icons/" />
    <spring:url var="iconsBundleUrl" value="/resources/images/icons/BUNDLE.png" />
    <spring:url var="iconsFeatureUrl" value="/resources/images/icons/FEATURE.png" />
    <spring:url var="iconsEfpUrl" value="/resources/images/icons/EFP.png" />
    <div id="iconsUrl" data-iconsUrl="${iconsUrl}" style="display: none;"/>
    <div id="results">
        <section id="left_column">
            <c:if test="${not empty leftColumn}">
                <ul id="leftTree" class="treeview-black">
                    <c:forEach var="bundle" items="${leftColumn}">
                        <li class="bundle">
                            <div class="case">
                                <img title="Bundle" src="${iconsBundleUrl}"/> 
                                <span class="clickable cancelClickEvent leaf" data-bundleName="${bundle.key}" data-clickEvent="bundle">${bundle.key}</span> 
                            </div>
                            <img  class="result_flag" title="${bundle.value.efpResult}" src="${iconsUrl}${bundle.value.efpResult}.png"/>
                            <c:if test="${not empty bundle.value.featureMap}">
                                <ul>
                                    <c:forEach var="feature" items="${bundle.value.featureMap}">
                                        <li class="cancelClickEvent">
                                            <div class="case">
                                                <img title="Feature" src="${iconsFeatureUrl}"/><img title="${feature.value.requiredOrProvided}" src="${iconsUrl}${feature.value.requiredOrProvided}.png"/><img title="${feature.value.type}" src="${iconsUrl}${feature.value.type}.png"/> 
                                                <span class="clickable cancelClickEvent leaf" data-clickEvent="feature" data-bundleName="${bundle.key}" data-featureName="${feature.key}" data-title="${feature.value.fullName}">${feature.value.name}, ${feature.value.version}</span>
                                            </div>
                                            <img  class="result_flag" title="${feature.value.efpResult}" src="${iconsUrl}${feature.value.efpResult}.png"/>
                                            <c:if test="${not empty feature.value.efpMap}">
                                                <ul>
                                                    <c:forEach var="efp" items="${feature.value.efpMap}">
                                                        <li>
                                                            <div class="case">
                                                                <img title="EFP" src="${iconsEfpUrl}"/><img title="${efp.value.type}" src="${iconsUrl}${efp.value.type}.png"/> 
                                                                <span class="clickable leaf cancelClickEvent" data-clickEvent="efp" data-bundleName="${bundle.key}" data-featureName="${feature.key}" data-efpName="${efp.key}">${efp.key} 
                                                                    <strong data-parent="efp" style="vertical-align: top;">value:</strong> ${efp.value.value}
                                                                </span>
                                                            </div>
                                                            <img class="result_flag" title="${efp.value.result}" src="${iconsUrl}${efp.value.result}.png"/>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </c:if>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </c:if>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
            <br class="clear" />
            <br/>
            <spring:url var="urlToChooseLR" value="/efpcomparator/choose_lr" />
            <form method="post">
                <input id="prev_url" type="hidden" value="${urlToChooseLR}"/>
                <input class="nav_left" type="submit" name="prev_button" value="Previous" />
            </form>
            
            <form method="post" action="${url}">
				<textarea id="json_data" name="data" readonly>${data}</textarea>
				<input class="nav_right" type="submit" value="Submit to Cocaex">
			</form>
        </section>
        <section id="right_column">
            <ul id="rightTree" class="treeview-black">

            </ul>
            <br class="clear" />
        </section>
    </div>
    <br class="clear" />
</div>

<c:if test="${not empty rightColumn}">
    <script>
        var rightColumn = $.parseJSON('${rightColumn}');
    </script>
</c:if>
