<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<h1>Compatibility Verification</h1>
<hr/>
<nav id="wizard">
	<ul>
		<li class="left_active">Choose framework</li>
		<li class="middle_inactive">Upload bundles</li>
		<li class="middle_inactive">Choose LR ID</li>
		<li class="right_inactive">Results</li>
	</ul>
</nav>
<div id="window">
	<div id="choose_framework">
		<h4>Choose framework:</h4>
		<spring:url var="urlToUploadBundles" value="/efpcomparator/upload_bundles" />
		<form method="post">
			<select name="framework">
				<option value="osgi" selected="selected">OSGi framework</option>
				<option value="cosi">CoSi framework</option>
			</select>
			<hr/>
            <input id="next_url" type="hidden" value="${urlToUploadBundles}"/>
			<input type="submit" name="next_button" value="Next" />
			<br class="clear"/>
		</form>
	</div>
</div>
