<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h1>EFP Comparator</h1>
<hr/>
<nav id="wizard">
    <ul>
        <li class="left_inactive">Choose framework</li>
        <li class="middle_active">Upload bundles</li>
        <li class="middle_inactive">Choose LR ID</li>
        <li class="right_inactive">Results</li>
    </ul>
</nav>
<div id="window">
    <div id="upload">
        <c:if test="${not empty errors}">
            <h4 class="header_error">Errors:</h4> 
            <ul id="error_messages">
                <c:forEach var="errorMessage" items="${errors}">
                    <li><spring:message code="${errorMessage}" /></li>
                </c:forEach>
            </ul>
        </c:if>
        <h4>Upload bundles:</h4>
        <spring:url var="urlToUploadBundlesAjax" value="/efpcomparator/upload_bundlesAjax" />
        <form:form commandName="uploadBundleBean" method="post" enctype='multipart/form-data'>
            <form:input path="bundles" type="file" multiple="multiple" required="required"/>
            <br class="clear"/>
            <input id="upload_url" type="hidden" value="${urlToUploadBundlesAjax}"/>
            <input class="upload" type="submit" name="upload_bundles_button" value="Upload" />
            <br class="clear"/>
            <label for="progress">Uploaded: 0%</label><progress id="progress" value="0" max="100"></progress>
            <hr />
        </form:form>
        <c:if test="${not empty bundleNames}">
            <h4>Uploaded bundles:</h4>
            <ul>
                <spring:url var="imgUrl" value="/resources/images/other/delete.png" />
                <spring:url var="urlToDeleteBundleAjax" value="/efpcomparator/delete_bundle_ajax/" />
                <c:forEach var="bundleName" items="${bundleNames}">
                    <li><strong>${bundleName}</strong><img id="${bundleName}" class="delete_bundle" title="delete" src="${imgUrl}" /></li>
                    </c:forEach>
            </ul>
            <ul>
                <spring:url var="imgUrl" value="/resources/images/other/delete.png" />
                <spring:url var="urlToDeleteAllBundlesAjax" value="/efpcomparator/delete_all_bundles_ajax/" />
                <li><strong>Delete all: </strong><img id="${urlToDeleteAllBundlesAjax}" class="delete_all_bundles" title="delete" src="${imgUrl}" /></li>
            </ul>
        </c:if>
        <spring:url var="urlToChooseFramework" value="/efpcomparator/" />
        <spring:url var="urlToChooseLR" value="/efpcomparator/choose_lr" />
        <form method="post">
            <input id="prev_url" type="hidden" value="${urlToChooseFramework}" />
            <input id="next_url" type="hidden" value="${urlToChooseLR}"/>
            <input class="nav_left" type="submit" name="prev_button" value="Previous" />
            <c:choose>
                <c:when test="${not empty bundleNames}">
                    <input class="nav_right" type="submit" name="next_button" value="Next" />
                </c:when>
                <c:otherwise>
                    <input class="nav_right" type="submit" name="next_button" value="Next" disabled="disabled" />
                </c:otherwise>
            </c:choose>
        </form>
    </div>
</div>