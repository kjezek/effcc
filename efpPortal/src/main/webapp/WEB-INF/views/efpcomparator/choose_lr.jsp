<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1>EFP Comparator</h1>
<hr/>
<nav id="wizard">
    <ul>
        <li class="left_inactive">Choose framework</li>
        <li class="middle_inactive">Upload bundles</li>
        <li class="middle_active">Choose LR ID</li>
        <li class="right_inactive">Results</li>
    </ul>
</nav>
<div id="window">
    <div id="choose_id">
        <form method="post">
            <h4 class="id">ID LRs all bundles:</h4>
            <select id="localRegistryID" name="id">
                <c:forEach var="id" items="${localRegistryIDs}">
                    <option value="${id}">${id}</option>
                </c:forEach>
            </select>
            <hr />
            <h4>Uploaded bundles:</h4>
            <ul>
                <spring:url var="imgUrl" value="/resources/images/other/chain.png" />
                <c:forEach var="bundle" items="${bundleNames}">
                    <li><img src="${imgUrl}" id="${bundle}" class="chain"/>${bundle}</li>
                </c:forEach>
            </ul>
            <spring:url var="urlToUploadBundles" value="/efpcomparator/upload_bundles" />
            <spring:url var="urlToResults" value="/efpcomparator/results" />
            <input id="prev_url" type="hidden" value="${urlToUploadBundles}" />
            <input id="next_url" type="hidden" value="${urlToResults}"/>
            <input class="nav_left" type="submit" name="prev_button" value="Previous" />
            <input class="nav_right" type="submit" name="next_button" value="Next" />
        </form>
    </div>
</div>
<c:if test="${not empty jsonData}">
    <script>
    var jsonData = $.parseJSON('${jsonData}');
    </script>
</c:if>