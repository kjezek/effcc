<h1>Home ${sessionID}</h1>
<hr/>
<article>
	<h4>EFFCC - Extra Functional Property Featured Compatibility Checks</h4>
	<p>
		EFFCC is a set of modules allowing to enrich existing Java-based component frameworks such as OSGi with featured compatibility checks
		concerning extra-functional properties.
	</p>
</article>
<article>
	<h4>Features</h4>
	<p>
		Visualize compatibility of your OSGi bundles: <a href="efpcomparator">Upload your bundles</a>
	</p>
</article>


<article>
	<h4>Project</h4>
	<p>
		This project is open-source. Feel free to enjoy and contribute!
	</p>

	<br>
		<ul>
			<li style="margin-left : 20px"> Hosted at: <a href="https://www.assembla.com/spaces/show/efps">https://www.assembla.com/spaces/show/efps</a>
			<li style="margin-left : 20px"> Source code: <a href="https://subversion.assembla.com/svn/efps/">https://subversion.assembla.com/svn/efps/</a>
		</ul>
</article>
