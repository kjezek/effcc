<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib tagdir="/WEB-INF/tags/menu" prefix="menu" %>
<% request.setCharacterEncoding("utf-8");%>
<% response.setCharacterEncoding("utf-8");%>

<nav id="menu">
	<ul>
		<li><menu:item requestMap="/" label="Home" /></li>
		<li><menu:item requestMap="/efpcomparator/" label="Compatibility Verification" /></li>
		<li><menu:item requestMap="/efpassignment/" label="Properties Assignment" /></li>
	</ul>
</nav>