<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8");%>
<% response.setCharacterEncoding("utf-8");%>

<footer id="footer">
	<a class="zcu" href="http://www.zcu.cz"></a>
	<div class="footer_separator"></div>
	<p class="info">	Západočeská univerzita v Plzni - ZČU<br/>
		Fakulta aplikovaných věd - FAV<br/>
		Katedra informatiky a výpočtní techniky - KIV
	</p>
	<div class="footer_separator"></div>
	<a class="kiv" href="http://www.kiv.zcu.cz"></a>
	<div class="footer_separator"></div>
	<p class="desc">
		EFFCC Portal @ 2012-2013<br/>
		UI for Comparator - Daniel Kuneš<br/>
		Design by Daniel Kuneš
	</p>
</footer>