<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<% request.setCharacterEncoding("utf-8");%>
<% response.setCharacterEncoding("utf-8");%>

<!DOCTYPE html>
<html>
	<head>
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <spring:url var="mainCssUrl" value="/resources/css/main.css" />
        <spring:url var="treeCssUrl" value="/resources/css/efp_comparator/jquery.treeview.css" />
        <spring:url var="qtip2CssUrl" value="/resources/css/jquery.qtip.min.css" />
        
        <link rel="stylesheet" href="${qtip2CssUrl}" type="text/css" media="screen" />
		<link rel="stylesheet" href="${mainCssUrl}" type="text/css" media="screen" />
        <link rel="stylesheet" href="${treeCssUrl}" type="text/css" media="screen" />
        
        <spring:url var="jqueryCookieUrl" value="/resources/js/jquery.cookie.js" />
        <spring:url var="cookieHandlerUrl" value="/resources/js/CookieHandler.js" />
        <spring:url var="jqueryUrl" value="/resources/js/jquery-1.7.1.js" />
        <spring:url var="jqueryFormUrl" value="/resources/js/jquery.form.js" />
        
        <spring:url var="wizardUrl" value="/resources/js/efpcomparator/Wizard.js" />
        <spring:url var="wizardManagerUrl" value="/resources/js/efpcomparator/WizardManager.js" />
        <spring:url var="uploadBundleEventsUrl" value="/resources/js/efpcomparator/UploadBundleEvents.js" />
        <spring:url var="uploadBundleActionsUrl" value="/resources/js/efpcomparator/UploadBundleActions.js" />
        <spring:url var="chooseLREventsUrl" value="/resources/js/efpcomparator/ChooseLREvents.js" />
        <spring:url var="chooseLRActionsUrl" value="/resources/js/efpcomparator/ChooseLRActions.js" />
        <spring:url var="resultEventsUrl" value="/resources/js/efpcomparator/ResultEvents.js" />
        <spring:url var="resultActionsUrl" value="/resources/js/efpcomparator/ResultActions.js" />
        <spring:url var="treeUrl" value="/resources/js/efpcomparator/tree/jquery.treeview.js" />
        <spring:url var="qtipUrl" value="/resources/js/jquery.qtip.min.js" />
        <spring:url var="spinUrl" value="/resources/js/spin.js" />
        <spring:url var="loaderUrl" value="/resources/js/loader.js" />
        
		<script type="text/javascript" src="${jqueryUrl}"></script>
        <script type="text/javascript" src="${jqueryFormUrl}"></script>
		<script type="text/javascript" src="${jqueryCookieUrl}"></script>
        <script type="text/javascript" src="${cookieHandlerUrl}"></script>
        
		<script type="text/javascript" src="${wizardUrl}"></script>
        <script type="text/javascript" src="${wizardManagerUrl}"></script>
        <script type="text/javascript" src="${uploadBundleEventsUrl}"></script>
        <script type="text/javascript" src="${uploadBundleActionsUrl}"></script>
        <script type="text/javascript" src="${chooseLREventsUrl}"></script>
        <script type="text/javascript" src="${chooseLRActionsUrl}"></script>
        <script type="text/javascript" src="${resultEventsUrl}"></script>
        <script type="text/javascript" src="${resultActionsUrl}"></script>
        <script type="text/javascript" src="${treeUrl}"></script>
        <script type="text/javascript" src="${qtipUrl}"></script>
        <script type="text/javascript" src="${spinUrl}"></script>
        <script type="text/javascript" src="${loaderUrl}"></script>
	</head>
	<body>
        <div id="loader"><div id="spinLoader"><p>Loading...</p></div></div>
		<div id="envelope">
			<tiles:insertAttribute name="header" ignore="true" />
			<div id="middle">
				<tiles:insertAttribute name="menu" ignore="true" />
				<section id="content">
					<tiles:insertAttribute name="body" />
				</section>
				<tiles:insertAttribute name="footer" ignore="true" />
			</div>
		</div>
	</body>
</html>
