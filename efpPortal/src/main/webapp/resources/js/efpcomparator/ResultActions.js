/*
 * Calls functions, which registers all events in step Results.
 */
function resultsAction(){
    console.debug("ENTRY resultsAction");
    chooseActionByClickEvent();
    iconsUrl = $("#iconsUrl").attr("data-iconsUrl");
    console.debug("EXIT resultsAction");
}
/*
 * Shows relevant result for given level of print to right tree.
 * param target - level of print
 */
function showResultsAction(target){
    console.debug("ENTRY showResultsAction");
    
    var results = makeBundleList(target);
    $("#rightTree").html(results);
    
    console.debug("EXIT showResultsAction");
}

/*
 * Make string with list of bundle.
 * param target - level of print
 * return string with list of bundles
 */
function makeBundleList(target){
    console.debug("ENTRY makeBundleList");
    var bundleList = "";
    var neighbours = rightColumn[target["bundle"]];
    if (neighbours == undefined){
        return bundleList;
    }
    
    for (var key in neighbours){
        var bundleData = neighbours[key];
        var featureMap = bundleData.featureMap;
        
        
        if (featureMap[target["feature"]] != undefined){
            bundleList += "<li><img title='Bundle' src='" + iconsUrl + "BUNDLE.png'/><span class='cancelClickEvent overrideTree'> " + key + "</span>";
            bundleList += makeFeature(featureMap, target);
            bundleList += "</li>";
            break;
        } else if (target["feature"] == undefined){
            bundleList += "<li><img title='Bundle' src='" + iconsUrl + "BUNDLE.png'/><span class='cancelClickEvent overrideTree'> " + key + "</span>";
            bundleList += makeFeature(featureMap, target);
            bundleList += "</li>";
        }
    }
    console.debug("EXIT makeBundleList");
    return bundleList;
}
/*
 * Make string with list of feature for given bundle.
 * param featureMap - map of feature
 * param target - level of print
 * return string with list of Features
 */
function makeFeature(featureMap, target){
    console.debug("ENTRY makeFeature");
    var feature = "";
    var featureData = featureMap[target["feature"]];
    
    if (target["feature"] == undefined){
        feature += "<ul>";
        for (var key in featureMap){
            featureData = featureMap[key];
            feature += "<li><img title='Feature' src='" + iconsUrl + "FEATURE.png'/><img title='" + featureData.requiredOrProvided + "' src='" + iconsUrl + featureData.requiredOrProvided + ".png'/><img title='" + featureData.type + "' src='" + iconsUrl + featureData.type + ".png'/><span class='cancelClickEvent overrideTree' data-clickEvent='feature' data-title='" + featureData.fullName + "'> " + featureData.name + ", " + featureData.version + "</span>";
            feature += makeEfp(featureData.efpMap, target);
            feature += "</li>";
        }
        feature += "</ul>";
    } else if (featureData != undefined){
        feature += "<ul>";
        feature += "<li><img title='Feature' src='" + iconsUrl + "FEATURE.png'/><img title='" + featureData.requiredOrProvided + "' src='" + iconsUrl + featureData.requiredOrProvided + ".png'/><img title='" + featureData.type + "' src='" + iconsUrl + featureData.type + ".png'/><span class='cancelClickEvent overrideTree' data-clickEvent='feature' data-title='" + featureData.fullName + "'> " + featureData.name + ", " + featureData.version + "</span>";
        feature += makeEfp(featureData.efpMap, target);
        feature += "</li>";
        feature += "</ul>";
    }
    console.debug("EXIT makeFeature");
    return feature;
}

/*
 * Make string with list of EFP for given feature.
 * param efpMap - map of EFP
 * param target - level of print
 * return string with list of EFPs
 */
function makeEfp(efpMap, target){
    console.debug("ENTRY makeEfp");
    var efpList = "";
    var efpData = efpMap[target["efp"]];
    
    if (target["efp"] == undefined && !isEmpty(efpMap)){
        efpList += "<ul>";
        for (var key in efpMap){
            efpData = efpMap[key];
            efpList += "<li><img title='EFP' src='" + iconsUrl + "EFP.png'/><img title='" + efpData.type + "' src='" + iconsUrl + efpData.type + ".png'/><span class='cancelClickEvent'> " + efpData.name + " <strong>value:</strong>" + efpData.value + " </span><img title='" + efpData.result + "' src='" + iconsUrl + efpData.result + ".png'/></li>";
        }
        efpList += "</ul>";
    } else if (efpData != undefined){
        efpList += "<ul>"; 
        efpList += "<li><img title='EFP' src='" + iconsUrl + "EFP.png'/><img title='" + efpData.type + "' src='" + iconsUrl + efpData.type + ".png'/><span class='cancelClickEvent'> " + efpData.name + " <strong>value:</strong>" + efpData.value + " </span><img title='" + efpData.result + "' src='" + iconsUrl + efpData.result + ".png'/></li>";
        efpList += "</ul>";
    }
    
    console.debug("EXIT makeEfp");
    return efpList;
}

/*
 * Creates tree from left list of results.
 */
function leftTreeBootstrap(){
    $("#leftTree").treeview({
        animated: "fast",
        collapsed: true
    });
    setTooltip("#leftTree span[data-clickEvent='feature']");
}
/*
 * Sets tooltips on features in trees of results.
 */
function setTooltip(selectorString){
    $(selectorString).each(function(){
        $(this).qtip({
            show: {
                solo: true,
                delay: 500
            },
            content: $(this).attr("data-title"),
            position: {
                my: "left center",
                at: "right center"
            },
            style: {
                corner: {
                    tip: "left center"
                },
                classes: "ui-tooltip-tipsy ui-tooltip-rounded ui-tooltip-shadow"
            }
        });
    });
}

/*
 * Creates tree from right list of results.
 */
function rightTreeBootstrap(){
    $("#rightTree").treeview({
        animated: "fast",
        collapsed: false
    });
    setTooltip("#rightTree span[data-clickEvent='feature']");
}

/*
 * Tests if Object is empty.
 * return true if Object is empty, otherwise false.
 */
function isEmpty(obj) {
    var p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            return false;
        }
    }
    return true;
};