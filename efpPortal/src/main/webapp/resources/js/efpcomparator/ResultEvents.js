/*
 * Registers event to show bundle neighbours.
 */
function chooseActionByClickEvent(){
    console.debug("ENTRY chooseActionByClickEvent");
    $(".cancelClickEvent").unbind("click");
    $(".bundle").click(function(event){
        var target = event.target;
        if($(target).attr("data-parent") == "efp"){
            target = $(target).parent().get(0);
        }
        if ($(target).hasClass("clickable")){
            $(".clickable").removeAttr("style");
            $(target).css("background-color","#6BCE69");
        }
        var clicked = target.getAttribute("data-clickEvent");
        console.debug("click: " + clicked);
        switch(clicked){
            case "bundle":
                showResultsAction(getArrayBundleName(target));
                break;
            case "feature":
                showResultsAction(getArrayFeatureName(target));
                break;
            case "efp":
                showResultsAction(getArrayEfpName(target));
                break;
            default:
                return;
        }
        rightTreeBootstrap();
        $(".cancelClickEvent").unbind("click");
    });
    console.debug("EXIT chooseActionByClickEvent");
}
/**
 * Gets array with bundle name.
 */
function getArrayBundleName(object){
    return {
        "bundle": object.getAttribute("data-bundleName")
    };
}
/**
 * Gets array with feature name and bundle name.
 */
function getArrayFeatureName(object){
    return {
        "bundle": object.getAttribute("data-bundleName"), 
        "feature": object.getAttribute("data-featureName")
    };
}
/**
 * Gets array with efp name and all names parents.
 */
function getArrayEfpName(object){
    return {
        "bundle": object.getAttribute("data-bundleName"), 
        "feature": object.getAttribute("data-featureName"),
        "efp": object.getAttribute("data-efpName")
    };
}
