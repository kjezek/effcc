/*
 * Registers event to highlight bundles which corresponds to selected LR id.
 */
function emphasizeBundlesEvent(){
    console.debug("ENTRY emphasizeBundlesEvent");
    console.info($("#localRegistryID").val());
    $("#localRegistryID").change(function(event){
        var lrID = event.currentTarget.value;
        console.debug("change localRegistryID");
        emphasizeBundlesAction(lrID);
    });
    console.debug("EXIT emphasizeBundlesEvent");
}