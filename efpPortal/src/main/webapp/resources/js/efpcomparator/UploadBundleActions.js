
/*
 * Calls functions, which registers all events in step UploadBundle.
 */
function uploadBundleAction(){
    console.debug("ENTRY uploadBundleAction");
    deleteBundleEvent();
    deleteAllBundlesEvent();
    uploadBundleEvent();
    console.debug("EXIT uploadBundleAction");
}

/*
 * Sends request on the server demanding delete of all bundles.
 */
function deleteAllBundlesAction(url) {
    console.debug("ENTRY deleteAllBundlesAction");

    $.post(url)
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        uploadBundleAction();
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT deleteAllBundlesAction");
}

/*
 * Sends request on the server demanding delete of bundle 
 * specified by bundle parameter.
 */
function deleteBundleAction(url, bundle) {
    console.debug("ENTRY deleteBundleAction");

    $.post(url,{
        delete_name: bundle
    })
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        uploadBundleAction();
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT deleteBundleAction");
}

/*
 * Sends chosen bundles to the server.
 */
function sendBundlesToServerAction() {
    console.debug("ENTRY sendBundlesToServerAction");
    var progressSelector = $("#progress");
    var progressLabel = $("label");
                
    var options = {
        url: $("#upload_url").val(),
        type: "post",
        
        beforeSend: function() {
            console.debug("beforeSend");
            progressSelector.attr("value", "0");
        },
        uploadProgress: function(event, position, total, percentComplete) {
            progressSelector.attr("value", percentComplete);
            progressLabel.html("Uploaded: " + percentComplete + "%");
        },
        error: function() {
            console.debug("files wasn't uploaded");
        },
        success: function(data){
            console.debug("files was uploaded");
            $("#content").html(data);
            WizardManager.setHandlers();
            uploadBundleAction();
        }
    }
    $('#uploadBundleBean').ajaxSubmit(options);
    console.debug("EXIT sendBundlesToServerAction");
    return false;
}

