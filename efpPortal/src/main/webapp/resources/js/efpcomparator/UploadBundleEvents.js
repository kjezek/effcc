
/*
 * Registers event to delete single bundle.
 */
function deleteBundleEvent(){
    console.debug("ENTRY deleteBundleEvent");
    $(".delete_bundle").click(function(event){
        var bundleName = event.target.id;
        var url = "/efpPortal/efpcomparator/delete_bundle_ajax/";
        console.debug("click delete_bundle");
        console.debug("url to delete bundle: " + url + bundleName);
        deleteBundleAction(url, bundleName);
    });
    console.debug("EXIT deleteBundleEvent");
}

/*
 * Registers event to delete all bundles.
 */
function deleteAllBundlesEvent(){
    console.debug("ENTRY deleteAllBundlesEvent");
    $(".delete_all_bundles").click(function(event){
        var url = event.target.id;
        console.debug("click delete_all_bundles");
        console.debug("url to delete all bundles: " + url);
        deleteAllBundlesAction(url);
    });
    console.debug("EXIT deleteAllBundlesEvent");
}

/*
 * Registers event to send users' desired bundles.
 */
function uploadBundleEvent(){
    console.debug("ENTRY uploadBundleEvent");
    $("input[name='upload_bundles_button']").click(function(event){
        event.preventDefault();
        var fileChooser = $("input[type=file]");
        if (fileChooser.val() == ""){
            fileChooser.qtip({
                content: {
                    text: "No bundles were chosen."
                },
                position: {
                    my: "top left",
                    at: "bottom left"
                },
                style: {
                    tip: {
                        corner: "top center"
                    },
                    classes: "ui-tooltip-tipsy ui-tooltip-rounded ui-tooltip-shadow"
                }
            }).qtip("show");
            
            $(window).mousemove(function(){
                setTimeout(function(){
                    fileChooser.qtip("hide");
                    fileChooser.qtip("destroy");
                }, 750);
                $(this).unbind("mousemove");
            });
            return false;
        }
        console.debug("click upload_bundles_button");
        console.debug("EXIT uploadBundleEvent");
        return sendBundlesToServerAction();
    });
    console.debug("EXIT uploadBundleEvent");
}


