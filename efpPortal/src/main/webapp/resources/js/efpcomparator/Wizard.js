
function showChooseFramework(url) {
    console.debug("ENTRY showFramework");
    
    $.post(url)
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        var cookieFramework = getCookie("framework");
        if (cookieFramework != undefined){
            $("option[value='" + cookieFramework + "']").attr('selected',true);
        }
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT showFramework");
}

/*
 * Shows form page of bundle's upload. This form page is related to
 * the step "Upload bundles".
 */
function showUploadBundles(url) {
    console.debug("ENTRY showUploadBundles");
    enableLoader();
    $.post(url)
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        uploadBundleAction();
        disableLoader();
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT showUploadBundles");
}

/*
 * Shows form page of LR choices. This form page is related to
 * the step "Choose LR".
 */
function showChooseLR(url) {
    console.debug("ENTRY showChooseLR");
    enableLoader();
    $.post(url)
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        chooseLRAction();
        setDefaultChainAction();
        
        var cookieLRID = getCookie("localRegister");
        var bundlesRelatedLRID = jsonData[cookieLRID];
        if (cookieLRID != undefined && bundlesRelatedLRID != undefined){
            $("option[value='" + cookieLRID + "']").attr('selected',true);
        }
        disableLoader();
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT showChooseLR");
}

/*
 * Shows form page of results. This form page is related to
 * the step "Results".
 */
function showResults(url) {
    console.debug("ENTRY showResults");
    enableLoader();
    $.post(url)
    .success(function(data) {
        console.debug("success");
        $("#content").html(data);
        WizardManager.setHandlers();
        leftTreeBootstrap();
        resultsAction();
        disableLoader();
    })
    .error(function() {
        console.debug("error");
    });
    console.debug("EXIT showResults");
}

/*
 * Action is called, before step called UploadBundle is showed.
 */
function beforeUploadBundleAction(){
    console.debug("ENTRY beforeUploadBundleAction");

    addCookie("framework", $("select").val());
    
    console.debug("EXIT beforeUploadBundleAction");
}

/*
 * Action is called, before step called Results is showed.
 */
function beforeResultsAction(){
    console.debug("ENTRY beforeUploadBundleAction");

    addCookie("localRegister", $("select").val());
    
    console.debug("EXIT beforeUploadBundleAction");
}
/*
 * After page is loaded, performs event's handlers setting.
 */
$(document).ready(function() {
    console.debug("ENTRY document ready");
    
    WizardManager.setHandlers();
    //function for using JQuery in spin.js
    $.fn.spin = function(opts) {
        this.each(function() {
            var $this = $(this),
            data = $this.data();

            if (data.spinner) {
                data.spinner.stop();
                delete data.spinner;
            }
            if (opts !== false) {
                data.spinner = new Spinner($.extend({
                    color: $this.css('color')
                }, opts)).spin(this);
            }
        });
        return this;
    };
    
    console.debug("EXIT document ready");
});

String.prototype.replaceAll = function(token, newToken, ignoreCase) {
    var str, i = -1, _token;
    if((str = this.toString()) && typeof token === "string") {
        _token = ignoreCase === true? token.toLowerCase() : undefined;
        while((i = (
            _token !== undefined? 
            str.toLowerCase().indexOf(
                _token, 
                i >= 0? i + newToken.length : 0
                ) : str.indexOf(
                token,
                i >= 0? i + newToken.length : 0
                )
            )) !== -1 ) {
            str = str.substring(0, i)
            .concat(newToken)
            .concat(str.substring(i + token.length));
        }
    }
    return str;
};

var enableLogging = false;
/*
 * Enables or disables logging.
 */
if (typeof (console) === 'undefined' || !enableLogging) {
    var console = {}
    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function () { };
}
