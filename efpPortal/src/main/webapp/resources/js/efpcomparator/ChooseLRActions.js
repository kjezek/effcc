/*
 * Calls functions, which registers all events in step ChooseLR.
 */
function chooseLRAction(){
    console.debug("ENTRY chooseLRAction");
    emphasizeBundlesEvent();
    console.debug("EXIT chooseLRAction");
}

/*
 * Highlights bundles which corresponds selected LR id.
 */
function emphasizeBundlesAction(lrID) {
    console.debug("ENTRY emphasizeBundlesAction");

    $(".chain").fadeTo('fast', 0);

    var bundles = jsonData[lrID];

    for (var key in bundles) {
        var bundleSelector = "#" + bundles[key].replaceAll('.','\\.');
        $(bundleSelector).fadeTo('slow', 1);
    }

    console.debug("EXIT emphasizeBundlesAction");
}

/*
 * Sets highlight for default selected bundles.
 */
function setDefaultChainAction() {
    console.debug("ENTRY setDefaultChainAction");

    $(".chain").css("visibility", "hidden");

    var lrID = $("#localRegistryID").val();

	var bundles = jsonData[lrID];

    for (var key in bundles) {
        var bundleSelector = "#" + bundles[key].replaceAll('.','\\.');
        $(bundleSelector).css("visibility", "visible");
    }

    console.debug("EXIT setDefaultChainAction");
}

