/* 
 * WizardManager helps to manage single steps wizard.
 * This class provides methods for moving between the steps.
 */
var WizardManager = {
    step: 0,
    CONST_STEP_NAMES: new Array("Choose framework", "Upload bundles", "Choose LR", "Results"),
    CONST_MIN_LIMIT_STEP: 0,
    /*
     * This method ensures switch to the next 
     * page wizard related to the next step.
     */
    stepNext: function(){
        console.debug("ENTRY WizardManager.stepNext");
        
        if (this.step < this.CONST_STEP_NAMES.length){
            this.step++;
            var url = $("#next_url").val();
            this.callActionByStep();
            this.showCurrentStep(url);
        } else {
            console.error("Step can not be greater than " + this.CONST_STEP_NAMES.length);
        }
        console.debug("EXIT WizardManager.stepNext");
        return false;
    },
    
    /*
     * This method ensures switch to the previous 
     * page wizard related to the previous step.
     */
    stepPrev: function(){
        console.debug("ENTRY WizardManager.stepPrev");
        if (this.step > this.CONST_MIN_LIMIT_STEP){
            this.step--;
            var url = $("#prev_url").val();
            this.showCurrentStep(url);
        } else {
            console.error("Step can not be less than " + this.CONST_MIN_LIMIT_STEP);
        }
        console.debug("EXIT WizardManager.stepPrev");
        return false;
    },
    
    /*
     * Gets name of current step.
     */
    getStep: function(){
        console.debug("ENTRY WizardManager.getStep");
        console.debug("EXIT WizardManager.getStep");
        return this.CONST_STEP_NAMES[this.step];
    },
    
    /*
     * Shows the page related with current value of the global variable step. 
     */
    showCurrentStep: function(url){
        console.debug("ENTRY WizardManager.showCurrentStep");
        console.info("STEP: " + this.step + " " + this.getStep());
        console.info("STEP: " + url);
        switch(this.step) {
            case 0:
                showChooseFramework(url);
                break;
            case 1:
                showUploadBundles(url);
                break;
            case 2:
                showChooseLR(url);
                break;
            case 3:
                showResults(url);
                break;
        }
        console.debug("EXIT WizardManager.showCurrentStep");
    },
    
    /*
     * Calls action according to current step.
     */
    callActionByStep: function(){
        console.debug("ENTRY WizardManager.callActionByStep");
        switch(this.step) {
            case 1:
                beforeUploadBundleAction();
                break;
            case 3:
                beforeResultsAction();
                break;
        }
        
        console.debug("EXIT WizardManager.callActionByStep");
    },
    
    /*
     * Sets handlers events for previous button and next button, 
     * if there are buttons on given html page. 
     */
    setHandlers: function(){
        console.debug("ENTRY WizardManager.setHandlers");
        var that = this;
        var selector = $("input[name='prev_button']");
        
        //register event click for previous button
        if (selector.val() != "undefined"){
            selector.click(function(event){
                event.preventDefault();
                console.debug("click prev_button");
                return that.stepPrev();
                
            });
        }
        selector = $("input[name='next_button']");
        
        //register event click for next button
        if (selector.val() != "undefined"){
            selector.click(function(event){
                event.preventDefault();
                console.debug("click next_button");
                return that.stepNext();
            });
        }
        
        console.debug("EXIT WizardManager.setHandlers");
    }
}
