function addCookie(key, value) {
    $.cookie(key, value);
}

function deleteCookie(key) {
    $.cookie(key, null);
}

function getCookie(key) {
    return $.cookie(key);
}