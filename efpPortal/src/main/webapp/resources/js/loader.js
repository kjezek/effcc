/*
 * Spin loader settings .
 */
var opts = {
    lines: 20, // The number of lines to draw
    length: 16, // The length of each line
    width: 5, // The line thickness
    radius: 40, // The radius of the inner circle
    rotate: 0, // The rotation offset
    color: '#FFFFFF', // #rgb or #rrggbb
    speed: 1.5, // Rounds per second
    trail: 50, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9 // The z-index (defaults to 2000000000)
};

/*
 * Enable spin loader.
 */
function enableLoader(){
    var spinLoader = $('#spinLoader');
    $('#loader').css("display", "block");
    spinLoader.css("display", "block");
    spinLoader.spin(opts);
}

/*
 * Disable spin loader.
 */
function disableLoader(){
    var spinLoader = $('#spinLoader');
    $('#loader').css("display", "none");
    spinLoader.css("display", "none");
    spinLoader.spin(false);
}

