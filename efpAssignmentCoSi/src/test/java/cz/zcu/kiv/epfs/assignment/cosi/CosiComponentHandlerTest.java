package cz.zcu.kiv.epfs.assignment.cosi;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.*;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.cosi.CosiAssignmentImpl;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import org.junit.rules.TestName;

/**
 * Tests of class CoSiComponentHandler.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CosiComponentHandlerTest {
    /** Path to original jar file. */
    private static final String PATH_TO_JAR_ORIGINAL = "./src/test/resources/testJar.jar";

    /** GRs. */
    private static GR gr = new GR(1, "glob register test", "GRT");

    /** LR. */
    private static LR lr = new LR(2, "High Performance", gr, null, "HP");

    /** Simple EFP 1. */
    private static EFP efp1 = new SimpleEFP(1, "memory", EfpNumber.class,
            new Meta("MB", "high", "low"), new UserGamma("gamma"), gr);

    /** Simple local register assignment. */
    private LrSimpleAssignment lrAssignment1 = new LrSimpleAssignment(
            1, efp1, "low", new EfpNumber(15), lr);

    /** Component loader. */
    private static EfpAwareComponentLoader componentLoader;

    /** Path to test jar file in the actual test. */
    private String pathToJarCopyFile = "";

    /** Name of the actual test.*/
    @Rule
    public TestName name = new TestName();

    /**
     * Does testing copy of jar.
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        componentLoader = new CosiAssignmentImpl();
        pathToJarCopyFile = "./" + name.getMethodName() + "JARCOPY.jar";
        FileUtils.copyFile(new File(PATH_TO_JAR_ORIGINAL), new File(pathToJarCopyFile));
    }

    /**
     * Deletes test jar copy and local repository file.
     *
     * @throws IOException Error during delete of the test jar copy.
     */
    @After
    public void close() throws IOException {
        if(!new File(pathToJarCopyFile).delete()) {
            throw new IOException("Can't delete test JAR.");
        }
    }



    /**
     * Reads from component some expected features by names.
     */
    @Test
    public void testOfReadingFeaturesByNames() {
        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);
        List<Feature> features = coSiComponent.getAllFeatures();

        boolean serviceAExist = false;
        boolean serviceBExist = false;
        boolean serviceCExist = false;
        boolean serviceMainServiceExist = false;

        for (Feature feature : features) {
            if (feature.getIdentifier().contains("serviceA")) {
                serviceAExist = true;
            } else if (feature.getIdentifier().contains("pult.frekvence")) {
                serviceBExist = true;
            } else if (feature.getIdentifier().contains("parkoviste.plne")) {
                serviceCExist = true;
            } else if (feature.getIdentifier().contains("BundleExample")) {
                serviceMainServiceExist = true;
            }
        }

        coSiComponent.close();

        Assert.assertTrue(serviceAExist);
        Assert.assertTrue(serviceBExist);
        Assert.assertTrue(serviceCExist);
        Assert.assertTrue(serviceMainServiceExist);
    }


    /**
     * Test of adding EFP assignment with EFP named value to some feature.
     */
    @Test
    public void testAssignNamedValue() {

        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testFeature = features.get(0);

        coSiComponent.assignEFP(testFeature, efp1, new EfpNamedValue(lrAssignment1));
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, lr);
        coSiComponent.close();

        Assert.assertNotNull(value);
        Assert.assertEquals(new EfpNamedValue(lrAssignment1), value);
    }


    /**
     * Test of adding EFP assignment with direct value to some feature.
     */
    @Test
    public void testAssignDirectValue() {
        EfpAssignedValue testDirectVal = new EfpDirectValue(new EfpNumber(189.7));
        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testFeature = features.get(2);
        coSiComponent.assignEFP(testFeature, efp1, testDirectVal);
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, null);
        coSiComponent.close();

        Assert.assertNotNull(value);
        Assert.assertEquals(testDirectVal, value);
    }



    /**
     * Test of adding and removing EFP assignments to some feature.
     */
    @Test
    public void testDeletingEfpAssignments() {
        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testFeature = features.get(0); // getting second feature from component

        // go through all assignments and delete them
        for (LR lr : coSiComponent.getLRs()) {
            EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, lr);

            EfpAssignment assignment = new EfpAssignment(efp1, value, testFeature);
            coSiComponent.removeEFP(
                    assignment.getFeature(), assignment.getEfp(), assignment.getEfpValue());
        }

        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        // verify all assignments are gone
        for (LR lr : coSiComponent.getLRs()) {
            EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, lr);
            Assert.assertNull(value);
        }

        EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, null);
        coSiComponent.close();

        Assert.assertNull(value);
    }

    /**
     * Test of changing assigned value in EFP assignment.
     */
    @Test
    public void testOfChangingEfpAssignmentValue() {
        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testFeature = features.get(1); // getting second feature from component

        coSiComponent.assignEFP(testFeature, efp1, new EfpNamedValue(lrAssignment1));
        EfpAssignedValue value = coSiComponent.getAssignedValue(testFeature, efp1, lr);
        EfpAssignment assignment = new EfpAssignment(efp1, value, testFeature);

        EfpAssignedValue efpValue = new EfpDirectValue(new EfpNumber(2));
        coSiComponent.changeEfpValue(
                assignment.getFeature(), assignment.getEfp(), assignment.getEfpValue(), efpValue);
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue newValue = coSiComponent.getAssignedValue(testFeature, efp1, null);
        coSiComponent.close();

        Assert.assertEquals(efpValue, newValue);
    }


    /**
     * Test of assigning EFP named value which contains LR derived EFP.
     */
    @Test
    public void testAssignNamedValueWithDerivedEfp() {
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(18, "load_time", EfpNumber.class, meta, null, gr);
        EFP efp_2 = new SimpleEFP(19, "response_time", EfpNumber.class, meta, null, gr);
        DerivedEFP efpDerived = new DerivedEFP(17, "derived_efp",
                EfpNumber.class, meta, null, gr, efp_1, efp_2);

        LrAssignment lrSimpleAssingEfp1 =
            new LrSimpleAssignment(1, efp_1, "high", new EfpNumber(17), lr);
        LrAssignment lrSimpleAssingEfp2 =
            new LrSimpleAssignment(2, efp_2, "low", new EfpNumber(357), lr);

        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "load_time_high > response_time_low",
                new EfpNumber(142),
                Arrays.asList(efp_1, efp_2));

        LrDerivedAssignment lrDerivedAssingEfpD = new LrDerivedAssignment(1, efpDerived,
                "high", eval, lr);

        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testFeature = features.get(0);

        EfpNamedValue efpAssignedValue = new EfpNamedValue(lrDerivedAssingEfpD,
                Arrays.asList(new EfpFeature(efp_1, testFeature), new EfpFeature(efp_2, testFeature)),
                lrSimpleAssingEfp1, lrSimpleAssingEfp2);

        coSiComponent.assignEFP(testFeature, efpDerived, efpAssignedValue);
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpNamedValue efpAssignedValueNew = (EfpNamedValue) coSiComponent.getAssignedValue(
                testFeature, efpDerived, lr);
        coSiComponent.close();

        Assert.assertEquals(efpAssignedValue, efpAssignedValueNew);
    }


    /**
     * Adding a EFP assignment with math.formula.
     */
    @Test
    public void testAssignMathFormulaValue() {
        GR gr = new GR(12, "Test GR", "TG");
        LR lr = new LR(17, "Test LR", gr, null, "TL");
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(18, "load_time", EfpNumber.class, meta, null, gr);
        EFP efp_2 = new SimpleEFP(19, "response_time", EfpNumber.class, meta, null, gr);

        LrSimpleAssignment lrSimpleAssingEfp1 =
            new LrSimpleAssignment(1, efp_1, "high", new EfpNumber(17), lr);
        LrSimpleAssignment lrSimpleAssingEfp2 =
            new LrSimpleAssignment(2, efp_2, "low", new EfpNumber(357), lr);

        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testF1 = features.get(0);
        Feature testF2 = features.get(1);
        Feature testF3 = features.get(2);

        coSiComponent.assignEFP(testF1, efp_1, new EfpNamedValue(lrSimpleAssingEfp1));
        coSiComponent.assignEFP(testF2, efp_2, new EfpNamedValue(lrSimpleAssingEfp2));

        EfpFeature efpF1 = new EfpFeature(efp_1, testF1);
        EfpFeature efpF2 = new EfpFeature(efp_2, testF2);

        String formula = testF1.getIdentifier() + "_" + efp_1.getName()
                + " + " + testF2.getIdentifier() + "_" + efp_2.getName();
        EfpFormulaValue efpMathFormula = new EfpFormulaValue(formula, efpF1, efpF2);

        coSiComponent.assignEFP(testF3, efp_2, efpMathFormula);
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpFormulaValue efpMathFormulaNew = (EfpFormulaValue) coSiComponent.getAssignedValue(
                testF3, efp_2, null);
        coSiComponent.close();



        Assert.assertEquals(efpMathFormula, efpMathFormulaNew);

        Assert.assertEquals(new EfpFeature(efp_1, testF1),
                efpMathFormulaNew.getRelatedRequiredSide().get(0));
        Assert.assertEquals(new EfpFeature(efp_2, testF2),
                efpMathFormulaNew.getRelatedRequiredSide().get(1));
    }


    /**
     * Test of working with EFP assignment without value.
     */
    @Test
    public void testAssignAndDeleteEmptyAssignment() {
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(7, "memory_usage", EfpNumber.class, meta, null, gr);

        ComponentEFPModifier coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = coSiComponent.getAllFeatures();
        Feature testF1 = features.get(0);

        coSiComponent.assignEFP(testF1, efp_1, null);
        coSiComponent.close();

        coSiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<EFP> efps = coSiComponent.getEfps(testF1);
        boolean containsEfp1 = false;

        for (EFP efp : efps) {
            if (efp.equals(efp_1)) {
                containsEfp1 = true;
                break;
            }
        }

        EfpAssignedValue efpAssignVal = coSiComponent.getAssignedValue(testF1, efp_1, null);
        Assert.assertTrue(containsEfp1 && (efpAssignVal == null));

        //deleting EFP
        coSiComponent.removeEFP(testF1, efp_1, null);
        efps = coSiComponent.getEfps(testF1);
        containsEfp1 = false;

        for (EFP efp : efps) {
            if (efp.equals(efp_1)) {
                containsEfp1 = true;
                break;
            }
        }

        coSiComponent.close();

        Assert.assertFalse(containsEfp1);
    }
}
