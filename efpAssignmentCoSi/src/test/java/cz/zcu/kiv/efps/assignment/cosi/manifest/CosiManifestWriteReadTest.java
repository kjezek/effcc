package cz.zcu.kiv.efps.assignment.cosi.manifest;

import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import com.sun.corba.se.spi.activation._ServerManagerImplBase;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandlerImpl;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.cosi.manifest.MFConstants.TypeOfCosiFeature;
import cz.zcu.kiv.efps.assignment.extension.manifest.Constants;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Test of classes CoSiManifestDataReader and CoSiManifestDataWriter
 * for manipulation with data in manifest.
 *
 * Date: 14.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CosiManifestWriteReadTest {

    /** Reference to manifest. */
    private static Manifest manifest;

    /** Name of provided service in manifest. */
    private static String provServNameA = "service A";
    /** Name of provided service in manifest. */
    private static String provServNameB = "service B";
    /** Name of bundle. */
    private static String bundleName = "main.service";

    /** Version of bundle. */
    private static String bundleVersion = "1.2.3.4";

    /** Provided services without EFPs in manifest. */
    private static String providedServices = provServNameA + Constants.SEPARATOR_PARAMS
        + MFConstants.VERSION_PARAM + "=1.3.5.4" + Constants.SEPARATOR_CLAUSES + provServNameB
        + Constants.SEPARATOR_PARAMS + MFConstants.VERSION_PARAM + "=1.2.1";


    /** Bundle feature. */
    private static Feature bundleFeature = new BasicFeature(bundleName,
            AssignmentSide.PROVIDED, MFConstants.TypeOfCosiFeature.COMPONENT.toString(), true, null,
            ManifestDataTools.createSimpleVersion(bundleVersion));





    /**
     * Setting of objects manifest and localRepositoryReader.
     */
    @Before
    public void init() {
        manifest = new Manifest();
        Attributes mainAttributtes = manifest.getMainAttributes();
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_NAME), bundleName);
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_VERSION), bundleVersion);
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_EXTRAFUNC), "");
        mainAttributtes.put(new Attributes.Name(MFConstants.PROVIDE_SERVICES), providedServices);
    }



    /**
     * Test of reading bundle feature.
     */
    //@Test
    public void testBundleFeature() {
        CoSiManifestDataReader coSiMfReader = new CoSiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));

        Feature feature = coSiMfReader.readBundleFeature();

        Assert.assertEquals(feature, bundleFeature);
    }


    /**
     * Test adding and reading two EFP assignments with lrSimpleAssignments
     * to main feature in manifest.
     */
   //@Test
    public void testAddingEFPsToBundle()  {

        String testEFPsStr = "1.response time = LRASSIGN{23.high}, 13.memory_usage = DIRECT{15}";

        //adding string with EFP assignments to bundle
        CoSiManifestDataWriter coSiMfWriter = new CoSiManifestDataWriter(new ManifestHeaderHandlerImpl(manifest));
        coSiMfWriter.writeBundleEFP(testEFPsStr);

        //reading string with EFP assignments from the manifest file.
        CoSiManifestDataReader coSiMfReader = new CoSiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        String testEFPsStrNew = coSiMfReader.readBundleEFP();

        Assert.assertEquals(testEFPsStr, testEFPsStrNew);
    }



    /**
     * Test of adding EFP assignment to provide service.
     */
    //@Test
    public void testAddingEFPsToProvideService()  {
        String testEFPsStr = "1.response time = LRASSIGN{23.high}, 13.memory_usage = DIRECT{15}";

        //read first provide feature from provide services from manifest
        CoSiManifestDataReader coSiMfReader = new CoSiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        Feature bundleFeature = coSiMfReader.readBundleFeature();

        List<Feature> providedFeatures = coSiMfReader.readFeaturesFromTargetHeader(
                bundleFeature, MFConstants.PROVIDE_SERVICES,
                TypeOfCosiFeature.INTERFACE, AssignmentSide.PROVIDED);

        Assert.assertFalse(providedFeatures.isEmpty());
        Feature provFeature = providedFeatures.get(0);

        //add EFP assignment to the select provide service
        CoSiManifestDataWriter coSiMfWriter = new CoSiManifestDataWriter(new ManifestHeaderHandlerImpl(manifest));
        coSiMfWriter.writeEfpsOfFeature(provFeature, testEFPsStr);

        //reading added EFP assignment from the manifest
        coSiMfReader = new CoSiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        String testEFPsStrNew = coSiMfReader.readEFPsFromFeature(provFeature);

        Assert.assertEquals(testEFPsStr, testEFPsStrNew);
    }


    /**
     * Test of creating features from required attributes and trying set EFPs to one feature.
     */
    @Test
    public void testOfAddingEFPsToRequireAttributes() {
        String featureName = "foo.featureType";
        String efps = "1.response_time=LRASSING{2.low}";

        Manifest manifest = new Manifest();
        Attributes mainAttributtes = manifest.getMainAttributes();

        Feature feature = new BasicFeature(featureName, AssignmentSide.REQUIRED,
                TypeOfCosiFeature.TYPE.toString(), false,
                ManifestDataTools.createVersionRange(""));

        mainAttributtes.put(new Attributes.Name(MFConstants.REQUIRE_TYPES), featureName
                + Constants.SEPARATOR_PARAMS + "type=java.lang.Integer");

        ManifestHeaderHandler mhh = new ManifestHeaderHandlerImpl(manifest);

        CoSiManifestDataReader mfReader = new CoSiManifestDataReader(mhh);
        List<Feature> features = mfReader.readFeaturesFromTargetHeader(
                null, MFConstants.REQUIRE_TYPES,
                TypeOfCosiFeature.TYPE, AssignmentSide.REQUIRED);

        //expected one feature with the attribute
        boolean featureFound = false;
        if (features.size() == 1 && features.get(0).getIdentifier().equals(
                feature.getIdentifier())) {
            featureFound = true;
        }
        Assert.assertTrue(featureFound);

        //adding some EFPs
        CoSiManifestDataWriter mfWriter = new CoSiManifestDataWriter(mhh);
        mfWriter.writeEfpsOfFeature(features.get(0), efps);

        String newEFPs = mfReader.readEFPsFromFeature(features.get(0));

        Assert.assertEquals(efps, newEFPs);
    }


    /**
     * Test of reading features from provided types which have same name but different version.
     * Trying set EFPs to one feature.
     */
    //@Test
    public void testOfWorkingWithFeaturesDifferentVersion() {
        String featureName = "provideType.A";
        String version1 = "1.2";
        String version2 = "2.4.1.alpha";
        String efps = "1.response_time=LRASSING{2.low}";

        Feature feature1 = new BasicFeature(featureName, AssignmentSide.PROVIDED,
                TypeOfCosiFeature.TYPE.toString(), false,
                ManifestDataTools.createSimpleVersion(version1));

        Feature feature2 = new BasicFeature(featureName, AssignmentSide.PROVIDED,
                TypeOfCosiFeature.TYPE.toString(), false,
                ManifestDataTools.createSimpleVersion(version2));


        //add features into manifest
        Manifest mf = new Manifest();
        new ManifestHeaderHandlerImpl(mf).writeSimpleHeader(MFConstants.PROVIDE_TYPES, featureName
                + Constants.SEPARATOR_PARAMS + MFConstants.VERSION_PARAM + Constants.EQUAL + version1
                + Constants.SEPARATOR_CLAUSES + featureName + Constants.SEPARATOR_PARAMS
                + MFConstants.VERSION_PARAM + Constants.EQUAL + version2);

        //read features from manifest
        ManifestHeaderHandler mhh = new ManifestHeaderHandlerImpl(manifest);

        CoSiManifestDataReader mfReader = new CoSiManifestDataReader(mhh);
        List<Feature> features = mfReader.readFeaturesFromTargetHeader(
                null, MFConstants.PROVIDE_TYPES,
                TypeOfCosiFeature.TYPE, AssignmentSide.PROVIDED);

        //expected two features with same name only different version
        boolean feature1Found = false;
        boolean feature2Found = false;
        if (features.size() == 2) {
            String identFeatureIndex0 = features.get(0).getIdentifier();
            String identFeatureIndex1 = features.get(1).getIdentifier();

            if (feature1.getIdentifier().equals(identFeatureIndex0)
                    || feature1.getIdentifier().equals(identFeatureIndex1)) {
                feature1Found = true;
            }

            if (feature2.getIdentifier().equals(identFeatureIndex0)
                    || feature2.getIdentifier().equals(identFeatureIndex1)) {
                feature2Found = true;
            }
        }

        Assert.assertTrue(feature2Found);
        Assert.assertTrue(feature1Found);

        //adding some EFPs
        CoSiManifestDataWriter mfWriter = new CoSiManifestDataWriter(mhh);
        mfWriter.writeEfpsOfFeature(features.get(0), efps);

        String newEFPs = mfReader.readEFPsFromFeature(features.get(0));

        Assert.assertEquals(efps, newEFPs);
    }
}
