package cz.zcu.kiv.efps.assignment.cosi.manifest;

import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import junit.framework.Assert;
import org.junit.Test;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.Constants;

/**
 * Tests of class ExtraFuncParser.
 * @author Jan Svab
 */
public class ExtraFuncParserTest {
    /** ID of a LR.*/
    private Integer lrId = 137;
    /** ID of a GR.*/
    private Integer grId = 14;
    /** Name of EFP. */
    private String efpName = "response_time";


    /** Name of a value. */
    private String valueName = "high";

    /** String with example of LR assignment in string. */
    private String lrAssignStr = "" + lrId + Constants.SEPARATOR_DATA + valueName;

    /** Example of EFP clause. */
    private String efpClause1 = "" + grId + Constants.SEPARATOR_DATA + efpName + "=named{"
            + lrId + Constants.SEPARATOR_DATA + valueName + "}";
    /** Example of EFP clause. */
    private String efpClause2 = "" + grId + Constants.SEPARATOR_DATA
            + "High performance.memory=direct{12}";

    /** Example of EFPs in string. */
    private String extraFuncStr = efpClause1 + MFConstants.SEPARATOR_EFPS + efpClause2;



    /**
     * Test of parsing EFP-clauses from string.
     */
    @Test
    public void testOfParsingEFPClauses() {
        ExtraFuncParser efpParser = new ExtraFuncParser(extraFuncStr);

        String efpClause1New = efpParser.readNextEFPClause();
        String efpClause2New = efpParser.readNextEFPClause();

        Assert.assertEquals(efpClause1, efpClause1New);
        Assert.assertEquals(efpClause2, efpClause2New);
    }


    /**
     * Test of parsing string with EFP assignment informations.
     * @throws MFParseException Error during parsing.
     */
    @Test
    public void testOfParsingEFPAssignData()  {
        ExtraFuncParser efpParser = new ExtraFuncParser(efpClause1);
        EfpLink efpAssignData = efpParser.parseEFPAssign();

        Assert.assertEquals(EfpAssignedValue.AssignmentType.named,
                efpAssignData.getTypeOfAssignedValue());
        Assert.assertEquals(grId, efpAssignData.getGrId());
        Assert.assertEquals(efpName, efpAssignData.getEfpName());
        Assert.assertEquals(lrId, efpAssignData.getLrId());
        Assert.assertEquals(valueName, efpAssignData.getValueName());
        Assert.assertEquals(null, efpAssignData.getParamObjectId());
    }


    /**
     * Test of parsing EFPs in string.
     * @throws MFParseException Error during parsing.
     */
    @Test
    public void testOfParsing()  {
        ExtraFuncParser efParser = new ExtraFuncParser(extraFuncStr);
        String extraFuncStrNew = "";

        while (efParser.hasEfpAssign()) {
            extraFuncStrNew += efParser.parseEFPAssign().toString() + MFConstants.SEPARATOR_EFPS;
        }

        extraFuncStrNew = extraFuncStrNew.substring(
                0, extraFuncStrNew.length() - 1);

        Assert.assertEquals(extraFuncStr, extraFuncStrNew);
    }


    /**
     * Test of getting lr ID from string with lr assignment.
     * @throws MFParseException Error during parsing integer value.
     */
    @Test
    public void testOfgettingLrIdFromString()  {
        Integer lrIdNew = ExtraFuncParser.getLrIdFromEfpNamedValue(lrAssignStr);

        Assert.assertEquals(lrId, lrIdNew);
    }

    /**
     * Test of getting name of value from string with lr-assignment.
     */
    @Test
    public void testOfGettingValueNameFromString() {
        String valueNameNew = ExtraFuncParser.getValueNameFromEfpNamedValue(lrAssignStr);

        Assert.assertEquals(valueName, valueNameNew);
    }
}
