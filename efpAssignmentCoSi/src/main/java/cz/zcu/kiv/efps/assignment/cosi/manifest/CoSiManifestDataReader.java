package cz.zcu.kiv.efps.assignment.cosi.manifest;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.cosi.CoSiAssignmentRTException;
import cz.zcu.kiv.efps.assignment.cosi.manifest.MFConstants.TypeOfCosiFeature;
import cz.zcu.kiv.efps.assignment.extension.manifest.FeatureParameters;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandlerImpl;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.types.FeatureVersion;

/**
 * Class for reading features and EFP assignments from manifest.
 *
 * Date: 14.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CoSiManifestDataReader {
    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(CoSiManifestDataReader.class);

    /** Handler for getting parsed data from manifest. */
    private ManifestHeaderHandler manifestHandler;



    /**
     * Constructor for setting of the reader.
     * @param mfHandler Reference to object for working with manifest.
     */
    public CoSiManifestDataReader(final ManifestHeaderHandler mfHandler) {

        if (mfHandler == null) {
            throw new CoSiAssignmentRTException("Parameter manifest is null.");
        }

        this.manifestHandler = mfHandler;
    }


    /**
     * Reads bundle feature.
     * @return Bundle feature.
     */
    public Feature readBundleFeature() {
            String featureName = manifestHandler.readMainAttribute(MFConstants.BUNDLE_NAME);
            String versionStr = manifestHandler.readMainAttribute(MFConstants.BUNDLE_VERSION);

            if (versionStr == null) {
                versionStr = "0.0.0";
            }

            return new BasicFeature(featureName,
                    AssignmentSide.PROVIDED,
                    TypeOfCosiFeature.COMPONENT.toString(),
                    true,
                    null,
                    ManifestDataTools.createSimpleVersion(versionStr));

    }


    /**
     * Reads EFP assignments of bundle.
     * @return List of EFP assignments
     */
    public String readBundleEFP() {

        String extraFunc = manifestHandler.readMainAttribute(MFConstants.BUNDLE_EXTRAFUNC);

        return extraFunc;
    }


    /**
     * Reads features and their EFP assignments from target header in the manifest.
     * @param bundleFeature Bundle feature
     * @param targetHeader Target header in the manifest
     * @param representType Represent element
     * @param side Specifies type of feature
     * @return Map with provide services and their EFP assignments where KEY is feature,
     * which represents service, and VALUE is list of EFP assignments.
     */
    public List<Feature> readFeaturesFromTargetHeader(final Feature bundleFeature,
            final String targetHeader, final TypeOfCosiFeature representType,
            final AssignmentSide side) {

        List<Feature> features = new ArrayList<Feature>();
        List<FeatureParameters> featuresFromMF = manifestHandler.readFeatureHeader(targetHeader);

        for (FeatureParameters featureWithParam : featuresFromMF) {
            String featureName = featureWithParam.getFeatureName();
            Feature feature = null;
            FeatureVersion featureVersion = null;

            if (side == AssignmentSide.PROVIDED) {
                String versionStr = featureWithParam.getFeatureAttributes().get(
                        MFConstants.VERSION_PARAM);
                featureVersion = ManifestDataTools.createSimpleVersion(versionStr);

            } else if (side == AssignmentSide.REQUIRED) {
                String versionRangeStr = ManifestDataTools.removeBorderMarks(
                        featureWithParam.getFeatureAttributes().get(
                                MFConstants.VERSION_RANGE_PARAM));

                featureVersion = ManifestDataTools.createVersionRange(versionRangeStr);

            } else {
                throw new RuntimeException("Unknown type of the side.");
            }


            if (representType == TypeOfCosiFeature.ATTRIBUTE || representType == TypeOfCosiFeature.EVENT) {
                String typeAttribute = featureWithParam.getFeatureAttributes().get(
                        MFConstants.TYPE_PARAM);

                feature = BasicFeature.CreateFeatureWithDatatype(featureName, side, representType.toString(),
                        true, bundleFeature, featureVersion, typeAttribute);

            } else if (representType == TypeOfCosiFeature.TYPE || representType == TypeOfCosiFeature.INTERFACE) {
                String serviceName = featureWithParam.getFeatureAttributes().get(
                        MFConstants.SERVICE_NAME_PARAM);

                feature = BasicFeature.CreateFeatureWithAdditionalName(featureName, side, representType.toString(),
                        true, bundleFeature, featureVersion, serviceName);

            } else {
                feature = new BasicFeature(featureName, side, representType.toString(), true,
                        bundleFeature, featureVersion);
            }

            features.add(feature);
        }

        return features;
    }


    /**
     * Reads from concrete feature parameter with EFPs.
     * @param feature Target feature
     * @return A string with EFPs of the feature or null if the feature hasn't no EFP.
     */
    public String readEFPsFromFeature(final Feature feature) {
        String targetHeader = "";
        TypeOfCosiFeature typeOfFeature = TypeOfCosiFeature.valueOf(feature.getRepresentElement());

        if (feature.getSide() == AssignmentSide.PROVIDED) {
            if (typeOfFeature == MFConstants.TypeOfCosiFeature.COMPONENT) {
                return readBundleEFP();
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.INTERFACE) {
                targetHeader = MFConstants.PROVIDE_SERVICES;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.ATTRIBUTE) {
                targetHeader = MFConstants.PROVIDE_ATTRIBUTES;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.EVENT) {
                targetHeader = MFConstants.PROVIDE_EVENTS;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.TYPE) {
                targetHeader = MFConstants.PROVIDE_TYPES;
            } else {
                throw new CoSiAssignmentRTException("Unknow type of CoSi feature.");
            }
        } else if (feature.getSide() == AssignmentSide.REQUIRED) {
            if (typeOfFeature == MFConstants.TypeOfCosiFeature.INTERFACE) {
                targetHeader = MFConstants.REQUIRE_SERVICES;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.ATTRIBUTE) {
                targetHeader = MFConstants.REQUIRE_ATTRIBUTES;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.EVENT) {
                targetHeader = MFConstants.REQUIRED_EVENTS;
            } else if (typeOfFeature == MFConstants.TypeOfCosiFeature.TYPE) {
                targetHeader = MFConstants.REQUIRE_TYPES;
            } else {
                throw new CoSiAssignmentRTException("Unknow type of CoSi feature.");
            }
        } else {
            throw new RuntimeException("Unknown type of side in the feature.");
        }

        FeatureParameters targetFeatureFromMF =
            findFeatureWithParams(feature, manifestHandler.readFeatureHeader(targetHeader));

        if (targetFeatureFromMF == null) {
            if (targetHeader.equals(MFConstants.PROVIDE_SERVICES)) {
                targetFeatureFromMF = findFeatureWithParams(feature,
                            manifestHandler.readFeatureHeader(MFConstants.PROVIDE_INTERFACES));
            }

            if (targetHeader.equals(MFConstants.REQUIRE_SERVICES)) {
                targetFeatureFromMF = findFeatureWithParams(feature,
                            manifestHandler.readFeatureHeader(MFConstants.REQUIRE_INTERFACES));
            }
        }

        if (targetFeatureFromMF != null) {
            return ManifestDataTools.removeBorderMarks(
                    targetFeatureFromMF.getFeatureAttributes().get(MFConstants.EXTRAFUNC_PARAM));
        } else {
            throw new CoSiAssignmentRTException("The target feature "
                    + feature.getIdentifier() + " wasn't find in manifest in the header: "
                    + targetHeader);
        }
    }


    /**
     * Finds in list with features and their parameters concrete feature.
     * @param feature Target feature
     * @param listOfFeatureWithParam List of features from manifest
     * @return Found object from the list or null
     */
    public static FeatureParameters findFeatureWithParams(final Feature feature,
            final List<FeatureParameters> listOfFeatureWithParam) {

        if (logger.isDebugEnabled()) {
            logger.debug("Searching feature " + feature.getIdentifier()
                    + " with parameters in list: " + listOfFeatureWithParam);
        }

        String targetFeatureIdent = feature.getIdentifier();

        FeatureParameters targetFeatureParam = null;

        for (FeatureParameters featureParameters : listOfFeatureWithParam) {
            String identifier =
                feature.getRepresentElement() + "." + featureParameters.getFeatureName();

            if (feature.getSide() == AssignmentSide.PROVIDED) {
                identifier += "," + ManifestDataTools.createSimpleVersion(
                        featureParameters.getFeatureAttributes().get(MFConstants.VERSION_PARAM));
            }

            if (targetFeatureIdent.equals(identifier)) {
                targetFeatureParam = featureParameters;
                break;
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + targetFeatureParam);
        }

        return targetFeatureParam;
    }
}
