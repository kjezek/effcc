package cz.zcu.kiv.efps.assignment.cosi.manifest;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.Constants;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;


/**
 * Parses string with extra-function properties.
 *
 * Date: 23.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ExtraFuncParser {

    /** Buffer with efps in string. */
    private StringBuffer extraFuncStr;
    /** Index of nextChar. */
    private int actualIndex;



    /**
     * Default constructor for basic setting of object.
     * @param extraFuncStr String with EFPs
     */
    public ExtraFuncParser(final String extraFuncStr) {
        if (extraFuncStr == null) {
            this.extraFuncStr = new StringBuffer("");
        } else {
            this.extraFuncStr = new StringBuffer(extraFuncStr.trim());
        }

        actualIndex = 0;
    }


    /**
     * Checks length of buffer. If isn't empty, read data for next efpAssignment.
     * @return Returns true if has data for next efpAssignment.
     */
    public boolean hasEfpAssign() {
        return (extraFuncStr.length() > actualIndex);
    }


    /**
     * Parses from extraFuncStr buffer data for next efpAssignment and saves them
     * into variables in current instance.
     * @return Object EfpAssignmentData with data of EFP assignment
     */
    public EfpLink parseEFPAssign() {
        Integer grID = null;
        String efpName;
        EfpAssignedValue.AssignmentType typeOfAssignedVal;

        skipWhiteSpaces();
        String efpClause = readNextEFPClause();

        if (efpClause.equals("")) {
            throw new AssignmentRTException("Empty clause with EFP.");
        }

        StringBuffer efpClauseBfr = new StringBuffer(efpClause.trim());

        try {
            grID = Integer.parseInt(efpClauseBfr.substring(0, efpClauseBfr.indexOf(
                    "" + Constants.SEPARATOR_DATA)).trim());
        } catch (Exception e) {
            throw new AssignmentRTException("Error during parsing GR id: " + e);
        }
        //delete String with GR ID
        efpClauseBfr.delete(0, efpClauseBfr.indexOf("" + Constants.SEPARATOR_DATA) + 1);

        if (efpClauseBfr.indexOf(Constants.EQUAL_EFP) == -1) {
            efpName = efpClauseBfr.toString().trim();
            return new EfpLink(grID, null, efpName);
        }

        efpName = efpClauseBfr.substring(0, efpClauseBfr.indexOf(Constants.EQUAL_EFP)).trim();
        //delete String with efpName
        efpClauseBfr.delete(0, efpClauseBfr.indexOf(Constants.EQUAL_EFP) + 1);

        String typeOfAssignedValStr = efpClauseBfr.substring(0, efpClauseBfr.indexOf("{")).trim();

        //correction for old bundles with EFPs which was assigned
        // before deleting of redundant enum in EfpLink
        if(typeOfAssignedValStr.equals("LRASSIGN")) {
            typeOfAssignedValStr = EfpAssignedValue.AssignmentType.named.toString();
        } else if(typeOfAssignedValStr.equals("DIRECT")) {
            typeOfAssignedValStr = EfpAssignedValue.AssignmentType.direct.toString();
        }  else if(typeOfAssignedValStr.equals("FORMULA")) {
            typeOfAssignedValStr = EfpAssignedValue.AssignmentType.formula.toString();
        }

        typeOfAssignedVal = EfpAssignedValue.AssignmentType.valueOf(typeOfAssignedValStr);

        //delete String with type of assigned value
        efpClauseBfr.delete(0, efpClauseBfr.indexOf("{") + 1);

        if (efpClauseBfr.charAt(efpClauseBfr.length() - 1) != '}') {
            throw new AssignmentRTException("Missing mark '}' in string with EFP assignment.");
        }

        String assignedValue = efpClauseBfr.substring(0, efpClauseBfr.length() - 1).trim();

        if (typeOfAssignedVal == EfpAssignedValue.AssignmentType.direct
                || typeOfAssignedVal == EfpAssignedValue.AssignmentType.formula) {
            Integer id = null;
            try {
                id = Integer.parseInt(assignedValue);
            } catch (Exception e) {
                throw new AssignmentRTException("Error during parsing id of "
                        + "math.formula or direct value: " + e);
            }

            return new EfpLink(grID, null, efpName, typeOfAssignedVal, null, id);

        } else if (typeOfAssignedVal == EfpAssignedValue.AssignmentType.named) {
            return new EfpLink(
                    grID, null, efpName, getLrIdFromEfpNamedValue(assignedValue), null,
                    getValueNameFromEfpNamedValue(assignedValue), null);
        } else {
            throw new AssignmentRTException("Unknown type of assigned value");

        }
    }

    /**
     * Reads from string with EFPs next EFP-clause.
     * @return String with EFP-clause or empty string if wasn't found a EFP assignment
     */
    public String readNextEFPClause() {
        String nextEFPClause = "";
        skipWhiteSpaces();

        for ( ; actualIndex < extraFuncStr.length(); actualIndex++) {
            if (extraFuncStr.charAt(actualIndex) == MFConstants.SEPARATOR_EFPS) {
                actualIndex++;
                break;
            }

            nextEFPClause += extraFuncStr.charAt(actualIndex);
        }

        return nextEFPClause.trim();
    }


    /**
     * Skips from actual position in EFPs string white spaces.
     */
    private void skipWhiteSpaces() {
        for ( ; actualIndex < extraFuncStr.length(); actualIndex++) {
            if (extraFuncStr.charAt(actualIndex) != ' '
                || extraFuncStr.charAt(actualIndex) != '\t') {
                break;
            }
        }
    }


    /**
     * Extracts ID of LR from LR assignment in string.
     * @param efpNamedValueStr String with LR assignment
     * @return ID of LR
     */
    public static int getLrIdFromEfpNamedValue(final String efpNamedValueStr) {
        try {
            return Integer.parseInt(efpNamedValueStr.substring(
                    0, efpNamedValueStr.indexOf(Constants.SEPARATOR_DATA)));
        } catch (Exception e) {
            throw new AssignmentRTException("Error during parsing GR id: " + e);
        }
    }


    /**
     * Extracts value name from LR assignment in string.
     * @param efpNamedValueStr String with LR assignment
     * @return Name of value
     */
    public static String getValueNameFromEfpNamedValue(final String efpNamedValueStr) {
            return efpNamedValueStr.substring(
                    efpNamedValueStr.lastIndexOf(Constants.SEPARATOR_DATA) + 1).trim();
    }
}
