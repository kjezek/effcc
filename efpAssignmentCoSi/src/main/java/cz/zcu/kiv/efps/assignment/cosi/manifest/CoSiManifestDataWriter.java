package cz.zcu.kiv.efps.assignment.cosi.manifest;

import java.util.List;

import cz.zcu.kiv.efps.assignment.cosi.CoSiAssignmentRTException;
import cz.zcu.kiv.efps.assignment.cosi.manifest.MFConstants.TypeOfCosiFeature;
import cz.zcu.kiv.efps.assignment.extension.manifest.FeatureParameters;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Class for writing features and their EFP assignments into manifest.
 *
 * Date: 14.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CoSiManifestDataWriter {
    /** Handler for getting and setting parsed data from manifest. */
    private ManifestHeaderHandler manifestHandler;



    /**
     * Constructor for setting writer.
     * @param mfHandler A object for working with manifest.
     */
    public CoSiManifestDataWriter(final ManifestHeaderHandler mfHandler) {
        if (mfHandler == null) {
            throw new CoSiAssignmentRTException("Parameter manifest is null.");
        }
        manifestHandler = mfHandler;
    }


    /**
     * Writes string with information about EFP assignments of the bundle into manifest.
     * @param efps String with EFPs
     */
    public void writeBundleEFP(final String efps) {
        manifestHandler.writeSimpleHeader(MFConstants.BUNDLE_EXTRAFUNC, efps);
    }


    /**
     * Select manifest header, which contains editing feature, and updates EFPs this feature.
     * @param feature Editing feature
     * @param efps A string with information about EFP assignments
     */
    public void writeEfpsOfFeature(final Feature feature,
            final String efps) {

        TypeOfCosiFeature typeOfFeature = TypeOfCosiFeature.valueOf(feature.getRepresentElement());
        String targetHeader = "";

        if (typeOfFeature == TypeOfCosiFeature.INTERFACE) {
            if (feature.getSide() == AssignmentSide.PROVIDED) {
                targetHeader = MFConstants.PROVIDE_SERVICES;
            } else {
                targetHeader = MFConstants.REQUIRE_SERVICES;
            }

        } else if (typeOfFeature == TypeOfCosiFeature.TYPE) {
            if (feature.getSide() == AssignmentSide.PROVIDED) {
                targetHeader = MFConstants.PROVIDE_TYPES;
            } else {
                targetHeader = MFConstants.REQUIRE_TYPES;
            }

        } else if (typeOfFeature == TypeOfCosiFeature.EVENT) {
            if (feature.getSide() == AssignmentSide.PROVIDED) {
                targetHeader = MFConstants.PROVIDE_EVENTS;
            } else {
                targetHeader = MFConstants.REQUIRED_EVENTS;
            }

        } else if (typeOfFeature == TypeOfCosiFeature.ATTRIBUTE) {
            if (feature.getSide() == AssignmentSide.PROVIDED) {
                targetHeader = MFConstants.PROVIDE_ATTRIBUTES;
            } else {
                targetHeader = MFConstants.REQUIRE_ATTRIBUTES;
            }

        } else if (typeOfFeature == TypeOfCosiFeature.COMPONENT) {
            writeBundleEFP(efps);
            return;
        }

        if (targetHeader.equals("")) {
            throw new CoSiAssignmentRTException("Unknown type of feature.");
        }

        updateFeature(feature, efps, targetHeader);
    }


    /**
     * Updates EFPs of the feature in manifest file.
     * @param feature Editing feature
     * @param efps A string with information about EFP assignments
     * @param targetHeader Name of target header which contains the editing feature
     */
    private void updateFeature(final Feature feature,
            final String efps, final String targetHeader) {
        List<FeatureParameters> featuresParamsMF = null;
            featuresParamsMF = manifestHandler.readFeatureHeader(targetHeader);

        FeatureParameters targetFeatureFromMF =
            CoSiManifestDataReader.findFeatureWithParams(feature, featuresParamsMF);

        if (targetFeatureFromMF == null) {
            //if wasn't found feature and feature represents interface
            //then is searched old-type header.
            if (targetHeader.equals(MFConstants.PROVIDE_SERVICES)) {
                updateFeature(feature, efps, MFConstants.PROVIDE_INTERFACES);
                return;
            } else if (targetHeader.equals(MFConstants.REQUIRE_SERVICES)) {
                updateFeature(feature, efps, MFConstants.REQUIRE_INTERFACES);
                return;
            }

            throw new CoSiAssignmentRTException(
                    "In manifest doesn't exist feature with specified identifier: "
                    + feature.getIdentifier());
        }


        if (efps != null && !efps.equals("")) {
            targetFeatureFromMF.getFeatureAttributes().put(
                    MFConstants.EXTRAFUNC_PARAM, "(" + efps + ")");
        } else {
            targetFeatureFromMF.getFeatureAttributes().remove(MFConstants.EXTRAFUNC_PARAM);
        }

        manifestHandler.writeFeatureHeader(targetHeader, featuresParamsMF);
    }
}
