package cz.zcu.kiv.efps.assignment.cosi.manifest;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.cosi.manifest.MFConstants.TypeOfCosiFeature;
import cz.zcu.kiv.efps.assignment.extension.BundleManifestReader;
import cz.zcu.kiv.efps.assignment.extension.Openable;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Class for manipulation with data in manifest.
 *
 * Date: 22.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CoSiEfpDataManipulator extends AbstractEfpDataManipulator<ManifestHeaderHandler> {

    /** A logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** It is used for opening a manifest. */
    private Openable<ManifestHeaderHandler> manifestOpener;

    /**
     * Default constructor for setting file with manifest.
     * @param efpDataLocation an object which is aware of a location of EFP files
     */
    public CoSiEfpDataManipulator(final AbstractEfpDataLocation efpDataLocation) {
        super(efpDataLocation);
        this.manifestOpener = new BundleManifestReader(efpDataLocation);
    }




    @Override
    public List<Feature> readFeatures() {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read all features from manifest.");
        }

        ManifestHeaderHandler mfHandler = openEfpData();
        List<Feature> featuresList = new ArrayList<Feature>();
        CoSiManifestDataReader mfReader = new CoSiManifestDataReader(mfHandler);

        Feature bundleFeature = mfReader.readBundleFeature();
        featuresList.add(bundleFeature);

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_SERVICES,
                TypeOfCosiFeature.INTERFACE, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_SERVICES,
                TypeOfCosiFeature.INTERFACE, AssignmentSide.REQUIRED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_INTERFACES,
                TypeOfCosiFeature.INTERFACE, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_INTERFACES,
                TypeOfCosiFeature.INTERFACE, AssignmentSide.REQUIRED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_TYPES,
                TypeOfCosiFeature.TYPE, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_TYPES,
                TypeOfCosiFeature.TYPE, AssignmentSide.REQUIRED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_ATTRIBUTES,
                TypeOfCosiFeature.ATTRIBUTE, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_ATTRIBUTES,
                TypeOfCosiFeature.ATTRIBUTE, AssignmentSide.REQUIRED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_EVENTS,
                TypeOfCosiFeature.EVENT, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRED_EVENTS,
                TypeOfCosiFeature.EVENT, AssignmentSide.REQUIRED));


        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + featuresList);
        }

        return featuresList;
    }



    @Override
    public List<EfpLink> readEFPs(final Feature feature) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read data of EFP assignments in feature "
                    + feature.getIdentifier());
        }
        ManifestHeaderHandler mfHandler = openEfpData();
        CoSiManifestDataReader mfReader = new CoSiManifestDataReader(mfHandler);

        List<EfpLink> efpAsssignsData = new ArrayList<EfpLink>();

        String efps = ManifestDataTools.removeBorderMarks(
                mfReader.readEFPsFromFeature(feature));
        ExtraFuncParser extraFuncParser = new ExtraFuncParser(efps);

        while (extraFuncParser.hasEfpAssign()) {
            efpAsssignsData.add(extraFuncParser.parseEFPAssign());
        }


        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + efpAsssignsData);
        }

        return efpAsssignsData;
    }



    @Override
    public void assignEFPs(final Feature feature,
                final List<EfpLink> efpAssignsData) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to write into feature "
                    + feature.getIdentifier() + " data of EFP assignments: " + efpAssignsData);
        }
        ManifestHeaderHandler mfHandler = openEfpData();
        CoSiManifestDataWriter mfWriter = new CoSiManifestDataWriter(mfHandler);

        boolean isFirst = true;
        String efps = "";

        for (EfpLink efpAssignData : efpAssignsData) {
            if (isFirst) {
                isFirst = false;
                efps += efpAssignData.toString();
            } else {
                efps += MFConstants.SEPARATOR_EFPS + efpAssignData.toString();
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Converting list objects with data of EFP assignments into one string: "
                    + efps);
        }

        if (feature.getRepresentElement().equals(TypeOfCosiFeature.COMPONENT.name())) {
            mfWriter.writeBundleEFP(efps);
        } else {
            mfWriter.writeEfpsOfFeature(feature, efps);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("EFPs was updated in feature " + feature.getIdentifier());
        }

    }


	@Override
	public ManifestHeaderHandler open() {
		return manifestOpener.open();
	}


	@Override
	public void close() {
		manifestOpener.close();
	}
}
