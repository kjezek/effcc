package cz.zcu.kiv.efps.assignment.cosi;

/**
 * This is a common runtime exception
 * thrown from the CoSi assignment implementation
 * which is used in common situations concerning
 * error with loading, saving, or modifying CoSi bundle's
 * manifest files.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class CoSiAssignmentRTException extends RuntimeException {

    /** UID. */
    private static final long serialVersionUID = 7835402880618065958L;

    /**
     * Empty constructor.
     */
    public CoSiAssignmentRTException() {
        super();
    }

    /**
     * Constructor with two parameters.
     * @param message String with message
     * @param cause Cause
     */
    public CoSiAssignmentRTException(final String message, final Throwable cause) {
        super(message, cause);
    }


    /**
     * Constructor with message parameter.
     * @param message Message
     */
    public CoSiAssignmentRTException(final String message) {
        super(message);
    }

    /**
     * Constructor with cause parameter.
     * @param cause Cause
     */
    public CoSiAssignmentRTException(final Throwable cause) {
        super(cause);
    }
}
