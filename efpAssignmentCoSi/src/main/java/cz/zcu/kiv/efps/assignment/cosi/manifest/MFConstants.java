package cz.zcu.kiv.efps.assignment.cosi.manifest;

/**
 * Contains names of headers and parameters in the manifest file.
 *
 * Date: 14.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class MFConstants {
    /**
     * Private constructor.
     */
    private MFConstants() { }


    /**
     * Specifies type of feature.
     *
     * Date: 14.2.2011
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    public enum TypeOfCosiFeature {
        /** Bundle feature. */
        COMPONENT,
        /** Service feature. */
        INTERFACE,
        /** Type feature. */
        TYPE,
        /** Attribute feature. */
        ATTRIBUTE,
        /** Event feature. */
        EVENT
    }


    /** Name of the header with provide services. */
    public static final String PROVIDE_SERVICES = "Provide-Services";
    /** Name of the header with require services. */
    public static final String REQUIRE_SERVICES = "Require-Services";

    /** Name of the header with provide services(old name). */
    public static final String PROVIDE_INTERFACES = "Provide-Interfaces";
    /** Name of the header with require services(old name). */
    public static final String REQUIRE_INTERFACES = "Require-Interfaces";

    /** Name of the header with provide events. */
    public static final String PROVIDE_EVENTS = "Generate-Events";
    /** Name of the header with require events. */
    public static final String REQUIRED_EVENTS = "Consume-Events";

    /** Name of the header with provide attributes. */
    public static final String PROVIDE_ATTRIBUTES = "Provide-Attributes";
    /** Name of the header with require attributes. */
    public static final String REQUIRE_ATTRIBUTES = "Require-Attributes";

    /** Name of the header with provide types. */
    public static final String PROVIDE_TYPES = "Provide-Types";
    /** Name of the header with require types. */
    public static final String REQUIRE_TYPES = "Require-Types";

    /** Name of the header with bundle name. */
    public static final String BUNDLE_NAME = "Bundle-Name";
    /** Name of the header with bundle version. */
    public static final String BUNDLE_VERSION = "Bundle-Version";
    /** Name of the header with bundle EFPs. */
    public static final String BUNDLE_EXTRAFUNC = "Bundle-Efp";

    /** Name of the parameter of the feature with the version. */
    public static final String VERSION_PARAM = "version";
    /** Name of the parameter of the feature with the version range. */
    public static final String VERSION_RANGE_PARAM = "versionrange";
    /** Name of the parameter of the feature with EFPs. */
    public static final String EXTRAFUNC_PARAM = "efp";
    /** Name of the parameter of the feature with type. */
    public static final String TYPE_PARAM = "type";
    /** Name of the parameter of the feature with service name. */
    public static final String SERVICE_NAME_PARAM = "name";

    /** Separator between EFPs in one string. */
    public static final char SEPARATOR_EFPS = ',';
}
