package cz.zcu.kiv.efps.assignment.cosi;

import cz.zcu.kiv.efps.assignment.client.plugin.EfpPluginConfiguredLoader;

/**
 * CoSi implementations for access to CoSi component.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 *
 */
public class CosiAssignmentImpl extends EfpPluginConfiguredLoader {

    @Override
    public String getPluginConfigurationFile() {
        return "/plugin.cosi.properties";
    }

}
