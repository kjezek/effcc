package cz.zcu.kiv.efps.assignment.osgi;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandlerImpl;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.osgi.manifest.MFConstants;
import cz.zcu.kiv.efps.assignment.osgi.manifest.OSGiManifestDataReader;
import cz.zcu.kiv.efps.assignment.osgi.manifest.OSGiManifestDataWriter;
import cz.zcu.kiv.efps.assignment.extension.manifest.Constants;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Test of classes OSGiManifestDataReader and OSGiManifestDataWriter
 * for manipulation with data in manifest.
 *
 * Date: 29.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiManifestWriteReadTest {

    /** Reference to manifest. */
    private Manifest manifest;

    /** Name of provided package in manifest. */
    private static String provPackageA = "package A";
    /** Name of provided package in manifest. */
    private static String provPackageB = "package B";
    /** Name of bundle. */
    private static String bundleName = "bundle";

    /** Version of bundle. */
    private static String bundleVersion = "1.2.3.4";

    /** Provided services without EFPs in manifest. */
    private static String provPackages = provPackageA + Constants.SEPARATOR_PARAMS
        + MFConstants.VERSION_PARAM + "=1.3.5.4" + Constants.SEPARATOR_CLAUSES + provPackageB
        + Constants.SEPARATOR_PARAMS + MFConstants.VERSION_PARAM + "=1.2.1";

    /** GRs. */
    private static GR gr = new GR(1, "glob register test", "GRT");

    /** Simple EFP 1. */
    private static EFP efp1 = new SimpleEFP(1, "memory", EfpNumber.class,
            new Meta("high", "low"), new UserGamma("gamma"), gr);

    /** Simple EFP 2. */
    private static EFP efp2 = new SimpleEFP(2, "response_time", EfpNumber.class,
            new Meta("high", "low"), new UserGamma("gamma"), gr);

    /** Bundle feature. */
    private static Feature bundleFeature = new BasicFeature(bundleName,
            AssignmentSide.PROVIDED, MFConstants.TypeOfOSGiFeature.BUNDLE.toString(), true, null,
            ManifestDataTools.createSimpleVersion(bundleVersion));

    /** Header with provided capabilities for test manifest. */
    private String provCapabilities =
            MFConstants.NAMESPACE_EFP +
            ".GR1.LR1;feature-type=\"package\";feature-name=\"example1.service\"; " +
            "efp=\"lang\"; value:String=\"CZ\";valueID=\"low\"," +
            MFConstants.NAMESPACE_EFP +
            ".GR1.LR1;feature-type=\"package\";feature-name=\"example1.service\";" +
            "efp=\"signed\";value:Long=0;valueID=\"value_1\"," +
            MFConstants.NAMESPACE_EFP +
            ".GR2;feature-type=\"package\";feature-name=\"example1.service\";" +
            "efp=\"efp2\";value:Long=10;valueID=DIRECT.10";

    /** Header with required capabilities for test manifest. */
    private String reqCapabilites =
            MFConstants.NAMESPACE_EFP +
            ".GR1.LR1;filter:=\"(&(feature-type=package)" +
            "(feature-name=example2.service)(efp=lang)(value=CZ))\"; valueID=\"low\"," +
            MFConstants.NAMESPACE_EFP +
            ".GR1.LR1;filter:=\"(&(feature-type=package)" +
            "(feature-name=example2.service)(efp=signed)(value=0))\"; valueID=\"value_2\"," +
            MFConstants.NAMESPACE_EFP +
            ".GR2;filter:=\"(&(feature-type=package)" +
            "(feature-name=example2.service)(efp=efp2)(value=10))\";valueID=DIRECT.10";



    /**
     * Setting of objects manifest and localRepositoryReader.
     */
    @Before
    public void init() {
        manifest = new Manifest();
        Attributes mainAttributtes = manifest.getMainAttributes();
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_FEATURE), bundleName);
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_VERSION), bundleVersion);
        mainAttributtes.put(new Attributes.Name(MFConstants.BUNDLE_EXTRAFUNC), "");
        mainAttributtes.put(new Attributes.Name(MFConstants.PROVIDE_PACKAGES), provPackages);
    }


    /**
     * Test of reading bundle feature.
     */
    @Test
    public void testBundleFeature() {
        OSGiManifestDataReader osgiMfReader = new OSGiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        Feature feature = osgiMfReader.readBundleFeature();
        Assert.assertEquals(feature, bundleFeature);

    }

    /**
     * Test of adding and reading Efp link objects.
     */
    @Test
    public void testAddEfpLink() {
        OSGiManifestDataReader osgiMfReader = new OSGiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        OSGiManifestDataWriter osgiMfWriter = new OSGiManifestDataWriter(new ManifestHeaderHandlerImpl(manifest));
        Feature fProv = new BasicFeature("example1.service", AssignmentSide.PROVIDED, "package", true);

        List<EfpLink> efpLinks = new ArrayList<EfpLink>();
        efpLinks.add(new EfpLink(null, efp1.getGr().getIdStr(), efp1.getName(), null, "locR1", "low", new EfpNumber(15)));
        efpLinks.add(new EfpLink(null, efp1.getGr().getIdStr(), efp1.getName(), EfpAssignedValue.AssignmentType.direct, new EfpNumber(15), 150));
        efpLinks.add(new EfpLink(null, efp2.getGr().getIdStr(), efp2.getName(), null, "loc2", "high", new EfpNumber(69)));

        osgiMfWriter.writeEfpsOfFeature(fProv, efpLinks);

        List<EfpLink> efpLinksFromMf = osgiMfReader.readEFPsFromFeature(fProv);

        boolean bEfpLink1 = false;
        boolean bEfpLink2 = false;
        boolean bEfpLink3 = false;

        for(EfpLink efpLink : efpLinksFromMf) {
            if(efpLink.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named
                    && efpLink.getGrIdStr().equals(efp1.getGr().getIdStr())
                    && efpLink.getLrIdStr().equals("locR1") && efpLink.getEfpName().equals(efp1.getName())
                    && efpLink.getValueName().equals("low")) {
                bEfpLink1 = true;
            }

            if(efpLink.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct
                    && efpLink.getGrIdStr().equals(efp1.getGr().getIdStr())
                    && efpLink.getEfpName().equals(efp1.getName())
                    && efpLink.getParamObjectId() == 150) {
                bEfpLink2 = true;
            }

            if(efpLink.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named
                    && efpLink.getGrIdStr().equals(efp2.getGr().getIdStr())
                    && efpLink.getLrIdStr().equals("loc2") && efpLink.getEfpName().equals(efp2.getName())
                    && efpLink.getValueName().equals("high")) {
                bEfpLink3 = true;
            }
        }

        Assert.assertTrue(bEfpLink1);
        Assert.assertTrue(bEfpLink2);
        Assert.assertTrue(bEfpLink3);
    }

    /**
     * Test of reading EFPs from capability headers.
     */
    @Test
    public void testGetEFPs() {
        Attributes mainAttributtes = manifest.getMainAttributes();
        mainAttributtes.put(new Attributes.Name(MFConstants.PROVIDE_CAPABILITY), provCapabilities);
        mainAttributtes.put(new Attributes.Name(MFConstants.REQUIRE_CAPABILITY), reqCapabilites);

        OSGiManifestDataReader osgiMfReader = new OSGiManifestDataReader(new ManifestHeaderHandlerImpl(manifest));
        Feature fProv = new BasicFeature("example1.service", AssignmentSide.PROVIDED, "package", true);
        Feature fReq = new BasicFeature("example2.service", AssignmentSide.REQUIRED, "package", true);

        EfpLink link1Prov = null;
        EfpLink link2Prov = null;
        EfpLink link3Prov = null;

        List<EfpLink> efpLinksProvide = osgiMfReader.readEFPsFromFeature(fProv);

        for(EfpLink el : efpLinksProvide) {
            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named && el.getGrIdStr().equals("GR1")
                    && el.getLrIdStr().equals("LR1") && el.getEfpName().equals("lang")
                    && el.getValueName().equals("low")) {
                link1Prov = el;
            }

            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named
                    && el.getGrIdStr().equals("GR1") && el.getLrIdStr().equals("LR1")
                    && el.getEfpName().equals("signed") && el.getValueName().equals("value_1")) {
                link2Prov = el;
            }

            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct
                    && el.getGrIdStr().equals("GR2") && el.getLrIdStr() == null
                    && el.getEfpName().equals("efp2") && el.getParamObjectId() == 10) {
                link3Prov = el;
            }
        }

        Assert.assertNotNull(link1Prov);
        Assert.assertNotNull(link2Prov);
        Assert.assertNotNull(link3Prov);

        EfpLink link1Req = null;
        EfpLink link2Req = null;
        EfpLink link3Req = null;

        List<EfpLink> efpLinksReq = osgiMfReader.readEFPsFromFeature(fReq);

        for(EfpLink el : efpLinksReq) {
            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named && el.getGrIdStr().equals("GR1")
                    && el.getLrIdStr().equals("LR1") && el.getEfpName().equals("lang")
                    && el.getValueName().equals("low")) {
                link1Req = el;
            }

            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named
                    && el.getGrIdStr().equals("GR1") && el.getLrIdStr().equals("LR1")
                    && el.getEfpName().equals("signed") && el.getValueName().equals("value_2")) {
                link2Req = el;
            }

            if (el.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct
                    && el.getGrIdStr().equals("GR2") && el.getLrIdStr() == null
                    && el.getEfpName().equals("efp2") && el.getParamObjectId() == 10) {
                link3Req = el;
            }
        }

        Assert.assertNotNull(link1Req);
        Assert.assertNotNull(link2Req);
        Assert.assertNotNull(link3Req);
    }
}
