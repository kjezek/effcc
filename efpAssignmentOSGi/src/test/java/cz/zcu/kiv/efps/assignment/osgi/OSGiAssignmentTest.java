package cz.zcu.kiv.efps.assignment.osgi;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.*;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import org.junit.rules.TestName;

/**
 * Tests of EFP assignment for OSGi.
 *
 * Date: 29.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiAssignmentTest {
    /** Path to original jar file. */
    private static final String PATH_TO_JAR_ORIGINAL = "./src/test/resources/example.jar";

    /** GRs. */
    private static GR gr = new GR(1, "glob register test", "GRT");

    /** LR. */
    private static LR lr = new LR(2, "High Performance", gr, null, "HP");

    /** Simple EFP 1. */
    private static EFP efp1 = new SimpleEFP(1, "memory", EfpNumber.class,
            new Meta("MB", "high", "low"), new UserGamma("gamma"), gr);

    /** Simple local register assignment. */
    private LrSimpleAssignment lrAssignment1 = new LrSimpleAssignment(
            1, efp1, "low", new EfpNumber(15), lr);

    /** Component loader. */
    private EfpAwareComponentLoader componentLoader;

    /** Path to test jar file in the actual test. */
    private String pathToJarCopyFile = "";

    /** Name of the actual test.*/
    @Rule
    public TestName name = new TestName();

    /**
     * Does testing copy of jar.
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        componentLoader = new OSGiAssignmentImpl();
        pathToJarCopyFile = "./" + name.getMethodName() + "JARCOPY.jar";
        FileUtils.copyFile(new File(PATH_TO_JAR_ORIGINAL), new File(pathToJarCopyFile));
    }

    /**
     * Deletes test jar copy and local repository file.
     *
     * @throws IOException Error during delete of the test jar copy.
     */
    @After
    public void close() throws IOException {
        if(!new File(pathToJarCopyFile).delete()) {
            throw new IOException("Can't delete test JAR.");
        }
    }


    /**
     * Reads from component some expected features by names.
     */
    @Test
    public void testOfReadingFeaturesByNames() {
        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);
        List<Feature> features = osgiComponent.getAllFeatures();

        boolean packageAExist = false;
        boolean packageBExist = false;
        boolean mainPackageExist = false;

        for (Feature feature : features) {

            if (feature.getIdentifier().equals(
                    "PACKAGE.cz.zcu.kiv.cosi.parkoviste.parkoviste,1.2.0.8alpha")) {
                packageAExist = true;

            } else if (feature.getIdentifier().equals(
                    "PACKAGE.cz.zcu.kiv.cosi.parkoviste.konfigurace")) {
                packageBExist = true;

            } else if (feature.getIdentifier().equals("BUNDLE.ParkovisteOsgiParkoviste,1.0.0.1")) {
                mainPackageExist = true;
            }
        }

        osgiComponent.close();

        Assert.assertTrue(packageAExist);
        Assert.assertTrue(packageBExist);
        Assert.assertTrue(mainPackageExist);
    }


    /**
     * Test of adding EFP assignment with EFP named value to some feature.
     */
    @Test
    public void testAssignNamedValue() {

        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testFeature = features.get(0);

        osgiComponent.assignEFP(testFeature, efp1, new EfpNamedValue(lrAssignment1));
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, lr);
        osgiComponent.close();

        Assert.assertNotNull(value);
        Assert.assertEquals(new EfpNamedValue(lrAssignment1), value);
    }


    /**
     * Test of adding EFP assignment with direct value to some feature.
     */
    @Test
    public void testAssignDirectValue() {
        EfpAssignedValue testDirectVal = new EfpDirectValue(new EfpNumber(189.7));
        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testFeature = features.get(2);
        osgiComponent.assignEFP(testFeature, efp1, testDirectVal);
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, null);
        osgiComponent.close();

        Assert.assertNotNull(value);
        Assert.assertEquals(testDirectVal, value);
    }



    /**
     * Test of adding and removing EFP assignments to some feature.
     */
    @Test
    public void testDeletingEfpAssignments() {
        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testFeature = features.get(0); // getting second feature from component

        // go through all assignments and delete them
        for (LR lr : osgiComponent.getLRs()) {
            EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, lr);

            EfpAssignment assignment = new EfpAssignment(efp1, value, testFeature);
            osgiComponent.removeEFP(
                    assignment.getFeature(), assignment.getEfp(), assignment.getEfpValue());
        }

        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        // verify all assignments are gone
        for (LR lr : osgiComponent.getLRs()) {
            EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, lr);
            Assert.assertNull(value);
        }

        EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, null);
        osgiComponent.close();

        Assert.assertNull(value);
    }

    /**
     * Test of changing assigned value in EFP assignment.
     */
    @Test
    public void testOfChangingEfpAssignmentValue() {
        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testFeature = features.get(1); // getting second feature from component

        osgiComponent.assignEFP(testFeature, efp1, new EfpNamedValue(lrAssignment1));
        EfpAssignedValue value = osgiComponent.getAssignedValue(testFeature, efp1, lr);
        EfpAssignment assignment = new EfpAssignment(efp1, value, testFeature);

        EfpAssignedValue efpValue = new EfpDirectValue(new EfpNumber(2));
        osgiComponent.changeEfpValue(
                assignment.getFeature(), assignment.getEfp(), assignment.getEfpValue(), efpValue);
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpAssignedValue newValue = osgiComponent.getAssignedValue(testFeature, efp1, null);
        osgiComponent.close();

        Assert.assertEquals(efpValue, newValue);
    }


    /**
     * Test of assigning EFP named value which contains LR derived EFP.
     */
    @Test
    public void testAssignNamedValueWithDerivedEfp() {
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(18, "load_time", EfpNumber.class, meta, null, gr);
        EFP efp_2 = new SimpleEFP(19, "response_time", EfpNumber.class, meta, null, gr);
        DerivedEFP efpDerived = new DerivedEFP(17, "derived_efp",
                EfpNumber.class, meta, null, gr, efp_1, efp_2);

        LrAssignment lrSimpleAssingEfp1 =
            new LrSimpleAssignment(1, efp_1, "high", new EfpNumber(17), lr);
        LrAssignment lrSimpleAssingEfp2 =
            new LrSimpleAssignment(2, efp_2, "low", new EfpNumber(357), lr);

        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "load_time_high > response_time_low",
                new EfpNumber(142),
                Arrays.asList(efp_1, efp_2));

        LrDerivedAssignment lrDerivedAssingEfpD = new LrDerivedAssignment(1, efpDerived,
                "high", eval, lr);


        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testFeature = features.get(0);

        EfpNamedValue efpAssignedValue = new EfpNamedValue(lrDerivedAssingEfpD,
                Arrays.asList(new EfpFeature(efp_1, testFeature), new EfpFeature(efp_2, testFeature)),
                lrSimpleAssingEfp1, lrSimpleAssingEfp2);

        osgiComponent.assignEFP(testFeature, efpDerived, efpAssignedValue);
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpNamedValue efpAssignedValueNew = (EfpNamedValue) osgiComponent.getAssignedValue(
                testFeature, efpDerived, lr);
        osgiComponent.close();

        Assert.assertEquals(efpAssignedValue, efpAssignedValueNew);

    }


    /**
     * Adding a EFP assignment with math.formula.
     */
    @Test
    public void testAssignMathFormulaValue() {
        GR gr = new GR(12, "Test GR", "TG");
        LR lr = new LR(17, "Test LR", gr, null, "TL");
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(18, "load_time", EfpNumber.class, meta, null, gr);
        EFP efp_2 = new SimpleEFP(19, "response_time", EfpNumber.class, meta, null, gr);

        LrSimpleAssignment lrSimpleAssingEfp1 =
            new LrSimpleAssignment(1, efp_1, "high", new EfpNumber(17), lr);
        LrSimpleAssignment lrSimpleAssingEfp2 =
            new LrSimpleAssignment(2, efp_2, "low", new EfpNumber(357), lr);

        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testF1 = features.get(0);
        Feature testF2 = features.get(1);
        Feature testF3 = features.get(2);

        osgiComponent.assignEFP(testF1, efp_1, new EfpNamedValue(lrSimpleAssingEfp1));
        osgiComponent.assignEFP(testF2, efp_2, new EfpNamedValue(lrSimpleAssingEfp2));

        EfpFeature efpF1 = new EfpFeature(efp_1, testF1);
        EfpFeature efpF2 = new EfpFeature(efp_2, testF2);

        String formula = testF1.getIdentifier() + "_" + efp_1.getName()
                + " + " + testF2.getIdentifier() + "_" + efp_2.getName();
        EfpFormulaValue efpMathFormula = new EfpFormulaValue(formula, efpF1, efpF2);

        osgiComponent.assignEFP(testF3, efp_2, efpMathFormula);
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        EfpFormulaValue efpMathFormulaNew = (EfpFormulaValue) osgiComponent.getAssignedValue(
                testF3, efp_2, null);
        osgiComponent.close();



        Assert.assertEquals(efpMathFormula, efpMathFormulaNew);

        Assert.assertEquals(new EfpFeature(efp_1, testF1),
                efpMathFormulaNew.getRelatedRequiredSide().get(0));
        Assert.assertEquals(new EfpFeature(efp_2, testF2),
                efpMathFormulaNew.getRelatedRequiredSide().get(1));
    }


    /**
     * Test of working with EFP assignment without value.
     */
    @Test
    public void testAssignAndDeleteEmptyAssignment() {
        Meta meta = new Meta("ms", "high", "low");
        EFP efp_1 = new SimpleEFP(7, "memory_usage", EfpNumber.class, meta, null, gr);

        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<Feature> features = osgiComponent.getAllFeatures();
        Feature testF1 = features.get(0);

        osgiComponent.assignEFP(testF1, efp_1, null);
        osgiComponent.close();

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        List<EFP> efps = osgiComponent.getEfps(testF1);
        boolean containsEfp1 = false;

        for (EFP efp : efps) {
            if (efp.equals(efp_1)) {
                containsEfp1 = true;
                break;
            }
        }

        EfpAssignedValue efpAssignVal = osgiComponent.getAssignedValue(testF1, efp_1, null);
        Assert.assertTrue(containsEfp1 && (efpAssignVal == null));

        //deleting EFP
        osgiComponent.removeEFP(testF1, efp_1, null);
        efps = osgiComponent.getEfps(testF1);
        containsEfp1 = false;

        for (EFP efp : efps) {
            if (efp.equals(efp_1)) {
                containsEfp1 = true;
                break;
            }
        }

        osgiComponent.close();

        Assert.assertFalse(containsEfp1);
    }


    /**
     * Assign and delete EFP value.
     */
    @Test
    public void testAssignAndDeleteValue() {

        Meta meta = new Meta(null);
        EFP efp = new SimpleEFP(5, "memory_usage", EfpNumber.class, meta, null, gr);

        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        try {
            List<Feature> features = osgiComponent.getAllFeatures();
            Feature testF1 = features.get(0);

            // add a value first
            osgiComponent.assignEFP(testF1, efp, new EfpDirectValue(new EfpNumber(100)));

            // then delete it
            osgiComponent.removeEFP(testF1, efp, new EfpDirectValue(new EfpNumber(100)));

            // value should not be assigned
            EfpAssignedValue value = osgiComponent.getAssignedValue(testF1, efp, null);
            Assert.assertNull(value);
        } finally {
            osgiComponent.close();
        }
    }

    /**
     * Clear of all EFP assignments from the component.
     */
    @Test
    public void testClearAllEFPAssignments() {

        Meta meta = new Meta(null);
        EFP efp = new SimpleEFP(5, "memory_usage", EfpNumber.class, meta, null, gr);

        ComponentEFPModifier osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);


        try {
            double val = 110;
            List<Feature> features = osgiComponent.getAllFeatures();
            for(Feature f : features)
            {
                //assigning EFP to all features
                osgiComponent.assignEFP(f, efp, new EfpDirectValue(new EfpNumber(val)));
                val += 10;
            }
        } finally {
            osgiComponent.close();
        }

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);

        try {
            //clear of all EFP assignments
            osgiComponent.clear();
        } finally {
            osgiComponent.close();
        }

        osgiComponent = componentLoader.loadForUpdate(pathToJarCopyFile);
        try {
            List<Feature> features = osgiComponent.getAllFeatures();
            for(Feature f : features)
            {
                // value should not be assigned
                EfpAssignedValue value = osgiComponent.getAssignedValue(f, efp, null);
                Assert.assertNull(value);
            }
        } finally {
            osgiComponent.close();
        }

    }
}
