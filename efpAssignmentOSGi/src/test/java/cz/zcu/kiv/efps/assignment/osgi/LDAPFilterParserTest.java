package cz.zcu.kiv.efps.assignment.osgi;

import cz.zcu.kiv.efps.assignment.osgi.manifest.ldap.*;
import junit.framework.Assert;
import org.junit.Test;
import org.osgi.framework.InvalidSyntaxException;

/**
 * Tests parsing strings with LDAP filter.
 * <p/>
 * Date: 18.3.2014
 *
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class LDAPFilterParserTest {
    /**
     * String identifier for EFP 1.
     */
    private final String efp1 = "GR1.lang";
    /**
     * A value for EFP 1.
     */
    private final String efp1Value = "CZ";
    /**
     * A value for EFP 1.
     */
    private final String efp1Value2 = "EN";
    /**
     * String identifier for EFP 2.
     */
    private final String efp2 = "GR1.signed";
    /**
     * A value for EFP 2.
     */
    private final String efp2value = "1";
    /**
     * String identifier for EFP 3.
     */
    private final String efp3 = "GlobReg.memory_consumation";
    /**
     * A value for EFP 3.
     */
    private final String efp3valueMin = "100";
    /**
     * A value for EFP 3.
     */
    private final String efp3valueMax = "500";

    /**
     * Example 1 of filter.
     */
    private final String filter1 = "(" + efp1 + "=" + efp1Value + ")";
    /**
     * Example 2 of filter.
     */
    private final String filter2 = "(|(" + efp1 + "=" + efp1Value + ")(" + efp1 + " = " + efp1Value2 + "))";
    /**
     * Example 3 of filter.
     */
    private final String filter3 = "(&(" + efp2 + "<=" + efp2value + ")(" + efp3 + ">=" + efp3valueMin
            + ")(" + efp3 + "<=" + efp3valueMax + ")" + filter2 + ")";


    /**
     * Test of parsing simple filter.
     */
    @Test
    public void testParseSimpleFilter() {
        FilterImpl f = null;
        try {
            f = new Parser(filter1).parse();
        } catch (InvalidSyntaxException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        Assert.assertTrue(f.isItem());
        Assert.assertEquals(efp1, f.getAttribute());
        Assert.assertEquals(FilterImpl.EQUAL, f.getOperator());
        Assert.assertEquals(efp1Value, f.getValue());
    }

    /**
     * Test of parsing advanced filter.
     */
    @Test
    public void testParseAdvancedFilter() {
        FilterImpl f = null;
        try {
            f = new Parser(filter2).parse();
        } catch (InvalidSyntaxException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        Assert.assertTrue(!f.isItem());
        Assert.assertEquals(FilterImpl.OR, f.getOperator());
        Assert.assertEquals(2, ((FilterImpl[])f.getValue()).length);

        boolean efp1Value1Item = false;
        boolean efp1Value2Item = false;

        for (int iChild = 0; iChild < ((FilterImpl[])f.getValue()).length; iChild++ ) {
            FilterImpl fChild = ((FilterImpl[])f.getValue())[iChild];
            if (!fChild.isItem()) {
                continue;
            }

            if (fChild.getAttribute().equals(efp1) && fChild.getOperator() == FilterImpl.EQUAL
                    && fChild.getValue().equals(efp1Value)) {
                efp1Value1Item = true;
            }

            if (fChild.getAttribute().equals(efp1) && fChild.getOperator() == FilterImpl.EQUAL
                    && fChild.getValue().equals(efp1Value2)) {
                efp1Value2Item = true;
            }
        }

        Assert.assertTrue(efp1Value1Item);
        Assert.assertTrue(efp1Value2Item);
    }

    /**
     * Test of parsing advanced filter 2.
     */
    @Test
    public void testParseAdvancedFilter2() {
        FilterImpl f = null;
        try {
            f = new Parser(filter3).parse();
        } catch (InvalidSyntaxException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        Assert.assertTrue(!f.isItem());
        Assert.assertEquals(FilterImpl.AND, f.getOperator());
        Assert.assertEquals(4, ((FilterImpl[])f.getValue()).length);

        FilterImpl fiEfp1 = null;
        FilterImpl fiEfp3Min = null;
        FilterImpl fiEfp3Max = null;
        FilterImpl fWithDisjunction = null;

        for (int iChild = 0; iChild < ((FilterImpl[])f.getValue()).length; iChild++ ) {
            FilterImpl fChild = ((FilterImpl[])f.getValue())[iChild];
            if (!fChild.isItem()) {
                fWithDisjunction = fChild;
                continue;
            }

            if (fChild.getAttribute().equals(efp2) && fChild.getOperator() == FilterImpl.LESS
                    && fChild.getValue().equals(efp2value)) {
                fiEfp1 = fChild;
            }

            if (fChild.getAttribute().equals(efp3) && fChild.getOperator() == FilterImpl.GREATER
                    && fChild.getValue().equals(efp3valueMin)) {
                fiEfp3Min = fChild;
            }

            if (fChild.getAttribute().equals(efp3) && fChild.getOperator() == FilterImpl.LESS
                    && fChild.getValue().equals(efp3valueMax)) {
                fiEfp3Max = fChild;
            }
        }

        Assert.assertNotNull(fiEfp1);
        Assert.assertNotNull(fiEfp3Min);
        Assert.assertNotNull(fiEfp3Max);
        Assert.assertNotNull(fWithDisjunction);
        Assert.assertEquals(FilterImpl.OR, fWithDisjunction.getOperator());
        Assert.assertEquals(2, ((FilterImpl[])fWithDisjunction.getValue()).length);

        boolean efp1Value1Item = false;
        boolean efp1Value2Item = false;

        for (int iChild = 0; iChild < ((FilterImpl[])fWithDisjunction.getValue()).length; iChild++ ) {
            FilterImpl fChild = ((FilterImpl[])fWithDisjunction.getValue())[iChild];
            if (!fChild.isItem()) {
                continue;
            }

            if (fChild.getAttribute().equals(efp1) && fChild.getOperator() == FilterImpl.EQUAL
                    && fChild.getValue().equals(efp1Value)) {
                efp1Value1Item = true;
            }

            if (fChild.getAttribute().equals(efp1) && fChild.getOperator() == FilterImpl.EQUAL
                    && fChild.getValue().equals(efp1Value2)) {
                efp1Value2Item = true;
            }
        }

        Assert.assertTrue(efp1Value1Item);
        Assert.assertTrue(efp1Value2Item);
    }
}
