package cz.zcu.kiv.efps.assignment.osgi.manifest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.zcu.kiv.efps.assignment.osgi.manifest.ldap.FilterImpl;

import cz.zcu.kiv.efps.assignment.osgi.manifest.ldap.Parser;
import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import cz.zcu.kiv.efps.assignment.types.*;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import org.osgi.framework.InvalidSyntaxException;

import cz.zcu.kiv.efps.assignment.extension.manifest.FeatureParameters;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.osgi.OSGiAssignmentRTException;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Class for reading features and EFP assignments from manifest.
 * <p/>
 * Date: 22.3.2011
 *
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiManifestDataReader {
    /**
     * A regular expression pattern for getting efp namespace from manifest.
     */
    public static final Pattern RGX_EFP_NAMESPACE = Pattern.compile(
            "^\\s*" + MFConstants.NAMESPACE_EFP
                    + "\\.([a-zA-Z0-9\\-_]+)(\\.([a-zA-Z0-9\\-_]+))?\\s*$");
    /**
     * A regular expression pattern for getting id from string with information about direct value.
     */
    public static final Pattern RGX_EFP_DIRECT_VALUE = Pattern.compile(
            "^\\s*" + MFConstants.DIRECT_VALUE_PREFIX  + "\\.([0-9]+)\\s*$");
    /**
     * A regular expression pattern for getting id from string with information
     * about math.formula value.
     */
    public static final Pattern RGX_EFP_FORMULA_VALUE = Pattern.compile(
            "^\\s*" + MFConstants.FORMULA_VALUE_PREFIX  + "\\.([0-9]+)\\s*$");


    /**
     * Handler for getting parsed data from manifest.
     */
    private ManifestHeaderHandler manifestHandler;

    // TODO [kjezek, B] could be configured
    /**
     * A list of system packages that are ignored.
     */
    private static final List<String> IGNORED_SYSTEM_FEATURES = Arrays.asList(
            "org.osgi.framework",
            "javax");

    /**
     * Constructor for setting of the reader.
     *
     * @param mfHandler Reference to object for working with manifest
     */
    public OSGiManifestDataReader(final ManifestHeaderHandler mfHandler) {

        if (mfHandler == null) {
            throw new OSGiAssignmentRTException("Parameter mfHandler is null.");
        }

        this.manifestHandler = mfHandler;
    }


    /**
     * Reads bundle feature.
     *
     * @return Bundle feature.
     */
    public Feature readBundleFeature() {
        List<FeatureParameters> featuresFromMF =
                manifestHandler.readFeatureHeader(MFConstants.BUNDLE_FEATURE);

        if (featuresFromMF.size() != 1) {
            throw new OSGiAssignmentRTException("Error during reading a bundle feature.");
        }

        String featureName = featuresFromMF.get(0).getFeatureName();
        String versionStr = manifestHandler.readMainAttribute(MFConstants.BUNDLE_VERSION);

        if (versionStr == null) {
            versionStr = "0.0.0";
        }

        return new BasicFeature(featureName,
                AssignmentSide.PROVIDED,
                MFConstants.TypeOfOSGiFeature.BUNDLE.toString(),
                true,
                null,
                ManifestDataTools.createSimpleVersion(versionStr));
    }

    /**
     * Reads features and their EFP assignments from target header in the manifest.
     *
     * @param bundleFeature Bundle feature
     * @param targetHeader  Target header in the manifest
     * @param representType Represent element
     * @param side          Specifies type of feature
     * @return Map with provide services and their EFP assignments where KEY is feature,
     * which represents service, and VALUE is list of EFP assignments.
     */
    public List<Feature> readFeaturesFromTargetHeader(final Feature bundleFeature,
            final String targetHeader, final MFConstants.TypeOfOSGiFeature representType,
            final AssignmentSide side) {

        List<Feature> features = new ArrayList<Feature>();
        List<FeatureParameters> featuresFromMF = manifestHandler.readFeatureHeader(targetHeader);

        for (FeatureParameters featureWithParam : featuresFromMF) {
            //setting basic parameters of new feature
            String featureName = featureWithParam.getFeatureName();
            FeatureVersion featureVersion;
            boolean mandatory = true;

            if (isIgnored(featureName)) {
                continue;
            }

            String versionStr = ManifestDataTools.removeBorderMarks(
                    featureWithParam.getFeatureAttributes().get(
                            MFConstants.VERSION_PARAM));

            if (side == AssignmentSide.PROVIDED) {
                featureVersion = ManifestDataTools.createSimpleVersion(versionStr);

            } else if (side == AssignmentSide.REQUIRED) {
                featureVersion = ManifestDataTools.createVersionRange(versionStr);

                String resolutionParam = featureWithParam.getFeatureAttributes().get(
                        MFConstants.PARAM_MANDATORY);
                if (resolutionParam != null && !resolutionParam.equals("mandatory")) {
                    mandatory = false;
                }
            } else {
                throw new RuntimeException("Unknown type of the side.");
            }

            //creating feature from data
            Feature feature;

            //if feature is package which can specify source bundle
            if (representType == MFConstants.TypeOfOSGiFeature.PACKAGE) {
                BasicFeature parentBundle = (BasicFeature) bundleFeature;

                if (side == AssignmentSide.REQUIRED) {
                    String bundleSourceName = featureWithParam.getFeatureAttributes().get(
                            MFConstants.BUNDLE_SYMB_NAME_PARAM);

                    if (bundleSourceName != null) {
                        String bundleVersionR = ManifestDataTools.removeBorderMarks(
                                featureWithParam.getFeatureAttributes().get(
                                        MFConstants.BUNDLE_VERSION_PARAM));

                        parentBundle = new BasicFeature(bundleSourceName, AssignmentSide.REQUIRED,
                                MFConstants.TypeOfOSGiFeature.BUNDLE.toString(), true, null,
                                ManifestDataTools.createVersionRange(bundleVersionR));
                    } else {
                        parentBundle = null;
                    }
                }

                feature = BasicFeature.CreateFeatureWithReferenceOnFeature(
                        featureName, side, representType.toString(),
                        mandatory, bundleFeature, featureVersion, parentBundle);

            } else {
                feature = new BasicFeature(featureName, side, representType.toString(),
                        mandatory, bundleFeature, featureVersion);
            }

            features.add(feature);
        }

        return features;
    }


    /**
     * This method returns true if a feature with the given
     * name should be ignored. The ignored features
     * are typically those provided by runtime environment
     * that we do not have at hand when running the evaluation.
     *
     * @param featureName a feature name.
     * @return true for an ignored one
     */
    private boolean isIgnored(final String featureName) {

        boolean result = false;

        for (String ignored : IGNORED_SYSTEM_FEATURES) {

            if (featureName.startsWith(ignored)) {
                result = true;

                break;
            }
        }

        return result;
    }


    /**
     * Reads from concrete feature parameter with EFPs.
     *
     * @param feature Target feature
     * @return A string with EFPs of the feature or null if the feature hasn't no EFP.
     */
    public List<EfpLink> readEFPsFromFeature(final Feature feature) {
        List<EfpLink> efpLinks = new ArrayList<EfpLink>();

        String targetHeaderWithCapab = (feature.getSide() == AssignmentSide.PROVIDED)
                ? MFConstants.PROVIDE_CAPABILITY : MFConstants.REQUIRE_CAPABILITY;

        List<FeatureParameters> listCapabilities =
                manifestHandler.readFeatureHeader(targetHeaderWithCapab);

        for (FeatureParameters fp : listCapabilities) {
            if (!fp.getFeatureName().startsWith(MFConstants.NAMESPACE_EFP)) {
                continue; //another namespace
            }
            Matcher matcherEfpId = RGX_EFP_NAMESPACE.matcher(fp.getFeatureName());

            if (!matcherEfpId.find()) {
                throw new AssignmentRTException("Capability ' " + fp.getFeatureName()
                        + "' uses namespace efp, but name doesn't match to pattern "
                        + "(efp.[GR_ID] or efp.[GR_ID].[LR_ID]).");
            }

            String idGrStr = matcherEfpId.group(1);
            String idLrStr = matcherEfpId.group(3);

            if (feature.getSide() == AssignmentSide.PROVIDED) {
                if (!isProvideCapabilityForFeature(feature, fp)) {
                    continue;
                }

                String efpName = ManifestDataTools.removeBorderMarks(
                        fp.getFeatureAttributes().get(MFConstants.CAP_ATTRB_EFP));
                String efpValueID = ManifestDataTools.removeBorderMarks(
                        fp.getFeatureAttributes().get(MFConstants.CAP_ATTRB_VALUE_ID));

                efpLinks.add(reconstructEfpLink(idGrStr, idLrStr, efpName, efpValueID));

            } else if (feature.getSide() == AssignmentSide.REQUIRED) {
                tryReconstructEfpLinkForRequireCapability(feature, efpLinks, fp, idGrStr, idLrStr);
            } else {
                throw new RuntimeException("Unknown type of side in the feature '"
                        + feature.getName() + "'.");
            }
        }

        return efpLinks;
    }

    /**
     * Reconstructs Efp link for require capability based on parsed string from manifest,
     * if assignment belongs to the feature. It is used for require side.
     * @param feature The require feature
     * @param targetList Target list with EFP links
     * @param fp The Parsed string from manifest
     * @param idGrStr The string ID of GR
     * @param idLrStr The string ID of LR
     */
    private void tryReconstructEfpLinkForRequireCapability(final Feature feature,
             final List<EfpLink> targetList, final FeatureParameters fp,
             final String idGrStr, final String idLrStr) {

        if (!fp.getFeatureDirectives().containsKey(MFConstants.FILTER_PARAM)) {
            throw  new AssignmentRTException("Require capability '" + fp.getFeatureName()
                    + "' with EFP assignment has to contain attributes: "
                    + MFConstants.FILTER_PARAM + ".");
        }

        FilterImpl filter;
        try {
            String filterStr = ManifestDataTools.removeBorderMarks(
                    fp.getFeatureDirectives().get(MFConstants.FILTER_PARAM));
            filter = new Parser(filterStr).parse();
        } catch (InvalidSyntaxException e) {
            throw  new AssignmentRTException(e);
        }

        if (filter.getOperator() != FilterImpl.AND) {
            throw  new AssignmentRTException("Capability '" + fp.getFeatureName()
                    + "' with EFP assignment in filter parameter doesn't contain on the first level"
                    + " AND-clause with items: feature-type, feature-name, efp.");
        }

        if (!isFeatureOwnerOfFilter(feature, filter)) {
            return;
        }

        FilterImpl fiFeatureTyp = null;
        FilterImpl fiFeatureName = null;
        FilterImpl fiEfpName = null;

        for (FilterImpl item : (FilterImpl[]) filter.getValue()) {
            if (!item.isItem()) { //looking only in items
                continue;
            }

            if (item.getAttribute().equals(MFConstants.CAP_ATTRB_FEATURE_TYPE)) {
                fiFeatureTyp = item;
            } else if (item.getAttribute().equals(MFConstants.CAP_ATTRB_FEATURE_NAME)) {
                fiFeatureName = item;
            } else if (item.getAttribute().equals(MFConstants.CAP_ATTRB_EFP)) {
                fiEfpName = item;
            }
        }

        if (fiFeatureTyp == null || fiFeatureName == null || fiEfpName == null) {
            throw  new AssignmentRTException("Capability '" + fp.getFeatureName()
                    + "' with EFP assignment in filter parameter doesn't contain on the first level"
                    + " AND-clause with items: feature-type, feature-name, efp.");
        }

        String efpValueID = ManifestDataTools.removeBorderMarks(fp.getFeatureAttributes().get(
                MFConstants.CAP_ATTRB_VALUE_ID));
        targetList.add(reconstructEfpLink(
                idGrStr, idLrStr, fiEfpName.getValue().toString(), efpValueID));
    }

    /**
     * Creates Efp link object.
     * @param idGrStr The string ID of GR
     * @param idLrStr The string ID of LR
     * @param efpName The name of EFP
     * @param efpValueID The detail information about value.
     * @return Efp link
     */
    private EfpLink reconstructEfpLink(final String idGrStr, final String idLrStr,
                                       final String efpName, final String efpValueID) {
        if (idLrStr != null) { //EFP named value
            return new EfpLink(null, idGrStr, efpName, null, idLrStr, efpValueID, null);
        } else if (efpValueID != null) {
            Matcher mDirect = RGX_EFP_DIRECT_VALUE.matcher(efpValueID);
            if (mDirect.find()) {
                return new EfpLink(null, idGrStr, efpName, EfpAssignedValue.AssignmentType.direct,
                        null, Integer.parseInt(mDirect.group(1)));
            }

            Matcher mFormula = RGX_EFP_FORMULA_VALUE.matcher(efpValueID);
            if (mFormula.find()) {
                return new EfpLink(null, idGrStr, efpName, EfpAssignedValue.AssignmentType.formula,
                        null, Integer.parseInt(mFormula.group(1)));
            }
        }

        return new EfpLink(null, idGrStr, efpName); //empty value
    }

    /**
     * Tests if LDAP filter subscribe capability with EFP assignment which belongs to the feature.
     * @param feature The feature
     * @param filter The LDAP filter
     * @return True if filter belongs to the feature, otherwise false.
     */
    public static boolean isFeatureOwnerOfFilter(final Feature feature, final FilterImpl filter) {
        FilterImpl fiFeatureType = null;
        FilterImpl fiFeatureName = null;
        String fiFeatureVersionMin = "[";
        String fiFeatureVersionMax = "]";

        for (FilterImpl item : (FilterImpl[]) filter.getValue()) {
            if (!item.isItem()) {
                //may contain definition of version range
                if (item.getOperator() == FilterImpl.NOT) {
                    FilterImpl[] subItems = (FilterImpl[]) item.getValue();

                    if (subItems.length == 1 && subItems[0].isItem()
                            && subItems[0].getAttribute().equals(
                            MFConstants.CAP_ATTRB_FEATURE_VERSION)) {
                        if (subItems[0].getOperator() == FilterImpl.LESS) {
                            fiFeatureVersionMin = "(" + subItems[0].getValue();
                        } else if (subItems[0].getOperator() == FilterImpl.GREATER) {
                            fiFeatureVersionMax = "" + subItems[0].getValue() + ")";
                        }
                    }
                }

                continue;
            }

            if (item.getAttribute().equals(MFConstants.CAP_ATTRB_FEATURE_TYPE)) {
                fiFeatureType = item;
            } else if (item.getAttribute().equals(MFConstants.CAP_ATTRB_FEATURE_NAME)) {
                fiFeatureName = item;
            } else if (item.getAttribute().equals(MFConstants.CAP_ATTRB_FEATURE_VERSION)) {
                if (item.getOperator() == FilterImpl.LESS) {
                    fiFeatureVersionMax = "" + item.getValue() + "]";
                } else if (item.getOperator() == FilterImpl.GREATER) {
                    fiFeatureVersionMin = "[" + item.getValue();
                }
            }
        }

        FeatureVersionRange fvr = ManifestDataTools.createVersionRange(
                fiFeatureVersionMin + "," + fiFeatureVersionMax);

        return !(fiFeatureType == null || fiFeatureName == null
                || !feature.getName().equals(fiFeatureName.getValue())
                || !feature.getRepresentElement().equals(fiFeatureType.getValue())
                || (feature.getVersion() != null && !feature.getVersion().equals(fvr)));
    }

    /**
     * Tests if feature is owner of provide capability with EFP assignment.
     * @param feature The feature
     * @param provideCap The parsed provide capability
     * @return True if feauter is owner provide capability, otherwise false.
     */
    public static boolean isProvideCapabilityForFeature(
            final Feature feature, final FeatureParameters provideCap) {

        if (!provideCap.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_FEATURE_TYPE)
             || !provideCap.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_FEATURE_NAME)
             || !provideCap.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_EFP)) {

            throw  new AssignmentRTException("Capability '" + provideCap.getFeatureName()
                    + "' with EFP assignment has to contain attributes: "
                    + MFConstants.CAP_ATTRB_FEATURE_TYPE + ", "
                    + MFConstants.CAP_ATTRB_FEATURE_NAME
                    + ", " + MFConstants.CAP_ATTRB_EFP + ", " + MFConstants.CAP_ATTRB_VALUE + ".");
        }

        String featureType = ManifestDataTools.removeBorderMarks(
                provideCap.getFeatureAttributes().get(MFConstants.CAP_ATTRB_FEATURE_TYPE));
        String featureName = ManifestDataTools.removeBorderMarks(
                provideCap.getFeatureAttributes().get(MFConstants.CAP_ATTRB_FEATURE_NAME));

        FeatureVersion featureVersion = SimpleFeatureVersion.IMPLICIT_VERSION;
        if (provideCap.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_FEATURE_VERSION)) {
            String versionStr = ManifestDataTools.removeBorderMarks(
                    provideCap.getFeatureAttributes().get(MFConstants.CAP_ATTRB_FEATURE_VERSION));
            featureVersion = ManifestDataTools.createSimpleVersion(versionStr);
        }

        return !(!feature.getName().equals(featureName)
                || !feature.getRepresentElement().equals(featureType)
                || (feature.getVersion() != null && !feature.getVersion().equals(featureVersion))
                || (feature.getVersion() == null && featureVersion != null
                        && !featureVersion.equals(SimpleFeatureVersion.IMPLICIT_VERSION)));

    }
}
