package cz.zcu.kiv.efps.assignment.osgi;

/**
 * This is a common runtime exception
 * thrown from the OSGi assignment implementation
 * which is used in common situations concerning
 * error with loading, saving, or modifying CoSi bundle's
 * manifest files.
 *
 * Date: 22.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiAssignmentRTException extends RuntimeException {

    /** UID. */
    private static final long serialVersionUID = 7835402880618065958L;

    /**
     * Empty constructor.
     */
    public OSGiAssignmentRTException() {
        super();
    }

    /**
     * Constructor with two parameters.
     * @param message String with message
     * @param cause Cause
     */
    public OSGiAssignmentRTException(final String message, final Throwable cause) {
        super(message, cause);
    }


    /**
     * Constructor with message parameter.
     * @param message Message
     */
    public OSGiAssignmentRTException(final String message) {
        super(message);
    }

    /**
     * Constructor with cause parameter.
     * @param cause Cause
     */
    public OSGiAssignmentRTException(final Throwable cause) {
        super(cause);
    }
}
