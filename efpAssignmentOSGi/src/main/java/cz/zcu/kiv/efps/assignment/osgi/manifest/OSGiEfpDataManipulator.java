package cz.zcu.kiv.efps.assignment.osgi.manifest;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.extension.manifest.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.extension.BundleManifestReader;
import cz.zcu.kiv.efps.assignment.extension.Openable;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
/**
 * EFP data manipulator for OSGi components.
 * Date: 22.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiEfpDataManipulator extends AbstractEfpDataManipulator<ManifestHeaderHandler> {
    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(OSGiEfpDataManipulator.class);

    /** It is used for opening a manifest. */
    private Openable<ManifestHeaderHandler> manifestOpener;



    /**
     * Default constructor for setting file with manifest.
     * @param efpDataLocation an object which is aware of a location of EFP files
     */
    public OSGiEfpDataManipulator(final AbstractEfpDataLocation efpDataLocation) {
        super(efpDataLocation);

        this.manifestOpener = new BundleManifestReader(efpDataLocation);
    }



    @Override
    public List<Feature> readFeatures() {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read all features from manifest.");
        }

        ManifestHeaderHandler mfHandler = openEfpData();
        List<Feature> featuresList = new ArrayList<Feature>();
        OSGiManifestDataReader mfReader = new OSGiManifestDataReader(mfHandler);

        Feature bundleFeature = mfReader.readBundleFeature();
        featuresList.add(bundleFeature);

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.PROVIDE_PACKAGES, MFConstants.TypeOfOSGiFeature.PACKAGE, AssignmentSide.PROVIDED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_PACKAGES, MFConstants.TypeOfOSGiFeature.PACKAGE, AssignmentSide.REQUIRED));

        featuresList.addAll(mfReader.readFeaturesFromTargetHeader(bundleFeature,
                MFConstants.REQUIRE_BUNDLES, MFConstants.TypeOfOSGiFeature.BUNDLE, AssignmentSide.REQUIRED));

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + featuresList);
        }

        return featuresList;
    }

    @Override
    public List<EfpLink> readEFPs(final Feature feature) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read data of EFP assignments in feature "
                    + feature.getIdentifier());
        }
        ManifestHeaderHandler mfHandler = openEfpData();
        OSGiManifestDataReader mfReader = new OSGiManifestDataReader(mfHandler);

        List<EfpLink> efpLinks = mfReader.readEFPsFromFeature(feature);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + efpLinks);
        }

        return efpLinks;
    }

    @Override
    public void assignEFPs(final Feature feature, final List<EfpLink> efpData) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to write into feature "
                    + feature.getIdentifier() + " data of EFP assignments: " + efpData);
        }
        ManifestHeaderHandler mfHandler = openEfpData();
        OSGiManifestDataWriter mfWriter = new OSGiManifestDataWriter(mfHandler);

        if (logger.isDebugEnabled()) {
            logger.debug("Converting list objects with data of EFP assignments "
                    + "into list of capabilities, count = " + efpData.size() + ".");
        }

        mfWriter.writeEfpsOfFeature(feature, efpData);

        if (logger.isDebugEnabled()) {
            logger.debug("EFPs was updated in feature " + feature.getIdentifier());
        }

    }

	@Override
	public ManifestHeaderHandler open() {
		return manifestOpener.open();
	}


	@Override
	public void close() {
		manifestOpener.close();
	}
}
