package cz.zcu.kiv.efps.assignment.osgi.manifest.ldap;

import org.osgi.framework.InvalidSyntaxException;

import java.util.ArrayList;
import java.util.List;

/**
 * Parser class for OSGi filter strings. This class parses the complete
 * filter string and builds a tree of Filter objects rooted at the
 * parent.
 *
 * This parser was obtain from page http://grepcode.com/
 * from class org.osgi.framework.FrameworkUtil in Eclipse-4.3 repository.
 */
public final class Parser {
    /** String for filter. */
    private final String filterstring;
    /** String for filter in char-array. */
    private final char[] filterChars;
     /** A position in string. */
    private int pos;

    /**
     * Default contructor.
     * @param filterstring String for parsing.
     */
    public Parser(final String filterstring) {
        this.filterstring = filterstring;
        filterChars = filterstring.toCharArray();
        pos = 0;
    }

    /**
     * Parses the string.
     * @return Parsed filter in tree from FilterImpl objects.
     * @throws InvalidSyntaxException Throws if filter contains syntax error.
     */
    public FilterImpl parse() throws InvalidSyntaxException {
        FilterImpl filter;
        try {
            filter = parseFilter();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidSyntaxException("Filter ended abruptly",
                    filterstring, e);
        }

        if (pos != filterChars.length) {
            throw new InvalidSyntaxException(
                    "Extraneous trailing characters: "
                            + filterstring.substring(pos), filterstring);
        }
        return filter;
    }

    private FilterImpl parseFilter() throws InvalidSyntaxException {
        FilterImpl filter;
        skipWhiteSpace();

        if (filterChars[pos] != '(') {
            throw new InvalidSyntaxException("Missing '(': "
                    + filterstring.substring(pos), filterstring);
        }

        pos++;

        filter = parseFiltercomp();

        skipWhiteSpace();

        if (filterChars[pos] != ')') {
            throw new InvalidSyntaxException("Missing ')': "
                    + filterstring.substring(pos), filterstring);
        }

        pos++;

        skipWhiteSpace();

        return filter;
    }

    private FilterImpl parseFiltercomp() throws InvalidSyntaxException {
        skipWhiteSpace();

        char c = filterChars[pos];

        switch (c) {
            case '&':
                pos++;
                return parseAnd();

            case '|':
                pos++;
                return parseOr();

            case '!':
                pos++;
                return parseNot();

            default:
        }
        return parseItem();
    }

    private FilterImpl parseAnd() throws InvalidSyntaxException {
        int lookahead = pos;
        skipWhiteSpace();

        if (filterChars[pos] != '(') {
            pos = lookahead - 1;
            return parseItem();
        }

        List<FilterImpl> operands = new ArrayList<FilterImpl>();

        while (filterChars[pos] == '(') {
            FilterImpl child = parseFilter();
            operands.add(child);
        }

        return new FilterImpl(FilterImpl.AND, null, operands
                .toArray(new FilterImpl[operands.size()]));
    }

    private FilterImpl parseOr() throws InvalidSyntaxException {
        int lookahead = pos;
        skipWhiteSpace();

        if (filterChars[pos] != '(') {
            pos = lookahead - 1;
            return parseItem();
        }

        List<FilterImpl> operands = new ArrayList<FilterImpl>();

        while (filterChars[pos] == '(') {
            FilterImpl child = parseFilter();
            operands.add(child);
        }

        return new FilterImpl(FilterImpl.OR, null, operands
                .toArray(new FilterImpl[operands.size()]));
    }

    private FilterImpl parseNot() throws InvalidSyntaxException {
        int lookahead = pos;
        skipWhiteSpace();

        if (filterChars[pos] != '(') {
            pos = lookahead - 1;
            return parseItem();
        }

        FilterImpl child = parseFilter();

        return new FilterImpl(FilterImpl.NOT, null, new FilterImpl[]{child});
    }

    private FilterImpl parseItem() throws InvalidSyntaxException {
        String attr = parseAttr();

        skipWhiteSpace();

        switch (filterChars[pos]) {
            case '~':
                if (filterChars[pos + 1] == '=') {
                    pos += 2;
                    return new FilterImpl(FilterImpl.APPROX, attr,
                            parseValue());
                }
                break;

            case '>':
                if (filterChars[pos + 1] == '=') {
                    pos += 2;
                    return new FilterImpl(FilterImpl.GREATER, attr,
                            parseValue());
                }
                break;

            case '<':
                if (filterChars[pos + 1] == '=') {
                    pos += 2;
                    return new FilterImpl(FilterImpl.LESS, attr,
                            parseValue());
                }
                break;

            case '=':
                if (filterChars[pos + 1] == '*') {
                    int oldpos = pos;
                    pos += 2;
                    skipWhiteSpace();
                    if (filterChars[pos] == ')') {
                        return new FilterImpl(FilterImpl.PRESENT, attr,
                                null);
                    }
                    pos = oldpos;
                }

                pos++;
                Object string = parseSubstring();

                if (string instanceof String) {
                    return new FilterImpl(FilterImpl.EQUAL, attr,
                            string);
                }
                return new FilterImpl(FilterImpl.SUBSTRING, attr,
                        string);

            default:
        }

        throw new InvalidSyntaxException("Invalid operator: "
                + filterstring.substring(pos), filterstring);
    }

    private String parseAttr() throws InvalidSyntaxException {
        skipWhiteSpace();

        int begin = pos;
        int end = pos;

        char c = filterChars[pos];

        while (c != '~' && c != '<' && c != '>' && c != '=' && c != '('
                && c != ')') {
            pos++;

            if (!Character.isWhitespace(c)) {
                end = pos;
            }

            c = filterChars[pos];
        }

        int length = end - begin;

        if (length == 0) {
            throw new InvalidSyntaxException("Missing attr: "
                    + filterstring.substring(pos), filterstring);
        }

        return new String(filterChars, begin, length);
    }

    private String parseValue() throws InvalidSyntaxException {
        StringBuffer sb = new StringBuffer(filterChars.length - pos);

        parseloop:
        while (true) {
            char c = filterChars[pos];

            switch (c) {
                case ')':
                    break parseloop;

                case '(':
                    throw new InvalidSyntaxException("Invalid value: "
                            + filterstring.substring(pos), filterstring);

                case '\\':
                    pos++;
                    c = filterChars[pos];

                /* fall through into default */
                default:
                    sb.append(c);
                    pos++;
                    break;
            }
        }

        if (sb.length() == 0) {
            throw new InvalidSyntaxException("Missing value: "
                    + filterstring.substring(pos), filterstring);
        }

        return sb.toString();
    }

    private Object parseSubstring() throws InvalidSyntaxException {
        StringBuffer sb = new StringBuffer(filterChars.length - pos);

        List<String> operands = new ArrayList<String>();

        parseloop:
        while (true) {
            char c = filterChars[pos];

            switch (c) {
                case ')':
                    if (sb.length() > 0) {
                        operands.add(sb.toString());
                    }
                    break parseloop;

                case '(':
                    throw new InvalidSyntaxException("Invalid value: "
                            + filterstring.substring(pos), filterstring);

                case '*':
                    if (sb.length() > 0) {
                        operands.add(sb.toString());
                    }

                    sb.setLength(0);
                    operands.add(null);
                    pos++;
                    break;

                case '\\':
                    pos++;
                    c = filterChars[pos];

                /* fall through into default */
                default:
                    sb.append(c);
                    pos++;
                    break;
            }
        }

        int size = operands.size();

        if (size == 0) {
            return "";
        }

        if (size == 1) {
            Object single = operands.get(0);

            if (single != null) {
                return single;
            }
        }

        return operands.toArray(new String[size]);
    }

    private void skipWhiteSpace() {
        for (int length = filterChars.length; (pos < length)
                && Character.isWhitespace(filterChars[pos]);) {
            pos++;
        }
    }
}
