package cz.zcu.kiv.efps.assignment.osgi;

import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunctionFactory;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.indianssoft.implementation.BundleChecker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class EmbeddedOSGiMatchingFunction implements MatchingFunctionFactory {


    @Override
    public MatchingFunction createMatchingFunction(final List<String> components) {

        // convert strings to files
        List<File> files = new ArrayList<File>();
        for (String name : components) {
            files.add(new File(name));
        }

        final BundleChecker lib = new BundleChecker(files);

        return new MatchingFunction() {

                @Override
                public boolean matches(final Feature f1, final Feature f2) {

                    Feature required = f1;
                    Feature provided = f2;

                    if (f1.getSide() == f2.getSide()) {
                        return false;
                    }

                    if (f1.getSide() == Feature.AssignmentSide.PROVIDED
                            && f2.getSide() == Feature.AssignmentSide.REQUIRED) {

                        required = f2;
                        provided = f1;
                    }

                    String req = required.getName();
                    String reqVersion = required.getVersion().toString();

                    String cap = provided.getName();
                    String provVersion = provided.getVersion().toString();

                    return lib.isBoundExisting(req, reqVersion, cap, provVersion);
                }
            };
    }
}
