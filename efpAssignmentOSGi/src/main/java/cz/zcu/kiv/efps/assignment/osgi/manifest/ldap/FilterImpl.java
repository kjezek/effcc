package cz.zcu.kiv.efps.assignment.osgi.manifest.ldap;

/**
 * Represent one part of parsed LDAP filter.
 */
public class FilterImpl {
    /** Filter operator for =. */
    public static final int EQUAL = 1;
    /** Filter operator for ~=. */
    public static final int APPROX = 2;
    /** Filter operator for >=. */
    public static final int GREATER = 3;
    /** Filter operator for <=. */
    public static final int LESS = 4;
    /** Filter operator for =*. */
    public static final int PRESENT = 5;
    /** Filter operator for substring. */
    public static final int SUBSTRING = 6;
    /** Filter operator for &. */
    public static final int AND = 7;
    /** Filter operator for |. */
    public static final int OR = 8;
    /** Filter operator for !. */
    public static final int NOT = 9;

    /**
     * Filter operation.
     */
    private int op;
    /**
     * Filter attribute or null if operation AND, OR or NOT.
     */
    private String attr;
    /**
     * Filter operands.
     */
    private Object value = null;

    /** Normalized filter string for Filter object. */
    private transient String filterString;

    /**
     * Default constructor.
     * @param operation The operation.
     * @param attr Name of attribute, if instance is a item.
     * @param value Value of attribute, if instance is a item.
     *              If instance is a node, value contains array of FilterImpl objects.
     */
    FilterImpl(final int operation, final String attr, final Object value) {
        this.op = operation;
        this.attr = (attr != null) ? attr.trim() : "";
        this.value = (value instanceof String) ? value.toString().trim() : value;
        filterString = null;
    }

    /**
     * @return Returns the operation.
     */
    public int getOperator() {
        return op;
    }

    /**
     * @return The name of attribute, if instance is a item.
     */
    public String getAttribute() {
        return attr;
    }

    /**
     * @return Value of attribute, if instance is a item.
     * If instance is a node, value contains array of FilterImpl objects.
     */
    public Object getValue() {
        return value;
    }

    /**
     * Checks if this instance is filter item or node.
     * @return TRUE - this instance is item, FALSE - this instance is node.
     */
    public boolean isItem() {
        return !(value instanceof FilterImpl[]);
    }

    /**
     * Returns this {@code Filter}'s filter string.
     * <p/>
     * The filter string is normalized by removing whitespace which does not
     * affect the meaning of the filter.
     *
     * @return This {@code Filter}'s filter string.
     */
    public String toString() {
        String result = filterString;
        if (result == null) {
            result = normalize().toString();
            filterString = result;
        }
        return result;
    }

    /**
     * Returns this {@code Filter}'s normalized filter string.
     * <p/>
     * The filter string is normalized by removing whitespace which does not
     * affect the meaning of the filter.
     *
     * @return This {@code Filter}'s filter string.
     */
    private StringBuffer normalize() {
        StringBuffer sb = new StringBuffer();
        sb.append('(');

        switch (op) {
            case AND:
                sb.append('&');

                FilterImpl[] filters = (FilterImpl[]) value;
                for (FilterImpl f : filters) {
                    sb.append(f.normalize());
                }

                break;
            case OR:
                sb.append('|');

                FilterImpl[] flt = (FilterImpl[]) value;
                for (FilterImpl f : flt) {
                    sb.append(f.normalize());
                }

                break;
            case NOT:
                sb.append('!');
                FilterImpl filter = (FilterImpl) value;
                sb.append(filter.normalize());

                break;
            case SUBSTRING:
                sb.append(attr);
                sb.append('=');

                String[] substrings = (String[]) value;

                for (String substr : substrings) {
                    if (substr == null) /* * */ {
                        sb.append('*');
                    } else /* xxx */ {
                        sb.append(encodeValue(substr));
                    }
                }

                break;
            case EQUAL:
                sb.append(attr);
                sb.append('=');
                sb.append(encodeValue((String) value));

                break;
            case GREATER:
                sb.append(attr);
                sb.append(">=");
                sb.append(encodeValue((String) value));

                break;
            case LESS:
                sb.append(attr);
                sb.append("<=");
                sb.append(encodeValue((String) value));

                break;
            case APPROX:
                sb.append(attr);
                sb.append("~=");
                sb.append(encodeValue(approxString((String) value)));

                break;

            case PRESENT:
                sb.append(attr);
                sb.append("=*");

                break;
            default:
        }

        sb.append(')');

        return sb;
    }

    /**
     * Encode the value string such that '(', '*', ')' and '\' are escaped.
     *
     * @param value unencoded value string.
     * @return encoded value string.
     */
    private static String encodeValue(final String value) {
        boolean encoded = false;
        int inlen = value.length();
        int outlen = inlen << 1; /* inlen 2 */

        char[] output = new char[outlen];
        value.getChars(0, inlen, output, inlen);

        int cursor = 0;
        for (int i = inlen; i < outlen; i++) {
            char c = output[i];

            switch (c) {
                case '(':
                case '*':
                case ')':
                case '\\':
                    output[cursor] = '\\';
                    cursor++;
                    encoded = true;

                    break;
                default:
            }

            output[cursor] = c;
            cursor++;
        }

        return encoded ? new String(output, 0, cursor) : value;
    }

    /**
     * Map a string for an APPROX (~=) comparison.
     * <p/>
     * This implementation removes white spaces. This is the minimum
     * implementation allowed by the OSGi spec.
     *
     * @param input Input string.
     * @return String ready for APPROX comparison.
     */
    private static String approxString(final String input) {
        boolean changed = false;
        char[] output = input.toCharArray();
        int cursor = 0;
        for (char c : output) {
            if (Character.isWhitespace(c)) {
                changed = true;
                continue;
            }

            output[cursor] = c;
            cursor++;
        }

        return changed ? new String(output, 0, cursor) : input;
    }

}
