package cz.zcu.kiv.efps.assignment.osgi;

import cz.zcu.kiv.efps.assignment.client.plugin.EfpPluginConfiguredLoader;

/**
 * OSGi implementations for access to OSGi component.
 *
 * Date: 22.3.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiAssignmentImpl extends EfpPluginConfiguredLoader {

    @Override
    public String getPluginConfigurationFile() {
        return "/plugin.osgi.properties";
    }

}
