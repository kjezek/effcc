package cz.zcu.kiv.efps.assignment.osgi.manifest;

/**
 * Contains constants for searching data in manifest.
 *
 * Date: 22.3.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class MFConstants {

    /**
     * Private constructor.
     */
    private MFConstants() { }

    /**
     * Specifies type of feature.
     *
     * Date: 14.2.2011
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    public enum TypeOfOSGiFeature {
        /** Bundle feature. */
        BUNDLE,
        /** Package feature. */
        PACKAGE
    }

    /** Name of the header with export packages. */
    public static final String PROVIDE_PACKAGES = "Export-Package";
    /** Name of the header with import packages. */
    public static final String REQUIRE_PACKAGES = "Import-Package";
    /** Name of the header with required bundles. */
    public static final String REQUIRE_BUNDLES = "Require-Bundle";

    /** Name of the header with bundle unique name. */
    public static final String BUNDLE_FEATURE = "Bundle-SymbolicName";
    /** Name of the header with bundle version. */
    public static final String BUNDLE_VERSION = "Bundle-Version";
    /** Name of the header with bundle EFPs. */
    public static final String BUNDLE_EXTRAFUNC = "Bundle-Efp";

    /** Name of the parameter of the feature with the version. */
    public static final String VERSION_PARAM = "version";

    /** Name of the parameter which provides information about mandatory of feature. */
    public static final String PARAM_MANDATORY = "resolution";

    /** Name of the parameter with name of the required bundle. */
    public static final String BUNDLE_SYMB_NAME_PARAM = "bundle-symbolic-name";
    /** Name of the parameter with version range of the required bundle. */
    public static final String BUNDLE_VERSION_PARAM = "bundle-version";

    /** Name of the header with provided capabilities. */
    public static final String PROVIDE_CAPABILITY = "Provide-Capability";
    /** Name of the header with required capabilities. */
    public static final String REQUIRE_CAPABILITY = "Require-Capability";
    /** A name of attribute in required capability with EFP assignments. */
    public static final String FILTER_PARAM = "filter";

    /** Namespace for EFP assignments in manifest. */
    public static final String NAMESPACE_EFP = "cz.zcu.kiv.efps";

    /** Name of attribute for type of feature. */
    public static final String CAP_ATTRB_FEATURE_TYPE = "feature-type";
    /** Name of attribute for name of feature. */
    public static final String CAP_ATTRB_FEATURE_NAME = "feature-name";
    /** Name of attribute for version of feature. */
    public static final String CAP_ATTRB_FEATURE_VERSION = "version";
    /** Name of attribute for name of efp.*/
    public static final String CAP_ATTRB_EFP = "efp";
    /** Name of attribute for value. */
    public static final String CAP_ATTRB_VALUE = "value";
    /** Name of attribute with information about value. */
    public static final String CAP_ATTRB_VALUE_ID = "valueID";
    /** Identifier for direct value. */
    public static final String DIRECT_VALUE_PREFIX = "DIRECT";
    /** Identifier for mathematical formula. */
    public static final String FORMULA_VALUE_PREFIX = "FORMULA";
}
