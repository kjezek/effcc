package cz.zcu.kiv.efps.assignment.osgi.manifest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import cz.zcu.kiv.efps.assignment.osgi.manifest.ldap.FilterImpl;
import cz.zcu.kiv.efps.assignment.osgi.manifest.ldap.Parser;
import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import cz.zcu.kiv.efps.assignment.types.FeatureVersionRange;
import cz.zcu.kiv.efps.assignment.types.SimpleFeatureVersion;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import org.osgi.framework.InvalidSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.extension.manifest.FeatureParameters;
import cz.zcu.kiv.efps.assignment.osgi.OSGiAssignmentRTException;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Class for writing features and their EFP assignments into manifest.
 *
 * Date: 22.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class OSGiManifestDataWriter {
    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(OSGiManifestDataWriter.class);
    /** Handler for getting and setting parsed data from manifest. */
    private ManifestHeaderHandler manifestHandler;



    /**
     * Constructor for setting writer.
     * @param mfHandler Reference to object for working with manifest
     */
    public OSGiManifestDataWriter(final ManifestHeaderHandler mfHandler) {
        if (mfHandler == null) {
            throw new OSGiAssignmentRTException("Parameter mfHandler is null.");
        }
        this.manifestHandler = mfHandler;
    }

    /**
     * Select manifest header, which contains editing feature, and updates EFPs this feature.
     * @param feature Editing feature
     * @param efpLinks A list of EFP links with updated EFP assignments.
     */
    public void writeEfpsOfFeature(final Feature feature,
            final List<EfpLink> efpLinks) {

        if (logger.isDebugEnabled()) {
            logger.debug("Updating of capabilities with EFP assignments: " + feature);
        }

        List<EfpLink> newEfpLinks = new ArrayList<EfpLink>(efpLinks);

        List<FeatureParameters> capabilitiesInMF;
        capabilitiesInMF = manifestHandler.readFeatureHeader(
                (feature.getSide() == AssignmentSide.PROVIDED) ? MFConstants.PROVIDE_CAPABILITY
                        : MFConstants.REQUIRE_CAPABILITY);

        for (int iCap = capabilitiesInMF.size() - 1; iCap >= 0; iCap--) {
            FeatureParameters fpCap = capabilitiesInMF.get(iCap);

            Matcher matcherEfpId = OSGiManifestDataReader.RGX_EFP_NAMESPACE.matcher(
                    fpCap.getFeatureName());

            if (!matcherEfpId.find()) {
                continue;
            }

            if (isCapabilityWithAssignmentForFeature(feature, fpCap)) {
                int indexNewLink = findEfpLinkForCap(newEfpLinks, fpCap, feature);

                if (indexNewLink != -1) {
                    newEfpLinks.remove(indexNewLink);
                } else {
                    capabilitiesInMF.remove(iCap);
                }
            }
        }

        capabilitiesInMF.addAll(convertEfpLinksToFeatureParameters(feature, newEfpLinks));

        if (logger.isDebugEnabled()) {
            logger.debug("Capabilities updated.");
        }

        manifestHandler.writeFeatureHeader(
                (feature.getSide() == AssignmentSide.PROVIDED) ? MFConstants.PROVIDE_CAPABILITY
                : MFConstants.REQUIRE_CAPABILITY, capabilitiesInMF);
    }

    /**
     * In list of Efp link finds EfpLink for concrete capability.
     * @param links The list of Efp links
     * @param capability The searched capability
     * @param feature The feature
     * @return Index of required Efp link in list or -1.
     */
    public static int findEfpLinkForCap(
            final List<EfpLink> links, final FeatureParameters capability, final Feature feature) {
        for (int iL = 0; iL < links.size(); iL++) {
            EfpLink link = links.get(iL);

            String capName = MFConstants.NAMESPACE_EFP + "." + link.getGrIdStr();
            String attrValueID = null;

            if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named) {
                capName += "." + link.getLrIdStr();
                attrValueID = link.getValueName();
            } else if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct) {
                attrValueID = MFConstants.DIRECT_VALUE_PREFIX + "." + link.getParamObjectId();
            } else if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.formula) {
                attrValueID = MFConstants.FORMULA_VALUE_PREFIX + "." + link.getParamObjectId();
            } else if (link.getTypeOfAssignedValue() != null) {
                throw new AssignmentRTException("Unknown type of EFP link object: " + link);
            }

            if (!capability.getFeatureName().equals(capName)) {
                continue;
            }

            //capability is for empty value, but link contains value
            if (!capability.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_VALUE_ID)) {
                if (attrValueID != null) {
                    continue;
                }
            } else { //capability is for value, but link contains empty value
                if (attrValueID == null || !attrValueID.equals(
                        capability.getFeatureAttributes().get(MFConstants.CAP_ATTRB_VALUE_ID))) {
                    continue;
                }
            }

            if (feature.getSide() == AssignmentSide.PROVIDED) {
                if (!capability.getFeatureAttributes().containsKey(MFConstants.CAP_ATTRB_EFP)
                        || !link.getEfpName().equals(capability.getFeatureAttributes().get(
                        MFConstants.CAP_ATTRB_EFP))) {
                    continue;
                }
            } else if (feature.getSide() == AssignmentSide.REQUIRED) {
                if (!capability.getFeatureDirectives().containsKey(MFConstants.FILTER_PARAM)) {
                    continue;
                }

                FilterImpl filter;
                try {
                    String filterStr = ManifestDataTools.removeBorderMarks(
                            capability.getFeatureDirectives().get(MFConstants.FILTER_PARAM));
                    filter = new Parser(filterStr).parse();
                } catch (InvalidSyntaxException e) {
                    throw  new AssignmentRTException(e);
                }

                Object value = findValueInFilterForItem(filter, MFConstants.CAP_ATTRB_EFP);

                if (value == null || !link.getEfpName().equals(value)) {
                    continue;
                }
            } else {
                throw new AssignmentRTException("Unknown type of side: " + feature.getSide());
            }

            return iL;
        }

        return -1;
    }

    /**
     * Converts Efp link objects to string form for writing into manifest.
     * @param feature The feature
     * @param efpData The list of Efp links
     * @return List of objects for writing into manifest
     */
    private List<FeatureParameters> convertEfpLinksToFeatureParameters(
            final Feature feature, final List<EfpLink> efpData) {
        List<FeatureParameters> listCapabilitiesWithEfp = new ArrayList<FeatureParameters>(
                efpData.size());

        for (EfpLink efpLink : efpData) {
            String capName = MFConstants.NAMESPACE_EFP + "." + efpLink.getGrIdStr();

            FeatureParameters featureParameters;

            if (efpLink.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named) {
                capName += "." + efpLink.getLrIdStr();
                featureParameters = new FeatureParameters(capName);
                featureParameters.getFeatureAttributes().put(MFConstants.CAP_ATTRB_VALUE_ID,
                        efpLink.getValueName());

            } else if (efpLink.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct) {
                featureParameters = new FeatureParameters(capName);
                featureParameters.getFeatureAttributes().put(MFConstants.CAP_ATTRB_VALUE_ID,
                        MFConstants.DIRECT_VALUE_PREFIX + "." + efpLink.getParamObjectId());

            } else if (efpLink.getTypeOfAssignedValue()
                         == EfpAssignedValue.AssignmentType.formula) {
                featureParameters = new FeatureParameters(capName);
                featureParameters.getFeatureAttributes().put(MFConstants.CAP_ATTRB_VALUE_ID,
                        MFConstants.FORMULA_VALUE_PREFIX + "." + efpLink.getParamObjectId());

            } else { //empty value for example
                featureParameters = new FeatureParameters(capName);
            }

            if (feature.getSide() == AssignmentSide.PROVIDED) {
                fillProvideCapabilityWithEfpLink(feature, featureParameters, efpLink);
            } else if (feature.getSide() == AssignmentSide.REQUIRED) {
                featureParameters.getFeatureDirectives().put(
                        MFConstants.FILTER_PARAM, convertEfpLinkToFilterForRequireCapability(
                        feature, efpLink));
            } else {
                throw new AssignmentRTException("Unknown side of feature " + feature + ".");
            }

            listCapabilitiesWithEfp.add(featureParameters);
        }

        return listCapabilitiesWithEfp;
    }

    /**
     * Checks if parsed string with capability from manifest contains EFP assignment for feature.
     * @param feature The feature
     * @param cap Parsed capability from manifest
     * @return TRUE - contains EFP assignment for the feature, otherwise FALSE.
     */
    private boolean isCapabilityWithAssignmentForFeature(
            final Feature feature, final FeatureParameters cap) {

        if (feature.getSide() == AssignmentSide.PROVIDED) {
            if (!OSGiManifestDataReader.isProvideCapabilityForFeature(feature, cap)) {
                return false;
            }

        } else if (feature.getSide() == AssignmentSide.REQUIRED) {
            FilterImpl filter;
            try {
                filter = new Parser(ManifestDataTools.removeBorderMarks(
                        cap.getFeatureDirectives().get(MFConstants.FILTER_PARAM))).parse();
            } catch (InvalidSyntaxException e) {
                throw  new AssignmentRTException(e);
            }

            if (filter.getOperator() != FilterImpl.AND) {
                return false;
            }

            if (!OSGiManifestDataReader.isFeatureOwnerOfFilter(feature, filter)) {
                return false;
            }

        } else {
            throw  new AssignmentRTException("Unknown type of assignment side in feature "
                    + feature + ".");
        }

        return true;
    }

    /**
     * Fills target object with parts of EFP assignment from provide side for writing into manifest.
     * @param feature The feature
     * @param featureParameters The object with parts of EFP assignment
     * @param efpLink The Efp link object
     */
    public static void fillProvideCapabilityWithEfpLink(final Feature feature,
            final FeatureParameters featureParameters, final EfpLink efpLink) {
        featureParameters.getFeatureAttributes().put(
                MFConstants.CAP_ATTRB_FEATURE_TYPE, feature.getRepresentElement());
        featureParameters.getFeatureAttributes().put(
                MFConstants.CAP_ATTRB_FEATURE_NAME, feature.getName());

        if (feature.getVersion() != null && !feature.getVersion().equals(
                SimpleFeatureVersion.IMPLICIT_VERSION)) {
            featureParameters.getFeatureAttributes().put(
                    MFConstants.CAP_ATTRB_FEATURE_VERSION, feature.getVersion().getIdentifier());
        }

        featureParameters.getFeatureAttributes().put(
                MFConstants.CAP_ATTRB_EFP, efpLink.getEfpName());
        if (efpLink.getEfpValue() != null) { //check of empty value
            Map<String, String> valueAttributes =
                    efpLink.getEfpValue().convertIntoParameters(MFConstants.CAP_ATTRB_VALUE);
            featureParameters.getFeatureAttributes().putAll(valueAttributes);
        } else {
            featureParameters.getFeatureAttributes().put(
                    MFConstants.CAP_ATTRB_VALUE, "\"\"");
        }
    }

    /**
     * Converts Efp link object into string for LDAP filter string on require side.
     * @param feature The feature which owns Efp link
     * @param efpLink The Efp link object
     * @return LDAP filter string
     */
    public static String convertEfpLinkToFilterForRequireCapability(
            final Feature feature, final EfpLink efpLink) {
        String filter = "\"(&";
        filter += "(" + MFConstants.CAP_ATTRB_FEATURE_TYPE
                + "=" + feature.getRepresentElement() + ")";
        filter += "(" + MFConstants.CAP_ATTRB_FEATURE_NAME + "=" + feature.getName() + ")";
        filter += "(" + MFConstants.CAP_ATTRB_EFP + "=" + efpLink.getEfpName() + ")";

        if (feature.getVersion() instanceof FeatureVersionRange) {
            FeatureVersionRange versionRange = (FeatureVersionRange) feature.getVersion();

            if (!versionRange.isImplicitLeftValue()) {
                if (versionRange.getLeftInclusive()) {
                    filter += "(" + MFConstants.CAP_ATTRB_FEATURE_VERSION
                            + ">=" + versionRange.getLeftValue().getIdentifier() + ")";
                } else {
                    filter += "(!(" + MFConstants.CAP_ATTRB_FEATURE_VERSION + "<="
                            + versionRange.getLeftValue().getIdentifier() + "))";
                }
            }

            if (!versionRange.isImplicitRightValue()) {
                if (versionRange.getRightInclusive()) {
                    filter += "(" + MFConstants.CAP_ATTRB_FEATURE_VERSION
                            + "<=" + versionRange.getRightValue().getIdentifier() + ")";
                } else {
                    filter += "(!(" + MFConstants.CAP_ATTRB_FEATURE_VERSION + ">="
                            + versionRange.getRightValue().getIdentifier() + "))";
                }
            }
        }

        if (efpLink.getEfpValue() != null) { //check of empty value
            filter +=  "(" +  efpLink.getEfpValue().convertIntoLDAPFilter(
                    MFConstants.CAP_ATTRB_VALUE) + ")";
        }

        filter += ")\"";

        return  filter;
    }

    /**
     * Tries to find item with value in ldap filter.
     * @param filter The parsed filter
     * @param itemName The nme of item
     * @return value of searched item or NULL
     */
    public static Object findValueInFilterForItem(final FilterImpl filter, final String itemName) {
        if (filter.isItem()) {
            if (itemName.equals(filter.getAttribute())) {
                return filter.getValue();
            }

            return null;
        }

        for (FilterImpl item : (FilterImpl[]) filter.getValue()) {
            if (!item.isItem()) {
                Object value = findValueInFilterForItem(item, itemName);

                if (value != null) {
                    return value;
                }

            } else if (itemName.equals(item.getAttribute())) {
                return item.getValue();
            }
        }

        return null;
    }
}
