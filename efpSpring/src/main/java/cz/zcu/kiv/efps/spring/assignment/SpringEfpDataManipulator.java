package cz.zcu.kiv.efps.spring.assignment;

import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.types.Feature;

import java.util.List;

/**
 * This is an implementation of the EFP data manipulator
 * for the Spring IoC container.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class SpringEfpDataManipulator extends AbstractEfpDataManipulator<Object> {

    public SpringEfpDataManipulator(AbstractEfpDataLocation efpDataLocation) {
        super(efpDataLocation);
        // TODO Auto-generated constructor stub
    }

    @Override
    public List<Feature> readFeatures() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<EfpLink> readEFPs(final Feature feature) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void assignEFPs(final Feature feature, final List<EfpLink> efpData) {
        // TODO Auto-generated method stub

    }

	@Override
	public Object open() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}



}
