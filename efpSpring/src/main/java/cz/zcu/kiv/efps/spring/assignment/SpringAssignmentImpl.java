/**
 *
 */
package cz.zcu.kiv.efps.spring.assignment;

import cz.zcu.kiv.efps.assignment.client.plugin.EfpPluginConfiguredLoader;

/**
 * The assignment plugin for the Spring IoC container.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class SpringAssignmentImpl extends EfpPluginConfiguredLoader {

    /** {@inheritDoc} */
    @Override
    public String getPluginConfigurationFile() {
        return "/plugin.spring.properties";
    }

}
