/**
 *
 */
package cz.zcu.kiv.efps.spring.annotations;

/**
 * This annotation represents a provided extra-functional properties
 *
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public @interface Provide {

	/** A GR this for which EFPs are provided */
	String gr();

	Efp[] efps();

}
