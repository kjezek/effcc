/**
 *
 */
package cz.zcu.kiv.efps.spring.annotations;

/**
 * An annotation expressing one EFP
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public @interface Efp {

	/** A name of an EFP this annotation expresses. */
	String name();

	String direct() default "";

	LrValue[] lr() default @LrValue(name = "", valueName = "");

	String computed() default "";

}
