/**
 *
 */
package cz.zcu.kiv.efps.spring.annotations;

/**
 * An annotation to express a LR value
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public @interface LrValue {

	String name();

	String valueName();

}
