/**
 *
 */
package cz.zcu.kiv.efps.spring.assignment;

import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataLocation;

/**
 * This is an implementation loading EFP data from components
 * of a Spring IoC container.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class SpringEfpDataLocation
    extends AbstractEfpDataLocation {

    @Override
    protected EfpDataLocation openComponent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void closeComponent() {
        // TODO Auto-generated method stub

    }


}
