/**
 *
 */
package cz.zcu.kiv.efps.spring.annotations;

/**
 * A requirement .
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public @interface Require {

	String gr();

	Condition[] conditions();

	Efp[] efps();
}
