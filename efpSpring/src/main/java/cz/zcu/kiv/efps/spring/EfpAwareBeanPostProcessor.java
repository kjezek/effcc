package cz.zcu.kiv.efps.spring;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.BeanReference;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;

import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.comparator.api.EfpComparator;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.spring.assignment.SpringAssignmentImpl;

/**
 * A bean post processor used for evaluating EFPs on starting beans.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpAwareBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter {

    /** A map of all beans represented as EFP Assignment features.  */
    private Map<String, Feature> beanFeatures = new HashMap<String, Feature>();

    /** This map holds all bound features. */
    private Map<Feature, Feature> boundFeatures = new HashMap<Feature, Feature>();

    /** A local registry ID from the configuration. */
    private Integer lrId;

    /**
     * A matching function that matches features according to pairs
     * obtained in the binding process.
     */
    private final MatchingFunction mu = new MatchingFunction() {

        @Override
        public boolean matches(final Feature feature1, final Feature feature2) {

            boolean result = false;

            // try first as the key.
            Feature f = boundFeatures.get(feature1);

            // try second as the key.
            if (f != null) {
                // verify the functions are in matching pairs.
                result = f.equals(feature2);
            } else {
                f = boundFeatures.get(feature2);
                if (f != null) {
                    result = f.equals(feature1);
                }
            }

            return result;
        }
    };

    @Override
    public PropertyValues postProcessPropertyValues(
            final PropertyValues pvs,
            final PropertyDescriptor[] pds,
            final Object bean,
            final String beanName)  {

        for (PropertyDescriptor pd : pds) {
            PropertyValue prop = pvs.getPropertyValue(pd.getName());

            // a property has no value to be set.
            if (prop == null) {
                continue;
            }

            Object value = prop.getValue();

            // we are interested only in a bean binding
            // we omit e.g. value binding.
            if (value instanceof BeanReference) {
                Feature firstBean = findBeanFeature(beanName);

                String featureName = prop.getName();
                Feature required = new BasicFeature(
                        featureName, Feature.AssignmentSide.REQUIRED, "service", true, firstBean);

                BeanReference secondBeanRef = (BeanReference) value;
                Feature secondBean = findBeanFeature(secondBeanRef.getBeanName());
                Feature provided = new BasicFeature(
                        featureName, Feature.AssignmentSide.PROVIDED, "service", true, secondBean);

                boundFeatures.put(required, provided);
            }

        }

        return pvs;
    }

    /**
     * This method evaluates EFPs on all bundles in the assembly.
     * @return a list of evaluation results.
     */
    public List<EfpEvalResult> evaluateEfps() {

        List<String> beans = new ArrayList<String>(beanFeatures.keySet());

        return EfpComparator.evaluate(beans, SpringAssignmentImpl.class.getName(), lrId);
    }

    /**
     * This method finds a "bean" feature by its name.
     * @param beanName the name of the bean
     * @return the feature corresponding to the name.
     */
    private Feature findBeanFeature(final String beanName) {
        Feature result = beanFeatures.get(beanName);

        if (result == null) {
            result = new BasicFeature(beanName, Feature.AssignmentSide.PROVIDED, "bean", true);
            beanFeatures.put(beanName, result);
        }

        return result;
    }

    /**
     * @param lrId the lrId to set
     */
    public void setLrId(final Integer lrId) {
        this.lrId = lrId;
    }


}
