package cz.zcu.kiv.efps.spring.annotations;

/**
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public @interface Condition {

	String lr() default "";

	String condition();


}
