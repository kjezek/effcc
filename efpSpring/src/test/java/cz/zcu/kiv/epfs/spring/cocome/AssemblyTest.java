/**
 *
 */
package cz.zcu.kiv.epfs.spring.cocome;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cz.zcu.kiv.efps.spring.EfpAwareBeanPostProcessor;
import cz.zcu.kiv.efps.spring.cocome.beans.InventoryApplication;

/**
 * This test tries to start the assembly
 * of beans with the evaluation of their EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class AssemblyTest {


    /** A test to evaluate an assembly with EFPs. */
    @Test
    public void testEvaluateAssembly() {
        ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");

        EfpAwareBeanPostProcessor efp = (EfpAwareBeanPostProcessor) context.getBean("efp");
        Assert.assertNotNull(efp);

        InventoryApplication app = (InventoryApplication) context.getBean("application");
        Assert.assertNotNull(app);

//        List<EfpEvalResult> reuslts = efp.evaluateEfps();
//
//        Assert.assertNotNull(reuslts);
    }
}
