/**
 *
 */
package cz.zcu.kiv.efps.spring.cocome.beans;

import cz.zcu.kiv.efps.spring.annotations.Condition;
import cz.zcu.kiv.efps.spring.annotations.Efp;
import cz.zcu.kiv.efps.spring.annotations.LrValue;
import cz.zcu.kiv.efps.spring.annotations.Provide;
import cz.zcu.kiv.efps.spring.annotations.Require;

/**
 * InventoryData a part of CoCoME stub implementation.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Provide(
		gr = "CoCoME",
		efps = @Efp(name = "response_time",
			 direct = "30",
			 lr = @LrValue(name = "Store", valueName = "slow"),
			 computed = "2 * jdbc.response_time")  // jdbc is named as the respective field
		)
public class InventoryData implements StoreQueryIf {

    /** Requires JDBC. */
	@Require(
			gr = "CoCoME",
			conditions = {
				@Condition(condition = "&(reposnse_time>10)(response_time<20)"),
				@Condition(lr = "Store",
				   condition = "&(reposnse_time>slow)(response_time<high)")
			},

		   efps = {
				@Efp(name="response_time", direct = "15"),
				@Efp(name="response_time", lr = @LrValue(name = "Store", valueName="average"))
				}

			)
    private Jdbc jdbc;

    /** A test parameter. */
    private String parameter;

    /**
     * @return the parameter
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * @param parameter the parameter to set
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    /**
     * @return the jdbc
     */
    public Jdbc getJdbc() {
        return jdbc;
    }

    /**
     * @param jdbc the jdbc to set
     */
    public void setJdbc(final Jdbc jdbc) {
        this.jdbc = jdbc;
    }


}
