/**
 *
 */
package cz.zcu.kiv.efps.spring.assignment;

import org.junit.Assert;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.client.EfpAssignmentClient;

/**
 * A test of the EFP Assignment Spring implementation.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class SpringAssignmentImplTest {


    /** It test whether the plugin is corretly configured. */
    @Test
    public void testModuleConfiguration() {

        EfpAwareComponentLoader loader = EfpAssignmentClient
            .initialiseComponentLoader(SpringAssignmentImpl.class);

        ComponentEfpAccessor access = loader.loadForRead("ABCD");

        Assert.assertNotNull(access);
    }
}
