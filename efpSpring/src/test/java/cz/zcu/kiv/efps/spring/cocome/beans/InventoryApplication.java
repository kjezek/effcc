package cz.zcu.kiv.efps.spring.cocome.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * InventoryApplication a part of CoCoME stub implementation.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service("application")
public class InventoryApplication {

    /** A required StoreQueryIf. */
    @Autowired
    private StoreQueryIf storeQueryIf;

    /**
     * @return the storeQueryIf
     */
    public StoreQueryIf getStoreQueryIf() {
        return storeQueryIf;
    }

    /**
     * @param storeQueryIf the storeQueryIf to set
     */
    public void setStoreQueryIf(final StoreQueryIf storeQueryIf) {
        this.storeQueryIf = storeQueryIf;
    }


}
