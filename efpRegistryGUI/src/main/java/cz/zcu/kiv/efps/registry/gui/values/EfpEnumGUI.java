package cz.zcu.kiv.efps.registry.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible enum value. <br>
 * Date: 10. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpEnumGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** JTextField for value of the enum. */
    private JTextField tfValue;

    /** Label of name of type. */
    private JLabel lblType;

    /**
     * Create the panel.
     */
    EfpEnumGUI() {
        super();
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        lblType = new JLabel(bundle.getProperty("lblEnum"));
        add(lblType, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.weightx = 1.0;
        tfValue = new JTextField();
        tfValue.setToolTipText(bundle.getProperty("toolTipEnum"));
        add(tfValue, gbc);
    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        if (tfValue.getText().equals("")) {
            throw new IllegalValueException(bundle.getProperty("ERR_FILL_VALUE_ENUM"));
        }
        return new EfpEnum(tfValue.getText().trim().replaceAll(", +", ",").split(","));
    }

    @Override
    public void setValue(final EfpValueType value) {
        String text = "";
        for (String s : ((EfpEnum) value).getItems()) {
            text += ", " + s;
        }
        text = text.substring(2);
        tfValue.setText(text);

    }

    @Override
    public void lock() {
        tfValue.setEditable(false);
    }
}
