package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;

/**
 * Dialog for test of gamma function with testing values. <br>
 * <br>
 * <b>Date:</b> 1.3.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpGammaEvaluate extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7802863519039960317L;

    /** Width of frame. */
    private static final int WIDTH = 600;

    /** Height of frame. */
    private static final int HEIGHT = 250;

    /** A testing gamma gamma. */
    private String gamma;

    /** A class of value type of efp. */
    private Class<?> valueType;

    /** A GUI for fist value. */
    private EfpValueGUI firstValue;

    /** A GUI for second value. */
    private EfpValueGUI secondValue;

    /** Variable of this dialog. */
    private EfpGammaEvaluate self;

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("efpGUI");

    /**
     * Test a gamma.
     * @param gamma A gamma function.
     * @param valueType The value type of efp which has the gamma.
     */
    public EfpGammaEvaluate(final String gamma, final Class<?> valueType) {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle(bundle.getProperty("gamma_editor_testing"));
        setModal(true);

        self = this;
        this.gamma = gamma;
        this.valueType = valueType;
        init();
        setVisible(true);
    }

    /** Initialization of dialog. */
    private void init() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.insets = new Insets(0, 0, 0, 0);
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(getValuesPanel(), gbc);

        gbc.insets = new Insets(5, 0, 5, 0);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(getControlPanel(), gbc);

        setContentPane(panel);
    }

    /**
     * Return a panel with form for input values.
     * @return A panel for input values.
     */
    private Container getValuesPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(bundle.getProperty("gamma_test_values")));
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridy = 0;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        panel.add(new JLabel(bundle.getProperty("gamma_first")), gbc);

        gbc.gridx = 1;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        firstValue = FactoryValueGUI.getValueGUI(valueType);
        panel.add(firstValue, gbc);

        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        panel.add(new JLabel(bundle.getProperty("gamma_second")), gbc);

        gbc.gridx = 1;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        secondValue = FactoryValueGUI.getValueGUI(valueType);
        panel.add(secondValue, gbc);
        JScrollPane sp = new JScrollPane();
        sp.setBorder(null);
        sp.setViewportView(panel);
        return sp;
    }

    /**
     * Return a control panel with test button.
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel panel = new JPanel();

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    EfpValueType value1 = firstValue.getValue();
                    EfpValueType value2 = secondValue.getValue();
                    try {
                        Gamma gam = new UserGamma(gamma);
                        int result = gam.compare(value1, value2);
                        JOptionPane.showMessageDialog(self,
                                bundle.getProperty("gamma_OK") + "\n" + result,
                                "Gamma is OK", JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(
                                self,
                                bundle.getProperty("gamma_KO") + "\n" + e1.getMessage(),
                                "Wrong gamma", JOptionPane.WARNING_MESSAGE);
                    }
                } catch (IllegalValueException e1) {
                    throw new WrongInputException(e1.getMessage());
                }
            }
        });
        panel.add(btnTest);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        panel.add(btnStorno);
        return panel;
    }


}
