package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.gui.exceptions.ValueUsedException;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.renderers.ValueNameListCellRenderer;

/**
 * Formular for creating and deleting value names. <br>
 * <br>
 * <b>Date:</b> 8.12.2010
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpValueNames extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Width of frame. */
    private static final int WIDTH = 450;

    /** Height of frame. */
    private static final int HEIGHT = 300;

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(EfpValueNames.class);

    /** Properties file with localization. */
    private EfpLocalization bundle;

    /** Content pane of frame. */
    private JPanel contentPane;

    /** JList of efp value. */
    private JList names;

    /** TextField of new value name. */
    private JTextField tfNewValueName;

    /**
     * Create the frame.
     */
    public EfpValueNames() {
        bundle = new EfpLocalization("efpValueNames");
        // setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        setTitle(bundle.getProperty("caption"));
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        init();
        load();
    }

    /**
     * Initialization GUI.
     */
    private void init() {

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridBagLayout());

        JLabel lblNewValueName = new JLabel(bundle.getProperty("lblNewValue"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(lblNewValueName, gbc);

        JScrollPane spNames = new JScrollPane();
        gbc.gridheight = 8;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        contentPane.add(spNames, gbc);

        names = new JList(new DefaultListModel());
        names.setCellRenderer(new ValueNameListCellRenderer());
        names.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        spNames.setViewportView(names);

        tfNewValueName = new JTextField();
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(tfNewValueName, gbc);
        tfNewValueName.setColumns(10);

        JButton btnAdd = new JButton(bundle.getProperty("btnAdd"));
        btnAdd.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/create.png")));
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                addName();
            }
        });
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 2;
        contentPane.add(btnAdd, gbc);

        JButton btnDelete = new JButton(bundle.getProperty("btnDelete"));
        btnDelete.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/delete.png")));
        btnDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                removeName();
            }
        });
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 3;
        contentPane.add(btnDelete, gbc);
    }

    /**
     * Load actual data from database to list.
     */
    private void load() {
        for (String obj : EfpRegistryGUI.getEfpClient().getAllValueNames(
                EfpRegistryGUI.getSelectedGR().getId())) {
            ((DefaultListModel) names.getModel()).addElement(obj);
        }
    }

    /**
     * Create new value name according text in tfNewValueName.
     */
    private void addName() {
        if (((DefaultListModel) names.getModel()).contains(tfNewValueName.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_NAME_EXIST"));
        }
        ((DefaultListModel) names.getModel()).addElement(tfNewValueName.getText());
        EfpRegistryGUI.getEfpClient().createValueName(
                EfpRegistryGUI.getSelectedGR().getId(), tfNewValueName.getText());

        logger.info("Create valueName:" + tfNewValueName.getText() + " TO GR:"
                + EfpRegistryGUI.getSelectedGR());

        tfNewValueName.setText("");

    }

    /**
     * Remove selected name.
     */
    private void removeName() {
        String pom = (String) names.getSelectedValue();
        if (!EfpRegistryGUI.getEfpClient()
                .findByAssignedValueName(EfpRegistryGUI.getSelectedGR().getId(), pom)
                .isEmpty()) {
            throw new ValueUsedException(bundle.getProperty("ERR_NAME_USED"));
        }

     // confirm
        Object[] options = new Object[] {bundle.getProperty("yes"),
                bundle.getProperty("no")};
        int value = JOptionPane.showOptionDialog(null,
                bundle.getProperty("del_valName_quest") + " " + pom + "?",
                "Delete value name", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

        if (value != JOptionPane.YES_OPTION) {
            return;
        }
        // end confirm

        ((DefaultListModel) names.getModel()).removeElement(pom);
        EfpRegistryGUI.getEfpClient().deleteValueName(
                EfpRegistryGUI.getSelectedGR().getId(), pom);

        logger.info("Delete valueName:" + pom + " FROM GR:"
                + EfpRegistryGUI.getSelectedGR());
    }
}
