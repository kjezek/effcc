package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.renderers.GrListCellRenderer;
import cz.zcu.kiv.efps.types.gr.GR;

/**
 * Formular for changing GR. <br>
 * <br>
 * <b>Date:</b> 8.12.2010
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class GRChangeGUI extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Width of frame. */
    private static final int WIDTH = 450;

    /** Height of frame. */
    private static final int HEIGHT = 300;

    /** Properties file with localization. */
    private EfpLocalization bundle;

    /** Content pane of frame. */
    private JPanel contentPane;

    /** Variable of this formular. */
    private final GRChangeGUI self;

    /** JList of efp value. */
    private JList grs;

    /**
     * Create the frame.
     */
    public GRChangeGUI() {
        EfpRegistryGUI.getRegistryGUI().setVisible(false);
        bundle = new EfpLocalization("GRChangeGUI");
        // setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setTitle(bundle.getProperty("caption"));
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        self = this;
        init();
        load();
        validate();
        repaint();
    }

    /**
     * Initialization GUI.
     */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                if (EfpRegistryGUI.getRegistryGUI().isVisible()) {
                    self.dispose();
                } else {
                    Object[] options = new Object[] {bundle.getProperty("yes"),
                            bundle.getProperty("no")};
                    int value = JOptionPane.showOptionDialog(null, new EfpLocalization(
                            "efpRegistryGUI").getProperty("quit_quest"),
                            "Quit a application", JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

                    if (value == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
        });
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridBagLayout());

        grs = new JList(new DefaultListModel());
        grs.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                changeGR();
            }
        });
        grs.setCellRenderer(new GrListCellRenderer());
        grs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JLabel lblChange = new JLabel(bundle.getProperty("lblChange"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(lblChange, gbc);

        Icon image = new ImageIcon(getClass().getClassLoader().getResource(
                "images/refresh.png"));

        JButton refresh = new JButton(image);
        refresh.setMargin(new Insets(0, 0, 0, 0));
        refresh.setBorderPainted(false);
        refresh.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                load();
            }
        });
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(refresh, gbc);

        JScrollPane spNames = new JScrollPane();
        spNames.setViewportView(grs);
        gbc.gridheight = 1;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        contentPane.add(spNames, gbc);
    }

    /**
     * Load actual data from database to list.
     */
    private void load() {
        grs.setListData(EfpRegistryGUI.getEfpClient().findGrAll().toArray());
    }

    /**
     * Change selected GR in RegistryGUI.
     */
    private void changeGR() {
        if (grs.getSelectedValue() == null) {
            return;
        }
        EfpRegistryGUI.setSelectedGR((GR) grs.getSelectedValue());
        EfpRegistryGUI.showRegistryGUI();
        this.dispose();
    }
}
