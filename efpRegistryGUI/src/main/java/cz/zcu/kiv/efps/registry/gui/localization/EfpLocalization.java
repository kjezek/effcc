package cz.zcu.kiv.efps.registry.gui.localization;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Localization class.
 * Localize label according set Locale. <br>
 * <br>
 * <b>Date:</b> 23.11.2010
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpLocalization {

    /** Path to properties. */
    private static final String PATH = "localization.";

    /** Resource of labels. */
    private ResourceBundle bundle;

    /**
     * Construct localization.
     * @param name name of properties file
     */
    public EfpLocalization(final String name) {
        try {
            bundle = ResourceBundle.getBundle(PATH + name);
        } catch (Exception e) {
            bundle = ResourceBundle.getBundle(PATH + name, Locale.ENGLISH);
        }
    }

    /**
     * Return label from properties.
     * @param key Key of label.
     * @return Value of label.
     */
    public String getProperty(final String key) {
        String value = (String) bundle.getString(key);
        try {
            return new String(value.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("No support encoding in properties file!");
        }

    }

}
