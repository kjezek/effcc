package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.types.lr.LR;

/**
 * Frame for create and edit local register. <br>
 * <br>
 * <b>Date:</b> 5.1.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class LrGUI extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = 4215783584521592624L;

    /** Width of frame. */
    private static final int WIDTH = 300;

    /** Height of frame. */
    private static final int HEIGHT = 250;

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(LrGUI.class);

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("LrGUI");

    /** Text field for name of LR. */
    private JTextField tfName;

    /** Text field for string id of LR. */
    private JTextField tfIdStr;

    /** Map of LR and its names. */
    private Map<String, LR> lrs;

    /** True if editing exists LR. */
    private boolean edit;

    /** Editing LR. */
    private LR lr;

    /** Combobox of names parent LR. */
    private JComboBox cmbParent;

    /**
     * Create the frame for create LR.
     */
    public LrGUI() {
        edit = false;
        setTitle(bundle.getProperty("create"));
        init();
    }

    /**
     * Create the frame for edit LR.
     * @param lr Edited local register.
     */
    public LrGUI(final LR lr) {
        edit = true;
        setTitle(bundle.getProperty("edit"));
        this.lr = lr;
        init();
        load();
    }

    /**
     * Initialize GUI.
     */
    private void init() {
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setVisible(true);
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl = new GridBagLayout();
        gbl.columnWidths = new int[] {0, 0};
        gbl.rowHeights = new int[] {0, 0, 0, 0, 0, 0};
        gbl.columnWeights = new double[] {1.0, Double.MIN_VALUE};
        gbl.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl);

        JLabel lblName = new JLabel(bundle.getProperty("lblName"));
        GridBagConstraints gbcLblName = new GridBagConstraints();
        gbcLblName.anchor = GridBagConstraints.WEST;
        gbcLblName.insets = new Insets(0, 0, 5, 0);
        gbcLblName.gridx = 0;
        gbcLblName.gridy = 0;
        contentPane.add(lblName, gbcLblName);

        tfName = new JTextField();
        GridBagConstraints gbcTfName = new GridBagConstraints();
        gbcTfName.insets = new Insets(0, 0, 5, 0);
        gbcTfName.fill = GridBagConstraints.HORIZONTAL;
        gbcTfName.gridx = 0;
        gbcTfName.gridy = 1;
        contentPane.add(tfName, gbcTfName);
        tfName.setColumns(10);

        JLabel lblIdStr = new JLabel(bundle.getProperty("lblIdStr"));
        GridBagConstraints gbcLblIdStr = new GridBagConstraints();
        gbcLblIdStr.anchor = GridBagConstraints.WEST;
        gbcLblIdStr.insets = new Insets(0, 0, 5, 0);
        gbcLblIdStr.gridx = 0;
        gbcLblIdStr.gridy = 2;
        contentPane.add(lblIdStr, gbcLblIdStr);

        tfIdStr = new JTextField();
        GridBagConstraints gbcTfIdStr = new GridBagConstraints();
        gbcTfIdStr.insets = new Insets(0, 0, 5, 0);
        gbcTfIdStr.fill = GridBagConstraints.HORIZONTAL;
        gbcTfIdStr.gridx = 0;
        gbcTfIdStr.gridy = 3;
        contentPane.add(tfIdStr, gbcTfIdStr);
        tfName.setColumns(10);


        JLabel lblParent = new JLabel(bundle.getProperty("lblParent"));
        GridBagConstraints gbcLblParentLocal = new GridBagConstraints();
        gbcLblParentLocal.anchor = GridBagConstraints.WEST;
        gbcLblParentLocal.insets = new Insets(10, 0, 5, 0);
        gbcLblParentLocal.gridx = 0;
        gbcLblParentLocal.gridy = 4;
        contentPane.add(lblParent, gbcLblParentLocal);

        cmbParent = new JComboBox();
        GridBagConstraints gbcCmbParent = new GridBagConstraints();
        gbcCmbParent.insets = new Insets(0, 0, 5, 0);
        gbcCmbParent.fill = GridBagConstraints.HORIZONTAL;
        gbcCmbParent.gridx = 0;
        gbcCmbParent.gridy = 5;
        contentPane.add(cmbParent, gbcCmbParent);

        lrs = new HashMap<String, LR>();
        for (LR lr : EfpRegistryGUI.getEfpClient().findLrForGr(
                EfpRegistryGUI.getSelectedGR())) {
            lrs.put(lr.getName(), lr);
        }
        Object[] array = new Object[lrs.size() + 1];
        array[0] = "root";
        System.arraycopy(lrs.keySet().toArray(), 0, array, 1, lrs.size());
        cmbParent.setModel(new DefaultComboBoxModel(array));

        JButton btnSave = new JButton(bundle.getProperty("btnSave"));
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (edit) {
                    edit();
                } else {
                    create();
                }

            }
        });
        GridBagConstraints gbcBtnSave = new GridBagConstraints();
        gbcBtnSave.gridx = 0;
        gbcBtnSave.gridy = 6;
        contentPane.add(btnSave, gbcBtnSave);
    }

    /**
     * Load data to frame.
     */
    private void load() {
        tfName.setText(lr.getName());
        tfIdStr.setText(lr.getIdStr());

        lrs.remove(lr.getName());
        cmbParent.setModel(new DefaultComboBoxModel(lrs.keySet().toArray()));
        Object[] array = new Object[lrs.size() + 1];
        array[0] = "root";
        System.arraycopy(lrs.keySet().toArray(), 0, array, 1, lrs.size());
        cmbParent.setModel(new DefaultComboBoxModel(array));
        if (lr.getParentLR() != null) {
            cmbParent.setSelectedItem(lr.getParentLR().getName());
        } else {
            cmbParent.setSelectedIndex(0);
        }
    }

    /**
     * Edit LR.
     */
    private void edit() {
        String name = tfName.getText();
        if (name.equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }

        if (tfIdStr.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_ID_STR"));
        }

        if (!GRAdministrationGUI.isValidUniqueShortcut(tfIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_NOTVALID"));
        }

        LR parent = null;
        if (!cmbParent.getSelectedItem().equals("root")) {
            parent = lrs.get(cmbParent.getSelectedItem());
        }

        LR newLr = new LR(lr.getId(), name, EfpRegistryGUI.getSelectedGR(), parent, tfIdStr.getText());

        EfpRegistryGUI.getEfpClient().update(newLr);
        logger.info("Update LR FROM:" + lr + " TO:" + newLr);
        EfpRegistryGUI.getRegistryGUI().refresh();
        this.dispose();
    }

    /**
     * Create new LR.
     */
    private void create() {
        String name = tfName.getText();
        if (name.equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }

        if (tfIdStr.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_ID_STR"));
        }

        if (!GRAdministrationGUI.isValidUniqueShortcut(tfIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_NOTVALID"));
        }

        LR parent = null;
        if (!cmbParent.getSelectedItem().equals("root")) {
            parent = lrs.get(cmbParent.getSelectedItem());
        }

        LR newLr = new LR(name, EfpRegistryGUI.getSelectedGR(), parent, tfIdStr.getText());

        EfpRegistryGUI.getEfpClient().create(newLr);

        logger.info("Create LR:" + newLr);
        EfpRegistryGUI.getRegistryGUI().refresh();
        this.dispose();
    }
}
