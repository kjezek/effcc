package cz.zcu.kiv.efps.registry.gui.renderers;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * ListCellRenderer renderer of name of value. <br>
 * <br>
 * <b>Date:</b> 22.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class ValueNameListCellRenderer extends DefaultListCellRenderer {

    /** Serial version. */
    private static final long serialVersionUID = 7569171632710325169L;

    @Override
    public Component getListCellRendererComponent(final JList list, final Object value,
            final int index, final boolean isSelected, final boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list,
                value, index, isSelected, cellHasFocus);

        label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/valueNameIcon.png")));
        return label;
    }
}
