package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Color;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;

/**
 * The welcome screen to repository.
 * Date: 9. 2. 2011
 *
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">
 *         martin.stulc@gmail.com</a>
 */

public class WelcomeScreen extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = -4127256236578156290L;

    /** Width of welcome screen. */
    private static final int WIDTH = 513;

    /** Height of welcome screen. */
    private static final int HEIGHT = 320;

    /**
     * Create welcome screen.
     */
    public WelcomeScreen() {
        this.setSize(WIDTH, HEIGHT);
        this.setTitle(new EfpLocalization("efpRegistryGUI").getProperty("caption"));
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


        URL url = getClass().getClassLoader().getResource("images/welcomeScreen.png");
        JLabel label = new JLabel(
                new ImageIcon(Toolkit.getDefaultToolkit().getImage(url)));
        label.setBorder(new LineBorder(Color.BLACK));
        add(label);

        this.repaint();
        this.setVisible(true);
    }

}
