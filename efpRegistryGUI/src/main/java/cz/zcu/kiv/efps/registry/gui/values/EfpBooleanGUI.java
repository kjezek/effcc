package cz.zcu.kiv.efps.registry.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible boolean value. <br>
 * Date: 9. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpBooleanGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** ComboBox for visible boolean. */
    private JComboBox cmbBool;

    /** Label of name of type. */
    private JLabel lblType;

    /**
     * Create the panel.
     */
    EfpBooleanGUI() {
        super();
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        lblType = new JLabel(bundle.getProperty("lblBoolean"));
        add(lblType, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.weightx = 1.0;
        cmbBool = new JComboBox();
        cmbBool.setToolTipText(bundle.getProperty("toolTipBoolean"));
        cmbBool.setModel(new DefaultComboBoxModel(new String[] {"TRUE", "FALSE"}));
        add(cmbBool, gbc);
    }

    @Override
    public EfpValueType getValue() {
        if (cmbBool.getSelectedIndex() == 0) {
            return new EfpBoolean(true);
        } else {
            return new EfpBoolean(false);
        }
    }

    @Override
    public void setValue(final EfpValueType value) {
        if (((EfpBoolean) value).getValue()) {
            cmbBool.setSelectedIndex(0);
        } else {
            cmbBool.setSelectedIndex(1);
        }
        cmbBool.setSelectedItem(value.getLabel());

    }

    @Override
    public void lock() {
        cmbBool.setModel(new DefaultComboBoxModel(new String[] {cmbBool.getSelectedItem()
                .toString()}));

    }
}
