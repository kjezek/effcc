package cz.zcu.kiv.efps.registry.gui.tools;

import cz.zcu.kiv.efps.registry.client.EfpClient;
import cz.zcu.kiv.efps.registry.client.EfpClientImpl;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;

import java.text.Normalizer;
import java.util.List;

/**
 * Tool for generating string IDs to GRs and LRs that don't have it set.
 * Checks all GRs and LRs in repository.
 */
public final class GRLRStringIDsGenerator {

    /**
     * A constructor.
     */
    private GRLRStringIDsGenerator() { }

    /**
     * Main.
     * @param args Arguments
     */
    public static void main(final String[] args) {
        EfpClient efpClient = EfpClientImpl.getClient();
        List<GR> grs = efpClient.findGrAll();

        for (GR gr : grs) {
            if (gr.getIdStr() == null || gr.getIdStr().equals("")) {
                GR grNew = new GR(gr.getId(), gr.getName(), createStringID(gr.getName()));
                efpClient.update(grNew);
            }

            List<LR> lrs = efpClient.findLrForGr(gr);

            for (LR lr : lrs) {
                if (lr.getIdStr() == null || lr.getIdStr().equals("")) {
                    LR lrNew = new LR(lr.getId(), lr.getName(), gr, lr.getParentLR(),
                            createStringID(lr.getName()));
                    efpClient.update(lrNew);
                }
            }
        }
    }

    /**
     * Creates from name string ID.
     * @param name The name
     * @return The string ID
     */
    private static String createStringID(final String name) {
        String nameDiac = removeAccents(name).trim();

        String strID = "";

        for (int i = 0; i < nameDiac.length(); i++) {
            char c = nameDiac.charAt(i);

            if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')
                    || (c >= 'A' && c <= 'Z') || c == '_') {
                strID += c;
            } else if ((c == ',' || c == ' ') && strID.length() > 0
                    && strID.charAt(strID.length() - 1) != '_') {
                strID += '_';
            }



        }

        return strID;
    }

    /**
     * Removes from text diacritic.
     * @param text The text
     * @return The text without diacritic
     */
    private static String removeAccents(final String text) {
        if (text == null) {
            return null;
        }

        return Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
