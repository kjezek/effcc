package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.client.EfpClient;
import cz.zcu.kiv.efps.registry.client.EfpClientImpl;
import cz.zcu.kiv.efps.registry.gui.components.JValueTree;
import cz.zcu.kiv.efps.registry.gui.components.JefpList;
import cz.zcu.kiv.efps.registry.gui.exceptions.ExceptionHandler;
import cz.zcu.kiv.efps.registry.gui.exceptions.ValueUsedException;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;

/**
 * The GUI class for EFP registry. <br>
 * Date: 11. 10. 2010
 *
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">
 *         martin.stulc@gmail.com</a>
 */
public final class EfpRegistryGUI extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = 3704027809593225106L;

    /** Width of frame. */
    private static final int WIDTH = 1000;

    /** Height of frame. */
    private static final int HEIGHT = 700;

    /** This frame singleton. */
    private static EfpRegistryGUI self;

    /** Instance of welcome screen. */
    private static WelcomeScreen welcome;

    /** Properties file with localization. */
    private EfpLocalization bundle;

    /** Client for communication with server. */
    private static EfpClient efpClient;

    /** Used global register. */
    private static GR gr;

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(EfpRegistryGUI.class);

    /** List of EFP. */
    private JefpList efpList;

    /** List of EFP depends on derived EFP. */
    private JefpList derivedList;

    /** Tree of local register and their values. */
    private JValueTree valueTree;

    /** The label of a selected EFP. */
    private JLabel lblEFP;

    /** The SplitPane between efp and derived. */
    private JSplitPane splitEfpAndDerived;

    /** The SplitPane value tree and properties value. */
    private JSplitPane splitValueAndProperties;

    /** Button for view assignment in all GR. */
    private JButton btnGlobalRegister;

    /**
     * Launch the application.
     * @param args Arguments of application
     */
    public static void main(final String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler", ExceptionHandler.class.getName());
        try {
            initLookAndFeel();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            welcome = new WelcomeScreen();
            efpClient = EfpClientImpl.getClient();
            getRegistryGUI();
            List<GR> grs = efpClient.findGrAll();
            if (grs.isEmpty()) {
                administrationGR();
                return;
            }

            if (grs.size() == 1) {
                setSelectedGR(grs.get(0));
                showRegistryGUI();
                return;
            }
            if (grs.size() > 1) {
                changeGR();
            }
        } finally {
            welcome.dispose();
        }

    }

    /**
     * Create frame if not exist or return existing frame.
     * @return new or existing frame
     */
    public static EfpRegistryGUI getRegistryGUI() {
        if (self == null) {
            self = new EfpRegistryGUI();
        }
        return self;
    }

    /**
     * Show Registry GUI.
     * @return new or existing frame
     */
    public static EfpRegistryGUI showRegistryGUI() {
        getRegistryGUI().refresh();
        self.setVisible(true);
        return self;
    }

    /**
     * @return The gr
     */
    public static GR getSelectedGR() {
        if (gr == null) {
            gr = new GR(0, "", "");
        }
        return gr;
    }

    /**
     * @param globalRegister new select GR.
     */
    public static void setSelectedGR(final GR globalRegister) {
        gr = globalRegister;
        if (self == null) {
            getRegistryGUI();
        }
    }

    /**
     * @return The gr
     */
    public static EfpClient getEfpClient() {
        return efpClient;
    }

    /**
     * Show formular for change GR.
     */
    private static void changeGR() {
        new GRChangeGUI();
    }

    /**
     * Show formular for creating and deleting GR.
     */
    private static void administrationGR() {
        new GRAdministrationGUI();
    }

    /**
     * Initialize look&feel.
     * @throws UnsupportedLookAndFeelException Unsupported look and feel.
     * @throws IllegalAccessException Access to look and feel is available.
     * @throws InstantiationException Look and feel cannot use.
     * @throws ClassNotFoundException Look and feel not exists.
     */
    private static void initLookAndFeel() throws UnsupportedLookAndFeelException,
            ClassNotFoundException, InstantiationException, IllegalAccessException {

        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
    }

    /**
     * Create the frame.
     */
    private EfpRegistryGUI() {
        bundle = new EfpLocalization("efpRegistryGUI");
        init();
    }

    /**
     * Refresh data from database.
     */
    public void refresh() {
        gr = efpClient.getGrById(gr.getId());
        btnGlobalRegister.setText("GR: " + gr.getName());
        splitValueAndProperties.setRightComponent(null);
        Object selectedEfp = efpList.getSelectedValue();
        efpList.updateData(efpClient.findEfpForGr(gr.getId()));
        efpList.setSelectedValue(selectedEfp, true);
        if (getSelectedEFP() != null) {
            valueTree.updateData((EFP) getSelectedEFP());
            if (getSelectedEFP().getType() == EfpType.DERIVED) {
                derivedList.updateData(((DerivedEFP) getSelectedEFP()).getEfps());
            }
        } else {
            splitEfpAndDerived.setRightComponent(null);
            derivedList.updateData(null);
            valueTree.updateData(gr);
            lblEFP.setText(gr.getName());
        }
        btnGlobalRegister.requestFocus();
        validate();
        repaint();
    }

    /**
     * Return selected efp in efpList.
     * @return the selected EFP or null if anyone is not selected.
     */
    public EFP getSelectedEFP() {
        return efpList.getSelectedValue() != null ? (EFP) efpList.getSelectedValue()
                : null;
    }

    /**
     * Return selected lrAssignment in valueTree.
     * @return the selected lrAssignment or null if anyone is not selected.
     */
    public LrAssignment getSelectedAssignment() {
        TreePath path = valueTree.getSelectionPath();
        if (path != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getPath()[path
                    .getPath().length - 1];
            if (node.getUserObject() instanceof LrAssignment) {
                return (LrAssignment) node.getUserObject();
            }
        }
        return null;
    }

    /**
     * Return selected LR in ValueTree.
     * If selected value then return its
     * @return Selected LR or null if anyone is not selected.
     */
    public LR getSelectedLR() {
        TreePath path = valueTree.getSelectionPath();
        if (path == null) {
            return null;
        }
        Object[] nodes = path.getPath();
        for (int i = nodes.length - 1; i >= 0; i--) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes[i];
            if (node.getUserObject() instanceof LR) {
                return (LR) node.getUserObject();
            }
        }
        return null;
    }

    /**
     * Initialization GUI.
     */
    private void init() {
        setTitle(bundle.getProperty("caption"));
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        setJMenuBar(menuBar());

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                Object[] options = new Object[] {bundle.getProperty("yes"),
                        bundle.getProperty("no")};
                int value = JOptionPane.showOptionDialog(null,
                        bundle.getProperty("quit_quest"), "Quit a application",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                        options, options[1]);

                if (value == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }

            }
        });

        JPanel contentPane = new JPanel();
        contentPane.setAlignmentY(0.0f);
        contentPane.setAlignmentX(0.0f);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));

        JSplitPane splitEfpAndValues = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                panelLeft(), panelRight());
        splitEfpAndValues.setResizeWeight(0.5);
        contentPane.add(splitEfpAndValues, BorderLayout.CENTER);
        setContentPane(contentPane);
        btnGlobalRegister.requestFocus();
    }

    /**
     * Create menuBar.
     * @return menuBar
     */
    private JMenuBar menuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu mnGR = new JMenu(bundle.getProperty("GR"));
        menuBar.add(mnGR);

        JMenuItem mntmChangeGlobalRegister = new JMenuItem(bundle.getProperty("changeGR"));
        mntmChangeGlobalRegister.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                changeGR();
            }
        });
        mnGR.add(mntmChangeGlobalRegister);

        JMenuItem mntmAdministrationGR = new JMenuItem(
                bundle.getProperty("administrationGR"));
        mntmAdministrationGR.setIcon(new ImageIcon(getClass().getClassLoader()
                .getResource("images/GR_administration.png")));
        mntmAdministrationGR.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                administrationGR();
            }
        });
        mnGR.add(mntmAdministrationGR);

        JMenu mnNames = new JMenu(bundle.getProperty("valueNames"));
        menuBar.add(mnNames);

        JMenuItem mntmValueNames = new JMenuItem(bundle.getProperty("administrationVN"));
        mntmValueNames.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/names_administration.png")));
        mntmValueNames.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                valueNames();
            }
        });
        mnNames.add(mntmValueNames);

        menuBar.add(Box.createHorizontalGlue());

        Icon image = new ImageIcon(getClass().getClassLoader().getResource(
                "images/refresh.png"));

        JButton refresh = new JButton(image);
        refresh.setMargin(new Insets(0, 0, 0, 0));
        refresh.setBorderPainted(false);
        refresh.setToolTipText(bundle.getProperty("refresh"));
        refresh.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                refresh();
            }
        });
        menuBar.add(refresh);
        menuBar.add(Box.createHorizontalStrut(10));
        return menuBar;
    }

    /**
     * Create left panel of SplitPane.
     * @return left panel
     */
    private Container panelLeft() {
        JPanel panelL = new JPanel();
        panelL.setLayout(new BorderLayout(0, 0));
        btnGlobalRegister = new JButton();
        btnGlobalRegister.requestFocus();
        btnGlobalRegister.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/view.png")));
        btnGlobalRegister.setText("GR: " + getSelectedGR().getName());
        btnGlobalRegister.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                /* Choosing all in global register */
                efpList.clearSelection();
                derivedList.clearSelection();
                valueTree.updateData(getSelectedGR());
                lblEFP.setText(getSelectedGR().getName());
                splitValueAndProperties.setRightComponent(null);
            }
        });
        panelL.add(btnGlobalRegister, BorderLayout.NORTH);

        JPanel panelEfps = new JPanel();
        panelL.add(panelEfps, BorderLayout.CENTER);
        panelEfps.setLayout(new BorderLayout(0, 0));

        final JScrollPane scrollPaneDerived = new JScrollPane();
        derivedList = new JefpList(null);
        derivedList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                efpList.setSelectedValue(derivedList.getSelectedValue(), true);
                valueTree.updateData(getSelectedEFP());
                lblEFP.setText(getSelectedEFP().getName());
                derivedList.clearSelection();
                if (getSelectedEFP().getType() == EfpType.DERIVED) {
                    derivedList.updateData(((DerivedEFP) getSelectedEFP()).getEfps());
                    splitEfpAndDerived.setRightComponent(derivedList);
                } else {
                    derivedList.updateData(null);
                    splitEfpAndDerived.setRightComponent(null);
                }
            }
        });
        scrollPaneDerived.setViewportView(derivedList);

        JScrollPane scrollPaneEfps = new JScrollPane();
        efpList = new JefpList(efpClient.findEfpForGr(getSelectedGR().getId()));
        efpList.setSelectedIndex(-1);
        efpList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                if (getSelectedEFP() == null) {
                    return;
                }
                splitValueAndProperties.setRightComponent(null);
                valueTree.updateData(getSelectedEFP());
                lblEFP.setText(getSelectedEFP().getName());
                derivedList.clearSelection();
                if (getSelectedEFP().getType() == EfpType.DERIVED) {
                    derivedList.updateData(((DerivedEFP) getSelectedEFP()).getEfps());
                    splitEfpAndDerived.setRightComponent(scrollPaneDerived);
                    splitEfpAndDerived.setDividerLocation(splitEfpAndDerived.getHeight() / 2);
                } else {
                    derivedList.updateData(null);
                    splitEfpAndDerived.setRightComponent(null);
                }

            }

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editEfp(getSelectedEFP());
                }
            }
        });
        scrollPaneEfps.setViewportView(efpList);
        splitEfpAndDerived = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPaneEfps,
                null);
        splitEfpAndDerived.setResizeWeight(0.5);
        panelEfps.add(splitEfpAndDerived, BorderLayout.CENTER);
        panelL.add(panelLeftControl(), BorderLayout.SOUTH);

        return panelL;
    }

    /**
     * Create control panel of EFPs.
     * @return Control panel of EFPs
     */
    private Container panelLeftControl() {
        JPanel ctrlPanelEfps = new JPanel();

        JButton btnNewEfp = new JButton(bundle.getProperty("newEfp"));
        btnNewEfp.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/create.png")));
        btnNewEfp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                newEfp();
            }
        });
        ctrlPanelEfps.add(btnNewEfp);

        JButton btnEditEfp = new JButton(bundle.getProperty("editEfp"));
        btnEditEfp.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/edit.png")));
        btnEditEfp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedEFP() != null) {
                    editEfp(getSelectedEFP());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_EFP"));
                }
            }
        });
        ctrlPanelEfps.add(btnEditEfp);

        JButton btnDeleteEfp = new JButton(bundle.getProperty("deleteEfp"));
        btnDeleteEfp.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/delete.png")));
        btnDeleteEfp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedEFP() != null) {
                    deleteEfp(getSelectedEFP());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_EFP"));
                }
            }
        });
        ctrlPanelEfps.add(btnDeleteEfp);
        return ctrlPanelEfps;
    }

    /**
     * Create right panel of SplitPane.
     * @return right panel
     */
    private Container panelRight() {
        JPanel panelR = new JPanel();

        panelR.setLayout(new BorderLayout(0, 0));

        valueTree = new JValueTree(getSelectedGR());
        valueTree.setRootVisible(false);
        valueTree.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                if (getSelectedAssignment() != null) {
                    splitValueAndProperties.setRightComponent(new AssignmentLrGUI(
                            getSelectedAssignment()));
                } else {
                    splitValueAndProperties.setRightComponent(null);
                }
            }

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && getSelectedAssignment() != null) {
                    editAssignment(getSelectedAssignment());
                }
            }
        });

        JScrollPane scrollPaneR = new JScrollPane();
        scrollPaneR.setViewportView(valueTree);

        lblEFP = new JLabel(getSelectedGR().getName());
        panelR.add(lblEFP, BorderLayout.NORTH);
        splitValueAndProperties = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPaneR,
                null);
        splitValueAndProperties.setResizeWeight(0.5);
        panelR.add(splitValueAndProperties, BorderLayout.CENTER);
        panelR.add(panelRightControl(), BorderLayout.SOUTH);

        return panelR;
    }

    /**
     * Create control panel of values.
     * @return Control panel of values
     */
    private Container panelRightControl() {
        JPanel ctrlPanelValues = new JPanel();
        JPanel efpPanel = new JPanel();
        JPanel lrPanel = new JPanel();
        ctrlPanelValues.setLayout(new GridBagLayout());

        // efps
        JButton btnNewValue = new JButton(bundle.getProperty("newAssignment"));
        btnNewValue.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/create.png")));
        btnNewValue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                newAssignment();
            }
        });
        efpPanel.add(btnNewValue);

        JButton btnEditValue = new JButton(bundle.getProperty("editAssignment"));
        btnEditValue.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/edit.png")));
        btnEditValue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedAssignment() != null) {
                    editAssignment(getSelectedAssignment());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_ASSIGNMNET"));
                }
            }
        });
        efpPanel.add(btnEditValue);

        JButton btnDeleteValue = new JButton(bundle.getProperty("deleteAssignment"));
        btnDeleteValue.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/delete.png")));
        btnDeleteValue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedAssignment() != null) {
                    deleteAssignment(getSelectedAssignment());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_ASSIGNMNET"));
                }
            }
        });
        efpPanel.add(btnDeleteValue);

        // lrs
        JButton btnNewLr = new JButton(bundle.getProperty("newLR"));
        btnNewLr.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_create.png")));
        btnNewLr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                newLR();
            }
        });
        lrPanel.add(btnNewLr);

        JButton btnEditLr = new JButton(bundle.getProperty("editLR"));
        btnEditLr.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_edit.png")));
        btnEditLr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedLR() != null) {
                    editLR(getSelectedLR());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_LR"));
                }
            }
        });
        lrPanel.add(btnEditLr);

        JButton btnDeleteLr = new JButton(bundle.getProperty("deleteLR"));
        btnDeleteLr.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_delete.png")));
        btnDeleteLr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (getSelectedLR() != null) {
                    deleteLR(getSelectedLR());
                } else {
                    throw new WrongInputException(bundle
                            .getProperty("ERR_NO_SELECTED_LR"));
                }
            }
        });
        lrPanel.add(btnDeleteLr);

        // add panels
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(0, 1, 0, 0);
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;

        gbc.gridx = 0;
        gbc.gridy = 0;
        ctrlPanelValues.add(efpPanel, gbc);

        gbc.gridy = 1;
        ctrlPanelValues.add(lrPanel, gbc);

        return ctrlPanelValues;
    }

    /**
     * Delete local register.
     * @param selectedLR Lr which will be deleted.
     */
    private void deleteLR(final LR selectedLR) {
        List<LrAssignment> list = getEfpClient().findLrAssignments(selectedLR.getId());

        if (!list.isEmpty()) {
            throw new ValueUsedException(bundle.getProperty("ERR_DEL_LR_A"));
        }

        List<LR> listLr = getEfpClient().findLrForGr(getSelectedGR());
        for (LR lr : listLr) {
            if (selectedLR.equals(lr.getParentLR())) {
                throw new ValueUsedException(bundle.getProperty("ERR_DEL_LR_B"));
            }
        }
        // confirm
        Object[] options = new Object[] {bundle.getProperty("yes"),
                bundle.getProperty("no")};
        int value = JOptionPane.showOptionDialog(null, bundle.getProperty("del_LR_quest")
                + " " + selectedLR.getName() + "?", "Delete local register",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
                options[1]);

        if (value != JOptionPane.YES_OPTION) {
            return;
        }
        // end confirm
        getEfpClient().deleteLr(selectedLR.getId());

        logger.info("Delete LR:" + selectedLR);

        refresh();
    }

    /**
     * Show dialog for change local register.
     * @param selectedLR Lr which will be edited.
     */
    private void editLR(final LR selectedLR) {
        new LrGUI(selectedLR);
    }

    /**
     * Show dialog for create local register.
     */
    private void newLR() {
        new LrGUI();
    }

    /**
     * Return true if exists a assignment for efp.
     * @param efp a efp.
     * @return true if exists.
     */
    private boolean isExistsAssignmentForEfp(final EFP efp) {
        for (LR lr : getEfpClient().findLrForGr(getSelectedGR())) {
            if (!getEfpClient().findLrAssignments(efp.getId(), lr.getId()).isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return true if exists depending on derived efp.
     * @param efp a efp.
     * @return true if exists.
     */
    private boolean isExistsDependingOnDerived(final EFP efp) {
        for (EFP e : getEfpClient().findEfpForGr(getSelectedGR().getId())) {
            if (e.getType().equals(EfpType.DERIVED)) {
                if (((DerivedEFP) e).getEfps().contains(efp)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Show a form for editing efp.
     * @param efp Editing efp
     */
    private void editEfp(final EFP efp) {
        EfpGUI editing = new EfpGUI(efp);
        editing.setVisible(true);
    }

    /**
     * Show a form for creating efp.
     */
    private void newEfp() {
        EfpGUI creating = new EfpGUI();
        creating.setVisible(true);
    }

    /**
     * Delete efp.
     * @param efp Deleting efp
     */
    private void deleteEfp(final EFP efp) {
        if (isExistsAssignmentForEfp(efp)) {
            throw new ValueUsedException(bundle.getProperty("ERR_DEL_ASSIGNMENT"));
        }
        if (isExistsDependingOnDerived(efp)) {
            throw new ValueUsedException(bundle.getProperty("ERR_DEL_DERIVED"));
        }

        // confirm
        Object[] options = new Object[] {bundle.getProperty("yes"),
                bundle.getProperty("no")};
        int value = JOptionPane.showOptionDialog(null,
                bundle.getProperty("del_EFP_quest") + " " + efp.getName() + "?",
                "Delete extra-functional property", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

        if (value != JOptionPane.YES_OPTION) {
            return;
        }
        // end confirm

        efpClient.deleteEfp(efp);
        logger.info("Delete EFP:" + efp);
        refresh();
    }

    /**
     * Show a form for creating new assignment.
     */
    private void newAssignment() {
        JFrame frame = new JFrame();
        new AssignmentLrGUI(frame);
        frame.setVisible(true);
    }

    /**
     * Show a form for edited existing assignment.
     * @param assignment Edited assignment value
     */
    private void editAssignment(final LrAssignment assignment) {

        JFrame frame = new JFrame();
        new AssignmentLrGUI(frame, assignment);
        frame.setVisible(true);
    }

    /**
     * Delete assignment value.
     * @param assignment Deleting assignment value
     */
    private void deleteAssignment(final LrAssignment assignment) {
        // confirm
        Object[] options = new Object[] {bundle.getProperty("yes"),
                bundle.getProperty("no")};
        int value = JOptionPane.showOptionDialog(null,
                bundle.getProperty("del_assignLr_quest"),
                "Delete value of extra-functional property", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

        if (value != JOptionPane.YES_OPTION) {
            return;
        }
        // end confirm
        efpClient.delete(assignment);
        logger.info("Delete LrAssignment:" + assignment);
        refresh();
    }

    /**
     * Show formular for creating and deleting value names.
     */
    private void valueNames() {
        new EfpValueNames();
    }

}
