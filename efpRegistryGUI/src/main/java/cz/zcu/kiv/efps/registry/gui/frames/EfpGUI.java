package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import cz.zcu.kiv.efps.registry.gui.components.JefpList;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.models.EfpListModel;
import cz.zcu.kiv.efps.registry.gui.renderers.ValueNameListCellRenderer;
import cz.zcu.kiv.efps.registry.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;

/**
 * The frame for creating or editing EFP. <br>
 * Date: 24. 10. 2010
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpGUI extends JFrame {

    /** The constant expressing the valid EFP name. */
    private static final String VALID_EFP_NAME = "\\w+";

    /** Serial. */
    private static final long serialVersionUID = -5639921687444310872L;

    /** Width of frame. */
    private static final int WIDTH = 600;

    /** Height of frame. */
    private static final int HEIGHT = 500;

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(EfpGUI.class);

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("efpGUI");

    /** Variable of this formular. */
    private final EfpGUI self;

    /** Editing efp. */
    private EFP efp;

    /** Content pane of frame. */
    private JPanel contentPane;

    /** Name of EFP. */
    private JTextField tfName;

    /** Gamma function of EFP. */
    private JTextField tfGamma;

    /** ValueType of EFP. */
    private JComboBox cmbValueType;

    /** Unit of Meta EFP. */
    private JTextField tfUnit;

    /** Button for add names. */
    private JButton btnAddNames;

    /** Button for remove names. */
    private JButton btnRemoveNames;

    /** ComboBox of names. */
    private JComboBox cmbNames;

    /** List of efp's names. */
    private List<String> lstNames;

    /** List of all efp's names. */
    private List<String> lstAllNames;

    /** Names of Meta EFP separated by comma. */
    private JList names;

    /** ComboBox of type value of efp. */
    private JComboBox cmbTypeEfp;

    /** Button which execute operation. */
    private JButton btnSaveCreate;

    /** ComboBox of EFP which could be derived. */
    private JComboBox cmbDerived;

    /** Button which add EFP from comboBox to list of derived Efps. */
    private JButton btnAddDerived;

    /** Button which remove EFP from list of derived Efps. */
    private JButton btnRemoveDerived;

    /** ScrollPane of efps list. */
    private JScrollPane spDerived;

    /** List of Efps which extends Derived Efps. */
    private JefpList lstDerived;

    /** True if edit assignment. */
    private boolean edit;

    /** Button for showing gamma editor. */
    private JButton btnGamma;

    /** Dialog of editing gamma function. */
    private EfpGammaDialog efpGammaDialog;

    /**
     * Create the clear frame.
     */
    public EfpGUI() {
        this.setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setTitle(bundle.getProperty("create"));

        efp = null;
        edit = false;
        self = this;
        init();
        setVisible(true);
    }

    /**
     * Create the frame with EFP.
     * @param efp Editing EFP
     */
    public EfpGUI(final EFP efp) {
        this.setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setTitle(bundle.getProperty("edit"));

        edit = true;
        this.efp = efp;
        self = this;
        init();
        load();
        setVisible(true);
    }

    /**
     * Initialization components.
     */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                Object[] options = new Object[] {bundle.getProperty("yes"),
                        bundle.getProperty("no"), bundle.getProperty("cancel")};
                int value = JOptionPane.showOptionDialog(null,
                        bundle.getProperty("close_quest"),
                        "Save an extra-functional property",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);

                if (value == JOptionPane.YES_OPTION) {
                    if (edit) {
                        editEfp();
                    } else {
                        createEfp();
                    }
                }

                if (value == JOptionPane.NO_OPTION) {
                    self.dispose();
                }

            }

            @Override
            public void windowClosed(final WindowEvent e) {
                if (efpGammaDialog != null) {
                    efpGammaDialog.dispose();
                }
            }
        });
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new FormLayout(new ColumnSpec[] {
                FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("max(53dlu;default)"), FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("max(50dlu;default):grow"),
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC},
                new RowSpec[] {FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC}));

        addNameRow();
        addTypeOfValueRow();
        addGammaRow();
        addUnitRow();
        addNamesRow();
        addTypeRow();
        addDerivedRow();
        addControlRow();
    }

    /**
     * Add to form row with name efp.
     */
    private void addNameRow() {
        JLabel lblName = new JLabel(bundle.getProperty("lblName"));
        contentPane.add(lblName, "3, 4");

        tfName = new JTextField();
        tfName.setColumns(10);
        contentPane.add(tfName, "5, 4, 5, 1, fill, center");
    }

    /**
     * Add to form row with value type efp.
     */
    private void addTypeOfValueRow() {
        cmbValueType = new JComboBox();
        if (!edit) {
            cmbValueType
                    .setModel(new DefaultComboBoxModel(EfpValueGUI.getNamesOfValues()));
        } else {
            cmbValueType.setModel(new DefaultComboBoxModel(new String[] {EfpValueGUI
                    .getNameOfType(efp.getValueType())}));
        }
        cmbValueType.setSelectedIndex(0);
        contentPane.add(cmbValueType, "5, 6, 5, 1, fill, default");
    }

    /**
     * Add to form row with gamma of efp.
     */
    private void addGammaRow() {
        JLabel lblGamma = new JLabel(bundle.getProperty("lblGamma"));
        contentPane.add(lblGamma, "3, 8");

        tfGamma = new JTextField();
        tfGamma.setEditable(false);
        contentPane.add(tfGamma, "5, 8, 3, 1, fill, default");
        tfGamma.setColumns(10);

        btnGamma = new JButton("...");
        btnGamma.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent event) {
                efpGammaDialog = new EfpGammaDialog(tfGamma, EfpValueGUI
                        .getTypeByName(cmbValueType.getSelectedItem().toString()));
            }
        });
        contentPane.add(btnGamma, "9, 8, right, default");
    }

    /**
     * Add to form row with unit of efp.
     */
    private void addUnitRow() {
        JLabel lblUnit = new JLabel(bundle.getProperty("lblUnit"));
        contentPane.add(lblUnit, "3, 10");

        tfUnit = new JTextField();
        contentPane.add(tfUnit, "5, 10, 5, 1, fill, default");
        tfUnit.setColumns(10);
    }

    /**
     * Add to form row with value names of efp.
     */
    private void addNamesRow() {
        JLabel lblNames = new JLabel(bundle.getProperty("lblNames"));
        contentPane.add(lblNames, "3, 12");

        lstNames = new ArrayList<String>();
        lstAllNames = new ArrayList<String>();
        lstAllNames = EfpRegistryGUI.getEfpClient().getAllValueNames(
                EfpRegistryGUI.getSelectedGR().getId());
        cmbNames = new JComboBox();
        cmbNames.setModel(new DefaultComboBoxModel(lstAllNames.toArray()));

        contentPane.add(cmbNames, "5, 12, 3, 1, fill, default");

        btnAddNames = new JButton(bundle.getProperty("btnAdd"));
        btnAddNames.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/add.png")));
        btnAddNames.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                addNames();
            }
        });
        contentPane.add(btnAddNames, "9, 12, center, default");

        btnRemoveNames = new JButton(bundle.getProperty("btnRemove"));
        btnRemoveNames.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/remove.png")));
        btnRemoveNames.setEnabled(false);
        btnRemoveNames.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                removeNames();
            }
        });
        contentPane.add(btnRemoveNames, "9, 14, center, default");

        names = new JList();
        names.setCellRenderer(new ValueNameListCellRenderer());
        names.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        names.addKeyListener(new KeyAdapter() {

            @Override
            public void keyTyped(final KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_DELETE) {
                    removeNames();
                }
            }
        });
        names.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                if (names.getSelectedValue() != null) {
                    btnRemoveNames.setEnabled(true);
                } else {
                    btnRemoveNames.setEnabled(false);
                }
            }
        });
        JScrollPane scrollName = new JScrollPane();
        scrollName.setViewportView(names);
        contentPane.add(scrollName, "5, 14, 3, 1, fill, default");
    }

    /**
     * Add to form row with value type of efp.
     */
    private void addTypeRow() {
        JLabel lblTypeOfEfp = new JLabel(bundle.getProperty("lblTypeOfEfp"));
        contentPane.add(lblTypeOfEfp, "3, 16");

        cmbTypeEfp = new JComboBox();
        if (!edit) {
            cmbTypeEfp.setModel(new DefaultComboBoxModel(EFP.EfpType.values()));
        } else {
            cmbTypeEfp.setModel(new DefaultComboBoxModel(new String[] {efp.getType()
                    .toString()}));
        }
        cmbTypeEfp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (cmbTypeEfp.getSelectedItem().equals(EFP.EfpType.DERIVED)) {
                    setVisibleDerived(true);

                } else {
                    setVisibleDerived(false);
                }
            }
        });
        contentPane.add(cmbTypeEfp, "5, 16, 5, 1, fill, default");
    }

    /**
     * Add to form row with save button.
     */
    private void addControlRow() {
        JPanel panel = new JPanel();
        btnSaveCreate = new JButton();
        btnSaveCreate.setText(getTitle());
        if (edit) {
            btnSaveCreate.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                    "images/edit.png")));
        } else {
            btnSaveCreate.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                    "images/create.png")));
        }
        btnSaveCreate.setMargin(new Insets(5, 5, 5, 5));
        btnSaveCreate.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (efp == null) {
                    createEfp();
                } else {
                    editEfp();
                }
            }
        });
        panel.add(btnSaveCreate);

        JButton btnStorno = new JButton();
        btnStorno.setText(bundle.getProperty("storno"));
        btnStorno.setMargin(new Insets(5, 5, 5, 5));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        panel.add(btnStorno);
        contentPane.add(panel, "1, 28, 11, 1");
    }

    /**
     * Initialization components for derived efp.
     */
    private void addDerivedRow() {
        List<String> efpsName = new ArrayList<String>();
        for (EFP ef : EfpRegistryGUI.getEfpClient().findEfpForGr(
                EfpRegistryGUI.getSelectedGR().getId())) {
            efpsName.add(ef.getName());
        }
        cmbDerived = new JComboBox(new DefaultComboBoxModel(efpsName.toArray()));
        contentPane.add(cmbDerived, "5, 18, 3, 1, fill, default");
        btnAddDerived = new JButton(bundle.getProperty("btnAdd"));
        btnAddDerived.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/add.png")));
        btnAddDerived.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                addDerived();
            }
        });

        contentPane.add(btnAddDerived, "9, 18, center, default");

        btnRemoveDerived = new JButton(bundle.getProperty("btnRemove"));
        btnRemoveDerived.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/remove.png")));
        btnRemoveDerived.setEnabled(false);
        btnRemoveDerived.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                removeDerived();
            }
        });
        contentPane.add(btnRemoveDerived, "9, 20, center, default");

        lstDerived = new JefpList(new ArrayList<EFP>());
        lstDerived.addKeyListener(new KeyAdapter() {

            @Override
            public void keyTyped(final KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_DELETE) {
                    removeDerived();
                }
            }
        });
        lstDerived.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                if (lstDerived.getSelectedValue() != null) {
                    btnRemoveDerived.setEnabled(true);
                } else {
                    btnRemoveDerived.setEnabled(false);
                }
            }
        });
        spDerived = new JScrollPane();
        spDerived.setViewportView(lstDerived);
        contentPane.add(spDerived, "3, 20, 4, 1, fill, fill");
        setVisibleDerived(false);
    }

    /**
     * Create EFP.
     */
    private void createEfp() {
        Class<?> typeValueEfp = null;
        String nameEfp = tfName.getText();

        Meta metaEfp;
        if (!lstNames.isEmpty()) {
            String[] arrNames = new String[lstNames.size()];
            arrNames = lstNames.toArray(arrNames);
            metaEfp = new Meta(tfUnit.getText(), arrNames);
        } else {
            metaEfp = new Meta(tfUnit.getText());
        }

        Gamma gammaEfp;
        if (!tfGamma.getText().equals("")) {
            gammaEfp = new UserGamma(tfGamma.getText());
        } else {
            gammaEfp = null;
        }

        if (nameEfp.equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }

        if (!nameEfp.matches(VALID_EFP_NAME)) {
            throw new WrongInputException(bundle.getProperty("ERR_INVALID_NAME"));
        }

        /* if (metaEfp.getNames().get(0).isEmpty()) {
         * throw new WrongInputException(bundle.getProperty("ERR_NO_NAME_VALUES"));
         * return;
         * } */

        if (isExistNameEfp(nameEfp)) {
            throw new WrongInputException(bundle.getProperty("ERR_NAME_EFP_EXISTS"));
        }

        GR grEfp = EfpRegistryGUI.getSelectedGR();

        typeValueEfp = EfpValueGUI.getTypeByName(cmbValueType.getSelectedItem()
                .toString());

        if (cmbTypeEfp.getSelectedItem().equals(EFP.EfpType.SIMPLE)) {
            efp = new SimpleEFP(nameEfp, typeValueEfp, metaEfp, gammaEfp, grEfp);
        }
        if (cmbTypeEfp.getSelectedItem().equals(EFP.EfpType.DERIVED)) {
            List<EFP> list = ((EfpListModel) lstDerived.getModel()).getData();
            EFP[] efpsEfp = new EFP[list.size()];
            list.toArray(efpsEfp);
            efp = new DerivedEFP(nameEfp, typeValueEfp, metaEfp, gammaEfp, grEfp, efpsEfp);
        }
        EfpRegistryGUI.getEfpClient().create(efp);

        logger.info("Create EFP:" + efp);

        EfpRegistryGUI.getRegistryGUI().refresh();
        this.dispose();
    }

    /**
     * Add derived names combobox to list.
     */
    private void addNames() {
        if (cmbNames.getSelectedItem() == null) {
            return;
        }
        lstNames.add(cmbNames.getSelectedItem().toString());
        names.setListData(lstNames.toArray());

        cmbNames.removeItem(cmbNames.getSelectedItem());
        if (cmbNames.getItemCount() == 0) {
            btnAddNames.setEnabled(false);
            cmbNames.setEnabled(false);
        }
        btnRemoveNames.setEnabled(false);
    }

    /**
     * Add derived efp from combobox to list.
     */
    private void addDerived() {
        if (!cmbDerived.isEnabled()) {
            return;
        }
        List<EFP> efps = ((EfpListModel) lstDerived.getModel()).getData();
        EFP efp;
        efp = EfpRegistryGUI.getEfpClient().findEfpByName(
                (String) cmbDerived.getSelectedItem(),
                EfpRegistryGUI.getSelectedGR().getId());

        if (!efps.contains(efp)) {
            efps.add(efp);
            lstDerived.updateData(efps);
            cmbDerived.removeItem(cmbDerived.getSelectedItem());
            if (cmbDerived.getItemCount() == 0) {
                cmbDerived.setEnabled(false);
                btnAddDerived.setEnabled(false);
            } else {
                cmbDerived.setSelectedIndex(0);
            }
        }
        btnRemoveDerived.setEnabled(false);
    }

    /**
     * Remove selected names from list.
     */
    private void removeNames() {
        if (names.getSelectedValue() == null) {
            return;
        }
        String value = (String) names.getSelectedValue();
        lstNames.remove(value);
        names.setListData(lstNames.toArray());

        cmbNames.addItem(value);
        if (cmbNames.getItemCount() != 0) {
            btnAddNames.setEnabled(true);
            cmbNames.setEnabled(true);
        }
        btnRemoveNames.setEnabled(false);

    }

    /**
     * Remove selected derived efp from list.
     */
    private void removeDerived() {
        List<EFP> efps = ((EfpListModel) lstDerived.getModel()).getData();
        efps.remove(lstDerived.getSelectedValue());
        cmbDerived.addItem(((EFP) lstDerived.getSelectedValue()).getName());
        lstDerived.updateData(efps);
        if (cmbDerived.getItemCount() > 0) {
            cmbDerived.setEnabled(true);
            btnAddDerived.setEnabled(true);
        }
        btnRemoveDerived.setEnabled(false);

    }

    /**
     * Edit EFP.
     */
    private void editEfp() {
        EFP oldEfp = efp;
        Class<?> valueTypeEfp = efp.getValueType();
        String nameEfp = tfName.getText();

        Meta metaEfp;
        if (!lstNames.isEmpty()) {
            String[] arrNames = new String[lstNames.size()];
            arrNames = lstNames.toArray(arrNames);
            metaEfp = new Meta(tfUnit.getText(), arrNames);
        } else {
            metaEfp = new Meta(tfUnit.getText());
        }

        Gamma gammaEfp;
        if (!tfGamma.getText().equals("")) {
            gammaEfp = new UserGamma(tfGamma.getText());
        } else {
            gammaEfp = null;
        }

        if (nameEfp.equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }

        if (!nameEfp.matches(VALID_EFP_NAME)) {
            throw new WrongInputException(bundle.getProperty("ERR_INVALID_NAME"));
        }

        /* if (metaEfp.getNames().get(0).isEmpty()) {
         * throw new WrongInputException(bundle.getProperty("ERR_NO_NAME_VALUES"));
         * return;
         * } */

        if (!nameEfp.equals(efp.getName()) && isExistNameEfp(nameEfp)) {
            throw new WrongInputException(bundle.getProperty("ERR_NAME_EFP_EXISTS"));
        }
        if (efp.getType().equals(EFP.EfpType.SIMPLE)) {
            efp = new SimpleEFP(efp.getId(), nameEfp, valueTypeEfp, metaEfp, gammaEfp,
                    efp.getGr());
        }
        if (efp.getType().equals(EFP.EfpType.DERIVED)) {
            List<EFP> list = ((EfpListModel) lstDerived.getModel()).getData();
            EFP[] efpsEfp = new EFP[list.size()];
            list.toArray(efpsEfp);
            efp = new DerivedEFP(efp.getId(), nameEfp, valueTypeEfp, metaEfp, gammaEfp,
                    efp.getGr(), efpsEfp);
        }
        EfpRegistryGUI.getEfpClient().update(efp);

        logger.info("Edit EFP FROM:" + oldEfp + " TO:" + efp);

        EfpRegistryGUI.getRegistryGUI().refresh();
        this.dispose();
    }

    /**
     * Set visible of the derived options.
     * @param bool True - components will be visible.
     */
    private void setVisibleDerived(final boolean bool) {
        cmbDerived.setVisible(bool);
        spDerived.setVisible(bool);
        btnAddDerived.setVisible(bool);
        btnRemoveDerived.setVisible(bool);
    }

    /**
     * Load EFP to the form.
     */
    private void load() {
        /* name */
        tfName.setText(efp.getName());

        /* value type */
        cmbValueType.setSelectedItem(EfpValueGUI.getNameOfType(efp.getValueType()));

        /* names of value */
        if (!efp.getMeta().getNames().isEmpty()) {
            lstNames = new ArrayList<String>(efp.getMeta().getNames());
            names.setListData(lstNames.toArray());
        }

        /* unit */
        if (efp.getMeta().getUnit() != null) {
            tfUnit.setText(efp.getMeta().getUnit());
        }

        List<String> nonUseNames = new ArrayList<String>(lstAllNames);
        nonUseNames.removeAll(lstNames);
        cmbNames.setModel(new DefaultComboBoxModel(nonUseNames.toArray()));
        if (cmbNames.getItemCount() == 0) {
            btnAddNames.setEnabled(false);
            cmbNames.setEnabled(false);
        }

        /* type of efp */
        cmbTypeEfp.setSelectedItem(efp.getType());
        if (efp.getGamma() != null) {
            tfGamma.setText(((UserGamma) efp.getGamma()).getFunction());
        }

        if (efp.getType().equals(EFP.EfpType.DERIVED)) {
            lstDerived.updateData(((DerivedEFP) efp).getEfps());
            cmbDerived.setVisible(true);
            for (EFP e : ((DerivedEFP) efp).getEfps()) {
                cmbDerived.removeItem(e.getName());
            }
            cmbDerived.removeItem(efp.getName());
            if (cmbDerived.getItemCount() == 0) {
                cmbDerived.setEnabled(false);
            } else {
                cmbDerived.setSelectedIndex(0);
            }
            spDerived.setVisible(true);
            btnAddDerived.setVisible(true);
            btnRemoveDerived.setVisible(true);
        }

    }

    /**
     * Return true if name efp exists.
     * @param name name of efp
     * @return efp exist?
     */
    private boolean isExistNameEfp(final String name) {
        if (EfpRegistryGUI.getEfpClient().findEfpByName(name,
                EfpRegistryGUI.getSelectedGR().getId()) == null) {
            return false;
        }
        return true;
    }

}
