package cz.zcu.kiv.efps.registry.gui.exceptions;

import java.util.Arrays;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class catch uncaught exception and show it on the GUI screen. <br>
 * <br>
 * <b>Date:</b> 10.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    /** Maximum rows of stack trace which will showed on screen. */
    private static final int MAX_TRACE_ON_SCREEN = 20;

    @Override
    public void uncaughtException(final Thread t, final Throwable e) {
        if (SwingUtilities.isEventDispatchThread()) {
            showException(t, e);
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    showException(t, e);
                }
            });
        }
    }

    /**
     * UncaughtException for modal dialogs.
     * @param t Throwable.
     */
    public void handle(final Throwable t) {
        uncaughtException(Thread.currentThread(), t);
    }

    /**
     * Show uncaught exception to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showException(final Thread t, final Throwable e) {
        if (e instanceof WrongInputException) {
            showInputException(t, e);
            return;
        }
        if (e instanceof ValueUsedException) {
            showUsedException(t, e);
            return;
        }

        showFatalException(t, e);
    }

    /**
     * Show WrongInputException to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showInputException(final Thread t, final Throwable e) {
        logger.warn("Input exception: ", e);
        JOptionPane.showMessageDialog(null, e.getMessage(), "Wrong input",
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Show WrongInputException to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showUsedException(final Thread t, final Throwable e) {
        logger.warn("Try delete used object: ", e);
        JOptionPane.showMessageDialog(null, e.getMessage(), "Used value",
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Show uncaught exception to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showFatalException(final Thread t, final Throwable e) {
        logger.error("NO EXPECTED ERROR:", e);
        JOptionPane.showMessageDialog(null,
                Arrays.copyOfRange(e.getStackTrace(), 0, MAX_TRACE_ON_SCREEN),
                "FATAL ERROR", JOptionPane.ERROR_MESSAGE);
    }

}
