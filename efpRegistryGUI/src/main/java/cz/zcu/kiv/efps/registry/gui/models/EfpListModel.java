package cz.zcu.kiv.efps.registry.gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The ListModel of EFPs <br>
 * Date: 13. 10. 2010
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpListModel extends AbstractListModel {

    /** Serial. */
    private static final long serialVersionUID = 3591408518173108725L;

    /** List of efps. */
    private final List<EFP> efps;

    /**
     * Create new model by list of efps.
     * @param efps List of efps
     */
    public EfpListModel(final List<EFP> efps) {
        this.efps = efps;
    }

    @Override
    public final Object getElementAt(final int index) {
        return efps.get(index);
    }

    @Override
    public final int getSize() {
        return efps.size();
    }

    /**
     * Return a data of list.
     * @return data of list
     */
    public final List<EFP> getData() {
        return new ArrayList<EFP>(efps);
    }

}
