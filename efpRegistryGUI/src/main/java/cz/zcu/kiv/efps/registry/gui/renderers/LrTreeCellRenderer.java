package cz.zcu.kiv.efps.registry.gui.renderers;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import cz.zcu.kiv.efps.types.lr.LR;

/**
 * TreeCellRenderer renderer of LR. <br>
 * <br>
 * <b>Date:</b> 22.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class LrTreeCellRenderer extends DefaultTreeCellRenderer {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value,
            final boolean selected, final boolean expanded, final boolean leaf,
            final int row, final boolean hasFocus) {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree,
                ((LR) node.getUserObject()).getName(), selected, expanded, leaf, row,
                hasFocus);
        label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LrIcon.png")));
        return label;

    }
}
