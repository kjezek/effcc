package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import cz.zcu.kiv.efps.registry.gui.components.JefpList;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.renderers.EfpListCellRenderer;
import cz.zcu.kiv.efps.registry.gui.renderers.ValueNameListCellRenderer;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for creating and editing formla of logical function. <br>
 * <br>
 * <b>Date:</b> 2.1.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpFormulaDialog extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7802863719039960317L;

    /** Width of frame. */
    private static final int WIDTH = 700;

    /** Height of frame. */
    private static final int HEIGHT = 500;

    /** Width of columns with lists. */
    private static final int LIST_COLUMN_WIDTH = 200;

    /** EFP. */
    private EFP efp;

    /** LR. */
    private LR lr;

    /** Variable of this dialog. */
    private EfpFormulaDialog self;

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("assignmentGUI");

    /** Text field in parent form initialize in constructor. */
    private JTextField tfParentFormula;

    /** Formula editor. */
    private JTextArea taFormula;

    /** List of efps. */
    private JefpList efpsList;

    /** List of value names of efp chosen in efpsList. */
    private JList namesList;

    /**
     * Create the dialog on position and with the existing formula.
     * @param tfFormula The JText field in parent frame.
     * @param efp Derived Efp.
     * @param lr Local register of editing LrAssignment.
     */
    public EfpFormulaDialog(final JTextField tfFormula, final EFP efp, final LR lr) {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle(bundle.getProperty("formula_editor"));
        setModal(true);

        tfParentFormula = tfFormula;
        this.efp = efp;
        this.lr = lr;
        self = this;
        init();
        setVisible(true);
    }

    /** Initialization of dialog. */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                Object[] options = new Object[] {bundle.getProperty("yes"),
                        bundle.getProperty("no"), bundle.getProperty("cancel")};
                int value = JOptionPane.showOptionDialog(null,
                        bundle.getProperty("formula_close_quest"), "Save a formula",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);

                if (value == JOptionPane.YES_OPTION) {
                    save();
                }

                if (value == JOptionPane.NO_OPTION) {
                    self.dispose();
                }
            }

        });
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] {WIDTH - 2 * LIST_COLUMN_WIDTH,
                LIST_COLUMN_WIDTH, LIST_COLUMN_WIDTH, 0};
        gridBagLayout.columnWeights = new double[] {1.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[] {0.0, 0.0, 1.0, 0.0};
        getContentPane().setLayout(gridBagLayout);

        JScrollPane spFormula = new JScrollPane();
        GridBagConstraints gbcSpFormula = new GridBagConstraints();
        gbcSpFormula.gridheight = 4;
        gbcSpFormula.insets = new Insets(0, 0, 5, 5);
        gbcSpFormula.fill = GridBagConstraints.BOTH;
        gbcSpFormula.gridx = 0;
        gbcSpFormula.gridy = 0;
        spFormula.setViewportView(getFormula());
        getContentPane().add(spFormula, gbcSpFormula);

        ImageIcon image = new ImageIcon(getClass().getClassLoader().getResource(
                "images/otaznik.jpg"));
        JLabel lblFormulaHelp = new JLabel(image);
        lblFormulaHelp.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                JOptionPane.showMessageDialog(null, "<html><body><div width=\"250\">"
                        + bundle.getProperty("formula_help") + "</div></body></html>",
                        "Help", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        GridBagConstraints gbcFormulaHelp = new GridBagConstraints();
        gbcFormulaHelp.anchor = GridBagConstraints.WEST;
        gbcFormulaHelp.fill = GridBagConstraints.NONE;
        gbcFormulaHelp.insets = new Insets(0, 0, 5, 0);
        gbcFormulaHelp.gridx = 1;
        gbcFormulaHelp.gridy = 0;
        getContentPane().add(lblFormulaHelp, gbcFormulaHelp);

        GridBagConstraints gbcLogicalBtn = new GridBagConstraints();
        gbcLogicalBtn.gridwidth = 2;
        gbcLogicalBtn.fill = GridBagConstraints.NONE;
        gbcLogicalBtn.insets = new Insets(0, 0, 5, 0);
        gbcLogicalBtn.gridx = 1;
        gbcLogicalBtn.gridy = 1;
        getContentPane().add(getLogicalButtons(), gbcLogicalBtn);

        GridBagConstraints gbcEfpsList = new GridBagConstraints();
        gbcEfpsList.fill = GridBagConstraints.BOTH;
        gbcEfpsList.insets = new Insets(0, 0, 5, 0);
        gbcEfpsList.gridx = 1;
        gbcEfpsList.gridy = 2;
        getContentPane().add(getEfpsList(), gbcEfpsList);

        GridBagConstraints gbcNamesList = new GridBagConstraints();
        gbcNamesList.fill = GridBagConstraints.BOTH;
        gbcNamesList.insets = new Insets(0, 0, 5, 0);
        gbcNamesList.gridx = 2;
        gbcNamesList.gridy = 2;
        getContentPane().add(getNamesList(), gbcNamesList);

        JButton btnInsert = new JButton(bundle.getProperty("formula_btnInsert"));
        btnInsert.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (efpsList.getSelectedValue() != null) {
                    taFormula.insert(" " + ((EFP) efpsList.getSelectedValue()).getName(),
                            taFormula.getCaretPosition());
                    if (namesList.getSelectedValue() != null) {
                        taFormula.insert("_" + namesList.getSelectedValue(),
                                taFormula.getCaretPosition());
                    }
                    taFormula.insert(" ", taFormula.getCaretPosition());
                    taFormula.requestFocus();
                }
            }
        });
        GridBagConstraints gbcBtnInsert = new GridBagConstraints();
        gbcBtnInsert.gridwidth = 2;
        gbcBtnInsert.gridx = 1;
        gbcBtnInsert.gridy = 3;
        getContentPane().add(btnInsert, gbcBtnInsert);

        GridBagConstraints gbcControlPanel = new GridBagConstraints();
        gbcControlPanel.gridwidth = 3;
        gbcControlPanel.gridx = 0;
        gbcControlPanel.gridy = 4;
        getContentPane().add(getControlPanel(), gbcControlPanel);

    }

    /**
     * Create a control panel.
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel controlPanel = new JPanel();

        JButton btnSave = new JButton(bundle.getProperty("formula_save"));
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                save();
            }
        });
        controlPanel.add(btnSave);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        controlPanel.add(btnStorno);

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                test();
            }
        });
        controlPanel.add(btnTest);

        return controlPanel;
    }

    /**
     * Create the list of efps.
     * @return the formula editor
     */
    private Container getEfpsList() {
        JScrollPane scrollPaneEfps = new JScrollPane();
        efpsList = new JefpList(((DerivedEFP) efp).getEfps());
        efpsList.setCellRenderer(new EfpListCellRenderer());
        efpsList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                namesList.setListData(((EFP) efpsList.getSelectedValue()).getMeta()
                        .getNames().toArray());
                namesList.clearSelection();
            }

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && efpsList.getSelectedValue() != null) {
                    taFormula.insert(" " + ((EFP) efpsList.getSelectedValue()).getName()
                            + " ", taFormula.getCaretPosition());
                    taFormula.requestFocus();
                }
            }
        });
        scrollPaneEfps.setViewportView(efpsList);
        return scrollPaneEfps;

    }

    /**
     * Create the list of value names.
     * @return the formula editor
     */
    private Container getNamesList() {
        JScrollPane scrollPaneNames = new JScrollPane();
        namesList = new JList(new DefaultListModel());
        namesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        namesList.setCellRenderer(new ListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(final JList list,
                    final Object value, final int index, final boolean isSelected,
                    final boolean cellHasFocus) {

                EFP efp = ((EFP) efpsList.getSelectedValue());
                LrAssignment assignment = EfpRegistryGUI.getEfpClient()
                    .findLrAssignment(efp.getId(), lr.getId(), (String) value);

                String val = value + " = ";

                if (assignment != null) {
                    val +=  assignment.getLabel();
                }

                JLabel label = (JLabel) new ValueNameListCellRenderer()
                        .getListCellRendererComponent(list, val, index, isSelected,
                                cellHasFocus);
                return label;
            }
        });

        namesList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && efpsList.getSelectedValue() != null
                        && namesList.getSelectedValue() != null) {
                    taFormula.insert(" " + ((EFP) efpsList.getSelectedValue()).getName()
                            + "_" + namesList.getSelectedValue() + " ",
                            taFormula.getCaretPosition());
                    taFormula.requestFocus();
                }
            }
        });
        scrollPaneNames.setViewportView(namesList);
        return scrollPaneNames;

    }

    /**
     * Create the Formula editor.
     * @return the formula editor
     */
    private Container getFormula() {
        taFormula = new JTextArea();
        taFormula.setLineWrap(true);
        taFormula.setWrapStyleWord(true);
        taFormula.setText(tfParentFormula.getText());
        taFormula.setToolTipText("<html><body><div width=\"250\">"
                + bundle.getProperty("formula_help") + "</div></body></html>");
        return taFormula;

    }

    /**
     * Create panel with buttons for writing formula.
     * @return Panel with buttons.
     */
    private Container getLogicalButtons() {
        JPanel panel = new JPanel();

        JButton btnAnd = new JButton("AND");
        btnAnd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" && ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnAnd);

        JButton btnOr = new JButton("OR");
        btnOr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" || ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnOr);

        JButton btnEq = new JButton("=");
        btnEq.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" == ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnEq);

        JButton btnNoEq = new JButton("!=");
        btnNoEq.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" != ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnNoEq);

        JButton btnLt = new JButton("<");
        btnLt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" < ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnLt);

        JButton btnGt = new JButton(">");
        btnGt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" > ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnGt);

        JButton btnLBrac = new JButton("(");
        btnLBrac.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" ( ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnLBrac);

        JButton btnRBrac = new JButton(")");
        btnRBrac.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" ) ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnRBrac);

        return panel;
    }

    /**
     * Save a formula.
     */
    private void save() {
        tfParentFormula.setText(taFormula.getText());
        self.dispose();
    }

    /**
     * Show dialog for test of formula.
     */
    private void test() {
        new EfpFormulaEvaluate(taFormula.getText(), lr, ((DerivedEFP) efp).getEfps());
    }

}
