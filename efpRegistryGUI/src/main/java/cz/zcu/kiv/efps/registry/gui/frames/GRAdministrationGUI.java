package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.gui.exceptions.ValueUsedException;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.renderers.GrListCellRenderer;
import cz.zcu.kiv.efps.types.gr.GR;

/**
 * Formular for creating, renaming and deleting GR. <br>
 * <br>
 * <b>Date:</b> 8.12.2010
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class GRAdministrationGUI extends JFrame {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Width of frame. */
    private static final int WIDTH = 500;

    /** Height of frame. */
    private static final int HEIGHT = 350;

    /** A regular expression for validation of shortcut for GR. */
    private static final String REGEX_SHORTCUT = "^[A-Za-z]+[A-Za-z0-9\\-\\._]*$";

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(GRAdministrationGUI.class);

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("GRAdministrationGUI");;

    /** Content pane of frame. */
    private JPanel contentPane;

    /** JList of efp value. */
    private JList grs;

    /** TextField of new value name. */
    private JTextField tfNewValueName;

    /** TextField for string ID. */
    private JTextField tfNewValueIdStr;

    /** Variable of this formular. */
    private final GRAdministrationGUI self;

    /**
     * Create the frame.
     */
    public GRAdministrationGUI() {
        // setAlwaysOnTop(true);
        this.setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setTitle(bundle.getProperty("caption"));
        this.setLocationRelativeTo(null);
        self = this;
        init();
        load();
        setVisible(true);
        validate();
        repaint();
    }

    /**
     * Initialization GUI.
     */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                if (EfpRegistryGUI.getRegistryGUI().isVisible()) {
                    self.dispose();
                } else {
                    Object[] options = new Object[] {bundle.getProperty("yes"),
                            bundle.getProperty("no")};
                    int value = JOptionPane.showOptionDialog(null, new EfpLocalization(
                            "efpRegistryGUI").getProperty("quit_quest"),
                            "Quit a application", JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

                    if (value == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
        });
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridBagLayout());

        JLabel lblNewValueName = new JLabel(bundle.getProperty("lblNewValue"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(lblNewValueName, gbc);

        JScrollPane spNames = new JScrollPane();
        gbc.gridheight = 8;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        contentPane.add(spNames, gbc);

        grs = new JList(new DefaultListModel());
        grs.addMouseListener(new MouseAdapter() {

            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && grs.getSelectedValue() != null) {
                    changeGR();
                }

            }
        });
        grs.setCellRenderer(new GrListCellRenderer());
        grs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        grs.addListSelectionListener(
                new GRListHandler());
        spNames.setViewportView(grs);

        tfNewValueName = new JTextField();
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(tfNewValueName, gbc);
        tfNewValueName.setColumns(10);

        lblNewValueName = new JLabel(bundle.getProperty("lblIdStr"));
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(lblNewValueName, gbc);

        tfNewValueIdStr = new JTextField();
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(tfNewValueIdStr, gbc);
        tfNewValueIdStr.setColumns(10);

        JButton btnAdd = new JButton(bundle.getProperty("btnAdd"));
        btnAdd.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_create.png")));
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                addGR();
            }
        });
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(btnAdd, gbc);

        JButton btnDelete = new JButton(bundle.getProperty("btnDelete"));
        btnDelete.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_delete.png")));
        btnDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                removeGR();
            }
        });
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(btnDelete, gbc);

        JButton btnRename = new JButton(bundle.getProperty("btnRename"));
        btnRename.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/LR_edit.png")));
        btnRename.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                renameGR();
            }
        });
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(btnRename, gbc);

        JButton btnChange = new JButton(bundle.getProperty("btnChange"));
        btnChange.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/refresh.png")));
        btnChange.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                changeGR();
            }
        });
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        contentPane.add(btnChange, gbc);
    }

    /**
     * Load actual data from database to list.
     */
    private void load() {
        grs.setModel(new DefaultListModel());
        for (GR obj : EfpRegistryGUI.getEfpClient().findGrAll()) {
            ((DefaultListModel) grs.getModel()).addElement(obj);
        }
    }

    /**
     * Create new GR.
     */
    private void addGR() {
        if (tfNewValueName.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }
        if (tfNewValueIdStr.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_ID_STR"));
        }
        if (isExistGR(tfNewValueName.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_GR_EXIST"));
        }
        if (isUsedUniqueShortcut(tfNewValueIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_EXIST"));
        }

        if (!isValidUniqueShortcut(tfNewValueIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_NOTVALID"));
        }

        GR gr = new GR(tfNewValueName.getText(), tfNewValueIdStr.getText());

        EfpRegistryGUI.getEfpClient().create(gr);

        logger.info("Create GR:" + gr);
        load();

        tfNewValueName.setText("");
    }

    /**
     * Rename selected GR according text in tfNewValueName.
     */
    private void renameGR() {
        if (grs.getSelectedValue() == null) {
            throw new WrongInputException(bundle.getProperty("ERR_GR_NO_SELECTED"));
        }

        GR gr = (GR) grs.getSelectedValue();

        if (tfNewValueName.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_NAME"));
        }

        if (tfNewValueIdStr.getText().equals("")) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_ID_STR"));
        }

        if (!tfNewValueName.getText().equals(gr.getName()) && isExistGR(tfNewValueName.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_GR_EXIST"));
        }

        if (!tfNewValueIdStr.getText().equals(gr.getIdStr())
                && isUsedUniqueShortcut(tfNewValueIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_EXIST"));
        }

        if (!isValidUniqueShortcut(tfNewValueIdStr.getText())) {
            throw new WrongInputException(bundle.getProperty("ERR_ID_STR_NOTVALID"));
        }

        int index = grs.getSelectedIndex();
        String oldName = gr.getName();
        String oldIdStr = gr.getIdStr();
        gr = new GR(gr.getId(), tfNewValueName.getText(), tfNewValueIdStr.getText());

        ((DefaultListModel) grs.getModel()).setElementAt(gr, index);
        EfpRegistryGUI.getEfpClient().update(gr);

        logger.info("Rename GR: old_name=" + oldName + " new_name=" + gr.getName() + " old_idStr=" + oldIdStr + " new_idStr=" + gr.getIdStr());

        tfNewValueName.setText("");
        tfNewValueIdStr.setText("");
        if (EfpRegistryGUI.getSelectedGR().getId() == gr.getId()) {
            EfpRegistryGUI.getRegistryGUI().refresh();
        }
    }

    /**
     * Remove selected GR.
     */
    private void removeGR() {
        if (grs.getSelectedValue() == null) {
            throw new WrongInputException(bundle.getProperty("ERR_GR_NO_SELECTED"));
        }
        /* if (((DefaultListModel) grs.getModel()).getSize() == 1) {
         * throw new WrongInputException(bundle.getProperty("ERR_GR_ONLY_ONE"));
         * } */
        GR pom = (GR) grs.getSelectedValue();
        if (!EfpRegistryGUI.getEfpClient().findLrForGr(pom).isEmpty()
                || !EfpRegistryGUI.getEfpClient().findEfpForGr(pom.getId()).isEmpty()
                || !EfpRegistryGUI.getEfpClient().getAllValueNames(pom.getId()).isEmpty()) {

            throw new ValueUsedException(bundle.getProperty("ERR_GR_USED"));
        }

        // confirm
        Object[] options = new Object[] {bundle.getProperty("yes"),
                bundle.getProperty("no")};
        int value = JOptionPane.showOptionDialog(null, bundle.getProperty("del_GR_quest")
                + " " + pom.getName() + "?", "Delete global register",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
                options[1]);

        if (value != JOptionPane.YES_OPTION) {
            return;
        }
        // end confirm

        ((DefaultListModel) grs.getModel()).removeElement(pom);
        if (EfpRegistryGUI.getSelectedGR().equals(pom)) {
            EfpRegistryGUI.getRegistryGUI().setVisible(false);
        }
        EfpRegistryGUI.getEfpClient().deleteGr(pom.getId());

        logger.info("Delete GR:" + pom);
    }

    /**
     * Change selected GR in Registry gui on selected here.
     */
    private void changeGR() {
        if (grs.getSelectedValue() == null) {
            throw new WrongInputException(bundle.getProperty("ERR_GR_NO_SELECTED"));
        }
        EfpRegistryGUI.setSelectedGR((GR) grs.getSelectedValue());
        EfpRegistryGUI.showRegistryGUI();
        this.dispose();
    }

    /**
     * Return if GR is already exists.
     * @param name Name GR
     * @return true if exists
     */
    private boolean isExistGR(final String name) {
        boolean exist = false;
        for (int i = 0; i < ((DefaultListModel) grs.getModel()).getSize(); i++) {
            GR gr = (GR) ((DefaultListModel) grs.getModel()).getElementAt(i);
            if (gr.getName().equals(name)) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    /**
     * Returns if string ID for GR is already exists.
     * @param idStr String ID
     * @return true if is used
     */
    private boolean isUsedUniqueShortcut(final String idStr)
    {
        boolean exist = false;
        for (int i = 0; i < ((DefaultListModel) grs.getModel()).getSize(); i++) {
            GR gr = (GR) ((DefaultListModel) grs.getModel()).getElementAt(i);
            if (gr.getIdStr() != null && gr.getIdStr().equals(idStr)) {
                exist = true;
                break;
            }
        }

        return exist;
    }

    /**
     * Returns if string ID for GR is valid.
     * @param idStr String ID
     * @return true if is valid
     */
    public static boolean isValidUniqueShortcut(final String idStr)
    {
        Pattern pattern = Pattern.compile(REGEX_SHORTCUT, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(idStr);

        return matcher.matches();
    }

    /**
     * Listener for change of selected item in JList.
     */
    class GRListHandler implements ListSelectionListener {
        /**
         * {@inheritDoc}
         */
        @Override
        public void valueChanged(ListSelectionEvent e) {
            GR gr = (GR) grs.getSelectedValue();

            if(gr == null) {
                tfNewValueName.setText("");
                tfNewValueIdStr.setText("");
            } else {
                tfNewValueName.setText(gr.getName());
                tfNewValueIdStr.setText(gr.getIdStr());
            }
        }
    }
}
