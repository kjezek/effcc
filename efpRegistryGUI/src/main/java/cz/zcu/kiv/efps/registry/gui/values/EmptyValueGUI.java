package cz.zcu.kiv.efps.registry.gui.values;

import javax.swing.JOptionPane;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * Empty EfpValueGUI extends JPanel. <br>
 * Date: 9. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EmptyValueGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /**
     * Create the panel.
     */
    EmptyValueGUI() {
        super();
    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        throw new IllegalValueException(bundle.getProperty("ERR_NOT_EXIST_GUI"));
    }

    @Override
    public void setValue(final EfpValueType value) {
        throw new RuntimeException(bundle.getProperty("ERR_NOT_EXIST_GUI"));
    }

    @Override
    public void lock() {
    }
}
