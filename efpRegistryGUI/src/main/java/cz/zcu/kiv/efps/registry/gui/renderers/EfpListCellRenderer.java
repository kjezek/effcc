package cz.zcu.kiv.efps.registry.gui.renderers;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;


/**
 * ListCellRenderer renderer of EFP.
 * <br><br>
 * <b>Date:</b> 22.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpListCellRenderer extends DefaultListCellRenderer {
    /** Serial version. */
    private static final long serialVersionUID = 7569171632710325169L;

    @Override
    public Component getListCellRendererComponent(final JList list,
            final Object value, final int index, final boolean isSelected,
            final boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list,
                ((EFP) value).getName(), index, isSelected, cellHasFocus);

        if (value instanceof SimpleEFP) {
            label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
            "images/efpSimpleIcon.png")));
        }

        if (value instanceof DerivedEFP) {
            label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
            "images/efpDerivedIcon.png")));
        }
        return label;
    }
}
