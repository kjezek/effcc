package cz.zcu.kiv.efps.registry.gui.values;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpRatio;
import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;

/**
 * Class for creating value GUI depends on type of value. <br>
 * Date: 9. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public final class FactoryValueGUI {

    /**
     * Private constructor.
     */
    private FactoryValueGUI() {
    }

    /**
     * Create GUI for value.
     * @param classOfValue for this class will be created GUI
     * @return JPanel with GUI
     */
    public static EfpValueGUI getValueGUI(final Class<?> classOfValue) {
        if (classOfValue == EfpBoolean.class) {
            return new EfpBooleanGUI();
        }

        if (classOfValue == EfpNumberInterval.class) {
            return new EfpNumberIntervalGUI();
        }

        if (classOfValue == EfpString.class) {
            return new EfpStringGUI();
        }

        if (classOfValue == EfpNumber.class) {
            return new EfpNumberGUI();
        }

        if (classOfValue == EfpRatio.class) {
            return new EfpRatioGUI();
        }

        if (classOfValue == EfpEnum.class) {
            return new EfpEnumGUI();
        }

        if (classOfValue == EfpSet.class || classOfValue == EfpSimpleSet.class) {
            return new EfpSetGUI();
        }

        if (classOfValue == EfpComplex.class) {
            return new EfpComplexGUI();
        }

        return new EmptyValueGUI();
    }
}
