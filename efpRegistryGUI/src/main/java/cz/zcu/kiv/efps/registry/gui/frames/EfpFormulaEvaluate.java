package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.registry.client.EfpClientImpl;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.AssignedValueFinder;
import cz.zcu.kiv.efps.types.evaluator.EfpLrAssignment;
import cz.zcu.kiv.efps.types.evaluator.EfpValue;
import cz.zcu.kiv.efps.types.evaluator.AbstractGroovyLrdAssignmentEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LogicalFormulaHelper;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for test of logical formula with testing values. <br>
 * <br>
 * <b>Date:</b> 1.3.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpFormulaEvaluate extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7802863519039960317L;

    /** Width of frame. */
    private static final int WIDTH = 600;

    /** Height of frame. */
    private static final int HEIGHT = 300;

    /** A testing logicat formula. */
    private String formula;

    /** Used efp with LrAssignment. */
    private List<EfpLrAssignment> efpLrs;

    /** List of JTextFields with values. */
    private List<EfpValueGUI> lstValues;

    /** Variable of this dialog. */
    private EfpFormulaEvaluate self;

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("assignmentGUI");

    /** LR. */
    private LR lr;

    /**
     * Test a logical formula.
     * @param formula A logical formula.
     * @param lr A local register of LrAssignment.
     * @param efps List of all possible efps for this formula.
     */
    public EfpFormulaEvaluate(final String formula, final LR lr, final List<EFP> efps) {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle(bundle.getProperty("formula_editor_testing"));
        setModal(true);

        self = this;
        this.lr = lr;
        LogicalFormulaHelper helper = new LogicalFormulaHelper(formula);
        efpLrs = helper.findUsedAssignments(efps);
        this.formula = formula;
        lstValues = new ArrayList<EfpValueGUI>();
        init();
        setVisible(true);
    }

    /** Initialization of dialog. */
    private void init() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.insets = new Insets(0, 0, 0, 0);
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(getValuesPanel(), gbc);

        gbc.insets = new Insets(5, 0, 5, 0);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(getControlPanel(), gbc);

        setContentPane(panel);
    }

    /**
     * Return a panel with form for input values.
     * @return A panel for input values.
     */
    private Container getValuesPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(bundle.getProperty("fomula_test_values")));
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        for (int i = 0; i < efpLrs.size(); i++) {
            gbc.gridy = i;

            gbc.gridx = 0;
            gbc.weightx = 0.0;
            gbc.fill = GridBagConstraints.NONE;
            panel.add(new JLabel(efpLrs.get(i).getEfp().getName()), gbc);

            gbc.gridx = 1;
            gbc.weightx = 1.0;
            gbc.fill = GridBagConstraints.BOTH;
            EfpValueGUI val = FactoryValueGUI.getValueGUI(efpLrs.get(i).getEfp()
                    .getValueType());
            lstValues.add(val);
            panel.add(val, gbc);
        }
        JScrollPane sp = new JScrollPane();
        sp.setBorder(null);
        sp.setViewportView(panel);
        return sp;
    }

    /**
     * Return a control panel with test button.
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel panel = new JPanel();

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                EfpValue[] values = new EfpValue[lstValues.size()];
                for (int i = 0; i < lstValues.size(); i++) {
                    try {
                        EfpValueType value = lstValues.get(i).getValue();
                        values[i] = new EfpValue(efpLrs.get(i).getEfp(), value);
                    } catch (IllegalValueException e1) {
                        throw new WrongInputException(e1.getMessage());
                    }
                }
                try {
                    Boolean  result = evaluate(values);
                    JOptionPane.showMessageDialog(self, bundle.getProperty("formula_OK")
                            + "\n" + result, "Formula is OK",
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(self, bundle.getProperty("formula_KO")
                            + "\n" + e1.getMessage(), "Wrong formula",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        panel.add(btnTest);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        panel.add(btnStorno);
        return panel;
    }

    /**
     * Evaluate valid of formula.
     * @param values Testing values.
     * @return True if formula is valid.
     */
    private Boolean evaluate(final EfpValue[] values) {
        List<LrAssignment> lrAssignments = new ArrayList<LrAssignment>();

        for (EfpLrAssignment efpLr : efpLrs) {

            for (String valueName : efpLr.getLrValueNames()) {
                LrAssignment relatedAssignment = EfpClientImpl.getClient()
                    .findLrAssignment(efpLr.getEfp().getId(), lr.getId(), valueName);

                lrAssignments.add(relatedAssignment);
            }

        }

        final Map<EFP, EfpValueType> valueMap = new HashMap<EFP, EfpValueType>();
        final List<EFP> efps = new LinkedList<EFP>();
        for (EfpValue value : values) {
            valueMap.put(value.getEfp(), value.getValue());
            efps.add(value.getEfp());
        }

        final AssignedValueFinder finder = new AssignedValueFinder() {
            @Override
            public EfpValueType findValue(final EFP efp) {
                return valueMap.get(efp);
            }
        };

        EfpValueType expected = new EfpNumber(1);
        LrDerivedValueEvaluator eval =
            new LrConstraintEvaluator(formula, expected, efps);
        return expected.equals(eval.evaluate(finder, lrAssignments));
    }

}
