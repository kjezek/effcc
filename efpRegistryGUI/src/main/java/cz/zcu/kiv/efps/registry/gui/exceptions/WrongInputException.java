package cz.zcu.kiv.efps.registry.gui.exceptions;

/**
 * This expection throws if we try action with wrong value. <br>
 * <br>
 * <b>Date:</b> 10.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class WrongInputException extends RuntimeException {

    /** Serial version. */
    private static final long serialVersionUID = -4802120145734674422L;

    /**
     * Throw WrongValueException.
     * @param msg Message which will send.
     */
    public WrongInputException(final String msg) {
        super(msg);
    }
}
