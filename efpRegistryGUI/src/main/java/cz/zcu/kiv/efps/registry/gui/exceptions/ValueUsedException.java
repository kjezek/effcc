package cz.zcu.kiv.efps.registry.gui.exceptions;

/**
 * This exception throws if we try delete value which is used. <br>
 * <br>
 * <b>Date:</b> 10.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class ValueUsedException extends RuntimeException {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /**
     * Throw WrongValueException.
     * @param msg Message which will send.
     */
    public ValueUsedException(final String msg) {
        super(msg);
    }
}
