package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import cz.zcu.kiv.efps.registry.client.EfpClientImpl;
import cz.zcu.kiv.efps.registry.gui.exceptions.WrongInputException;
import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;
import cz.zcu.kiv.efps.registry.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.registry.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The frame for creating or editing assignment value to EFP. <br>
 * Date: 26. 10. 2010
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class AssignmentLrGUI extends JPanel {

    /** Serial version. */
    private static final long serialVersionUID = -6442515676902884185L;

    /** Width of frame. */
    private static final int WIDTH = 700;

    /** Height of frame. */
    private static final int HEIGHT = 600;

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(AssignmentLrGUI.class);

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("assignmentGUI");

    /** Content pane of the panel. */
    private JPanel content;

    /** ComboBox of local registers. */
    private JComboBox cmbLr;

    /** ComboBox of efps. */
    private JComboBox cmbEfp;

    /** ComboBox actual ValueName by efp. */
    private JComboBox cmbValueName;

    /** New or edited assignment. */
    private LrAssignment assignment;

    /** Map of local registers and their names. */
    private Map<String, Integer> lrs;

    /** Efp of assignment. */
    private EFP efp;

    /** TextField for editing formula. Visible only for Derived Efp. */
    private JTextField tfFormula;

    /** Label for formula. Visible only for Derived Efp. */
    private JLabel lblFormula;

    /** Panel of GUI for show value. Depends on type of value. */
    private EfpValueGUI pnValue;

    /** True if edit assignment. */
    private boolean edit;

    /** ScrollPane of value. */
    private JScrollPane spContent;

    /** Link to parent frame. */
    private JFrame frame;

    /** Button for showing formula editor. */
    private JButton btnFormula;

    /** True if components has been lock. */
    private boolean lock = false;

    /**
     * Create the panel and load LrAssignment for view assignment.
     * @param assignment Assignment which will be edited
     */
    public AssignmentLrGUI(final LrAssignment assignment) {
        this.assignment = assignment;
        this.setEnabled(false);
        efp = this.assignment.getEfp();
        edit = true;
        init();
        load();
        lock();
    }

    /**
     * Create the panel and set this like contentPane parent form for create assignment.
     * @param frame link to parent frame.
     */
    public AssignmentLrGUI(final JFrame frame) {
        this.frame = frame;
        // this.frame.setAlwaysOnTop(true);
        this.frame.setSize(WIDTH, HEIGHT);
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.frame.addWindowListener(new CloseWindowListener());
        this.frame.setTitle(bundle.getProperty("create"));
        this.frame.setContentPane(this);

        assignment = null;
        efp = EfpRegistryGUI.getRegistryGUI().getSelectedEFP();
        edit = false;
        init();
        addControlButton();
    }

    /**
     * Create the panel and set this like contentPane parent form and load LrAssignment.
     * @param frame parent frame.
     * @param assignment Assignment which will be edited
     */
    public AssignmentLrGUI(final JFrame frame, final LrAssignment assignment) {
        this.frame = frame;
        // this.frame.setAlwaysOnTop(true);
        this.frame.setSize(WIDTH, HEIGHT);
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.frame.addWindowListener(new CloseWindowListener());
        this.frame.setTitle(bundle.getProperty("edit"));
        this.frame.setContentPane(this);

        this.assignment = assignment;
        efp = this.assignment.getEfp();
        edit = true;
        init();
        load();
        addControlButton();
    }

    /**
     * Initialization GUI.
     */
    private void init() {
        setLayout(new BorderLayout());
        spContent = new JScrollPane();
        spContent.setBorder(null);
        spContent.setViewportView(createPanelProperties());
        add(spContent, BorderLayout.CENTER);
    }

    /**
     * Create properties panel of assignment.
     * @return properties panel of assignment
     */
    private Container createPanelProperties() {
        content = new JPanel();
        content.setBorder(new EmptyBorder(5, 5, 5, 5));
        add(content, BorderLayout.CENTER);
        content.setLayout(new FormLayout(new ColumnSpec[] {
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("max(120dlu;default):grow"),
                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC},
                new RowSpec[] {FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC}));

        addLrRow();
        addEfpRow();
        addValueNameRow();
        addValueRow();
        addFormulaRow();

        if (efp != null && efp.getType().equals(EFP.EfpType.DERIVED)) {
            setVisibleFormula(true);
        } else {
            setVisibleFormula(false);
        }
        return content;
    }

    /**
     * Add to formular in gui row with LR.
     */
    private void addLrRow() {
        JLabel lblLocalRegister = new JLabel(bundle.getProperty("lblLR"));
        content.add(lblLocalRegister, "4, 4");

        cmbLr = new JComboBox();
        cmbLr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                refreshNameValue();
            }
        });
        lrs = new HashMap<String, Integer>();
        for (LR lr : EfpRegistryGUI.getEfpClient().findLrForGr(
                EfpRegistryGUI.getSelectedGR())) {
            lrs.put(lr.getName(), lr.getId());
        }
        cmbLr.setModel(new DefaultComboBoxModel(lrs.keySet().toArray()));
        LR selectedLR;
        selectedLR = EfpRegistryGUI.getRegistryGUI().getSelectedLR();
        if (selectedLR != null) {
            cmbLr.setSelectedItem(selectedLR.getName());
        }
        content.add(cmbLr, "8, 4, fill, default");

    }

    /**
     * Add to formular in gui row with Efp.
     */
    private void addEfpRow() {
        JLabel lblEfp = new JLabel(bundle.getProperty("lblEfp"));
        content.add(lblEfp, "4, 6");

        cmbEfp = new JComboBox();

        List<String> efpsName = new ArrayList<String>();
        for (EFP ef : EfpRegistryGUI.getEfpClient().findEfpForGr(
                EfpRegistryGUI.getSelectedGR().getId())) {
            efpsName.add(ef.getName());
        }
        cmbEfp.setModel(new DefaultComboBoxModel(efpsName.toArray()));
        if (efp != null) {
            cmbEfp.setSelectedItem(efp.getName());
        } else {
            cmbEfp.setSelectedIndex(-1);
        }
        cmbEfp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent events) {
                efp = EfpRegistryGUI.getEfpClient().findEfpByName(
                        cmbEfp.getSelectedItem().toString(),
                        EfpRegistryGUI.getSelectedGR().getId());
                refreshNameValue();

                if (efp.getType().equals(EFP.EfpType.DERIVED)) {
                    setVisibleFormula(true);
                } else {
                    setVisibleFormula(false);
                }
                content.remove(pnValue);
                pnValue = FactoryValueGUI.getValueGUI(efp.getValueType());
                if (assignment != null
                        && efp.getValueType() == assignment.getEfp().getValueType()) {
                    pnValue.setValue(pnValue.getValueFromAssignment(assignment));
                }
                content.add(pnValue, "4, 10, 5, 1, fill, fill");
                if (frame != null) {
                    frame.validate();
                    frame.repaint();
                }
            }
        });
        content.add(cmbEfp, "8, 6, fill, default");
    }

    /**
     * Add to formular in gui row with value name.
     */
    private void addValueNameRow() {
        JLabel lblValueName = new JLabel(bundle.getProperty("lblValueName"));
        content.add(lblValueName, "4, 8");

        cmbValueName = new JComboBox();
        if (efp != null) {
            refreshNameValue();
        }

        content.add(cmbValueName, "8, 8, fill, default");
    }

    /**
     * Add to formular in gui row with value name.
     */
    private void addValueRow() {
        if (efp == null) {
            pnValue = FactoryValueGUI.getValueGUI(null);
        } else {
            pnValue = FactoryValueGUI.getValueGUI(efp.getValueType());
        }

        pnValue.setBorder(new TitledBorder(bundle.getProperty("titleBorderValue")));
        content.add(pnValue, "4, 10, 5, 1, fill, fill");
    }

    /**
     * Add to formular in gui row with formula.
     */
    private void addFormulaRow() {

        lblFormula = new JLabel(bundle.getProperty("lblFormula"));
        content.add(lblFormula, "4, 12");

        tfFormula = new JTextField();
        tfFormula.setEditable(false);

        content.add(tfFormula, "8, 12, fill, default");

        btnFormula = new JButton("...");
        btnFormula.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent event) {
                new EfpFormulaDialog(tfFormula, efp, EfpRegistryGUI.getEfpClient()
                        .getLrById(lrs.get(cmbLr.getSelectedItem())));
            }
        });
        content.add(btnFormula, "10, 12, right, default");
    }

    /**
     * Set visibility of formula.
     * @param bool True - formula will be visibled.
     */
    private void setVisibleFormula(final boolean bool) {
        lblFormula.setVisible(bool);
        tfFormula.setVisible(bool);
        btnFormula.setVisible(bool);
    }

    /**
     * Create submit button.
     */
    private void addControlButton() {
        JPanel panel = new JPanel();
        JButton btnCreateAssignment = new JButton();
        btnCreateAssignment.setMargin(new Insets(5, 5, 5, 5));
        if (edit) {
            btnCreateAssignment.setText(bundle.getProperty("edit"));
            btnCreateAssignment.setIcon(new ImageIcon(getClass().getClassLoader()
                    .getResource("images/edit.png")));
        } else {
            btnCreateAssignment.setText(bundle.getProperty("create"));
            btnCreateAssignment.setIcon(new ImageIcon(getClass().getClassLoader()
                    .getResource("images/create.png")));
        }
        btnCreateAssignment.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (!edit) {
                    newAssignment();
                } else {
                    editAssignment();
                }
            }
        });
        panel.add(btnCreateAssignment);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.setMargin(new Insets(5, 5, 5, 5));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                frame.dispose();
            }
        });
        panel.add(btnStorno);
        add(panel, BorderLayout.SOUTH);
    }

    /**
     * Load information about editing assignment to form.
     */
    private void load() {
        /* formula */
        if (assignment.getAssignmentType() == LrAssignment.LrAssignmentType.DERIVED) {
            tfFormula.setText(((LrDerivedAssignment) assignment).getEvaluator().serialise());
        }

        /* efp */
        cmbEfp.setSelectedItem(assignment.getEfp().getName());

        /* lr */
        cmbLr.setSelectedItem(assignment.getLr().getName());

        /* value name */
        refreshNameValue();
        cmbValueName.setSelectedItem(assignment.getValueName());

        /* value */
        pnValue.setValue(pnValue.getValueFromAssignment(assignment));
    }

    /**
     * Create new assignment.
     */
    private void newAssignment() {
        String valueName = (String) cmbValueName.getSelectedItem();
        EfpValueType efpValue;
        if (valueName == null) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_VALUE_NAME"));
        }
        try {
            efpValue = pnValue.getValue();
        } catch (IllegalValueException e) {
            throw new WrongInputException(e.getMessage());
        }

        LR lr = EfpRegistryGUI.getEfpClient().getLrById(lrs.get(cmbLr.getSelectedItem()));

        if (efp.getType().equals(EFP.EfpType.SIMPLE)) {
            assignment = new LrSimpleAssignment(efp, valueName, efpValue, lr);
        }

        if (efp.getType().equals(EFP.EfpType.DERIVED)) {
            String logicalFormula = tfFormula.getText();

            List<LrAssignment> derivingAssignments = EfpClientImpl.getClient()
                .findLrAssignmentsInFormula(logicalFormula, ((DerivedEFP) efp).getEfps(), lr);

            // TODO [kjezek, A] I deal only with a logical formulas here.
            LrDerivedValueEvaluator evaluator = new LrConstraintEvaluator(
                    logicalFormula,  efpValue,
                    ((DerivedEFP) efp).getEfps());

            assignment = new LrDerivedAssignment(efp, valueName, evaluator, lr);
        }
        EfpRegistryGUI.getEfpClient().create(assignment);

        logger.info("Create LrAssignment:" + assignment);

        EfpRegistryGUI.getRegistryGUI().refresh();
        frame.dispose();
    }

    /**
     * Edit existing assignment.
     */
    private void editAssignment() {
        LrAssignment oldAssignment = assignment;
        String valueName = (String) cmbValueName.getSelectedItem();
        EfpValueType efpValue;
        if (valueName == null) {
            throw new WrongInputException(bundle.getProperty("ERR_NO_VALUE_NAME"));
        }
        try {
            efpValue = pnValue.getValue();
        } catch (IllegalValueException e) {
            throw new WrongInputException(e.getMessage());
        }
        LR lr = EfpRegistryGUI.getEfpClient().getLrById(lrs.get(cmbLr.getSelectedItem()));

        if (efp.getType() == EFP.EfpType.SIMPLE) {
            assignment = new LrSimpleAssignment(assignment.getId(), efp, valueName,
                    efpValue, lr);
        }

        if (efp.getType() == EFP.EfpType.DERIVED) {
            String logicalFormula = tfFormula.getText();

            // TODO [kjezek, A] I deal only with a logical formulas here.
            LrDerivedValueEvaluator evaluator = new LrConstraintEvaluator(
                    logicalFormula,  efpValue,
                    ((DerivedEFP) efp).getEfps());

            assignment = new LrDerivedAssignment(assignment.getId(), efp, valueName,
                    evaluator, lr);
        }

        EfpRegistryGUI.getEfpClient().update(assignment);

        logger.info("Edit LrAssignment FROM:" + oldAssignment + " TO:" + assignment);

        EfpRegistryGUI.getRegistryGUI().refresh();
        frame.dispose();
    }

    /**
     * Refresh name of value in combobox according EFP and LR.
     */
    private void refreshNameValue() {
        if (cmbValueName == null || efp == null || lock) {
            return;
        }
        if (cmbLr.getSelectedItem() == null) {
            cmbValueName.setModel(new DefaultComboBoxModel());
            return;
        }
        List<String> names = new ArrayList<String>(efp.getMeta().getNames());
        if (names.isEmpty()) {
            cmbValueName.setModel(new DefaultComboBoxModel());
            return;
        }

        LR selectedLr = EfpRegistryGUI.getEfpClient().getLrById(
                lrs.get(cmbLr.getSelectedItem()));
        List<String> use = new ArrayList<String>();
        List<LrAssignment> asg = EfpRegistryGUI.getEfpClient().findLrAssignments(
                efp.getId(), selectedLr.getId());
        if (selectedLr.getParentLR() != null) {
            asg.removeAll(EfpRegistryGUI.getEfpClient().findLrAssignments(efp.getId(),
                    selectedLr.getParentLR().getId()));
        }
        for (LrAssignment as : asg) {
            use.add(as.getValueName());
        }
        names.removeAll(use);

        if (edit && efp.equals(assignment.getEfp())
                && lrs.get(cmbLr.getSelectedItem()).equals(assignment.getLr().getId())) {
            names.add(0, assignment.getValueName());
        }
        if (names.isEmpty()) {
            cmbValueName.setModel(new DefaultComboBoxModel());
            return;
        }
        cmbValueName.setModel(new DefaultComboBoxModel(names.toArray()));
        if (assignment != null && names.contains(assignment.getValueName())) {
            cmbValueName.setSelectedItem(assignment.getValueName());
        } else {
            cmbValueName.setSelectedIndex(0);
        }
    }

    /**
     * Lock component against changes.
     */
    private void lock() {
        lock = true;
        cmbLr.setModel(new DefaultComboBoxModel(new String[] {assignment.getLr()
                .getName()}));
        cmbEfp.setModel(new DefaultComboBoxModel(new String[] {assignment.getEfp()
                .getName()}));
        cmbValueName.setModel(new DefaultComboBoxModel(new String[] {assignment
                .getValueName()}));

        cmbEfp.setSelectedItem(assignment.getEfp().getName());
        cmbLr.setSelectedItem(assignment.getLr().getName());
        cmbValueName.setSelectedItem(assignment.getValueName());

        btnFormula.setVisible(false);
        pnValue.lock();
    }

    /**
     * Windows listener for close operation. <br>
     * <br>
     * <b>Date:</b> 22.2.2011
     * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
     */
    private class CloseWindowListener extends WindowAdapter {

        @Override
        public void windowClosing(final WindowEvent e) {
            Object[] options = new Object[] {bundle.getProperty("yes"),
                    bundle.getProperty("no"), bundle.getProperty("cancel")};
            int value = JOptionPane.showOptionDialog(null,
                    bundle.getProperty("close_quest"), "Save a value",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    options, options[0]);

            if (value == JOptionPane.YES_OPTION) {
                if (edit) {
                    editAssignment();
                } else {
                    newAssignment();
                }
            }

            if (value == JOptionPane.NO_OPTION) {
                frame.dispose();
            }
        }
    }
}
