package cz.zcu.kiv.efps.registry.gui.frames;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cz.zcu.kiv.efps.registry.gui.localization.EfpLocalization;

/**
 * Dialog for creating and editing gamma function. <br>
 * <br>
 * <b>Date:</b> 2.1.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpGammaDialog extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7702863719039960317L;

    /** Width of frame. */
    private static final int WIDTH = 500;

    /** Height of frame. */
    private static final int HEIGHT = 400;

    /** Variable of this dialog. */
    private EfpGammaDialog self;

    /** Properties file with localization. */
    private EfpLocalization bundle = new EfpLocalization("efpGUI");

    /** Text field in parent form initialize in constructor. */
    private JTextField tfParentGamma;

    /** A class of value type of efp. */
    private Class<?> valueType;

    /** Gamma editor. */
    private JTextArea taGamma;

    /**
     * Create the dialog on position and with the existing gamma function.
     * @param tfGamma The JText field in parent frame.
     * @param valueType The value type of efp which has the gamma.
     */
    public EfpGammaDialog(final JTextField tfGamma, Class<?> valueType) {
        setSize(WIDTH, HEIGHT);
        setTitle(bundle.getProperty("gamma_editor"));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);

        self = this;
        this.valueType = valueType;
        tfParentGamma = tfGamma;
        init();
        setVisible(true);
    }

    /** Initialization of dialog. */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                Object[] options = new Object[] {bundle.getProperty("yes"),
                        bundle.getProperty("no"), bundle.getProperty("cancel")};
                int value = JOptionPane.showOptionDialog(null,
                        bundle.getProperty("gamma_close_quest"), "Save a gamma function",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);

                if (value == JOptionPane.YES_OPTION) {
                    save();
                }

                if (value == JOptionPane.NO_OPTION) {
                    self.dispose();
                }
            }

        });
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] {0, 0, 0};
        gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[] {1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE};
        getContentPane().setLayout(gridBagLayout);

        ImageIcon image = new ImageIcon(getClass().getClassLoader().getResource(
                "images/otaznik.jpg"));
        JLabel lblGammaHelp = new JLabel(image);
        lblGammaHelp.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                JOptionPane.showMessageDialog(null, "<html><body><div width=\"250\">"
                        + bundle.getProperty("gamma_help") + "</div></body></html>",
                        "Help", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        GridBagConstraints gbcGammaHelp = new GridBagConstraints();
        gbcGammaHelp.gridwidth = 1;
        gbcGammaHelp.anchor = GridBagConstraints.WEST;
        gbcGammaHelp.fill = GridBagConstraints.BOTH;
        gbcGammaHelp.insets = new Insets(0, 0, 5, 0);
        gbcGammaHelp.gridx = 1;
        gbcGammaHelp.gridy = 0;
        getContentPane().add(lblGammaHelp, gbcGammaHelp);

        JButton btnFirst = new JButton(bundle.getProperty("gamma_first"));
        btnFirst.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taGamma.insert(" first ", taGamma.getCaretPosition());
                taGamma.requestFocus();
            }
        });
        GridBagConstraints gbcFirst = new GridBagConstraints();
        gbcFirst.insets = new Insets(0, 0, 5, 0);
        gbcFirst.gridx = 1;
        gbcFirst.gridy = 1;
        getContentPane().add(btnFirst, gbcFirst);

        JButton btnSecond = new JButton(bundle.getProperty("gamma_second"));
        btnSecond.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taGamma.insert(" second ", taGamma.getCaretPosition());
                taGamma.requestFocus();
            }
        });
        GridBagConstraints gbcSecond = new GridBagConstraints();
        gbcSecond.insets = new Insets(0, 0, 5, 0);
        gbcSecond.gridx = 1;
        gbcSecond.gridy = 2;
        getContentPane().add(btnSecond, gbcSecond);

        JButton btnReturn = new JButton(bundle.getProperty("gamma_return"));
        btnReturn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taGamma.insert(" return ", taGamma.getCaretPosition());
                taGamma.requestFocus();
            }
        });
        GridBagConstraints gbcReturn = new GridBagConstraints();
        gbcReturn.insets = new Insets(0, 0, 5, 0);
        gbcReturn.gridx = 1;
        gbcReturn.gridy = 3;
        getContentPane().add(btnReturn, gbcReturn);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbcScrollPane = new GridBagConstraints();
        gbcScrollPane.gridheight = 5;
        gbcScrollPane.insets = new Insets(0, 0, 5, 5);
        gbcScrollPane.fill = GridBagConstraints.BOTH;
        gbcScrollPane.gridx = 0;
        gbcScrollPane.gridy = 0;
        getContentPane().add(scrollPane, gbcScrollPane);

        scrollPane.setViewportView(getGamma());

        GridBagConstraints gbcControlPanel = new GridBagConstraints();
        gbcControlPanel.gridwidth = 2;
        gbcControlPanel.gridx = 0;
        gbcControlPanel.gridy = 5;
        getContentPane().add(getControlPanel(), gbcControlPanel);

    }

    /**
     * Return a control panel.
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel controlPanel = new JPanel();

        JButton btnSave = new JButton(bundle.getProperty("gamma_save"));
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                save();
            }
        });
        controlPanel.add(btnSave);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        controlPanel.add(btnStorno);

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                test();
            }
        });
        controlPanel.add(btnTest);
        return controlPanel;
    }

    /**
     * Create the gamma editor.
     * @return the gamma editor
     */
    private Container getGamma() {
        taGamma = new JTextArea();
        taGamma.setLineWrap(true);
        taGamma.setWrapStyleWord(true);
        taGamma.setText(tfParentGamma.getText());
        taGamma.setToolTipText("<html><body><div width=\"250\">"
                + bundle.getProperty("gamma_help") + "</div></body></html>");
        return taGamma;

    }

    /**
     * Save the gamma function.
     */
    private void save() {
        tfParentGamma.setText(taGamma.getText());
        self.dispose();
    }

    /**
     * Show dialog for test of gamma.
     */
    private void test() {
        new EfpGammaEvaluate(taGamma.getText(), valueType);
    }

}
