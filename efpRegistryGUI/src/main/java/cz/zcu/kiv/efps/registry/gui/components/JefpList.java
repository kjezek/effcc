package cz.zcu.kiv.efps.registry.gui.components;

import java.awt.Component;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

import cz.zcu.kiv.efps.registry.gui.models.EfpListModel;
import cz.zcu.kiv.efps.registry.gui.renderers.EfpListCellRenderer;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * The class extends JList for representing list of EFP. <br>
 * Date: 13. 10. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class JefpList extends JList {

    /** Serial version. */
    private static final long serialVersionUID = 1378035568538587944L;

    /**
     * Create new JList of efps.
     * @param efpList List of efp which will be visibled
     */
    public JefpList(final List<EFP> efpList) {
        super();
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        addRenderer();
        updateData(efpList);
    }

    /**
     * Set list renderer.
     */
    private void addRenderer() {
        this.setCellRenderer(new EfpListCellRenderer());
    }

    /**
     * Ask client to data and update it.
     * @param efpList List of efp which will be visibled
     */
    public final void updateData(final List<EFP> efpList) {
        if (efpList == null) {
            this.setModel(new DefaultListModel());
        } else {
            this.setModel(new EfpListModel(efpList));
        }
    }
}
