package cz.zcu.kiv.efps.registry.gui.components;

import java.awt.Component;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

import cz.zcu.kiv.efps.registry.gui.frames.EfpRegistryGUI;
import cz.zcu.kiv.efps.registry.gui.renderers.GrTreeCellRenderer;
import cz.zcu.kiv.efps.registry.gui.renderers.LrAssignmentTreeCellRenderer;
import cz.zcu.kiv.efps.registry.gui.renderers.LrTreeCellRenderer;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The class extends JTree for representing tree local registers and their values. <br>
 * Date: 14. 10. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class JValueTree extends JTree {

    /** Serial version. */
    private static final long serialVersionUID = -540928087381008541L;

    /** The tree model. */
    private TreeModel model;

    /**
     * Create new JTree from local registers and their values.
     * @param gr The global register which local register's values will be in tree
     */
    public JValueTree(final GR gr) {
        super();
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        updateData(gr);
        addRenderer();
    }

    /**
     * Update tree according to efp.
     * @param efp The efp which values will be in tree
     */
    public final void updateData(final EFP efp) {
        if (efp == null) {
            model = null;
            return;
        }
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(efp.getGr());
        List<LR> lrs = EfpRegistryGUI.getEfpClient().findLrForGr(efp.getGr());
        // for all LR in the GR
        for (LR lr : lrs) {
            List<LrAssignment> assignments = EfpRegistryGUI.getEfpClient()
                    .findLrAssignments(efp.getId(), lr.getId());
            if (lr.getParentLR() != null) {
                assignments.removeAll(EfpRegistryGUI.getEfpClient().findLrAssignments(
                        efp.getId(), lr.getParentLR().getId()));
            }
            if (!assignments.isEmpty()) {
                DefaultMutableTreeNode node = addLR(root, lr);
                for (LrAssignment assign : assignments) {
                    node.add(new DefaultMutableTreeNode(assign));
                }
            }
        }
        model = new DefaultTreeModel(root);
        this.setModel(model);
        expandAll();
    }

    /**
     * Update tree according to efp.
     * @param gr The global register which local register's values will be in tree
     */
    public final void updateData(final GR gr) {
        if (gr == null) {
            model = null;
            return;
        }
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(gr);
        List<LR> lrs = EfpRegistryGUI.getEfpClient().findLrForGr(gr);
        // for all LR in the GR
        for (LR lr : lrs) {
            List<LrAssignment> assignments = EfpRegistryGUI.getEfpClient()
                    .findLrAssignments(lr.getId());
            if (lr.getParentLR() != null) {
                assignments.removeAll(EfpRegistryGUI.getEfpClient().findLrAssignments(
                        lr.getParentLR().getId()));
            }
            DefaultMutableTreeNode node = addLR(root, lr);
            if (!assignments.isEmpty()) {
                for (LrAssignment assign : assignments) {
                    node.add(new DefaultMutableTreeNode(assign));
                }
            }
        }
        model = new DefaultTreeModel(root);
        this.setModel(model);
        expandAll();
    }

    /**
     * Expand all nodes.
     */
    private void expandAll() {
        for (int i = 0; i < this.getRowCount(); i++) {
            this.expandRow(i);
        }
    }

    /**
     * Add local register to tree with parents if not exits.
     * @param root Root of tree
     * @param lr Adding local register
     * @return Node of local register
     */
    private DefaultMutableTreeNode addLR(final DefaultMutableTreeNode root, final LR lr) {
        DefaultMutableTreeNode node = findNode(root, lr);
        if (node != null) {
            return node;
        }
        node = new DefaultMutableTreeNode(lr);
        if (lr.getParentLR() == null) {
            root.add(node);
            return node;
        }
        DefaultMutableTreeNode parent = findNode(root, lr.getParentLR());
        if (parent == null) {
            parent = addLR(root, lr.getParentLR());
        }
        parent.add(node);
        return node;
    }

    /**
     * Find node of parentLR.
     * @param rootNode The root node
     * @param lookNode Search node
     * @return The mode of parentLR
     */
    private DefaultMutableTreeNode findNode(final DefaultMutableTreeNode rootNode,
            final Object lookNode) {
        if (rootNode.getUserObject().equals(lookNode)) {
            return rootNode;
        }
        if (rootNode.isLeaf()) {
            return null;
        }
        for (int i = 0; i < rootNode.getChildCount(); i++) {
            DefaultMutableTreeNode pom = findNode(
                    (DefaultMutableTreeNode) rootNode.getChildAt(i), lookNode);
            if (pom != null) {
                return pom;
            }
        }
        return null;
    }

    /**
     * Set cell renderer.
     */
    private void addRenderer() {
        setCellRenderer(new DefaultTreeCellRenderer() {

            private static final long serialVersionUID = 1L;

            @Override
            public Component getTreeCellRendererComponent(final JTree tree,
                    final Object value, final boolean selected, final boolean expanded,
                    final boolean leaf, final int row, final boolean hasFocus) {

                DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

                // LR
                if (node.getUserObject() instanceof LR) {
                    return new LrTreeCellRenderer().getTreeCellRendererComponent(tree,
                            value, selected, expanded, leaf, row, hasFocus);
                }

                // Assignment
                if (node.getUserObject() instanceof LrAssignment) {
                    return new LrAssignmentTreeCellRenderer()
                            .getTreeCellRendererComponent(tree, value, selected,
                                    expanded, leaf, row, hasFocus);
                }

                // GR
                if (node.getUserObject() instanceof GR) {
                    return new GrTreeCellRenderer().getTreeCellRendererComponent(tree,
                            value, selected, expanded, leaf, row, hasFocus);
                }

                return super.getTreeCellRendererComponent(tree, value, selected,
                        expanded, leaf, row, hasFocus);
            }
        });

    }
}
