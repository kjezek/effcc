/**
 *
 */
package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class CompCStub implements ComponentEfpAccessor {

    /** Provided feature. */
    private Feature feature4 = new BasicFeature("f-4",
            AssignmentSide.PROVIDED, "component", true);

    /** Provided feature. */
    private Feature feature5 = new BasicFeature("f-5",
            AssignmentSide.PROVIDED, "component", true);

    /** Required feature. */
    private Feature feature6 = new BasicFeature("f-6",
            AssignmentSide.REQUIRED, "component", true);

    /** This optional feature is not fulfilled. */
//    private Feature optionalFeature = new BasicFeature("optional",
//            AssignmentSide.REQUIRED, "component", false);

    /** LR. */
    private LR lr = new LR("LR 1", null, null, "LR1");

    /** Stub data storage. */
    private Map<Feature, List<EFP>> efpsOnFeature;
    /** Stub data storage. */
    private Map<Feature, List<EfpAssignment>> assignmentsOnFeature;
    /** Stub data storage. */
    private List<Feature> features;

    /** Creates the instance with test data. */
    public CompCStub() {

        createStubData();
    }


    /** It creates test data. */
    private void createStubData() {
        efpsOnFeature = new HashMap<Feature, List<EFP>>();
        assignmentsOnFeature = new HashMap<Feature, List<EfpAssignment>>();
        features = new ArrayList<Feature>();

        features.add(feature4);
        features.add(feature5);
        features.add(feature6);
//        features.add(optionalFeature);


        EFP efp42 = new SimpleEFP("efp-4-2", null, null, null, null);
        LrSimpleAssignment lrAssign = new LrSimpleAssignment(efp42, "low", new EfpNumber(10), lr);
        EfpAssignedValue val5 = new EfpNamedValue(lrAssign);
        EfpAssignment efp42Assign = new EfpAssignment(efp42, val5, feature4);

        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);
        EfpAssignedValue val4 = new EfpDirectValue(new EfpNumber(4));
        EfpAssignment efp41Assign = new EfpAssignment(efp41, val4, feature4);

        assignmentsOnFeature.put(feature4, Arrays.asList(efp41Assign, efp42Assign));
        efpsOnFeature.put(feature4, Arrays.asList(efp41, efp42));

        // No EFPs on this features
        efpsOnFeature.put(feature5, new ArrayList<EFP>());
        efpsOnFeature.put(feature6, new ArrayList<EFP>());
//        efpsOnFeature.put(optionalFeature, new ArrayList<EFP>());
    }


    @Override
    public List<EFP> getEfps(final Feature feature) {
        return efpsOnFeature.get(feature);
    }

    @Override
    public List<Feature> getAllFeatures() {
        return features;
    }



    @Override
    public EfpAssignedValue getAssignedValue(final Feature feature, final EFP efp, final LR lr) {
        for (EfpAssignment assig : assignmentsOnFeature.get(feature)) {
            if (assig.getEfp().equals(efp)) {
                return assig.getEfpValue();
            }
        }
        return null;
    }


    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>(Arrays.asList(lr));
    }

    @Override
    public void close() {
    }


}
