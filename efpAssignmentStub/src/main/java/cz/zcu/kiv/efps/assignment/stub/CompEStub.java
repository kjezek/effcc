/**
 *
 */
package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * @author Lukas Vlcek
 *
 */
public class CompEStub implements ComponentEfpAccessor {


    private LR lr = new LR("LR 1", null, null, "LR1");

    /** Provided feature. */
    private Feature feature8 = new BasicFeature("f-8",
            AssignmentSide.PROVIDED, "component", true);



    /** Stub data storage. */
    private Map<Feature, List<EFP>> efpsOnFeature;
    /** Stub data storage. */
    private Map<Feature, List<EfpAssignment>> assignmentsOnFeature;
    /** Stub data storage. */
    private List<Feature> features;

    /** Creates the instance with test data. */
    public CompEStub() {

        createStubData();
    }


    /** It creates test data. */
    private void createStubData() {
        efpsOnFeature = new HashMap<Feature, List<EFP>>();
        assignmentsOnFeature = new HashMap<Feature, List<EfpAssignment>>();
        features = new ArrayList<Feature>();

        features.add(feature8);
        EFP efp81 = new SimpleEFP("efp-8-1", null, null, null, null);


        EfpAssignedValue val8 = null;
        EfpAssignment efp81Assign = new EfpAssignment(efp81, val8, feature8);

        assignmentsOnFeature.put(feature8, Arrays.asList(efp81Assign));
        efpsOnFeature.put(feature8, Arrays.asList(efp81));
    }


    @Override
    public List<EFP> getEfps(final Feature feature) {
        return efpsOnFeature.get(feature);
    }

    @Override
    public List<Feature> getAllFeatures() {
        return features;
    }



    @Override
    public EfpAssignedValue getAssignedValue(final Feature feature, final EFP efp, final LR lr) {
        for (EfpAssignment assig : assignmentsOnFeature.get(feature)) {
            if (assig.getEfp().equals(efp)) {
                return assig.getEfpValue();
            }
        }
        return null;
    }


    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>(Arrays.asList(lr));
    }

    @Override
    public void close() {
    }


}

