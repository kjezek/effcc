/**
 *
 */
package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class CompAStub implements ComponentEfpAccessor {

    /** Main feature. */
    private Feature compFeature = new BasicFeature(
            "Component-A", AssignmentSide.PROVIDED, "component", true);

    /** Required featur 1. */
    private Feature feature1 = new BasicFeature("f-1",
            AssignmentSide.REQUIRED, "component", true, compFeature);

    /** Required feature 2. */
    private Feature feature2 = new BasicFeature("f-2",
            AssignmentSide.REQUIRED, "component", true, compFeature);

    /** A feature representing a single method. */
    private Feature methodFeatureA1 = new BasicFeature(
            "Method-A", AssignmentSide.PROVIDED, "method", true, feature1);

    /** A feature representing a single method. */
    private Feature methodFeatureA2 = new BasicFeature(
            "Method-A", AssignmentSide.PROVIDED, "method", true, feature2);

    /** Stub data storage. */
    private Map<Feature, List<EFP>> efpsOnFeature;
    /** Stub data storage. */
    private Map<Feature, List<EfpAssignment>> assignmentsOnFeature;
    /** Stub data storage. */
    private List<Feature> features;

    /** Creates the instance with test data. */
    public CompAStub() {

        createStubData();
    }


    /** It creates test data. */
    private void createStubData() {
        efpsOnFeature = new HashMap<Feature, List<EFP>>();
        assignmentsOnFeature = new HashMap<Feature, List<EfpAssignment>>();
        features = new ArrayList<Feature>();

        features.add(compFeature);
        features.add(feature1);
        features.add(feature2);
        features.add(methodFeatureA1);
        features.add(methodFeatureA2);

        // feature 1
        // required EFPs on features 1
        EFP efp11 = new SimpleEFP("efp-1-1", null, null, null, null);
        EFP efp12 = new SimpleEFP("efp-1-2", null, null, null, null);
        EFP efp13 = new SimpleEFP("efp-1-3", null, null, null, null);
        efpsOnFeature.put(feature1, Arrays.asList(efp11, efp12, efp13));

        // values of EFPs on required feature 1,
        EfpAssignedValue val1 = new EfpDirectValue(new EfpNumber(3));
        EfpAssignedValue val2 = new EfpDirectValue(new EfpNumber(4));
        EfpAssignedValue val3 = new EfpDirectValue(new EfpNumber(4));
        EfpAssignment efp11Assign = new EfpAssignment(efp11, val1, feature1);
        EfpAssignment efp12Assign = new EfpAssignment(efp12, val2, feature1);
        EfpAssignment efp13Assign = new EfpAssignment(efp13, val3, feature1);
        assignmentsOnFeature.put(feature1, Arrays.asList(efp11Assign, efp12Assign, efp13Assign));

        // Feature 2 has not EFPs
        efpsOnFeature.put(feature2, new ArrayList<EFP>());
        assignmentsOnFeature.put(feature2, new ArrayList<EfpAssignment>());

    }

    @Override
    public List<EFP> getEfps(final Feature feature) {
        List<EFP> efps = efpsOnFeature.get(feature);

        if (efps == null) {
            efps = new ArrayList<EFP>();
        }

        return efps;
    }

    @Override
    public List<Feature> getAllFeatures() {
        return features;
    }

    @Override
    public EfpAssignedValue getAssignedValue(final Feature feature, final EFP efp, final LR lr) {
        for (EfpAssignment assig : assignmentsOnFeature.get(feature)) {
            if (assig.getEfp().equals(efp)) {
                return assig.getEfpValue();
            }
        }
        return null;
    }

    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>();
    }


    @Override
    public void close() {
    }

}
