
package cz.zcu.kiv.efps.assignment.stub;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;

/**
 * A stub implementation serving for test purposes.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class StubAssignmentImpl implements EfpAwareComponentLoader {


    /** A name of a test component. */
    public static final String COMPONENT_A = "A";

    /** A name of a test component. */
    public static final String COMPONENT_B = "B";

    /** A name of a test component. */
    public static final String COMPONENT_C = "C";

    /** Test component ID. */
    public static final String COMPONENT_D = "D";

    /** Test component ID. */
    public static final String COMPONENT_E = "E";

    /** Map of stub implementations. */
    private static Map<String, ComponentEfpAccessor> stubData
            = new HashMap<String, ComponentEfpAccessor>();

    static {

        stubData.put(COMPONENT_A, new CompAStub());
        stubData.put(COMPONENT_B, new CompBStub());
        stubData.put(COMPONENT_C, new CompCStub());
        stubData.put(COMPONENT_D, new CompDStub());
        stubData.put(COMPONENT_E, new CompEStub());
    }

    @Override
    public ComponentEfpAccessor loadForRead(final String component) {
        ComponentEfpAccessor accesor = stubData.get(component);


        if (accesor == null) {
            accesor = new StubDataAccesor(component);
        }

        return accesor;
    }

    @Override
    public ComponentEFPModifier loadForUpdate(final String component) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MatchingFunction createMatchingFunction(final List<String> components) {
        return MatchingFunction.DEFAULT_MATCHING_FUNCTION;
    }
}
