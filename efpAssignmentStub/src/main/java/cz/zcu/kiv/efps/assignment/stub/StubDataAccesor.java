package cz.zcu.kiv.efps.assignment.stub;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Class for access to data.
 * @author Lukas Vlcek
 *
 */
public class StubDataAccesor implements ComponentEfpAccessor {

    /**
     * Name of component.
     */
    private String component;

    /**
     * reference to data.
     */
    private StubData data;

    /**
     * Constructor.
     * @param component name of component
     */
    public StubDataAccesor(final String component) {
        this.component = component;
        data = new StubData();
    }

    public List<EfpAssignment> getAssignmentsFromFeature(final Feature feature) {
        if (component.equals("ComponentA")) {
            return data.componentA();
               } else if (component.equals("ComponentB")) {
            return data.componentB();
               } else if (component.equals("math")) {
            return data.math();
               } else if (component.equals("math1")) {
            return data.math1();
               }
               return null;


    }

    @Override
    public List<EFP> getEfps(final Feature feature) {
        return data.featureEfp(feature);
    }

    @Override
    public List<Feature> getAllFeatures() {
        if (component.equals("ComponentA")) {
            return data.componentAFeature();
               } else if (component.equals("ComponentB")) {
            return data.componentBFeature();
               }
               return null;
    }


    @Override
    public EfpAssignedValue getAssignedValue(Feature feature, EFP efp, LR lr) {
        List<EfpAssignment> assignments = getAssignmentsFromFeature(feature);
        for (EfpAssignment assig : assignments) {
            if (assig.getEfp().equals(efp)) {
                return assig.getEfpValue();
            }

        }
        return null;
    }

    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>();
    }

    @Override
    public void close() {
    }

}
