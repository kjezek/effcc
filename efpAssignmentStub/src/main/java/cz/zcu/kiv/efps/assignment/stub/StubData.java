package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 *
 * @author Lukas Vlcek <vlceklu@students.zcu.cz>
 *
 */

public class StubData {

    /** list of EfpAssignment. */
    private List<EfpAssignment> listAssignment;

    /**
     * Constructor, here we will create new list.
     */
    public StubData() {
        listAssignment = new ArrayList<EfpAssignment>();
    }

    /**
     * feature from directvalue.
     * @return list
     */
    public final List<Feature> componentAFeature()  {
        Feature feature = new BasicFeature("feature",
                AssignmentSide.REQUIRED, "component", true);
        Feature featurey = new BasicFeature("featureAReq",
                AssignmentSide.REQUIRED, "component", true);
        return Arrays.asList(feature, featurey);
    }
    /**
     * Direct value - data from EfpAssignedValueTest.
     *
     * @return list of EfpAssignments
     */
    public final List<EfpAssignment> componentA() {
        List<Feature> list = componentAFeature();
        EfpAssignedValue x = new EfpDirectValue(new EfpNumber(3));
        EfpAssignedValue y = new EfpDirectValue(new EfpNumber(4));

        List<EFP> efpFeature1 = featureEfp(list.get(0));
        List<EFP> efpFeature2 = featureEfp(list.get(1));

        EfpAssignment xAssign = new EfpAssignment(efpFeature1.get(0), x, list.get(0));
        EfpAssignment yAssign = new EfpAssignment(efpFeature2.get(0), y, list.get(1));

        listAssignment.add(xAssign);
        listAssignment.add(yAssign);
        return listAssignment;
    }

    /**
     * Direct value - data from EfpAssignedValueTest.
     *
     * @return list of EfpAssignments
     */
    public final List<EfpAssignment> componentB() {
        List<Feature> list = componentBFeature();
        EfpAssignedValue x = new EfpDirectValue(new EfpNumber(7));

        List<EFP> efpFeature1 = featureEfp(list.get(0));

        EfpAssignment directNumber = new EfpAssignment(efpFeature1.get(0),
                x, list.get(0));

        listAssignment.add(directNumber);
        return listAssignment;
    }


    /**
     * other features from direct value.
     * @return list
     */
    public final List<Feature> componentBFeature()  {
        Feature feature = new BasicFeature("feature",
                AssignmentSide.PROVIDED, "component", true);
        return Arrays.asList(feature);
    }

    /**
     * return efp for feature.
     * @param feature feature
     * @return list efp
     */
    public final List<EFP> featureEfp(final Feature feature) {
        List<EFP> list = new ArrayList<EFP>();
        Feature featur = new BasicFeature("feature",
                AssignmentSide.REQUIRED, "component", true);
        Feature featurey = new BasicFeature("featureAReq",
                AssignmentSide.REQUIRED, "component", true);
        Feature featurex = new BasicFeature("feature",
                AssignmentSide.PROVIDED, "component", true);
        if (feature.equals(featur)) {
            list.add(new SimpleEFP("EFPFeaureA", null, null, null, null));
        }
        if (feature.equals(featurex)) {
            list.add(new SimpleEFP("EFPFeatureB", null, null, null, null));
        }
        if (feature.equals(featurey)) {
            list.add(new SimpleEFP("EFPFeatureB_Req", null, null, null, null));
        }
        return list;
    }



    /**
     * Math formula - data from EfpAssignedValueTest.
     *
     * @return list of EfpAssignments
     */
    public final List<EfpAssignment> math() {
        Feature provFeature = new BasicFeature("provided_math",
                AssignmentSide.PROVIDED, "component", true);
        Feature reqFeature = new BasicFeature("required_math",
                AssignmentSide.REQUIRED, "component", true);

        EFP provEfp = new SimpleEFP("provEfp", null, null, null, null);
        EFP reqEfp = new SimpleEFP("reqEfp", null, null, null, null);

        EfpAssignedValue math = new EfpFormulaValue("pow(required_math_reqEfp,2)",
                new EfpFeature(reqEfp, reqFeature));
        final EfpAssignment provAssig = new EfpAssignment(provEfp, math , provFeature);
        final EfpAssignment reqAssig = new EfpAssignment(reqEfp, null, reqFeature);


        listAssignment = Arrays.asList(provAssig, reqAssig);
        return listAssignment;
    }



    /**
     * Math formula - data from EfpAssignedValueTest.
     *
     * @return list of EfpAssignments
     */
    public final List<EfpAssignment> math1() {
        Feature feature = new BasicFeature("math",
                AssignmentSide.PROVIDED, "component", true);
        Feature featurey = new BasicFeature("mathY",
                AssignmentSide.REQUIRED, "component", true);
        EfpAssignedValue y = new EfpDirectValue(new EfpNumber(9));

        EFP yEfp = new SimpleEFP("y", null, null, null, null);

        final EfpAssignment yAssig = new EfpAssignment(yEfp, y, featurey);

        EfpAssignedValue math = new EfpFormulaValue("sqrt(mathY_y)", new EfpFeature(yEfp, featurey));

        EFP mathEfp = new SimpleEFP("math",  null, null, null, null);

        EfpAssignment mathAssig = new EfpAssignment(mathEfp,
                math, feature);
        listAssignment = Arrays.asList(mathAssig, yAssig);
        return listAssignment;
    }



}
