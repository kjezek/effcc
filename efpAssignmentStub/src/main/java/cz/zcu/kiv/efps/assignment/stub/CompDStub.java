/**
 *
 */
package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;

import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;

import cz.zcu.kiv.efps.types.lr.LR;

import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * @author Lukas Vlcek
 *
 */
public class CompDStub implements ComponentEfpAccessor {

    private LR lr = new LR("LR 1", null, null, "LR1");

    /** Provided feature. */
    private Feature feature7 = new BasicFeature("f-7",
            AssignmentSide.PROVIDED, "component", true);


    /** Required feature. */
    private Feature feature8 = new BasicFeature("f-8",
            AssignmentSide.REQUIRED, "component", true);



    /** Stub data storage. */
    private Map<Feature, List<EFP>> efpsOnFeature;
    /** Stub data storage. */
    private Map<Feature, List<EfpAssignment>> assignmentsOnFeature;
    /** Stub data storage. */
    private List<Feature> features;

    /** Creates the instance with test data. */
    public CompDStub() {

        createStubData();
    }


    /** It creates test data. */
    private void createStubData() {
        efpsOnFeature = new HashMap<Feature, List<EFP>>();
        assignmentsOnFeature = new HashMap<Feature, List<EfpAssignment>>();
        features = new ArrayList<Feature>();

        features.add(feature7);
        features.add(feature8);
        EFP efp81 = new SimpleEFP("efp-8-1", null, null, null, null);

        EFP efp71 = new SimpleEFP("efp-7-1", null, null, null, null);
        EfpAssignedValue val7 = new EfpFormulaValue("2 * component.f-8_efp-8-1",
                new EfpFeature(efp81, feature8));
        EfpAssignment efp71Assign = new EfpAssignment(efp71, val7, feature7);

        assignmentsOnFeature.put(feature7, Arrays.asList(efp71Assign));
        efpsOnFeature.put(feature7, Arrays.asList(efp71));

        efpsOnFeature.put(feature8, Arrays.asList(efp81));
        assignmentsOnFeature.put(feature8, new ArrayList<EfpAssignment>());

    }


    @Override
    public List<EFP> getEfps(final Feature feature) {
        return efpsOnFeature.get(feature);
    }

    @Override
    public List<Feature> getAllFeatures() {
        return features;
    }



    @Override
    public EfpAssignedValue getAssignedValue(final Feature feature, final EFP efp, final LR lr) {
        for (EfpAssignment assig : assignmentsOnFeature.get(feature)) {
            if (assig.getEfp().equals(efp)) {
                return assig.getEfpValue();
            }
        }
        return null;
    }


    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>(Arrays.asList(lr));
    }

    @Override
    public void close() {
    }


}
