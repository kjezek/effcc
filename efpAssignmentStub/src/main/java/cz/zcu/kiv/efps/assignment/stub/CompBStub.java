package cz.zcu.kiv.efps.assignment.stub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class CompBStub implements ComponentEfpAccessor {

    /** Required featur 1. */
    private Feature feature1 = new BasicFeature("f-1",
            AssignmentSide.PROVIDED, "component", true);

    /** Required feature 2. */
    private Feature feature2 = new BasicFeature("f-2",
            AssignmentSide.PROVIDED, "component", true);

    private Feature feature4 = new BasicFeature("f-4",
            AssignmentSide.REQUIRED, "component", true);

    private LR lr = new LR("LR 1", null, null, "LR1");

    /** Stub data storage. */
    private Map<Feature, List<EFP>> efpsOnFeature;
    /** Stub data storage. */
    private Map<Feature, List<EfpAssignment>> assignmentsOnFeature;
    /** Stub data storage. */
    private List<Feature> features;

    /** Creates the instance with test data. */
    public CompBStub() {

        createStubData();
    }


    /** It creates test data. */
    private void createStubData() {
        efpsOnFeature = new HashMap<Feature, List<EFP>>();
        assignmentsOnFeature = new HashMap<Feature, List<EfpAssignment>>();
        features = new ArrayList<Feature>();

        features.add(feature1);
        features.add(feature2);
        features.add(feature4);

        // feature 1
        // provided EFPs on features 1
        EFP efp11 = new SimpleEFP("efp-1-1", null, null, null, null);
        EFP efp13 = new SimpleEFP("efp-1-3", null, null, null, null);
        efpsOnFeature.put(feature1, Arrays.asList(efp11, efp13));

        // only EFP without a value
        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);

        // values of EFPs on provided feature 1,
        EfpAssignedValue val2 = new EfpDirectValue(new EfpNumber(4));
        // value on efp3 is computed as a math formula via efp4
        EfpAssignedValue val3 = new EfpFormulaValue("2 * component.f-4_efp-4-1", new EfpFeature(efp41, feature4));
        EfpAssignment efp11Assign = new EfpAssignment(efp11, val2, feature1);
        EfpAssignment efp13Assign = new EfpAssignment(efp13, val3, feature1);
        assignmentsOnFeature.put(feature1, Arrays.asList(efp11Assign, efp13Assign));

        // Feature 2 has not EFPs
        efpsOnFeature.put(feature2, new ArrayList<EFP>());
        assignmentsOnFeature.put(feature2, new ArrayList<EfpAssignment>());

        EFP efp42 = new SimpleEFP("efp-4-2", null, null, null, null);
        LrSimpleAssignment lrAssign = new LrSimpleAssignment(efp42, "low", new EfpNumber(10), lr);
        EfpAssignedValue val5 = new EfpNamedValue(lrAssign);
        EfpAssignment efp42Assign = new EfpAssignment(efp42, val5, feature4);
        assignmentsOnFeature.put(feature4, Arrays.asList(efp42Assign));

        efpsOnFeature.put(feature4, Arrays.asList(efp41, efp42));
    }


    @Override
    public List<EFP> getEfps(final Feature feature) {
        return efpsOnFeature.get(feature);
    }

    @Override
    public List<Feature> getAllFeatures() {
        return features;
    }


    @Override
    public EfpAssignedValue getAssignedValue(Feature feature, EFP efp, LR lr) {
        for (EfpAssignment assig : assignmentsOnFeature.get(feature)) {
            if (assig.getEfp().equals(efp)) {

                return assig.getEfpValue();
            }
        }
        return null;
    }


    @Override
    public Set<LR> getLRs() {
        return new HashSet<LR>(Arrays.asList(lr));
    }

    @Override
    public void close() {
    }

}