# Overview  

EFFCC stands for Extra-functional Featured Compatibility Checks. It is a toolset allowing to define, use, and evaluate extra-functional (also refereed as non-functional) properties.

The main aim is to bring extra-functional properties into practical frameworks such as **OSGi** or **Spring**. 

What it is good for? API compatibility is commonly understand as must to handle complexity of independently evolved software libraries (aka Java JAR files). But the syntactic API compatibility (= methods signatures not changed) is not enough as an evolved library should not worsen its behaviour. 

It is where extra-functional properties play role. They allow for **annotating API** with additional **meta-information** about API behaviour. They may be then used for tools evaluating **compatibility** of an API. 

EFFCC covers all use-cases from definition, application and compatibility checks in several modules

* **EFFCC Registry** -- it is a server data storage keeping definitions of the properties
* **EFFCC Assignment** -- it is a client tool helping with annotating libraries with the properties
* **EFFCC Comparator** -- it is a main evaluation engine that takes care of checking properies compatibility

The whole architecture follows software development process. First a domain expert must fill-in definitions of properties in the repository, then developers can enrich their code with the annotations. Finally, runtime or build-time guards assure no non-functional constrain leading to incompatible composition is broken.

Most parts of EFFCC are generic, but contain a set of plugins for existing systems. So far prototypes for OSGi and Spring are available. 



# Build #

The project is build by ``java`` and ``maven``. It can be obtained and build simply by typing: 


```
#!java

git clone git@bitbucket.org:kjezek/effcc.git
cd effcc
mvn clean install
```

The last stable version can be get by:

```
#!java
git checkout effcc-1.4.2

```

# Documentation #

As the system is complex and innovating, more detailed information are provided.

* Kamil Jezek, Premek Brada, Lukas Holy: **Enhancing OSGi with Explicit, Vendor Independent Extra-Functional Properties**, in proceedings of TOOLS 2012, *Volume 7304 of the series Lecture Notes in Computer Science* pp 108-123 [[pdf](http://link.springer.com/chapter/10.1007%2F978-3-642-30561-0_9)], [[manuscript](https://bitbucket.org/kjezek/effcc/raw/e918104b0f2fecdb5796baddf35b4ad9d46305ff/doc/jezek2012enhancing-osgi.pdf)]
* Kamil Ježek, Premek Brada: **Formalisation of a Generic Extra-Functional Properties Framework**, book chapter, *Volume 275 of the series Communications in Computer and Information Science pp 203-217* [[pdf](http://link.springer.com/chapter/10.1007%2F978-3-642-32341-6_14)], [[manuscript](https://bitbucket.org/kjezek/effcc/raw/e918104b0f2fecdb5796baddf35b4ad9d46305ff/doc/jezek2011formalisation.pdf)]
* Kamil Jezek: **Extra-Functional Properties Support For a Variety of Component Models**, Phd Thesis, University of West Bohemia, Plzen, [[pdf](https://bitbucket.org/kjezek/effcc/raw/e918104b0f2fecdb5796baddf35b4ad9d46305ff/doc/jezek2012efp-thesis.pdf)]