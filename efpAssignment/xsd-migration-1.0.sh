#!/bin/bash

FILE=$1
UNPACK=$FILE".dir"
XML=$UNPACK"/META-INF/repository.xml"
TMP=$XML".tmp"

OLD=( "globalRegistr" "properties" "simpleEfp" "derivedEfp" "listOfRepositories" "localRegistr" "otherValues" "otherValue" "localRepository" )
NEW=( "gr" "efps" "simple" "derived" "registries" "lr" "parameters" "parameter" "efpMirror" )

unzip $FILE -d $UNPACK

index=0
for j in ${OLD[@]}; do

  #echo "Replacing" $j " -> " ${NEW[$index]}

  sed {s/$j/${NEW[index]}/}  $XML > $TMP
  mv $TMP $XML

  index=$(($index+1))
done

CURRENT=`pwd`
cd $UNPACK
zip -r tmp.zip *
cd $CURRENT

mv $UNPACK/tmp.zip $FILE

#clean
rm -rf $UNPACK

