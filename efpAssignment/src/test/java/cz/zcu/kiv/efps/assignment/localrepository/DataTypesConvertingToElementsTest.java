package cz.zcu.kiv.efps.assignment.localrepository;

import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.assignment.repomirror.support.DataTypesReconstructor;
import cz.zcu.kiv.efps.assignment.repomirror.support.ElementsCreatorSetter;
import cz.zcu.kiv.efps.assignment.repository.generated.GammaElement;
import cz.zcu.kiv.efps.assignment.repository.generated.LRElement;
import cz.zcu.kiv.efps.assignment.repository.generated.LrSimpleAssignmentElement;
import cz.zcu.kiv.efps.assignment.repository.generated.MetaElement;
import cz.zcu.kiv.efps.assignment.repository.generated.RepositoryElement;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.NumberGamma;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;

/**
 * Tests of methods classes DataTypesReconstructor and ElementsCreaterSetter for converting
 * datatypes into elements to local repository and back.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class DataTypesConvertingToElementsTest {

    /** Reference to DataTypesReconstructor. */
    private DataTypesReconstructor dtReconstructor = new DataTypesReconstructor();

    /** Reference to ElementsCreaterSetter. */
    private ElementsCreatorSetter elemsCreaterSetter = new ElementsCreatorSetter();

    /** GR. */
    private GR gr = new GR(1, "High performance", "HP");

    /** LR. */
    private LR lr = new LR(1, "local repository 1", gr, null, "LR1");

    /** Meta. */
    private Meta meta = new Meta("ms", "high", "low");

    /** EFP. */
    private SimpleEFP efp = new SimpleEFP(1, "response_time", EfpNumber.class,
            meta, new UserGamma("gamma"), gr);

    /** LrSimpleAssignment. */
    private LrSimpleAssignment lrAssignment =
        new LrSimpleAssignment(2, efp, meta.getNames().get(0), new EfpNumber(15), lr);



    /**
     * Test converting GR.
     */
    @Test
    public void testConvertingGR() {
        RepositoryElement re = elemsCreaterSetter.createAndSetRepositoryElement(gr);

        GR grNew = dtReconstructor.reconstructGR(re);

        Assert.assertEquals(gr, grNew);
    }

    /**
     * Test converting LR.
     */
    @Test
    public void testConvertingLR() {
        LRElement lre = elemsCreaterSetter.createAndSetLRElement(lr);

        LR newLr = dtReconstructor.reconstructLR(lre, lr.getGr());

        Assert.assertEquals(lr, newLr);
    }


    /**
     * Test converting Meta.
     */
    @Test
    public void testConvertingMeta() {
        MetaElement me = elemsCreaterSetter.createAndSetMetaElement(meta);

        Meta newMeta = dtReconstructor.reconstructMeta(me);

        Assert.assertEquals(meta, newMeta);
    }


    /**
     * Test converting Gamma.
     */
    @Test
    public void testConvertingGamma() {
        Gamma gamma = new NumberGamma();
        Gamma gamma2 = new UserGamma("user gamma function");

        GammaElement ge = elemsCreaterSetter.createAndSetGammaElement(gamma);
        GammaElement ge2 = elemsCreaterSetter.createAndSetGammaElement(gamma2);

        Gamma newGamma = dtReconstructor.reconstructGamma(ge);
        Gamma newGamma2 = dtReconstructor.reconstructGamma(ge2);

        Assert.assertEquals(gamma.getClass().getName(), newGamma.getClass().getName());
        Assert.assertEquals(gamma2, newGamma2);
    }


    /**
     * Test converting EFP.
     * @throws MissingElementException Missing some required Element from local repository.
     */
    @Test
    public void testConvertingEFP()  {
        Object simpleEfpElem =
            elemsCreaterSetter.createAndSetSimpleEFPElement(efp);

        EFP efpNew = dtReconstructor.reconstructEFP(null, gr, simpleEfpElem);

        Assert.assertEquals(efp, efpNew);
    }


    /**
     * Test converting LrAssignment.
     */
    @Test
    public void testConvertingLrAssignment() {
        LrSimpleAssignmentElement lrAssignElem =
            elemsCreaterSetter.createAndSetLrSimAssignElement(lrAssignment);

        LrSimpleAssignment newLrAssignment =
            dtReconstructor.reconstructLrSimpleAssignment(lrAssignElem, efp, lr);

        Assert.assertEquals(lrAssignment, newLrAssignment);
    }
}
