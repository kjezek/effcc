package cz.zcu.kiv.efps.assignment.extension;

import java.io.File;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.repomirror.api.MirroredDataManipulator;
import cz.zcu.kiv.efps.assignment.repomirror.impl.XMLDataManipulator;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;
import org.junit.rules.TestName;


/**
 * Test of the class CommonBundleFilesManagerImpl for manipulation with bundle.
 *
 * Date: 21.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CommonBundleFilesManagerImplTest {
    /** Path to original jar file. */
    private static final String PATH_TO_JAR_ORIGINAL = "./src/test/resources/testJar.jar";

    /** GRs. */
    private GR gr = new GR(1, "glob.register 1", "GR1");
    /** Simple EFP. */
    private EFP efp = new SimpleEFP(1, "response_time", EfpNumber.class,
            new Meta("ms", "high", "low"), new UserGamma("gamma"), gr);

    /** Path to test jar file in the actual test. */
    private String pathToJarCopyFile = "";

    /** Name of the actual test.*/
    @Rule
    public TestName name = new TestName();

    /**
     * Does testing copy of jar.
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        pathToJarCopyFile = "./" + name.getMethodName() + "JARCOPY.jar";
        FileUtils.copyFile(new File(PATH_TO_JAR_ORIGINAL), new File(pathToJarCopyFile));
    }

    /**
     * Deletes test jar copy and local repository file.
     *
     * @throws IOException Error during delete of the test jar copy.
     */
    @After
    public void close() throws IOException {
        if(!new File(pathToJarCopyFile).delete()) {
            throw new IOException("Can't delete test JAR.");
        }
    }


    /**
     * Tests of edit a manifest file in bundle.
     * @throws IOException IO error.
     */
    @Test
    public void testOfEditManifestFile() throws IOException {
        String testHeader = "Test-Header2";
        String testValue = "test value2";
        AbstractEfpDataLocation bundleManager = new CommonBundleFilesManagerImpl(pathToJarCopyFile);

        ManifestHeaderHandler mfHandler = bundleManager.open()
            .getEfpData(new BundleManifestReader(bundleManager));
        mfHandler.writeSimpleHeader(testHeader, testValue);
        bundleManager.close();

        //reopening
        bundleManager = new CommonBundleFilesManagerImpl(pathToJarCopyFile);

        mfHandler = bundleManager.open()
        .getEfpData(new BundleManifestReader(bundleManager));

        String newValue = (String) mfHandler.readMainAttribute(testHeader);
        bundleManager.close();

        Assert.assertEquals(testValue, newValue);
    }

    /**
     * Tests of edit a local repository file in bundle.
     * @throws IOException IO error.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testOfEditLocalRepositoryFile() throws IOException {
        Feature feature = new BasicFeature("service.A", AssignmentSide.PROVIDED, null, true);
        EfpAssignment efpAssignment1 =
            new EfpAssignment(efp, new EfpDirectValue(new EfpNumber(2)), feature);


        AbstractEfpDataLocation bundleManager = new CommonBundleFilesManagerImpl(pathToJarCopyFile);

        MirroredDataManipulator xmlDataModifier =
            new XMLDataManipulator(bundleManager);
        int id = xmlDataModifier.addEfpAssignment(efpAssignment1);

        bundleManager.close();

        //reopening
        bundleManager = new CommonBundleFilesManagerImpl(pathToJarCopyFile);
        xmlDataModifier = new XMLDataManipulator(bundleManager);


        EFP newEfp = xmlDataModifier.readEFP(efp.getName(), efp.getGr().getId());
        EfpValueType efpValueType =
            EfpSerialiser.deserialize((Class<EfpValueType>) efp.getValueType(),
                   xmlDataModifier.readParamObject(id));
        EfpAssignment newEfpAssignment1 = new EfpAssignment(
                newEfp, new EfpDirectValue(efpValueType), feature);

        bundleManager.close();
        Assert.assertEquals(efpAssignment1, newEfpAssignment1);

    }
}
