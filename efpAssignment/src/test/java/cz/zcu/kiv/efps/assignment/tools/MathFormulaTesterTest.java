package cz.zcu.kiv.efps.assignment.tools;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.evaluator.FormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.AbstractGroovyFormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.MathFormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.MathFormulaHelper;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class MathFormulaTesterTest {

    /** A feature. */
    private Feature feature = new BasicFeature(
            "service", AssignmentSide.REQUIRED, "class", true);

    /** Formula. */
    private String formula = "sqrt(pow(class.service_x,2) + pow(class.service_y,2))";

    @Test
    public void testTindUsedEfps() {

        EFP xEfp = new SimpleEFP("x", null, null, null, null);
        EFP yEfp = new SimpleEFP("y", null, null, null, null);
        EFP invalidEfp = new SimpleEFP("z", null, null, null, null);

        EfpFeature xFe = new EfpFeature(xEfp, feature);
        EfpFeature yFe = new EfpFeature(yEfp, feature);
        EfpFeature invalidFe = new EfpFeature(invalidEfp, feature);

        MathFormulaHelper tester = new MathFormulaHelper(formula);
        List<EfpFeature> usedEfp = tester.findUsedEfps(xFe, invalidFe, yFe);

        // it should return only EFP really used in the formula.
        Assert.assertTrue(usedEfp.contains(xFe));
        Assert.assertTrue(usedEfp.contains(yFe));
        Assert.assertFalse(usedEfp.contains(invalidEfp));

        // Now, I may invoke evaluator only with valid efp+features
        EfpAssignedValue x = new EfpDirectValue(new EfpNumber(3)); // test value
        EfpAssignedValue y = new EfpDirectValue(new EfpNumber(4)); // test value
        EfpAssignment xAssign = new EfpAssignment(xEfp, x, feature);
        EfpAssignment yAssign = new EfpAssignment(yEfp, y, feature);

        FormulaEvaluator fe = new MathFormulaEvaluator(formula);
        EfpValueType result =  fe.evaluate(Arrays.asList(xAssign, yAssign));

        Assert.assertEquals(new EfpNumber(5), result);
    }
}
