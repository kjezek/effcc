package cz.zcu.kiv.efps.assignment.manifestparser;

import cz.zcu.kiv.efps.assignment.extension.manifest.FeatureParameters;
import cz.zcu.kiv.efps.assignment.extension.manifest.HeaderParser;
import junit.framework.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Tests parsing strings from the manifest file.
 *
 * Date: 18.3.2014
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class HeaderParserTest {

    /**
     * A name of test feature 1.
     */
    private final String feature1 = "example1.service";
    /**
     * A name of parameter 1 in the feature 1.
     */
    private final String feature1Param1 = "GR1.lang:String";
    /**
     * A value of parameter 1 in the feature 1.
     */
    private final String feature1Param1Value = "\"EN,USA\"";
    /**
     * A name of parameter 2 in the feature 1.
     */
    private final String feature1Param2 = "GR1.signed:Long";
    /**
     * A value of parameter 2 in the feature 1.
     */
    private final String feature1Param2Value = "1";
    /**
     * A name of parameter 3 in the feature 1.
     */
    private final String feature1Param3 = "_GR_2.efp1";
    /**
     * A value of parameter 3 in the feature 1.
     */
    private final String feature1Param3Value = "value1";
    /**
     * A name of test feature 2
     */
    private final String feature2 = "serviceB";
    /**
     * A name of parameter in the feature 2
     */
    private final String feature2Param1 = "Global2.efp2";
    /**
     * A value of parameter in the feature 2
     */
    private final String feature2Param1Value = "\"r;ete,zec\"";
    /**
     * A complex test header
     */
    private final String header = feature1 + ";" + feature1Param1 +"=" + feature1Param1Value + ";"
            + feature1Param2 + "=" + feature1Param2Value + ";" + feature1Param3
            + "=" + feature1Param3Value + "," + feature2 + ";" + feature2Param1 + "=" + feature2Param1Value + "";
    /**
     * A header which contains in one clause more features.
     */
    private final String headerMoreFeaturesOneClause = feature1 + ";" + feature2 + ";"
            + feature1Param1 +"=" + feature1Param1Value + ";"
            + feature1Param2 + "=" + feature1Param2Value;

    /**
     * A header which contains in one clause more features. Example 2.
     */
    private String headerMoreFeaturesOneClause2 = "cz.zcu.kiv.efps.CoCoMe.CD;valueID=europe;"
        + "feature-type=PACKAGE;feature-name=cz.zcu.kiv.osgi.example.cashdesk.scannercont"
        + "roller.productbarcodescannedevent;feature-version=0.0.0;efp=bar_code_type;"
        + "value:List<String>=\"EAN-13,EAN-8\"";

    /**
     * Test of parsing on simple headers.
     */
    @Test
    public void testParseSimpleHeader() {
        List<FeatureParameters> listOfFeatures = HeaderParser.parseHeader(feature1);
        Assert.assertTrue(listOfFeatures.size() == 1 && listOfFeatures.get(0).getFeatureName().equals(feature1));


        listOfFeatures = HeaderParser.parseHeader(feature1 + "," + feature2);
        FeatureParameters fp1 = null;
        FeatureParameters fp2 = null;

        for(FeatureParameters fp : listOfFeatures) {
            if(fp.getFeatureName().contains("example1.service")) fp1 = fp;
            if(fp.getFeatureName().contains("serviceB")) fp2 = fp;
        }
        Assert.assertNotNull(fp1);
        Assert.assertNotNull(fp2);
    }

    /**
     * Test of parsing on the complex header.
     */
    @Test
    public void testParseHeader() {
        List<FeatureParameters> listOfFeatures = HeaderParser.parseHeader(header);

        FeatureParameters fp1 = null;
        FeatureParameters fp2 = null;

        for(FeatureParameters fp : listOfFeatures) {
            if(fp.getFeatureName().contains("example1.service")) fp1 = fp;
            if(fp.getFeatureName().contains("serviceB")) fp2 = fp;
        }

        Assert.assertNotNull(fp1);
        Assert.assertNotNull(fp2);

        Assert.assertTrue(fp1.getFeatureAttributes().containsKey(feature1Param1));
        Assert.assertEquals(feature1Param1Value, fp1.getFeatureAttributes().get(feature1Param1));

        Assert.assertTrue(fp1.getFeatureAttributes().containsKey(feature1Param2));
        Assert.assertEquals(feature1Param2Value, fp1.getFeatureAttributes().get(feature1Param2));

        Assert.assertTrue(fp1.getFeatureAttributes().containsKey(feature1Param3));
        Assert.assertEquals(feature1Param3Value, fp1.getFeatureAttributes().get(feature1Param3));

        Assert.assertTrue(fp2.getFeatureAttributes().containsKey(feature2Param1));
        Assert.assertEquals(feature2Param1Value, fp2.getFeatureAttributes().get(feature2Param1));
    }

    /**
     * Test of parsing on the complex header with clause, which contains more features.
     */
    @Test
    public void testParseHeaderMoreFeaturesOneClause() {
        List<FeatureParameters> listOfFeatures = HeaderParser.parseHeader(headerMoreFeaturesOneClause);

        FeatureParameters fp1 = null;
        FeatureParameters fp2 = null;

        for(FeatureParameters fp : listOfFeatures) {
            if(fp.getFeatureName().contains("example1.service")) fp1 = fp;
            if(fp.getFeatureName().contains("serviceB")) fp2 = fp;
        }

        Assert.assertNotNull(fp1);
        Assert.assertNotNull(fp2);

        Assert.assertTrue(fp1.getFeatureAttributes().containsKey(feature1Param1));
        Assert.assertEquals(feature1Param1Value, fp1.getFeatureAttributes().get(feature1Param1));

        Assert.assertTrue(fp1.getFeatureAttributes().containsKey(feature1Param2));
        Assert.assertEquals(feature1Param2Value, fp1.getFeatureAttributes().get(feature1Param2));

        Assert.assertTrue(fp2.getFeatureAttributes().containsKey(feature1Param1));
        Assert.assertEquals(feature1Param1Value, fp2.getFeatureAttributes().get(feature1Param1));

        Assert.assertTrue(fp2.getFeatureAttributes().containsKey(feature1Param2));
        Assert.assertEquals(feature1Param2Value, fp2.getFeatureAttributes().get(feature1Param2));

        try {
            HeaderParser.parseHeader(headerMoreFeaturesOneClause2);
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }
}
