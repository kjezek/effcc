package cz.zcu.kiv.efps.assignment.types;

import junit.framework.Assert;

import org.junit.Test;

/**
*
* This is a junit test for FeatureVersion.
* This test was generated at
* @author
*/

public class FeatureVersionTest  {

    private SimpleFeatureVersion version = new SimpleFeatureVersion(10, 11, 12, "alfa-1");

    /**
     * @see cz.zcu.kiv.efps.assignment.types.FeatureVersion#matches.
     */
    @Test
    public void testMatches() {
        SimpleFeatureVersion another = new SimpleFeatureVersion(10, 11, 12, "alfa-1");
        Assert.assertTrue(version.matchesTo(another));

        another = new SimpleFeatureVersion(9, 11, 12, "alfa-1");
        Assert.assertTrue(version.matchesTo(another));

        another = new SimpleFeatureVersion(10, 10, 12, "alfa-1");
        Assert.assertTrue(version.matchesTo(another));

        another = new SimpleFeatureVersion(10, 11, 11, "alfa-1");
        Assert.assertTrue(version.matchesTo(another));

        another = new SimpleFeatureVersion(11, 11, 12, "alfa-1");
        Assert.assertFalse(version.matchesTo(another));

        another = new SimpleFeatureVersion(10, 12, 12, "alfa-1");
        Assert.assertFalse(version.matchesTo(another));

        another = new SimpleFeatureVersion(10, 11, 13, "alfa-1");
        Assert.assertFalse(version.matchesTo(another));

        another = new SimpleFeatureVersion(10, 11, 12, "alfa-2");
        Assert.assertFalse(version.matchesTo(another));
    }



}
