package cz.zcu.kiv.efps.assignment.types;

import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
* This is a junit test for Feature.
*/
public class FeatureTest  {

    SimpleFeatureVersion version = new SimpleFeatureVersion(10, 11, 12, "beta-2");
    SimpleFeatureVersion wrongVersion = new SimpleFeatureVersion(16, 11, 12, "beta-2");

    SimpleFeatureVersion leftVersion = new SimpleFeatureVersion(2, 3, 5, "beta");
    SimpleFeatureVersion rightVersion = new SimpleFeatureVersion(15, 12, 17, "alfa");
    FeatureVersionRange versionRange = new FeatureVersionRange(leftVersion, true, rightVersion, true);


    /**
     * @see BasicFeature#getIdentifier
     */
    @Test
    public void testGetIdentifier() {

        SimpleFeatureVersion version = new SimpleFeatureVersion(10, 11, 12, "beta-2");
        Feature feature
            = new BasicFeature(
                    "EfpAssignmentModifier",
                    AssignmentSide.REQUIRED,
                    "interface",
                    true,
                    version);

        Assert.assertEquals(
                "interface.EfpAssignmentModifier,10.11.12.beta-2", feature.getIdentifier());
    }


    /**
     * Test of matching basic features.
     */
    @Test
    public void testOfMatchingBasicFeatures() {
        BasicFeature featureProv = new BasicFeature(
                "FeatureA",
                AssignmentSide.PROVIDED,
                "interface",
                true,
                version);

        BasicFeature featureReq = new BasicFeature(
                "FeatureA",
                AssignmentSide.REQUIRED,
                "interface",
                true,
                versionRange);

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        version = new SimpleFeatureVersion(1, 11, 12, "beta-2");
        BasicFeature featureProvWrongVersion = new BasicFeature(
                "FeatureA",
                AssignmentSide.REQUIRED,
                "interface",
                true,
                versionRange);

        Assert.assertFalse(featureProvWrongVersion.matchesTo(featureReq));
        Assert.assertFalse(featureReq.matchesTo(featureProvWrongVersion));
    }


    /**
     * Test of matching typed features.
     */
    @Test
    public void testOfMatchingFeatureWithDatatype() {
        BasicFeature featureProv = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.PROVIDED,
                "event",
                true,
                null,
                null,
                "zcu.foo.bartype");

        BasicFeature featureReq = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.REQUIRED,
                "event",
                true,
                null,
                null,
                "zcu.foo.bartype");

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        BasicFeature featureReqOther = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.REQUIRED,
                "event",
                true,
                null,
                null,
                "java.lang.integer");

        Assert.assertFalse(featureProv.matchesTo(featureReqOther));
        Assert.assertFalse(featureReqOther.matchesTo(featureProv));

        //matching with specify version of provided cast
        featureProv = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.PROVIDED,
                "event",
                true,
                null,
                version,
                "zcu.foo.bartype");

        featureReqOther = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.REQUIRED,
                "event",
                true,
                null,
                null,
                "zcu.foo.bartype");

        Assert.assertTrue(featureProv.matchesTo(featureReqOther));
        Assert.assertTrue(featureReqOther.matchesTo(featureProv));

        featureProv = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.PROVIDED,
                "event",
                true,
                null,
                wrongVersion,
                "zcu.foo.bartype");

        featureReqOther = BasicFeature.CreateFeatureWithDatatype(
                "TypedFeature",
                AssignmentSide.REQUIRED,
                "event",
                true,
                null,
                versionRange,
                "zcu.foo.bartype");

        Assert.assertFalse(featureProv.matchesTo(featureReqOther));
        Assert.assertFalse(featureReqOther.matchesTo(featureProv));
    }


    /**
     * Test of matching features different type.
     */
    @Test
    public void testMatchingDiferentFeatures() {
        BasicFeature featureReqType = BasicFeature.CreateFeatureWithDatatype(
                "FeatureA",
                AssignmentSide.REQUIRED,
                "event",
                true,
                null,
                null,
                "java.lang.integer");

        BasicFeature featureProvBasic = new BasicFeature(
                "FeatureA",
                AssignmentSide.PROVIDED,
                "interface",
                true,
                null,
                new SimpleFeatureVersion(10, 11, 12, "beta-2"));

        Assert.assertFalse(featureReqType.matchesTo(featureProvBasic));
        Assert.assertFalse(featureProvBasic.matchesTo(featureReqType));
    }

    /**
     * Test of matching referenced features.
     */
    @Test
    public void testOfMatchingReferencedFeature() {
        BasicFeature featureBundleProv = new BasicFeature(
                "Bundle.Example", AssignmentSide.PROVIDED, "bundle", true, version);
        BasicFeature featureBundleProv2 = new BasicFeature(
                "Bundle.Example", AssignmentSide.PROVIDED, "bundle", true, leftVersion);

        BasicFeature featureBundleReq = new BasicFeature(
                "Bundle.Example", AssignmentSide.REQUIRED, "bundle", true, versionRange);

        BasicFeature featureProv = BasicFeature.CreateFeatureWithReferenceOnFeature(
                "Feature.A", AssignmentSide.PROVIDED, "package",
                true, null, version, featureBundleProv);

        BasicFeature featureReq = BasicFeature.CreateFeatureWithReferenceOnFeature(
                "Feature.A", AssignmentSide.REQUIRED, "package",
                true, null, versionRange, featureBundleReq);

        BasicFeature featureReqWithoutRef = BasicFeature.CreateFeatureWithReferenceOnFeature(
                "Feature.A", AssignmentSide.REQUIRED, "package",
                true, null, versionRange, null);

        BasicFeature featureProv2 = BasicFeature.CreateFeatureWithReferenceOnFeature(
                "Feature.B", AssignmentSide.PROVIDED, "package", true, null,
                version, featureBundleProv);

        BasicFeature featureReq2 = BasicFeature.CreateFeatureWithReferenceOnFeature(
                "Feature.A", AssignmentSide.REQUIRED, "package",
                true, null, versionRange, featureBundleProv2);

        Assert.assertFalse(featureProv.matchesTo(featureBundleProv));

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        Assert.assertTrue(featureProv.matchesTo(featureReqWithoutRef));
        Assert.assertTrue(featureReqWithoutRef.matchesTo(featureProv));

        Assert.assertFalse(featureProv2.matchesTo(featureReq));
        Assert.assertFalse(featureReq.matchesTo(featureProv2));

        Assert.assertFalse(featureProv.matchesTo(featureReq2));
        Assert.assertFalse(featureReq2.matchesTo(featureProv));
    }


    /**
     * Test of matching named features.
     */
    @Test
    public void testOfMatchingFeatureWithAdditionalName() {
        BasicFeature featureProv = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.PROVIDED, "interface",
                true, null, version, "InterfaceAImpl");

        BasicFeature featureReq = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.REQUIRED, "interface",
                true, null, versionRange, "InterfaceAImpl");

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        //matching with wrong version
        featureProv = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.PROVIDED, "interface",
                true, null, wrongVersion, "InterfaceAImpl");

        Assert.assertFalse(featureProv.matchesTo(featureReq));
        Assert.assertFalse(featureReq.matchesTo(featureProv));


        //without specify name of implementation interface
        featureProv = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.PROVIDED, "interface",
                true, null, version, null);

        featureReq = new BasicFeature(
                "InterfaceA", AssignmentSide.REQUIRED, "interface",
                true);

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        featureReq = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.REQUIRED, "interface",
                true, null, versionRange, null);

        Assert.assertTrue(featureProv.matchesTo(featureReq));
        Assert.assertTrue(featureReq.matchesTo(featureProv));

        //matching with wrong version
        featureProv = BasicFeature.CreateFeatureWithAdditionalName(
                "InterfaceA", AssignmentSide.PROVIDED, "interface",
                true, null, wrongVersion, null);

        Assert.assertFalse(featureProv.matchesTo(featureReq));
        Assert.assertFalse(featureReq.matchesTo(featureProv));
    }
}
