package cz.zcu.kiv.efps.assignment.localrepository;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.core.AssignmentsTools;
import cz.zcu.kiv.efps.assignment.extension.CommonBundleFilesManagerImpl;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.repomirror.impl.XMLDataManipulator;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.*;
import java.util.*;


/**
 * Test of the sub-module for working with local repository.
 *
 * Date: 21.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class XMLDataModifierImplTest {
    /** Path to test file. */
    private static final String TEST_BUNDLE_SOURCE = "./src/test/resources/testJar.jar";

    /** A feature. */
    private Feature feature =
        new BasicFeature("service", AssignmentSide.REQUIRED, "class", true);

    /** GR. */
    private GR gr = new GR(1, "glob.register 1", "GR1");
    /** A main LR. */
    private LR lrMain = new LR(1, "PC desktop", gr, null, "PC_Desktop");
    /** LR. */
    private LR lr = new LR(2, "High Performance", gr, lrMain, "HP");

    /** Simple EFP 1. */
    private EFP efp1 = new SimpleEFP(1, "response_time", EfpNumber.class,
            new Meta("ms", "high", "low"), new UserGamma("gamma"), gr);

    /** Simple lrAssignment 1. */
    private LrAssignment lrAssignSimple1 =
        new LrSimpleAssignment(1, efp1, "high", new EfpNumber(20), lr);

    /** EFP assignment 1. */
    private EfpAssignment efpAssignment1 =
        new EfpAssignment(efp1, new EfpNamedValue(lrAssignSimple1), feature);
    /** EFP assignment with direct value. */
    private EfpAssignment efpDirectAssign =
        new EfpAssignment(efp1, new EfpDirectValue(new EfpNumber(145)), feature);

    /** Bundle file manager. */
    private CommonBundleFilesManagerImpl bundleManager;

    /** Path to test jar file in the actual test. */
    private String pathToJarCopyFile = "";

    /** Name of the actual test.*/
    @Rule
    public TestName name = new TestName();



    /**
     * Does testing copy of jar.
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        pathToJarCopyFile = "./" + name.getMethodName() + "JARCOPY.jar";
        FileUtils.copyFile(new File(TEST_BUNDLE_SOURCE), new File(pathToJarCopyFile));
        bundleManager = new CommonBundleFilesManagerImpl(pathToJarCopyFile);
    }

    /**
     * Deletes test jar copy and local repository file.
     *
     * @throws IOException Error during delete of the test jar copy.
     */
    @After
    public void close() throws IOException {
        bundleManager.close();

        if(!new File(pathToJarCopyFile).delete()) {
            throw new IOException("Can't delete test JAR.");
        }
    }


    /**
     * Test of adding EFP assignment with EFP named value into a local repository.
     */
    @Test
    public void testOfAddingEfpAssignmentIntoRepository() {

        XMLDataManipulator xmlDataModifier =
            new XMLDataManipulator(bundleManager);

        xmlDataModifier.addEfpAssignment(efpAssignment1);

        EFP efp = xmlDataModifier.readEFP(
                efpAssignment1.getEfp().getName(), efpAssignment1.getEfp().getId());

        LrSimpleAssignment lrSimpleAssign = (LrSimpleAssignment) xmlDataModifier.readLrAssignment(
                efpAssignment1.getEfp(), lrAssignSimple1.getLr().getId(),
                lrAssignSimple1.getValueName());

        EfpAssignment newEfpAssignment1 = new EfpAssignment(
                efp, new EfpNamedValue(lrSimpleAssign), feature);

        Assert.assertEquals(efpAssignment1, newEfpAssignment1);
    }


    /**
     * Test of adding EFP assignment with EFP direct value into a local repository.
     */
    @Test
    @SuppressWarnings(value="unchecked")
    public void testOfAddingEfpAssignmentWithDirectValue() {
        XMLDataManipulator xmlDataModifier =
            new XMLDataManipulator(bundleManager);
        int id = xmlDataModifier.addEfpAssignment(efpDirectAssign);

        EfpValueType efpValueType =
                EfpSerialiser.deserialize((Class<EfpValueType>) efp1.getValueType(),
                        xmlDataModifier.readParamObject(id));
        EfpDirectValue efpDirectValueNew = new EfpDirectValue(efpValueType);

        EfpAssignment efpAssignNew = new EfpAssignment(
                efpDirectAssign.getEfp(), efpDirectValueNew, efpDirectAssign.getFeature());

        Assert.assertEquals(efpDirectAssign, efpAssignNew);
    }


    /**
     * Test of deleting EFP.
     */
    @Test
    public void testOfDeletingEFP() {
        XMLDataManipulator xmlDataModifier =
            new XMLDataManipulator(bundleManager);
        xmlDataModifier.addEfpAssignment(efpAssignment1);
        boolean isDel = false;

        EFP efp = efpAssignment1.getEfp();

        try {
            xmlDataModifier.readEFP(efp.getName(), efp.getGr().getId());
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(!isDel);

        xmlDataModifier.deleteEFP(efp);
        
        try {
            xmlDataModifier.readEFP(efp.getName(), efp.getGr().getId());
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(isDel);
    }


    /**
     * Test of deleting LR assignment.
     */
    @Test
    public void testOfDeletingLrAssignment() {
        XMLDataManipulator xmlDataModifier =
            new XMLDataManipulator(bundleManager);
        xmlDataModifier.addEfpAssignment(efpAssignment1);
        boolean isDel = false;
        LrAssignment lrAssign = ((EfpNamedValue) efpAssignment1.getEfpValue()).getLrAssignment();
        try {
            xmlDataModifier.readLrAssignment(
                    lrAssign.getEfp(), lrAssign.getLr().getId(), lrAssign.getValueName());
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(!isDel);

        xmlDataModifier.deleteLRAssignment(lrAssign);
        try {
            xmlDataModifier.readLrAssignment(
                    lrAssign.getEfp(), lrAssign.getLr().getId(), lrAssign.getValueName());
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(isDel);
    }


    /**
     * Test of deleting LR assignment.
     */
    @Test
    public void testOfDeletingDirectValue() {
        XMLDataManipulator xmlDataModifier = new XMLDataManipulator(bundleManager);
        int id = xmlDataModifier.addEfpAssignment(efpDirectAssign);
        boolean isDel = false;

        try {
            xmlDataModifier.readParamObject(id);
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(!isDel);

        xmlDataModifier.deleteParamObject(id);
        try {
            xmlDataModifier.readParamObject(id);
        } catch (AssignmentRTException e) {
            isDel = true;
        }

        Assert.assertTrue(isDel);
    }


    /**
     * Test of adding EFP assignment into repository, which contains EFP named value
     * with derived property with participating LR assignments.
     */
    @Test
    public void testAddingEfpAssignWithNamedValDerived() {
        Meta meta = new Meta("ms", "high", "low");
        EFP efp1 = new SimpleEFP(10, "load_time", EfpNumber.class, meta, null, gr);
        EFP efp2 = new SimpleEFP(1, "response_time", EfpNumber.class, meta, null, gr);
        DerivedEFP efpDerived = new DerivedEFP(25, "derived_efp2",
                EfpNumber.class, meta, null, gr, efp1, efp2);

        LrAssignment lrSimpleAssingEfp1 =
            new LrSimpleAssignment(1, efp1, "high", new EfpNumber(17), lrMain);
        LrAssignment lrSimpleAssingEfp2 =
            new LrSimpleAssignment(2, efp2, "low", new EfpNumber(357), lr);

        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "load_time_high > response_time_low", new EfpNumber(142), efpDerived.getEfps());

        LrDerivedAssignment lrDerivedAssingEfpD = new LrDerivedAssignment(1, efpDerived,
                "high", eval, lr);

        List<EfpFeature> efpFeatures = new ArrayList<EfpFeature>();
        efpFeatures.add(new EfpFeature(efp1, feature));
        efpFeatures.add(new EfpFeature(efp2, feature));

        EfpNamedValue efpAssignedValue = new EfpNamedValue(
                lrDerivedAssingEfpD, efpFeatures, lrSimpleAssingEfp1, lrSimpleAssingEfp2);

        EfpAssignment efpAssignment = new EfpAssignment(efpDerived, efpAssignedValue, feature);


        XMLDataManipulator xmlDataModifier = new XMLDataManipulator(bundleManager);

        xmlDataModifier.addEfpAssignment(efpAssignment);
        LrDerivedAssignment lrDerAssingNew = (LrDerivedAssignment) xmlDataModifier.readLrAssignment(
                efpDerived, lrDerivedAssingEfpD.getLr().getId(),
                lrDerivedAssingEfpD.getValueName());

        EfpNamedValue efpAssignedValueNew = (EfpNamedValue)
            AssignmentsTools.setAssignmentWithLrDerivedAssign(
                    new TestEfpDataManipulator(), lrDerAssingNew, xmlDataModifier);


        Assert.assertEquals(efpAssignedValue, efpAssignedValueNew);

        Assert.assertEquals(new EfpFeature(efp1, feature),
                efpAssignedValueNew.getRelatedRequiredSide().get(0));
        Assert.assertEquals(new EfpFeature(efp2, feature),
                efpAssignedValueNew.getRelatedRequiredSide().get(1));

        Assert.assertEquals(lrSimpleAssingEfp1,
                efpAssignedValueNew.getLrParticipatingAssignments()[0]);
        Assert.assertEquals(lrSimpleAssingEfp2,
                efpAssignedValueNew.getLrParticipatingAssignments()[1]);
    }

    private class TestEfpDataManipulator implements EfpDataManipulator {

        @Override
        public List<Feature> readFeatures() {
            return Arrays.asList(feature);
        }

        @Override
        public List<EfpLink> readEFPs(final Feature feature) {
            return Arrays.asList(
                    new EfpLink(null, null, "load_time"),
                    new EfpLink(null, null, "response_time")
                    );
        }

        @Override
        public void assignEFPs(final Feature feature, final List<EfpLink> efpData) {
        }

    }
}
