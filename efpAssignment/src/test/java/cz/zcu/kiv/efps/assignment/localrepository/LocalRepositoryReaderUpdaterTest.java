package cz.zcu.kiv.efps.assignment.localrepository;

import javax.xml.bind.JAXBElement;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.repomirror.support.LocalRepositoryReader;
import cz.zcu.kiv.efps.assignment.repomirror.support.LocalRepositoryUpdater;
import cz.zcu.kiv.efps.assignment.repository.generated.ObjectFactory;
import cz.zcu.kiv.efps.assignment.repository.generated.RepositoriesElement;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * Test of classes LocalRepositoryReader and LocalRepositoryUpdate
 * for manipulation with XML which contains a local repository.
 *
 * Date: 21.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class LocalRepositoryReaderUpdaterTest {
    /** A feature. */
    private Feature feature =
        new BasicFeature("service", AssignmentSide.REQUIRED, "class", true);

    /** GRs. */
    private GR gr = new GR(1, "glob.register 1", "GR1");

    /** LR. */
    private LR lr = new LR(2, "High Performance", gr, null, "HP");


    /** Simple EFP 1. */
    private EFP efp1 = new SimpleEFP(1, "response_time", EfpNumber.class,
            new Meta("ms", "high", "low"), new UserGamma("gamma"), gr);
    /** Simple EFP 2. */
    private EFP efp2 = new SimpleEFP(2, "memory", EfpNumber.class,
            new Meta("MB", "high", "low"), new UserGamma("gamma"), gr);

    /** Derived EFP. */
    private DerivedEFP efpDerived1 = new DerivedEFP(3, "derived 1", EfpNumberInterval.class,
            new Meta("ms", "name1", "name2", "name3"), new UserGamma("gamma"), gr, efp1, efp2);

    /** Simple lrAssignment 1. */
    private LrAssignment lrAssignSimple1 =
        new LrSimpleAssignment(1, efp1, "high", new EfpNumber(20), lr);
    /** Simple lrAssignment 1. */
    private LrAssignment lrAssignSimple2 =
        new LrSimpleAssignment(2, efp2, "low", new EfpNumber(60), lr);

    private LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
            "logical formula", new EfpNumberInterval(10.3, 500), efpDerived1.getEfps());

    /** Derived lrAssignment. */
    private LrDerivedAssignment lrAssignDerived1 = new LrDerivedAssignment(3,
            efpDerived1, "name3", eval, lr);

    /** efpAssignment 1. */
    private EfpAssignment efpAssignment1 =
        new EfpAssignment(efp1, new EfpNamedValue(lrAssignSimple1), feature);
    /** efpAssignment 2. */
    private EfpAssignment efpAssignment2 =
        new EfpAssignment(efp2, new EfpNamedValue(lrAssignSimple2), feature);

    /** Root element of XML. */
    private JAXBElement<?> localRepository;



    /**
     * Creates root element of XML.
     */
    @Before
    public void init() {
        ObjectFactory objFact = new ObjectFactory();
        RepositoriesElement repositories = objFact.createRepositoriesElement();
        localRepository = objFact.createEfpMirror(repositories);
    }


    /**
     * Test of adding a simple EFP to repository.
     */
    @Test
    public void testAddingSimpleEFP()  {
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.writeMissingSimpleOrDerivedEFPElement(efp1);

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);

        EFP newEfp1 = locRepReader.readEFP(efp1.getName(), efp1.getGr().getId());
        EFP newEfp1ByStrId = locRepReader.readEFP(efp1.getName(), efp1.getGr().getIdStr());

        Assert.assertEquals(efp1, newEfp1);
        Assert.assertEquals(efp1, newEfp1ByStrId);
    }


    /**
     * Test of adding a derived EFP to repository.
     */
    @Test
    public void testAddingDerivedEFP() {
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.writeMissingSimpleOrDerivedEFPElement(efpDerived1);

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);

        EFP newEfp1 = locRepReader.readEFP(efpDerived1.getName(), efpDerived1.getGr().getId());
        EFP newEfp1ByStrId = locRepReader.readEFP(efpDerived1.getName(), efpDerived1.getGr().getIdStr());

        Assert.assertEquals(efpDerived1, newEfp1);
        Assert.assertEquals(efpDerived1, newEfp1ByStrId);
    }


    /**
     * Test of adding a simple LR assignment.
     */
    @Test
    public void testAddingSimpleLrAssignment() {
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);
        locRepUpdater.writeMissingLrSimOrDerAssignElement(lrAssignSimple1, null);

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);

        LrAssignment lrAssignment1New = locRepReader.readLrAssignment(
                lrAssignSimple1.getEfp(),
                lrAssignSimple1.getLr().getId(),
                lrAssignSimple1.getValueName());

        LrAssignment lrAssignment1NewByStrId = locRepReader.readLrAssignment(
                lrAssignSimple1.getEfp(),
                lrAssignSimple1.getLr().getIdStr(),
                lrAssignSimple1.getValueName());

        Assert.assertEquals(lrAssignSimple1, lrAssignment1New);
        Assert.assertEquals(lrAssignSimple1, lrAssignment1NewByStrId);
    }


    /**
     * Test of adding a derived LR assignment.
     */
    @Test
    public void testAddingDerivedLrAssignment() {
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);
        locRepUpdater.writeMissingLrSimOrDerAssignElement(lrAssignDerived1, null);

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);

        LrAssignment lrAssignment1New = locRepReader.readLrAssignment(
                lrAssignDerived1.getEfp(),
                lrAssignDerived1.getLr().getId(),
                lrAssignDerived1.getValueName());

        LrAssignment lrAssignment1NewByStrId = locRepReader.readLrAssignment(
                lrAssignDerived1.getEfp(),
                lrAssignDerived1.getLr().getId(),
                lrAssignDerived1.getValueName());

        Assert.assertEquals(lrAssignDerived1, lrAssignment1New);
        Assert.assertEquals(lrAssignDerived1, lrAssignment1NewByStrId);
    }


    /**
     * Test of creating local repository with some efpAssignments with EFP named values.
     */
    @Test
    public void testInsertingAssignmentWithEfpNamedValue() {

        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.writeAssignment(efpAssignment1);
        locRepUpdater.writeAssignment(efpAssignment2);

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);

        //getting efpAssignment1
        EFP efp1New = locRepReader.readEFP(
                efpAssignment1.getEfp().getName(),
                efpAssignment1.getEfp().getGr().getId());

        EFP efp1NewByStrId = locRepReader.readEFP(
                efpAssignment1.getEfp().getName(),
                efpAssignment1.getEfp().getGr().getIdStr());

        LrSimpleAssignment lrAssignSimple1New =
            (LrSimpleAssignment) locRepReader.readLrAssignment(
                    efp1New, lrAssignSimple1.getLr().getId(),
                    lrAssignSimple1.getValueName());

        LrSimpleAssignment lrAssignSimple1NewByStrId =
                (LrSimpleAssignment) locRepReader.readLrAssignment(
                        efp1New, lrAssignSimple1.getLr().getIdStr(),
                        lrAssignSimple1.getValueName());

        //getting efpAssignment2
        EFP efp2New = locRepReader.readEFP(
                efpAssignment2.getEfp().getName(),
                efpAssignment2.getEfp().getGr().getId());

        EFP efp2NewByStrId = locRepReader.readEFP(
                efpAssignment2.getEfp().getName(),
                efpAssignment2.getEfp().getGr().getIdStr());

        LrSimpleAssignment lrAssignSimple2New =
                (LrSimpleAssignment) locRepReader.readLrAssignment(
                        efp2New, lrAssignSimple2.getLr().getId(),
                        lrAssignSimple2.getValueName());

        LrSimpleAssignment lrAssignSimple2NewByStrId =
                (LrSimpleAssignment) locRepReader.readLrAssignment(
                        efp2New, lrAssignSimple2.getLr().getIdStr(),
                        lrAssignSimple2.getValueName());


        Assert.assertEquals(lrAssignSimple1.toString(), lrAssignSimple1New.toString());
        Assert.assertEquals(efp1.toString(), efp1New.toString());
        Assert.assertEquals(lrAssignSimple1.toString(), lrAssignSimple1NewByStrId.toString());
        Assert.assertEquals(efp1.toString(), efp1NewByStrId.toString());

        Assert.assertEquals(lrAssignSimple2.toString(), lrAssignSimple2New.toString());
        Assert.assertEquals(efp2.toString(), efp2New.toString());
        Assert.assertEquals(lrAssignSimple2.toString(), lrAssignSimple2NewByStrId.toString());
        Assert.assertEquals(efp2.toString(), efp2NewByStrId.toString());
    }


    /**
     * Test of inserting a EFP direct value into repository.
     */
    @Test
    public void testInsertingEfpDirectValue() {

        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        EfpValueType val = new EfpNumber(2);
        EfpDirectValue efpDirectValue = new EfpDirectValue(val);

        int id = locRepUpdater.writeAssignment(new EfpAssignment(efp1, efpDirectValue, null));

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);
        EfpValueType efpValueType =
            (EfpValueType) EfpSerialiser.deserialize((Class<EfpValueType>) efp1.getValueType(),
                    locRepReader.readOtherValue(id));
        EfpDirectValue efpDirectValueNew = new EfpDirectValue(efpValueType);

        Assert.assertEquals(efpDirectValue, efpDirectValueNew);
    }


    /**
     * Test of inserting EFP math.formula into repository.
     */
    @Test
    public void testInsertingEfpMathFormula() {
        EfpFormulaValue efpMathFormula = new EfpFormulaValue("2 * response_time_low");

        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);
        int id = locRepUpdater.writeAssignment(new EfpAssignment(efp1, efpMathFormula, null));

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(localRepository);
        String efpMathFormulaStr = locRepReader.readOtherValue(id);

        Assert.assertEquals(efpMathFormula.getFormula(), efpMathFormulaStr);
    }
}
