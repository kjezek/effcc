package cz.zcu.kiv.efps.assignment.types;

import junit.framework.Assert;

import org.junit.Test;

public class FeatureVersionRangeTest {

    private SimpleFeatureVersion leftVersion =
        new SimpleFeatureVersion(2, 3, 5, "alfa");
    private SimpleFeatureVersion rightVersion =
        new SimpleFeatureVersion(15, 12, 17, "beta");
    private FeatureVersionRange versionRange =
        new FeatureVersionRange(leftVersion, true, rightVersion, true);



    /**
     * Test of matching simple feature version with version range.
     */
    @Test
    public void testMatches() {
        SimpleFeatureVersion version = new SimpleFeatureVersion(10, 11, 12, "alfa-1");
        Assert.assertTrue(versionRange.matchesTo(version));


        version = new SimpleFeatureVersion(2, 3, 5, "alfa");
        Assert.assertTrue(versionRange.matchesTo(version));

        version = new SimpleFeatureVersion(2, 3, 5, "alfa-2");
        Assert.assertFalse(versionRange.matchesTo(version));

        version = new SimpleFeatureVersion(1, 3, 4, "alfa-1");
        Assert.assertFalse(versionRange.matchesTo(version));


        version = new SimpleFeatureVersion(15, 11, 16, "alfa-2");
        Assert.assertTrue(versionRange.matchesTo(version));

        version = new SimpleFeatureVersion(15, 12, 17, "beta");
        Assert.assertTrue(versionRange.matchesTo(version));

        version = new SimpleFeatureVersion(15, 12, 17, "alfa-1");
        Assert.assertFalse(versionRange.matchesTo(version));



        versionRange = new FeatureVersionRange(leftVersion, false, rightVersion, false);

        version = new SimpleFeatureVersion(2, 3, 5, "alfa");
        Assert.assertFalse(versionRange.matchesTo(version));

        version = new SimpleFeatureVersion(15, 12, 17, "beta");
        Assert.assertFalse(versionRange.matchesTo(version));


        versionRange = new FeatureVersionRange(leftVersion, true, leftVersion, true);
        Assert.assertTrue(versionRange.matchesTo(leftVersion));

        versionRange = new FeatureVersionRange(leftVersion, false, leftVersion, false);
        Assert.assertFalse(versionRange.matchesTo(leftVersion));
    }
}
