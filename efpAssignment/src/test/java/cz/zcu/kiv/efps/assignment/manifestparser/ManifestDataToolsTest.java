package cz.zcu.kiv.efps.assignment.manifestparser;

import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestDataTools;
import cz.zcu.kiv.efps.assignment.types.FeatureVersionRange;
import cz.zcu.kiv.efps.assignment.types.SimpleFeatureVersion;

/**
 * Test of tool methods in class ManifestDataTools.
 *
 * Date: 15.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ManifestDataToolsTest {

    /**
     * Test of creating FeatureVersion object from string.
     */
    @Test
    public void createFeatureVersionTest() {
        String versionStr = "1.2.3.4";

        SimpleFeatureVersion featureVersion =
            ManifestDataTools.createSimpleVersion(versionStr);

        String versionStrNew = featureVersion.getIdentifier();
        String versionStrEmptyNew =
            ManifestDataTools.createSimpleVersion("").getIdentifier();
        String versionStrNullNew =
            ManifestDataTools.createSimpleVersion(null).getIdentifier();

        Assert.assertEquals(versionStr, versionStrNew);
        Assert.assertEquals("0.0.0", versionStrEmptyNew);
        Assert.assertEquals("0.0.0", versionStrNullNew);
    }


    /**
     * Test of creating FeatureVersionRange object from string.
     */
    @Test
    public void createFeatureVersionRange() {
        String versionStr = "[1.2.3.4, 2.3)";

        FeatureVersionRange featureVersionRange =
            ManifestDataTools.createVersionRange(versionStr);
        String versionStrNew = featureVersionRange.getIdentifier();

        Assert.assertEquals(versionStr, versionStrNew);


        versionStr = "1.2.3.4";
        featureVersionRange = ManifestDataTools.createVersionRange(versionStr);
        versionStrNew = featureVersionRange.getIdentifier();
        Assert.assertEquals("[" + versionStr + ", " + Integer.MAX_VALUE + ")", versionStrNew);


        featureVersionRange = ManifestDataTools.createVersionRange("");
        versionStrNew = featureVersionRange.getIdentifier();
        Assert.assertEquals("[0.0.0, " + Integer.MAX_VALUE + ")", versionStrNew);


        featureVersionRange = ManifestDataTools.createVersionRange(null);
        versionStrNew = featureVersionRange.getIdentifier();
        Assert.assertEquals("[0.0.0, " + Integer.MAX_VALUE + ")", versionStrNew);
    }


    /**
     * Test of removing border marks from parameter in manifest.
     */
    @Test
    public void removeBorderMarksTest() {
        String value = "value";

        String valueNew = ManifestDataTools.removeBorderMarks("(" + value + ")");
        String valueNew2 = ManifestDataTools.removeBorderMarks("\"" + value + "\"");

        Assert.assertEquals(value, valueNew);
        Assert.assertEquals(value, valueNew2);
    }
}
