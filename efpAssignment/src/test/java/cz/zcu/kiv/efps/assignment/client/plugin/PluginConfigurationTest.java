package cz.zcu.kiv.efps.assignment.client.plugin;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.core.ComponentEFPModifierImpl;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.types.Feature;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * A plugin configuration test.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class PluginConfigurationTest {


    /** Test to create a loader according to a configuration. */
    @Test
    public void testCreateLoader() {

        PluginAPIWrapper conf = PluginConfiguration.configure("/plugin.test.properties");

        ComponentEFPModifier modifier = conf.createComponentModifier("ABCD");

        Assert.assertNotNull(modifier);
    }

    /** A test that one configuration has been switched to another one. */
    @Test
    public void testSwitchConfOn() {
        PluginAPIWrapper conf =  PluginConfiguration.configure("/plugin.test.properties");

        Assert.assertEquals(ComponentEFPModifierImpl.class.getName(),
                conf.createComponentModifier("").getClass().getName());

        Assert.assertEquals(MatchingFunction.DEFAULT_MATCHING_FUNCTION.getClass().getName(),
                conf.createMatchingFunction(new ArrayList<String>()).getClass().getName());
    }

    /**
     * A test class.
     * @author Kamil Jezek [kjezek@kiv.zcu.cz]
     *
     */
    public static class EfpTestDataManipulatorA implements EfpDataManipulator {

        @Override
        public List<Feature> readFeatures() {
            return null;
        }

        @Override
        public List<EfpLink> readEFPs(final Feature feature) {
            return null;
        }

        @Override
        public void assignEFPs(final Feature feature, final List<EfpLink> efpData) {

        }

    }

}
