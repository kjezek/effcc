/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import java.util.Arrays;

import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.types.SimpleFeatureVersion;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.types.datatypes.EfpInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * Tests evaluation of math formulas.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class MathFormulaEvaluatorTest {

    /** Common for all tests. */
    private Feature feature
        = new BasicFeature("s", AssignmentSide.REQUIRED, "component", true);

    /** Tests a computation with simple numbers. */
    @Test
    public void testComputeValueNumbers() {

        // some participating efps
        EFP speed = new SimpleEFP("speed", EfpNumber.class, null, null, null);
        EfpValueType speedValueType = new EfpNumber(200);
        EfpAssignedValue speedValue = new EfpDirectValue(speedValueType);
        EfpAssignment speedAssignment = new EfpAssignment(speed, speedValue, feature);

        EFP time = new SimpleEFP("time", EfpNumber.class, null, null, null);
        EfpValueType timeValueType = new EfpNumber(30);
        EfpAssignedValue timeValue = new EfpDirectValue(timeValueType);
        EfpAssignment timeAssignment = new EfpAssignment(time, timeValue, feature);

        EfpNumber distance = (EfpNumber) new MathFormulaEvaluator(
                "2 * component_s_speed * component_s_time + 10").evaluate(
                Arrays.asList(speedAssignment, timeAssignment));

        Assert.assertEquals(new EfpNumber(12010), distance.round(0));

        distance = (EfpNumber) new MathFormulaEvaluator(
                "component_s_speed * (component_s_time + 1)").evaluate(
                Arrays.asList(speedAssignment, timeAssignment));

        Assert.assertEquals(new EfpNumber(6200), distance.round(0));
    }

    /** Tests with intervals. */
    @Test
    public void testComputeValueIntervals() {

        // some participating efps
        EFP efp1 = new SimpleEFP("efp1", EfpInterval.class, null, null, null);
        EfpValueType efp1Value = new EfpNumberInterval(10, 20);
        EfpAssignedValue efp1ValueType = new EfpDirectValue(efp1Value);
        EfpAssignment efp1Assignment = new EfpAssignment(efp1, efp1ValueType, feature);

        EFP efp2 = new SimpleEFP("efp2", EfpNumber.class, null, null, null);
        EfpValueType efp2Value = new EfpNumber(30);
        EfpAssignedValue efp2ValueType = new EfpDirectValue(efp2Value);
        EfpAssignment efp2Assignment = new EfpAssignment(efp2, efp2ValueType, feature);

        // test evaluator
        EfpNumberInterval result = (EfpNumberInterval) new MathFormulaEvaluator(
                "3 + component_s_efp1 + component_s_efp2 * 2").evaluate(
                Arrays.asList(efp1Assignment, efp2Assignment));

        Assert.assertEquals(new EfpNumberInterval(73, 83), result.round(0));

    }


    /** Test with math formulas. */
    @Test
    public void testComputeMathFunctions() {
        EFP efp = new SimpleEFP("efp", EfpNumber.class, null, null, null);
        EfpValueType efp1Value = new EfpNumber(2);

        EfpAssignedValue efpValueType = new EfpDirectValue(efp1Value);
        EfpAssignment efpAssignment = new EfpAssignment(efp, efpValueType, feature);


        // EFP number with Integer
        EfpNumber number = (EfpNumber) new MathFormulaEvaluator(
                "2 + pow(component_s_efp, 3)").evaluate(Arrays.asList(efpAssignment));
        Assert.assertEquals(new EfpNumber(10), number);

        // EFP number with EFP number
        number = (EfpNumber) new MathFormulaEvaluator(
                "pow(component_s_efp, component_s_efp)").evaluate(Arrays.asList(efpAssignment));
        Assert.assertEquals(new EfpNumber(4), number);

        // EFP number with BigDecimal
        number = (EfpNumber) new MathFormulaEvaluator(
                "pow(component_s_efp, 3.0)").evaluate(Arrays.asList(efpAssignment));
        Assert.assertEquals(new EfpNumber(8), number);
    }

    /** Test with math formulas. */
    @Test
    public void testComputeWithReleaseNotes() {
        EFP efp = new SimpleEFP("efp", EfpNumber.class, null, null, null);
        EfpValueType efp1Value = new EfpNumber(2);

        SimpleFeatureVersion version = new SimpleFeatureVersion(10, 9, 8, "beta-9");
        Feature feature = new BasicFeature("s", AssignmentSide.REQUIRED, "component", true, version);
        EfpAssignedValue efpValueType = new EfpDirectValue(efp1Value);
        EfpAssignment efpAssignment = new EfpAssignment(efp, efpValueType, feature);

        // EFP number with Integer
        EfpNumber number = (EfpNumber) new MathFormulaEvaluator(
                "2 + pow(component_s_10_9_8_beta_9_efp, 3)").evaluate(Arrays.asList(efpAssignment));
        Assert.assertEquals(new EfpNumber(10), number);

    }
}
