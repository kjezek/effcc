/**
 *
 */
package cz.zcu.kiv.efps.assignment.values;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.evaluator.RelatedAssignmentsFinder;
import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * Tests of classes for assigned values.
 *
 * This test shows up a practical realization of assignments of
 * EFPs to features.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpAssignedValueTest {

    /** A feature. */
    private Feature feature = new BasicFeature(
            "service", AssignmentSide.REQUIRED, "class", true);

    /** LR. */
    private LR lr = new LR(1, "High Performance LR", null, null, "HP");

    /** This map simulate a lookup from comparator to the assignment. */
    private final Map<EfpFeature, EfpAssignment> lookup = new HashMap<EfpFeature, EfpAssignment>();

    /** A fake lookup implementation. */
    private RelatedAssignmentsFinder finder = relatedAssignmentFake();

    /** Initialization. */
    @Before
    public void setUp() {
        lookup.clear();
    }

    /**
     * Test of values containing math formulas.
     */
    @Test
    public void testEfpMathFormula() {

        EfpAssignedValue x = new EfpDirectValue(new EfpNumber(3));
        EfpAssignedValue y = new EfpDirectValue(new EfpNumber(4));

        EFP xEfp = new SimpleEFP("x", null, null, null, null);
        EFP yEfp = new SimpleEFP("y", null, null, null, null);

        EfpAssignment xAssign = new EfpAssignment(xEfp, x, feature);
        EfpAssignment yAssign = new EfpAssignment(yEfp, y, feature);

        EfpFeature xFe = new EfpFeature(xEfp, feature);
        EfpFeature yFe = new EfpFeature(yEfp, feature);
        lookup.put(xFe, xAssign);
        lookup.put(yFe, yAssign);

        // Pythagoras -:)
        EfpAssignedValue value =
            new EfpFormulaValue("sqrt(pow(class_service_x,2) + pow(class_service_y,2))", xFe, yFe);
        value.setRelatedValuesCallback(finder);


        Assert.assertEquals(new EfpNumber(5), value.computeValue());
    }

    /**
     * Test of values with nested math formulas.
     */
    @Test
    public void testNestedFormula() {
        EFP zEfp = new SimpleEFP("z", null, null, null, null);
        EfpAssignedValue z = new EfpDirectValue(new EfpNumber(2));
        EfpAssignment zAssig = new EfpAssignment(zEfp, z, feature);

        EfpFeature zFe = new EfpFeature(zEfp, feature);
        lookup.put(zFe, zAssig);

        EfpAssignedValue x = new EfpFormulaValue("cos(toRadians(0)) + class_service_z", zFe);
        x.setRelatedValuesCallback(finder);

        // a simple formula must results in EFPValueType
        EfpAssignedValue y = new EfpFormulaValue("1 * 2 + 2 * new EfpNumber(1)");
        y.setRelatedValuesCallback(finder);

        EFP xEfp = new SimpleEFP("x", null, null, null, null);
        EFP yEfp = new SimpleEFP("y", null, null, null, null);

        EfpAssignment xAssign = new EfpAssignment(xEfp, x, feature);
        EfpAssignment yAssign = new EfpAssignment(yEfp, y, feature);

        EfpFeature xFe = new EfpFeature(xEfp, feature);
        EfpFeature yFe = new EfpFeature(yEfp, feature);
        lookup.put(xFe, xAssign);
        lookup.put(yFe, yAssign);

        // Pythagoras -:)
        EfpAssignedValue value =
            new EfpFormulaValue("sqrt(pow(class_service_x,2) + pow(class_service_y,2))", xFe, yFe);
        value.setRelatedValuesCallback(finder);

        Assert.assertEquals(new EfpNumber(5), value.computeValue());
    }

    /**
     * We do not allow formulas which return type is no an EFP type.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSimpleFormulaNotAllowed() {

        // does not results in a EFP type. It is forbidden.
        EfpAssignedValue simple = new EfpFormulaValue("1 + 1");

        simple.computeValue();
    }

    /**
     * Test directly assigned values - the most simple case.
     */
    @Test
    public void testDirectValue() {
        EfpAssignedValue value = new EfpDirectValue(new EfpBoolean(true));

        Assert.assertEquals(new EfpBoolean(true), value.computeValue());
    }

    /**
     * Test of named values from LR.
     */
    @Test
    public void testNamedSimpleValue() {

        EFP efp = new SimpleEFP("efp", null, null, null, null);
        EfpValueType efpValue = new EfpNumber(10);

        LrSimpleAssignment lrAssig = new LrSimpleAssignment(efp, "low", efpValue, lr);
        EfpAssignedValue valueAssig = new EfpNamedValue(lrAssig);

        EfpValueType value = valueAssig.computeValue();
        Assert.assertEquals(efpValue, value);
    }

    /**
     * Test of named derived values from LR.
     * This test combines a derived property with
     *  direct values.
     */
    @Test
    public void testNamedDerivedValueWithDirectValues() {
        EFP time = new SimpleEFP("time", null, null, null, null);
        EFP memory = new SimpleEFP("memory", null, null, null, null);
        EFP performance = new DerivedEFP("performance", null, null, null, null, time, memory);
        EfpValueType goodPerofmance = new EfpNumber(400);

        // --------------- test with fulfilled formula -----------
        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "time > 199 && memory < 31 && 29 < memory && memory == 30", goodPerofmance,
                Arrays.asList(time, memory));

        LrDerivedAssignment lrAssig = new LrDerivedAssignment(performance, "good", eval, lr);

       EfpAssignedValue timeAssigValue = new EfpDirectValue(new EfpNumber(200));
       EfpAssignedValue memoryAssigValue = new EfpDirectValue(new EfpNumber(30));
       EfpAssignment timeAssig = new EfpAssignment(time, timeAssigValue, feature);
       EfpAssignment memoryAssig = new EfpAssignment(memory, memoryAssigValue, feature);

       EfpFeature timeFe = new EfpFeature(time, feature);
       EfpFeature memoryFe = new EfpFeature(memory, feature);
       lookup.put(timeFe, timeAssig);
       lookup.put(memoryFe, memoryAssig);

        EfpAssignedValue valueAssigOk =
            new EfpNamedValue(lrAssig, Arrays.asList(timeFe, memoryFe));
        valueAssigOk.setRelatedValuesCallback(finder);

        EfpValueType valueOk = valueAssigOk.computeValue();

        // the formula is valid -> the value must be returned
        Assert.assertEquals(goodPerofmance, valueOk);

        // ----------------- test with not satisfied formula --------------
        // the formula does not hold
        eval = new LrConstraintEvaluator(
                "time == 199 && memory == 30", goodPerofmance,
                Arrays.asList(time, memory));

        lrAssig = new LrDerivedAssignment(
                performance, "good", eval, lr);

        EfpAssignedValue valueAssigBad =
            new EfpNamedValue(lrAssig, Arrays.asList(timeFe, memoryFe));
        valueAssigBad.setRelatedValuesCallback(finder);
        EfpValueType valueBad = valueAssigBad.computeValue();

        // the formula is NOT valid -> the value is NOT returned
        Assert.assertEquals(null, valueBad);
    }

    /**
     * Test of named derived values from LR.
     * This test combines a derived property with
     *  direct values.
     */
    @Test
    public void testNamedDerivedValueDifferentDataTypes() {
        EFP falible = new SimpleEFP("falible", null, null, null, null);
        EFP os = new SimpleEFP("os", null, null, null, null);
        EFP installedApp = new SimpleEFP("app", null, null, null, null);
        EFP params = new SimpleEFP("params", null, null, null, null);
        EFP machine = new DerivedEFP("machine", null, null, null, null,
                falible, os, installedApp, params);

        EfpValueType goodMachine = new EfpEnum("good", "nice", "perfect");

        // --------------- test with fulfilled formula -----------
        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "falible == true "
                + "&& os == 'Linux' "
                + "&& app == ['eclipse', 'java', 'mvn'] "
                + "&& params == ['hello', 10, true, ['x', 'y', 'z']]", goodMachine,
                Arrays.asList(falible, os, installedApp, params));

        LrDerivedAssignment lrAssig = new LrDerivedAssignment(
                machine, "good", eval, lr);

       EfpAssignedValue falibleAssigValue = new EfpDirectValue(new EfpBoolean(true));
       EfpAssignedValue osAssigValue = new EfpDirectValue(new EfpString("Linux"));
       EfpAssignedValue appAssigValue = new EfpDirectValue(new EfpEnum("eclipse", "java", "mvn"));
       EfpAssignedValue paramsAssigValue = new EfpDirectValue(
               new EfpSimpleSet(new EfpString("hello"),
                       new EfpNumber(10),
                       new EfpBoolean(true),
                       new EfpSimpleSet(
                               new EfpString("x"),
                               new EfpString("y"),
                               new EfpString("z")
                               )
               )
               );

       EfpAssignment falibleAssig = new EfpAssignment(falible, falibleAssigValue, feature);
       EfpAssignment osAssig = new EfpAssignment(os, osAssigValue, feature);
       EfpAssignment appAssig = new EfpAssignment(installedApp, appAssigValue, feature);
       EfpAssignment paramsAssig = new EfpAssignment(params, paramsAssigValue, feature);

       EfpFeature falibleFe = new EfpFeature(falible, feature);
       EfpFeature osFe = new EfpFeature(os, feature);
       EfpFeature appFe = new EfpFeature(installedApp, feature);
       EfpFeature paramFe = new EfpFeature(params, feature);
       lookup.put(falibleFe, falibleAssig);
       lookup.put(osFe, osAssig);
       lookup.put(appFe, appAssig);
       lookup.put(paramFe, paramsAssig);

        EfpAssignedValue valueAssigOk =
            new EfpNamedValue(lrAssig, Arrays.asList(falibleFe, osFe, appFe, paramFe));
        valueAssigOk.setRelatedValuesCallback(finder);

        EfpValueType valueOk = valueAssigOk.computeValue();

        // the formula is valid -> the value must be returned
        Assert.assertEquals(goodMachine, valueOk);
    }

    /**
     * Test of named derived values from LR.
     * This test combines a derived property with
     *  other values from LR.
     */
    @Test
    public void testNamedDerivedValueWithLRValues() {
        EFP time = new SimpleEFP("time", null, null, null, null);
        EFP memory = new SimpleEFP("memory", null, null, null, null);

        EFP machine = new DerivedEFP("machine", null, null, null, null,
                time, memory);

        // Assignments in LR
        LrAssignment slow =
            new LrSimpleAssignment(time, "slow", new EfpNumberInterval(100, 200), lr);
        LrAssignment average =
            new LrSimpleAssignment(time, "average", new EfpNumberInterval(50, 100), lr);
        LrAssignment high =
            new LrSimpleAssignment(time, "high", new EfpNumberInterval(0, 50), lr);

        EfpValueType goodMachine = new EfpEnum("good", "nice", "perfect");

        // --------------- test with fulfilled formula -----------
        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "time <= time_slow && memory == 200", goodMachine,
                Arrays.asList(time, memory));

        LrDerivedAssignment lrAssig = new LrDerivedAssignment(
                machine, "good", eval, lr);

       EfpAssignedValue timeAssigValue = new EfpDirectValue(new EfpNumberInterval(50, 100));
       EfpAssignedValue memoryAssigValue = new EfpDirectValue(new EfpNumber(200));

       EfpAssignment timeAssig = new EfpAssignment(time, timeAssigValue, feature);
       EfpAssignment memoryAssig = new EfpAssignment(memory, memoryAssigValue, feature);

       EfpFeature timeFe = new EfpFeature(time, feature);
       EfpFeature memoryFe = new EfpFeature(memory, feature);
       lookup.put(timeFe, timeAssig);
       lookup.put(memoryFe, memoryAssig);

        EfpAssignedValue valueAssigOk =
            new EfpNamedValue(lrAssig, Arrays.asList(timeFe, memoryFe), slow, average, high);
        valueAssigOk.setRelatedValuesCallback(finder);

        EfpValueType valueOk = valueAssigOk.computeValue();

        // the formula is valid -> the value must be returned
        Assert.assertEquals(goodMachine, valueOk);
    }


    /**
     * It creates a fake implementation of the lookup class
     * loading values from other compoents.
     * @return the fake impl. which returns values that have been put into the map.
     */
    private RelatedAssignmentsFinder relatedAssignmentFake() {

        return new RelatedAssignmentsFinder() {

            @Override
            public EfpAssignedValue getConnectedAssignments(
                    final Feature requiredFeature,
                    final EFP requiredEfp) {

                EfpFeature fe = new EfpFeature(requiredEfp, requiredFeature);

                return lookup.get(fe).getEfpValue();
            }
        };
    }
}
