/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * This is a class evaluating math formulas.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class MathFormulaEvaluator extends AbstractGroovyFormulaEvaluator
        implements FormulaEvaluator {



    /**
     * Constructor.
     * @param formula a string representation of a math formula.
     */
    public MathFormulaEvaluator(final String formula) {
        super(formula);
    }

    /** {@inheritDoc} */
    @Override
    public String getGroovyScript() {
        return GroovyScripts.GROOVY_SCRIPT;
    }

}
