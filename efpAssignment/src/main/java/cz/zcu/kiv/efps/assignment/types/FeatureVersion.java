package cz.zcu.kiv.efps.assignment.types;

/**
 * Interface for working with object which represents a version of feature.
 *
 * Date: 15.3.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public interface FeatureVersion {

    /**
     * Creates and returns identifier of the version object.
     * @return a literal identifier of this version object
     */
    String getIdentifier();


    /**
     * This method returns true if THIS object matches to ANOTHER objects. The
     * very term matching means: THIS object must be the same, or a newer
     * version then ANOTHER object. If the versions are the same, a string
     * release note must be only equal.
     *
     * @param another
     *            another project which is examined to match.
     * @return true it THIS object matches (is compatible) with the ANOTHER one.
     */
    boolean matchesTo(final FeatureVersion another);
}
