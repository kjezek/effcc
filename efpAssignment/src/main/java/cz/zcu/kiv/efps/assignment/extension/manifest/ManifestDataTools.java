package cz.zcu.kiv.efps.assignment.extension.manifest;

import cz.zcu.kiv.efps.assignment.types.FeatureVersionRange;
import cz.zcu.kiv.efps.assignment.types.SimpleFeatureVersion;

import java.util.List;

/**
 * Contains methods for converting data in string into object.
 *
 * Date: 13.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class ManifestDataTools {

    /** Index of major cast of version in array. */
    private static final int CAST_MAJOR = 1;
    /** Index of minor cast of version in array. */
    private static final int CAST_MINOR = 2;
    /** Index of micro cast of version in array. */
    private static final int CAST_MICRO = 3;
    /** Index of note cast of version in array. */
    private static final int CAST_NOTE = 4;

    /**
     * Private implicit constructor.
     */
    private ManifestDataTools() { }

    /**
     * Creates object of class FeatureVersion.
     * @param version Version in string
     * @return Object of class FeatureVersion
     */
    public static SimpleFeatureVersion createSimpleVersion(final String version) {
        if (version == null || version.trim().equals("")) {
            return SimpleFeatureVersion.IMPLICIT_VERSION;
        }

        String[] versionCasts = version.split("\\.");

        switch(versionCasts.length) {
            case CAST_MAJOR:
                return new SimpleFeatureVersion(Integer.parseInt(versionCasts[CAST_MAJOR - 1]));
            case CAST_MINOR:
                return new SimpleFeatureVersion(Integer.parseInt(versionCasts[0]),
                        Integer.parseInt(versionCasts[CAST_MINOR - 1]));
            case CAST_MICRO:
                return new SimpleFeatureVersion(Integer.parseInt(versionCasts[CAST_MAJOR - 1]),
                        Integer.parseInt(versionCasts[CAST_MINOR - 1]),
                        Integer.parseInt(versionCasts[CAST_MICRO - 1]));
            case CAST_NOTE:
                return new SimpleFeatureVersion(Integer.parseInt(versionCasts[CAST_MAJOR - 1]),
                        Integer.parseInt(versionCasts[CAST_MINOR - 1]),
                        Integer.parseInt(versionCasts[CAST_MICRO - 1]),
                        versionCasts[CAST_NOTE - 1]);
            default:
                return new SimpleFeatureVersion(1);
        }
    }


    /**
     * Parse from string version range.
     * @param versionRange String with text for parsing.
     * @return Created feature version range.
     */
    public static FeatureVersionRange createVersionRange(final String versionRange) {
        if (versionRange == null) {
            return new FeatureVersionRange(FeatureVersionRange.IMPLICIT_LEFT_VERSION,
                    true, FeatureVersionRange.IMPLICIT_RIGHT_VERSION, false);
        }

        if (versionRange.indexOf(',') != -1) {
            boolean leftInclude = false;
            boolean rightInclude = false;

            if (versionRange.charAt(0) == '[') {
                leftInclude = true;
            }

            if (versionRange.charAt(versionRange.length() - 1) == ']') {
                rightInclude = true;
            }

            String leftVersionStr = versionRange.substring(1, versionRange.indexOf(',')).trim();
            String rightVersionStr = versionRange.substring(
                    versionRange.indexOf(',') + 1, versionRange.length() - 1).trim();

            SimpleFeatureVersion leftVersion;
            SimpleFeatureVersion rightVersion;

            if (leftVersionStr.equals("")) {
                leftVersion = FeatureVersionRange.IMPLICIT_LEFT_VERSION;
            } else {
                leftVersion = createSimpleVersion(leftVersionStr);
            }

            if (rightVersionStr.equals("")) {
                rightVersion = FeatureVersionRange.IMPLICIT_RIGHT_VERSION;
                rightInclude = false;
            } else {
                rightVersion = createSimpleVersion(rightVersionStr);
            }

            return new FeatureVersionRange(leftVersion, leftInclude, rightVersion, rightInclude);
        } else {
            SimpleFeatureVersion leftVersion;

            if (versionRange.equals("")) {
                leftVersion = FeatureVersionRange.IMPLICIT_LEFT_VERSION;
            } else {
                leftVersion = createSimpleVersion(versionRange);
            }

            return new FeatureVersionRange(leftVersion,
                    true, FeatureVersionRange.IMPLICIT_RIGHT_VERSION, false);
        }
    }


    /**
     * Removes from parameter value start and end mark.
     * @param value Value of the parameter.
     * @return Value without marks.
     */
    public static String removeBorderMarks(final String value) {
        if (value == null || value.trim().equals("")) {
            return value;
        }

        String val = value.trim();

        char firstChar = val.charAt(0);
        char lastChar = val.charAt(val.length() - 1);

        if ((firstChar == Constants.QUOTE && lastChar == Constants.QUOTE)
                || (firstChar == Constants.VALUE_LEFT_BRACKET
                        && lastChar == Constants.VALUE_RIGHT_BRACKET)) {
            return val.substring(1, val.length() - 1);
        }

        return val;
    }

    /**
     * Converts map with features and their parameters into string value.
     * @param featuresList List of features with their parameters.
     * @return String with features.
     */
    public static String featuresIntoString(final List<FeatureParameters> featuresList) {
        String headerValue = "";
        String clause = "";
        String paramsPreviousFeature = "";

        boolean isFirst = true;

        for (FeatureParameters featureWithParam : featuresList) {
            String featureParameters = featureWithParam.parametersToString();

            if (clause.equals("")) {
                clause = featureWithParam.getFeatureName();
                paramsPreviousFeature = featureParameters;
                continue;
            }

            if (!paramsPreviousFeature.equals("")
                    && featureParameters.equals(paramsPreviousFeature)) {

                clause += Constants.SEPARATOR_PARAMS + featureWithParam.getFeatureName();
                continue;
            }

            if (!paramsPreviousFeature.equals("")) {
                clause += Constants.SEPARATOR_PARAMS + paramsPreviousFeature;
            }

            if (!isFirst) {
                headerValue += Constants.SEPARATOR_CLAUSES + clause;
            } else {
                isFirst = false;
                headerValue += clause;
            }

            paramsPreviousFeature = featureParameters;
            clause = featureWithParam.getFeatureName();
        }



        if (!paramsPreviousFeature.equals("")) {
            clause += Constants.SEPARATOR_PARAMS + paramsPreviousFeature;
        }

        if (!isFirst) {
            headerValue += Constants.SEPARATOR_CLAUSES + clause;
        } else {
            headerValue += clause;
        }


        return headerValue;
    }
}
