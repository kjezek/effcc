/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * This class evaluates all kind of formulas
 * expressed as a string that may be evaluated as a part
 * of a groovy script.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class AbstractGroovyFormulaEvaluator implements FormulaEvaluator {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Formula. */
    private String formula;

    /**
     * It creates a new instance of the evaluator.
     *
     *  @param formula a string representation of maht. formula
     */
    public AbstractGroovyFormulaEvaluator(final String formula) {
        this.formula = formula;
    }

    /** {@inheritDoc} */
    @Override
    public EfpValueType evaluate(final List<EfpAssignment> assignments) {
        return evaluateFormula(assignments);
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return formula;
    }

    /**
     * This method will return a groovy script
     * in which a formula will be put and the script will be evaluated.
     *
     * The script should contain one pair of the '%s' characters to mark
     * the place where the formula will be put.
     * @return a string representation of the groovy script
     */
    public abstract String getGroovyScript();

    /**
     * Executes a match formula invoking it as a groovy script.
     *
     * @param values
     *            the values concerning the formula
     * @return a computed value.
     */
    private EfpValueType evaluateFormula(final List<EfpAssignment> values) {

        if (logger.isDebugEnabled()) {
            logger.debug("Evaluating formula " + formula + " for values "
                    + values + ".");
        }

        String safeFormula = formula;
        Binding binding = new Binding();
        for (EfpAssignment value : values) {

            // a name must match the name in the math formula
            String valueName = (value.getFeature().getIdentifier()
            + GroovyScripts.DELIMITER + value.getEfp().getName());

            if (logger.isTraceEnabled()) {
                logger.trace("EFP and Feature name " + value.getFeature().getIdentifier()
                        + GroovyScripts.DELIMITER + value.getEfp().getName() + " "
                        + "translated as " + valueName);
            }


            String safeValueName = GroovyScripts.scriptSafeString(valueName);
            safeFormula = GroovyScripts.replaceAll(safeFormula, valueName, safeValueName);

            if (logger.isTraceEnabled()) {
                logger.trace("Computing value for " + value.getEfpValue());
            }

            EfpValueType computedValue = value.getEfpValue().computeValue();

            if (computedValue == null) {
                if (logger.isTraceEnabled()) {
                    logger.trace("A related value is null. Returning null.");
                }

                return null;
            }


            if (logger.isTraceEnabled()) {
                logger.trace("Setting value " + computedValue + " for variable " + valueName);
            }

            binding.setVariable(safeValueName, computedValue);
        }

        GroovyShell groovyShell = new GroovyShell(binding);

        if (logger.isDebugEnabled()) {
            logger.debug("Formula is " + safeFormula);
        }

        String script = String.format(getGroovyScript(), safeFormula);

        if (logger.isDebugEnabled()) {
            logger.debug("Excuting script " + script);
        }

        // it is up to a user the formula is a correct groovy script.
        Object result = groovyShell.evaluate(script);

        if (logger.isDebugEnabled()) {
            logger.debug("Result of the evaluation " + result);
        }

        // the formula has not contained EFP value type.
        // Such formulas have not sense
        if (!(result instanceof EfpValueType)) {
            throw new IllegalArgumentException("The formula '" + formula + "' "
                    + "does not contain any EFP type. Such formulas are not allowed.");
        }

        return (EfpValueType) result;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((formula == null) ? 0 : formula.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractGroovyFormulaEvaluator other = (AbstractGroovyFormulaEvaluator) obj;
        if (formula == null) {
            if (other.formula != null) {
                return false;
            }
        } else if (!formula.equals(other.formula)) {
            return false;
        }
        return true;
    }



}
