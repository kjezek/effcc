/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandler;
import cz.zcu.kiv.efps.assignment.extension.manifest.ManifestHeaderHandlerImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataLocation;

/**
 * A convenient implementation for CoSi + OSGi bundles.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class BundleManifestReader extends AbstractOpenableSingleton<ManifestHeaderHandler> {

    /** A logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** An opened manifest. */
    private Manifest manifest;

    /** The handler for working with the manifest. */
    private ManifestHeaderHandler manifestHeaderHandler;

    /**
     * EFP Data location.
     */
    private EfpDataLocation efpDataLocation;

    /**
     *
     * @param efpDataLocation EFP Data location
     */
    public BundleManifestReader(final EfpDataLocation efpDataLocation) {
        this.efpDataLocation = efpDataLocation;
    }

    @Override
    protected ManifestHeaderHandler doOpen() {
        File manifestFile =  new File(efpDataLocation.getEfpDataPath());

        if (!manifestFile.exists()) {
            logger.error("The file with EFPs doesn't exist.");
            throw new AssignmentRTException("File with EFP assignments doesn't exist.");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read manifest from file " + manifestFile.getAbsolutePath());
        }

        manifest = new Manifest();
        manifestHeaderHandler = new ManifestHeaderHandlerImpl(manifest);
        InputStream is = null;

        if (!manifestFile.exists() || !manifestFile.isFile()) {
            throw new AssignmentRTException("File with manifest is empty.");
        }

        try {
            is = FileUtils.openInputStream(manifestFile);
            manifest.read(is);
            Attributes attributes = manifest.getMainAttributes();

            if (!attributes.containsKey(Attributes.Name.MANIFEST_VERSION)) {
                throw new AssignmentRTException(
                        "Manifest doesn't contain header 'Manifest-Version'.");
            }

        } catch (IOException e) {
            throw new AssignmentRTException(e);
        } finally {
            IOUtils.closeQuietly(is);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Instance of Manifest was created.");
        }

        return manifestHeaderHandler;
    }

    @Override
    protected void doClose() {
        if(!manifestHeaderHandler.isModified())
        {
            if (logger.isDebugEnabled()) {
                logger.debug("No changes in the manifest -> saving will be skipped.");
            }
            return;
        }

        efpDataLocation.setIsModified(true);
        File manifestFile =  new File(efpDataLocation.getEfpDataPath());

        if (logger.isDebugEnabled()) {
            logger.debug("Saving manifest into file.");
        }

        if (manifestFile.exists()) {
            try {
                FileUtils.forceDelete(manifestFile);
            } catch (IOException e) {
                throw new AssignmentRTException(e);
            }
        }

        FileOutputStream fos = null;
        try {
            fos = FileUtils.openOutputStream(manifestFile);
            manifest.write(fos);
            fos.write('\n');
            fos.close();
        } catch (IOException e) {
            throw new AssignmentRTException(e);
        } finally {
            IOUtils.closeQuietly(fos);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Saving manifest into file is completed.");
        }

    }
}
