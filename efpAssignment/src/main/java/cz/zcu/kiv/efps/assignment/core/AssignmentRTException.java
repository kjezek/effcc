package cz.zcu.kiv.efps.assignment.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a common runtime exception
 * thrown from the common assignment implementation
 * which is used in common situations concerning
 * error with loading, saving, or modifying CoSi bundle's
 * manifest files.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class AssignmentRTException extends RuntimeException {

    /** Logger.  */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** UID. */
    private static final long serialVersionUID = 7835402880618065958L;

    /**
     * Empty constructor.
     */
    public AssignmentRTException() {
        super();
    }

    /**
     * Constructor with two parameters.
     * @param message String with message
     * @param cause Cause
     */
    public AssignmentRTException(final String message, final Throwable cause) {
        super(message, cause);
        logger.error(message);
    }


    /**
     * Constructor with message parameter.
     * @param message Message
     */
    public AssignmentRTException(final String message) {
        super(message);
        logger.error(message);
    }

    /**
     * Constructor with cause parameter.
     * @param cause Cause
     */
    public AssignmentRTException(final Throwable cause) {
        super(cause);
        logger.error(cause.getLocalizedMessage());
    }
}
