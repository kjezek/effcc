package cz.zcu.kiv.efps.assignment.types;

import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The main class that represents an assignment of an extra-functional property
 * to a feature (e.g. a service).
 * This assignment hold the e-f property and its value.
 * The value may be a direct value (a concrete number, string, boolean, etc),
 * a named value (with a name that has been assigned in local registry),
 * or a math formula (concerning other properties, math functions, operators,
 * operands, etc.).
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek <a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpAssignment {

    /** An extra-functional property. */
    private EFP efp;
    /** A value assigned to the property. */
    private EfpAssignedValue efpValue;
    /** A feature the property is assigned to. */
    private Feature feature;

    /**
     *
     * @param efp an extra-functional property
     * @param efpValue A value assigned to the property
     * @param feature A feature the property is assigned to
     */
    public EfpAssignment(
            final EFP efp,
            final EfpAssignedValue efpValue,
            final Feature feature) {

        this.efp = efp;
        this.efpValue = efpValue;
        this.feature = feature;
    }

    /**
     * @return efp an extra-functional property
     */
    public final EFP getEfp() {
        return efp;
    }

    /**
     * @return efpValue A value assigned to the property
     */
    public final EfpAssignedValue getEfpValue() {
        return efpValue;
    }

    /**
     * @return A feature the property is assigned to
     */
    public final Feature getFeature() {
        return feature;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return "EfpAssignment [efp=" + efp + ", efpValue=" + efpValue
                + ", feature=" + feature + "]";
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result
                + ((efpValue == null) ? 0 : efpValue.hashCode());
        result = prime * result + ((feature == null) ? 0 : feature.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpAssignment other = (EfpAssignment) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (efpValue == null) {
            if (other.efpValue != null) {
                return false;
            }
        } else if (!efpValue.equals(other.efpValue)) {
            return false;
        }
        if (feature == null) {
            if (other.feature != null) {
                return false;
            }
        } else if (!feature.equals(other.feature)) {
            return false;
        }
        return true;
    }
}
