package cz.zcu.kiv.efps.assignment.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.repomirror.api.MirroredDataManipulator;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This is a tool class providing several
 * functions for manipulations with EfpAssignments.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class AssignmentsTools {

    /** It is private. */
    private AssignmentsTools() { }

    /**
     * The method finds a value in a list of assignments which fits to a given LR.
     * @param assignments the list of assginments.
     * @param lr a LR to find a value for
     * @param efp an EFP to find a value for.
     * @return a value or null.
     */
    public static EfpAssignedValue findValue(
            final List<EfpAssignment> assignments, final EFP efp, final LR lr) {

        for (EfpAssignment efpAssignment : assignments) {
            if (efpAssignment.getEfpValue() == null) {
                continue;
            }

            EFP currentEfp = efpAssignment.getEfp();
            LR currentLR = efpAssignment.getEfpValue().getLR();
            // we have the same efp with the same LR.
            // Values that are not in LR are returned all the time (they are valid in all LRs)
            //
            if (currentEfp.equals(efp)) {

                // EFP is in requested LR.
                if (lr != null && currentLR != null && lr.equals(currentLR)) {
                    return efpAssignment.getEfpValue();
                }

                // EFP is not in LR and a user requested a value not being in any LR
                if (lr == null && currentLR == null) {
                    return efpAssignment.getEfpValue();
                }

            }
        }

        return null;

    }


    /**
     * Builds EFP assignment from LR derived assignment. In logical formula
     * finds all EFP and participating LR assignments.
     * @param efpDataManipulator a manipulator of raw component data.
     * @param lrDerivedAssignment LR derived assignment.
     * @param xmlDataAccessor Reference to opened local repository reader,
     * where will be searched participating LR assignments.
     * @return EFP assignment with the LR derived assignment.
     */
    public static EfpAssignedValue setAssignmentWithLrDerivedAssign(
            final EfpDataManipulator efpDataManipulator,
            final LrDerivedAssignment lrDerivedAssignment,
            final MirroredDataManipulator xmlDataAccessor) {

        List<EfpFeature> efpFeatures = new ArrayList<EfpFeature>();

        List<EFP> efps = ((DerivedEFP) lrDerivedAssignment.getEfp()).getEfps();

        String formula = lrDerivedAssignment.getEvaluator().serialise();

        if (formula == null || formula.equals("")) {
            return new EfpNamedValue(
                    lrDerivedAssignment,
                    efpFeatures,
                    new LrSimpleAssignment[0]);
        }


        for (EFP particuralEfp : efps) {

            if (formula.indexOf(particuralEfp.getName() + "_") != -1) {

                // TODO [kjezek] I must create pairs for current EFP and all
                // assignments having this EFP on this component. The correct approach would be to
                // find an exact assignment where this EFP is assigned.
                // However the relation of feature and deriving EFP is not currently
                // contained within the assignment model.
                for (Feature feature : efpDataManipulator.readFeatures()) {
                    for (EfpLink efpLink : efpDataManipulator.readEFPs(feature)) {
                        if (efpLink.getEfpName().equals(particuralEfp.getName())) {
                            efpFeatures.add(new EfpFeature(particuralEfp, feature));
                        }
                    }
                }
            }
        }

        List<LrSimpleAssignment> participatingLrAssigns =
                xmlDataAccessor.readLrPartAssignments(lrDerivedAssignment);

        return new EfpNamedValue(
                lrDerivedAssignment,
                efpFeatures,
                participatingLrAssigns
                .toArray(new LrSimpleAssignment[participatingLrAssigns.size()]));
    }





    /**
     * Creates list of EFPs from list of EFP assignments.
     * @param efpAssignments List of EFP assignments.
     * @return List of EFPs
     */
    public static List<EFP> getListEFPFromEfpAssignments(final List<EfpAssignment> efpAssignments) {
        Set<EFP> efpSet = new HashSet<EFP>();

        for (EfpAssignment efpAssignment : efpAssignments) {
            efpSet.add(efpAssignment.getEfp());
        }

        return new ArrayList<EFP>(efpSet);
    }


    /**
     * Controls if a list of EFPs contains concrete EFP.
     * @param efps A list of EFPs
     * @param searchedEfp Searched EFP
     * @return True if contains otherwise false
     */
    public static boolean containEFP(final List<EFP> efps, final EFP searchedEfp) {

        for (EFP efp : efps) {
            if (efp.equals(searchedEfp)) {
                return true;
            }

            if (efp.getType() == EFP.EfpType.DERIVED) {
                if (containEFP(((DerivedEFP) efp).getEfps(), searchedEfp)) {
                    return true;
                }
            }
        }

        return false;
    }
}
