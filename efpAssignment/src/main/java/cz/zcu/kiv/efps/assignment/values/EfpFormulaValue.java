package cz.zcu.kiv.efps.assignment.values;

import java.util.Arrays;
import java.util.List;

import cz.zcu.kiv.efps.assignment.evaluator.FormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.MathFormulaEvaluator;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LR;

/**
 * The representation of a mathematical formula assigned
 * as an extra-functional value for a property.
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public final class EfpFormulaValue extends AbstractEfpAssignedValue {

    /** An evaluator. */
    private FormulaEvaluator evaluator;

    /**
     * Creates an instance of an assigned formula value.
     * @param formula the formula
     * @param efpFeatures features participating in the formula
     */
    public EfpFormulaValue(
            final String formula,
            final EfpFeature... efpFeatures) {
        super(Arrays.asList(efpFeatures));

        this.evaluator = new MathFormulaEvaluator(formula);
    }

    /**
     * Creates an instance of an assigned formula value.
     * @param evaluator evaluator
     * @param efpFeatures features participating in the formula
     */
    public EfpFormulaValue(
            final FormulaEvaluator evaluator,
            final EfpFeature... efpFeatures) {
        super(Arrays.asList(efpFeatures));

        this.evaluator = evaluator;
    }


    /** {@inheritDoc} */
    @Override
    protected EfpValueType doComputeValue(final List<EfpAssignment> values) {
        return evaluator.evaluate(values);
    }

    /**
     *
     * @return a formula wrapped in this assignment
     */
    public String getFormula() {
        return evaluator.serialise();
    }

    /** {@inheritDoc}*/
    @Override
    public LR getLR() {
        return null;
    }

    /** {@inheritDoc}*/
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((evaluator == null) ? 0 : evaluator.hashCode());
        return result;
    }

    /** {@inheritDoc}*/
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpFormulaValue other = (EfpFormulaValue) obj;
        if (evaluator == null) {
            if (other.evaluator != null) {
                return false;
            }
        } else if (!evaluator.equals(other.evaluator)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc}*/
    @Override
    public String getValueName() {
        return null;
    }

    /** {@inheritDoc}*/
    @Override
    public String getSerialisedValue() {
        return evaluator.serialise();
    }

    /** {@inheritDoc}*/
    @Override
    public AssignmentType getAssignmentType() {

        return EfpAssignedValue.AssignmentType.formula;
    }

    /** {@inheritDoc}*/
    @Override
    public String toString() { return getFormula(); }
}
