package cz.zcu.kiv.efps.assignment.extension.api;

import java.util.LinkedList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.extension.AbstractOpenableSingleton;
import cz.zcu.kiv.efps.assignment.extension.Openable;



/**
 * API for getting and updating files with EFP assignments.
 *
 * Date: 20.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public abstract class AbstractEfpDataLocation
    extends AbstractOpenableSingleton<ComponentEfpDataRepresentation>
    implements EfpDataLocation {

    private List<Openable<?>> allOpened = new LinkedList<Openable<?>>();

    /** {@inheritDoc} */
    @Override
    protected final ComponentEfpDataRepresentation doOpen() {

        final EfpDataLocation location = openComponent();

        ComponentEfpDataRepresentation result = new ComponentEfpDataRepresentation() {

            /** {@inheritDoc} */
            @Override
            public <T> T getEfpData(Openable<T> openable) {
                allOpened.add(openable);
                return openable.open();
            }

            /** {@inheritDoc} */
            @Override
            public String getComponentPath() {
                return location.getComponentPath();
            }

            /** {@inheritDoc} */
            @Override
            public String getEfpDataPath() {
                return location.getEfpDataPath();
            }

            /** {@inheritDoc} */
            @Override
            public String getEfpMirrorPath() {
                return location.getEfpMirrorPath();
            }

            /** {@inheritDoc} */
            @Override
            public void setIsModified(boolean isModified) {
                location.setIsModified(isModified);
            }
        };

        return result;
    }



    /** {@inheritDoc} */
    @Override
    protected final void doClose() {
        // close all
        for (Openable<?> opened : allOpened) {
            opened.close();
        }

        closeComponent();
    }

    /** {@inheritDoc} */
    @Override
    public final String getComponentPath() {
        return open().getComponentPath();
    }

    /** {@inheritDoc} */
    @Override
    public final String getEfpDataPath() {
        return open().getEfpDataPath();
    }

    /** {@inheritDoc} */
    @Override
    public final String getEfpMirrorPath() {
        return open().getEfpMirrorPath();
    }

    /** {@inheritDoc} */
    @Override
    public final void setIsModified(final boolean isModified) {
        open().setIsModified(isModified);
    }

    /**
     * This method opens a component allowing to read
     * EFPs data.
     * @return Returns object with paths on required files from the component.
     */
    protected abstract EfpDataLocation openComponent();

    /**
     * This method closes a component opened before.
     */
    protected abstract void closeComponent();

}
