package cz.zcu.kiv.efps.assignment.extension.manifest;

import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;


/**
 * Reads data from Manifest and parses them into blocks.
 * <p/>
 * Date: 23.11.2010
 *
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ManifestHeaderHandlerImpl implements ManifestHeaderHandler {
    /**
     * Reference to Manifest.
     */
    private Manifest manifest;
    /**
     * TRUE - the manifest was modified, FALSE - the manifest is without modification.
     */
    private boolean isModified;


    /**
     * Default constructor for setting reference to opened manifest.
     *
     * @param mf Manifest
     */
    public ManifestHeaderHandlerImpl(final Manifest mf) {
        manifest = mf;
        isModified = false;
    }


    @Override
    public List<FeatureParameters> readFeatureHeader(final String headerName) {
        String headerValue = readMainAttribute(headerName);
        return HeaderParser.parseHeader(headerValue);
    }


    @Override
    public String readMainAttribute(final String header) {
        Attributes attribute = manifest.getMainAttributes();

        if (!attribute.containsKey(new Attributes.Name(header))) {
            return "";
        }

        return attribute.getValue(new Attributes.Name(header));
    }


    @Override
    public void writeFeatureHeader(final String headerName,
                                   final List<FeatureParameters> featuresWithParams) {
        String featuresInString = ManifestDataTools.featuresIntoString(featuresWithParams);

        Attributes attribute = manifest.getMainAttributes();
        attribute.put(new Attributes.Name(headerName), featuresInString);

        isModified = true;
    }


    @Override
    public void writeSimpleHeader(final String headerName, final String headerValue) {
        Attributes attribute = manifest.getMainAttributes();
        attribute.put(new Attributes.Name(headerName), headerValue);
        isModified = true;
    }


    @Override
    public boolean isModified() {
        return isModified;
    }
}
