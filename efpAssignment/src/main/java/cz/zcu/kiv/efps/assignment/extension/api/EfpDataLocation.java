/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension.api;


/**
 * A interface responsible for opening a component
 * to load EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface EfpDataLocation  {


    /**
     * This method returns a path where a component is located.
     * @return a path to the component.
     */
    String getComponentPath();

    /**
     *
     * @return a path to EFP data.
     */
    String getEfpDataPath();

    /**
     *
     * @return a path to EFP mirror
     */
    String getEfpMirrorPath();

    /**
     * Sets an information whether the component should be updated.
     * @param modified true = the component was changed, false = no changes.
     */
    void setIsModified(boolean modified);
}
