/**
 *
 */
package cz.zcu.kiv.efps.assignment.repomirror.api;

import cz.zcu.kiv.efps.assignment.extension.AbstractOpenableSingleton;
import cz.zcu.kiv.efps.assignment.extension.Openable;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;

/**
 * An implementation of {@link MirroredDataManipulator}
 * allowing to open an EFP mirror on a component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T> a type of the EFP mirror representation
 */
public abstract class AbstractMirroredDataManipulator<T>
	implements MirroredDataManipulator, Openable<T> {

    /** A class to localise EFP data. */
    private AbstractEfpDataLocation efpDataLocation;

    /** Delegates an open request to be processed only once. */
    private Openable<T> singletonDelegator = new AbstractOpenableSingleton<T>() {

		@Override
		protected T doOpen() {
			return AbstractMirroredDataManipulator.this.open();
		}

		@Override
		protected void doClose() {
			AbstractMirroredDataManipulator.this.close();
		}
	};

    /**
     * Constructor.
     * @param dataLocation EFP Data location
     */
    public AbstractMirroredDataManipulator(final AbstractEfpDataLocation dataLocation) {
        super();
        this.efpDataLocation = dataLocation;
    }

    /**
     * This method opens a component and locates EFP data mirror on the component.
     * This method opens a component first delegatin its call to {@link AbstractEfpDataLocation#open()},
     * then an EFP mirror is opened delegating the call to a sub class.
     * Several call of this method will open the EFP mirror only once.
     *
     * @return a generic representation of the EFP data mirror.
     */
    protected T openEfpMirror() {
        return efpDataLocation.open().getEfpData(singletonDelegator);
    }

}
