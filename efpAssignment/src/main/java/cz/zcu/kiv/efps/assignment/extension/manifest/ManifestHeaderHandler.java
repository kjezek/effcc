package cz.zcu.kiv.efps.assignment.extension.manifest;

import java.util.List;



/**
 * Interface for manipulation with values of headers in the manifest.
 *
 * Date: 22.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public interface ManifestHeaderHandler {
    /**
     * Reads from header value as features. It will focus on the splitting into
     * features and their parameters
     * @param headerName Name of the header
     * @return List of features with their parameters
     */
    List<FeatureParameters> readFeatureHeader(String headerName);


    /**
     * Writes into the manifest new value of feature type.
     * @param headerName Name of a header
     * @param featuresWithParams List of features with their parameters
     */
    void writeFeatureHeader(String headerName, List<FeatureParameters> featuresWithParams);


    /**
     * Writes into the manifest new simple value.
     * @param headerName Name of the header.
     * @param headerValue Value
     */
    void writeSimpleHeader(final String headerName, final String headerValue);


    /**
     * Reads value of one main attribute from Manifest.
     * @param header Name of header
     * @return Value of concrete header
     */
    String readMainAttribute(final String header);


    /**
     * Returns information if the manifest was modified.
     * @return TRUE - the manifest was modified, FALSE - the manifest was only read.
     */
    boolean isModified();
}
