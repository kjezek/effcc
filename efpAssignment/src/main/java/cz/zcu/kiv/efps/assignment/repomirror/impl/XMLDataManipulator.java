package cz.zcu.kiv.efps.assignment.repomirror.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataLocation;
import cz.zcu.kiv.efps.assignment.repomirror.api.AbstractMirroredDataManipulator;
import cz.zcu.kiv.efps.assignment.repomirror.support.JAXBParameters;
import cz.zcu.kiv.efps.assignment.repomirror.support.LocalRepositoryReader;
import cz.zcu.kiv.efps.assignment.repomirror.support.LocalRepositoryUpdater;
import cz.zcu.kiv.efps.assignment.repomirror.support.ValidatorErrors;
import cz.zcu.kiv.efps.assignment.repository.generated.ObjectFactory;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;


/**
 * Class for working with XML which contains data about EFP assignments.
 *
 * Date: 21.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class XMLDataManipulator extends AbstractMirroredDataManipulator<JAXBElement<?>> {
    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(XMLDataManipulator.class);

    /** EFP Data location. */
    private EfpDataLocation efpDataLocation;
    /** XML repository. */
    private JAXBElement<?> xmlRepo;
    /** For indication of changes. TRUE = is modified, FALSE = without modification. */
    private boolean isModified = false;

    /**
     * Constructor for setting empty repository.
     * @param efpDataLocation an object which is aware of EFP mirrored data
     */
    public XMLDataManipulator(final AbstractEfpDataLocation efpDataLocation) {
        super(efpDataLocation);

        this.efpDataLocation = efpDataLocation;
    }



    @Override
    public EFP readEFP(final String efpName, final int grId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read a EFP with name " + efpName + " from GR with ID " + grId
                    + " from xml.");
        }

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        EFP efp = locRepReader.readEFP(efpName, grId);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + efp);
        }

        return efp;
    }

    @Override
    public EFP readEFP(final String efpName, final String grIdStr) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read a EFP with name " + efpName
                    + " from GR with string ID " + grIdStr + " from xml.");
        }

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        EFP efp = locRepReader.readEFP(efpName, grIdStr);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + efp);
        }

        return efp;
    }


    @Override
    public LrAssignment readLrAssignment(final EFP efp,
            final int lrId, final String valueName) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read LR assignment from LR with ID "
                    + lrId + " with value's name " + valueName + " of EFP " + efp + ".");
        }
        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        LrAssignment lrAssignment = locRepReader.readLrAssignment(efp, lrId, valueName);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + lrAssignment);
        }

        return lrAssignment;
    }

    @Override
    public LrAssignment readLrAssignment(
            final EFP efp, final String lrIdStr, final String valueName) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read LR assignment from LR with string ID "
                    + lrIdStr + " with value's name " + valueName + " of EFP " + efp + ".");
        }
        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        LrAssignment lrAssignment = locRepReader.readLrAssignment(efp, lrIdStr, valueName);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + lrAssignment);
        }

        return lrAssignment;
    }

    @Override
    public List<LrSimpleAssignment> readLrPartAssignments(final LrDerivedAssignment assignment) {

        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read particular LR assignments for derived LR assignment: "
                    + assignment + ".");
        }

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        List<LrSimpleAssignment> lrAssignmentList = locRepReader.readLrPartAssignments(assignment);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + lrAssignmentList);
        }

        return lrAssignmentList;
    }


    @Override
    public String readParamObject(final int id) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to read value of EFP assignment with "
                    + "direct or math.formula value by ID " + id + ".");
        }

        LocalRepositoryReader locRepReader = new LocalRepositoryReader(openEfpMirror());
        String otherValue = locRepReader.readOtherValue(id);

        if (logger.isDebugEnabled()) {
            logger.debug("Result: " + otherValue);
        }

        return otherValue;
    }


    @Override
    public Integer addEfpAssignment(final EfpAssignment efpAssignment) {
        if (logger.isDebugEnabled()) {
            logger.debug("Writing into repository new EFP assignment " + efpAssignment);
        }

        isModified = true;

        JAXBElement<?> localRepository = openEfpMirror();
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        Integer id = locRepUpdater.writeAssignment(efpAssignment);

        if (logger.isDebugEnabled()) {
            logger.debug("XML with added parts of the EFP assignment "
                    + efpAssignment + " was saved.");
        }

        return id;
    }


    @Override
    public void deleteEFP(final EFP efp) {
        if (logger.isDebugEnabled()) {
            logger.debug("Deleting of the EFP " + efp + " from repository.");
        }

        isModified = true;

        JAXBElement<?> localRepository = openEfpMirror();
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.deleteEFP(efp);

        if (logger.isDebugEnabled()) {
            logger.debug("The EFP '" + efp + "' was deleted from repository.");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Repository with the deleted efp " + efp + " was saved into file.");
        }
    }


    @Override
    public void deleteParamObject(final int id) {
        if (logger.isDebugEnabled()) {
            logger.debug("Deleting of direct or math.formula value with id "
                    + id + " from repository.");
        }

        isModified = true;

        JAXBElement<?> localRepository = openEfpMirror();
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.deleteOtherValue(id);

        if (logger.isDebugEnabled()) {
            logger.debug("The direct or math.formula value with id " + id
                    + " was deleted from repository.");
        }
    }


    @Override
    public void deleteLRAssignment(final LrAssignment lrAssignment) {
        if (logger.isDebugEnabled()) {
            logger.debug("Deleting of the LR assignment " + lrAssignment + " from repository.");
        }

        isModified = true;

        JAXBElement<?> localRepository = openEfpMirror();
        LocalRepositoryUpdater locRepUpdater = new LocalRepositoryUpdater(localRepository);

        locRepUpdater.deleteLrAssignment(lrAssignment);

        if (logger.isDebugEnabled()) {
            logger.debug("The LR assignment " + lrAssignment + " was deleted from repository.");
        }
    }

    /** {@inheritDoc} */
	@Override
	public JAXBElement<?> open() {
        File repFile = new File(efpDataLocation.getEfpMirrorPath());

        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source sourceFile = new StreamSource(
                LocalRepositoryReader.class.getClassLoader().getResourceAsStream(
                        JAXBParameters.SCHEMA));
        Source inFile;

        if (repFile.exists()) {
            try {
                Schema schemaXSD = sf.newSchema(sourceFile);
                javax.xml.validation.Validator validator = schemaXSD.newValidator();
                validator.setErrorHandler(new ValidatorErrors());

                inFile = new StreamSource(repFile);
                validator.validate(inFile);

                JAXBContext jc = JAXBContext.newInstance(JAXBParameters.OBJECT_FACTORY_CLASS);
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                xmlRepo = (JAXBElement<?>) unmarshaller.unmarshal(repFile);

            } catch (Exception e) {
                throw new AssignmentRTException(e);
            }

        } else {
            ObjectFactory objFact = new ObjectFactory();
            xmlRepo = objFact.createEfpMirror(objFact.createRepositoriesElement());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("New XML with local repository was read from file: "
                    + repFile.getAbsolutePath());
        }

        return xmlRepo;	}



    /** {@inheritDoc} */
	@Override
	public void close() {
        if (!isModified) {
            if (logger.isDebugEnabled()) {
                logger.debug("No changes in repository -> saving into file will be skipped.");
            }
            return;
        }

        efpDataLocation.setIsModified(true);
        File repFile = new File(efpDataLocation.getEfpMirrorPath());

        if (repFile.exists()) {
            try {
                FileUtils.forceDelete(repFile);
            } catch (IOException e) {
                logger.error("Can't delete file on location for saving repository: " + e, e);
                throw new AssignmentRTException(e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Calling method saveActualRepository() on file "
                    + repFile.getAbsolutePath());
        }

        try {
            JAXBContext jc = JAXBContext.newInstance(JAXBParameters.OBJECT_FACTORY_CLASS);

            Marshaller u = jc.createMarshaller();
            u.setProperty(Marshaller.JAXB_ENCODING, JAXBParameters.ENCODING);
            u.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            u.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, JAXBParameters.SCHEMA);

            FileOutputStream outputStream = FileUtils.openOutputStream(repFile);
            boolean isCreated = false;

            try {
                u.marshal(xmlRepo, outputStream);
                if (logger.isDebugEnabled()) {
                    logger.debug("XML with repository was written into file: "
                            + repFile.getAbsolutePath());
                }

                isCreated = true;
            } finally {
                IOUtils.closeQuietly(outputStream);
                if (!isCreated) {
                    FileDeleteStrategy.NORMAL.delete(repFile);
                }
            }
        } catch (JAXBException e) {
            logger.error("An Error during writing a content of the repository into file: "
                    + e, e);
            throw new AssignmentRTException(e);
        } catch (IOException e) {
            logger.error("An Error during writing a content of the repository into file: "
                    + e, e);
            throw new AssignmentRTException(e);
        }
    }
}
