package cz.zcu.kiv.efps.assignment.extension.manifest;

/**
 * Constants for working with data from manifest.
 *
 * Date: 23.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class Constants {

    /**
     * Private constructor.
     */
    private Constants() {
    }


    /** Separator between parts of feature. */
    public static final char SEPARATOR_PARAMS = ';';

    /** Separator between features in one string. */
    public static final char SEPARATOR_CLAUSES = ',';

    /** Symbol for assignment a value to parameter as attribute. */
    public static final String EQUAL = "=";

    /** Symblo for assignment a value to EFP. */
    public static final String EQUAL_EFP = "=";

    /** Symbol for left mark of the value in brackets. */
    public static final char VALUE_LEFT_BRACKET = '(';

    /** Symbol for right mark of the value in brackets. */
    public static final char VALUE_RIGHT_BRACKET = ')';

    /** Symbol for marks of the value in quotes. */
    public static final char QUOTE = '"';

    /** Symbol for assignment a value to parameter as directive. */
    public static final String ASSIGNMENT_DIRECTIVES = ":=";

    /** Separator between data of EFP. */
    public static final char SEPARATOR_DATA = '.';
}
