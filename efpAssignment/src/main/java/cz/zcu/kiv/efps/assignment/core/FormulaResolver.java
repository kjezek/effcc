package cz.zcu.kiv.efps.assignment.core;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Buffer for temporary saving data of math.formula before their creating.
 *
 * Date: 2.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class FormulaResolver {
    /** List with data of math.formula. */
    private List<PartialFormulaAssignment> formulaBuffer;
    /** EFP access. */
    private ComponentEfpAccessor efpAccessor;


    /**
     * Default constructor.
     * @param efpAccessor object to access EFPs on a component.
     */
    public FormulaResolver(ComponentEfpAccessor efpAccessor) {
        this.formulaBuffer = new ArrayList<PartialFormulaAssignment>();
        this.efpAccessor = efpAccessor;
    }


    /**
     * Adds math formula to buffer.
     * @param feature Feature which owns formula.
     * @param efp EFP which for is created formula.
     * @param formula Math.formula.
     */
    public void addFormula(final Feature feature, final EFP efp, final String formula) {
        formulaBuffer.add(new PartialFormulaAssignment(feature, efp, formula, efpAccessor));
    }


    /**
     * Creates from buffer items and returns EFP assignments with math.formula.
     * @return List EFP assignments.
     */
    public List<EfpAssignment> getEfpAssignments() {

        List<EfpAssignment> efpAssignments = new ArrayList<EfpAssignment>();

        for (PartialFormulaAssignment preMathFormAssigns : formulaBuffer) {
            efpAssignments.add(preMathFormAssigns.createEFPAssignment());
        }

        return efpAssignments;
    }
}
