package cz.zcu.kiv.efps.assignment.types;


import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;

/**
 * A general feature that may have assigned an extra-functional property.
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */

public class BasicFeature implements Feature {

    /** A name of this feature. */
    private String name;

    /** A type of this feature. */
    private Feature.AssignmentSide side;

    /** A version of this feature. */
    private FeatureVersion version;

    /** A type of the element which this feature represents. */
    private String representElement;

    /** A parent feature. */
    private Feature parent;

    /** a flag indicating whether this feature is mandatory to
     * be presented on a component. */
    private boolean mandatory;

    /** A additional name. */
    private String additionalName;

    /** A datatype which is represented by feature. */
    private String featureDatatype;

    /** A referenced feature (f.e. a required feature can specify source bundle of the feature). */
    private Feature referencedFeature;


    /**
     * Create an instance of a feature with additional name.
     * @param name a name of the feature
     * @param side a type of the feature
     * @param representElement an element which this feature represents
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
     * @param version a version of the feature
     * @param additionalName a additional name
     * @return the new feature
     */
    public static BasicFeature CreateFeatureWithAdditionalName(
            final String name, final Feature.AssignmentSide side, final String representElement,
            final Boolean mandatory, final Feature parent, final FeatureVersion version,
            final String additionalName)
    {
        BasicFeature bf = new BasicFeature(name, side, representElement, mandatory, parent, version);
        bf.additionalName = additionalName;

        return bf;
    }

    /**
     * Create an instance of a feature with additional name.
     * @param name a name of the feature
     * @param side a type of the feature
     * @param representElement an element which this feature represents
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
     * @param parent a parent feature
     * (e.g. parent for a method is a class, parent for a class is package)
     * @param version a version of the feature
     * @param featureDatatype a datatype
     * @return the new feature
     */
    public static BasicFeature CreateFeatureWithDatatype(
            final String name, final Feature.AssignmentSide side, final String representElement,
            final Boolean mandatory, final Feature parent, final FeatureVersion version,
            final String featureDatatype)
    {
        BasicFeature bf = new BasicFeature(name, side, representElement, mandatory, parent, version);
        bf.featureDatatype = featureDatatype;

        return bf;
    }

    /**
     * Create an instance of a feature with additional name.
     * @param name a name of the feature
     * @param side a type of the feature
     * @param representElement an element which this feature represents
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
     * @param parent a parent feature
     * (e.g. parent for a method is a class, parent for a class is package)
     * @param version a version of the feature
     * @param referencedFeature a referenced feature
     * @return the new feature
     */
    public static BasicFeature CreateFeatureWithReferenceOnFeature(
            final String name, final Feature.AssignmentSide side, final String representElement,
            final Boolean mandatory, final Feature parent, final FeatureVersion version,
            final Feature referencedFeature)
    {
        BasicFeature bf = new BasicFeature(name, side, representElement, mandatory, parent, version);
        bf.referencedFeature = referencedFeature;

        return bf;
    }

    /**
     * Creates an instance of a feature.
     *
     * @param name a name of the feature
     * @param side a type of the feature
     * @param representElement an element which this feature represents
     * (e.g. class, method, interface)
     * @param parent a parent feature
     * (e.g. parent for a method is a class, parent for a class is package)
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
     */
    public BasicFeature(
            final String name, final Feature.AssignmentSide side,
            final String representElement, final Boolean mandatory, final Feature parent) {

        this(name, side, representElement, mandatory, parent, null);
    }

   /**
    * Creates an instance of a feature.
    *
    * @param name a name of the feature
    * @param side a type of the feature
    * @param representElement an element which this feature represents
    * (e.g. class, method, interface)
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
    */
   public BasicFeature(
           final String name, final Feature.AssignmentSide side,
           final String representElement, final Boolean mandatory) {
       this.name = name;
       this.side = side;
       this.representElement = representElement;
       this.mandatory = mandatory;
   }


   /**
    * Creates an instance of a feature.
    *
    * @param name a name of the feature
    * @param side a type of the feature
    * @param version a version of the feature
    * @param representElement an element which this feature represents
    * (e.g. class, method, interface)
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
    */
   public BasicFeature(
           final String name, final Feature.AssignmentSide side,
           final String representElement, final Boolean mandatory, final FeatureVersion version) {

       this.name = name;
       this.side = side;
       this.representElement = representElement;
       this.mandatory = mandatory;
       this.version = version;
   }

    /**
     * Creates an instance of a feature.
     *
     * @param name a name of the feature
     * @param side a type of the feature
     * @param version a version of the feature
     * @param representElement an element which this feature represents
     * (e.g. class, method, interface)
     * @param parent a parent feature
     * (e.g. parent for a method is a class, parent for a class is package)
     * @param mandatory a flag indicating whether this feature is mandatory to
     * be presented on a component.
     */
    public BasicFeature(
            final String name, final Feature.AssignmentSide side,
            final String representElement, final Boolean mandatory, final Feature parent,
            final FeatureVersion version) {

        this.name = name;
        this.side = side;
        this.representElement = representElement;
        this.mandatory = mandatory;
        this.parent = parent;
        this.version= version;
    }

    /** {@inheritDoc}  */
    @Override
    public final String getIdentifier() {

        StringBuffer buffer = new StringBuffer()
                .append(representElement)
                .append(".").append(name);

        if (version != null && version instanceof SimpleFeatureVersion) {
            buffer.append(",").append(version.getIdentifier());
        }

        return buffer.toString();
    }

    /** {@inheritDoc}  */
    @Override
    public final String getName() {
        return name;
    }

    /** {@inheritDoc}  */
    @Override
    public final AssignmentSide getSide() {
        return side;
    }

    /** {@inheritDoc}  */
    @Override
    public final FeatureVersion getVersion() {
        return version;
    }

    /** {@inheritDoc}  */
    @Override
    public final String getRepresentElement() {
        return representElement;
    }

    /** {@inheritDoc}  */
    @Override
    public final Feature getParent() {
        return parent;
    }

    /** {@inheritDoc}  */
    @Override
    public final boolean isMandatory() {
        return mandatory;
    }

    /**
     * This method matches THIS object with ANOTHER one.
     * The features match when:
     * <ul>
     * <li> the features has the same names
     * <li> the features are attached to the same element (e.g. component, interface, method)
     * <li> the versions of features matches, which means
     * <li> a provided feature is matches with a required one or vice versa
     * (no features with the same types)
     * <ul>
     * <li> for a provided feature being matched to a required feature:
     * a provided feature must be the same or a newer version than the required one
     * <li> for a required feature begin matched to a provided feature:
     * a required feature must be the same or an older version than the provided one.
     * </ul>
     * </ul>
     * @param feature feature to be matched
     * @return true if the features match, false otherwise
     */
    public boolean matchesTo(final Feature feature) {
        if (!feature.getClass().isAssignableFrom(BasicFeature.class)) {
            return false;
        }

        BasicFeature another = (BasicFeature) feature;
        // different type, the same names, the same element
        boolean result = getSide() != another.getSide()
            && getName().equals(another.getName())
            && getRepresentElement().equals(another.getRepresentElement());

        //comparing additional names
        if (result) {
            result = additionalName == another.additionalName
                    || (additionalName != null && additionalName.equals(another.additionalName));
        }

        //comparing types
        if (result) {
            result = featureDatatype == another.featureDatatype
                    || (featureDatatype != null && featureDatatype.equals(another.featureDatatype));
        }

       // compare versions if there are any
        if (result &&  getVersion() != null && another.getVersion() != null) {
            if (getSide() == AssignmentSide.REQUIRED && another.getSide()
                    == AssignmentSide.PROVIDED) {

                result = another.getVersion().matchesTo(getVersion());
            }
            if (getSide() == AssignmentSide.PROVIDED && another.getSide()
                    == AssignmentSide.REQUIRED) {

                result = getVersion().matchesTo(another.getVersion());
            }
        }

        // TODO [kjezek, B] default matching function here
        // however I suppose to avoid this all class in the future
        // coz it is used only for OSGi implementation
        // in which we would like to participate in OSGi binding mechanism
        // instead.
        MatchingFunction mf = MatchingFunction.DEFAULT_MATCHING_FUNCTION;

        //compare source bundles
        if (result && getSide() == AssignmentSide.REQUIRED && another.getSide()
                == AssignmentSide.PROVIDED) {
            result = (referencedFeature == null || mf.matches(referencedFeature, another.referencedFeature));
        }

        if (result && getSide() == AssignmentSide.PROVIDED && another.getSide()
                == AssignmentSide.REQUIRED) {
            result = (another.referencedFeature == null
                    || mf.matches(referencedFeature, another.referencedFeature));
        }

        return result;
    }

    /** {@inheritDoc}  */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime
                * result
                + ((representElement == null) ? 0 : representElement.hashCode());
        result = prime * result + ((side == null) ? 0 : side.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /** {@inheritDoc}  */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BasicFeature other = (BasicFeature) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (parent == null) {
            if (other.parent != null) {
                return false;
            }
        } else if (!parent.equals(other.parent)) {
            return false;
        }
        if (!representElement.equals(other.representElement)) {
            return false;
        }
        if (side != other.side) {
            return false;
        }
        if (version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!version.equals(other.version)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return getIdentifier();
    }
}
