/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.EfpFeature;

/**
 * This is a helper class which allows a user
 * to test mathematical formulas before
 * they are used on components.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class MathFormulaHelper {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** A string represenation of the formula. */
    private String formula;

    /**
     * Creates instance.
     * @param formula a formula used in this helper class.
     */
    public MathFormulaHelper(final String formula) {
        this.formula = formula;
    }

    /**
     * This method takes a list of efp-feature pairs on input
     * and results a list of the pairs used in the formula.
     * @param possibleEfps the list of possilble pairs
     * @return the list of really used pairs.
     */
    public List<EfpFeature> findUsedEfps(final EfpFeature... possibleEfps) {

        if (logger.isDebugEnabled()) {
            logger.debug("Verifying if the list " + possibleEfps
                    + " is used in the formula: " + formula);
        }

        List<EfpFeature> result = new LinkedList<EfpFeature>();

        for (EfpFeature efpFeature : possibleEfps) {
            String variable =
                efpFeature.getFeature().getIdentifier()
                + "_"
                + efpFeature.getEfp().getName();

            if (formula.indexOf(variable) != -1) {
                result.add(efpFeature);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("A set of used EFPs: " + result);
        }

        return result;
    }

}
