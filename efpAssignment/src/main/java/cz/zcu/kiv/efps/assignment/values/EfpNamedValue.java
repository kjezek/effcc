package cz.zcu.kiv.efps.assignment.values;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.AssignedValueFinder;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrAssignment.LrAssignmentType;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The representation of a value denoted by a name from local registry.
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public final class EfpNamedValue extends AbstractEfpAssignedValue {

    /** A link to a value from LR. */
    private LrAssignment lrAssignment;

    /** LR assignments participating in the logical formula. */
    private LrAssignment[] lrParticipatingAssignments;

    /**
     * Constructs an instance for simple EFP value.
     * @param lrAssignment a link to a value from LR.
     */
    public EfpNamedValue(final LrAssignment lrAssignment) {
        super(new ArrayList<EfpFeature>());

        this.lrAssignment = lrAssignment;
    }

    /**
     * Construct an instance for derived EFP values.
     * @param lrAssignment derived property from LR
     * @param efpFeatures values participating in computation of this property
     * @param lrParticipatingAssignments LR assignments participating in the logical formula
     */
    public EfpNamedValue(
            final LrDerivedAssignment lrAssignment,
            final List<EfpFeature> efpFeatures,
            final LrAssignment... lrParticipatingAssignments) {
        super(efpFeatures);

        this.lrAssignment = lrAssignment;
        this.lrParticipatingAssignments = lrParticipatingAssignments;
    }

    /** {@inheritDoc} */
    @Override
    protected EfpValueType doComputeValue(final List<EfpAssignment> values) {
        EfpValueType value = null;

        // resolve simple property
        if (LrAssignmentType.SIMPLE.equals(lrAssignment.getAssignmentType())) {
            value = ((LrSimpleAssignment) lrAssignment).getEfpValue();
        }

        // resolve derived property
        if (LrAssignmentType.DERIVED.equals(lrAssignment.getAssignmentType())) {

            // TODO [kjezek, A] we do not check from which Feature the EFP and value
            // go. The EfpAssignment object should be extended to hold this information.
            // I.e. it should hold from which features the deriving EFPs should be used.
            // For now, there could be a clash if one EFP is assigned to multiple features.
            final Map<EFP, EfpValueType> relatedValues = new HashMap<EFP, EfpValueType>();
            for (EfpAssignment assignment : values) {
                relatedValues.put(assignment.getEfp(), assignment.getEfpValue().computeValue());
            }


            final AssignedValueFinder finder = new AssignedValueFinder() {

                @Override
                public EfpValueType findValue(final EFP efp) {
                    EfpValueType value = relatedValues.get(efp);
                    return value;
                }
            };

            value = ((LrDerivedAssignment) lrAssignment).deriveValue(finder,
                    Arrays.asList(lrParticipatingAssignments));

        }

        return value;
    }


    /**
     *
     * @return Array with participating LR assignments.
     */
    public LrAssignment[] getLrParticipatingAssignments() {
        return lrParticipatingAssignments;
    }

    /**
     *
     * @return a LR-related value wrapped in this assignment
     */
    public LrAssignment getLrAssignment() {
        return lrAssignment;
    }

    /** {@inheritDoc}*/
    @Override
    public LR getLR() {
        return lrAssignment.getLr();
    }

    /** {@inheritDoc}*/
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
        + ((lrAssignment == null) ? 0 : lrAssignment.hashCode());
        return result;
    }

    /** {@inheritDoc}*/
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpNamedValue other = (EfpNamedValue) obj;
        if (lrAssignment == null) {
            if (other.lrAssignment != null) {
                return false;
            }
        } else if (!lrAssignment.equals(other.lrAssignment)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc}*/
    @Override
    public String toString() {
        return "" + lrAssignment.getLr();
    }

    /** {@inheritDoc}*/
    @Override
    public String getValueName() {
        return lrAssignment.getValueName();
    }

    /** {@inheritDoc}*/
    @Override
    public String getSerialisedValue() {
        return computeValue().serialise();
    }

    /** {@inheritDoc}*/
    @Override
    public AssignmentType getAssignmentType() {
        return EfpAssignedValue.AssignmentType.named;
    }
}
