package cz.zcu.kiv.efps.assignment.evaluator;

import java.util.List;

/**
 * It is a default implementation delegating
 * the matching function to {@link MatchingFunction#DEFAULT_MATCHING_FUNCTION}.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class DefaultMatchingFunctionFactory implements MatchingFunctionFactory {


    @Override
    public MatchingFunction createMatchingFunction(final List<String> components) {
        return MatchingFunction.DEFAULT_MATCHING_FUNCTION;
    }
}
