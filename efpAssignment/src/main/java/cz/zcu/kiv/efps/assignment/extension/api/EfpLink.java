package cz.zcu.kiv.efps.assignment.extension.api;

import cz.zcu.kiv.efps.assignment.extension.manifest.Constants;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * Contains information about one EFP assignment.
 *
 * Date: 1.3.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class EfpLink {
    /** ID of GR. */
    private Integer grId = null;
    /** String ID of GR. */
    private String grIdStr = null;
    /** EFP name of next record. */
    private String efpName = null;
    /** Type of assigned value of record. */
    private EfpAssignedValue.AssignmentType typeOfAssignedValue = null;

    /** LR id. */
    private Integer lrId = null;
    /** String LR id. */
    private String lrIdStr = null;
    /** Name of value from EFP. */
    private String valueName = null;
    /** ID of math.formula or direct value. */
    private Integer paramObjectId = null;
    /** Value. */
    private EfpValueType efpValue = null;


    /**
     * Constructor for information about EFP assignment with LR assignment.
     * @param grId ID of a GR
     * @param grIdStr String ID of a GR
     * @param efpName A EFP
     * @param lrId ID of a LR
     * @param lrIdStr String ID of a LR
     * @param valueName A name of value
     * @param efpValue A value
     */
    public EfpLink(final Integer grId, final String grIdStr, final String efpName,
            final Integer lrId,
            final String lrIdStr, final String valueName, final EfpValueType efpValue) {
        this.grId = grId;
        this.grIdStr = grIdStr;
        this.efpName = efpName;
        this.typeOfAssignedValue = EfpAssignedValue.AssignmentType.named;
        this.lrId = lrId;
        this.lrIdStr = lrIdStr;
        this.valueName = valueName;
        this.efpValue = efpValue;
    }


    /**
     * Constructor for information about EFP assignment with direct value or math. formula.
     * @param grId ID of a GR
     * @param grIdStr String ID of a GR
     * @param efpName A EFP
     * @param assignmentType Type of value
     * @param otherValueId ID of direct or math.formula value
     * @param efpValue A value
     */
    public EfpLink(final Integer grId, final String grIdStr, final String efpName,
            final EfpAssignedValue.AssignmentType assignmentType, final EfpValueType efpValue,
            final Integer otherValueId) {
        this.grId = grId;
        this.grIdStr = grIdStr;
        this.efpName = efpName;
        this.typeOfAssignedValue = assignmentType;
        this.efpValue = efpValue;
        this.paramObjectId = otherValueId;
    }

    /**
     * Constructor for information about EFP assignment with empty assigment.
     * @param grId ID of a GR
     * @param grIdStr String ID of a GR
     * @param efpName A EFP
     */
    public EfpLink(final Integer grId, final String grIdStr, final String efpName) {
        this.grId = grId;
        this.grIdStr = grIdStr;
        this.efpName = efpName;
    }

    @Override
    public String toString() {
        String str = "" + grId + Constants.SEPARATOR_DATA + efpName;

        if (typeOfAssignedValue == EfpAssignedValue.AssignmentType.named) {
            str += Constants.EQUAL_EFP + typeOfAssignedValue + "{"
                + lrId + Constants.SEPARATOR_DATA + valueName + "}";

        } else if (typeOfAssignedValue == EfpAssignedValue.AssignmentType.direct
                || typeOfAssignedValue == EfpAssignedValue.AssignmentType.formula) {
            str += Constants.EQUAL_EFP + typeOfAssignedValue + "{" + paramObjectId + "}";
        }

        return str;
    }


    /**
     * @return the grId
     */
    public Integer getGrId() {
        return grId;
    }

    /**
     * @return the string ID of GR
     */
    public String getGrIdStr() { return grIdStr; }

    /**
     * @return the efpName
     */
    public String getEfpName() {
        return efpName;
    }

    /**
     * @return the typeOfAssignedValue
     */
    public EfpAssignedValue.AssignmentType getTypeOfAssignedValue() {
        return typeOfAssignedValue;
    }

    /**
     * @return the lrId
     */
    public Integer getLrId() { return lrId; }

    /**
     * @return the lrIdStr
     */
    public String getLrIdStr() {
        return lrIdStr;
    }

    /**
     * @return the valueName
     */
    public String getValueName() {
        return valueName;
    }

    /**
     * @return the otherValueId
     */
    public Integer getParamObjectId() {
        return paramObjectId;
    }

    /**
     * @return The value
     */
    public EfpValueType getEfpValue() { return efpValue; }
}
