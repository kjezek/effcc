/**
 *
 */
package cz.zcu.kiv.efps.assignment.client.plugin;

import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunctionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;

import java.util.List;

/**
 * This class is a plugin implementation for
 * the EFP Assignment plugin concept allowing
 * to use several implementation for different component
 * model.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class EfpPluginConfiguredLoader implements
        EfpAwareComponentLoader,
        MatchingFunctionFactory {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** A configured API. */
    private PluginAPIWrapper configured = null;

    /**
     * This method should return a name of a .properties file
     * with the plugin configuration available on the classpath.
     * @return the .properties file name
     */
    public abstract String getPluginConfigurationFile();

    /** {@inheritDoc} */
    @Override
    public final ComponentEfpAccessor loadForRead(final String component) {
        logger.debug("Creating a loader for the component: " + component);
        return loadForUpdate(component);
    }

    /** {@inheritDoc} */
    @Override
    public final ComponentEFPModifier loadForUpdate(final String component) {
        logger.debug("Creating a loader for the component: " + component);
        return configureOnce().createComponentModifier(component);
    }

    @Override
    public MatchingFunction createMatchingFunction(
            final List<String> components) {

        logger.debug("Creating a matching function.");

        return configureOnce().createMatchingFunction(components);
    }

    /**
     * This method configures API plugin once.
     * @return the API wrapper
     */
    private PluginAPIWrapper configureOnce() {

        if (configured == null) {
            configured = PluginConfiguration.configure(getPluginConfigurationFile());
        }

        return configured;
    }
}
