package cz.zcu.kiv.efps.assignment.api;


import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunctionFactory;

/**
 * This interface serves as an API for those
 * who need to obtain or modify EFPs attached to a component.
 * @author Jan Svab
  * @author Kamil Jezek [kjezek@kiv.zcu.cz]
*/
public interface EfpAwareComponentLoader extends MatchingFunctionFactory {

    /**
     * The implementation should load a representation of a component.
     * @param component this string represents a component indetifier which allows
     * an implementation to load component. Typically it is a path + name of the component.
     * @return an interface allowing to access componen's assignments
     */
     ComponentEfpAccessor loadForRead(String component);

     /**
      * The implementation should load a representation of a component.
      * @param component this string represents a component indetifier which allows
      * an implementation to load component. Typically it is a path + name of the component.
      * @return an interface allowing to access componen's assignments
      */
      ComponentEFPModifier loadForUpdate(String component);

}
