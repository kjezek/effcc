/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import java.util.List;

import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * Interface for all formula evaluators.
 *
 * The evaluators evaluate functions
 * f: EFP x EfpAssignedValue x Feature -> EfpValue <br>
 *
 * It means that all EFP, Values assigned to a Features and features
 * are projected to EFP values.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface FormulaEvaluator {

    /**
     * It computes a list of EFP assignments into concrete EFP value.
     *
     * @param assignments a list of assignments.
     * @return a computed value.
     */
    EfpValueType evaluate(List<EfpAssignment> assignments);

    /**
     * This method serializes a formula representation into
     * a string.
     * @return a string
     */
    String serialise();
}
