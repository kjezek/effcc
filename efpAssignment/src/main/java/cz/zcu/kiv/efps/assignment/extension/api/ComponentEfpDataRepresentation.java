/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension.api;

import cz.zcu.kiv.efps.assignment.extension.Openable;

/**
 * This interface represents a representation
 * of opened component. An implementation
 * should be created by opening a component first.
 * Then this interface contains methods to
 * get particular information of the component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface ComponentEfpDataRepresentation extends EfpDataLocation {

    /**
     * Returns an object with information of a EFP data loaded from a component.
     *
     * @param <T> a type to be opened
     * @param openable an implementation allowing to open the data
     * @return Path to the object with EFP assignments.
     *
     */
    <T> T getEfpData(Openable<T> openable);



}
