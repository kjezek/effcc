/**
 *
 */
package cz.zcu.kiv.efps.assignment.client.plugin;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;

import java.util.List;

/**
 * This interface allows to access methods
 * wrapped by particular plugins.
 *
 * @author Kamil Jezek  [kjezek@kiv.zcu.cz]
 */
public interface PluginAPIWrapper {


    /**
     * This method creates a component loader according to the data from configuration.
     * @param component a component name.
     * @return the component loader implementation defined in the plugin
     */
    ComponentEFPModifier createComponentModifier(String component);

    /**
     * This method creates a matching function for given input components.
     * @param components the components to create the matching function for.
     * @return the matching function
     */
    MatchingFunction createMatchingFunction(List<String> components);

}
