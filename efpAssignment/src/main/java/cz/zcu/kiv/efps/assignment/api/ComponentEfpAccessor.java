package cz.zcu.kiv.efps.assignment.api;

import java.util.List;
import java.util.Set;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This interface accesses EFPs attached to a component.
 * It allows to read all feature, EFPs and values attached
 * to the EFPs on the component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface ComponentEfpAccessor {

    /**
     * The implementation should returns all EFPs which belong to a concrete feature.
     * @param feature Editing feature.
     * @return List of EfpAssignments
     */
    List<EFP> getEfps(Feature feature);

    /**
     * This method lists all features being able to have attached EFPs of the component.
     * @return the list of features being able to hold EFPs
     */
    List<Feature> getAllFeatures();

    /**
     * The implementation should returns value assignment to the efp on feature.
     * @param feature Editing feature.
     * @param efp EFP
     * @param lr LR which a value should be returned for. A null value signalises that
     * the returned value is not assigned to any LR. E.g. direct value.
     * @return Assigned EFP value
     */
    EfpAssignedValue getAssignedValue(Feature feature, EFP efp, LR lr);

    /**
     * This method returns all LRs that are used on a component.
     * @return the list of LRs
     */
    Set<LR> getLRs();

    /**
     * This method closes all persisting files
     * and evidently deletes all temporary files.
     */
    void close();

}
