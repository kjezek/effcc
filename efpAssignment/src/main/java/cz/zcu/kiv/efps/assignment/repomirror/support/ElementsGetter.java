package cz.zcu.kiv.efps.assignment.repomirror.support;

import java.util.List;

import cz.zcu.kiv.efps.assignment.repository.generated.*;
import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpRatio;
import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;

/**
 * Static class which provides methods for getting required elements from
 * other elements in repository.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class ElementsGetter {

    /**
     * Private constructor.
     */
    private ElementsGetter() {
    }

    /**
     * Return type of value in some EFP.
     * @param typeOfValue Selected type of values in enum EfpTypeOfValueElement.
     * @return Class which represents type of values in some EFP.
     */
    public static Class<?> getEfpValueType(final EfpTypeOfValueElement typeOfValue) {

        if (typeOfValue == EfpTypeOfValueElement.EFP_BOOLEAN) {
            return EfpBoolean.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_ENUM) {
            return EfpEnum.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_NUMBER_INTERVAL) {
            return EfpNumberInterval.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_RATIO) {
            return EfpRatio.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_COMPLEX) {
            return EfpComplex.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_NUMBER) {
            return EfpNumber.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_SET) {
            return EfpSet.class;
        } else if (typeOfValue == EfpTypeOfValueElement.EFP_STRING) {
            return EfpString.class;
        }

        return null;
    }


    /**
     * Finds required RepositoryElement in RepositoriesElement by GR ID.
     * @param repositories RepositoriesElement which contains objects RepositoryElement.
     * @param grID ID of GR
     * @return Required RepositoryElement or null
     */
    public static RepositoryElement getRepositoryElement(
            final RepositoriesElement repositories, final int grID) {

        List<RepositoryElement> repsList = repositories.getRegistries();

        for (RepositoryElement repositoryElement : repsList) {
            if (repositoryElement.getGr().getId().intValue() == grID) {
                return repositoryElement;
            }
        }

        return null;
    }


    /**
     * Finds required RepositoryElement in RepositoriesElement by GR ID.
     * @param repositories RepositoriesElement which contains objects RepositoryElement.
     * @param grIDStr A string ID of GR
     * @return Required RepositoryElement or null
     */
    public static RepositoryElement getRepositoryElement(
            final RepositoriesElement repositories, final String grIDStr) {

        List<RepositoryElement> repsList = repositories.getRegistries();

        for (RepositoryElement repositoryElement : repsList) {
            if (repositoryElement.getGr().getIdStr().equals(grIDStr)) {
                return repositoryElement;
            }
        }

        return null;
    }


    /**
     * Find required SimpleEFPElement or DerivedEFPElement in concrete RepositoryElement
     * by EFP name.
     * @param repository RepositoryElement
     * @param efpName Name of EFP
     * @return Required SimpleEFPElement or DerivedEFPElement or null
     */
    public static Object getSimpleOrDerivedEFPElement(final RepositoryElement repository,
            final String efpName) {

        List<Object> listOfProperties =
            repository.getGr().getEfps().getSimpleOrDerived();

        for (Object property : listOfProperties) {
            if (property.getClass() == SimpleEFPElement.class) {
                if (((SimpleEFPElement) property).getName().equals(efpName)) {
                    return property;
                }

            } else if (property.getClass() == DerivedEFPElement.class) {
                if (((DerivedEFPElement) property).getName().equals(efpName)) {
                    return property;
                }

            } else {
                throw new RuntimeException("Unknown class which represents EFP element.");
            }
        }

        return null;
    }


    /**
     * Find required SimpleEFPElement or DerivedEFPElement in concrete RepositoryElement
     * by EFP id.
     * @param repository RepositoryElement
     * @param efpId Id of EFP
     * @return Required SimpleEFPElement or DerivedEFPElement or null
     */
    public static Object getSimpleOrDerivedEFPElement(final RepositoryElement repository,
            final int efpId) {

        List<Object> listOfProperties =
            repository.getGr().getEfps().getSimpleOrDerived();

        for (Object property : listOfProperties) {
            if (property.getClass() == SimpleEFPElement.class) {
                if (((SimpleEFPElement) property).getId().intValue() == efpId) {
                    return property;
                }

            } else if (property.getClass() == DerivedEFPElement.class) {
                if (((DerivedEFPElement) property).getId().intValue() == efpId) {
                    return property;
                }

            } else {
                throw new RuntimeException("Unknown class which represents EFP element.");
            }
        }

        return null;
    }


    /**
     * Finds required LRElement in concrete RepositoryElement by LR id.
     * @param repository RepositoryElement
     * @param lrId ID of LR
     * @return Required LRElement or null
     */
    public static LRElement getLRElement(
            final RepositoryElement repository, final int lrId) {
        List<LRElement> listOfLRElem = repository.getLr();

        for (LRElement lrElem : listOfLRElem) {
            if (lrElem.getId().intValue() == lrId) {
                return lrElem;
            }
        }

        return null;
    }


    /**
     * Finds required LRElement in concrete RepositoryElement by LR string ID.
     * @param repository RepositoryElement
     * @param lrIdStr A string ID of LR
     * @return Required LRElement or null
     */
    public static LRElement getLRElement(
            final RepositoryElement repository, final String lrIdStr) {
        List<LRElement> listOfLRElem = repository.getLr();

        for (LRElement lrElem : listOfLRElem) {
            if (lrElem.getIdStr().equals(lrIdStr)) {
                return lrElem;
            }
        }

        return null;
    }


    /**
     * Finds required LrSimpleAssignmentElement or LrDerivedAssignmentElement in concrete LRElement
     * by EFP name and name of value.
     * @param lrElement LRElement
     * @param efpId Id of EFP which contains assignment.
     * @param nameOfValue Name of value which contains assignment.
     * @return Required LrSimpleAssignmentElement or LrDerivedAssignmentElement or null.
     */
    public static Object getSimpleOrDerivedAssignmentElement(
            final LRElement lrElement, final int efpId, final String nameOfValue) {

        List<Object> listOfLrAssignments = lrElement.getSimpleAssignmentOrDerivedAssignment();

        for (Object lrAssignment : listOfLrAssignments) {
            if (lrAssignment.getClass() == LrSimpleAssignmentElement.class) {

                LrSimpleAssignmentElement simpleLrAssign =
                    ((LrSimpleAssignmentElement) lrAssignment);

                if (simpleLrAssign.getEfpId().intValue() == efpId
                    && simpleLrAssign.getValueName().equals(nameOfValue)) {

                    return lrAssignment;
                }
            } else {
                LrDerivedAssignmentElement derivedLrAssign =
                    ((LrDerivedAssignmentElement) lrAssignment);

                if (derivedLrAssign.getEfpId().intValue() == efpId
                    && derivedLrAssign.getValueName().equals(nameOfValue)) {

                    return lrAssignment;
                }
            }
        }

        return null;
    }


    /**
     * Returns from concrete repository other value with required id.
     * @param paramValue Element with other values
     * @param id ID of required other value
     * @return Other value or null.
     */
    public static ParameterValueElement getParamValue(
            final ParameterValues paramValue, final int id) {

        if (paramValue == null) {
            return null;
        }

        List<ParameterValueElement> paramValues = paramValue.getParameter();

        for (ParameterValueElement item : paramValues) {
            if (id == item.getId().intValue()) {
                return item;
            }
        }

        return null;
    }
}
