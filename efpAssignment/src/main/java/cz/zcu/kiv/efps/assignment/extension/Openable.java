/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension;

/**
 * A generic representation of an object
 * which may be opened. E.g. a component may be opened,
 * a EFP data storage may be opened
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T> a type to be opened
 */
public interface Openable<T> {


    /**
     *
     * @return an object that has been opened
     */
    T open();

    /**
     * It closes an object previously opened.
     *
     */
    void close();
}
