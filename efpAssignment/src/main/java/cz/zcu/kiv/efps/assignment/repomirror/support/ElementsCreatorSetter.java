package cz.zcu.kiv.efps.assignment.repomirror.support;

import java.math.BigInteger;
import java.util.List;

import cz.zcu.kiv.efps.assignment.repository.generated.*;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * It provides methods for creating Elements into repository from
 * objects of types in package efpTypes.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ElementsCreatorSetter {

    /**
     * Creates EfpTypeOfValueElement object.
     * @param nameOfDataType Simple name of class which represents some datatype.
     * @return New EfpTypeOfValueElement object
     */
    public EfpTypeOfValueElement getEfpTypeOfValueElement(final String nameOfDataType) {
        return EfpTypeOfValueElement.fromValue(nameOfDataType);
    }


    /**
     * Creates RepositoryElement object.
     * @param gr GR for which this RepositoryElement object is created.
     * @return New RepositoryElement object
     */
    public RepositoryElement createAndSetRepositoryElement(final GR gr) {
        ObjectFactory objFact = new ObjectFactory();

        RepositoryElement newRepository = objFact.createRepositoryElement();
        GRElement grElement = objFact.createGRElement();
        EfpsElement propertiesElement = objFact.createEfpsElement();

        grElement.setId(BigInteger.valueOf(gr.getId()));
        grElement.setName(gr.getName());
        grElement.setIdStr(gr.getIdStr());
        grElement.setEfps(propertiesElement);

        newRepository.setGr(grElement);

        return newRepository;
    }

    /**
     * Creates MetaElement object.
     * @param meta Meta for which this MetaElement object is created.
     * @return New MetaElement object
     */
    public MetaElement createAndSetMetaElement(final Meta meta) {
        ObjectFactory objFact = new ObjectFactory();

        MetaElement metaElement = new MetaElement();
        ListOfNamesElement listOfNamesElem = objFact.createListOfNamesElement();

        listOfNamesElem.getName().addAll(meta.getNames());

        if (meta.getUnit() != null) {
            metaElement.setUnit(meta.getUnit());
        } else {
            metaElement.setUnit("");
        }

        metaElement.setNames(listOfNamesElem);

        return metaElement;
    }

    /**
     * Creates GammaElement object.
     * @param gamma Gamma for which this GammaElement object is created.
     * @return New GammaElement object
     */
    public GammaElement createAndSetGammaElement(final Gamma gamma) {
        ObjectFactory objFact = new ObjectFactory();

        GammaElement gammaElem = objFact.createGammaElement();

        if (gamma != null) {
            GammaTypeElement gammaTypeElem =
                GammaTypeElement.fromValue(gamma.getClass().getSimpleName());

            gammaElem.setType(gammaTypeElem);

            if (gamma.getClass() == UserGamma.class) {
                gammaElem.setFunction(((UserGamma) gamma).getFunction());
            } else {
                gammaElem.setFunction("");
            }
        }

        return gammaElem;
    }


    /**
     * Creates ReferenceToEFPElement object.
     * @param efp EFP for which this ReferenceToEFPElement object is created.
     * @return New ReferenceToEFPElement object
     */
    public ReferenceToEFPElement createAndSetReferenceToEFPElement(final EFP efp) {
        ObjectFactory objFact = new ObjectFactory();

        ReferenceToEFPElement referenceToEFPElem = objFact.createReferenceToEFPElement();
        referenceToEFPElem.setId(BigInteger.valueOf(efp.getId()));

        return referenceToEFPElem;
    }


    /**
     * Creates PropertyReferencesElement object.
     * @param repository RepositoryElement for saving efps, which aren't there.
     * @param efps List of objects EFP
     * @return New PropertyReferencesElement object
     */
    public PropertyReferencesElement createAndSetPropRefsElement(
            final RepositoryElement repository, final List<EFP> efps) {

        ObjectFactory objFact = new ObjectFactory();

        PropertyReferencesElement propertyRefsElem = objFact.createPropertyReferencesElement();

        for (EFP efp : efps) {
            Object efpInGRElement = ElementsGetter.getSimpleOrDerivedEFPElement(
                    repository, efp.getName());

            if (efpInGRElement == null) {

                if (efp.getType() == EfpType.SIMPLE) {
                    efpInGRElement = createAndSetSimpleEFPElement((SimpleEFP) efp);

                } else {
                    PropertyReferencesElement pre =
                        createAndSetPropRefsElement(repository,
                                ((DerivedEFP) efp).getEfps());
                    efpInGRElement =
                        createAndSetDerivedEFPElement(
                                (DerivedEFP) efp, pre);
                }


                repository.getGr().getEfps().
                    getSimpleOrDerived().add(efpInGRElement);
            }

            propertyRefsElem.getProperty().add(createAndSetReferenceToEFPElement(efp));
        }

        return propertyRefsElem;
    }


    /**
     * Creates SimpleEFPElement object.
     * @param efp Simple efp
     * @return New SimpleEFPElement object
     */
    public SimpleEFPElement createAndSetSimpleEFPElement(final SimpleEFP efp) {

        ObjectFactory objFact = new ObjectFactory();
        SimpleEFPElement simpleEfpElem = objFact.createSimpleEFPElement();

        simpleEfpElem.setId(BigInteger.valueOf(efp.getId()));
        simpleEfpElem.setName(efp.getName());
        simpleEfpElem.setMeta(createAndSetMetaElement(efp.getMeta()));
        simpleEfpElem.setGamma(createAndSetGammaElement(efp.getGamma()));
        simpleEfpElem.setValueType(getEfpTypeOfValueElement(
                efp.getValueType().getSimpleName()));

        return simpleEfpElem;
    }

    /**
     * Creates DerivedEFPElement object.
     * @param efp Derived efp
     * @param pre PropertyReferencesElement which contains objects ReferenceToEFPElement.
     * @return New DerivedEFPElement object
     */
    public DerivedEFPElement createAndSetDerivedEFPElement(
            final DerivedEFP efp,
            final PropertyReferencesElement pre) {

        ObjectFactory objFact = new ObjectFactory();
        DerivedEFPElement derivedEfpElem = objFact.createDerivedEFPElement();

        derivedEfpElem.setEfps(pre);
        derivedEfpElem.setId(BigInteger.valueOf(efp.getId()));
        derivedEfpElem.setName(efp.getName());
        derivedEfpElem.setMeta(createAndSetMetaElement(efp.getMeta()));
        derivedEfpElem.setGamma(createAndSetGammaElement(efp.getGamma()));
        derivedEfpElem.setValueType(getEfpTypeOfValueElement(
                efp.getValueType().getSimpleName()));

        return derivedEfpElem;
    }


    /**
     * Creates LRElement object.
     * @param lr LR
     * @return New LRElement object.
     */
    public LRElement createAndSetLRElement(final LR lr) {

        ObjectFactory objFact = new ObjectFactory();
        LRElement lrElem = objFact.createLRElement();
        lrElem.setId(BigInteger.valueOf(lr.getId()));
        lrElem.setName(lr.getName());
        lrElem.setIdStr(lr.getIdStr());

        return lrElem;
    }


    /**
     * Creates LrSimpleAssignmentElement object.
     * @param lrAssignment LrSimpleAssignment
     * @return New LrSimpleAssignmentElement object.
     */
    public LrSimpleAssignmentElement createAndSetLrSimAssignElement(
            final LrSimpleAssignment lrAssignment) {

        ObjectFactory objFact = new ObjectFactory();

        LrSimpleAssignmentElement assignmentElem = objFact.createLrSimpleAssignmentElement();

        assignmentElem.setEfpId(BigInteger.valueOf(lrAssignment.getEfp().getId()));
        assignmentElem.setValueName(lrAssignment.getValueName());
        assignmentElem.setId(BigInteger.valueOf(lrAssignment.getId()));
        assignmentElem.setEfpValue(EfpSerialiser.serialize(lrAssignment.getEfpValue()));

        return assignmentElem;
    }


    /**
     * Creates LrDerivedAssignmentElement object.
     * @param lrAssignment LrSimpleAssignment
     * @param lrParticularAssignments List of particular LR assignments
     * @return New LrDerivedAssignmentElement object.
     */
    public LrDerivedAssignmentElement createAndSetLrDerAssignElement(
            final LrDerivedAssignment lrAssignment, final LrAssignment[] lrParticularAssignments) {

        ObjectFactory objFact = new ObjectFactory();
        LrDerivedAssignmentElement assignmentElem = objFact.createLrDerivedAssignmentElement();

        assignmentElem.setEfpId(BigInteger.valueOf(lrAssignment.getEfp().getId()));
        assignmentElem.setValueName(lrAssignment.getValueName());
        assignmentElem.setId(BigInteger.valueOf(lrAssignment.getId()));

        // TODO [kjezek, A] I deal only with a logical formula here.
        LrConstraintEvaluator eval = (LrConstraintEvaluator) lrAssignment.getEvaluator();

        assignmentElem.setEfpValue(EfpSerialiser.serialize(eval.getExpectedValue()));
        assignmentElem.setFormula(eval.getLogicalRule());

        if (lrParticularAssignments != null) {
            assignmentElem.setLrPartAssignIDs(
                    createAndSetLrPartAssignmentIDs(lrParticularAssignments));
        }

        return assignmentElem;
    }


    /**
     * Creates element with requested value and ID.
     * @param value The value
     * @param id ID of element in XML
     * @return New OtherValueElement object.
     */
    public ParameterValueElement createAndSetStringValue(
            final String value, final int id) {

        ObjectFactory objFact = new ObjectFactory();

        ParameterValueElement otherValueElem = objFact.createParameterValueElement();
        otherValueElem.setId(BigInteger.valueOf(id));
        otherValueElem.setValue(value);

        return otherValueElem;
    }

    /**
     * Creates element with identifiers of particular LR assignments.
     * @param lrParticularAssignments List of particular LR assignments
     * @return New LrPartAssignmentIDs object with identifiers of particular LR assignments.
     */
    public LrPartAssignmentIDs createAndSetLrPartAssignmentIDs(
            final LrAssignment[] lrParticularAssignments) {

        ObjectFactory objFact = new ObjectFactory();
        LrPartAssignmentIDs lrPartAssignmentIDs = objFact.createLrPartAssignmentIDs();


        for (LrAssignment lrPartAssignment : lrParticularAssignments) {
            LrPartAssignmentIDElement lrPartAssignmentIDElement =
                    objFact.createLrPartAssignmentIDElement();
            lrPartAssignmentIDElement.setEfp(lrPartAssignment.getEfp().getName());
            lrPartAssignmentIDElement.setLrID(BigInteger.valueOf(lrPartAssignment.getLr().getId()));
            lrPartAssignmentIDElement.setLrValueName(lrPartAssignment.getValueName());

            lrPartAssignmentIDs.getLrPartAssignment().add(lrPartAssignmentIDElement);
        }

        return lrPartAssignmentIDs;
    }
}
