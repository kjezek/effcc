package cz.zcu.kiv.efps.assignment.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.core.EfpInMemoryData.EfpInMemoryDataLoader;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.repomirror.api.MirroredDataManipulator;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This is a convenient implementation of {@link ComponentEFPModifier}.
 * It aggregates classes working with the EFP component model specific data
 * and the EFP common mirror providing one facade transparently accessing EFP data.
 *
 * Date: 22.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ComponentEFPModifierImpl implements ComponentEFPModifier {

    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(ComponentEFPModifierImpl.class);
    /** For manipulation with files in bundle. */
    private AbstractEfpDataLocation efpDataLocation;
    /** The manipulator of EFP component assignments. */
    private EfpDataManipulator efpDataManipulator;
    /** The manipulator of mirrored EFP data. */
    private MirroredDataManipulator mirroredDataManipulator;
    /** The in memory data loader. */
    private EfpInMemoryDataLoader efpInMemoryDataLoader;


    /**
     * Default constructor.
     * @param efpDataLocation a submodule managing physical location of EFP data files.
     * @param efpDataManipulator a component model based EFP data manipulator
     * @param mirroredDataManipulator an EFP data mirror manipulator.
     */
    public ComponentEFPModifierImpl(
            final AbstractEfpDataLocation efpDataLocation,
            final EfpDataManipulator efpDataManipulator,
            final MirroredDataManipulator mirroredDataManipulator) {

        this.efpDataLocation = efpDataLocation;
        this.efpDataManipulator = efpDataManipulator;
        this.mirroredDataManipulator = mirroredDataManipulator;
        this.efpInMemoryDataLoader = EfpInMemoryData.createLoader(
            efpDataManipulator, mirroredDataManipulator, this);

    }


    @Override
    public List<EFP> getEfps(final Feature feature) {
        if (logger.isDebugEnabled()) {
            logger.debug("A request for getting list of EFPs in a feature "
                    + feature.getIdentifier());
        }

        List<EFP> efps = AssignmentsTools.getListEFPFromEfpAssignments(
                efpInMemoryDataLoader.loadData().getAssignments(feature));

        if (logger.isDebugEnabled()) {
            logger.debug("A list of EFPs: " + efps);
        }

        return efps;
    }


    @Override
    public List<Feature> getAllFeatures() {
        if (logger.isDebugEnabled()) {
            logger.debug("A request for getting all features from component.");
        }

        List<Feature> features = new ArrayList<Feature>(
                efpInMemoryDataLoader.loadData().getFeatures());

        if (logger.isDebugEnabled()) {
            logger.debug("A list of features in component: " + features);
        }

        return features;
    }


    @Override
    public EfpAssignedValue getAssignedValue(final Feature feature, final EFP efp, final LR lr) {
        if (logger.isDebugEnabled()) {
            logger.debug("Finding EFP assigned value in feature "
                    + feature.getIdentifier() + " with EFP " + efp + " and LR " + lr);
        }

        List<EfpAssignment> efpAssignsList =
            efpInMemoryDataLoader.loadData().getAssignments(feature);
        EfpAssignedValue efpAssignedVal = AssignmentsTools.findValue(efpAssignsList, efp, lr);

        if (logger.isDebugEnabled()) {
            logger.debug("Found efp assigned value:" + efpAssignedVal);
        }
        return efpAssignedVal;
    }


    @Override
    public Set<LR> getLRs() {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting set of LRs which are used in EFP assignments of all features.");
        }

        Set<LR> result = new HashSet<LR>();
        for (Feature feature : efpInMemoryDataLoader.loadData().getFeatures()) {
            for (EfpAssignment efpAssignment : efpInMemoryDataLoader
                    .loadData().getAssignments(feature)) {

                if (efpAssignment.getEfpValue() == null) {
                    continue;
                }

                LR lr = efpAssignment.getEfpValue().getLR();
                if (lr != null) {
                    result.add(lr);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Set with found LRs: " + result);
        }

        return result;
    }


    @Override
    public void assignEFP(final Feature feature, final EFP newEfp,
            final EfpAssignedValue efpAssignedValue) {

        if (logger.isDebugEnabled()) {
            logger.debug("Creating new EFP assignment to feature " + feature.getIdentifier()
                    + " with EFP " + newEfp + " and assigned value " + efpAssignedValue);
        }

        // test if isn't exist EFP assignment with empty value and with EFP
        List<EfpAssignment> efpAssignments =
            efpInMemoryDataLoader.loadData().getAssignments(feature);

        for (EfpAssignment efpAssignment : efpAssignments) {
            if (efpAssignment.getEfpValue() == null && efpAssignment.getEfp().equals(newEfp)) {
                throw new AssignmentRTException("You can't adds EFP assignment with EFP "
                        + newEfp.getName() + " to feature " + feature.getIdentifier()
                        + " if it contains empty EFP assignment with this EFP.");
            }

        }

        // test if isn't exist similar EFP assignment
        if (efpAssignedValue != null
                && (efpAssignedValue.getAssignmentType()
                    == EfpAssignedValue.AssignmentType.direct
                || efpAssignedValue.getAssignmentType()
                        == EfpAssignedValue.AssignmentType.formula)) {

            if (AssignmentsTools.findValue(
                    efpInMemoryDataLoader.loadData().getAssignments(feature),
                    newEfp, null) != null) {

                throw new AssignmentRTException("Feature already contains "
                        + "EFP assignment with EFP '" + newEfp.getName()
                        + "' and EFP direct value or math.formula "
                        + "value or EFP named value from same LR.");
            }
        }

        //create new EFP assignment
        EfpAssignment efpAssignment = new EfpAssignment(newEfp, efpAssignedValue, feature);
        if (logger.isDebugEnabled()) {
            logger.debug("The EFP assignment was created: " + efpAssignment);
        }

        efpInMemoryDataLoader.loadData().getAssignments(feature).add(efpAssignment);
        addEfpAssignmentIntoFiles(efpAssignment);

        if (logger.isDebugEnabled()) {
            logger.debug("The EFP assignment was written into");
        }
    }


    @Override
    public void changeEfpValue(final Feature feature, final EFP efp,
            final EfpAssignedValue oldEfpAssignedValue,
            final EfpAssignedValue newEfpAssignedValue) {

        if (logger.isDebugEnabled()) {
            logger.debug("Changing value of a EFP assignment in feature "
                    + feature.getIdentifier() + " with EFP " + efp + ". OLD value: "
                    +  oldEfpAssignedValue + ", NEW value: " + newEfpAssignedValue);
        }

        List<EfpAssignment> efpAssignsList =
            efpInMemoryDataLoader.loadData().getAssignments(feature);
        EfpAssignment oldEfpAssignment = null;

        for (int index = 0; index < efpAssignsList.size(); index++) {
            if (efpAssignsList.get(index).getEfpValue().equals(oldEfpAssignedValue)) {
                oldEfpAssignment = efpAssignsList.remove(index);
                break;
            }
        }

        if (oldEfpAssignment == null) {
            logger.error("Editing EFP assignments doesn't exist in the component.");
            throw new AssignmentRTException("Editing EFP assignments "
                    + "doesn't exist in the component.");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("The EFP assignment with old value was found: " + oldEfpAssignment);
        }

        EfpAssignment newEfpAssign = new EfpAssignment(efp, newEfpAssignedValue, feature);
        efpAssignsList.add(newEfpAssign);

        addEfpAssignmentIntoFiles(newEfpAssign);
        deleteEfpAssignmentFromFiles(oldEfpAssignment);

        if (logger.isDebugEnabled()) {
            logger.debug("The EFP assignment with old value was updated.");
        }
    }


    @Override
    public void removeEFP(final Feature feature, final EFP efp,
            final EfpAssignedValue efpAssignedValue) {
        if (logger.isDebugEnabled()) {
            logger.debug("Removing EFP assignment from feature " + feature
                    + " with EFP " + efp + " and value " + efpAssignedValue);
        }

        List<EfpAssignment> efpAssignsList =
            efpInMemoryDataLoader.loadData().getAssignments(feature);

        EfpAssignment removedEfpAssign = null;

        for (int index = 0; index < efpAssignsList.size(); index++) {
            if (efpAssignedValue == null && efpAssignsList.get(index).getEfpValue() == null
                    && efp.equals(efpAssignsList.get(index).getEfp())) {
                removedEfpAssign = efpAssignsList.remove(index);
                break;
            }

            if (efp.equals(efpAssignsList.get(index).getEfp())
                    && efpAssignsList.get(index).getEfpValue().equals(efpAssignedValue)) {
                removedEfpAssign = efpAssignsList.remove(index);
                break;
            }
        }

        if (removedEfpAssign == null) {
            logger.error("Removing EFP assignment doesn't exist in the component.");
            throw new AssignmentRTException("Removing EFP assignments "
                    + "doesn't exist in the component.");
        }

        deleteEfpAssignmentFromFiles(removedEfpAssign);


        if (logger.isDebugEnabled()) {
            logger.debug("The EFP assignment was removed.");
        }
    }




    @Override
    public void close() {
        efpDataLocation.close();
        logger.info("Component was closed.");
    }




    @Override
    public void clear() {
        List<Feature> features = getAllFeatures();

        for (Feature feature : features) {
            List<EfpAssignment> efpAssignments =
                    efpInMemoryDataLoader.loadData().getAssignments(feature);

            while (!efpAssignments.isEmpty()) {
                EfpAssignment efpAssignment = efpAssignments.get(0);
                removeEFP(feature, efpAssignment.getEfp(), efpAssignment.getEfpValue());
            }
        }
    }


    /**
     * Writes into repository file and EFPs file data of new EFP assignment.
     * @param efpAssignment New EFP assignment
     */
    private void addEfpAssignmentIntoFiles(final EfpAssignment efpAssignment) {

        Integer id = mirroredDataManipulator.addEfpAssignment(efpAssignment);
        EfpLink efpAssignData = convertToEfpLink(efpAssignment, id);

        List<EfpLink> efpAssignsData = efpDataManipulator.readEFPs(efpAssignment.getFeature());
        efpAssignsData.add(efpAssignData);
        efpDataManipulator.assignEFPs(efpAssignment.getFeature(), efpAssignsData);

        if (logger.isDebugEnabled()) {
            logger.debug("New object EfpData with EFP assignment was created "
                    + "and added into EFP data file: " + efpAssignData);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Adding of new EFP assignment " + efpAssignment
                    + " into component's files is complete.'");
        }
    }


    /**
     * Deletes a EFP assignment from repository file and EFPs file.
     * @param efpAssignment Deleted EFP assignment
     */
    private void deleteEfpAssignmentFromFiles(final EfpAssignment efpAssignment) {

        if (logger.isDebugEnabled()) {
            logger.debug("Deleting of not used parts of EFP assignment: " + efpAssignment);
        }

        Feature feature = efpAssignment.getFeature();
        EFP efp = efpAssignment.getEfp();
        EfpAssignedValue efpAssignedValue = efpAssignment.getEfpValue();


        //deleting EfpData object with the EFP assignment from EFP-file
        List<EfpLink> efpDataList = efpDataManipulator.readEFPs(feature);

        EfpLink efpDataToDelete = findEfpDataOfEfpAssignment(efpAssignment, efpDataList);
        if (efpDataToDelete == null) {
            logger.error("Can't find required EfpData object with EFP assignment "
                    + efpAssignment + " in the list: " + efpDataList);
            throw new AssignmentRTException(
                    "Can't find required EfpData object with EFP assignment in the list");
        }

        efpDataList.remove(efpDataToDelete);
        if (logger.isDebugEnabled()) {
            logger.debug("From list of EfpData objects was removed EfpData "
                    + "object with deleted EFP assignment: " + efpDataToDelete);
        }

        //delete value
        if (efpAssignedValue != null) {
            //delete EFP named value
            if (efpAssignedValue.getAssignmentType() == EfpAssignedValue.AssignmentType.named) {
                EfpNamedValue namedValue = (EfpNamedValue) efpAssignedValue;
                LrAssignment[] particLrAssigns = namedValue.getLrParticipatingAssignments();

                //delete LR assignment in EFP named value
                if (!containsValueFromLR(namedValue.getLrAssignment())) {
                    mirroredDataManipulator.deleteLRAssignment(namedValue.getLrAssignment());
                    if (logger.isDebugEnabled()) {
                        logger.debug("From a local repository was deleted LR assignment: "
                                + namedValue.getLrAssignment());
                    }
                }
                //delete LR assignments in array with participating LR assignments
                if (particLrAssigns != null) {
                    for (LrAssignment lrAssignment : particLrAssigns) {
                        if (!containsValueFromLR(lrAssignment)) {
                            mirroredDataManipulator.deleteLRAssignment(lrAssignment);

                            if (logger.isDebugEnabled()) {
                                logger.debug("From a local repository was deleted participating "
                                        + "LR assignment: " + namedValue.getLrAssignment());
                            }
                        }
                    }
                }

                //delete EFP direct value or EFP math.formula value
            } else if (efpAssignedValue.getAssignmentType()
                        ==  EfpAssignedValue.AssignmentType.direct
                    || efpAssignedValue.getAssignmentType()
                        == EfpAssignedValue.AssignmentType.formula) {
                int id = efpDataToDelete.getParamObjectId();
                mirroredDataManipulator.deleteParamObject(id);

                if (logger.isDebugEnabled()) {
                    logger.debug("The EFP direct value or math.formula of feature '"
                            + feature.getIdentifier()
                            + "' and EFP '" + efp.getName() + "' with ID " + id
                            + " was deleted from a repository.");
                }

            } else {
                throw new RuntimeException("Unknown type of EFP assigned value.");
            }
        }


        //update files
        deleteEFPFromRepository(efp);
        efpDataManipulator.assignEFPs(efpAssignment.getFeature(), efpDataList);

        if (logger.isDebugEnabled()) {
            logger.debug("Feature " + efpAssignment.getFeature().getIdentifier()
                    + " in EFP-file was updated with new List of EfpData: " + efpDataList);
        }
    }


    /**
     * Deletes from repository not used EFP.
     * @param efp A deleting EFP
     */
    private void deleteEFPFromRepository(final EFP efp) {
        if (logger.isDebugEnabled()) {
            logger.debug("Deleting not used EFP " + efp);
        }

        for (Feature featureInMap : efpInMemoryDataLoader.loadData().getFeatures()) {
            if (AssignmentsTools.containEFP(getEfps(featureInMap), efp)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("EFP " + efp + " is still used in other EFP assignments.");
                }
                return;
            }
        }

        if (efp.getType() == EFP.EfpType.DERIVED) {
            List<EFP> efps = ((DerivedEFP) efp).getEfps();

            for (EFP efpInList : efps) {
                deleteEFPFromRepository(efpInList);
            }
        }

        mirroredDataManipulator.deleteEFP(efp);
        if (logger.isDebugEnabled()) {
            logger.debug("The EFP '" + efp + "' was deleted from a repository.");
        }
    }



    /**
     * Checks if component has a EFP assignment which contains concrete LR assignment.
     * @param targetLrAssign Searched LR assignment.
     * @return True if contains otherwise false.
     */
    private boolean containsValueFromLR(final LrAssignment targetLrAssign) {
        if (logger.isDebugEnabled()) {
            logger.debug("Test if is used in component LR assignment: " + targetLrAssign);
        }

        EfpAssignedValue assignedValue;
        boolean isUsed = false;

        for (Feature feature : efpInMemoryDataLoader.loadData().getFeatures()) {
            assignedValue = getAssignedValue(
                    feature, targetLrAssign.getEfp(), targetLrAssign.getLr());

            //if assignment with EFP named value exist
            if (assignedValue != null && assignedValue.getAssignmentType()
                    == EfpAssignedValue.AssignmentType.named) {
                LrAssignment lrAssign = ((EfpNamedValue) assignedValue).getLrAssignment();

                if (lrAssign.getLr().equals(targetLrAssign.getLr())
                        && lrAssign.getValueName().equals(targetLrAssign.getValueName())) {
                    isUsed = true;
                }

                //if LR assignment in assigned value is derived can contains next
                //participating LR assignments
                LrAssignment[] partLRAssigns =
                    ((EfpNamedValue) assignedValue).getLrParticipatingAssignments();

                if (partLRAssigns != null && partLRAssigns.length != 0) {
                    for (LrAssignment lrAssignment : partLRAssigns) {
                        if (containsValueFromLR(lrAssignment)) {
                            isUsed = true;
                            break;
                        }
                    }
                }

                if (isUsed) {
                    break;
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("A result of the test LR assignment " + targetLrAssign + ": " + isUsed);
        }

        return isUsed;
    }



    /**
     * Finds  in list concrete EfpData object, which contains required EFP assignment.
     * @param efpAssignment A EFP assignment
     * @param efpDataList List with EfpData objects
     * @return Target EfpData object from the list
     */
    private EfpLink findEfpDataOfEfpAssignment(
            final EfpAssignment efpAssignment, final List<EfpLink> efpDataList) {

        EFP efp = efpAssignment.getEfp();

        for (EfpLink efpData : efpDataList) {
            //if both doesn't contain same EFP
            if (!efp.getName().equals(efpData.getEfpName())) {
                continue;
            }

            if (efpData.getGrIdStr() != null) {
                if (!efpData.getGrIdStr().equals(efp.getGr().getIdStr())) {
                    continue;
                }
            } else if (!efpData.getGrId().equals(efp.getGr().getId())) {
                continue;
            }

            //if both contain information about same EFP assignment without value
            if (efpAssignment.getEfpValue() == null && efpData.getTypeOfAssignedValue() == null) {
                return efpData;
            }

            LR lr = efpAssignment.getEfpValue().getLR();

            //if both contain information about same EFP assignment
            //with direct or math.formula value
            if (lr == null && (efpData.getTypeOfAssignedValue()
                    == EfpAssignedValue.AssignmentType.direct || efpData.getTypeOfAssignedValue()
                    == EfpAssignedValue.AssignmentType.formula)) {
                return efpData;
            }

            //if both contain information about same EFP assignment with EFP named value
            if (lr != null && efpAssignment.getEfpValue().getAssignmentType()
                    == EfpAssignedValue.AssignmentType.named) {
                if (efpData.getLrIdStr() != null) {
                    if (!efpData.getLrIdStr().equals(lr.getIdStr())) {
                        continue;
                    }
                } else if (!lr.getId().equals(efpData.getLrId())) {
                    continue;
                }

                String valueName = ((EfpNamedValue)
                         efpAssignment.getEfpValue()).getLrAssignment().getValueName();

                if (valueName.equals(efpData.getValueName())) {
                    return efpData;
                }
            }
        }

        return null;
    }

    /**
     * Converts instance into into Efp link object.
     * @param efpAssignment Assigned Efp value
     * @param idOfValueInRepository Contains ID in repository for direct value or formula.
     * @return Converted Efp assignment
     */
    public final EfpLink convertToEfpLink(final EfpAssignment efpAssignment,
                                          final Integer idOfValueInRepository) {
        EfpAssignedValue efpValue = efpAssignment.getEfpValue();
        EFP efp = efpAssignment.getEfp();

        if (efpValue != null) {
            EfpValueType efpValueType = efpAssignment.getEfpValue().computeValue();

            if (efpValue.getAssignmentType() == EfpAssignedValue.AssignmentType.named) {
                LrAssignment lrAssignment = ((EfpNamedValue) efpValue).getLrAssignment();

                return new EfpLink(efp.getGr().getId(), efp.getGr().getIdStr(), efp.getName(),
                        lrAssignment.getLr().getId(), lrAssignment.getLr().getIdStr(),
                        lrAssignment.getValueName(), efpValueType);

            } else if (efpValue.getAssignmentType() == EfpAssignedValue.AssignmentType.direct
                    || efpValue.getAssignmentType() == EfpAssignedValue.AssignmentType.formula) {

                return new EfpLink(efp.getGr().getId(), efp.getGr().getIdStr(), efp.getName(),
                        efpValue.getAssignmentType(), efpValueType, idOfValueInRepository);

            } else {
                throw new AssignmentRTException("Unknown type of assigned value.");
            }

        } else {
            return new EfpLink(efp.getGr().getId(), efp.getGr().getIdStr(), efp.getName());
        }
    }
}
