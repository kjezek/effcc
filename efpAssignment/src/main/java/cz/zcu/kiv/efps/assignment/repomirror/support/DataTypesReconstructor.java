package cz.zcu.kiv.efps.assignment.repomirror.support;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.repository.generated.DerivedEFPElement;
import cz.zcu.kiv.efps.assignment.repository.generated.GammaElement;
import cz.zcu.kiv.efps.assignment.repository.generated.GammaTypeElement;
import cz.zcu.kiv.efps.assignment.repository.generated.LRElement;
import cz.zcu.kiv.efps.assignment.repository.generated.LrDerivedAssignmentElement;
import cz.zcu.kiv.efps.assignment.repository.generated.LrSimpleAssignmentElement;
import cz.zcu.kiv.efps.assignment.repository.generated.MetaElement;
import cz.zcu.kiv.efps.assignment.repository.generated.ReferenceToEFPElement;
import cz.zcu.kiv.efps.assignment.repository.generated.RepositoryElement;
import cz.zcu.kiv.efps.assignment.repository.generated.SimpleEFPElement;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.BooleanGamma;
import cz.zcu.kiv.efps.types.properties.comparing.ComplexTypeGamma;
import cz.zcu.kiv.efps.types.properties.comparing.EnumGamma;
import cz.zcu.kiv.efps.types.properties.comparing.NumberGamma;
import cz.zcu.kiv.efps.types.properties.comparing.NumberIntervalGamma;
import cz.zcu.kiv.efps.types.properties.comparing.StringGamma;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * It provides methods for reconstruction of objects efpTypes from repository elements.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class DataTypesReconstructor {


    /**
     * Reconstructs a GR object.
     * @param repElem RepositoryElement
     * @return New GR object
     */
    public GR reconstructGR(final RepositoryElement repElem) {
        return new GR(repElem.getGr().getId().intValue(),
                repElem.getGr().getName(), repElem.getGr().getIdStr());
    }


    /**
     * Reconstructs a LR object.
     * @param lrElem LRElement
     * @param gr GR which contains LR.
     * @return New LR object
     */
    public LR reconstructLR(final LRElement lrElem, final GR gr) {
        return new LR(lrElem.getId().intValue(), lrElem.getName(), gr, null, lrElem.getIdStr());
    }


    /**
     * Reconstructs a Meta object.
     * @param metaElem MetaElement
     * @return New Meta object
     */
    public Meta reconstructMeta(final MetaElement metaElem) {
        List<String> namesList = metaElem.getNames().getName();
        return new Meta(metaElem.getUnit(), namesList.toArray(new String[0]));
    }


    /**
     * Reconstructs a Gamma object.
     * @param gammaElem GammaElement
     * @return New Gamma object
     */
    public Gamma reconstructGamma(final GammaElement gammaElem) {
        if (gammaElem.getType() == GammaTypeElement.BOOLEAN_GAMMA) {
            return new BooleanGamma();
        } else if (gammaElem.getType() == GammaTypeElement.COMPLEX_TYPE_GAMMA) {
            return new ComplexTypeGamma();
        } else if (gammaElem.getType() == GammaTypeElement.ENUM_GAMMA) {
            return new EnumGamma();
        } else if (gammaElem.getType() == GammaTypeElement.NUMBER_GAMMA) {
            return new NumberGamma();
        } else if (gammaElem.getType() == GammaTypeElement.NUMBER_INTERVAL_GAMMA) {
            return new NumberIntervalGamma();
        } else if (gammaElem.getType() == GammaTypeElement.STRING_GAMMA) {
            return new StringGamma();
        } else if (gammaElem.getType() == GammaTypeElement.USER_GAMMA) {
            return new UserGamma(gammaElem.getFunction());
        }

        return null;
    }


    /**
     * Reconstruct EFP from data in repository. If EFP is derived type,
     * recursively reconstruct all required EFP.
     * @param repository Reference to RepositoryElement which is needed
     * for searching required efps if is reconstructs DerivedEFP.
     * @param gr Reference to GR
     * @param efpFromRep Reference to SimpleEFPElement or DerivedEFPElement
     * @return New EFP.
     */
    public EFP reconstructEFP(final RepositoryElement repository, final GR gr,
             final Object efpFromRep)  {

        if (efpFromRep.getClass() == SimpleEFPElement.class) {
            SimpleEFPElement simEfpElem = (SimpleEFPElement) efpFromRep;

            Class<?> valueType = ElementsGetter.getEfpValueType(simEfpElem.getValueType());

            if (valueType == null) {
                throw new AssignmentRTException(
                        "Error: Unknown value type in simple EFP " + simEfpElem.getName());
            }

            return new SimpleEFP(simEfpElem.getId().intValue(),
                            simEfpElem.getName(),
                            valueType,
                            reconstructMeta(simEfpElem.getMeta()),
                            reconstructGamma(simEfpElem.getGamma()),
                            gr);
        } else if (efpFromRep.getClass() == DerivedEFPElement.class) {

            if (repository == null) {
                new AssignmentRTException("For reconstruct DerivedEFP "
                        + "is required RepositoryElement with GR.");
            }

            DerivedEFPElement derEfpElem = (DerivedEFPElement) efpFromRep;
            List<ReferenceToEFPElement> efpRefs = derEfpElem.getEfps().getProperty();

            List<EFP> requiredEFPs = new ArrayList<EFP>();

            for (ReferenceToEFPElement refToEFPElem : efpRefs) {
                requiredEFPs.add(
                    reconstructEFP(repository, gr,
                            ElementsGetter.getSimpleOrDerivedEFPElement(
                                repository, refToEFPElem.getId().intValue())));
            }

            Class<?> valueType = ElementsGetter.getEfpValueType(derEfpElem.getValueType());

            if (valueType == null) {
                throw new AssignmentRTException(
                        "Error: Unknown value type in derived EFP " + derEfpElem.getName());
            }

            return new DerivedEFP(derEfpElem.getId().intValue(),
                    derEfpElem.getName(),
                    valueType,
                    reconstructMeta(derEfpElem.getMeta()),
                    reconstructGamma(derEfpElem.getGamma()),
                    gr,
                    (EFP[]) requiredEFPs.toArray(new EFP[0]));

        } else {
            throw new AssignmentRTException("Parametr efpFromRep doesn't "
                    + "contain object of type SimpleEFPElement or DerivedEFPElement");
        }
    }

    /**
     * Reconstructs LrSimpleAssignment object.
     * @param assignment LrSimpleAssignmentElement
     * @param efp Reference to EFP which belongs this assignment
     * @param lr Reference to LR which contains this assignment
     * @return New LrSimpleAssignment object
     */
    @SuppressWarnings("unchecked")
    public LrSimpleAssignment reconstructLrSimpleAssignment(
            final LrSimpleAssignmentElement assignment, final EFP efp, final LR lr) {

        EfpValueType efpValueType =
            (EfpValueType) EfpSerialiser.deserialize(
                    (Class<EfpValueType>) efp.getValueType(), assignment.getEfpValue());

        return new LrSimpleAssignment(assignment.getId().intValue(), efp,
                    assignment.getValueName(), efpValueType, lr);
    }

    /**
     * Reconstructs LrDerivedAssignment object.
     * @param assignment LrDerivedAssignmentElement
     * @param efp Reference to EFP which belongs this assignment
     * @param lr Reference to LR which contains this assignment
     * @return New LrDerivedAssignment object
     */
    @SuppressWarnings("unchecked")
    public LrDerivedAssignment reconstructLrDerivedAssignment(
            final LrDerivedAssignmentElement assignment, final EFP efp, final LR lr) {

        EfpValueType efpValueType =
            (EfpValueType) EfpSerialiser.deserialize(
                    (Class<EfpValueType>) efp.getValueType(), assignment.getEfpValue());

        // TODO [kjezek, A] I deal only with a logical formula here.
        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                assignment.getFormula(),
                efpValueType,
                ((DerivedEFP) efp).getEfps());

        return new LrDerivedAssignment(assignment.getId().intValue(), efp,
                    assignment.getValueName(), eval, lr);
    }
}
