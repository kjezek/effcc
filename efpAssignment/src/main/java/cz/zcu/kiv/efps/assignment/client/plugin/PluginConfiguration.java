/**
 *
 */
package cz.zcu.kiv.efps.assignment.client.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunctionFactory;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.repomirror.api.MirroredDataManipulator;
import org.apache.commons.io.IOUtils;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.behaviors.Caching;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.core.ComponentEFPModifierImpl;

/**
 * This class is used to configure a plugin.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class PluginConfiguration implements PluginAPIWrapper {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());


    /** EFP mirror implementing class.  */
    private Class<MirroredDataManipulator> efpMirror;

    /** EFP data loader implementing class. */
    private Class<EfpDataManipulator> efpData;

    /** EFP data location implementing class. */
    private Class<EfpDataLocation> efpLocation;

    /** Matching function. */
    private Class<MatchingFunction> matchingFunction;

    /** No instance. */
    private PluginConfiguration() { }

    /**
     * This method configure a plugin according to a configuration file.
     * @param configuration the name of the configuration file.
     * @return a configured information of the plugin
     */
    public static PluginAPIWrapper configure(final String configuration) {

        PluginConfiguration conf = new PluginConfiguration();
        conf.loadConfiguration(configuration);

        return conf;
    }

    /**
     * This method loads a configuration of the plugin from a configuration file.
     * @param configuration the name of a configuration file
     */
    @SuppressWarnings("unchecked")
    private void loadConfiguration(final String configuration) {
        logger.info("Configuring EFP Assignment component dependent module");

        InputStream  stream =  getClass().getResourceAsStream(configuration);
        Properties prop = new Properties();
        try {

            logger.debug("loading configuration from the file: " + configuration);

            prop.load(stream);
        } catch (IOException e) {
            throw new AssignmentRTException("Canot read a plugin configuration file: "
                    + configuration, e);
        } finally {
            IOUtils.closeQuietly(stream);
        }

        try {
            efpMirror = (Class<MirroredDataManipulator>) Class.forName(
                    prop.getProperty("plugin.efp.mirror"));

            efpData = (Class<EfpDataManipulator>) Class.forName(
                    prop.getProperty("plugin.efp.dependent"));

            efpLocation = (Class<EfpDataLocation>) Class.forName(
                    prop.getProperty("plugin.efp.location"));

            matchingFunction = (Class<MatchingFunction>) Class.forName(
                    prop.getProperty("plugin.efp.matching.function"));

        } catch (ClassNotFoundException e) {
            throw new AssignmentRTException("Cannot load class for plugin", e);
        }

        logger.info("Configuration loaded.");
    }

    @Override
    public ComponentEFPModifier createComponentModifier(
            final String component) {

        MutablePicoContainer pico = new DefaultPicoContainer(new Caching());

       pico.addComponent(component);
       pico.addComponent(efpMirror);
       pico.addComponent(efpData);
       pico.addComponent(efpLocation);
       pico.addComponent(ComponentEFPModifierImpl.class);

       ComponentEFPModifier result = pico.getComponent(
               ComponentEFPModifier.class);

       logger.info("Configuration succesfull");

       return result;
    }

    @Override
    public MatchingFunction createMatchingFunction(
            final List<String> components) {

        MutablePicoContainer pico = new DefaultPicoContainer(new Caching());

        pico.addComponent(matchingFunction);

        return pico.getComponent(MatchingFunctionFactory.class)
                .createMatchingFunction(components);
    }
}
