package cz.zcu.kiv.efps.assignment.extension.api;

import java.util.List;

import cz.zcu.kiv.efps.assignment.types.Feature;

/**
 * Interface for manipulation of storage which contains features
 * and their EFP assignments (e.g. manifest).
 *
 * Date: 22.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface EfpDataManipulator {

    /**
     * This method loads all features of a component.
     * @return a list of features
     */
    List<Feature> readFeatures();


    /**
     * This method loads all links between the component and a common EFP mirror
     * for a particular feature.
     * @param feature a feature
     * @return a list of all objects with information of EFP assignments.
     */
    List<EfpLink> readEFPs(Feature feature);


    /**
     * This method assigns a list of EFP link to a feature on a particular component.
     * @param feature a feature
     * @param efpData a list with objects with information of EFP assignments.
     */
    void assignEFPs(Feature feature, List<EfpLink> efpData);
}
