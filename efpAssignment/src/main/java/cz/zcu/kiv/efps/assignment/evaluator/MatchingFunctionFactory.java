package cz.zcu.kiv.efps.assignment.evaluator;

import java.util.List;

/**
 * A factory to create {@link MatchingFunction}.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface MatchingFunctionFactory {


    /**
     * This method creates a matching function for the given
     * components set.
     *
     * @param components the components to create
     *                   the matching function for
     *
     * @return the matching function.
     */
    MatchingFunction createMatchingFunction(List<String> components);
}
