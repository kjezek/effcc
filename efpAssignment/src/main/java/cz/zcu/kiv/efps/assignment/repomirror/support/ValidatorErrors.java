package cz.zcu.kiv.efps.assignment.repomirror.support;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * ValidatorErrors for JAXB.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ValidatorErrors implements ErrorHandler {

    /**
     * Generates exception.
     * @param level Level
     * @param e Exception
     * @throws SAXException Error information from XML parser
     */
    public void generate(final String level, final SAXException e) throws SAXException {
        throw new SAXException(level + ": " + e.toString());
    }

    @Override
    public void error(final SAXParseException exception) throws SAXException {
        generate("Error", exception);

    }

    @Override
    public void fatalError(final SAXParseException exception) throws SAXException {
        generate("Fatal Error", exception);

    }

    @Override
    public void warning(final SAXParseException exception) throws SAXException {
        generate("Warning", exception);

    }


}
