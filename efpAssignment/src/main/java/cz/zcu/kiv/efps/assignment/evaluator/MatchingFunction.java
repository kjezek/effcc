/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;

/**
 * This interface represents a matching function
 * applicable to two Features determining
 * whether they match. The matching of two features
 * means that the features may be connected in
 * a component binding.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface MatchingFunction {


    /**
     * This function return true if two features match.
     * It means that these two features are compatible
     * in component binding process.
     * @param feature1 first feature
     * @param feature2 second feature
     * @return true if the features match
     */
    boolean matches(Feature feature1, Feature feature2);

    /**
     * A default matching function simply calling {@link BasicFeature#matchesTo(Feature)}
     * method.
     */
    MatchingFunction DEFAULT_MATCHING_FUNCTION
        = new MatchingFunction() {
        @Override
        public boolean matches(final Feature feature1, final Feature feature2) {

            BasicFeature f1 = (BasicFeature) feature1;
            BasicFeature f2 = (BasicFeature) feature2;

            return f1.matchesTo(f2);
        }
    };
}
