package cz.zcu.kiv.efps.assignment.extension.manifest;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Contains all parameters of the feature in manifest.
 *
 * Date: 13.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class FeatureParameters {

    /** Name of feature. */
    private String featureName;
    /** Map with all attributes of the feature in manifest.
     * KEY is parameter name, VALUE is value of the parameter.*/
    private Map<String, String> featureAttributes;
    /** Map with all directives of the feature in manifest.
     * KEY is parameter name, VALUE is value of the parameter.*/
    private Map<String, String> featureDirectives;



    /**
     * Default constructor.
     */
    public FeatureParameters() {
        featureAttributes = new LinkedHashMap<String, String>();
        featureDirectives = new LinkedHashMap<String, String>();
    }

    /**
     * Constructor with a name of feature.
     * @param featureName The name of feature
     */
    public FeatureParameters(String featureName) {
        this.featureName = featureName;
        featureAttributes = new LinkedHashMap<String, String>();
        featureDirectives = new LinkedHashMap<String, String>();
    }

    /**
     * Copy constructor.
     * @param featureParams Feature parameters.
     * @param featureName Name of feature
     */
    public FeatureParameters(final FeatureParameters featureParams, final String featureName) {
        this.featureName = featureName.trim();
        featureAttributes = new LinkedHashMap<String, String>();
        featureDirectives = new LinkedHashMap<String, String>();

        for (String paramName : featureParams.featureAttributes.keySet()) {
            featureAttributes.put(paramName, featureParams.featureAttributes.get(paramName));
        }

        for (String paramName : featureParams.featureDirectives.keySet()) {
            featureDirectives.put(paramName, featureParams.featureDirectives.get(paramName));
        }
    }


    /**
     * @return the featureAttributes
     */
    public Map<String, String> getFeatureAttributes() {
        return featureAttributes;
    }


    /**
     * @return the featureDirectives
     */
    public Map<String, String> getFeatureDirectives() {
        return featureDirectives;
    }


    /**
     * @return the feature's name
     */
    public String getFeatureName() {
        return featureName;
    }


    /**
     * Converts parameter of feature into one string.
     * @return A string with parameters.
     */
    public String parametersToString() {
        String parameters = "";

        boolean isFirst = true;
        for (String paramName : featureAttributes.keySet()) {
            if (!isFirst) {
                parameters += Constants.SEPARATOR_PARAMS;
            } else {
                isFirst = false;
            }

            parameters += paramName + Constants.EQUAL + featureAttributes.get(paramName);
        }

        for (String paramName : featureDirectives.keySet()) {
            if (!isFirst) {
                parameters += Constants.SEPARATOR_PARAMS;
            } else {
                isFirst = false;
            }

            parameters += paramName + Constants.ASSIGNMENT_DIRECTIVES
                            + featureDirectives.get(paramName);
        }

        return parameters;
    }
}
