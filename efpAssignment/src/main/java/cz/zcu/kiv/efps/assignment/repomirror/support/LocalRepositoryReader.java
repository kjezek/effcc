package cz.zcu.kiv.efps.assignment.repomirror.support;

import javax.xml.bind.JAXBElement;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.repository.generated.*;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;

import java.util.ArrayList;
import java.util.List;

/**
 * Opens a saves local repository of concrete component and provides data to concrete component.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class LocalRepositoryReader {
    /** Root of repository. */
    private RepositoriesElement repositoriesElement;

    /** Reference to object of class DataTypesReconstructor. */
    private DataTypesReconstructor dtReconstructor;


    /**
     * Default constructor for setting basic objects and loading data from file.
     * @param localRepository Reference to root element of XML
     */
    public LocalRepositoryReader(final JAXBElement<?> localRepository) {
        if (localRepository == null
                || !(localRepository.getValue() instanceof RepositoriesElement)) {
            throw new AssignmentRTException("Parameter doesn't "
                    + "contain element with root of repository.");
        }


        dtReconstructor = new DataTypesReconstructor();
        repositoriesElement = (RepositoriesElement) localRepository.getValue();
    }


    /**
     * Gets from actual local repository LrAssignment by EFP, ID of LR and name of value.
     * @param efp Reference to EFP for which was created required LrAssignment.
     * @param lrId ID of LR
     * @param valueName Name of value
     * @return LrDerivedAssignment or LrSimpleAssignment
     */
    public LrAssignment readLrAssignment(final EFP efp, final int lrId, final String valueName) {

        // get repositoryElement with required GR and LRs
        RepositoryElement repositoryElem = ElementsGetter.getRepositoryElement(
                repositoriesElement, efp.getGr().getId());

        if (repositoryElem == null) {
            throw new AssignmentRTException(
                    "Repository with required global register doesn't exist.");
        }

        LRElement lrElem = ElementsGetter.getLRElement(repositoryElem, lrId);
        if (lrElem == null) {
            throw new AssignmentRTException(
                    "Required local register with id " + lrId + " is missing.");
        }
        LR lr = dtReconstructor.reconstructLR(lrElem, efp.getGr());


        if (efp.getType() == EFP.EfpType.SIMPLE) {

            LrSimpleAssignmentElement assignmentElem = (LrSimpleAssignmentElement)
                            ElementsGetter.getSimpleOrDerivedAssignmentElement(
                                lrElem, efp.getId(), valueName);

            if (assignmentElem == null) {
                throw new AssignmentRTException(
                   "No assignment with name " + valueName + " for EFP "
                       + efp + " is assigned on component.");
            }

            return dtReconstructor.reconstructLrSimpleAssignment(assignmentElem, efp, lr);

        } else if (efp.getType() == EfpType.DERIVED) {

               LrDerivedAssignmentElement assignmentElem = (LrDerivedAssignmentElement)
                ElementsGetter.getSimpleOrDerivedAssignmentElement(
                        lrElem, efp.getId(), valueName);

            if (assignmentElem == null) {
                throw new AssignmentRTException(
                   "Required object of the type LrSimpleAssignmentElement is missing.");
            }

            return dtReconstructor.reconstructLrDerivedAssignment(assignmentElem, efp, lr);

        } else {
            throw new RuntimeException("Unknown type of EFP.");
        }
    }

    /**
     * Gets from actual local repository LrAssignment by EFP, string ID of LR and name of value.
     * @param efp Reference to EFP for which was created required LrAssignment.
     * @param lrIdStr A string ID of LR
     * @param valueName Name of value
     * @return LrDerivedAssignment or LrSimpleAssignment
     */
    public LrAssignment readLrAssignment(
            final EFP efp, final String lrIdStr, final String valueName) {

        // get repositoryElement with required GR and LRs
        RepositoryElement repositoryElem = ElementsGetter.getRepositoryElement(
                repositoriesElement, efp.getGr().getId());

        if (repositoryElem == null) {
            throw new AssignmentRTException(
                    "Repository with required global register with ID " + efp.getGr().getId()
                            + " doesn't exist.");
        }

        LRElement lrElem = ElementsGetter.getLRElement(repositoryElem, lrIdStr);
        if (lrElem == null) {
            throw new AssignmentRTException(
                    "Required local register with string id " + lrIdStr + " is missing.");
        }
        LR lr = dtReconstructor.reconstructLR(lrElem, efp.getGr());


        if (efp.getType() == EFP.EfpType.SIMPLE) {

            LrSimpleAssignmentElement assignmentElem = (LrSimpleAssignmentElement)
                    ElementsGetter.getSimpleOrDerivedAssignmentElement(
                            lrElem, efp.getId(), valueName);

            if (assignmentElem == null) {
                throw new AssignmentRTException(
                        "No assignemtn with name " + valueName + " for EFP "
                                + efp + " is assignemd on component.");
            }

            return dtReconstructor.reconstructLrSimpleAssignment(assignmentElem, efp, lr);

        } else if (efp.getType() == EfpType.DERIVED) {

            LrDerivedAssignmentElement assignmentElem = (LrDerivedAssignmentElement)
                    ElementsGetter.getSimpleOrDerivedAssignmentElement(
                            lrElem, efp.getId(), valueName);

            if (assignmentElem == null) {
                throw new AssignmentRTException(
                        "Required object of the type LrSimpleAssignmentElement is missing.");
            }

            return dtReconstructor.reconstructLrDerivedAssignment(assignmentElem, efp, lr);

        } else {
            throw new RuntimeException("Unknown type of EFP.");
        }
    }


    /**
     * Gets EFP direct value or math.formula value in string from repository.
     * @param id ID of required other value
     * @return String with the direct value or math.formula value
     */
    public String readOtherValue(final int id) {

        ParameterValueElement otherValue = ElementsGetter.getParamValue(
                repositoriesElement.getParameters(), id);

        if (otherValue == null) {
            throw new AssignmentRTException(
                "Repository doesn't contain other value with specified ID " + id);
        }

        return otherValue.getValue();
    }


    /**
     * Gets from actual local repository a EFP by EFP name and ID of GR.
     * @param efpName Name of EFP
     * @param grId ID of GR.
     * @return SimpleEFP or DerivedEFP
     */
    public EFP readEFP(final String efpName, final int grId) {

        // get repositoryElement with required GR
        RepositoryElement repositoryElem = ElementsGetter.getRepositoryElement(
                repositoriesElement, grId);

        if (repositoryElem == null) {
            throw new AssignmentRTException(
                    "Repository with required global register with ID "
                            + grId + " doesn't exist.");
        }

        GR gr = dtReconstructor.reconstructGR(repositoryElem);
        Object efpElement = ElementsGetter.getSimpleOrDerivedEFPElement(repositoryElem, efpName);

        if (efpElement == null) {
            throw new AssignmentRTException(
                    "Required EFP element with EFP " + efpName + " in GR with ID "
                    + grId + " is missing.");
        }

        return dtReconstructor.reconstructEFP(repositoryElem, gr, efpElement);
    }

    /**
     * Gets from actual local repository a EFP by EFP name and string ID of GR.
     * @param efpName Name of EFP
     * @param grIdStr A string ID of GR
     * @return SimpleEFP or DerivedEFP
     */
    public EFP readEFP(final String efpName, final String grIdStr) {

        // get repositoryElement with required GR
        RepositoryElement repositoryElem = ElementsGetter.getRepositoryElement(
                repositoriesElement, grIdStr);

        if (repositoryElem == null) {
            throw new AssignmentRTException(
                    "Repository with required global register with string ID '"
                            + grIdStr + "' doesn't exist.");
        }

        GR gr = dtReconstructor.reconstructGR(repositoryElem);
        Object efpElement = ElementsGetter.getSimpleOrDerivedEFPElement(repositoryElem, efpName);

        if (efpElement == null) {
            throw new AssignmentRTException(
                    "Required EFP element with EFP " + efpName + " in GR with string ID "
                            + grIdStr + " is missing.");
        }

        return dtReconstructor.reconstructEFP(repositoryElem, gr, efpElement);
    }

    /**
     * Gets for concrete derived LR assignment list of particular LR assignments.
     * @param lrAssignment The derived LR assignment
     * @return List of particular LR assignments
     */
    public List<LrSimpleAssignment> readLrPartAssignments(final LrDerivedAssignment lrAssignment) {
        List<LrSimpleAssignment> lrPartAssignments = new ArrayList<LrSimpleAssignment>();

        RepositoryElement repository = ElementsGetter.getRepositoryElement(
                repositoriesElement, lrAssignment.getEfp().getGr().getId());

        if (repository == null) {
            throw new AssignmentRTException(
                    "Repository with required global register with ID '"
                            + lrAssignment.getEfp().getGr().getId() + "' doesn't exist.");
        }

        LRElement lrElement = ElementsGetter.getLRElement(repository,
                lrAssignment.getLr().getId());

        if (lrElement == null) {
            throw new AssignmentRTException("Required local register with ID "
                    +  lrAssignment.getLr().getId() + " is missing.");
        }

        Object lrAssignmentElem =
                ElementsGetter.getSimpleOrDerivedAssignmentElement(lrElement,
                        lrAssignment.getEfp().getId(), lrAssignment.getValueName());

        if (lrAssignmentElem == null) {
            throw new AssignmentRTException(
                    "Required object of the type LrDerivedAssignmentElement is missing.");
        }

        for (LrPartAssignmentIDElement lrPartAssignIDElem
                : ((LrDerivedAssignmentElement) lrAssignmentElem)
                    .getLrPartAssignIDs().getLrPartAssignment()) {

            EFP efp = readEFP(lrPartAssignIDElem.getEfp(), lrAssignment.getEfp().getGr().getId());

            LrSimpleAssignment lrPartAssign =  (LrSimpleAssignment) readLrAssignment(
                    efp, lrPartAssignIDElem.getLrID().intValue(),
                    lrPartAssignIDElem.getLrValueName());


            lrPartAssignments.add(lrPartAssign);
        }

        return lrPartAssignments;
    }
}
