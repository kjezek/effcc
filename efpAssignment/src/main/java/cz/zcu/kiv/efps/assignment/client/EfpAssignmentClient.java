/**
 *
 */
package cz.zcu.kiv.efps.assignment.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;


/**
 * This class allows to call methods from
 * the EFP assignment module.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpAssignmentClient {

    /** A logger. */
    private static Logger logger = LoggerFactory.getLogger(EfpAssignmentClient.class);


    /** This is private.  */
    private EfpAssignmentClient() { }


    /**
     * This method loads a class accessing
     * the EFP Assignment module.
     * @param componentLoaderName an implementation allowing to access EFPs on a component
     * @return the instance holding the accessing class
     */
    @SuppressWarnings("unchecked")
    public static EfpAwareComponentLoader initialiseComponentLoader(
            final String componentLoaderName) {

        try {
            return initialiseComponentLoader(
                    (Class<EfpAwareComponentLoader>) Class.forName(componentLoaderName));

        } catch (ClassNotFoundException e) {
            logger.error("Error creating EFP Assignment interface: " + e, e);

            throw new IllegalArgumentException(
                    "Impossible to load an implementation for " + componentLoaderName, e);
        }
    }

    /**
     * This method loads a class accessing
     * the EFP Assignment module.
     * @param componentLoaderClass an implementation allowing to modify EFPs on a component
     * @param <V> an interface allowing to modify EFPs on a component
     * @return the instance holding the accessing class
     */
    public static <V extends EfpAwareComponentLoader>
        EfpAwareComponentLoader initialiseComponentLoader(final Class<V> componentLoaderClass) {

        try {
            return componentLoaderClass.newInstance();
        } catch (Exception e) {
            logger.error("Error creating EFP Assignment interface: " + e, e);

            throw new IllegalArgumentException(
                    "Impossible to load an implementation for " + componentLoaderClass, e);
        }
    }

}
