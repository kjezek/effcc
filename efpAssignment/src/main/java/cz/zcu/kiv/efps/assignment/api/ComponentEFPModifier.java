package cz.zcu.kiv.efps.assignment.api;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This interface holds a set of definitions of methods providing
 * means to modify EFPs attached on components.
 * @author Jan Svab
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface ComponentEFPModifier extends ComponentEfpAccessor {

    /**
     * The implementation should add a new efpAssignment
     * of a value to a concrete feature in a component.
     * @param feature the feature which the value will be attached to
     * @param newEfp a new EFP which will be attached to the feature
     * @param efpAssignedValue a value being attached to the feature together with the EFP
     */
    void assignEFP(Feature feature, EFP newEfp, EfpAssignedValue efpAssignedValue);


    /**
     * The implementation should change a value an assignment in a concrete feature.
     * @param feature editing feature
     * @param efp a EFP of assigned values
     * @param oldEfpAssignedValue a old value of EFP
     * @param newEfpAssignedValue a new value of EFP that will be changed on the assignments.
     */
    void changeEfpValue(Feature feature, EFP efp,
            EfpAssignedValue oldEfpAssignedValue, EfpAssignedValue newEfpAssignedValue);


    /**
     * The implementation should remove an assignment from the concrete feature.
     * @param feature Feature which owns required assignment.
     * @param efp EFP which is owner of this assignment.
     * @param efpAssignedValue EFP assigned value for delete
     */
    void removeEFP(final Feature feature, final EFP efp,
            final EfpAssignedValue efpAssignedValue);


    /**
     * Clears from the component all EFP assignments.
     */
    void clear();
}
