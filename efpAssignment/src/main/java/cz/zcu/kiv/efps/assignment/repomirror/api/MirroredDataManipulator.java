package cz.zcu.kiv.efps.assignment.repomirror.api;

import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

import java.util.List;

/**
 * Interface for updating data in XML which contains local repository.
 *
 * Date: 21.2.2011
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface MirroredDataManipulator {


    /**
     * It reads LR assignment according to the input parameters.
     * @param efp A EFP
     * @param lrId ID of LR
     * @param valueName Name of value
     * @return Required LR assignment
     */
    LrAssignment readLrAssignment(final EFP efp, final int lrId, final String valueName);

    /**
     * It reads LR assignment according to the input parameters.
     * @param efp A EFP
     * @param lrIdStr A string ID of LR
     * @param valueName Name of value
     * @return Required LR assignment
     */
    LrAssignment readLrAssignment(final EFP efp, final String lrIdStr, final String valueName);

    /**
     * It reads particular LR assignments for concrete derived LR assignment.
     * @param assignment The derived LR assignment
     * @return List of required particular LR assignment
     */
    List<LrSimpleAssignment> readLrPartAssignments(final LrDerivedAssignment assignment);

    /**
     * It reads EFP according to the input parameters..
     * @param efpName Name of the EFP
     * @param grId ID of the GR which contains the EFP.
     * @return Required EFP
     */
    EFP readEFP(final String efpName, final int grId);

    /**
     * It reads EFP according to the input parameters..
     * @param efpName Name of the EFP
     * @param grIdStr A string ID of the GR which contains the EFP.
     * @return Required EFP
     */
    EFP readEFP(final String efpName, final String grIdStr);


    /**
     * It reads a parameter identified by its ID.
     * The parameters are used for storing any additional information
     * in the EFP mirror.
     * @param id ID of required the other value
     * @return Text of other value.
     */
    String readParamObject(final int id);

    /**
     * IT adds EFP assignment to the mirror.
     * @param efpAssignment A EFP assingment.
     * @return Returns ID if was insert EFP assignment with direct or math.formula value.
     */
    Integer addEfpAssignment(EfpAssignment efpAssignment);


    /**
     * It deletes an EFP.
     * @param efp A EFP for deleting
     */
    void deleteEFP(EFP efp);


    /**
     * It deletes a parameter according to the input ID.
     * @param id ID of other value
     */
    void deleteParamObject(int id);


    /**
     * It deletes a LR assignment from the mirror..
     * @param lrAssignment A LR assignment for deleting
     */
    void deleteLRAssignment(LrAssignment lrAssignment);
}
