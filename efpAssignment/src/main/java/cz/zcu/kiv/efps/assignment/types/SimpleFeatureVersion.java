package cz.zcu.kiv.efps.assignment.types;


/**
 * This element represents a version of a feature.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class SimpleFeatureVersion implements FeatureVersion {

    /** A major number of the version. */
    private Integer major;
    /** A minor number of the version. */
    private Integer minor;
    /** A micro number of the version. */
    private Integer micro;
    /** A release note of the version. */
    private String releaseNote;

    /**
     * Instance for implicit version for object, where version is missing.
     */
    public static final SimpleFeatureVersion IMPLICIT_VERSION = new SimpleFeatureVersion(0, 0, 0);

    /**
     * @param major
     *            A major number of the version
     * @param minor
     *            A minor number of the version
     * @param micro
     *            A micro number of the version
     * @param releaseNote
     *            A release note of the version
     */
    public SimpleFeatureVersion(final Integer major, final Integer minor,
            final Integer micro, final String releaseNote) {

        this.major = major;
        this.minor = minor;
        this.micro = micro;
        this.releaseNote = releaseNote;
    }

    /**
     * @param major
     *            A major number of the version
     * @param minor
     *            A minor number of the version
     * @param micro
     *            A micro number of the version
     */
    public SimpleFeatureVersion(final Integer major, final Integer minor,
            final Integer micro) {

        this(major, minor, micro, null);
    }

    /**
     * @param major
     *            A major number of the version
     * @param minor
     *            A minor number of the version
     */
    public SimpleFeatureVersion(final Integer major, final Integer minor) {

        this(major, minor, null, null);
    }

    /**
     * @param major
     *            A major number of the version
     */
    public SimpleFeatureVersion(final Integer major) {

        this(major, null, null, null);
    }


    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((major == null) ? 0 : major.hashCode());
        result = prime * result + ((micro == null) ? 0 : micro.hashCode());
        result = prime * result + ((minor == null) ? 0 : minor.hashCode());
        result = prime * result
                + ((releaseNote == null) ? 0 : releaseNote.hashCode());
        return result;
    }

    @Override
    public boolean matchesTo(final FeatureVersion another) {

        if (!(another instanceof SimpleFeatureVersion)) {
            return (another instanceof FeatureVersionRange) && another.matchesTo(this);
        }

        SimpleFeatureVersion anotherVersion = (SimpleFeatureVersion) another;

        int cmp = major.compareTo(anotherVersion.major);

        if (minor != null && anotherVersion.minor != null) {
            if (cmp == 0) {
                cmp = minor.compareTo(anotherVersion.minor);
            }

            if (micro != null && anotherVersion.micro != null) {
                if (cmp == 0) {
                    cmp = micro.compareTo(anotherVersion.micro);
                }

                if (releaseNote != null && anotherVersion.releaseNote != null) {
                    // we can decide only by release note
                    if (cmp == 0) {
                        return releaseNote.equals(anotherVersion.releaseNote);
                    }
                }
            }
        }

        return cmp >= 0;
    }


    @Override
    public String getIdentifier() {

        StringBuffer buffer = new StringBuffer()
            .append(major);

        if (minor != null) {
            buffer.append(".").append(minor);
        }

        if (micro != null) {
            buffer.append(".").append(micro);
        }

        if (releaseNote != null) {
            buffer.append(".").append(releaseNote);
        }

        return buffer.toString();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SimpleFeatureVersion other = (SimpleFeatureVersion) obj;
        if (major == null) {
            if (other.major != null) {
                return false;
            }
        } else if (!major.equals(other.major)) {
            return false;
        }
        if (micro == null) {
            if (other.micro != null) {
                return false;
            }
        } else if (!micro.equals(other.micro)) {
            return false;
        }
        if (minor == null) {
            if (other.minor != null) {
                return false;
            }
        } else if (!minor.equals(other.minor)) {
            return false;
        }
        if (releaseNote == null) {
            if (other.releaseNote != null) {
                return false;
            }
        } else if (!releaseNote.equals(other.releaseNote)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return getIdentifier();
    }
}
