/**
 *
 */
package cz.zcu.kiv.efps.assignment.values;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.evaluator.RelatedAssignmentsFinder;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * A convenient class for other concrete implementations.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class AbstractEfpAssignedValue
implements EfpAssignedValue {

	/** Logger. */
	private Logger logger = LoggerFactory.getLogger(getClass());

	/** A list of related features. */
	private List<EfpFeature> relatedFeatures;

	/** A related values finder. */
	private RelatedAssignmentsFinder finder;

	/**
	 * @param relatedFeatures a list of related features.
	 */
	public AbstractEfpAssignedValue(final List<EfpFeature> relatedFeatures) {

		this.relatedFeatures = relatedFeatures;
	}

	/** {@inheritDoc} */
	@Override
	public final EfpValueType computeValue() {

		// go through required side of this component
		// and load the values from provided side of
		// other components.
		List<EfpAssignment> relatedAssignments = new LinkedList<EfpAssignment>();
		for (EfpFeature feature : relatedFeatures) {

			if (finder == null) {
				return null;
			}

			// finds related value
			EfpAssignedValue relatedValue = finder.getConnectedAssignments(
					feature.getFeature(),
					feature.getEfp());

			if (logger.isTraceEnabled()) {
				logger.trace("Efp-feature pair " + feature + " has value " + relatedValue);
			}


			if (relatedValue == null) {

				if (logger.isTraceEnabled()) {
					logger.trace("Efp-feature pair " + feature
							+ " has not attached value. Returning null");
				}

				return null;
			}

			// compose a new assignment as a required side of this component
			// plus a value from a related component
			EfpAssignment relatedAssignment = new EfpAssignment(
					feature.getEfp(), relatedValue, feature.getFeature());

			relatedAssignments.add(relatedAssignment);
		}

		return doComputeValue(relatedAssignments);
	}

	/**
	 * This method computes a concrete value.
	 * It may e.g. return a direct value that has been assigned to
	 * a property,return a value stored in a registry, or compute
	 * the value from math formula.
	 *
	 * @param values values participating in computation of this property
	 * @return a concrete computed value
	 */
	protected abstract EfpValueType doComputeValue(List<EfpAssignment> values);


	/** {@inheritDoc} */
	@Override
	public final List<EfpFeature> getRelatedRequiredSide() {
		return relatedFeatures;
	}

	/** {@inheritDoc} */
	@Override
	public final void setRelatedValuesCallback(final RelatedAssignmentsFinder finder) {
		this.finder = finder;
	}


}

