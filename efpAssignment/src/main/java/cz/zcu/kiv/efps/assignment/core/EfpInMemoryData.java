package cz.zcu.kiv.efps.assignment.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataManipulator;
import cz.zcu.kiv.efps.assignment.extension.api.EfpLink;
import cz.zcu.kiv.efps.assignment.repomirror.api.MirroredDataManipulator;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * This is a helper class used for holding
 * the EFP data loaded from components in memory.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpInMemoryData {

    /** A logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Map with features and their EFP assignments. */
    private Map<Feature, List<EfpAssignment>> efpAssignmentsMap;

    /** EFP data manipulator.  */
    private EfpDataManipulator efpDataManipulator;

    /** Mirrored data manipulator.  */
    private MirroredDataManipulator mirroredDataManipulator;
    /** EFP access. */
    private ComponentEfpAccessor efpAccessor;

    /** A private constructor. */
    private EfpInMemoryData() { }

    /**
     * This is an interface for a memory data loader.
     * @author Kamil Jezek [kjezek@kiv.zcu.cz]
     *
     */
    public static interface EfpInMemoryDataLoader {

        /**
         * This method is called to load the EFP data into memory.
         * @return an in memory data representation.
         */
        EfpInMemoryData loadData();
    }

    /**
     * This method creates an instance of an in memory EFP data loader.
     * @param efpDataManipulator the EFP data manipulator
     * @param mirroredDataManipulator the EFP mirror
     * @param efpAccessor object to access EFPs on a component.
     * @return an object holding in memory representation of the EFP data.
     */
    public static EfpInMemoryDataLoader createLoader(
            final EfpDataManipulator efpDataManipulator,
            final MirroredDataManipulator mirroredDataManipulator,
            final ComponentEfpAccessor efpAccessor) {

        return new EfpInMemoryDataLoader() {

            /** A self instance. */
            private volatile EfpInMemoryData self;

            @Override
            public synchronized EfpInMemoryData loadData() {

                if (self == null) {
                    self = new EfpInMemoryData();
                    self.efpDataManipulator = efpDataManipulator;
                    self.mirroredDataManipulator =  mirroredDataManipulator;
                    self.efpAccessor = efpAccessor;
                    self.load();
                }

                return self;
            }
        };

    }

    /**
     * It returns a list of assignments for a feature.
     * @param feature a feature.
     * @return a list of assignments.
     */
    public List<EfpAssignment> getAssignments(final Feature feature) {

        if (!efpAssignmentsMap.containsKey(feature)) {
            throw new AssignmentRTException("Key with feature " + feature.getIdentifier()
                    + " doesn't exist in map with EFP assingmnets.");
        }

        return efpAssignmentsMap.get(feature);
    }

    /**
     * It returns a list of features loaded in memory.
     * @return a list of features.
     */
    public Set<Feature> getFeatures() {
        return efpAssignmentsMap.keySet();
    }

    /**
     * If data weren't loaded, loads them.
     */
    private void load() {

        if (logger.isDebugEnabled()) {
            logger.debug("Loading features and their EFP assignments into program.");
        }

        efpAssignmentsMap = new HashMap<Feature, List<EfpAssignment>>();

        // reading features with their EFP assignments
        List<Feature> features = efpDataManipulator.readFeatures();
        FormulaResolver mathFormBfr = new FormulaResolver(efpAccessor);

        for (Feature feature : features) {
        List<EfpLink> efpAssignsData = efpDataManipulator.readEFPs(feature);
            efpAssignmentsMap.put(
                    feature, createEfpAssignments(
                            feature, efpAssignsData, mathFormBfr));
        }

        //creating EFP assignment with math formula
        List<EfpAssignment> efpMathFormAssigns = mathFormBfr.getEfpAssignments();
        for (EfpAssignment efpAssignment : efpMathFormAssigns) {
            if (!efpAssignmentsMap.containsKey(efpAssignment.getFeature())) {
                efpAssignmentsMap.put(
                        efpAssignment.getFeature(), new ArrayList<EfpAssignment>());
            }

            efpAssignmentsMap.get(efpAssignment.getFeature()).add(efpAssignment);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Features and EFP assignments was loaded.");
        }

    }


    /**
     * Creates efpAssignments of one feature from a string.
     * @param feature Owner of EFPs
     * @param efpAssignsData List of objects with information about EFP assignments.
     * @param mathFormulaBfr Buffer for creating EFP assignments with math.formula
     * @return List of new EFP assignments
     */
    @SuppressWarnings("unchecked")
    private List<EfpAssignment> createEfpAssignments(final Feature feature,
            final List<EfpLink> efpAssignsData,
            final FormulaResolver mathFormulaBfr) {

        if (logger.isDebugEnabled()) {
            logger.debug("Reconstruction of EFP assignments in feature "
                    + feature.getIdentifier() + " from list of EfpAssignmentData "
                    + "objects: " + efpAssignsData);
        }
        List<EfpAssignment> efpAssignments = new ArrayList<EfpAssignment>();

        for (EfpLink efpAssignData : efpAssignsData) {
            EfpAssignment efpAssignment = createEfpAssignment(
                    feature, efpAssignData, mathFormulaBfr);
            if (efpAssignment == null) {
                continue; //for math formula
            }

            efpAssignments.add(efpAssignment);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("EFP assignments was reconstructed: " + efpAssignments);
        }
        return efpAssignments;
    }

    /**
     * Reconstructs EFP assignment from parts.
     * @param feature The feature for the assignment.
     * @param link Information about EFP
     * @param mathFormulaBfr Buffer for creating EFP assignments with math.formula
     * @return Reconstructed EFP assignment.
     */
    public EfpAssignment createEfpAssignment(final Feature feature, final EfpLink link,
                                             final FormulaResolver mathFormulaBfr) {

        EFP efp;
        if (link.getGrIdStr() != null) {
            efp = mirroredDataManipulator.readEFP(
                    link.getEfpName(), link.getGrIdStr());
        } else {
            efp = mirroredDataManipulator.readEFP(
                    link.getEfpName(), link.getGrId());
        }

        EfpAssignedValue assignedValue;

        if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.named) {
            if (efp.getType() == EFP.EfpType.SIMPLE) {
                if (link.getLrIdStr() != null) {
                    assignedValue = new EfpNamedValue(mirroredDataManipulator.readLrAssignment(
                            efp, link.getLrIdStr(), link.getValueName()));
                } else {
                    assignedValue = new EfpNamedValue(mirroredDataManipulator.readLrAssignment(
                            efp, link.getLrId(), link.getValueName()));
                }

            } else if (efp.getType() == EFP.EfpType.DERIVED) {
                LrDerivedAssignment lrDerivedAssignment;
                if (link.getLrIdStr() != null) {
                    lrDerivedAssignment =
                            (LrDerivedAssignment) mirroredDataManipulator.readLrAssignment(
                            efp, link.getLrIdStr(), link.getValueName());
                } else {
                    lrDerivedAssignment =
                            (LrDerivedAssignment) mirroredDataManipulator.readLrAssignment(
                            efp, link.getLrId(), link.getValueName());
                }



                assignedValue = AssignmentsTools.setAssignmentWithLrDerivedAssign(
                        efpDataManipulator, lrDerivedAssignment, mirroredDataManipulator);

            } else {
                throw new AssignmentRTException("Unknown type of EFP.");
            }

        } else if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.direct) {
            Integer id = link.getParamObjectId();
            EfpValueType efpValueType = EfpSerialiser.deserialize(
                    (Class<EfpValueType>) efp.getValueType(),
                    mirroredDataManipulator.readParamObject(id));
            assignedValue = new EfpDirectValue(efpValueType);

        } else if (link.getTypeOfAssignedValue() == EfpAssignedValue.AssignmentType.formula) {
            Integer id = link.getParamObjectId();
            mathFormulaBfr.addFormula(
                    feature, efp, mirroredDataManipulator.readParamObject(id));
            if (logger.isDebugEnabled()) {
                logger.debug("EFP assignment with math.formula was "
                        + "found and added into buffer.");
            }
            return null;

        } else if (link.getTypeOfAssignedValue() == null) {
            assignedValue = null;

        } else {
            throw new RuntimeException("Unknown type of assigned value.");
        }

        return new EfpAssignment(efp, assignedValue, feature);
    }
}

