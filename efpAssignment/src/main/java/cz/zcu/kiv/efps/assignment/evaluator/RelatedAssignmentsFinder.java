/**
 *
 */
package cz.zcu.kiv.efps.assignment.evaluator;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This interface is used for finding
 * related assignments (assigned to other components)
 * in situations where computation of an assignment
 * needs values from connected components.
 *
 * Since this module works only with one component
 * this interface should be implemented by modules
 * evaluating a graph of components.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface RelatedAssignmentsFinder {

    /**
     * The method returns a value assigned on
     * a provided side of a component which is
     * connected to the required side of this component.
     *
     * The input parameters identify a feature and
     * an EFP on the required side of this components
     * while the returned value is loaded from the
     * provided side of a connected component.
     *
     * @param requiredEfp an EFP on the required side of this component
     * @param requiredFeature an EFP on the required side of this component
     * @return a value loaded from the
     * provided connected side of another component
     */
    EfpAssignedValue getConnectedAssignments(
            Feature requiredFeature,
            EFP requiredEfp);
}
