/**
 *
 */
package cz.zcu.kiv.efps.assignment.types;

import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This class holds a pair consisting
 * of a feature and an EFP assigned to this feature.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpFeature {

    /** An extra-functional property. */
    private EFP efp;
    /** A feature the property is assigned to. */
    private Feature feature;

    /**
    *
    * @param efp an extra-functional property
    * @param feature A feature the property is assigned to
    */
   public EfpFeature(final EFP efp, final Feature feature) {

       this.efp = efp;
       this.feature = feature;
   }

    /**
     * @return the efp
     */
    public EFP getEfp() {
        return efp;
    }

    /**
     * @return the feature
     */
    public Feature getFeature() {
        return feature;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((feature == null) ? 0 : feature.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpFeature other = (EfpFeature) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (feature == null) {
            if (other.feature != null) {
                return false;
            }
        } else if (!feature.equals(other.feature)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "EfpFeature [efp=" + efp + ", feature=" + feature + "]";
    }



}
