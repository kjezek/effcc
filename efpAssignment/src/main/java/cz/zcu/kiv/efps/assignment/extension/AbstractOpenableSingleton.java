/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension;

/**
 * An openable object implemented as a singleton.
 *
 * @param <T> a type to be opened.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class AbstractOpenableSingleton<T> implements Openable<T> {

    /** A singleton self instance. */
    private volatile Openable<T> self;

    /** A result of a first open operation. */
    private T result;

    /** {@inheritDoc} */
    @Override
    public final synchronized T open() {

        if (self == null) {
            self = new Openable<T>() {

                @Override
                public T open() {
                    return doOpen();  // delegate
                }

                @Override
                public void close() {
                    doClose(); // delegate
                }
            };

            result = self.open();
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public final synchronized void close() {

        // close only if there is something opened
        if (self != null) {
            self.close(); // delegate
            self = null;
        }
    }

    /**
     * This method is delegated from {@link #open()}
     * but several invocation of the {@link #open()} method
     * causes this method to be called only once until
     * the method {@link #close()} is called.
     * @return an opened object.
     */
    protected abstract T doOpen();

    /**
     * This method closes an opened object.
     */
    protected abstract void doClose();
}
