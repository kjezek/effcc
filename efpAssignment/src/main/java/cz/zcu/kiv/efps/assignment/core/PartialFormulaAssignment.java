package cz.zcu.kiv.efps.assignment.core;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Storage object for math.formula before loading all features a EFP named
 * and direct values in component.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class PartialFormulaAssignment {

    /** Feature which owns the math.formula. */
    private Feature feature;
    /** EFP which for was created the math.formula. */
    private EFP efp;
    /** Math.formula. */
    private String mathFormula;
    /** EFP access. */
    private ComponentEfpAccessor efpAccessor;


    /**
     * Default constructor.
     * @param feature Feature which owns the math.formula.
     * @param efp EFP which for was created the math.formula.
     * @param mathFormula Math.formula.
     * @param efpAccessor object to access EFPs on a component.
     */
    public PartialFormulaAssignment(
            final Feature feature,
            final EFP efp,
            final String mathFormula,
            final ComponentEfpAccessor efpAccessor) {

        this.feature = feature;
        this.efp = efp;
        this.mathFormula = mathFormula;
        this.efpAccessor = efpAccessor;
    }


    /**
     * Creates EFP assignment with math.formula from this object.
     * @return Created EFP assignment.
     */
    public EfpAssignment createEFPAssignment() {

        EfpFormulaValue efpMathformula = createEfpMathFormula();
        return new EfpAssignment(efp, efpMathformula, feature);
    }


    /**
     * Creates EFP assigned value with math.formula.
     * @return EFP assigned value with math.formula
     */
    public EfpFormulaValue createEfpMathFormula() {

        List<EfpFeature> efpFeatures = new ArrayList<EfpFeature>();
        List<Feature> features = efpAccessor.getAllFeatures();


        for (Feature feature : features) {
            String searchedEfpFeature;
            List<EFP> efps = efpAccessor.getEfps(feature);

            for (EFP efp : efps) {
                searchedEfpFeature = feature.getIdentifier() + "_" + efp.getName();
                if (mathFormula.indexOf(searchedEfpFeature) != -1) {
                    efpFeatures.add(new EfpFeature(efp, feature));
                }
            }
        }

        return new EfpFormulaValue(
                mathFormula, (EfpFeature[]) efpFeatures.toArray(new EfpFeature[0]));
    }
}
