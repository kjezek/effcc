package cz.zcu.kiv.efps.assignment.values;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LR;

/**
 * The representation of a directly assigned value to an extra-functional
 * property.
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public final class EfpDirectValue extends AbstractEfpAssignedValue {

    /** A directly assigned value. */
    private EfpValueType value;

    /**
     * Creates an instance of an assigned direct value.
     * @param value the value
     */
    public EfpDirectValue(final EfpValueType value) {
        super(new ArrayList<EfpFeature>());

        this.value = value;
    }

    /** {@inheritDoc} */
    @Override
    protected EfpValueType doComputeValue(final List<EfpAssignment> values) {
        return value;
    }

    /**
     *
     * @return a value wrapped in this assignment
     */
    public EfpValueType getValue() {
        return value;
    }

    /** {@inheritDoc}*/
    @Override
    public LR getLR() {
        return null;
    }

    /** {@inheritDoc}*/
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /** {@inheritDoc}*/
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpDirectValue other = (EfpDirectValue) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc}*/
    @Override
    public String toString() {
        return "" + value;
    }

    /** {@inheritDoc}*/
    @Override
    public String getValueName() {
        return null;
    }

    /** {@inheritDoc}*/
    @Override
    public String getSerialisedValue() {
        return value.serialise();
    }

    /** {@inheritDoc}*/
    @Override
    public AssignmentType getAssignmentType() {
        return EfpAssignedValue.AssignmentType.direct;
    }

}
