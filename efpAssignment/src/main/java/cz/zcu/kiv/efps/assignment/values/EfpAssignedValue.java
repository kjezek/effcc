package cz.zcu.kiv.efps.assignment.values;

import java.util.List;

import cz.zcu.kiv.efps.assignment.evaluator.RelatedAssignmentsFinder;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LR;

/**
 * A value that may be assigned to an extra-functional property on a feature.
 *
 * Date: 23.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public interface EfpAssignedValue {

    /**
     * This method computes a concrete value.
     * It may e.g. return a direct value that has been assigned to
     * a property,return a value stored in a registry, or compute
     * the value from math formula.
     *
     * @return a concrete computed value
     */
    EfpValueType computeValue();


    /**
     * This method should return an information about
     * related required side of this component.
     * The related side is the list of Features and their EFPs
     * which are needed to compute this assigned value.
     *
     * @return the list of relates features and their EFPs.
     */
    List<EfpFeature> getRelatedRequiredSide();

    /**
     * This method return LR which this value is assigned to.
     * It may return null if this value is not assigned to any LR
     * @return LR or null
     */
    LR getLR();

    /**
     * This method injects a lookup object which is able to
     * find values from other components, which are needed to compute
     * this value.
     *
     * @param finder the lookup (callback) object
     */
    void setRelatedValuesCallback(RelatedAssignmentsFinder finder);

    /**
     * This method returns name of an assigned value.
     * In case that the value name has no sense for a concrete implementation,
     * the implementation should return either null or empty string.
     * @return String ValueName or null
     */
    String getValueName();

    /**
     * This method returns string with serialised assigned value.
     * @return String with serialised value
     */
    String getSerialisedValue();

    /**
     * AssignmentType enumeration describes type of assignment.
     * There are usual types like Direct, Formula
     * and value identified by a name.
     */
    public enum AssignmentType { named, direct, formula };

    /**
     * This method allows to identify to which AssignmentType family
     * a concrete efp belongs.
     * @return AssignmentType of a concrete efp.
     */
    AssignmentType getAssignmentType();

}
