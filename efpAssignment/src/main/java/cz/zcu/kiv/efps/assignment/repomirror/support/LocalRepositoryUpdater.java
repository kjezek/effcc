package cz.zcu.kiv.efps.assignment.repomirror.support;

import java.util.List;

import javax.xml.bind.JAXBElement;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.repository.generated.*;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrAssignment.LrAssignmentType;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * .
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class LocalRepositoryUpdater {
    /** Root of repository. */
    private RepositoriesElement repositoriesElement;
    /** Reference to object of class ElementsCreaterSetter. */
    private ElementsCreatorSetter elemsCreatorSetter;



    /**
     * Default constructor.
     * @param localRepository Root of updated repository
     */
    public LocalRepositoryUpdater(final JAXBElement<?> localRepository) {
        if (localRepository == null
                || !(localRepository.getValue() instanceof RepositoriesElement)) {
            throw new AssignmentRTException("Parameter doesn't "
                    + "contain element with root of repository.");
        }

        elemsCreatorSetter = new ElementsCreatorSetter();
        this.repositoriesElement = (RepositoriesElement) localRepository.getValue();
    }


    /**
     * Writes into repository file data of the EFP assignment.
     * @param efpAssignment The Efp assignment
     * @return ID in XML for direct value and math.formula or NULL for named value.
     */
    public Integer writeAssignment(final EfpAssignment efpAssignment) {
        //finding or adding repository with required GR
        writeMissingRepositoryElement(efpAssignment.getEfp().getGr());

        //finding required EFP in GR in repository
        writeMissingSimpleOrDerivedEFPElement(efpAssignment.getEfp());

        if (efpAssignment.getEfpValue() != null) { //not empty value
            if (efpAssignment.getEfpValue().getAssignmentType()
                    == EfpAssignedValue.AssignmentType.named) {
                EfpNamedValue efpNamedValue = (EfpNamedValue) efpAssignment.getEfpValue();

                //finding lrAssignment in repository if efpAssignment contains efpNamedValue
                writeMissingLrSimOrDerAssignElement(efpNamedValue.getLrAssignment(),
                        efpNamedValue.getLrParticipatingAssignments());

                if (efpNamedValue.getLrAssignment() instanceof LrDerivedAssignment) {
                    writeMissingParticLrAssignments(efpNamedValue.getLrParticipatingAssignments());
                }

            } else if (efpAssignment.getEfpValue().getAssignmentType()
                        == EfpAssignedValue.AssignmentType.direct
                    || efpAssignment.getEfpValue().getAssignmentType()
                            == EfpAssignedValue.AssignmentType.formula) {

                if (repositoriesElement.getParameters() == null) {
                    repositoriesElement.setParameters(new ObjectFactory().createParameterValues());
                }

                int maxId = 0;
                for (ParameterValueElement otherValue : repositoriesElement.getParameters().
                        getParameter()) {
                    if (otherValue.getId().intValue() > maxId) {
                        maxId = otherValue.getId().intValue();
                    }
                }

                repositoriesElement.getParameters().getParameter().add(
                        elemsCreatorSetter.createAndSetStringValue(
                                efpAssignment.getEfpValue().getSerialisedValue(), ++maxId));

                return maxId;
            }
        }

        return null;
    }

    /**
     * If RepositoryElement with concrete GR doesn't exist in actual local repository ,
     * it's created.
     * @param gr GR
     * @return Repository element with the GR.
     */
    public RepositoryElement writeMissingRepositoryElement(final GR gr) {

        RepositoryElement requiredRepository = ElementsGetter.getRepositoryElement(
                repositoriesElement, gr.getId());

        if (requiredRepository == null) {
            requiredRepository =
                elemsCreatorSetter.createAndSetRepositoryElement(gr);
            repositoriesElement.getRegistries().add(requiredRepository);
        }

        return requiredRepository;
    }


    /**
     * If EFP element doesn't exist in RepositoryElement with concrete EFP, it's created.
     * @param efp EFP
     */
    public void writeMissingSimpleOrDerivedEFPElement(final EFP efp) {

        RepositoryElement repository = ElementsGetter.getRepositoryElement(
                repositoriesElement, efp.getGr().getId());

        if (repository == null) {
            repository = writeMissingRepositoryElement(efp.getGr());
        }

        Object requiredEfp = ElementsGetter.getSimpleOrDerivedEFPElement(repository, efp.getName());

        if (requiredEfp == null) {
            Object newEfpElem;

            if (efp.getType() == EfpType.SIMPLE) {
                newEfpElem =
                    elemsCreatorSetter.createAndSetSimpleEFPElement((SimpleEFP) efp);

            } else if (efp.getType() == EFP.EfpType.DERIVED) {
                PropertyReferencesElement pre =
                    elemsCreatorSetter.createAndSetPropRefsElement(repository,
                            ((DerivedEFP) efp).getEfps());

                newEfpElem = elemsCreatorSetter.createAndSetDerivedEFPElement(
                        ((DerivedEFP) efp), pre);

            } else {
                throw new RuntimeException("Unknown type of a EFP.");
            }

            repository.getGr().
                getEfps().getSimpleOrDerived().add(newEfpElem);
        }
    }


    /**
     * If LR assignment element doesn't exist  in LRElement with concrete LrAssignment,
     * it's created.
     * @param lrAssignment LrAssignment
     * @param lrParticularAssignments List of particular LR assignments
     */
    public void writeMissingLrSimOrDerAssignElement(
            final LrAssignment lrAssignment, final LrAssignment[] lrParticularAssignments) {

        RepositoryElement repository = ElementsGetter.getRepositoryElement(
                repositoriesElement, lrAssignment.getEfp().getGr().getId());

        if (repository == null) {
            repository = writeMissingRepositoryElement(lrAssignment.getEfp().getGr());
        }

        LRElement lrElement = ElementsGetter.getLRElement(repository,
                lrAssignment.getLr().getId());

        if (lrElement == null) {
            lrElement = elemsCreatorSetter.createAndSetLRElement(lrAssignment.getLr());
            repository.getLr().add(lrElement);
        }

        Object lrAssignmentElem =
            ElementsGetter.getSimpleOrDerivedAssignmentElement(lrElement,
                    lrAssignment.getEfp().getId(), lrAssignment.getValueName());

        //adding lrAssignment into selected LRElement
        if (lrAssignmentElem == null) {
            if (lrAssignment.getAssignmentType() == LrAssignmentType.SIMPLE) {

                lrAssignmentElem =
                    elemsCreatorSetter.createAndSetLrSimAssignElement(
                        (LrSimpleAssignment) lrAssignment);
            } else if (lrAssignment.getAssignmentType() == LrAssignmentType.DERIVED) {

                lrAssignmentElem =
                    elemsCreatorSetter.createAndSetLrDerAssignElement(
                        (LrDerivedAssignment) lrAssignment, lrParticularAssignments);
            } else {
                throw new RuntimeException("Unknow type of a LR assignment.");
            }

            lrElement.getSimpleAssignmentOrDerivedAssignment().add(
                    lrAssignmentElem);
        }
    }


    /**
     * If participating LR assignments don't exists in actual repository, they're created.
     * @param lrAssignments Array with participating LR assignments.
     */
    private void writeMissingParticLrAssignments(final LrAssignment[] lrAssignments) {
        if (lrAssignments == null) {
            return;
        }

        for (LrAssignment lrAssignment : lrAssignments)  {
            EFP efp = lrAssignment.getEfp();
            GR gr = efp.getGr();

            //finding or adding repository with required GR
            writeMissingRepositoryElement(gr);

            //finding required EFP in GR in repository
            writeMissingSimpleOrDerivedEFPElement(efp);

            //finding lrAssignment in repository
            writeMissingLrSimOrDerAssignElement(lrAssignment, null);
        }
    }


    /**
     * Deletes from a repository a EFP.
     * @param efp A EFP for deleting
     */
    public void deleteEFP(final EFP efp)  {
        RepositoryElement repository = ElementsGetter.getRepositoryElement(
                repositoriesElement, efp.getGr().getId());

        if (repository == null) {
            throw new AssignmentRTException("Missing required repository "
                    + "element with global register.");
        }

        Object efpElement = ElementsGetter.getSimpleOrDerivedEFPElement(repository, efp.getName());
        if (efpElement == null) {
            throw new AssignmentRTException("Missing required element with EFP.");
        }

        List<Object> listOfProperties =
            repository.getGr().getEfps().getSimpleOrDerived();

        listOfProperties.remove(efpElement);
        if (listOfProperties.size() == 0) {
            repositoriesElement.getRegistries().remove(repository);
        }
    }


    /**
     * Deletes from a repository element with direct or math.formula value.
     * @param id ID of element for deleting
     */
    public void deleteOtherValue(final int id)  {
        if (repositoriesElement.getParameters() == null) {
            throw new AssignmentRTException("Element with math.formula or direct values is empty.");
        }

        Object otherValue = ElementsGetter.getParamValue(
                repositoriesElement.getParameters(), id);

        if (otherValue == null) {
            throw new AssignmentRTException("Required math.formula or direct value is missing.");
        }

        if (!repositoriesElement.getParameters().getParameter().remove(otherValue)) {
            throw new AssignmentRTException(
                    "Math.formula or direct value with id " + id + " wasn't deleted.");
        }
    }


    /**
     * Deletes from a repository element with LR assignment.
     * @param lrAssignment A LR assignment for deleting
     */
    public void deleteLrAssignment(final LrAssignment lrAssignment)  {
        EFP efp = lrAssignment.getEfp();

        RepositoryElement repository = ElementsGetter.getRepositoryElement(
                repositoriesElement, efp.getGr().getId());

        if (repository == null) {
            throw new AssignmentRTException("Missing required repository "
                    + "element with global register.");
        }

        LRElement lrElement = ElementsGetter.getLRElement(repository, lrAssignment.getLr().getId());
        if (lrElement == null) {
            throw new AssignmentRTException("Missing required LR element.");
        }

        Object lrAssignmentElem = ElementsGetter.getSimpleOrDerivedAssignmentElement(
                lrElement, efp.getId(), lrAssignment.getValueName());
        if (lrAssignmentElem == null) {
            throw new AssignmentRTException("Missing required LR assignment element.");
        }

        lrElement.getSimpleAssignmentOrDerivedAssignment().remove(lrAssignmentElem);

        if (lrElement.getSimpleAssignmentOrDerivedAssignment().size() == 0) {
            repository.getLr().remove(lrElement);
        }
    }
}
