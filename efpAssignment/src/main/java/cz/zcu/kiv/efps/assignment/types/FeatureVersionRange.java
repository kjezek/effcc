package cz.zcu.kiv.efps.assignment.types;

/**
 * Version range of a feature.
 *
 * Date: 13.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class FeatureVersionRange implements FeatureVersion {

    /** Implicit right version in version range. */
    public static final SimpleFeatureVersion IMPLICIT_RIGHT_VERSION
            = new SimpleFeatureVersion(Integer.MAX_VALUE);
    /** Implicit left version in version range. */
    public static final SimpleFeatureVersion IMPLICIT_LEFT_VERSION
            = new SimpleFeatureVersion(0, 0, 0);

    /** Min.version. */
    private SimpleFeatureVersion leftValue;
    /** Max.version. */
    private SimpleFeatureVersion rightValue;

    /** Including min.version. */
    private Boolean leftInclusive;
    /** Including max.version. */
    private Boolean rightInclusive;


    /**
     * Constructor for setting version range.
     * @param leftValue Min. simple version
     * @param leftInclusive True if including min.version
     * @param rightValue Max. simple version
     * @param rightInclusive True if including max.version
     */
    public FeatureVersionRange(final SimpleFeatureVersion leftValue, final boolean leftInclusive,
            final SimpleFeatureVersion rightValue, final boolean rightInclusive) {
        this.leftValue = leftValue;
        this.leftInclusive = leftInclusive;

        this.rightValue = rightValue;
        this.rightInclusive = rightInclusive;
    }


    @Override
    public String getIdentifier() {
        String featureVersion = "";

        if (leftInclusive) {
            featureVersion += "[";
        } else {
            featureVersion += "(";
        }

        featureVersion += leftValue.getIdentifier() + ", " + rightValue.getIdentifier();

        if (rightInclusive) {
            featureVersion += "]";
        } else {
            featureVersion += ")";
        }

        return featureVersion;
    }


    @Override
    public boolean matchesTo(final FeatureVersion another) {
        if (!(another instanceof SimpleFeatureVersion)) {
            //TODO may be do test of matching this range to other range
            return false;
        }

        SimpleFeatureVersion anotherVersion = (SimpleFeatureVersion) another;

        if (!anotherVersion.matchesTo(leftValue)) {
            return false;
        }

        if (!leftInclusive && leftValue.getIdentifier().equals(anotherVersion.getIdentifier())) {
            return false;
        }

        if (!rightValue.matchesTo(anotherVersion)) {
            return false;
        }

        if (!rightInclusive && rightValue.getIdentifier().equals(anotherVersion.getIdentifier())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((leftInclusive == null) ? 0 : leftInclusive.hashCode());
        result = prime * result + ((leftValue == null) ? 0 : leftValue.hashCode());
        result = prime * result + ((rightValue == null) ? 0 : rightValue.hashCode());
        result = prime * result + ((rightInclusive == null) ? 0 : rightInclusive.hashCode());

        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FeatureVersionRange other = (FeatureVersionRange) obj;
        if (leftInclusive == null) {
            if (other.leftInclusive != null) {
                return false;
            }
        } else if (!leftInclusive.equals(other.leftInclusive)) {
            return false;
        }

        if (leftValue == null) {
            if (other.leftValue != null) {
                return false;
            }
        } else if (!leftValue.equals(other.leftValue)) {
            return false;
        }

        if (rightValue == null) {
            if (other.rightValue != null) {
                return false;
            }
        } else if (!rightValue.equals(other.rightValue)) {
            return false;
        }

        if (rightInclusive == null) {
            if (other.rightInclusive != null) {
                return false;
            }
        } else if (!rightInclusive.equals(other.rightInclusive)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return getIdentifier();
    }

    /**
     * @return Minimal version from interval.
     */
    public SimpleFeatureVersion getLeftValue() { return leftValue; }

    /**
     * @return Maximal version from interval.
     */
    public SimpleFeatureVersion getRightValue() { return rightValue; }

    /**
     * @return If is TRUE, interval includes minimal version.
     */
    public Boolean getLeftInclusive() { return leftInclusive; }

    /**
     * @return If is TRUE, interval includes maximal version.
     */
    public Boolean getRightInclusive() { return rightInclusive; }

    /**
     * @return Returns true, if left value in range is implicit.
     */
    public Boolean isImplicitLeftValue() {
        return leftValue.equals(IMPLICIT_LEFT_VERSION);
    }

    /**
     * @return Returns true, if right value in range is implicit.
     */
    public Boolean isImplicitRightValue() {
        return rightValue.equals(IMPLICIT_RIGHT_VERSION);
    }
}
