package cz.zcu.kiv.efps.assignment.repomirror.support;

/**
 * It contains parameters for using with JAXB.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class JAXBParameters {
    /** Default path to xsd schema of local repository. */
    public static final String SCHEMA = "localrepository.xsd";

    /** Reference to ObjectFactory.class.*/
    public static final Class<?> OBJECT_FACTORY_CLASS =
        cz.zcu.kiv.efps.assignment.repository.generated.ObjectFactory.class;

    /** Settings of JAXB encoding. */
    public static final String ENCODING = "utf-8";


    /**
     * Private constructor.
     */
    private JAXBParameters() {

    }
}
