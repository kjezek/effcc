package cz.zcu.kiv.efps.assignment.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.extension.api.AbstractEfpDataLocation;
import cz.zcu.kiv.efps.assignment.extension.api.EfpDataLocation;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * This is a convenient out-of-the-box implementation of {@link AbstractEfpDataLocation}
 * working with a manifest file. Its application is for a component models
 * such as OSGi or CoSi which work with the manifest files
 * <p/>
 * Date: 21.2.2011
 *
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class CommonBundleFilesManagerImpl extends AbstractEfpDataLocation {

    /**
     * A logger.
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * A pattern for searching of file with signature in JAR.
     */
    private static final String PATTERN_SF_FILE = "META-INF/.*\\.SF";
    /**
     * Default path to the xml file with repository in jar.
     */
    private static final String PATH_TO_REPOSITORY = "META-INF/repository.xml";
    /**
     * Default path to the manifest in jar.
     */
    private static final String PATH_TO_MANIFEST = "META-INF/MANIFEST.MF";
    /**
     * A postfix for dir with extracted files from the jar.
     */
    public static final String UNPACK_DIR_POSTFIX = "_extract";
    /**
     * A component opened by this class.
     */
    private String component;
    /**
     * Contains absolute path to extract directory.
     */
    private String extractedDir;
    /**
     * TRUE - a component was changed (update of bundle during closing),
     * FALSE - no changes (no update).
     */
    private boolean isModified;

    /**
     * Default constructor for setting path to component and extract dir.
     *
     * @param component Absolute path to jar file
     */
    public CommonBundleFilesManagerImpl(final String component) {
        if (component == null) {
            logger.error("Parameter with path to component is null.");
            throw new AssignmentRTException("Parameter with path to the component is null.");
        }

        this.component = component;
        this.isModified = false;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected EfpDataLocation openComponent() {
        File testFile = new File(component);
        if (!testFile.exists() || !testFile.isFile()) {
            logger.error("Bundle on the path doesn't exist.");
            throw new AssignmentRTException("Required file with jar "
                    + component + " doesn't exist.");
        }

        this.extractedDir = FilenameUtils.getFullPath(testFile.getAbsolutePath())
                + FilenameUtils.getBaseName(component)
                + UNPACK_DIR_POSTFIX + new Date().getTime();

        //check if directory with same name already exists or reseting of extract directory name
        File extractDirFile = new File(extractedDir);
        while (extractDirFile.exists()) {
            this.extractedDir = FilenameUtils.getFullPath(testFile.getAbsolutePath())
                    + FilenameUtils.getBaseName(component)
                    + UNPACK_DIR_POSTFIX + new Date().getTime();
            extractDirFile = new File(extractedDir);
        }

        JarFile jarFile = null;

        try {
            jarFile = new JarFile(new File(component));

            if (checkIfJarHasSignature(jarFile)) {
                throw new AssignmentRTException("Unsupported file: Signed JAR.");
            }

            FileUtils.forceMkdir(extractDirFile);

            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry je = entries.nextElement();
                java.io.File fl = new java.io.File(extractedDir, je.getName());
                if (!fl.exists()) {
                    fl.getParentFile().mkdirs();
                    fl = new java.io.File(extractedDir, je.getName());
                }

                if (je.isDirectory()) {
                    continue;
                }

                InputStream is = jarFile.getInputStream(je);
                try {
                    FileUtils.copyInputStreamToFile(is, fl);
                } finally {
                    IOUtils.closeQuietly(is);
                    is = null;
                }
            }

            //finding of manifest file
            File copyOfMF = new File(extractedDir, PATH_TO_MANIFEST);
            if (!copyOfMF.exists()) {
                logger.error("Manifest file in jar is missing.");
                throw new AssignmentRTException("Manifest file in jar is missing.");
            }
        } catch (IOException e) {
            logger.error("Error during extracting required files from the jar: " + e, e);
            throw new AssignmentRTException(e);
        } finally {
            IOUtils.closeQuietly(jarFile);
            jarFile = null;
            System.gc();
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Extract dir for component '"
                    + component + "' with required files was created.");
        }

        logger.info("Component '" + component + "' was opened.");

        return new EfpDataLocation() {
            /** {@inheritDoc} */
            @Override
            public String getComponentPath() {
                return extractedDir;
            }

            /** {@inheritDoc} */
            @Override
            public String getEfpDataPath() {
                return getComponentPath() + IOUtils.DIR_SEPARATOR + PATH_TO_MANIFEST;
            }

            /** {@inheritDoc} */
            @Override
            public String getEfpMirrorPath() {
                return getComponentPath() + IOUtils.DIR_SEPARATOR + PATH_TO_REPOSITORY;
            }

            /** {@inheritDoc} */
            @Override
            public void setIsModified(final boolean modified) {
                isModified = modified;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void closeComponent() {
        //no changes -> skip creating of new jar file.
        if (!isModified) {
            logger.info("Component '" + component + "' will be closed without changes.");

            File extractDirFile = new File(extractedDir);
            if (extractDirFile.exists()) {
                try {
                    FileDeleteStrategy.FORCE.delete(extractDirFile);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Directory '" + extractDirFile
                                + "' with working copies files from JAR was deleted.");
                    }
                } catch (IOException e) {
                    logger.error("Can't delete working directory '" + extractDirFile
                            + "' with copies of JAR files during closing bundle: " + e, e);
                    throw new AssignmentRTException(e);
                }
            }

            return;
        }

        if (!new File(component).exists()) {
            logger.info("Component '" + component + "' will be closed without changes.");

            File extractDirFile = new File(extractedDir);
            if (extractDirFile.exists()) {
                try {
                    FileDeleteStrategy.FORCE.delete(extractDirFile);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Directory '" + extractDirFile
                                + "' with working copies files from JAR was deleted.");
                    }
                } catch (IOException e) {
                    logger.error("Can't delete working directory '" + extractDirFile
                            + "' with copies of JAR files during closing bundle: " + e, e);
                    throw new AssignmentRTException(e);
                }
            }

            return;
        }

        //rename old jar file for backup
        File oldJarFile = new File(FilenameUtils.getFullPath(component)
                + FilenameUtils.getBaseName(component) + "_old"
                + ".jar");

        if (oldJarFile.exists()) {
            logger.error("The file '" + oldJarFile.getAbsolutePath()
                    + "' that is temporarily used for " + "the original jar already exists.");
            throw new AssignmentRTException(new FileExistsException(oldJarFile));
        }

        if (!new File(component).renameTo(oldJarFile)) {
            logger.error("Can't rename editing jar '" + component + "' with temporary name "
                    + "during creating updated jar.");
            throw new AssignmentRTException("Can't rename editing jar '"
                    + component + "' with temporary name " + "during creating updated jar.");
        }

        //creating of new jar
        File newJarFile = new File(component);
        FileOutputStream tempJarFileStream = null;
        JarOutputStream newJarOutStream = null;
        boolean newJarDone = false;

        try {
            tempJarFileStream = new FileOutputStream(newJarFile);
            newJarOutStream = new JarOutputStream(tempJarFileStream);

            copyContentFromOldJarIntoNewJar(newJarOutStream, new File(extractedDir));

            newJarDone = true;
        } catch (IOException e) {
            logger.error("Error during creating a new copy of the jar"
                    + " with updated files." + e, e);
            throw new AssignmentRTException(e);

        } finally {
            IOUtils.closeQuietly(newJarOutStream);
            IOUtils.closeQuietly(tempJarFileStream);

            newJarOutStream = null;
            tempJarFileStream = null;
            System.gc(); //for sure

            //if new jar file wasn't created complete, removes it and rename old jar back.
            if (!newJarDone) {
                try {
                    FileDeleteStrategy.NORMAL.delete(newJarFile);
                } catch (IOException e) {
                    logger.error("Unfinished updated jar can't be deleted: " + e, e);
                }

                if (!oldJarFile.renameTo(newJarFile)) {
                    logger.error("Original jar on path '" + oldJarFile.getAbsolutePath()
                            + "' can't be renamed back.");
                }
            }
        }

        //deleting of old jar file and working directory
        try {
            FileDeleteStrategy.NORMAL.delete(oldJarFile);
        } catch (IOException e) {
            logger.error("Can't delete old jar after creating updated copy." + e, e);
            throw new AssignmentRTException(e);
        }
        logger.info("Componet '" + component + "' was updated.");

        File extractDirFile = new File(extractedDir);

        if (extractDirFile.exists()) {
            try {
                FileDeleteStrategy.FORCE.delete(extractDirFile);
                if (logger.isDebugEnabled()) {
                    logger.debug("Directory '" + extractDirFile
                        + "' with working copies files from JAR was deleted.");
                }
            } catch (IOException e) {
                logger.error("Can't delete working directory '" + extractDirFile
                        + "' with copies of JAR files during closing bundle: " + e, e);
                throw new AssignmentRTException(e);
            }
        }
    }


    /**
     * Copy files recursively from directory into new jar.
     *
     * @param newJarOutStream Reference to JarOutputStream for new jar.
     * @param sourceDirectory Directory with files and other directories for new jar file.
     * @throws IOException Error during copying files.
     */
    private void copyContentFromOldJarIntoNewJar(
            final JarOutputStream newJarOutStream, final File sourceDirectory) throws IOException {

        if (sourceDirectory == null || !sourceDirectory.exists()) {
            throw new FileNotFoundException(
                    "Directory with files of old jar is null or directory doesn't exist.");
        }

        if (newJarOutStream == null) {
            throw new IOException("Parameter with JarOutputStream is empty.");
        }

        File[] files = sourceDirectory.listFiles();
        FileInputStream fis = null;

        for (File f : files) {
            if (f.isDirectory()) {
                copyContentFromOldJarIntoNewJar(newJarOutStream, f);
            } else {
                String pathToFileInJar = f.getAbsolutePath().substring(
                        extractedDir.length() + 1).replace('\\', '/');

                try {
                    JarEntry newEntry = new JarEntry(pathToFileInJar);
                    fis = FileUtils.openInputStream(f);
                    newJarOutStream.putNextEntry(newEntry);
                    IOUtils.copy(fis, newJarOutStream);
                } finally {
                    IOUtils.closeQuietly(fis);
                    fis = null;
                }
            }
        }
    }

    /**
     * Checks if JAR file is signed.
     *
     * @param jarfile JAR file
     * @return TRUE - file is signed, FALSE - file isn't signed.
     */
    private boolean checkIfJarHasSignature(final JarFile jarfile) {
        Enumeration<JarEntry> entries = jarfile.entries();
        JarEntry entry;

        Pattern p = Pattern.compile(PATTERN_SF_FILE);

        while (entries.hasMoreElements()) {
            entry = entries.nextElement();

            Matcher m = p.matcher(entry.getName().toUpperCase());
            if (m.find()) {
                return true;
            }
        }

        return false;
    }
}
