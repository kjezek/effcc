package cz.zcu.kiv.efps.assignment.extension.manifest;


import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser for headers from manifest file.
 * <p/>
 * Date: 17.3.2014
 *
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class HeaderParser {

    /**
     * A regular expression pattern for getting parameters and directives from string.
     */
    private static final Pattern RGX_PARAMETER = Pattern.compile(
            "^([a-zA-Z0-9\\*\\-_\\.:<>]+[a-zA-Z0-9\\*\\-_\\.>])\\s*(=|:=)(.+)$", Pattern.MULTILINE);
    /**
     * A regular expression pattern for getting names of feature.
     */
    private static final Pattern RGX_FEATURE = Pattern.compile(
            "^([a-zA-Z0-9\\*\\-_\\.:<>]+[a-zA-Z0-9\\*\\-_\\.>])\\s*$", Pattern.MULTILINE);

    /**
     * Actual position in string.
     */
    private int iChar = 0;
    /**
     * The index in string where starts some object (feature, parameter,...).
     */
    private int iObjectStart = 0;
    /**
     * A header for parsing.
     */
    private String header = "";
    /**
     * The list with results.
     */
    private List<FeatureParameters> features = null;
    /**
     * All features in actual clause.
     */
    private List<String> featureNames = null;
    /**
     * Parameters of feature in actual clause.
     */
    private FeatureParameters fpInActualClause = null;
    /**
     * Because the clause can contains on the begin multiple features.
     * TRUE - only parameters in the clause are expected.
     * FALSE - before first parameter in the clause is read.
     */
    private boolean bParametersInClauseRead = false;

    /**
     * Default constructor.
     *
     * @param headerForParsing A header for parsing
     */
    private HeaderParser(final String headerForParsing) {
        this.header = headerForParsing;
    }

    /**
     * For simple use of parser.
     *
     * @param headerForParsing A header for parsing
     * @return The list of results.
     */
    public static List<FeatureParameters> parseHeader(final String headerForParsing) {
        HeaderParser hp = new HeaderParser(headerForParsing);
        return hp.parse();
    }

    /**
     * Main method of parser.
     *
     * @return The list of Results.
     */
    private List<FeatureParameters> parse() {
        features = new ArrayList<FeatureParameters>();
        iChar = 0;

        for (; iChar < header.length(); iChar++) {
            char c = header.charAt(iChar);

            if (endOfObjectInString()) {
                if (iChar == iObjectStart) {
                    if (featureNames == null) {
                        throw new AssignmentRTException("Before "
                                + Constants.SEPARATOR_PARAMS + " on position " + iChar
                                + " is expected at least one name of feature in string '"
                                + header + "'.");
                    }

                    continue; // empty parameter -> skip
                }

                processObject();

                if (c == Constants.SEPARATOR_CLAUSES || end()) {
                    saveFeaturesWithParams();
                }

                iObjectStart = iChar + 1;

            } else if (c == Constants.QUOTE) {
                skipValueInQuotes();

                if (end()) {
                    processObject();
                    saveFeaturesWithParams();
                }
            } else if (c == Constants.VALUE_LEFT_BRACKET) { //for CoSi
                skipValueInBrackets();

                if (end()) {
                    processObject();
                    saveFeaturesWithParams();
                }
            }
        }

        return features;
    }

    /**
     * Returns information about status of the parsing.
     *
     * @return True if the string was end to the end.
     */
    private boolean end() {
        return iChar + 1 >= header.length();
    }

    /**
     * Checks if actual char marks the end of some object (feature, parameter,...).
     *
     * @return True, if actual char is separator.
     */
    private boolean endOfObjectInString() {
        return (header.charAt(iChar) == Constants.SEPARATOR_PARAMS
                || header.charAt(iChar) == Constants.SEPARATOR_CLAUSES
                || header.charAt(iChar) == Constants.VALUE_RIGHT_BRACKET
                || end());
    }

    /**
     * For values in quotes. Skips to the second quote after occurs the first.
     */
    private void skipValueInQuotes() {
        int indexSecond = header.indexOf(Constants.QUOTE, iChar + 1);

        if (indexSecond == -1) {
            throw new AssignmentRTException(
                    "Missing second quote for quote on position " + (iChar + 1)
                    + " in string '" + header + "'.");
        } else {
            iChar = indexSecond;
        }
    }

    /**
     * For values in brackets. Skips to the right bracket after occurs the left bracket.
     */
    private void skipValueInBrackets() {
        int indexSecond = header.indexOf(Constants.VALUE_RIGHT_BRACKET, iChar + 1);

        if (indexSecond == -1) {
            throw new AssignmentRTException(
                    "Missing right bracket for left bracket on position " + (iChar + 1)
                            + " in string '" + header + "'.");
        } else {
            iChar = indexSecond;
        }
    }

    /**
     * Creates object from substring (feature, parameter,...) according
     * to actual state of the parser.
     */
    private void processObject() {
        String objectStr;
        if (end()) {
            objectStr = header.substring(iObjectStart).trim(); //substring to the end
        } else {
            objectStr = header.substring(iObjectStart, iChar).trim();
        }

        if (featureNames == null) {
            featureNames = new ArrayList<String>();
            featureNames.add(objectStr);
            fpInActualClause = new FeatureParameters();
        } else {
            constructObject(objectStr);
        }
    }

    /**
     * Creates a name of feature, parameter or directive from string.
     *
     * @param objectStr String representation of object.
     */
    private void constructObject(final String objectStr) {

        if (!bParametersInClauseRead) {
            Matcher matcher = RGX_FEATURE.matcher(objectStr);

            if (matcher.find()) {
                featureNames.add(objectStr);
                return;
            } else {
                if (featureNames.size() == 0) {
                    throw new AssignmentRTException("Before parameter '" + objectStr
                            + "' is expected at least one feature.");
                }

                bParametersInClauseRead = true;
            }
        }

        Matcher matcher = RGX_PARAMETER.matcher(objectStr);

        if (matcher.find()) {
            String name = matcher.group(1);
            String operator = matcher.group(2);
            String value = matcher.group(3);

            if (operator.equals(Constants.ASSIGNMENT_DIRECTIVES)) {
                if (fpInActualClause.getFeatureDirectives().containsKey(name)) {
                    throw new AssignmentRTException("The feature '"
                            + featureNames.get(0) + "' contains more "
                            + "directives with same name '" + name + "'.");
                }
                fpInActualClause.getFeatureDirectives().put(name, value);
            } else if (operator.equals(Constants.EQUAL)) {
                if (fpInActualClause.getFeatureAttributes().containsKey(name)) {
                    throw new AssignmentRTException("The feature '"
                            + featureNames.get(0) + "' contains more "
                            + "parameters with same name '" + name + "'.");
                }
                fpInActualClause.getFeatureAttributes().put(name, value);
            }
        } else {
             throw  new AssignmentRTException("String '" + objectStr
                     + "' doesn't contain a parameter or directive of feature.");
        }
    }

    /**
     * When clause is processed, this method put parsed features
     * with their parameters and directives into return object.
     */
    private void saveFeaturesWithParams() {
        if (featureNames == null) { //not empty clause
            return;
        }

        for (String feature : featureNames) {
            features.add(new FeatureParameters(fpInActualClause, feature));
        }

        featureNames = null;
        fpInActualClause = null;
        bParametersInClauseRead = false;
    }
}
