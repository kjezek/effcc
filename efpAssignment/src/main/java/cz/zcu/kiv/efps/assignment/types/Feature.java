package cz.zcu.kiv.efps.assignment.types;

/**
 * This interface expresses a general feature
 * which may be e.g. class, method, parameter.
 *
 * The purpose is to represent any feature
 * a concrete implementation of a component framework
 * may provide, or require.
 *
 * For instance, OSGi may export or import packages.
 * In terms of this feature, OSGi provides or requires packages.
 *
 * Date: 17.11.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public interface Feature {

    /**
    *
    * @return a literal indentifier of this object
    */
    String getIdentifier();

    /**
     * This method returns on which side of the component
     * this feature is.
     * @return the side of the component
     */
    AssignmentSide getSide();

    /**
     * It returns a name which is used for identifying
     * this feature in the system.
     *
     * @return the name
     */
    String getName();

    /**
     * This method returns which type this feature
     * represents in the system.
     *
     * @return the representElement
     */
    String getRepresentElement();

    /**
     * This method returns a parent
     * if this feature.
     *
     * @return the parent
     */
    Feature getParent();

    /**
     * This method returns a version of this
     * feature.
     * @return the version
     */
    FeatureVersion getVersion();

    /**
     * This method returns whether
     * this feature is mandatory to be presented
     * on a component.
     *
     * For instance a component may use a loggining interface,
     * however its absence does not prevent the component to work.
     * Hence the loggining feature should not be mandatory
     * @return true if the feature is a mandatory one
     */
    boolean isMandatory();


    /**
     * This enum holds information of the feature type. The type may be provided
     * or required one following the required and provided sides of the
     * component.
     */
    public enum AssignmentSide {
        /**
         * This value should be assigned once this feature is on the required
         * side of the component.
         */
        REQUIRED,
        /**
         * This value should be assigned once this feature is on the provided
         * side of the component.
         */
        PROVIDED
    }

}
