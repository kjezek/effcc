/**
 *
 */
package cz.zcu.kiv.efps.assignment.extension.api;

import cz.zcu.kiv.efps.assignment.extension.AbstractOpenableSingleton;
import cz.zcu.kiv.efps.assignment.extension.Openable;


/**
 * An abstract implementation of the {@link EfpDataManipulator}}
 * allowing also to locate an EFP data on a component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 * @param <T> a type of the EFP data.
 */
public abstract class AbstractEfpDataManipulator<T>
    implements EfpDataManipulator, Openable<T> {

    /** A component location manager. */
    private AbstractEfpDataLocation efpDataLocation;

    /** Delegates an open request to be processed only once. */
    private Openable<T> singletonDelegator = new AbstractOpenableSingleton<T>() {

		@Override
		protected T doOpen() {
			return AbstractEfpDataManipulator.this.open();
		}

		@Override
		protected void doClose() {
			AbstractEfpDataManipulator.this.close();
		}
	};


    /**
     * Constructor.
     * @param efpDataLocation EFP Data Location
     */
    public AbstractEfpDataManipulator(final AbstractEfpDataLocation efpDataLocation) {
        super();
        this.efpDataLocation = efpDataLocation;
    }

    /**
     * This method opens a component and locates EFP data on the component.
     * The component is opened delegating this method call to {@link AbstractEfpDataLocation#open()}}
     * and then the EFP data are opened delegating the call to a subclass
     *
     * This method opens the EFP data only once if the method is called multiple times.
     *
     * @return a generic representation of the EFP data.
     */
    protected T openEfpData() {
        return efpDataLocation.open().getEfpData(singletonDelegator);
    }

}
