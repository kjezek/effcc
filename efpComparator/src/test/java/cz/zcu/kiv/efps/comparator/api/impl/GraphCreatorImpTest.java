package cz.zcu.kiv.efps.comparator.api.impl;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test creating graph.
 *
 * @author Lukas Vlcek
 *
 */
public class GraphCreatorImpTest {

    @Test
    public void create() {
        ComponentLoaderImp loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C"),
        "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        DirectedGraph<GraphVertexRepr, DefaultEdge> graph = creator.create();
        GraphVertexRepr comp = new GraphVertexRepr("A");
        Feature compFeature = new BasicFeature("Component-A", AssignmentSide.PROVIDED,
                "component", true);
        GraphVertexRepr vertexFeatureR = new GraphVertexRepr(new BasicFeature("f-1",
                AssignmentSide.REQUIRED, "component", true, compFeature),"A");
        GraphVertexRepr vertexFeatureP = new GraphVertexRepr(new BasicFeature(
                "f-1", AssignmentSide.PROVIDED, "component", true),"B");
        EfpAssignedValue value = new EfpDirectValue(new EfpNumber(3));
        GraphVertexRepr efp = new GraphVertexRepr(new SimpleEFP("efp-1-1", null,
                null, null, null), value, vertexFeatureR);
        assertFalse(graph.containsEdge(comp, vertexFeatureR));
        assertTrue(graph.containsEdge(vertexFeatureR, comp));
        assertTrue(graph.containsEdge(vertexFeatureP, vertexFeatureR));
        assertFalse(graph.containsEdge(vertexFeatureR, vertexFeatureP));
        assertFalse(graph.containsEdge(vertexFeatureR, efp));
        assertTrue(graph.containsVertex(efp));
        assertTrue(graph.containsEdge(efp, vertexFeatureR));
    }

}
