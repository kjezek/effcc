package cz.zcu.kiv.efps.comparator.api.impl.evaluation;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.RelatedComponentsAssignments;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;
import cz.zcu.kiv.efps.comparator.result.EfpResult;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class EfpResultTest {

    static EfpResult efpResult;

    static List<EfpEvalResult> results;

    static DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    private Feature compFeature = new BasicFeature(
            "Component-A", AssignmentSide.PROVIDED, "component", true);
    private Feature feature = new BasicFeature("f-1",
            AssignmentSide.REQUIRED, "component", true, compFeature);
    private Feature feature1 = new BasicFeature("f-1",
            AssignmentSide.PROVIDED, "component", true);

    @BeforeClass
    public static void createGraph() {
        ComponentLoaderImp loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C"),
        "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        graph = creator.create();
        VertexNeighborFinder finder = new VertexNeighborFinder(graph);
        results = new ArrayList<EfpEvalResult>();
        efpResult = new EfpResult(finder);
    }

    /**
     * ¨Test result two connect efp.
     */
    @Test
    public void testmatchEfpResult() {
        EFP efp11 = new SimpleEFP("efp-1-1", null, null, null, null);
        GraphVertexRepr efpVertex = new GraphVertexRepr(efp11,  new EfpDirectValue(
                new EfpNumber(4)), new GraphVertexRepr(feature1,"B"));

        EFP efp11N = new SimpleEFP("efp-1-1", null, null, null, null);
        GraphVertexRepr matchEfp = new GraphVertexRepr(efp11N, new EfpDirectValue(
                new EfpNumber(3)), new GraphVertexRepr(feature,"A"));

        matchEfp.getAssignmentValue().setRelatedValuesCallback(new RelatedComponentsAssignments(
                graph,"B"));

        EfpEvalResult result = efpResult.matchEfpResult(efpVertex, matchEfp);
        EfpEvalResult log = new EfpEvalResult("B", "A", feature, efp11, MatchingResult.OK,
                new EfpNumber(4), new EfpNumber(3));
        assertTrue(log.equals(result));
    }

    /**
     * Test result efp without neighbor.
     */
    @Test
    public void testnoMatchEfpResult() {
        EFP efp12 = new SimpleEFP("efp-1-2", null, null, null, null);
        EfpAssignment efp12Assign = new EfpAssignment(efp12, new EfpDirectValue(
                new EfpNumber(4)), feature);
        GraphVertexRepr efpVertex = new GraphVertexRepr(efp12, efp12Assign.getEfpValue(),
                new GraphVertexRepr(efp12Assign.getFeature(),"A"));
        EfpEvalResult log = new EfpEvalResult("A", "B", feature, efp12, MatchingResult.MISSING,
                AssignmentSide.PROVIDED);
        EfpEvalResult resultCompare = efpResult.noMatchEfpResult(efpVertex);
        assertTrue(log.equals(resultCompare));
    }

}
