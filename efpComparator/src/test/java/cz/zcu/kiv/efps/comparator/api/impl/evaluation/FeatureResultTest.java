package cz.zcu.kiv.efps.comparator.api.impl.evaluation;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;
import cz.zcu.kiv.efps.comparator.result.FeatureResult;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class FeatureResultTest {

    static FeatureResult featureResult;

    static List<EfpEvalResult> results;

    static DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    private Feature compFeature = new BasicFeature(
            "Component-A", AssignmentSide.PROVIDED, "component", true);
    private Feature feature = new BasicFeature("f-1",
            AssignmentSide.REQUIRED, "component", true, compFeature);
    private Feature matchFeature = new BasicFeature(
            "f-1", AssignmentSide.PROVIDED, "component", true);


    @BeforeClass
    public static void createGraph() {
        ComponentLoaderImp loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C"),
        "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        graph = creator.create();
        VertexNeighborFinder finder = new VertexNeighborFinder(graph);
        results = new ArrayList<EfpEvalResult>();
        featureResult = new FeatureResult(finder);
    }

    /**
     * Test result of two connect features.
     */
    @Test
    public void testaddMatchFeatureResult() {
        EfpEvalResult result = featureResult.addMatchFeatureResult(
                new GraphVertexRepr(feature,"A"), new GraphVertexRepr(matchFeature,"A"));
        assertTrue(result == null);
    }

    /**
     * Test feature without neighbor.
     */
    @Test
    public void testaddNoMatchFeatureResult() {
        EfpEvalResult result = featureResult.addNoMatchFeatureResult(
                new GraphVertexRepr(feature,"A"));
        EfpEvalResult log = new EfpEvalResult("A", feature, MatchingResult.MISSING,
                AssignmentSide.PROVIDED);
        assertTrue(result.equals(log));
    }

}
