package cz.zcu.kiv.efps.comparator.api.impl;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.api.ComponentEvaluator;
import cz.zcu.kiv.efps.comparator.api.ComponentLoader;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.ComponentEvaluatorImp;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;


/**
 * First test of evaluation
 *
 * @author Lukas Vlcek
 *
 */
public class ComponentEvaluatorImpTest {

    /**
     * List with results.
     */
    static List<EfpEvalResult> compareLog;

    static Feature compFeature = new BasicFeature(
            "Component-A", AssignmentSide.PROVIDED, "component", true);

    /**
     * Create result for components A, B and C.
     */
    @BeforeClass
    public static void createCompareResult() {
        compareLog = new ArrayList<EfpEvalResult>();
        resultAB();
        resultBC();
        resultC();


    }

    /**
     * Create result for connection between components A and B.
     */
    static void resultAB() {
        EfpEvalResult log2 = new EfpEvalResult("B", "A", new BasicFeature("f-1",
                AssignmentSide.PROVIDED, "component", true, compFeature),
                new SimpleEFP("efp-1-3", null, null, null, null), MatchingResult.OK, new EfpNumber(8),new EfpNumber(4));

        EfpEvalResult log3 = new EfpEvalResult("B", "A", new BasicFeature("f-1",
                AssignmentSide.PROVIDED, "component", true, compFeature),
                new SimpleEFP("efp-1-1", null, null, null, null), MatchingResult.OK, new EfpNumber(4), new EfpNumber(3));

        EfpEvalResult log4 = new EfpEvalResult("A", "B", new BasicFeature("f-1",
                AssignmentSide.REQUIRED, "component", true, compFeature),
                new SimpleEFP("efp-1-2", null, null, null, null), MatchingResult.MISSING,
                AssignmentSide.PROVIDED);

        EfpEvalResult log5 = new EfpEvalResult("A", "B", new BasicFeature("f-2",
                AssignmentSide.REQUIRED, "component", true, compFeature), MatchingResult.OK);
        compareLog.add(log2);
        compareLog.add(log3);
        compareLog.add(log4);
        compareLog.add(log5);
    }

    /**
     * Create result for connection between components B and C.
     */
    static void resultBC() {
        EfpEvalResult log2 = new EfpEvalResult("C", "B", new BasicFeature("f-4",
                AssignmentSide.PROVIDED, "component", true),
                new SimpleEFP("efp-4-2", null, null, null, null), MatchingResult.OK, new EfpNumber(10),
                new EfpNumber(10));

        EfpEvalResult log3 = new EfpEvalResult("C", "B", new BasicFeature("f-4",
                AssignmentSide.PROVIDED, "component", true),
                new SimpleEFP("efp-4-1", null, null, null, null), MatchingResult.OK, new EfpNumber(4), null);
        compareLog.add(log2);
        compareLog.add(log3);
    }

    /**
     * Create result for component C.
     */
    static void resultC() {
        EfpEvalResult log2 = new EfpEvalResult("C", new BasicFeature("f-6",
                AssignmentSide.REQUIRED, "component", true), MatchingResult.MISSING,
                AssignmentSide.PROVIDED);
        compareLog.add(log2);
    }

    @Test
    public void testEvaluate2() {
        ComponentLoader loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C"),
                "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        DirectedGraph<GraphVertexRepr, DefaultEdge> graph = creator.create();
        ComponentEvaluator evaluator = new ComponentEvaluatorImp();
        List<EfpEvalResult> results = evaluator.evaluate(graph);
        assertTrue(results.equals(compareLog));
    }
}
