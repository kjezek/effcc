package cz.zcu.kiv.efps.comparator.api.impl.evaluation;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.RelatedComponentsAssignments;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class VertexNeighborFinderTest {

    static DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    static VertexNeighborFinder finder;

    @BeforeClass
    public static void createGraph() {
        ComponentLoaderImp loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C"),
        "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        DirectedGraph<GraphVertexRepr, DefaultEdge> graph = creator.create();
        finder = new VertexNeighborFinder(graph);
    }

    @Test
    public void testgetMatchEfp() {
        Feature feature1 = new BasicFeature("f-1",
                AssignmentSide.PROVIDED, "component", true);
        EFP efp11 = new SimpleEFP("efp-1-1", null, null, null, null);
        GraphVertexRepr efpVertex = new GraphVertexRepr(efp11,  new EfpDirectValue(
                new EfpNumber(4)), new GraphVertexRepr(feature1,"B"));
        GraphVertexRepr neighbor = finder.getMatchEfp(efpVertex);
        EFP efp11N = new SimpleEFP("efp-1-1", null, null, null, null);
        Feature compFeature = new BasicFeature(
                "Component-A", AssignmentSide.PROVIDED, "component", true);
        Feature feature = new BasicFeature("f-1",
                AssignmentSide.REQUIRED, "component", true, compFeature);
        GraphVertexRepr efpNeighbor = new GraphVertexRepr(efp11N, new EfpDirectValue(
                new EfpNumber(3)), new GraphVertexRepr(feature,"A"));
        efpNeighbor.getAssignmentValue().setRelatedValuesCallback(new RelatedComponentsAssignments(
                graph,"B"));
        assertTrue(neighbor.equals(efpNeighbor));
    }

    @Test
    public void testgetMatchedFeature() {
        Feature feature1 = new BasicFeature("f-1",
                AssignmentSide.PROVIDED, "component", true);
        GraphVertexRepr feature = finder.getMatchedFeature(new GraphVertexRepr(feature1, "B"));
        Feature compFeature = new BasicFeature(
                "Component-A", AssignmentSide.PROVIDED, "component", true);
        GraphVertexRepr featureMatch = new GraphVertexRepr(new BasicFeature("f-1",
                AssignmentSide.REQUIRED, "component", true, compFeature),"A");
        assertTrue(feature.equals(featureMatch));
    }

    @Test
    public void testfindComponentByFeature() {
        Feature feature1 = new BasicFeature("f-1",
                AssignmentSide.PROVIDED, "component", true);
        String component = finder.findComponentByFeature(new GraphVertexRepr(feature1,"B"));
        assertTrue(component.equals("B"));
    }
}
