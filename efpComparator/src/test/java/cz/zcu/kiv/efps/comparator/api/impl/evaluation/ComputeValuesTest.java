package cz.zcu.kiv.efps.comparator.api.impl.evaluation;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.RelatedComponentsAssignments;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.result.EfpResult;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class ComputeValuesTest {

    static EfpResult efpResult;

    static DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    private Feature compFeature = new BasicFeature(
            "Component-A", AssignmentSide.PROVIDED, "component", true);
    private Feature feature = new BasicFeature("f-1",
            AssignmentSide.REQUIRED, "component", true, compFeature);
    private Feature feature1 = new BasicFeature("f-1",
            AssignmentSide.PROVIDED, "component", true);


    @BeforeClass
    public static void createGraph() {
        ComponentLoaderImp loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                Arrays.asList("A", "B", "C", "D", "E"),
        "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl", -1);
        graph = creator.create();
        VertexNeighborFinder finder = new VertexNeighborFinder(graph);
        efpResult = new EfpResult(finder);
    }

    @Test
    public void computeMathWithNullValue() {

        Feature feature7 = new BasicFeature("f-7",
                AssignmentSide.PROVIDED, "component", true);
       Feature feature8 = new BasicFeature("f-8",
                AssignmentSide.REQUIRED, "component", true);
       EFP efp81 = new SimpleEFP("efp-8-1", null, null, null, null);
       EFP efp71 = new SimpleEFP("efp-7-1", null, null, null, null);
       EfpAssignedValue val7 = new EfpFormulaValue("2 * component.f-8_efp-8-1",
               new EfpFeature(efp81, feature8));
       val7.setRelatedValuesCallback(new RelatedComponentsAssignments(graph, "D"));

        GraphVertexRepr mathFormule = new GraphVertexRepr(efp71, val7, new GraphVertexRepr(feature7, "D"));
       efpResult.computeValue(mathFormule);
       assertTrue(mathFormule.getValue() == null);

    }

    /**
     * Test vertexes with direct value.
     */
    @Test
    public void computeValues() {
        GraphVertexRepr featureReq = new GraphVertexRepr(feature,"A");
        GraphVertexRepr featureProv = new GraphVertexRepr(feature1,"A");
        EFP efp11 = new SimpleEFP("efp-1-1", null, null, null, null);

        GraphVertexRepr efpProv = new GraphVertexRepr(efp11, new EfpDirectValue(
                new EfpNumber(9)), featureProv);
        GraphVertexRepr efpReq = new GraphVertexRepr(efp11, new EfpDirectValue(
                new EfpNumber(7)), featureReq);
        assertTrue(efpResult.compareValue(efpProv, efpReq) == 2);
    }

    /**
     * Test math value.
     */
    @Test
    public void computeMathValues() {
        EFP efp13 = new SimpleEFP("efp-1-3", null, null, null, null);
        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);

        Feature feature4 = new BasicFeature("f-4",
                AssignmentSide.REQUIRED, "component", true);

        GraphVertexRepr efpProv = new GraphVertexRepr(efp13, new EfpFormulaValue(
                "2 * component.f-4_efp-4-1", new EfpFeature(efp41, feature4)),
                new GraphVertexRepr(feature1, "A"));

        efpProv.getAssignmentValue().setRelatedValuesCallback(
                new RelatedComponentsAssignments(graph, "B"));

        GraphVertexRepr efpReq = new GraphVertexRepr(efp13, new EfpDirectValue(
                new EfpNumber(4)), new GraphVertexRepr(feature, "A"));

        assertTrue(efpResult.compareValue(efpProv, efpReq) == 4);
    }


    /**
     * Vertexes with LR value.
     */
    @Test
    public void computeLRValues() {
        LR lr = new LR("LR 1", null, null, "LR1");
        EFP efp42 = new SimpleEFP("efp-4-2", null, null, null, null);
        GraphVertexRepr efpProv = new GraphVertexRepr(efp42, new EfpNamedValue(
                new LrSimpleAssignment(efp42, "low", new EfpNumber(10), lr)),
                new GraphVertexRepr(feature1,"A"));

        GraphVertexRepr efpReq = new GraphVertexRepr(efp42, new EfpNamedValue(
                new LrSimpleAssignment(efp42, "low", new EfpNumber(9), lr)),
                new GraphVertexRepr(feature,"A"));

        assertTrue(efpResult.compareValue(efpProv, efpReq) == 1);
    }

    /**
     * Vertexes without value.
     */
    @Test
    public void computeWithNoValue() {
        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);

        GraphVertexRepr efpProv = new GraphVertexRepr(efp41, null,
                new GraphVertexRepr(feature1,"A"));

        GraphVertexRepr efpReq = new GraphVertexRepr(efp41, null,
                new GraphVertexRepr(feature,"A"));

        assertTrue(efpResult.compareValue(efpProv, efpReq) == 0);
    }

    /**
     * Only required side have value.
     */
    @Test
    public void computeWithReqValue() {
        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);

        GraphVertexRepr efpProv = new GraphVertexRepr(efp41, null,
                new GraphVertexRepr(feature1,"A"));

        GraphVertexRepr efpReq = new GraphVertexRepr(efp41, new EfpDirectValue(
                new EfpNumber(4)), new GraphVertexRepr(feature,"A"));

        assertTrue(efpResult.compareValue(efpProv, efpReq) == -1);
    }

    /**
     * Only provided side have value.
     */
    @Test
    public void computeWithProvValue() {
        EFP efp41 = new SimpleEFP("efp-4-1", null, null, null, null);
        GraphVertexRepr efpProv = new GraphVertexRepr(efp41, new EfpDirectValue(
                new EfpNumber(4)), new GraphVertexRepr(feature1,"A"));

        GraphVertexRepr efpReq = new GraphVertexRepr(efp41, null,
                new GraphVertexRepr(feature,"A"));

        assertTrue(efpResult.compareValue(efpProv, efpReq) == 1);
    }


}
