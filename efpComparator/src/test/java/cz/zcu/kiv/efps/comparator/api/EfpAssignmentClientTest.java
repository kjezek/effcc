package cz.zcu.kiv.efps.comparator.api;

import static org.junit.Assert.assertNotNull;
import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.client.EfpAssignmentClient;

/**
*
* This is a junit test for EfpAssignmentClient.
* This test was generated at
* @author
*/

public class EfpAssignmentClientTest  {

    /**
     * @see cz.zcu.kiv.efps.comparator.api.EfpAssignmentClient#initialize.
     */
    @Test
    public void testInitialize() {

        EfpAwareComponentLoader result = EfpAssignmentClient.initialiseComponentLoader(
                "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl");

        assertNotNull(result);

        String className = "cz.zcu.kiv.efps.assignment.stub.StubAssignmentImpl";
        Assert.assertEquals(className, result.getClass().getName());
    }




}
