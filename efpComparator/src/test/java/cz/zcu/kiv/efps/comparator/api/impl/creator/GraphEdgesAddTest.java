package cz.zcu.kiv.efps.comparator.api.impl.creator;

import static org.junit.Assert.assertTrue;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cz.zcu.kiv.efps.assignment.types.BasicFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.comparator.core.graph.GraphStructureCreator;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;



public class GraphEdgesAddTest {

    static DirectedGraph<GraphVertexRepr, DefaultEdge> graph;
    static GraphVertexRepr comp;
    static Feature compFeature;
    static Feature vertexFeatureR;
    static Feature vertexFeatureP;
    static EfpAssignedValue value;
    static GraphVertexRepr efp;
    static GraphStructureCreator edgeAdd;

    @Before
    public  void init() {
         graph =   new DefaultDirectedGraph<GraphVertexRepr,
        DefaultEdge>(DefaultEdge.class);
        compFeature = new BasicFeature("Component-A", AssignmentSide.PROVIDED, "component", true);
        vertexFeatureR = new BasicFeature("f-1",
                AssignmentSide.REQUIRED, "component", true, compFeature);
        vertexFeatureP = new BasicFeature(
                "f-1", AssignmentSide.PROVIDED, "component", true);
        efp = new GraphVertexRepr(new SimpleEFP("efp-1-1", null,
                null, null, null), value, new GraphVertexRepr(vertexFeatureR, "A"));
        edgeAdd = new GraphStructureCreator(graph);
    }
    @Test
    public void addEdgeCompFeature() {
        edgeAdd.addEdgeCompFeature("A", compFeature);
        assertTrue(graph.containsEdge(new GraphVertexRepr("A"), new GraphVertexRepr(compFeature, "A")));
    }

    @Test
    public void addEdgeFeatureFeature() {
        edgeAdd.addEdgeFeatureFeature(vertexFeatureR, vertexFeatureP, "A", "B");
        assertTrue(graph.containsEdge(new GraphVertexRepr(vertexFeatureP, "B"),
                new GraphVertexRepr(vertexFeatureR, "A")));
    }

    @Test
    public void addEdgeFeatureEfp() {
        edgeAdd.addEdgeCompFeature("A", compFeature);
        edgeAdd.addEdgeFeatureEfp(new GraphVertexRepr(compFeature, "A"), efp);
        assertTrue(graph.containsEdge(new GraphVertexRepr(compFeature, "A"), efp));
    }

    @Test
    public void addEdgeEfpEfpTest() {
        GraphVertexRepr efpSecond = new GraphVertexRepr(new SimpleEFP("efp-1-1", null,
                null, null, null), value, new GraphVertexRepr(vertexFeatureP, "A"));

        edgeAdd.addEdgeCompFeature("A", compFeature);
        edgeAdd.addEdgeFeatureEfp(new GraphVertexRepr(compFeature, "A"), efp);
        edgeAdd.addEdgeEfpEfp(efp, efpSecond);
        assertTrue(graph.containsEdge(efpSecond, efp));
    }
}
