package cz.zcu.kiv.efps.comparator.core.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.ComponentAssignments;
import cz.zcu.kiv.efps.comparator.core.RelatedComponentsAssignments;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Class for creating graph.
 *
 * @author Lukas Vlcek <vlceklu@students.zcu.cz/>
 *
 */
public class GraphCreatorImp implements GraphCreator {

    /**
     * Logger for make logs.
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * List with data for creating graph.
     */
    private List<ComponentAssignments> efpAssignments;

    /**
     * Local register ID.
     */
    private Integer localRegistryID;


    /** A matching function. */
    private MatchingFunction matchingFunction;


    /**
     * Constructor, take list ComponentAssignments for creating graph.
     *
     * @param efpAssignments list of ComparatorData
     * @param localRegistryID local registry ID
     * @param  matchingFunction a current matching function
     */
    public GraphCreatorImp(
            final List<ComponentAssignments> efpAssignments,
            final Integer localRegistryID,
            final MatchingFunction matchingFunction) {

        this.efpAssignments = efpAssignments;
        this.localRegistryID = localRegistryID;
        this.matchingFunction = matchingFunction;
    }



    @Override
    public final DirectedGraph<GraphVertexRepr, DefaultEdge> create() {

        logger.info("Start creating a graph");

        DirectedGraph<GraphVertexRepr, DefaultEdge> graph =
                new DefaultDirectedGraph<GraphVertexRepr, DefaultEdge>(
                        DefaultEdge.class);

        /*
      Class for adding edges.
     */
        GraphStructureCreator graphStructure = new GraphStructureCreator(graph);

        for (int i = 0; i < efpAssignments.size(); i++) {
            List<Feature> listAssignments = efpAssignments.get(i).getAccesor().getAllFeatures();
            for (Feature feature : listAssignments) {
                graphStructure.addEdgeCompFeature(efpAssignments.get(i).getCompName(), feature);
                Map<Feature, Integer> matchFeature = matchFeatures(feature, i, matchingFunction);
                Set<Entry<Feature, Integer>> set = matchFeature.entrySet();
                for (Entry<Feature, Integer> entry : set) {
                    graphStructure.addEdgeFeatureFeature(feature, entry.getKey(),
                            efpAssignments.get(i).getCompName(),
                            efpAssignments.get(entry.getValue()).getCompName());
                    List<GraphVertexRepr> vertexsEfp = addFeatureEfp(
                            graph, graphStructure, feature, i);
                    addEfpEfp(graph, graphStructure, vertexsEfp,
                            entry.getKey(), entry.getValue());
                }
                if (matchFeature.isEmpty()) {
                    addFeatureEfp(graph, graphStructure, feature, i);
                }
            }

            efpAssignments.get(i).getAccesor().close();
        }
        logger.info("Graph is created");
        return graph;
    }


    /**
     * Method for make edge between tho efps vertexes.
     * @param graph a current graph
     * @param graphStructure a graph structure
     * @param vertexesEfp list with efps vertexes
     * @param matchFeature feature match with first feature
     * @param matchFeatureIndex index of match feature
     */
    private void addEfpEfp(
            final DirectedGraph<GraphVertexRepr, DefaultEdge> graph,
            final GraphStructureCreator graphStructure,
            final List<GraphVertexRepr> vertexesEfp,
            final Feature matchFeature, final int matchFeatureIndex) {

        ComponentEfpAccessor accesor = efpAssignments.get(matchFeatureIndex).getAccesor();
        List<EFP> listEfp = accesor.getEfps(matchFeature);

        for (GraphVertexRepr efp : vertexesEfp) {
            for (EFP efpMatch : listEfp) {
               if (efp.getEfp().equals(efpMatch)) {
                   EfpAssignedValue assigValue = accesor.getAssignedValue(matchFeature, efpMatch,
                           findLR(accesor.getLRs()));
                   if (assigValue == null) {
                       assigValue = accesor.getAssignedValue(matchFeature, efpMatch, null);
                   }
                   GraphVertexRepr vertexEfp = new GraphVertexRepr(efpMatch, assigValue,
                           new GraphVertexRepr(matchFeature,
                                   efpAssignments.get(matchFeatureIndex).getCompName()));
                   if (vertexEfp.getAssignmentValue() != null) {
                       setCallBack(graph, vertexEfp,
                               efpAssignments.get(matchFeatureIndex).getCompName());
                   }
                   graphStructure.addEdgeEfpEfp(efp, vertexEfp);
               }
            }
        }
    }

    /**
     * Set callback for efps.
     * @param graph current graph.
     * @param efp efp
     * @param component component
     */
    public void setCallBack(
            final DirectedGraph<GraphVertexRepr, DefaultEdge> graph,
            final GraphVertexRepr efp,
            final String component) {

        efp.getAssignmentValue().setRelatedValuesCallback(
                new RelatedComponentsAssignments(graph, component));
    }

    /**
     * Add connection between feature and EFP and return list with EFPs.
     * @param graph a current graph
     * @param graphStructure a graph structure creator.
     * @param feature feature
     * @param index index in list
     * @return list with creating efp vertexes
     */
    private List<GraphVertexRepr> addFeatureEfp(
            final DirectedGraph<GraphVertexRepr, DefaultEdge> graph,
            final GraphStructureCreator graphStructure,
            final Feature feature, final int index) {

        List<GraphVertexRepr> efpVertexs = new ArrayList<GraphVertexRepr>();
        ComponentEfpAccessor accesor = efpAssignments.get(index).getAccesor();
        List<EFP> listEfp = accesor.getEfps(feature);
        GraphVertexRepr featureVertex = new GraphVertexRepr(feature,
                efpAssignments.get(index).getCompName());
        for (EFP efp : listEfp) {
            EfpAssignedValue assigValue = accesor.getAssignedValue(feature, efp,
                    findLR(accesor.getLRs()));
            if (assigValue == null) {
                assigValue = accesor.getAssignedValue(feature, efp, null);
            }
            GraphVertexRepr vertexEfp = new GraphVertexRepr(efp, assigValue,
                featureVertex);

            if (vertexEfp.getAssignmentValue() != null) {
                setCallBack(graph, vertexEfp,
                        efpAssignments.get(index).getCompName());
            }
            efpVertexs.add(vertexEfp);
            graphStructure.addEdgeFeatureEfp(featureVertex, vertexEfp);
        }
        return efpVertexs;
    }

    /**
     * Find LR with ID.
     * @param localRegistry set with registers.
     * @return Local register
     */
    public LR findLR(final Set<LR> localRegistry) {
        for (LR localRegister : localRegistry) {
            if (localRegister.getId() != null) {
                if (localRegister.getId().equals(localRegistryID)) {
                    return localRegister;
            }
        }
        }
        return null;
    }


     /**
     * Method for finding matches features. On input are feature and index of
     * comparing component in list.
     * @param feature a feature to match
     * @param index index of component
     * @param matchingFunction matching function
     * @return EfpAssignment with matches feature, if we dont find equal
     *         Feature, return null
     */
    public Map<Feature, Integer> matchFeatures(
            final Feature feature,
            final int index,
            final MatchingFunction matchingFunction) {

        Map<Feature, Integer> features = new HashMap<Feature, Integer>();
        for (int j = index + 1; j < efpAssignments.size(); j++) {
            for (Feature featureMatch : efpAssignments.get(j)
                       .getAccesor().getAllFeatures()) {

                   if (matchingFunction.matches(feature, featureMatch)) {
                       features.put(featureMatch, j);
                   }
               }
        }
        return features;
    }
}
