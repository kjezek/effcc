package cz.zcu.kiv.efps.comparator.standalone;

import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.jgraph.JGraph;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;

import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.JGraphLayout;
import com.jgraph.layout.graph.JGraphSimpleLayout;

import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;

/**
 * GUI to show graph.
 *
 * @author Lukas Vlcek
 *
 */
public class GraphFrame extends JFrame {

    /**
     * UID.
     */
    private static final long serialVersionUID = 2099575689075832430L;

    /**
     * Graph.
     */
    private DirectedGraph<GraphVertexRepr, DefaultEdge> graph;


    /**
     * Constructor.
     * @param graph graph
     */
    public GraphFrame(final DirectedGraph<GraphVertexRepr, DefaultEdge> graph)  {
        super();
        this.graph = graph;
        showGraph();
        setWindow();
    }

    /**
     * Set windows settings.
     */
    private void setWindow() {
        this.setTitle("Graph");
        this.setSize(700, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * Method for show graph.
     */
    private void showGraph() {
        JGraphModelAdapter<GraphVertexRepr, DefaultEdge> adapter = new
                JGraphModelAdapter<GraphVertexRepr, DefaultEdge>(graph);
        JGraph jgraph = new JGraph(adapter);
        jgraph.setEdgeLabelsMovable(false);
        JGraphFacade facade = new JGraphFacade(jgraph);
        JGraphLayout layout = new JGraphSimpleLayout(
                JGraphSimpleLayout.TYPE_CIRCLE); //type change position of components in graph
        layout.run(facade);
        @SuppressWarnings("unchecked")
        Map<GraphVertexRepr, DefaultEdge> nested = facade.createNestedMap(true, true);
        jgraph.getGraphLayoutCache().edit(nested);

        this.getContentPane().add(new JScrollPane(jgraph));
    }





}
