package cz.zcu.kiv.efps.comparator.core;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
/**
*
* Class representing data for comparator.
* @author Lukas Vlcek <vlceklu@students.zcu.cz>
*
*/
public class ComponentAssignments {

    /**
     * Accesor to efpData.
     */
    private ComponentEfpAccessor efpData;


    /**
     * Name of component.
     */
    private String componentName;

    /**
     * Constructor.
     * @param componentName component name
     * @param efpData accesor
     */
    public ComponentAssignments(
            final ComponentEfpAccessor efpData,
            final String componentName
           ) {
        this.efpData = efpData;
        this.componentName = componentName;
    }

    /**
     *
     * @return list of efpassignments
     */
    public ComponentEfpAccessor getAccesor() {
        return efpData;
    }

    /**
     *
     * @return name of component
     */
    public String getCompName() {
        return componentName;
    }



}
