package cz.zcu.kiv.efps.comparator.standalone;


import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  This class opens a desktop application
 *  showing a topology of a graph generated
 *  by the evaluator.
 *
 * @author Kamil Jezek
 * @author Lukas Vlcek
 *
 */
public final class EvaluatorViewer {

    /** It is public. */
    private EvaluatorViewer() { }

    /**
     * @param args arguments
     */
    public static void main(final String[] args) {
        Logger logger = LoggerFactory.getLogger(EvaluatorViewer.class);
        logger.info("Creating gui for showing graph.");
        ComponentLoaderImp loader = new ComponentLoaderImp();
        if (args.length != 2) {
            System.out.println("Start with parameters - <Path to bundles> <LR ID>");
            return;
        }
        GraphCreator creator = loader.loadComponents(
                StandAloneEvaluator.loadBundles(args[0]),
                "cz.zcu.kiv.efps.assignment.cosi.CosiAssignmentImpl",
                Integer.valueOf(args[1]));
       DirectedGraph<GraphVertexRepr, DefaultEdge> graph = creator.create();
       new GraphFrame(graph);

    }

}
