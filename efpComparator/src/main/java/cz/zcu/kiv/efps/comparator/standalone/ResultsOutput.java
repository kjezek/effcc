package cz.zcu.kiv.efps.comparator.standalone;

import java.io.File;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;

/**
 * Class for make output of comparator.
 * @author Lukas Vlcek
 *
 */
public class ResultsOutput {


    /**
     * Method for write the results out.
     * @param results List with results.
     */
    public void writeOut(final List<EfpEvalResult> results) {
        Collections.sort(results, new sortList());
        String firstComponent = "";
        String secondComponent = "";
        String featureName = "";

        for (EfpEvalResult log : results) {
            if ((!log.getFirstComponent().equals(firstComponent)
                    || !log.getSecondComponent().equals(secondComponent))
                        && (!log.getSecondComponent().equals(firstComponent)
                            || !log.getFirstComponent().equals(secondComponent))) {
               firstComponent = log.getFirstComponent();
               secondComponent = log.getSecondComponent();
               writeCompInfo(firstComponent, secondComponent);
            }
            if (!log.getFeature().getIdentifier().equals(featureName)) {
                featureName = log.getFeature().getIdentifier();
                writeFeatInfo(featureName);
            }
            System.out.println(efpInfo(log));
        }
    }

    /**
     * Method for write out the info about EFP.
     * @param log log
     * @return string
     */
    private String efpInfo(final EfpEvalResult log) {
       StringBuffer buffer = new StringBuffer();
       if (log.getEfp() == null && log.getTypeError().equals(MatchingResult.MISSING)) {
           buffer.append(String.format("%-30s",   "         " + log.getTypeError() + " Feature on "
                   + log.getSide() + " side"));
           buffer.append("\n");
           return buffer.toString();
       }
       if (log.getEfp() == null) {
           buffer.append(String.format("%-30s",   "         Feature are CONNECTED"));
           buffer.append("\n");
           return buffer.toString();
       }
       if (log.getTypeError().equals(MatchingResult.MISSING)) {
           buffer.append(String.format("         " + log.getTypeError()));
           buffer.append(String.format(" EFP " + log.getEfp().getName()));
           buffer.append(String.format(" on " + log.getSide() + " side."));
           buffer.append("\n");
           return buffer.toString();
       }

       buffer.append(String.format("         EFPs " + log.getEfp().getName()));
       buffer.append(String.format(" Evaluation : " + log.getTypeError()));
       buffer.append(String.format(" - Value on " + compName(log.getFirstComponent()) + ": "
               + log.getFirstEfpValue() + ", "
               + "Value on " + compName(log.getSecondComponent())) + ": "
               + log.getSecondEfpValue());
       buffer.append("\n");
       return buffer.toString();
    }

    /**
     * Make short name of component from path.
     * @param fullName name with path
     * @return name
     */
    private String compName(final String fullName) {
        int lastSep = fullName.lastIndexOf(File.separator);
        return fullName.substring(lastSep + 1);
    }

    /**
     * Write header for feature.
     * @param feature name of feature
     */
    private void writeFeatInfo(final String feature) {
        System.out.println("    Feature : " + feature);
        System.out.println("");
    }
    /**
     * Method for write the header with name of component.
     * @param firstComponent first component
     * @param secondComponent second component
     */
    private void writeCompInfo(final String firstComponent, final String secondComponent) {
        System.out.println("");
        if (secondComponent.equals("")) {
            System.out.println("Component : " + firstComponent);
        } else {
            System.out.println("Components : " + firstComponent + " and " + secondComponent);
        }
        System.out.println("");
    }

    /**
     * Class for sorting the list.
     * @author Lukas Vlcek
     *
     */
    class sortList implements Comparator<EfpEvalResult> {
        /**
         * Set Collator to english.
         */
        private Collator engCol = Collator.getInstance(Locale.ENGLISH);
        @Override
        public int compare(final EfpEvalResult o1, final EfpEvalResult o2) {
            String firstComponento1 = o1.getFirstComponent();
            String firstComponento2 = o2.getFirstComponent();
            String secondComponento1 = o1.getSecondComponent();
            String secondComponento2 = o2.getSecondComponent();

            int helpCompare = engCol.compare(firstComponento1, secondComponento1);
            if (helpCompare < 0) {
                firstComponento1 = secondComponento1;
                secondComponento1 = o1.getFirstComponent();
            }
            helpCompare = engCol.compare(firstComponento2, secondComponento2);
            if (helpCompare < 0) {
                firstComponento2 = secondComponento2;
                secondComponento2 = o2.getFirstComponent();
            }
            int compare = engCol.compare(firstComponento1, firstComponento2);

            if (compare == 0) {
                compare = engCol.compare(secondComponento1, secondComponento2);
            }
            return compare;
        }

    }

}
