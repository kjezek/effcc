package cz.zcu.kiv.efps.comparator.result;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;


/**
 * Class for creating results of efps vertexes in graph.
 * @author Lukas Vlcek
 *
 */
public class EfpResult {

       /**
     * Finder for find neighbor of vertex.
     */
    private VertexNeighborFinder finder;

    /**
     * Logger.
     */
    private Logger logger;

    /**
     * Constructor.
     * @param finder for find neighbors in graph.
     */
    public EfpResult(final VertexNeighborFinder finder) {
        this.finder = finder;
        logger = LoggerFactory.getLogger(EfpResult.class);
    }


    /**
     * Method for compute value.
     * @param efp efp vertex
     * @param matchEfp second vertex
     * @return -1 if incompatible, 0 or 1 if compatible
     */
    public int compareValue(final GraphVertexRepr efp, final GraphVertexRepr matchEfp) {
        if (efp.getAssignmentValue() == null && matchEfp.getAssignmentValue() == null) {
            return 0;
        }
        if (efp.getAssignmentValue() == null) {
            if (efp.getConnectFeature().getFeature().getSide().equals(AssignmentSide.REQUIRED)) {
                return 1;
            } else {
                return -1;
            }
        }
        if (matchEfp.getAssignmentValue() == null) {
            if (matchEfp.getConnectFeature().getFeature().getSide().equals(
                    AssignmentSide.REQUIRED)) {
                return 1;
            } else {
                return -1;
            }
        }
        computeValue(efp);
        computeValue(matchEfp);
        if (efp.getValue() == null || matchEfp.getValue() == null) {
            return computeWithNullValue(efp, matchEfp);
        }
        if (efp.getConnectFeature().getFeature().getSide().equals(AssignmentSide.PROVIDED)) {
            return efp.getValue().compareTo(matchEfp.getValue());
        }
        return matchEfp.getValue().compareTo(efp.getValue());
    }


    /**
     * Method for case, if the compute value from math formula is null.
     * @param efp first efp vertex
     * @param matchEfp second efp vertex
     * @return result of compare
     */
    private int computeWithNullValue(final GraphVertexRepr efp, final GraphVertexRepr matchEfp) {
        if (efp.getValue() == null && matchEfp.getValue() == null) {
            return 0;
        }
        if (efp.getValue() == null) {
            if (efp.getConnectFeature().getFeature().getSide().equals(AssignmentSide.REQUIRED)) {
                return 1;
            } else {
                return -1;
            }
        }
        if (matchEfp.getConnectFeature().getFeature().getSide().equals(AssignmentSide.REQUIRED)) {
                return 1;
            } else {
                return -1;
            }

    }

    /**
     * Computing value assignment on EFP.
     * @param efp vertex representing EFP
     */
    public void computeValue(final GraphVertexRepr efp) {
            efp.setValue(efp.getAssignmentValue().computeValue());
    }


    /**
     * Method return name of component, to with is connect efp.
     * @param efp efp
     * @return name of component
     */
    private String getConnectComponent(final GraphVertexRepr efp) {
        GraphVertexRepr connectFeature = finder.getMatchedFeature(efp.getConnectFeature());
        if (connectFeature == null) {
            return "";
        } else {
            return finder.findComponentByFeature(connectFeature);
        }
    }


    /**
     * Make EvaluateLog if on EFP is missing.
     * @param vertex efp
     * @return EvaluateLog
     */
    public EfpEvalResult noMatchEfpResult(final GraphVertexRepr vertex) {
        logger.debug("EFP without neighbor : " + vertex.getEfp().getName());
        if (vertex.getConnectFeature().getFeature().getSide().equals(AssignmentSide.PROVIDED)
                && getConnectComponent(vertex).equals("")) {
            return null;
        }
        EfpEvalResult log = new EfpEvalResult(finder.findComponentByFeature(vertex.getConnectFeature()),
               getConnectComponent(vertex), vertex.getConnectFeature().getFeature(),
               vertex.getEfp(), MatchingResult.MISSING,
               invertSide(vertex.getConnectFeature().getFeature().getSide()));
        return log;
    }

    /**
     * Make MatchResult by compare number.
     * @param vertex first vertex
     * @param matchEfp second vertex
     * @return result
     */
    private MatchingResult getResult(final GraphVertexRepr vertex,
            final GraphVertexRepr matchEfp) {
        logger.debug("Compare value of : " + vertex.getEfp().toString()
                + " and " + matchEfp.getEfp().toString());
        int compare = compareValue(vertex, matchEfp);
        if (compare >= 0) {
            return MatchingResult.OK;
        } else {
            return MatchingResult.INCOMPATIBLE_VALUES;
        }

    }

    /**
     * Make EvaluateLog for two match vertexes.
     * @param vertex first efp vertex
     * @param matchEfp second efp vertex
     * @return EvaluateLog
     */
    public EfpEvalResult matchEfpResult(final GraphVertexRepr vertex,
            final GraphVertexRepr matchEfp) {
        MatchingResult result = getResult(vertex, matchEfp);
        if (vertex.getValue() == null && vertex.getAssignmentValue() != null) {
            computeValue(vertex);
        }
        if (matchEfp.getValue() == null && matchEfp.getAssignmentValue() != null) {
           computeValue(matchEfp);
        }
        EfpEvalResult log = new EfpEvalResult(finder.findComponentByFeature(vertex.getConnectFeature()),
                finder.findComponentByFeature(matchEfp.getConnectFeature()),
                vertex.getConnectFeature().getFeature(), vertex.getEfp(), result,
                vertex.getValue(), matchEfp.getValue());
        return log;
    }

    /**
     * Reverse to the side, where is missing feature.
     * @param side side
     * @return reverse side
     */
    private AssignmentSide invertSide(final AssignmentSide side) {
        if (side.equals(AssignmentSide.PROVIDED)) {
            return AssignmentSide.REQUIRED;
        }
        return AssignmentSide.PROVIDED;
    }



}
