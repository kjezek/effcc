package cz.zcu.kiv.efps.comparator.core;

import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.evaluator.RelatedAssignmentsFinder;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr.VertexType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This class is used for finding
 * assignments connected to a component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class RelatedComponentsAssignments
    implements RelatedAssignmentsFinder {

   /**
     * Graph, where we will find component.
     */
    private DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    /**
     * Component.
     */
    private String component;
    /**
     * Logger.
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * @param graph graph where we found connecting efp.
     * @param component name of component
     */
    public RelatedComponentsAssignments(final DirectedGraph<GraphVertexRepr, DefaultEdge> graph,
            final String component) {
        this.graph = graph;
        this.component = component;
    }


    /** {@inheritDoc}  */
    @Override
    public EfpAssignedValue getConnectedAssignments(
            final Feature requiredFeature,
            final EFP requiredEfp) {

        GraphVertexRepr feature = new GraphVertexRepr(requiredFeature, component);
        Set<DefaultEdge> edges = graph.incomingEdgesOf(feature);
        for (DefaultEdge edge : edges) {
            GraphVertexRepr vertex = graph.getEdgeSource(edge);
            if (vertex.getVertexType().equals(VertexType.EFP)) {
                if (vertex.getEfp().equals(requiredEfp)) {

                    // if this required EFP has already assigned a value,
                    // use this value (this value has priority over an connected one).
                    if (vertex.getAssignmentValue() != null) {
                        return vertex.getAssignmentValue();
                    }

                    // otherwise, use a value from a connected EFP.
                   Set<DefaultEdge> edgesFromEfp = graph.incomingEdgesOf(vertex);
                    for (DefaultEdge edgeEfp : edgesFromEfp) {
                        GraphVertexRepr vertexEfp = graph.getEdgeSource(edgeEfp);
                        if (vertexEfp.getVertexType().equals(VertexType.EFP)) {
                            logger.debug("Return assignmentValue from : "
                                    + vertexEfp.getEfp().getName());
                            return vertexEfp.getAssignmentValue();
                        }
                    }
                }
            }
        }
        logger.error("Vertex " + requiredEfp.toString() + "does not have a neighbor: " + component);
        throw new IllegalArgumentException("Vertex " + requiredEfp.getName()
                + " does not have neighbor in component: " + component);
    }
}
