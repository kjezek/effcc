package cz.zcu.kiv.efps.comparator.result;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult.MatchingResult;


/**
 * Class for creating results of features vertexes in graph.
 * @author Lukas Vlcek
 *
 */
public class FeatureResult {

    /**
     * Finder for find neighbor of vertex.
     */
    private VertexNeighborFinder finder;

    /**
     * Logger.
     */
    private Logger logger;
    /**
     * Constructor.
     * @param finder for find neighbors in graph.
     */
    public FeatureResult(final VertexNeighborFinder finder) {
        this.finder = finder;
        logger = LoggerFactory.getLogger(FeatureResult.class);
    }

    /**
     * Make evaluateLog if one feature is missing.
     * @param vertex vertex
     * @return log
     */
    public EfpEvalResult addNoMatchFeatureResult(final GraphVertexRepr vertex) {
        logger.debug("Feature without neighbor : " + vertex.getFeature().getIdentifier());
        if (vertex.getFeature().getSide().equals(AssignmentSide.PROVIDED)) {
            return null;
        }
        EfpEvalResult log = new EfpEvalResult(vertex.getConnectCompName(),
                vertex.getFeature(), MatchingResult.MISSING, invertSide(
                        vertex.getFeature().getSide()));
        return log;
    }

    /**
     * Make evaluate log if two feature are connect and have no Efp.
     * @param vertex first vertex
     * @param matchFeature second vertex
     * @return log
     */
    public EfpEvalResult addMatchFeatureResult(final GraphVertexRepr vertex,
            final GraphVertexRepr matchFeature) {
        if (finder.haveEfpNeighbor(vertex) || finder.haveEfpNeighbor(matchFeature)) {
            return null;
        }
        logger.debug("Two matched features : " + vertex.getFeature().getIdentifier() + " and "
                + matchFeature.getFeature().getIdentifier());

        EfpEvalResult log = new EfpEvalResult(vertex.getConnectCompName(),
                matchFeature.getConnectCompName(), vertex.getFeature(), MatchingResult.OK);
        return log;
    }

    /**
     * Reverse to the side, where is missing feature.
     * @param side side
     * @return reverse side
     */
    private AssignmentSide invertSide(final AssignmentSide side) {
        if (side.equals(AssignmentSide.PROVIDED)) {
            return AssignmentSide.REQUIRED;
        }
        return AssignmentSide.PROVIDED;
    }








}
