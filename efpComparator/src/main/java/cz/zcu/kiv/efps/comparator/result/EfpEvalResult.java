package cz.zcu.kiv.efps.comparator.result;


import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Class with errors in evaluating components.
 *
 * @author Lukas Vlcek <vlceklu@students.zcu.cz>
 *
 */
public class EfpEvalResult {

    /**
     *
     * Types of logs.
     *
     */
    public enum MatchingResult {
        /**
         * match is ok.
         */
        OK,
        /**
         * Efps dont match.
         */
        INCOMPATIBLE_VALUES,
        /**
         * Feature is missing.
         */
        MISSING
    };

    /** Name of feature. */
    private Feature feature;

    /**
     * Name of EFP.
     */
    private EFP efp;

    /** Type of error. */
    private MatchingResult typeError;

    /**
     * Assignment side.
     */
    private AssignmentSide side;

    /**
     * Name of first component.
     */
    private String firstComponent;


    /**
     * Computing value of first efp.
     */
    private EfpValueType firstEfpValue;

    /**
     * Computing value of second efp.
     */
    private EfpValueType secondEfpValue;

    /**
     * Name of second component.
     */
    private String secondComponent;

    /**
     * Class giving info what errors was in the evaluating.
     *
     * @param firstComponent name of first component
     * @param feature Feature
     * @param type type of error
     * @param side assignment side
     */
    public EfpEvalResult(final String firstComponent, final Feature feature,
            final MatchingResult type, final AssignmentSide side) {
        super();
        this.firstComponent = firstComponent;
        this.secondComponent = "";
        this.feature = feature;
        this.efp = null;
        this.typeError = type;
        this.side = side;
        this.firstEfpValue = null;
        this.secondEfpValue = null;
    }

    /**
     * Class giving info what errors was in the evaluating.
     *
     * @param firstComponent name of first component
     * @param secondComponent name of second component
     * @param feature Feature
     * @param efp efp
     * @param type type of error
     * @param firstValue computing value of efp
     * @param secondValue computing value of efp
     */
    public EfpEvalResult(final String firstComponent, final String secondComponent,
            final Feature feature, final EFP efp,
            final MatchingResult type, final EfpValueType firstValue,
            final EfpValueType secondValue) {
        super();
        this.firstComponent = firstComponent;
        this.secondComponent = secondComponent;
        this.feature = feature;
        this.efp = efp;
        this.typeError = type;
        this.side = null;
        this.firstEfpValue = firstValue;
        this.secondEfpValue = secondValue;
    }

    /**
     * Class giving info what errors was in the evaluating.
     *
     * @param firstComponent name of first component
     * @param secondComponent name of second component
     * @param feature Feature
     * @param type type of error
     */
    public EfpEvalResult(final String firstComponent, final String secondComponent,
            final Feature feature, final MatchingResult type) {
        this.firstComponent = firstComponent;
        this.secondComponent = secondComponent;
        this.feature = feature;
        this.efp = null;
        this.typeError = type;
        this.side = null;
        this.firstEfpValue = null;
        this.secondEfpValue = null;
    }

    /**
     * Class giving info what errors was in the evaluating.
     *
     * @param firstComponent name of first component
     * @param secondComponent name of second component
     * @param feature Feature
     * @param efp efp
     * @param type type of error
     * @param side assignment side
     */
    public EfpEvalResult(final String firstComponent, final String secondComponent,
            final Feature feature, final EFP efp,
            final MatchingResult type, final AssignmentSide side) {
        this.firstComponent = firstComponent;
        this.secondComponent = secondComponent;
        this.feature = feature;
        this.efp = efp;
        this.typeError = type;
        this.side = side;
        this.firstEfpValue = null;
        this.secondEfpValue = null;
    }

    /**
     * Getter returning name of feature.
     *
     * @return NameOfFeature
     */
    public Feature getFeature() {
        return feature;
    }

    /**
     * Getter returning name of efp.
     *
     * @return NameOfEfp
     */
    public EFP getEfp() {
        return efp;
    }

    /**
     * Getter returning type of error.
     *
     * @return typeError
     */
    public MatchingResult getTypeError() {
        return typeError;
    }


    /**
     * Get side.
     * @return AssignmentSide
     */
    public AssignmentSide getSide() {
        return side;
    }

    /**
     * Get name of first component.
     * @return name
     */
    public String getFirstComponent() {
        return firstComponent;
    }

    /**
     * Get name of second component.
     * @return name
     */
    public String getSecondComponent() {
        return secondComponent;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((feature == null) ? 0 : feature.hashCode());
        result = prime * result
                + ((firstComponent == null) ? 0 : firstComponent.hashCode());
        result = prime * result
                + ((firstEfpValue == null) ? 0 : firstEfpValue.hashCode());
        result = prime * result
                + ((secondComponent == null) ? 0 : secondComponent.hashCode());
        result = prime * result
                + ((secondEfpValue == null) ? 0 : secondEfpValue.hashCode());
        result = prime * result + ((side == null) ? 0 : side.hashCode());
        result = prime * result
                + ((typeError == null) ? 0 : typeError.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpEvalResult other = (EfpEvalResult) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (feature == null) {
            if (other.feature != null) {
                return false;
            }
        } else if (!feature.getIdentifier().equals(other.feature.getIdentifier())) {
            return false;
        }
        if (firstComponent == null) {
            if (other.firstComponent != null) {
                return false;
            }
        } else if (!firstComponent.equals(other.firstComponent)) {
            return false;
        }
        if (firstEfpValue == null) {
            if (other.firstEfpValue != null) {
                return false;
            }
        } else if (!firstEfpValue.equals(other.firstEfpValue)) {
            return false;
        }
        if (secondComponent == null) {
            if (other.secondComponent != null) {
                return false;
            }
        } else if (!secondComponent.equals(other.secondComponent)) {
            return false;
        }
        if (secondEfpValue == null) {
            if (other.secondEfpValue != null) {
                return false;
            }
        } else if (!secondEfpValue.equals(other.secondEfpValue)) {
            return false;
        }
        if (side != other.side) {
            return false;
        }
        if (typeError != other.typeError) {
            return false;
        }
        return true;
    }

    /**
     * Get first value.
     * @return firstEfpValue
     */
    public EfpValueType getFirstEfpValue() {
        return firstEfpValue;
    }

    /**
     * Get second value.
     * @return secondEfpValue
     */
    public EfpValueType getSecondEfpValue() {
        return secondEfpValue;
    }

    @Override
    public String toString() {
        String efpName = "NO EFP";
        if (efp != null) {
            efpName = efp.getName();
        }
        return "EvaluateLog [feature=" + feature + ", efp=" + efpName
                + ", typeError=" + typeError + ", side=" + side
                + ", firstComponent=" + firstComponent + ", firstEfpValue="
                + firstEfpValue + ", secondEfpValue=" + secondEfpValue
                + ", secondComponent=" + secondComponent + "]";
    }


}

