package cz.zcu.kiv.efps.comparator.api;

import java.util.List;


/**
 *
 * Interface for loading data from efpAssignment. The input is a list of strings
 * representing names of components. The output is a graph of components
 * which may be further used for showing it in a GUI, to be evaluated
 * to its consistency etc.
 *
 * @author Kamil Jezek
 * @author Lukas Vlcek <vlceklu@students.zcu.cz>
 *
 */
public interface ComponentLoader {

    /**
     * An implementation of this method will get a list
     * of component names on its input and it should
     * create a graph of components expressing their bindings.
     *
     * @param componentNames names of the components
     * @param assignmentModul a name of a specific module which is responsible for loading
     * a representation of each component denoted by its name.
     * @param lrID ID of local register
     * @return the components graph
     */
    GraphCreator loadComponents(List<String> componentNames,
            String assignmentModul, Integer lrID);

}
