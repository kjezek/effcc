package cz.zcu.kiv.efps.comparator.standalone;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Class with filter for files without needed end of file.
 * @author Lukas Vlcek
 *
 */
class NameFilter implements FilenameFilter {

    /**
     * End of file.
     */
    private String ext;

    /**
     * Constructor.
     * @param ext what end of file we need
     */
    public NameFilter(final String ext) {
        this.ext = "." + ext;
    }

    @Override
    public boolean accept(final File dir, final String name) {
        return name.endsWith(ext);
    }
}
