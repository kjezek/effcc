package cz.zcu.kiv.efps.comparator.core.graph;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;

/**
 * Class for adding edges to the graph.
 * @author Lukas Vlcek
 *
 */
public class GraphStructureCreator {

    /**
     * Graph.
     */
    private Graph<GraphVertexRepr, DefaultEdge> graph;

    /**
     * Logger.
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Constructor. For construct we need references to graph.
     * @param graph graph
     */
    public GraphStructureCreator(final Graph<GraphVertexRepr, DefaultEdge> graph) {
        this.graph = graph;
    }

    /**
     * Add connection between component and feature.
     * @param component name of component
     * @param feature feature
    */
    public void addEdgeCompFeature(final String component, final Feature feature) {
        logger.debug("Adding edge feature: " + feature.getIdentifier()
                + " - component: " + component);
        GraphVertexRepr vertex = new GraphVertexRepr(component);
        GraphVertexRepr vertexFeature = new GraphVertexRepr(feature, component);
        if (!graph.containsVertex(vertex)) {
           graph.addVertex(vertex);
        }
        if (!graph.containsVertex(vertexFeature)) {
            graph.addVertex(vertexFeature);
        }
        if (vertexFeature.getFeature().getSide().equals(
                Feature.AssignmentSide.PROVIDED)) {
           graph.addEdge(vertex, vertexFeature);
        } else {
            graph.addEdge(vertexFeature, vertex);
        }
    }

    /**
     * Method for adding edge and vertex in graph.
     * @param feature feature
     * @param matchFeature match feature
     * @param featureComp component on first feature
     * @param matchFeatureComp component on second feature
     */
    public void addEdgeFeatureFeature(final Feature feature, final Feature matchFeature,
            final String featureComp, final String matchFeatureComp) {
        logger.debug("Adding edge feature: " + feature.getIdentifier()
                + " - feature" + matchFeature.getIdentifier());
        GraphVertexRepr firstVertex =  new GraphVertexRepr(feature, featureComp);
        GraphVertexRepr secondVertex =  new GraphVertexRepr(matchFeature, matchFeatureComp);
        if (!graph.containsVertex(secondVertex)) {
            graph.addVertex(secondVertex);
        }
        if (!graph.containsVertex(firstVertex)) {
            graph.addVertex(firstVertex);
        }
        if (feature.getSide().equals(AssignmentSide.PROVIDED)) {
            graph.addEdge(firstVertex, secondVertex);
        } else {
            graph.addEdge(secondVertex, firstVertex);
        }
    }

    /**
     * Add edge between feature and efp.
     * @param feature feature
     * @param efp efp
     */
    public void addEdgeFeatureEfp(final GraphVertexRepr feature, final GraphVertexRepr efp) {
        graph.addVertex(efp);
        logger.debug("Adding edge feature: " + feature.getFeature().getIdentifier()
                + " - efp: " + efp.getEfp().getName());
        if (feature.getFeature().getSide().equals(
                Feature.AssignmentSide.REQUIRED)) {
           graph.addEdge(efp, feature);
        } else {
            graph.addEdge(feature, efp);
        }
    }

    /**
     * Add edge between two efps.
     * @param firstEfp first efp
     * @param secondEfp second efp
     */
    public void addEdgeEfpEfp(final GraphVertexRepr firstEfp, final GraphVertexRepr secondEfp) {
        logger.debug("Adding edge efp: " + firstEfp.getEfp().getName()
                + " - efp: " + secondEfp.getEfp().getName());
        graph.addVertex(secondEfp);
        if (firstEfp.getConnectFeature().getFeature().getSide().equals(AssignmentSide.REQUIRED)) {
            graph.addEdge(secondEfp, firstEfp);
        } else {
            graph.addEdge(firstEfp, secondEfp);
        }
    }

}
