package cz.zcu.kiv.efps.comparator.api;

import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;


/**
 * Interface for evaluating of EFPs on components.
 * An implementation will get a graph of components
 * on its input and it is suppose to evaluate it
 * and return a list of evaluating results of the output.
 *
 * @author Kamil Jezek
 * @author Lukas Vlcek <vlceklu@students.zcu.cz>
 *
 */
public interface ComponentEvaluator {

    /**
     * Method that evaluates components.
     *
     * @param graph graph representing the connected components
     * @return a list of results of the evaluation.
     */
    List<EfpEvalResult> evaluate(DirectedGraph<GraphVertexRepr, DefaultEdge> graph);
}
