package cz.zcu.kiv.efps.comparator.core.graph;

import java.util.ArrayList;
import java.util.List;

import cz.zcu.kiv.efps.assignment.evaluator.MatchingFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.client.EfpAssignmentClient;
import cz.zcu.kiv.efps.comparator.api.ComponentLoader;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.ComponentAssignments;


/**
 *
 * Implementation of Comparator api. On input is here a list of names (string)
 * of component. On this list is called method from efpassignment, thats return
 * list of EfpAssignment, then is list compared.
 *
 * @author Lukas Vlcek <vlceklu@students.zcu.cz>
 *
 */
public class ComponentLoaderImp implements ComponentLoader {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public final GraphCreator loadComponents(
            final List<String> componentNames, final String assignmentModul,
            final Integer localRegistryID) {

        logger.info("Loading assignments from : " + assignmentModul);
        EfpAwareComponentLoader loader
            = EfpAssignmentClient.initialiseComponentLoader(assignmentModul);

        List<ComponentAssignments> listEfpAssig = new ArrayList<ComponentAssignments>();
        for (String name : componentNames) {
            logger.debug(name);
            ComponentEfpAccessor efpData = loader.loadForRead(name);
            ComponentAssignments data = new ComponentAssignments(efpData, name);
            listEfpAssig.add(data);
        }

        MatchingFunction mf = loader.createMatchingFunction(componentNames);

        return new GraphCreatorImp(listEfpAssig, localRegistryID, mf);
    }



}
