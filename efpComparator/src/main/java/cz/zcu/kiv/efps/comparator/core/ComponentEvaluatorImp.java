package cz.zcu.kiv.efps.comparator.core;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.DepthFirstIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.comparator.api.ComponentEvaluator;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.core.graph.VertexNeighborFinder;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr.VertexType;
import cz.zcu.kiv.efps.comparator.result.EfpResult;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import cz.zcu.kiv.efps.comparator.result.FeatureResult;

/**
 * Class for evaluating components and creating graph.
 *
 * @author Lukas Vlcek <vlceklu@students.zcu.cz/>
 *
 */
public class ComponentEvaluatorImp implements ComponentEvaluator {

   /**
     * List with result of evaluation.
     */
    private List<EfpEvalResult> results;

    /**
     * Finder for find the neighbors of vertexes.
     */
    private VertexNeighborFinder finder;

    /**
     * Instance of class for make efp result.
     */
    private EfpResult efpResult;

    /**
     * Instance of class for make feature result.
     */
    private FeatureResult featureResult;

    /**
     * Logger.
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Constructor.
     *
     */
    public ComponentEvaluatorImp() {
        results = new ArrayList<EfpEvalResult>();
    }

    @Override
    public final List<EfpEvalResult> evaluate(
            final DirectedGraph<GraphVertexRepr, DefaultEdge> graph) {

        logger.info("Starting evaluation");
        this.finder = new VertexNeighborFinder(graph);
        efpResult = new EfpResult(finder);
        featureResult = new FeatureResult(finder);
        DepthFirstIterator<GraphVertexRepr, DefaultEdge> iter =
            new DepthFirstIterator<GraphVertexRepr, DefaultEdge>(
                graph);
        while (iter.hasNext()) {
            GraphVertexRepr vertex = iter.next();
            if (vertex.getVertexType().equals(VertexType.FEATURE) && !vertex.getMatched()) {
                featureVertex(vertex);
            }
            if (vertex.getVertexType().equals(VertexType.EFP) && !vertex.getMatched()) {
                efpVertex(vertex);
            }
            }
        logger.info("End evaluation");
        return results;
    }


    /**
     * Make result if the vertex is feature.
     * @param vertex feature
     */
    private void featureVertex(final GraphVertexRepr vertex) {
        EfpEvalResult result;
        GraphVertexRepr matchFeature = finder.getMatchedFeature(vertex);
        if (matchFeature == null) {
            result = featureResult.addNoMatchFeatureResult(vertex);
        } else {
            matchFeature.setMatched(true);
            vertex.setMatched(true);
            result = featureResult.addMatchFeatureResult(vertex, matchFeature);
        }
        if (result != null) {
            results.add(result);
        }
    }

    /**
     * Make result if vertex is efp.
     * @param vertex efp
     */
    private void efpVertex(final GraphVertexRepr vertex) {
        EfpEvalResult result;
        GraphVertexRepr matchEfp = finder.getMatchEfp(vertex);
        if (matchEfp == null) {
        result = efpResult.noMatchEfpResult(vertex);
    } else {
        matchEfp.setMatched(true);
        result = efpResult.matchEfpResult(vertex, matchEfp);
    }
        if (result != null) {
            results.add(result);
        }
       vertex.setMatched(true);
    }
 }
