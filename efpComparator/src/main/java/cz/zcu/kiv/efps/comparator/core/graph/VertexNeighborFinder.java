package cz.zcu.kiv.efps.comparator.core.graph;

import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr.VertexType;

/**
 * Class for finding neighbors of vertexes.
 * @author Lukas Vlcek
 *
 */
public class VertexNeighborFinder {

    /**
     * graph, where we found the neighbors.
     */
    private DirectedGraph<GraphVertexRepr, DefaultEdge> graph;

    /**
     * Logger.
     */
    private Logger logger = LoggerFactory.getLogger(VertexNeighborFinder.class);

    /**
     * Constructor.
     * @param graph graph
     */
    public VertexNeighborFinder(final DirectedGraph<GraphVertexRepr, DefaultEdge> graph) {
        this.graph = graph;
    }
    /**
     * Find to what component is connect feature.
     *
     * @param featureVertex   connecting feature
     * @return name of component
     */
    public String findComponentByFeature(final GraphVertexRepr featureVertex) {
        logger.debug("Finding component neighbor for feature : "
                + featureVertex.getFeature().getIdentifier());
        if (featureVertex.getFeature().getSide()
                .equals(AssignmentSide.PROVIDED)) {
            Set<DefaultEdge> edges = graph.incomingEdgesOf(featureVertex);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeSource(edge);
                if (vertex.getVertexType().equals(VertexType.COMPONENT)) {
                    logger.debug("Return " + vertex.getCompName());
                    return vertex.getCompName();
                }
            }
        } else {
            Set<DefaultEdge> edges = graph.outgoingEdgesOf(featureVertex);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeTarget(edge);
                if (vertex.getVertexType().equals(VertexType.COMPONENT)) {
                    logger.debug("Return " + vertex.getCompName());
                    return vertex.getCompName();
                }
            }
        }
        logger.debug("Return null");
        return null;
    }

    /**
     * Method find match Feature to the feature in parameter.
     * @param feature feature vertex
     * @return match feature
     */
    public GraphVertexRepr getMatchedFeature(final GraphVertexRepr feature) {
        logger.debug("Finding match feature for feature : "
                + feature.getFeature().getIdentifier());
        if (feature.getFeature().getSide().equals(AssignmentSide.REQUIRED)) {
            Set<DefaultEdge> edges = graph.incomingEdgesOf(feature);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeSource(edge);
                if (vertex.getVertexType().equals(VertexType.FEATURE)) {
                    logger.debug("Return " + vertex.getFeature().getIdentifier());
                    return vertex;
                }
            }
        } else {
            Set<DefaultEdge> edges = graph.outgoingEdgesOf(feature);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeTarget(edge);
                if (vertex.getVertexType().equals(VertexType.FEATURE)) {
                    logger.debug("Return " + vertex.getFeature().getIdentifier());
                    return vertex;
                }
            }
        }
        logger.debug("Return null");
        return null;
    }

    /**
     * Return match efp.
     *
     * @param efp efp
     * @return vertex
     */
    public GraphVertexRepr getMatchEfp(final GraphVertexRepr efp) {
        logger.debug("Finding match efp for efp : "
                + efp.getEfp().getName());
        if (efp.getConnectFeature().getFeature().getSide()
                .equals(AssignmentSide.REQUIRED)) {
            Set<DefaultEdge> edges = graph.incomingEdgesOf(efp);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeSource(edge);
                if (vertex.getVertexType().equals(VertexType.EFP)) {
                    logger.debug("Return " + vertex.getEfp().getName());
                    return vertex;
                }
            }
        } else {
            Set<DefaultEdge> edges = graph.outgoingEdgesOf(efp);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr vertex = graph.getEdgeTarget(edge);
                if (vertex.getVertexType().equals(VertexType.EFP)) {
                    logger.debug("Return " + vertex.getEfp().getName());
                    return vertex;
                }
            }
        }
        logger.debug("Return null");
        return null;
    }


    /**
     * Find EFP neighbor of feature vertex.
     * @param vertex feature
     * @return true if have neighbor, else false
     */
    public boolean haveEfpNeighbor(final GraphVertexRepr vertex) {
        logger.debug("Finding if feature have efp neighbor feature : "
                + vertex.getFeature().getIdentifier());
        if (vertex.getFeature().getSide().equals(AssignmentSide.PROVIDED)) {
            Set<DefaultEdge> edges = graph.outgoingEdgesOf(vertex);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr ver = graph.getEdgeTarget(edge);
                if (ver.getVertexType().equals(VertexType.EFP)) {
                    logger.debug("Return true");
                    return true;
                }
            }
        } else {
            Set<DefaultEdge> edges = graph.incomingEdgesOf(vertex);
            for (DefaultEdge edge : edges) {
                GraphVertexRepr ver = graph.getEdgeSource(edge);
                if (ver.getVertexType().equals(VertexType.EFP)) {
                    logger.debug("Return true");
                    return true;
                }
            }
        }
        logger.debug("Return false");
        return false;
    }
   }
