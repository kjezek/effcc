package cz.zcu.kiv.efps.comparator.api;

import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;


/**
 * The interface for creating a graph of components.
 * The purpose of this interface is allow a variety
 * of graph creators to be used during hte evaluaton
 * process.
 *
 * @author Kamil Jezek
 * @author Lukas Vlcek
 *
 */

public interface GraphCreator {

    /**
     * This method connects components into a graph.
     * @return graph
     */
    DirectedGraph<GraphVertexRepr, DefaultEdge> create();

}
