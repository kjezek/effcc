package cz.zcu.kiv.efps.comparator.standalone;

import cz.zcu.kiv.efps.assignment.cosi.CosiAssignmentImpl;
import cz.zcu.kiv.efps.assignment.osgi.OSGiAssignmentImpl;
import cz.zcu.kiv.efps.comparator.api.ComponentEvaluator;
import cz.zcu.kiv.efps.comparator.api.ComponentLoader;
import cz.zcu.kiv.efps.comparator.api.GraphCreator;
import cz.zcu.kiv.efps.comparator.core.ComponentEvaluatorImp;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for starting evaluation.
 * @author Lukas Vlcek
 *
 */
public final class StandAloneEvaluator {

    /** Private constructor. */
    private StandAloneEvaluator() { }

    /**
     * List with results.
     */
    private static List<EfpEvalResult> results;

    /**
     * String with path to AssignmentModul for load EFPs.
     */
    private static String modul = CosiAssignmentImpl.class.getName();

    /**
     * ID of local register.
     */
    private static int localRegisterID;

    /**
     * List with bundles.
     */
    private static List<String> bundles;

    /**
     * Main method for get names of JARs in file.
     * @param args args
     */
    public static void main(final String[] args) {
        localRegisterID = Integer.valueOf(args[1]);
        ResultsOutput output = new ResultsOutput();
        if (args.length < 2 || args.length > 3) {
            System.out.println("Run with parameters <Path to bundles> <LocalRegister ID>");
            System.out.println("If you want to start evaluation for OSGi bundles, "
                    + "put <OSGi> as third parametr.");
            return;
        }
        if (args.length == 3) {
            if (args[2].equalsIgnoreCase("OSGi")) {
                modul = OSGiAssignmentImpl.class.getName();
            }
        }
        bundles = loadBundles(args[0]);
        evaluate();
        output.writeOut(results);

        /**System.out.println("------efpComparator-----");
        Scanner sc = new Scanner(System.in);
        for (;;) {
            String[] commands = sc.next().split(" ");
            if (commands[0].equals("install") && commands.length == 2) {
                if (new File(commands[1]).exists()) {
                    bundles.add(commands[1]);
                    evaluate();
                } else {
                    System.out.println("File " + commands[1] + " dont exist.");
                }
            }
            if (commands[0].equals("EfpResults")) {
                output.writeOut(results);
            }
            if (commands[0].equals("help")) {
                System.out.println("write : install <pathToBundle> - for add Bundle to evaluation");
                System.out.println("      : EfpResults - for write out the results.");
            }
        }*/

     }


    /**
     * Evaluate bundles from list.
     */
    private static void evaluate() {
        ComponentLoader loader = new ComponentLoaderImp();
        GraphCreator creator = loader.loadComponents(
                bundles, modul, localRegisterID);
        DirectedGraph<GraphVertexRepr, DefaultEdge> graph = creator.create();
        ComponentEvaluator evaluator = new ComponentEvaluatorImp();
        results = evaluator.evaluate(graph);
    }

    /**
     * Load bundles from folder.
     * @param pathToBundles path to folder with bundles
     * @return List with names
     */
    public static List<String> loadBundles(final String pathToBundles) {
        File folder = new File(pathToBundles);
        FilenameFilter filter = new NameFilter("jar");

        File[] listOfFiles = folder.listFiles(filter);

        return makeStringList(listOfFiles);

    }

    /**
     * Method for make list with path to files.
     * @param listOfFiles field with files
     * @return list with paths
     */
    private static List<String> makeStringList(final File[] listOfFiles) {
        List<String> pathToFiles = new ArrayList<String>();
        for (File jar : listOfFiles) {
            pathToFiles.add(jar.getAbsolutePath());
        }
        return pathToFiles;
    }
}
