package cz.zcu.kiv.efps.comparator.core.graph;

import java.io.File;

import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Class representing Vertex in graph. It hold name of component, feature
 * and EFP. If the vertex is EFP, it hold assignment value, and compute Value.
 *
 * @author Lukáš Vlček
 *
 */
public class GraphVertexRepr {

    /**
     * Type of vertex.
     * @author Lukas Vlcek
     *
     */
    public enum VertexType {
            /**
             * Vertex is feature.
             */
            FEATURE,
            /**
             * Vertex is component.
             */
            COMPONENT,
            /**
             * Vertex is efp.
             */
            EFP

    };

    /**
     * Type of vertex.
     */
    private final VertexType type;

    /**
     * Connected component.
     */
    private String connectComponent;

    /**
     * Boolean if this vertex is matched with other vertex.
     */
    private boolean matched;
    /**
     * List of assignments to the feature.
     */
    private EfpAssignedValue assignmentValue;
    /**
     * Feature.
     */
    private final Feature feature;

    /**
     * Name of component.
     */
    private final String compName;

    /**
     * Feature connection with EFP.
     */
    private GraphVertexRepr connectFeature;

    /**
     * EFP.
     */
    private final EFP efp;

    /**
     * Computing value.
     */
    private EfpValueType value;

    /**
     * Vertex is feature.
     * @param feature feature
     * @param connectComponent connect component
     */
    public GraphVertexRepr(final Feature feature, final String connectComponent) {
        super();
        this.feature = feature;
        this.compName = null;
        this.efp = null;
        this.assignmentValue = null;
        this.connectFeature = null;
        this.type = VertexType.FEATURE;
        this.connectComponent = connectComponent;
    }

    /**
     * Vetrex is Component.
     * @param compName name of component
     */
    public GraphVertexRepr(final String compName) {
        super();
        this.feature = null;
        this.compName = compName;
        this.efp = null;
        this.assignmentValue = null;
        this.connectFeature = null;
        this.type = VertexType.COMPONENT;
    }

    /**
     * Vertex is EFP.
     * @param efp efp
     * @param connectFeature feature connect to efp
     * @param assignmentValue assignment value
     */
    public GraphVertexRepr(final EFP efp, final EfpAssignedValue assignmentValue,
            final GraphVertexRepr connectFeature) {
        super();
        this.feature = null;
        this.compName = null;
        this.connectFeature = connectFeature;
        this.efp = efp;
        this.assignmentValue = assignmentValue;
        this.type = VertexType.EFP;
    }

    /**
     * Get feature.
     * @return feature
     */
    public Feature getFeature() {
        return feature;
    }

    /**
     * Get feature connect with efp.
     * @return feature
     */
    public GraphVertexRepr getConnectFeature() {
        return connectFeature;
    }

    /**
     * Get feature.
     * @return feature
     */
    public EFP getEfp() {
        return efp;
    }

    /**
     * Get boolean.
     * @return matched
     */
    public boolean getMatched() {
        return matched;
    }

    /**
     * Set boolean.
     * @param matched boolean
     */
    public void setMatched(final boolean matched) {
        this.matched = matched;
    }


    /**
     * Get assignment value.
     * @return assignment value
     */
    public EfpAssignedValue getAssignmentValue() {
        return assignmentValue;
    }

    /**
     * Set compute value.
     * @param value value
     */
    public void setValue(final EfpValueType value) {
        this.value = value;
    }
    /**
     * Get compute value.
     * @return value
     */
    public EfpValueType getValue() {
        return value;
    }

    /**
     * Get vertex type.
     * @return type of vertex
     */
    public VertexType getVertexType() {
        return type;
    }

    /**
     * Get name of component.
     * @return name
     */
    public String getCompName() {
        return compName;
    }

    /**
     * Get name of connect component.
     * @return connectComponent
     */
    public String getConnectCompName() {
        return connectComponent;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((assignmentValue == null) ? 0 : assignmentValue.hashCode());
        result = prime * result
                + ((compName == null) ? 0 : compName.hashCode());
        result = prime * result
                + ((connectFeature == null) ? 0 : connectFeature.hashCode());
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((feature == null) ? 0 : feature.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }



    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GraphVertexRepr other = (GraphVertexRepr) obj;
        if (assignmentValue == null) {
            if (other.assignmentValue != null) {
                return false;
            }
        } else if (!assignmentValue.equals(other.assignmentValue)) {
            return false;
        }
        if (compName == null) {
            if (other.compName != null) {
                return false;
            }
        } else if (!compName.equals(other.compName)) {
            return false;
        }
        if (connectComponent == null) {
            if (other.connectComponent != null) {
                return false;
            }
        } else if (!connectComponent.equals(other.connectComponent)) {
            return false;
        }
        if (connectFeature == null) {
            if (other.connectFeature != null) {
                return false;
            }
        } else if (!connectFeature.equals(other.connectFeature)) {
            return false;
        }
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (feature == null) {
            if (other.feature != null) {
                return false;
            }
        } else if (!feature.equals(other.feature)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (efp != null) {
          return "EFP: " + efp.getName() + " "
              + connectFeature.getFeature().getSide().toString();
        }
        if (feature != null) {
            return "Feature: " + feature.getName() + " "
                + feature.getSide().toString();
        }
        int lastSep = compName.lastIndexOf(File.separator);
        return "Component: " +  compName.substring(lastSep + 1);
    }



}
