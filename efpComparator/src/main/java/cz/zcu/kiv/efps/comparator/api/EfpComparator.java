/**
 *
 */
package cz.zcu.kiv.efps.comparator.api;

import java.util.Arrays;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.comparator.core.ComponentEvaluatorImp;
import cz.zcu.kiv.efps.comparator.core.graph.ComponentLoaderImp;
import cz.zcu.kiv.efps.comparator.core.graph.GraphVertexRepr;
import cz.zcu.kiv.efps.comparator.result.EfpEvalResult;

/**
 * This is a simple facade called to
 * evaluate EFPs on a set of components.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpComparator {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EfpComparator.class);

    /** Private. */
    private EfpComparator() { }

    /**
     * This method is called to evaluate a set of components
     * with EFPs. The result of the evaluation is a compatibility
     * decision in terms of component binding and related EFPs
     * comparing.
     *
     * This method is a simple facade internally calling
     * methods {@link ComponentLoader#loadComponents(List, String, Integer)}},
     * {@link GraphCreator#create()}
     * and {@link ComponentEvaluator#evaluate(org.jgrapht.DirectedGraph)}
     * respectively.
     *
     * @param components a set of components to be compared
     * @param assignmentModul a class name of a module to be used for
     * loading components EFP representation.
     * @param lrID ID of a local registry which the evaluation will be created for.
     * @return the list of compatibility results.
     */
    public static List<EfpEvalResult> evaluate(
            final List<String> components,
            final String assignmentModul,
            final Integer lrID) {

        LOGGER.info("Evaluationg components: {}, modul: {}, LR id: {}",
                Arrays.asList(components, assignmentModul, lrID));

        ComponentLoader cl = new ComponentLoaderImp();
        GraphCreator gc = cl.loadComponents(components, assignmentModul, lrID);
        DirectedGraph<GraphVertexRepr, DefaultEdge> graph = gc.create();
        ComponentEvaluator ce = new ComponentEvaluatorImp();

        List<EfpEvalResult> result = ce.evaluate(graph);

        LOGGER.info("Evaluation finished. ");

        return result;
    }
}
