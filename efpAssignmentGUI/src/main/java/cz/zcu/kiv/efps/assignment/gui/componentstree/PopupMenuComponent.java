package cz.zcu.kiv.efps.assignment.gui.componentstree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.tree.TreeNode;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;

/**
 * Popup menu for providing actions at the component in component tree.
 *
 * Date: 8.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class PopupMenuComponent  extends JPopupMenu {
    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** Editing node with component. */
    private ComponentNode editingComponentNode;
    /** Editing component tree. */
    private ComponentsTree componentTree;



    /**
     * Default constructor for setting source objects.
     * @param editingComponentNode Reference to editing component node.
     * @param componentsTree Reference to changing component tree
     */
    public PopupMenuComponent(final ComponentNode editingComponentNode,
            final ComponentsTree componentsTree) {

        localization = new Localization("componentTree");

        JMenuItem mni = new JMenuItem(localization.getProperty("lblSave"));
        mni.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/edit.png")));
        mni.addActionListener(new SaveAction());
        add(mni);

        mni = new JMenuItem(localization.getProperty("lblClear"));
        mni.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                "images/delete.png")));
        mni.addActionListener(new ClearAction());
        add(mni);

        mni = new JMenuItem(localization.getProperty("lblClose"));
        mni.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/close.png")));
        mni.addActionListener(new CloseAction());
        add(mni);


        this.editingComponentNode = editingComponentNode;
        this.componentTree = componentsTree;
    }



    /**
     * Action listener for delete concrete component node.
     *
     * Date: 8.2.2011
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class SaveAction implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            editingComponentNode.getComponent().close();
        }
    }


    /**
     * Action listener for delete concrete component node.
     *
     * Date: 8.2.2011
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class CloseAction implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            editingComponentNode.getComponent().close();
            TreeNode changedNode = editingComponentNode.getParent();

            ComponentsTreeModel componentTreeModel = componentTree.getComponentsTreeModel();

            componentTreeModel.removeNodeFromParent(editingComponentNode);
            componentTreeModel.nodeStructureChanged(changedNode);
            componentTree.expandAll();
        }

    }

    /**
     * Action listener for delete all EFP assignments from the component.
     *
     * Date: 6.2.2014
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class ClearAction implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            int n = JOptionPane.showConfirmDialog(null,
                    localization.getProperty("confirmClearDlgText"),
                    localization.getProperty("confirmClearDlgTitle"), JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                editingComponentNode.getComponent().clear();
            }

            ComponentsTreeModel componentTreeModel = componentTree.getComponentsTreeModel();
            editingComponentNode.removeAllChildren();
            editingComponentNode.setFeatureNodes();
            componentTreeModel.nodeStructureChanged(editingComponentNode.getParent());
            componentTree.expandAll();
        }
    }
}
