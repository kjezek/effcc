package cz.zcu.kiv.efps.assignment.gui.exceptions;

/**
 * This exception is thrown if an error occurs in GUI of the EFP assignment.
 *
 * Date: 5.3.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class GuiRTException extends RuntimeException {

    /** UID. */
    private static final long serialVersionUID = 7835402880618065958L;

    /**
     * Empty constructor.
     */
    public GuiRTException() {
        super();
    }

    /**
     * Constructor with two parameters.
     * @param message String with message
     * @param cause Cause
     */
    public GuiRTException(final String message, final Throwable cause) {
        super(message, cause);
    }


    /**
     * Constructor with message parameter.
     * @param message Message
     */
    public GuiRTException(final String message) {
        super(message);
    }

    /**
     * Constructor with cause parameter.
     * @param cause Cause
     */
    public GuiRTException(final Throwable cause) {
        super(cause);
    }
}
