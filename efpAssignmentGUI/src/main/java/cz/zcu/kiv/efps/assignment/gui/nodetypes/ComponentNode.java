package cz.zcu.kiv.efps.assignment.gui.nodetypes;

import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.types.Feature;

/**
 * Node for tree which represents opened component.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentNode extends DefaultMutableTreeNode {
    /** Serial version. */
    private static final long serialVersionUID = 1L;
    /** Reference to opened component. */
    private ComponentEFPModifier component;
    /** Name of component and node. */
    private String componentName;


    /**
     * Default constructor.
     * @param component Reference to opened component.
     * @param componentName A label of this node.
     */
    public ComponentNode(final ComponentEFPModifier component, final String componentName) {
        super(componentName);

        if (component == null) {
            throw new RuntimeException("Parameter 'component' is null.");
        }

        this.component = component;
        this.componentName = componentName;
    }


    /**
     * Sets all features node for each feature from the component.
     */
    public void setFeatureNodes() {
        List<Feature> features = component.getAllFeatures();

        for (Feature feature : features) {
            FeatureNode featureNode = createFeatureNode(feature);
            featureNode.addEFPNodes(component);
        }
    }


    /**
     * Creates feature node if doesn't exist and joins it into tree of component.
     * @param feature A feature
     * @return Feature node for feature
     */
    private FeatureNode createFeatureNode(final Feature feature) {
        DefaultMutableTreeNode fpn = findParentFeatureNode(this, feature);
        if (fpn != null) {
            return (FeatureNode) fpn;
        }

        FeatureNode newFeatureNode = new FeatureNode(feature);

        if (feature.getParent() != null) {
            FeatureNode parent = createFeatureNode(feature.getParent());
            parent.add(newFeatureNode);
        } else {
            this.add(newFeatureNode);
        }

        return newFeatureNode;
    }


    /**
     * Tries to find parent node of node with feature in concrete node.
     * @param node A node for searching
     * @param feature Searched feature.
     * @return Parent node of node with feature or null
     */
    private static DefaultMutableTreeNode findParentFeatureNode(
            final DefaultMutableTreeNode node, final Feature feature) {

        for (int indexNode = 0; indexNode < node.getChildCount(); indexNode++) {
            DefaultMutableTreeNode dtn = (DefaultMutableTreeNode) node.getChildAt(indexNode);

            if (dtn instanceof FeatureNode) {
                if (((FeatureNode) dtn).getFeature() == feature) {
                    return dtn;
                }

                DefaultMutableTreeNode targetNode = findParentFeatureNode(dtn, feature);

                if (targetNode != null) {
                    return targetNode;
                }
            }
        }

        return null;
    }


    /**
     * @return Reference to component
     */
    public ComponentEFPModifier getComponent() {
        return component;
    }


    /**
     * @return Name of component
     */
    public String getComponentName() {
        return componentName;
    }
}
