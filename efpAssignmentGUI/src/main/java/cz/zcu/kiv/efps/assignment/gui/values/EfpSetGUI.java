package cz.zcu.kiv.efps.assignment.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible set value. <br>
 * Date: 10. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpSetGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** Map of position and new gui value. */
    private Map<Integer, EfpValueGUI> gui;

    /** Map of position and button for delete. */
    private Map<Integer, JButton> btns;

    /** ComboBox for adding new value. */
    private JComboBox cmbNewValue;

    /** Button for adding new value. */
    private JButton btnAdd;

    /** Position gui. (postincrement) */
    private int y = 1;

    /**
     * Create the panel.
     */
    EfpSetGUI() {
        super();
        setBorder(new TitledBorder(bundle.getProperty("titleBorderSet")));
        gui = new HashMap<Integer, EfpValueGUI>();
        btns = new HashMap<Integer, JButton>();

        init();

    }

    /**
     * Initialization GUI.
     */
    private void init() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        cmbNewValue = new JComboBox();
        cmbNewValue.setModel(new DefaultComboBoxModel(EfpValueGUI.getNamesOfValues()));
        cmbNewValue.setToolTipText(bundle.getProperty("toolTipSet"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.weightx = 0.0;
        gbc.gridy = 0;
        add(cmbNewValue, gbc);

        btnAdd = new JButton(bundle.getProperty("btnAddSet"));
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                addValue();
            }
        });
        gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.EAST;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 2;
        gbc.weightx = 0.0;
        gbc.gridy = 0;
        add(btnAdd, gbc);
    }

    /**
     * Add gui for new value according parametr.
     * @param value value which will be added
     */
    private void addValue(final EfpValueType value) {
        EfpValueGUI newValue = FactoryValueGUI.getValueGUI(value.getClass());
        newValue.setValue(value);
        addGui(newValue);
        this.validate();
        this.repaint();
    }

    /**
     * Add gui for new value according combobox.
     */
    private void addValue() {
        EfpValueGUI newValue = FactoryValueGUI.getValueGUI(EfpValueGUI
                .getTypeByName(cmbNewValue.getSelectedItem().toString()));
        addGui(newValue);
        this.getRootPane().validate();
        this.getRootPane().repaint();
    }

    /**
     * Create GUI fo new value.
     * @param newValue new gui value
     */
    private void addGui(final EfpValueGUI newValue) {
        gui.put(y, newValue);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 1;
        gbc.weightx = 1.0;
        gbc.gridy = y;
        add(newValue, gbc);

        JButton deleteBtn = new JButton(bundle.getProperty("btnDeleteSet"));
        deleteBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                for (Entry<Integer, JButton> i : btns.entrySet()) {
                    if (i.getValue() == e.getSource()) {
                        deleteValue(i.getKey());
                        break;
                    }
                }

            }
        });
        btns.put(y, deleteBtn);
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridx = 2;
        gbc.weightx = 0.0;
        gbc.gridy = y;
        add(deleteBtn, gbc);
        y++;

    }

    /**
     * Delete value on position y.
     * @param y position deleted value
     */
    private void deleteValue(final int y) {
        this.remove(gui.get(y));
        this.remove(btns.get(y));
        gui.remove(y);
        btns.remove(y);
        this.getRootPane().validate();
        this.getRootPane().repaint();
    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        EfpValueType[] items = new EfpValueType[gui.size()];
        int i = 0;
        for (EfpValueGUI g : gui.values()) {
            items[i++] = g.getValue();
        }
        return new EfpSet(items);
    }

    @Override
    public void setValue(final EfpValueType value) {
        for (EfpValueType val : ((EfpSet) value).getItems()) {
            addValue(val);
        }
        this.validate();
        this.repaint();
    }

    /* (non-Javadoc)
     *
     * @see values.EfpValueGUI#lock() */
    @Override
    public void lock() {
        for (int i = 0; i < y; i++) {
            if (gui.containsKey(i)) {
                gui.get(i).lock();
                this.remove(btns.get(i));
                btns.remove(i);
            }
        }
        this.remove(cmbNewValue);
        this.remove(btnAdd);
    }
}
