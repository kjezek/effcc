package cz.zcu.kiv.efps.assignment.gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.swing.JOptionPane;

import cz.zcu.kiv.efps.assignment.gui.itemtypes.ComponentImplItem;

/**
 * Class for getting all component model implementations from default properties file.
 *
 * Date: 7.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class ComponentImplsProperties {
    /** Path to properties file with supported component types. */
    private static final String PATH = "supportedcomponents.properties";

    /** Hide implicit constructor. */
    private ComponentImplsProperties() { }



    /**
     * Loads from property all component model implementations and returns them in list.
     * @return List with component model implementations in ComponentImplItem objects.
     */
    public static List<ComponentImplItem> getComponentLoaders() {
        List<ComponentImplItem> listCompImplItems = new ArrayList<ComponentImplItem>();


        Properties properties = new Properties();
        InputStream is = AssignmentGUI.class.getClassLoader().getResourceAsStream(PATH);

        try {
            properties.load(is);

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "Exception", JOptionPane.ERROR_MESSAGE);
            return listCompImplItems;

        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(),
                            "Exception", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        Set<String> namesOfTypes = properties.stringPropertyNames();

        for (String key : namesOfTypes) {
            String value = properties.getProperty(key);

            listCompImplItems.add(new ComponentImplItem(key, value));
        }

        return listCompImplItems;
    }
}
