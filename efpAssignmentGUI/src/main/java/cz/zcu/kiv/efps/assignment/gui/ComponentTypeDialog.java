package cz.zcu.kiv.efps.assignment.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.itemtypes.ComponentImplItem;

/**
 * Dialog for choosing component loader.
 *
 * Date: 23.1.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentTypeDialog extends JDialog {

    /** Serial version.  */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** X-coordinate of the frame. */
    private static final int WINDOW_X = 100;
    /** Y-coordinate of the frame. */
    private static final int WINDOW_Y = 100;
    /** Width of the frame. */
    private static final int WINDOW_WIDTH = 450;
    /** Height of the frame. */
    private static final int WINDOW_HEIGHT = 300;
    /** Basic insets of a element. */
    private static final int INSETS = 5;

    /** Content panel. */
    private final JPanel contentPanel = new JPanel();
    /** List with supported component implementations. */
    private JList listComponentType;
    /** Path to selected component model implementation. */
    private String pathToCompImpl;
    /** Name of selected component model implementation. */
    private String nameOfCompImpl;
    /** Contains true, if dialog was closed by OK button. */
    private boolean isDialogConfirm;



    /**
     * Create the dialog.
     */
    public ComponentTypeDialog() {
        super(null, Dialog.ModalityType.APPLICATION_MODAL);

        localization = new Localization("componentTypeDlg");
        pathToCompImpl = null;
        nameOfCompImpl = null;
        isDialogConfirm = false;

        initialization();
    }


    /**
     * Fills a list with types supported components.
     * @param componentImplItems Reference to list with component model implementations.
     */
    public void fillListComponentType(final List<ComponentImplItem> componentImplItems) {
        DefaultListModel listModel = new DefaultListModel();

        for (ComponentImplItem item : componentImplItems) {
            listModel.addElement(item);
        }

        listComponentType.setModel(listModel);
    }


    /**
     * Initialization of the dialog.
     */
    private void initialization() {
        setAlwaysOnTop(true);
        setResizable(false);

        setTitle(localization.getProperty("title"));
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(INSETS, INSETS, INSETS, INSETS));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));

        listComponentType = new JList();
        listComponentType.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        contentPanel.add(listComponentType, BorderLayout.CENTER);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton okButton = new JButton(localization.getProperty("btnOK"));
        okButton.addActionListener(new OkBtnActionListener());
        okButton.setActionCommand(localization.getProperty("btnOK"));
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton(localization.getProperty("btnStorno"));
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ComponentTypeDialog.this.setVisible(false);
            }
        });
        cancelButton.setActionCommand(localization.getProperty("btnStorno"));
        buttonPane.add(cancelButton);
    }


    /**
     * @return Path to class with component model implementation.
     */
    public String getPathToCompImpl() {
        return pathToCompImpl;
    }

    /**
     * @return Name of component model implementation.
     */
    public String getNameOfCompImpl() {
        return nameOfCompImpl;
    }

    /**
     * @return True, if the dialog was closed by OK button, otherwise false.
     */
    public boolean getIsDialogConfirm() {
        return isDialogConfirm;
    }


    /**
     * Action listener for OK button.
     *
     * Date: 23.1.2011
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class OkBtnActionListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            ComponentImplItem cti = (ComponentImplItem) listComponentType.getSelectedValue();

            if (cti == null) {
                throw new GuiRTException(localization.getProperty("ERR_SELECT"));
            }

            pathToCompImpl = cti.getPath();
            nameOfCompImpl = cti.getNameOfType();

            isDialogConfirm = true;

            AppParametersProperties.saveParameter(
                    "comp_impl_name", nameOfCompImpl);
            AppParametersProperties.saveParameter(
                    "comp_impl_path", pathToCompImpl);

            ComponentTypeDialog.this.setVisible(false);
        }
    }
}
