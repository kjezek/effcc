package cz.zcu.kiv.efps.assignment.gui.componentstree;

import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.FeatureNode;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Selection listener for tree with openede components.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentsTreeSelectionListener implements TreeSelectionListener {

    /** Target text pane for showing information about selected node. */
    private JTextPane targetTextPane;
    /** Properties file with localization. */
    private Localization localization;


    /**
     * Default constructor.
     * @param targetTextPane Reference to target text pane.
     */
    public ComponentsTreeSelectionListener(final JTextPane targetTextPane) {
        localization = new Localization("componentTree");
        this.targetTextPane = targetTextPane;
    }


    @Override
    public void valueChanged(final TreeSelectionEvent e) {
        JTree tree = (JTree) e.getSource();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

        if (node == null) {
            targetTextPane.setText("");

        } else if (node instanceof ComponentNode) {
            targetTextPane.setText(localization.getProperty("component")
                    + ((ComponentNode) node).getComponentName());

        } else if (node instanceof FeatureNode) {
            targetTextPane.setText(((FeatureNode) node).getFeature().getSide() + " feature: "
                    + ((FeatureNode) node).getFeature().toString());

        } else if (node.getUserObject() instanceof EFP) {
            EFP efp = (EFP) node.getUserObject();

            targetTextPane.setText(efp.getType() + " EFP:\n" + efp.toString());

        } else if (node.getUserObject() instanceof EfpAssignedValue) {
            targetTextPane.setText(localization.getProperty("efpAssign") + ": \n"
                    + ((EfpAssignedValue) node.getUserObject()).toString());

        } else {
           targetTextPane.setText("");
        }
    }
}
