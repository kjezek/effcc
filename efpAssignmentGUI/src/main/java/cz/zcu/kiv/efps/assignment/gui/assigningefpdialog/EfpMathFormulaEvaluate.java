package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.assignment.evaluator.AbstractGroovyFormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.MathFormulaEvaluator;
import cz.zcu.kiv.efps.assignment.evaluator.MathFormulaHelper;
import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.assignment.types.EfpAssignment;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for test of logical formula with testing values. <br>
 * <br>
 * <b>Date:</b> 1.3.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpMathFormulaEvaluate extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7802863519039960317L;

    /** Width of frame. */
    private static final int WIDTH = 600;

    /** Height of frame. */
    private static final int HEIGHT = 300;

    /** A testing logicat formula. */
    private String formula;

    /** Used efp with LrAssignment. */
    private List<EfpFeature> efpFeature;

    /** List of JTextFields with values. */
    private List<EfpValueGUI> lstValues;

    /** Variable of this dialog. */
    private EfpMathFormulaEvaluate self;

    /** Properties file with localization. */
    private Localization bundle = new Localization("mathFormulaGUI");

    /**
     * Test a logical formula.
     * @param formula A logical formula.
     * @param efpFeatures List of all possible efpFeatures for this formula.
     */
    public EfpMathFormulaEvaluate(final String formula, final EfpFeature... efpFeatures) {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle(bundle.getProperty("editor_testing"));
        setModal(true);

        self = this;
        MathFormulaHelper helper = new MathFormulaHelper(formula);
        efpFeature = helper.findUsedEfps(efpFeatures);
        this.formula = formula;
        lstValues = new ArrayList<EfpValueGUI>();
        init();
        setVisible(true);
    }

    /** Initialization of dialog. */
    private void init() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.insets = new Insets(0, 0, 0, 0);
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(getValuesPanel(), gbc);

        gbc.insets = new Insets(5, 0, 5, 0);
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(getControlPanel(), gbc);

        setContentPane(panel);
    }

    /**
     * Return a panel with form for input values.
     * @return A panel for input values.
     */
    private Container getValuesPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(bundle.getProperty("test_values")));
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        for (int i = 0; i < efpFeature.size(); i++) {
            gbc.gridy = i;

            gbc.gridx = 0;
            gbc.weightx = 0.0;
            gbc.fill = GridBagConstraints.NONE;
            panel.add(new JLabel(efpFeature.get(i).getEfp().getName()), gbc);

            gbc.gridx = 1;
            gbc.weightx = 1.0;
            gbc.fill = GridBagConstraints.BOTH;
            EfpValueGUI val = FactoryValueGUI.getValueGUI(efpFeature.get(i).getEfp()
                    .getValueType());
            lstValues.add(val);
            panel.add(val, gbc);
        }
        JScrollPane sp = new JScrollPane();
        sp.setBorder(null);
        sp.setViewportView(panel);
        return sp;
    }

    /**
     * Return a control panel with test button.
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel panel = new JPanel();

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                Map<EFP, EfpValueType> values = new HashMap<EFP, EfpValueType>();
                for (int i = 0; i < lstValues.size(); i++) {
                    try {
                        EfpValueType value = lstValues.get(i).getValue();
                        values.put(efpFeature.get(i).getEfp(), value);
                    } catch (IllegalValueException e1) {
                        JOptionPane.showMessageDialog(self, e1.getMessage(), "Error",
                                JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                }
                try {
                    EfpValueType result = evaluate(values);
                    JOptionPane.showMessageDialog(self, bundle.getProperty("formula_OK")
                            + "\n" + result.getLabel(), "Math formula is OK",
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(self, bundle.getProperty("formula_KO")
                            + "\n" + e1.getMessage(), "Wrong math formula",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        panel.add(btnTest);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                self.dispose();
            }
        });
        panel.add(btnStorno);
        return panel;
    }

    /**
     * Evaluate valid of formula.
     * @param efpValues Testing values.
     * @return True if formula is valid.
     */
    private EfpValueType evaluate(final Map<EFP, EfpValueType> efpValues) {
        List<EfpAssignment> values = new ArrayList<EfpAssignment>();
        for (EfpFeature sf : efpFeature) {
            values.add(new EfpAssignment(sf.getEfp(), new EfpDirectValue(efpValues.get(sf
                    .getEfp())), sf.getFeature()));
        }

        return new MathFormulaEvaluator(formula).evaluate(values);
    }
}
