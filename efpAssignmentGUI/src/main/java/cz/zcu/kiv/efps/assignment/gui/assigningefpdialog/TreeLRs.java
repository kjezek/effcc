package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import cz.zcu.kiv.efps.assignment.gui.AssignmentGUI;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForTrees;
import cz.zcu.kiv.efps.registry.client.EfpClient;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Tree with lr assignments one concrete efp.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class TreeLRs extends JTree {

    /** Serial version. */
    private static final long serialVersionUID = 1L;


    /**
     * Default Constructor.
     */
    public TreeLRs() {
        super(new DefaultTreeModel(new DefaultMutableTreeNode()));
        setRootVisible(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setCellRenderer(new CellRendererForTrees());
    }


    /**
     * Sets data model for all lr assignments of the concrete efp.
     * @param efp Required efp
     */
    public void setData(final EFP efp) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(efp.getGr());
        EfpClient efpClient = AssignmentGUI.getEfpClient();

        List<LR> lrs = efpClient.findLrForGr(efp.getGr());
        for (LR lr : lrs) {
            addLR(root, lr);
        }

        this.setModel(new DefaultTreeModel(root));
        expandAll();
    }


    /**
     * Adds lr to tree with parents if not exits.
     * @param root Root of tree
     * @param lr Target lr
     * @return Node of lr
     */
    private DefaultMutableTreeNode addLR(final DefaultMutableTreeNode root, final LR lr) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(lr);
        if (lr.getParentLR() == null) {
            root.add(node);
            return node;
        }

        DefaultMutableTreeNode parent = findNode(root, lr.getParentLR());
        if (parent == null) {
            parent = addLR(root, lr.getParentLR());
        }

        parent.add(node);
        return node;
    }


    /**
     * Finds node of parentLR.
     * @param rootNode Root node
     * @param lr Searched lr
     * @return Node of parentLR
     */
    private DefaultMutableTreeNode findNode(final DefaultMutableTreeNode rootNode,
            final LR lr) {

        if (rootNode.getUserObject() instanceof LR
                && ((LR) rootNode.getUserObject()).getId() == lr.getId()) {
            return rootNode;
        }
        if (rootNode.isLeaf()) {
            return null;
        }
        for (int i = 0; i < rootNode.getChildCount(); i++) {
            DefaultMutableTreeNode dmtn = findNode((DefaultMutableTreeNode) rootNode.getChildAt(i),
                    lr);
            if (dmtn != null) {
                return dmtn;
            }
        }
        return null;
    }


    /**
     * Expands all nodes.
     */
    private void expandAll() {
        for (int i = 0; i < this.getRowCount(); i++) {
            this.expandRow(i);
        }
    }
}
