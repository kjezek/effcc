package cz.zcu.kiv.efps.assignment.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

/**
 * Class for working with properties file which contains start parameters of application.
 *
 * Date: 7.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class AppParametersProperties {
    /** Path to properties file with parameters of application. */
    private static final String APP_PARAM_PROP = "app_parameters.properties";

    /** Hide implicit constructor. */
    private AppParametersProperties() { }


    /**
     * Gets value of parameter from properties file.
     * @param key Name of the parameter
     * @return Returns null if properties file or parameter doesn't exist otherwise returns value
     */
    public static String getParameterValue(final String key) {
        Properties properties = new Properties();
        File propFile = new File(APP_PARAM_PROP);

        if (!propFile.exists()) {
            return null;
        }

        InputStream is = null;

        try {
            is = new FileInputStream(propFile);
            properties.load(is);
            return properties.getProperty(key);

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(),
                    "Exception", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(),
                            "Exception", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        return null;
    }


    /**
     * Update properties file with new parameter. Creates properties file if it doesn't exist.
     * @param key Name of the parameter
     * @param parameter Value of the parameter
     */
    public static void saveParameter(final String key, final String parameter) {
        File propFile = new File(APP_PARAM_PROP);
        Properties properties = new Properties();

        if (propFile.exists()) {
            try {
                properties.load(new FileInputStream(propFile));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                        null, ex.getMessage(),
                        "Exception", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        }

        properties.setProperty(key, parameter);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(propFile);
            properties.store(fos, "");
            fos.close();


        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(),
                    "Exception", JOptionPane.ERROR_MESSAGE);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(),
                    "Exception", JOptionPane.ERROR_MESSAGE);

        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(),
                            "Exception", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
