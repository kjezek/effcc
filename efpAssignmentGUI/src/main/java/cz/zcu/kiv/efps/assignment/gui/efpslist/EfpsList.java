package cz.zcu.kiv.efps.assignment.gui.efpslist;

import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;

import javax.swing.JList;
import javax.swing.ListSelectionModel;

import cz.zcu.kiv.efps.assignment.gui.TransferableObject;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForLists;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * List for EFPs.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class EfpsList extends JList {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor for filling list with the content.
     * @param dataModel Data model with EFP items.
     */
    public EfpsList(final EfpsListModel dataModel) {
        super(dataModel);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setCellRenderer(new CellRendererForLists());

        DragSource dragSource = DragSource.getDefaultDragSource();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY,
                new ListDragGestureListener());
    }


    /**
     * @return A data model of the list.
     */
    public EfpsListModel getEfpsListModel() {
        return (EfpsListModel) getModel();
    }


    /**
     * DragGestureListener for dragging items with EFPs.
     *
     * Date: 30.12.2010
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private static class ListDragGestureListener implements DragGestureListener {

        @Override
        public void dragGestureRecognized(final DragGestureEvent dragGestureEvent) {
            JList list = (JList) dragGestureEvent.getComponent();
            EFP efp = (EFP) list.getSelectedValue();

            if  (efp != null) {
                TransferableObject transfObj = new TransferableObject(efp.serialise());

                dragGestureEvent.startDrag(
                        DragSource.DefaultCopyDrop, transfObj, new MyDragSourceListener());
            }
        }
    }


    /**
     * DragSourceListener for dragging nodes with EFPs.
     *
     * Date: 30.12.2010
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private static class MyDragSourceListener implements DragSourceListener {

        @Override
        public void dragEnter(final DragSourceDragEvent dragSourceDragEvent) {
            DragSourceContext context = dragSourceDragEvent.getDragSourceContext();
            int dropAction = dragSourceDragEvent.getDropAction();

            if ((dropAction & DnDConstants.ACTION_COPY) != 0) {
                context.setCursor(DragSource.DefaultCopyDrop);
            } else {
                context.setCursor(DragSource.DefaultCopyNoDrop);
            }
        }

        @Override
        public void dragDropEnd(final DragSourceDropEvent dragSourceDropEvent) { }
        @Override
        public void dragExit(final DragSourceEvent dragSourceEvent) { }
        @Override
        public void dragOver(final DragSourceDragEvent dragSourceDragEvent) { }
        @Override
        public void dropActionChanged(final DragSourceDragEvent dragSourceDragEvent) { }
    }
}
