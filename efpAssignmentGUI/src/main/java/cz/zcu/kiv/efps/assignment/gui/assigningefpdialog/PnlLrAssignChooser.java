package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForLists;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Panel with form for choosing LR assignment of concrete EFP from repository.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class PnlLrAssignChooser extends JPanel {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** Start position of the splitter in a split pane.*/
    private static final double SPLITPANE_START_RESIZE_WEIGHT = 0.4;

    /** Text pane for showing information about selected object. */
    private JTextPane txpDetailInfo;
    /** List with LR assignments. */
    private JList listLrAssignments;
    /** Data model with LR assignment items.*/
    private ListLrAssignModel lrAssignmentsModel;
    /** Tree with LRs. */
    private TreeLRs treeLRs;
    /** EFP which for was created this panel. */
    private EFP efp;


    /**
     * Create the panel.
     */
    public PnlLrAssignChooser() {
        initialization();
    }


    /**
     * Basic initialization of the panel.
     */
    private void initialization() {
        localization = new Localization("PnlLrAssignChooser");
        txpDetailInfo = new JTextPane();

        setLayout(new BorderLayout(0, 0));
        JSplitPane splitPane = new JSplitPane();
        splitPane.setResizeWeight(SPLITPANE_START_RESIZE_WEIGHT);
        add(splitPane, BorderLayout.CENTER);

        JPanel leftPnl = new JPanel();
        leftPnl.setLayout(new BorderLayout(0, 0));
        JScrollPane leftScrollPane = new JScrollPane(leftPnl);
        leftScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        splitPane.setLeftComponent(leftScrollPane);


        JPanel rightPnl = new JPanel();
        rightPnl.setLayout(new BorderLayout(0, 0));
        JScrollPane rightScrollPane = new JScrollPane(rightPnl);
        rightScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        splitPane.setRightComponent(rightScrollPane);

        treeLRs = new TreeLRs();
        treeLRs.addTreeSelectionListener(new TreeLRsSelectionListener());
        leftPnl.add(treeLRs, BorderLayout.CENTER);

        JLabel lblSelectedLocalRegister = new JLabel(localization.getProperty("lblLRs"));
        leftPnl.add(lblSelectedLocalRegister, BorderLayout.NORTH);

        lrAssignmentsModel = new ListLrAssignModel();
        listLrAssignments = new JList(lrAssignmentsModel);
        listLrAssignments.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listLrAssignments.addListSelectionListener(
                new ListLrAssingSelectionListener(txpDetailInfo));
        listLrAssignments.setCellRenderer(new CellRendererForLists());
        rightPnl.add(listLrAssignments, BorderLayout.CENTER);

        JLabel lblLocalRegisterAssignments = new JLabel(localization.getProperty("lblLRAssigns"));
        rightPnl.add(lblLocalRegisterAssignments, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, localization.getProperty("lblInfo"),
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        add(panel, BorderLayout.SOUTH);
        panel.setLayout(new BorderLayout(0, 0));

        panel.add(txpDetailInfo);
    }


    /**
     * Sets EFP and calls method for updating content of panel.
     * @param efp EFP
     */
    public void setEFP(final EFP efp) {
        this.efp = efp;
        treeLRs.setData(efp);
    }


    /**
     * Returns selected LR assignment.
     * @return Selected LR assignment
     */
    public LrAssignment getSelectedLRAssignment() {

        if (listLrAssignments.getSelectedValue() instanceof LrAssignment) {
            return (LrAssignment) listLrAssignments.getSelectedValue();
        }

        return null;
    }


    /**
     * Tree selection listener for tree with LRs.
     *
     * Date: 1.2.2011
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class TreeLRsSelectionListener implements TreeSelectionListener {

        @Override
        public void valueChanged(final TreeSelectionEvent e) {
            DefaultMutableTreeNode selectedNode =
                (DefaultMutableTreeNode) treeLRs.getLastSelectedPathComponent();

            if (selectedNode == null) {
                return;
            }

            if (selectedNode.getUserObject() instanceof LR) {
                LR lr = (LR) selectedNode.getUserObject();
                lrAssignmentsModel.setData(lr, efp);

                txpDetailInfo.setText(lr.toString());
            }
        }

    }
}
