package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.core.AssignmentsTools;
import cz.zcu.kiv.efps.assignment.core.PartialFormulaAssignment;
import cz.zcu.kiv.efps.assignment.gui.AssignmentGUI;
import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.componentstree.ComponentsTree;
import cz.zcu.kiv.efps.assignment.gui.componentstree.ComponentsTreeModel;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.FeatureNode;
import cz.zcu.kiv.efps.assignment.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for creating EFP assignment to concrete feature.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class AssignmentEfpDialog extends JFrame {
    /** ID of panel for set LR assignment to EFP. */
    private static final String PNL_LR_ASSIGN = "lr_assign";
    /** ID of panel for set direct value to EFP. */
    private static final String PNL_DIRECT = "direct";
    /** ID of panel for set math formula to EFP. */
    private static final String PNL_MATH_FORM = "math_form";
    /** ID of panel for set empty value to EFP. */
    private static final String PNL_EMPTY_VALUE = "empty_val";

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** X-coordinate of the frame. */
    private static final int WINDOW_X = 100;
    /** Y-coordinate of the frame. */
    private static final int WINDOW_Y = 100;
    /** Width of the frame. */
    private static final int WINDOW_WIDTH = 520;
    /** Height of the frame. */
    private static final int WINDOW_HEIGHT = 500;
    /** Smaller height of the frame. */
    private static final int WINDOW_HEIGHT_SMALL = 250;
    /** The smallest height of the frame. */
    private static final int WINDOW_HEIGHT_SMALLEST = 150;

    /** Basic insets of a element. */
    private static final int INSETS = 5;

    /** Reference to content panel. */
    private final JPanel contentPanel = new JPanel();

    /** Center panel. */
    private JPanel pnlCenter;

    /** Radio button for choosing LR assignment. */
    private JRadioButton rbLRAssign;
    /** Radio button for choosing math formula. */
    private JRadioButton rbMathFormAssign;
    /** Radio button for choosing direct value. */
    private JRadioButton rbDirectAssign;
    /** Radio button for choosing empty value. */
    private JRadioButton rbEmptyValue;

    /** Text field for text of math formula. */
    private JTextField txfMathFormula;
    /** Feature which for is created new EFP assignment. */
    private FeatureNode editingFeatureNode;
    /** Reference to component which owns the editing feature. */
    private ComponentEFPModifier component;
    /** Assigning EFP. */
    private EFP efp;

    /** EFP value GUI for a form with a direct value. */
    private EfpValueGUI efpValueGUI;

    /** Panel with a form for choosing of a LR assignment. */
    private PnlLrAssignChooser pnlLrAssignChooser;

    /** Component tree which is owner of the editing component. */
    private ComponentsTree componentTree;



    /**
     * Default constructor for setting input data.
     * @param featureNode Node with feature which is editing
     * @param editingEFP EFP for which is created new assignment
     * @param component Reference to component
     * @param componentTree Reference to tree with components
     */
    public AssignmentEfpDialog(final FeatureNode featureNode, final ComponentsTree componentTree,
            final EFP editingEFP, final ComponentEFPModifier component) {
        if (featureNode == null) {
            throw new RuntimeException("Parameter 'featureNode' is null.");
        }
        if (editingEFP == null) {
            throw new RuntimeException("Parameter 'efp' is null.");
        }
        if (component == null) {
            throw new RuntimeException("Parameter 'component' is null.");
        }
        if (componentTree == null) {
            throw new RuntimeException("Parameter 'componentTree' is null.");
        }

        localization = new Localization("AssigningEfpDlg");
        this.editingFeatureNode = featureNode;
        this.componentTree = componentTree;
        this.efp = editingEFP;
        this.component = component;

        initialization();
        rbLRAssign.setSelected(true);
    }


    /**
     * Initializes window.
     */
    private void initialization() {
        this.setTitle(localization.getProperty("title"));
        efpValueGUI = FactoryValueGUI.getValueGUI(efp.getValueType());
        pnlLrAssignChooser = new PnlLrAssignChooser();
        pnlLrAssignChooser.setEFP(efp);
        JPanel pnlDirectVal = createPanelDirectValue();

        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        setResizable(true);

        setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(INSETS, INSETS, INSETS, INSETS));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));

        pnlCenter = new JPanel();
        contentPanel.add(pnlCenter, BorderLayout.CENTER);
        pnlCenter.setLayout(new CardLayout(0, 0));

        pnlCenter.add(pnlLrAssignChooser, PNL_LR_ASSIGN);
        pnlCenter.add(pnlDirectVal, PNL_DIRECT);
        pnlCenter.add(createPanelMathForm(), PNL_MATH_FORM);

        JPanel panel = new JPanel();
        contentPanel.add(panel, BorderLayout.NORTH);

        rbLRAssign = new JRadioButton(localization.getProperty("rbLR"));
        rbLRAssign.addActionListener(new RadioButtonActionListener());
        panel.add(rbLRAssign);

        rbDirectAssign = new JRadioButton(localization.getProperty("rbDirect"));
        rbDirectAssign.addActionListener(new RadioButtonActionListener());
        panel.add(rbDirectAssign);

        rbMathFormAssign = new JRadioButton(localization.getProperty("rbFormula"));
        rbMathFormAssign.addActionListener(new RadioButtonActionListener());
        panel.add(rbMathFormAssign);

        ButtonGroup myButtonGroup = new ButtonGroup();
        myButtonGroup.add(rbLRAssign);
        myButtonGroup.add(rbDirectAssign);
        myButtonGroup.add(rbMathFormAssign);

        if (editingFeatureNode.getFeature().getSide() == AssignmentSide.REQUIRED) {
            pnlCenter.add(new JPanel(), PNL_EMPTY_VALUE);
            rbEmptyValue = new JRadioButton(localization.getProperty("rbEmpty"));
            rbEmptyValue.addActionListener(new RadioButtonActionListener());
            myButtonGroup.add(rbEmptyValue);
            panel.add(rbEmptyValue);
        }

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton okButton = new JButton(localization.getProperty("btnOK"));
        okButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/create.png")));
        okButton.addActionListener(new OkButtonActionListener());

        okButton.setActionCommand(localization.getProperty("btnOK"));
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton btnStorno = new JButton(localization.getProperty("btnStorno"));

        btnStorno.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                AssignmentEfpDialog.this.setVisible(false);
            }
        });

        btnStorno.setActionCommand(localization.getProperty("btnStorno"));
        buttonPane.add(btnStorno);
    }


    /**
     * Creates panel for writing math formula to efp.
     * @return Panel with form
     */
    public JPanel createPanelMathForm() {
        JPanel pnlMathFormAssign = new JPanel();
        pnlMathFormAssign.setLayout(new BorderLayout(0, 0));

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 0));

        txfMathFormula = new JTextField();

        panel.add(txfMathFormula, BorderLayout.NORTH);
        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
                localization.getProperty("lblMathForm"), TitledBorder.LEADING, TitledBorder.TOP,
                null, new Color(0, 0, 0)));

        pnlMathFormAssign.add(panel, BorderLayout.CENTER);

        JPanel pnlEditButton = new JPanel();
        pnlEditButton.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton btnEdit = new JButton(localization.getProperty("btnEditForm"));
        btnEdit.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/edit.png")));
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                EfpMathFormulaDialog efpMathFormDialog =
                    new EfpMathFormulaDialog(txfMathFormula.getText(), component);

                txfMathFormula.setText(efpMathFormDialog.getMathFormula());
                efpMathFormDialog.dispose();
            }
        });

        pnlEditButton.add(btnEdit);
        panel.add(pnlEditButton, BorderLayout.SOUTH);

        return pnlMathFormAssign;
    }


    /**
     * Creates panel for writing direct value to EFP.
     * @return Panel with form
     */
    public JPanel createPanelDirectValue() {
        JPanel pnlDirectValAssign = new JPanel();
        pnlDirectValAssign.setLayout(new BorderLayout(0, 0));

        efpValueGUI = FactoryValueGUI.getValueGUI(efp.getValueType());

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 0));
        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
                localization.getProperty("lblDirectVal"), TitledBorder.LEADING, TitledBorder.TOP,
                null, new Color(0, 0, 0)));
        panel.add(efpValueGUI, BorderLayout.NORTH);


        pnlDirectValAssign.add(panel, BorderLayout.NORTH);

        return pnlDirectValAssign;
    }


    /**
     * ActionListener of radio buttons for choosing center panel in dialog.
     *
     * Date: 31.12.2010
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class RadioButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            Point location = AssignmentEfpDialog.this.getLocationOnScreen();

            if (e.getSource() == rbLRAssign && rbLRAssign.isSelected()) {
                ((CardLayout) pnlCenter.getLayout()).show(pnlCenter, PNL_LR_ASSIGN);
                AssignmentEfpDialog.this.setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);

            } else if (e.getSource() == rbDirectAssign && rbDirectAssign.isSelected()) {
                ((CardLayout) pnlCenter.getLayout()).show(pnlCenter, PNL_DIRECT);
                AssignmentEfpDialog.this.setBounds(
                        WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT_SMALL);

            } else if (e.getSource() == rbMathFormAssign  && rbMathFormAssign.isSelected()) {
                ((CardLayout) pnlCenter.getLayout()).show(pnlCenter, PNL_MATH_FORM);
                AssignmentEfpDialog.this.setBounds(
                        WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT_SMALL);

            } else if (e.getSource() == rbEmptyValue  && rbEmptyValue.isSelected()) {
                ((CardLayout) pnlCenter.getLayout()).show(pnlCenter, PNL_EMPTY_VALUE);
                AssignmentEfpDialog.this.setBounds(
                        WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT_SMALLEST);
            }

            AssignmentEfpDialog.this.setLocation(location);
        }
    }


    /**
     * ActionListener for confirm button this dialog. Checks if new efp assigned value is OK.
     *
     * Date: 31.12.2010
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class OkButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            Feature editingFeature = editingFeatureNode.getFeature();
            EfpAssignedValue efpAssignedValue = null;


            //LR assignment selected
            if (rbLRAssign.isSelected()) {
                LrAssignment lrAssignment = pnlLrAssignChooser.getSelectedLRAssignment();

                if (lrAssignment == null) {
                    throw new GuiRTException(localization.getProperty("ERR_LR_ASSIGN"));

                } else {
                    if (component.getAssignedValue(editingFeature, efp,
                            lrAssignment.getLr()) != null) {
                        throw new GuiRTException(localization.getProperty("ERR_LR_ASSIGN_EXIST"));
                    }

                    efpAssignedValue = createEfpNamedValue(editingFeature, lrAssignment);
                }

            //direct value selected
            } else if (rbDirectAssign.isSelected()) {

                if (component.getAssignedValue(editingFeature, efp, null) != null) {
                    throw new GuiRTException(localization.getProperty("ERR_OTHER_VAL_EXIST"));
                }

                try {
                    efpAssignedValue = new EfpDirectValue(efpValueGUI.getValue());
                } catch (IllegalValueException e1) {
                    throw new GuiRTException(e1.getMessage());
                }

            //mathematical value selected
            } else if (rbMathFormAssign.isSelected()) {
                if (txfMathFormula.getText().trim().equals("")) {
                    throw new GuiRTException(localization.getProperty("ERR_EMPTY_FORM"));
                }
                if (component.getAssignedValue(editingFeature, efp, null) != null) {
                    throw new GuiRTException(localization.getProperty("ERR_OTHER_VAL_EXIST"));
                }

                PartialFormulaAssignment pfa = new PartialFormulaAssignment(
                        null, null, txfMathFormula.getText().trim(), component);
                efpAssignedValue = pfa.createEfpMathFormula();

            //empty value selected
            } else if (rbEmptyValue != null && rbEmptyValue.isSelected()) {
                if (AssignmentsTools.containEFP(
                        component.getEfps(editingFeature), efp)) {
                    throw new GuiRTException(localization.getProperty("ERR_EMPTY_VAL"));
                }
            }


            component.assignEFP(editingFeature, efp, efpAssignedValue);
            ComponentsTreeModel dataModel = (ComponentsTreeModel) (componentTree.getModel());
            dataModel.addNewEfpAssignedValue(efp, editingFeatureNode, efpAssignedValue);
            dataModel.reload();
            componentTree.expandAll();

            AssignmentEfpDialog.this.dispose();
        }
    }


    /**
     * Creates instance of EfpNamedValue.
     * @param editingFeature A editing feature
     * @param lrAssignment A LR assignment for new EFP named value
     * @return Created EFP named value
     */
    public static EfpNamedValue createEfpNamedValue(
            final Feature editingFeature, final LrAssignment lrAssignment) {

        EfpNamedValue efpNamedValue = null;

        if (lrAssignment instanceof LrSimpleAssignment) {
            efpNamedValue = new EfpNamedValue(lrAssignment);

        } else if (lrAssignment instanceof LrDerivedAssignment) {
            efpNamedValue = (EfpNamedValue) setAssignmentWithLrDerivedAssign(
                    editingFeature,
                    (LrDerivedAssignment) lrAssignment);


        } else {
            throw new RuntimeException("Unknown type of LR assignment.");
        }

        return efpNamedValue;
    }

    /**
     * Builds EFP assignment from LR derived assignment. In logical formula
     * finds all EFP and participating LR assignments.
     * @param feature Feature which for is creating the EFP assignment.
     * @param lrDerivedAssignment LR derived assignment.
     * where will be searched participating LR assignments.
     * @return EFP assignment with the LR derived assignment.
     */
    public static EfpAssignedValue setAssignmentWithLrDerivedAssign(
            final Feature feature, final LrDerivedAssignment lrDerivedAssignment) {

        List<EfpFeature> efpFeatures = new ArrayList<EfpFeature>();
        List<LrSimpleAssignment> lrPartSimpAssignments = new ArrayList<LrSimpleAssignment>();

        List<EFP> efps = ((DerivedEFP) lrDerivedAssignment.getEfp()).getEfps();
        String formula = lrDerivedAssignment.getEvaluator().serialise();
        int lrId = lrDerivedAssignment.getLr().getId();


        if (formula != null && !formula.equals("")) {
            for (EFP particuralEfp : efps) {
                if (formula.indexOf(particuralEfp.getName()) != -1) {
                    efpFeatures.add(new EfpFeature(particuralEfp, feature));

                    List<LrAssignment> lrAssignmentsByPartEFP = null;

                    lrAssignmentsByPartEFP =
                        AssignmentGUI.getEfpClient().findLrAssignments(particuralEfp.getId(), lrId);

                    for (LrAssignment lrAssignment : lrAssignmentsByPartEFP) {
                        String key = particuralEfp.getName() + "_" + lrAssignment.getValueName();

                        if (formula.indexOf(key) != -1) {
                            lrPartSimpAssignments.add((LrSimpleAssignment) lrAssignment);
                        }
                    }
                }
            }
        }

        return new EfpNamedValue(
                lrDerivedAssignment,
                efpFeatures,
                lrPartSimpAssignments.toArray(new LrSimpleAssignment[0]));
    }

}
