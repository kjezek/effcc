package cz.zcu.kiv.efps.assignment.gui.itemtypes;

/**
 * Object for store type of supported component.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentImplItem {
    /** Name of type component. */
    private String nameOfType;
    /** Path to implementation. */
    private String path;



    /**
     * Default constructor.
     * @param nameOfType Name of type component
     * @param path Path to implementation
     */
    public ComponentImplItem(final String nameOfType, final String path) {
        this.nameOfType = nameOfType;
        this.path = path;
    }


    /**
     * @return Name of type component.
     */
    public String getNameOfType() {
        return nameOfType;
    }


    /**
     * @return Path to implementation
     */
    public String getPath() {
        return path;
    }


    @Override
    public String toString() {
        return nameOfType + " - " + path;
    }
}
