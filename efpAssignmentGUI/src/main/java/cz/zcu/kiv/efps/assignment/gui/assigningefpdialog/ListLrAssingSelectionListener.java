package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cz.zcu.kiv.efps.types.lr.LrAssignment;

/**
 * List selection listener for list with LR assignments.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ListLrAssingSelectionListener implements ListSelectionListener {

    /** Target text pane for showing information about selected item. */
    private JTextPane targetTextPane;


    /**
     * Default constructor.
     * @param targetTextPane Target text pane.
     */
    public ListLrAssingSelectionListener(final JTextPane targetTextPane) {
        this.targetTextPane = targetTextPane;
    }

    @Override
    public void valueChanged(final ListSelectionEvent listSelectionEvent) {
        boolean adjust = listSelectionEvent.getValueIsAdjusting();

        if (!adjust) {
            JList list = (JList) listSelectionEvent.getSource();

            if (list.getSelectedValue() instanceof LrAssignment) {
                LrAssignment lrAssign = (LrAssignment) list.getSelectedValue();

                targetTextPane.setText(lrAssign.getAssignmentType() + " LR assignment:\n"
                        + lrAssign.getValueName() + " = " + lrAssign.getLabel());
            } else {
                targetTextPane.setText(" ");
            }
        }
    }

}
