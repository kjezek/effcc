package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.util.List;

import javax.swing.DefaultListModel;

import cz.zcu.kiv.efps.assignment.gui.AssignmentGUI;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Data model for list with LR assignments.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ListLrAssignModel extends DefaultListModel {

    /** Serial version. */
    private static final long serialVersionUID = 1L;



    /** Default constructor. */
    public ListLrAssignModel() { }


    /**
     * Fills the data model with LR assingments of concrete LR.
     * @param lr LR
     * @param efp EFP
     */
    public void setData(final LR lr, final EFP efp) {
        clear();

        List<LrAssignment> lrAssigns =
        AssignmentGUI.getEfpClient().findLrAssignments(efp.getId(), lr.getId());

        for (LrAssignment lrAssignment : lrAssigns) {
            addElement(lrAssignment);
        }
    }


    /**
     * Fills the data model with LR assingments of concrete LR
     * and finds if contains required LR assignment.
     * @param lr LR
     * @param efp EFP
     * @param selectedLrAssignment Selected LR assignment
     * @return Index of selected LR assignment in model
     */
    public int setData(final LR lr, final EFP efp, final LrAssignment selectedLrAssignment) {
        clear();

        List<LrAssignment> lrAssigns =
        AssignmentGUI.getEfpClient().findLrAssignments(efp.getId(), lr.getId());
        int selectedItem = 0;


        for (LrAssignment lrAssignment : lrAssigns) {
            addElement(lrAssignment);

            if (selectedLrAssignment.equals(lrAssignment)) {
                selectedItem = size() - 1;
            }
        }

        return selectedItem;
    }
}
