package cz.zcu.kiv.efps.assignment.gui.exceptions;

import java.util.Arrays;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.assignment.core.AssignmentRTException;
import cz.zcu.kiv.efps.assignment.cosi.CoSiAssignmentRTException;

/**
 * The class catch uncaught exception and show it on the GUI screen. <br>
 * <br>
 * <b>Date:</b> 22.2.2011
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    /** Maximum rows of stack trace which will showed on screen. */
    private static final int MAX_TRACE_ON_SCREEN = 20;

    @Override
    public void uncaughtException(final Thread t, final Throwable e) {
        if (SwingUtilities.isEventDispatchThread()) {
            showException(t, e);
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    showException(t, e);
                }
            });
        }
    }

    /**
     * UncaughtException for modal dialogs.
     * @param t Throwable.
     */
    public void handle(final Throwable t) {
        uncaughtException(Thread.currentThread(), t);
    }

    /**
     * Show uncaught exception to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showException(final Thread t, final Throwable e) {

        if (e instanceof AssignmentRTException) {
            processException(t, e);
            return;
        }

        if (e instanceof CoSiAssignmentRTException) {
            processException(t, e);
            return;
        }

        if (e instanceof GuiRTException) {
            processGuiException(t, e);
            return;
        }

        showFatalException(t, e);
    }

    /**
     * Show uncaught exception to GUI.
     * @param t Thread, where exception throws.
     * @param e Uncaught exception.
     */
    private void showFatalException(final Thread t, final Throwable e) {
        logger.error("NO EXPECTED ERROR:", e);
        JOptionPane.showMessageDialog(null,
                Arrays.copyOfRange(e.getStackTrace(), 0, MAX_TRACE_ON_SCREEN),
                "FATAL ERROR", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Process exception.
     * @param t A thread
     * @param e An exception
     */
    private static void processException(final Thread t, final Throwable e) {
        logger.warn("Unexpected exception: ", e);
        JOptionPane.showMessageDialog(null, e.getMessage(), "Unexpected error",
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Process exception from GUI.
     * @param t A thread
     * @param e An exception
     */
    private static void processGuiException(final Thread t, final Throwable e) {
        logger.warn("Unexpected exception: ", e);
        JOptionPane.showMessageDialog(null, e.getMessage(), "Error",
                JOptionPane.WARNING_MESSAGE);
    }
}
