package cz.zcu.kiv.efps.assignment.gui;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import cz.zcu.kiv.efps.assignment.gui.exceptions.ExceptionHandler;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.itemtypes.ComponentImplItem;

/**
 * Class which open application.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public final class EfpAssignmentLauncher {

    /** Private default constructor. */
    private EfpAssignmentLauncher() { }


    /**
     * Launch the application.
     * @param args Parameters
     */
    public static void main(final String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler", ExceptionHandler.class.getName());

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    try {
                        initLookAndFeel();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    AssignmentGUI gui = null;

                    String cmName =
                        AppParametersProperties.getParameterValue("comp_impl_name");
                    String cmPath =
                        AppParametersProperties.getParameterValue("comp_impl_path");

                    if (cmName != null && cmPath != null) {
                        gui = AssignmentGUI.getAssignmentGUI();
                        gui.setNameOfCompImpl(cmName);
                        gui.setPathToCompImpl(cmPath);
                    } else {
                        gui = getGUIWithSeletectedCompLoader();
                    }


                    if (gui != null) {
                        gui.setVisible(true);
                    }

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

        });
    }

    /**
     * Initialize look&feel.
     * @throws UnsupportedLookAndFeelException Unsupported look and feel.
     * @throws IllegalAccessException Access to look and feel is available.
     * @throws InstantiationException Look and feel cannot use.
     * @throws ClassNotFoundException Look and feel not exists.
     */
    private static void initLookAndFeel() throws UnsupportedLookAndFeelException,
            ClassNotFoundException, InstantiationException, IllegalAccessException {

        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
    }


    /**
     * Opens dialog for selecting component model implementation. Creates and sets assignment GUI.
     * @return Created assignment GUI
     */
    private static AssignmentGUI getGUIWithSeletectedCompLoader() {
        AssignmentGUI gui = null;

        List<ComponentImplItem> listCompImplItems = ComponentImplsProperties.getComponentLoaders();

        if (listCompImplItems.size() > 1) {
            ComponentTypeDialog compTypeDlg = new ComponentTypeDialog();
            compTypeDlg.fillListComponentType(listCompImplItems);
            compTypeDlg.setVisible(true);

            if (compTypeDlg.getIsDialogConfirm()) {
                gui = AssignmentGUI.getAssignmentGUI();

                gui.setNameOfCompImpl(
                        compTypeDlg.getNameOfCompImpl());
                gui.setPathToCompImpl(
                        compTypeDlg.getPathToCompImpl());
            }

        } else if (listCompImplItems.size() == 1) {
            gui = AssignmentGUI.getAssignmentGUI();

            String nameOfImpl = listCompImplItems.get(0).getNameOfType();
            String pathToImpl = listCompImplItems.get(0).getPath();

            gui.setNameOfCompImpl(nameOfImpl);
            gui.setPathToCompImpl(pathToImpl);

            AppParametersProperties.saveParameter("comp_impl_name", nameOfImpl);
            AppParametersProperties.saveParameter("comp_impl_path", pathToImpl);

        } else {
            throw new GuiRTException(
                    new Localization("efpAssignmentGUI").getProperty("ERR_PROPERTY_LOADERS"));
        }

        return gui;
    }
}
