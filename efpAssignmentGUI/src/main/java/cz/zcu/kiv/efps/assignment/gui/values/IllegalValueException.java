package cz.zcu.kiv.efps.assignment.gui.values;

/**
 * Exception if vaulue is not valid. <br>
 * <br>
 * <b>Date:</b> 17.11.2010
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class IllegalValueException extends Exception {

    /** Serial version. */
    private static final long serialVersionUID = 1270675801446897813L;

    /**
     * Create exception.
     * @param e describe exception
     */
    public IllegalValueException(final String e) {
        super(e);
    }

}
