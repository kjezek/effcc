package cz.zcu.kiv.efps.assignment.gui.componentstree;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.FeatureNode;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Extended {@link DefaultTreeModel} for adding nodes {@link ComponentNode}.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentsTreeModel extends DefaultTreeModel {

    /**Serial version UID.*/
    private static final long serialVersionUID = 1L;



    /**
     * Default constructor.
     */
    public ComponentsTreeModel() {
        super(new DefaultMutableTreeNode("root"));
    }


    /**
     * Deletes all data.
     */
    public void clear() {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.getRoot();

        root.removeAllChildren();
        this.reload();
    }


    /**
     * Adds into root of model new node with opened component.
     * @param componentNode Node with component
     */
    public void addComponentNode(final DefaultMutableTreeNode componentNode) {
        ((DefaultMutableTreeNode) this.getRoot()).add(componentNode);
    }


    /**
     * Adds to feature node new EFP node if it doesn't exist. Then to efp node
     * adds new assigned value.
     * @param efp Assigned EFP
     * @param parent Parent node with feature
     * @param efpAssignedValue New EFP assigned value
     */
    public void addNewEfpAssignedValue(final EFP efp,
            final FeatureNode parent, final EfpAssignedValue efpAssignedValue) {

        for (int index = 0; index < parent.getChildCount(); index++) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent.getChildAt(index);

            if (node.getUserObject() instanceof EFP) {
                EFP efpInNode = (EFP) node.getUserObject();

                if (efpInNode.getName().equals(efp.getName())) {
                    node.add(new DefaultMutableTreeNode(efpAssignedValue));
                    return;
                }
            }
        }

        DefaultMutableTreeNode efpNode = new DefaultMutableTreeNode(efp);
        if (efpAssignedValue != null) {
            efpNode.add(new DefaultMutableTreeNode(efpAssignedValue));
        }
        parent.add(efpNode);
    }


    /**
     * Finds all nodes with components and calls their methods for saving them.
     */
    public void saveAllComponents() {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.getRoot();

        for (int index = 0; index < root.getChildCount(); index++) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(index);

            if (node instanceof ComponentNode) {
                ((ComponentNode) node).getComponent().close();
            }
        }
    }


    /**
     * Finds all nodes with components and calls their methods for closing them.
     */
    public void closeAllComponents() {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.getRoot();

        for (int index = 0; index < root.getChildCount(); index++) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(index);

            if (node instanceof ComponentNode) {
                ((ComponentNode) node).getComponent().close();
            }
        }

        root.removeAllChildren();
    }


    /**
     * Checks if component tree is empty.
     * @return True is empty, otherwise false.
     */
    public boolean isEmpty() {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.getRoot();

        return root.getChildCount() == 0;
    }
}
