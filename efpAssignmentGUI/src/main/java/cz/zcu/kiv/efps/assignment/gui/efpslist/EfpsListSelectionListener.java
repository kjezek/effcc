package cz.zcu.kiv.efps.assignment.gui.efpslist;

import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * List selection listener for list with efps.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class EfpsListSelectionListener implements ListSelectionListener {

    /** Target text pane for showing information about selected item. */
    private JTextPane targetTextPane;

    /**
     * Default constructor.
     * @param targetTextPane Target text pane.
     */
    public EfpsListSelectionListener(final JTextPane targetTextPane) {
        this.targetTextPane = targetTextPane;
    }

    @Override
    public void valueChanged(final ListSelectionEvent listSelectionEvent) {
        boolean adjust = listSelectionEvent.getValueIsAdjusting();

        if (!adjust) {
            JList list = (JList) listSelectionEvent.getSource();
            EFP efp = (EFP) list.getSelectedValue();

            if (efp == null) {
                targetTextPane.setText(" ");
            } else {
                targetTextPane.setText(efp.getType()
                        + " EFP:\n" + efp.toString());
            }
        }
    }

}
