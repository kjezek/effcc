package cz.zcu.kiv.efps.assignment.gui.cellrenderers;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;

/**
 * Cell renderer for trees with opened components and LRs.
 *
 * Date: 2.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CellRendererForTrees extends DefaultTreeCellRenderer {

    /** Serial version. */
    private static final long serialVersionUID = 1L;


    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value,
            final boolean selected, final boolean expanded, final boolean leaf,
            final int row, final boolean hasFocus) {

        super.getTreeCellRendererComponent(
                tree, value, selected, expanded, leaf, row, hasFocus);

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        String label = null;

        if (node instanceof ComponentNode) {
            label = ((ComponentNode) node).getComponentName();
            ImageIcon image = new ImageIcon(
                    getClass().getClassLoader().getResource("images/componentIcon.png"));
            setIcon(image);
        }


        if (node.getUserObject() instanceof Feature) {
            Feature feature = (Feature) node.getUserObject();

            label = feature.getIdentifier();
            ImageIcon image = null;

            if (feature.getSide() == AssignmentSide.PROVIDED) {
                image = new ImageIcon(
                        getClass().getClassLoader().getResource("images/feature_provided.png"));
            } else {
                image = new ImageIcon(
                        getClass().getClassLoader().getResource("images/feature_required.png"));
            }
            setIcon(image);
        }

        if (node.getUserObject() instanceof LR) {
            label = ((LR) node.getUserObject()).getName();
            setIcon(new ImageIcon(
                    getClass().getClassLoader().getResource("images/LrIcon.png")));
        }

        if (node.getUserObject() instanceof LrAssignment) {
            LrAssignment lrAssign = (LrAssignment) node.getUserObject();

            label = lrAssign.getValueName() + " = " + lrAssign.getLabel();
        }


        if (node.getUserObject() instanceof EfpAssignedValue) {
            EfpAssignedValue efpAssignedValue = (EfpAssignedValue) node.getUserObject();

            if (efpAssignedValue instanceof EfpNamedValue) {
                EfpNamedValue efpNamedValue = (EfpNamedValue) efpAssignedValue;

                label = efpNamedValue.getLR().getName() + ": "
                + efpNamedValue.getLrAssignment().getValueName()
                + " = "
                + efpNamedValue.getLrAssignment().getLabel();

            } else if (efpAssignedValue instanceof EfpFormulaValue) {
                label = "Math formula";

            } else if ((efpAssignedValue instanceof EfpDirectValue)) {
                label = ((EfpDirectValue) efpAssignedValue).getValue().getLabel();
            }

            setIcon(null);
        }

        if (node.getUserObject() instanceof EFP) {
            EFP efp = (EFP) node.getUserObject();
            label = efp.getName();
            ImageIcon image = null;

            if (efp.getType() == EfpType.SIMPLE) {
                image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/efpSimpleIcon.png"));
            } else {
                image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/efpDerivedIcon.png"));
            }
            setIcon(image);
        }

        this.setText(label);

        return this;
    }
}
