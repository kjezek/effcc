package cz.zcu.kiv.efps.assignment.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible number interval value. <br>
 * Date: 9. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpNumberIntervalGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** JTextField for low value of the interval. */
    private JTextField tfLow;

    /** JTextField for high value of the interval. */
    private JTextField tfHigh;

    /** Label of name of type. */
    private JLabel lblType;

    /**
     * Create the panel.
     */
    EfpNumberIntervalGUI() {
        super();
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.insets = new Insets(0, 5, 0, 10);
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        lblType = new JLabel(bundle.getProperty("lblNumberInterval"));
        add(lblType, gbc);

        gbc.insets = new Insets(0, 5, 0, 0);
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 1;
        gbc.weightx = 0.0;
        add(new JLabel(bundle.getProperty("fromInt")), gbc);

        tfLow = new JTextField();
        tfLow.setToolTipText(bundle.getProperty("toolTipLowInt"));
        gbc.gridx = 2;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(tfLow, gbc);

        gbc.gridx = 3;
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        add(new JLabel(bundle.getProperty("toInt")), gbc);

        tfHigh = new JTextField();
        tfHigh.setToolTipText(bundle.getProperty("toolTipHighInt"));
        gbc.gridx = 4;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(tfHigh, gbc);

    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        if (tfLow.getText().equals("") || tfHigh.getText().equals("")) {
            throw new IllegalValueException(
                    bundle.getProperty("ERR_FILL_VALUE_INTERVAL"));
        }
        try {
            double low = Double.parseDouble(tfLow.getText());
            double high = Double.parseDouble(tfHigh.getText());
            if (low > high) {
                throw new IllegalValueException(
                        bundle.getProperty("ERR_LOW_BIGER") + "\n\"["
                                + tfLow.getText() + ";" + tfHigh.getText() + "]\"");
            }
            return new EfpNumberInterval(low, high);

        } catch (NumberFormatException e) {
            throw new IllegalValueException(
                    bundle.getProperty("ERR_NOT_NUMBER_INTERVAL") + "\n\"["
                            + tfLow.getText() + ";" + tfHigh.getText() + "]\"");
        }
    }

    @Override
    public void setValue(final EfpValueType value) {
        tfLow.setText(((EfpNumberInterval) value).getLow().getLabel());
        tfHigh.setText(((EfpNumberInterval) value).getHigh().getLabel());

    }

    /* (non-Javadoc)
     *
     * @see values.EfpValueGUI#lock() */
    @Override
    public void lock() {
        tfLow.setEditable(false);
        tfHigh.setEditable(false);
    }
}
