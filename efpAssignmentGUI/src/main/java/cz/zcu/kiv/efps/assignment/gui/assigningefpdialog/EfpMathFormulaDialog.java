package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;

import cz.zcu.kiv.efps.assignment.api.ComponentEfpAccessor;
import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForLists;
import cz.zcu.kiv.efps.assignment.types.EfpFeature;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.types.Feature.AssignmentSide;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for creating and editing math formula. <br>
 * <br>
 * <b>Date:</b> 15.2.2011
 *
 * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpMathFormulaDialog extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = -7802363719059960317L;

    /** Width of frame. */
    private static final int WIDTH = 850;

    /** Height of frame. */
    private static final int HEIGHT = 500;

    /** Width of columns with lists. */
    private static final int LIST_COLUMN_WIDTH = 200;

    /** Array of Math functions. */
    private static final String[] FUNCTIONS = new String[] {"sin()", "cos()", "tan()", "pow(, )",
            "sqrt()", "abs()", "ceil()", "floor()", "exp()", "log()", "round()", "min(, )",
            "max(, )", "acos()", "asin()", "atan()"};

    /** ComponentEfpAccessor for access to efp and feature. */
    private ComponentEfpAccessor accessor;

    /** If Checkbox is checked, only used features are showed. */
    private JCheckBox cbShowUsed;

    /** Properties file with localization. */
    private Localization bundle = new Localization("mathFormulaGUI");

    /** A string with old math.formula. */
    private String oldFormula;

    /** Formula editor. */
    private JTextArea taFormula;

    /** List of efps. */
    private JList efpsList;

    /** List of math functions. */
    private JList mathList;

    /** List of value names of efp chosen in efpsList. */
    private JList featureList;

    /** Map of features and their vars. */
    private Map<Feature, String> featureVars;

    /**
     * Create the dialog on position and with the existing formula.
     *
     * @param formula
     *            A string with math.formula.
     *
     * @param accessor
     *            Accessor to efp and feature.
     */
    public EfpMathFormulaDialog(final String formula, final ComponentEfpAccessor accessor) {
        setSize(WIDTH, HEIGHT);
        setTitle(bundle.getProperty("caption"));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);

        this.accessor = accessor;
        initFeatureVars();
        init();
        this.oldFormula = setMathFormula(formula);
        load();
        setVisible(true);
    }

    /**
     * Create the dialog on position and with the existing formula.
     *
     * @param accessor
     *            Accessor to efp and feature.
     */
    public EfpMathFormulaDialog(final ComponentEfpAccessor accessor) {
        this("", accessor);
    }

    /** Initialization of dialog. */
    private void init() {
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                Object[] options = new Object[] {bundle.getProperty("yes"),
                        bundle.getProperty("no"), bundle.getProperty("cancel")};
                int value = JOptionPane.showOptionDialog(null,
                        bundle.getProperty("formula_close_quest"), "Save a formula",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                        options, options[0]);

                if (value == JOptionPane.YES_OPTION) {
                    exitDialog();
                }

                if (value == JOptionPane.NO_OPTION) {
                    taFormula.setText(oldFormula);
                    exitDialog();
                }
            }

        });
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] {WIDTH - 120 - 2 * LIST_COLUMN_WIDTH, 120,
                LIST_COLUMN_WIDTH, LIST_COLUMN_WIDTH, 0};
        gridBagLayout.columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[] {0.0, 0.0, 1.0, 0.0};
        getContentPane().setLayout(gridBagLayout);

        JScrollPane spFormula = new JScrollPane();
        GridBagConstraints gbcSpFormula = new GridBagConstraints();
        gbcSpFormula.gridheight = 4;
        gbcSpFormula.insets = new Insets(0, 0, 5, 5);
        gbcSpFormula.fill = GridBagConstraints.BOTH;
        gbcSpFormula.gridx = 0;
        gbcSpFormula.gridy = 0;
        spFormula.setViewportView(getFormula());
        getContentPane().add(spFormula, gbcSpFormula);

        ImageIcon image = new ImageIcon(getClass().getClassLoader().getResource(
                "images/otaznik.jpg"));
        JLabel lblFormulaHelp = new JLabel(image);
        lblFormulaHelp.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                JOptionPane.showMessageDialog(null,
                        "<html><body><div width=\"250\">" + bundle.getProperty("help")
                                + "</div></body></html>", "Help", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        GridBagConstraints gbcFormulaHelp = new GridBagConstraints();
        gbcFormulaHelp.anchor = GridBagConstraints.WEST;
        gbcFormulaHelp.fill = GridBagConstraints.NONE;
        gbcFormulaHelp.insets = new Insets(0, 0, 5, 0);
        gbcFormulaHelp.gridx = 1;
        gbcFormulaHelp.gridy = 0;
        getContentPane().add(lblFormulaHelp, gbcFormulaHelp);

        GridBagConstraints gbcLogicalBtn = new GridBagConstraints();
        gbcLogicalBtn.gridwidth = 3;
        gbcLogicalBtn.fill = GridBagConstraints.NONE;
        gbcLogicalBtn.insets = new Insets(0, 0, 5, 0);
        gbcLogicalBtn.gridx = 1;
        gbcLogicalBtn.gridy = 1;
        getContentPane().add(getLogicalButtons(), gbcLogicalBtn);

        GridBagConstraints gbcMathList = new GridBagConstraints();
        gbcMathList.fill = GridBagConstraints.BOTH;
        gbcMathList.insets = new Insets(0, 0, 5, 0);
        gbcMathList.gridx = 1;
        gbcMathList.gridy = 2;
        getContentPane().add(getMathList(), gbcMathList);

        GridBagConstraints gbcEfpsList = new GridBagConstraints();
        gbcEfpsList.fill = GridBagConstraints.BOTH;
        gbcEfpsList.insets = new Insets(0, 0, 5, 0);
        gbcEfpsList.gridx = 2;
        gbcEfpsList.gridy = 2;
        getContentPane().add(getFeatureList(), gbcEfpsList);

        GridBagConstraints gbcNamesList = new GridBagConstraints();
        gbcNamesList.fill = GridBagConstraints.BOTH;
        gbcNamesList.insets = new Insets(0, 0, 5, 0);
        gbcNamesList.gridx = 3;
        gbcNamesList.gridy = 2;
        getContentPane().add(getEfpsList(), gbcNamesList);

        addControlOfLists();

        GridBagConstraints gbcBtnSave = new GridBagConstraints();
        gbcBtnSave.gridwidth = 4;
        gbcBtnSave.gridx = 0;
        gbcBtnSave.gridy = 4;
        getContentPane().add(getControlPanel(), gbcBtnSave);

    }

    /**
     * Return a control panel with save button.
     *
     * @return A control panel.
     */
    private Container getControlPanel() {
        JPanel panel = new JPanel();

        JButton btnSave = new JButton(bundle.getProperty("save"));
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                exitDialog();
            }
        });
        panel.add(btnSave);

        JButton btnStorno = new JButton(bundle.getProperty("storno"));
        btnStorno.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.setText(oldFormula);
                exitDialog();
            }
        });
        panel.add(btnStorno);

        JButton btnTest = new JButton(bundle.getProperty("test"));
        btnTest.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                test();
            }
        });
        panel.add(btnTest);

        return panel;
    }

    /**
     * Create the list with Math functions.
     *
     * @return the formula editor
     */
    private Container getMathList() {
        JScrollPane scrollPaneMath = new JScrollPane();
        mathList = new JList(new DefaultListModel());
        mathList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && mathList.getSelectedValue() != null) {
                    taFormula.insert(" " + mathList.getSelectedValue() + " ",
                            taFormula.getCaretPosition());
                    taFormula.requestFocus();

                }
            }

            @Override
            public void mouseReleased(final MouseEvent e) {
                efpsList.setListData(new Object[] {});
                efpsList.clearSelection();
                featureList.clearSelection();
            }
        });
        Arrays.sort(FUNCTIONS);
        mathList.setListData(FUNCTIONS);
        scrollPaneMath.setViewportView(mathList);
        return scrollPaneMath;

    }

    /**
     * Create the list of efps.
     *
     * @return the formula editor
     */
    private Container getEfpsList() {
        JScrollPane scrollPaneEfps = new JScrollPane();
        efpsList = new JList(new DefaultListModel());
        efpsList.setCellRenderer(new CellRendererForLists());
        efpsList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                if (e.getClickCount() == 2 && efpsList.getSelectedValue() != null
                        && featureList.getSelectedValue() != null) {
                    taFormula.insert(" " + featureVars.get(featureList.getSelectedValue()) + "_"
                            + ((EFP) efpsList.getSelectedValue()).getName() + " ",
                            taFormula.getCaretPosition());
                    taFormula.requestFocus();
                }

            }

            @Override
            public void mouseReleased(final MouseEvent e) {
                mathList.clearSelection();
            }
        });
        scrollPaneEfps.setViewportView(efpsList);
        return scrollPaneEfps;

    }

    /**
     * Create the list of value names.
     *
     * @return the formula editor
     */
    private Container getFeatureList() {
        JScrollPane scrollPaneNames = new JScrollPane();
        featureList = new JList(new DefaultListModel());
        featureList.setCellRenderer(new DefaultListCellRenderer() {

            private static final long serialVersionUID = 7569171632710325169L;

            @Override
            public Component getListCellRendererComponent(final JList list, final Object value,
                    final int index, final boolean isSelected, final boolean cellHasFocus) {
                String val = ((Feature) value).getName();
                if (featureVars.containsKey(value)) {
                    val = featureVars.get(value) + ": " + val;
                }
                JLabel label = (JLabel) super.getListCellRendererComponent(list, val, index,
                        isSelected, cellHasFocus);

                if (((Feature) value).getSide() == AssignmentSide.PROVIDED) {
                    label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                            "images/feature_provided.png")));
                } else {
                    label.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                            "images/feature_required.png")));
                }
                return label;
            }
        });
        featureList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        featureList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                efpsList.setListData(accessor.getEfps(
                        (Feature) featureList.getSelectedValue()).toArray());
                efpsList.clearSelection();
                mathList.clearSelection();
            }
        });
        scrollPaneNames.setViewportView(featureList);
        return scrollPaneNames;

    }

    /**
     * Create the Formula editor.
     *
     * @return the formula editor
     */
    private Container getFormula() {
        taFormula = new JTextArea();
        taFormula.setLineWrap(true);
        taFormula.setWrapStyleWord(true);
        taFormula.setToolTipText("<html><body><div width=\"250\">" + bundle.getProperty("help")
                + "</div></body></html>");
        return taFormula;

    }

    /**
     * Create panel with buttons for writing formula.
     *
     * @return Panel with buttons.
     */
    private Container getLogicalButtons() {
        JPanel panel = new JPanel();

        JButton btnPlus = new JButton("+");
        btnPlus.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" + ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnPlus);

        JButton btnMinus = new JButton("-");
        btnMinus.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" - ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnMinus);

        JButton btnMul = new JButton("*");
        btnMul.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" * ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnMul);

        JButton btnDiv = new JButton("/");
        btnDiv.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" / ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnDiv);

        JButton btnLeftBrac = new JButton("(");
        btnLeftBrac.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" ( ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnLeftBrac);

        JButton btnRightBrac = new JButton(")");
        btnRightBrac.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                taFormula.insert(" ) ", taFormula.getCaretPosition());
                taFormula.requestFocus();
            }
        });
        panel.add(btnRightBrac);

        return panel;
    }

    /**
     * Add control components for lists (features, efp, math function).
     */
    private void addControlOfLists() {
        cbShowUsed = new JCheckBox(bundle.getProperty("used"));
        cbShowUsed.setSelected(true);
        cbShowUsed.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                featureList.setListData(getArrayOfFeature());
                efpsList.setListData(new Object[] {});
                efpsList.clearSelection();
                featureList.clearSelection();
            }
        });
        GridBagConstraints gbcCbShowUsed = new GridBagConstraints();
        gbcCbShowUsed.anchor = GridBagConstraints.EAST;
        gbcCbShowUsed.insets = new Insets(0, 0, 0, 5);
        gbcCbShowUsed.gridx = 3;
        gbcCbShowUsed.gridy = 3;
        getContentPane().add(cbShowUsed, gbcCbShowUsed);

        JButton btnInsert = new JButton(bundle.getProperty("btnInsert"));
        btnInsert.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (mathList.getSelectedValue() != null) {
                    taFormula.insert(" " + mathList.getSelectedValue() + " ",
                            taFormula.getCaretPosition());
                }
                if (featureList.getSelectedValue() != null && efpsList.getSelectedValue() != null) {
                    taFormula.insert(" " + featureVars.get(featureList.getSelectedValue()) + "_"
                            + ((EFP) efpsList.getSelectedValue()).getName() + " ",
                            taFormula.getCaretPosition());
                }
                taFormula.requestFocus();
            }
        });
        GridBagConstraints gbcBtnInsert = new GridBagConstraints();
        gbcBtnInsert.gridwidth = 3;
        gbcBtnInsert.gridx = 1;
        gbcBtnInsert.gridy = 3;
        getContentPane().add(btnInsert, gbcBtnInsert);
    }

    /**
     * Return sorted array of features by name.
     * Features depends on checkbox (all or only used)
     *
     * @return sorted array of features.
     */
    private Object[] getArrayOfFeature() {
        List<Feature> features = accessor.getAllFeatures();
        if (cbShowUsed != null && cbShowUsed.isSelected()) {
            List<Feature> noUsed = new ArrayList<Feature>();
            for (Feature feature : features) {
                if (accessor.getEfps(feature).isEmpty()) {
                    noUsed.add(feature);
                }
            }
            features.removeAll(noUsed);
            Collections.sort(features, new FeatureComparator());
            return features.toArray();
        } else {
            Collections.sort(features, new FeatureComparator());
            return features.toArray();
        }
    }

    /**
     * Load existing formula and replace identifier features to vars.
     */
    private void load() {
        featureList.setListData(getArrayOfFeature());
        taFormula.setText(oldFormula);
    }

    /**
     * Initialize featureVars.
     */
    private void initFeatureVars() {
        featureVars = new HashMap<Feature, String>();
        List<Feature> features = accessor.getAllFeatures();
        int num = 1;
        for (Feature feature : features) {
            if (!accessor.getEfps(feature).isEmpty()) {
                featureVars.put(feature, "F." + num++);
            }
        }
    }

    /**
     * Show dialog for test of formula.
     */
    private void test() {
        List<EfpFeature> efpFeatures = new ArrayList<EfpFeature>();
        for (Feature feature : accessor.getAllFeatures()) {
            for (EFP efp : accessor.getEfps(feature)) {
                efpFeatures.add(new EfpFeature(efp, feature));
            }
        }
        String formula = taFormula.getText();
        for (Entry<Feature, String> var : featureVars.entrySet()) {
            formula = formula.replace(var.getValue(), var.getKey().getIdentifier());
        }
        EfpFeature[] efpFeaturesArray = new EfpFeature[efpFeatures.size()];
        efpFeaturesArray = efpFeatures.toArray(efpFeaturesArray);
        new EfpMathFormulaEvaluate(formula, efpFeaturesArray);
    }

    /**
     * Return a math formula with getIdentifier features.
     *
     * @return A math formula.
     */
    public String getMathFormula() {
        String text = taFormula.getText();
        for (Entry<Feature, String> var : featureVars.entrySet()) {
            text = text.replace(var.getValue(), var.getKey().getIdentifier());
        }
        return text;
    }

    /**
     * Set a math formula.
     *
     * @param formula
     *            A math formula.
     *
     * @return Formula with replaced feature's identifiers.
     */
    public String setMathFormula(final String formula) {
        String text = formula;
        for (Entry<Feature, String> var : featureVars.entrySet()) {
            text = text.replace(var.getKey().getIdentifier(), var.getValue());
        }
        taFormula.setText(text);
        return text;
    }

    /**
     * Exit the dialog.
     */
    private void exitDialog() {
        this.setVisible(false);
    }

    /**
     * Comparator of Features - by funtion getName(). <br>
     * <br>
     * <b>Date:</b> 2.3.2011
     *
     * @author Martin Štulc <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
     */
    private class FeatureComparator implements Comparator<Feature> {

        @Override
        public int compare(final Feature f1, final Feature f2) {
            return f1.getName().compareTo(f2.getName());
        }
    }

}
