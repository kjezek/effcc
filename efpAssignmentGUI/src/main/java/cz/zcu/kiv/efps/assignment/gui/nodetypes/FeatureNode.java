package cz.zcu.kiv.efps.assignment.gui.nodetypes;

import java.util.List;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Node for tree which represents the feature.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class FeatureNode extends DefaultMutableTreeNode {
    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Reference to feature. */
    private Feature feature;


    /**
     * Default constructor.
     * @param feature A feature
     */
    public FeatureNode(final Feature feature) {
        super(feature);
        if (feature == null) {
            throw new RuntimeException("Parameter 'feature' is null.");
        }
        this.feature = feature;
    }

    /**
     * Adds efp nodes which are assigned to this feature.
     * @param component Reference to component for efps
     */
    public void addEFPNodes(final ComponentEFPModifier component) {
        List<EFP> efps = component.getEfps(feature);
        Set<LR> lrs = component.getLRs();


        for (EFP efp : efps) {
            DefaultMutableTreeNode newEfpNode = new DefaultMutableTreeNode(efp);

            EfpAssignedValue efpAssignedVal = component.getAssignedValue(feature, efp, null);
            if (efpAssignedVal != null) {
                newEfpNode.add(new DefaultMutableTreeNode(efpAssignedVal));
            }

            for (LR lr : lrs) {
                efpAssignedVal = component.getAssignedValue(feature, efp, lr);

                if (efpAssignedVal != null) {
                    newEfpNode.add(new DefaultMutableTreeNode(efpAssignedVal));
                }
            }

            this.add(newEfpNode);
        }
    }

    /**
     * @return A feature
     */
    public Feature getFeature() {
        return feature;
    }
}
