package cz.zcu.kiv.efps.assignment.gui;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.api.EfpAwareComponentLoader;
import cz.zcu.kiv.efps.assignment.client.EfpAssignmentClient;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForLists;
import cz.zcu.kiv.efps.assignment.gui.componentstree.ComponentsTree;
import cz.zcu.kiv.efps.assignment.gui.componentstree.ComponentsTreeModel;
import cz.zcu.kiv.efps.assignment.gui.componentstree.ComponentsTreeSelectionListener;
import cz.zcu.kiv.efps.assignment.gui.efpslist.EfpsList;
import cz.zcu.kiv.efps.assignment.gui.efpslist.EfpsListModel;
import cz.zcu.kiv.efps.assignment.gui.efpslist.EfpsListSelectionListener;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.itemtypes.ComponentImplItem;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.registry.client.EfpClient;
import cz.zcu.kiv.efps.registry.client.EfpClientImpl;
import cz.zcu.kiv.efps.types.gr.GR;

/**
 * Class which represents gui.
 * @author Jan Svab
 *
 */
public final class AssignmentGUI extends JFrame {
    /**Serial version UID.*/
    private static final long serialVersionUID = 1L;
    /**Reference to the instance of AssignmentGUI. */
    private static AssignmentGUI instance = null;

    /** X-coordinate of the frame. */
    private static final int WINDOW_X = 100;
    /** Y-coordinate of the frame. */
    private static final int WINDOW_Y = 100;
    /** Width of the frame. */
    private static final int WINDOW_WIDTH = 850;
    /** Height of the frame. */
    private static final int WINDOW_HEIGHT = 600;
    /** Basic insets of a element. */
    private static final int INSETS = 5;
    /** Start position of the splitter in a split pane.*/
    private static final double SPLITPANE_START_RESIZE_WEIGHT = 0.5;

    /** Properties file with localization. */
    private Localization localization;

    /** Path to selected component model implementation. */
    private String pathToCompImpl;
    /** Name of selected component model implementation. */
    private String nameOfCompImpl;

    /** Panel with all content. */
    private JPanel contentPane;

    /** Tree with components. */
    private ComponentsTree trComponents;
    /** List of EFPs in selected GR. */
    private EfpsList efpsList;

    /** Text pane for showing detail information about selected object. */
    private JTextPane txpDetailInfo;





    /**
     * Returns the instance of this class.
     * @return GUI AssignmentGUI
     */
    public static AssignmentGUI getAssignmentGUI() {
        if (instance == null) {
            instance = new AssignmentGUI();
            instance.setVisible(true);
        }
        return instance;
    }





    /**
     * Creates the frame.
     */
    private AssignmentGUI() {
        localization = new Localization("efpAssignmentGUI");
        initializationGUI();
    }


    /**
     * Initialization of the frame with GUI.
     */
    private void initializationGUI() {
        setTitle(localization.getProperty("titleGUI"));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                if (trComponents.getComponentsTreeModel().isEmpty()) {
                    return;
                }

                int n = JOptionPane.showConfirmDialog(null,
                        localization.getProperty("confirmSaveDlgText"),
                        localization.getProperty("confirmSaveDlgTitle"), JOptionPane.YES_NO_OPTION);

                if (n == 0) {
                    trComponents.getComponentsTreeModel().saveAllComponents();
                }

                trComponents.getComponentsTreeModel().closeAllComponents();
            }
        });


        setJMenuBar(createMenu());

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(INSETS, INSETS, INSETS, INSETS));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JSplitPane sppMainWindow = new JSplitPane();
        sppMainWindow.setResizeWeight(SPLITPANE_START_RESIZE_WEIGHT);
        contentPane.add(sppMainWindow, BorderLayout.CENTER);

        JScrollPane scpLeftPanel = new JScrollPane();
        scpLeftPanel.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sppMainWindow.setLeftComponent(scpLeftPanel);

        JPanel pnlLeftInScp = new JPanel();
        scpLeftPanel.setViewportView(pnlLeftInScp);
        pnlLeftInScp.setLayout(new BorderLayout(0, 0));

        JScrollPane scpRightPanel = new JScrollPane();
        scpRightPanel.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sppMainWindow.setRightComponent(scpRightPanel);

        JPanel pnlRightInScp = new JPanel();
        scpRightPanel.setViewportView(pnlRightInScp);
        pnlRightInScp.setLayout(new BorderLayout(0, 0));


        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);


        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
                localization.getProperty("lblDetailInfo"), TitledBorder.LEADING, TitledBorder.TOP,
                null, new Color(0, 0, 0)));
        panel.setLayout(new BorderLayout(0, 0));

        txpDetailInfo = new JTextPane();
        txpDetailInfo.setEditable(false);
        panel.add(txpDetailInfo, BorderLayout.CENTER);


        trComponents = new ComponentsTree(new ComponentsTreeModel());
        trComponents.addTreeSelectionListener(new ComponentsTreeSelectionListener(txpDetailInfo));
        trComponents.expandAll();
        pnlLeftInScp.add(trComponents, BorderLayout.CENTER);


        efpsList = new EfpsList(new EfpsListModel());
        efpsList.addListSelectionListener(new EfpsListSelectionListener(txpDetailInfo));
        pnlRightInScp.add(efpsList, BorderLayout.CENTER);

        JComboBox cmbGlobalRegisters = getCmbWithGRs();
        cmbGlobalRegisters.addActionListener(new CmbGRsActionListener());
        cmbGlobalRegisters.setRenderer(new CellRendererForLists());
        pnlRightInScp.add(cmbGlobalRegisters, BorderLayout.NORTH);

        JLabel labelGR = new JLabel(localization.getProperty("lblGrsChooser"));
        labelGR.setHorizontalAlignment(SwingConstants.LEFT);
        scpRightPanel.setColumnHeaderView(labelGR);
    }


    /**
     * Reads GRs from repository and creates combo box for choosing GR.
     * @return Created combo box
     */
    private JComboBox getCmbWithGRs() {
        JComboBox cmbGlobalRegisters = new JComboBox();

        List<GR> grs = getEfpClient().findGrAll();
        String lastGR = AppParametersProperties.getParameterValue("last_selected_gr");
        int lastGRId = 0;

        if (lastGR != null && !lastGR.equals("")) {
            lastGRId = Integer.parseInt(lastGR);
        }


        for (GR gr : grs) {
            cmbGlobalRegisters.addItem(gr);

            if (lastGRId == gr.getId()) {
                cmbGlobalRegisters.setSelectedIndex(cmbGlobalRegisters.getItemCount() - 1);
                efpsList.getEfpsListModel().fillingWithData(lastGRId);
            }
        }

        if (lastGRId == 0 && cmbGlobalRegisters.getItemCount() != 0) {
            cmbGlobalRegisters.setSelectedIndex(0);
            efpsList.getEfpsListModel().fillingWithData(
                    ((GR) cmbGlobalRegisters.getSelectedItem()).getId());
        }

        return cmbGlobalRegisters;
    }


    /**
     * Creates menu.
     * @return Reference to menu bar
     */
    private JMenuBar createMenu() {
        JMenuBar mnbMenu = new JMenuBar();

        JMenu mnOpenComponent = new JMenu(localization.getProperty("mnComponent"));
        mnbMenu.add(mnOpenComponent);

        JMenuItem mniCosiComponent = new JMenuItem(localization.getProperty("mniOpenComponent"));
        mniCosiComponent.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/open.png")));
        mniCosiComponent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                JFileChooser fc = new JFileChooser();

                String lastFile =
                    AppParametersProperties.getParameterValue("last_opened_comp_path");

                if (lastFile != null && !lastFile.equals("")) {
                    File file = new File(lastFile);

                    if (file.exists()) {
                        fc.setSelectedFile(file);
                    }
                }

                int returnVal = fc.showOpenDialog(AssignmentGUI.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();

                    if (!file.isFile()) {
                        throw new GuiRTException(localization.getProperty("ERR_FILE"));

                    } else if (pathToCompImpl == null) {
                        throw new GuiRTException(localization.getProperty("ERR_COMPONENT_LOADER"));

                    } else {
                        openComponent(pathToCompImpl, file);
                    }
                }
            }
        });
        mnOpenComponent.add(mniCosiComponent);

        JMenuItem mniChangeLoader = new JMenuItem(localization.getProperty("mniChangeLoader"));
        mniChangeLoader.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
        "images/administration.png")));
        mniChangeLoader.addActionListener(new ChangingComponentImplActionListener());
        mnOpenComponent.add(mniChangeLoader);

        return mnbMenu;
    }


    /**
     * Opens a component.
     * @param componentLoader Class for access the component data
     * @param file Reference to file with the component
     */
    public void openComponent(final String componentLoader, final File file) {
        AppParametersProperties.saveParameter("last_opened_comp_path", file.getAbsolutePath());

        EfpAwareComponentLoader loader =
            EfpAssignmentClient.initialiseComponentLoader(componentLoader);
        ComponentEFPModifier componentModifier = loader.loadForUpdate(file.getAbsolutePath());

        ComponentNode compNode = new ComponentNode(componentModifier, file.getName());
        compNode.setFeatureNodes();

        trComponents.getComponentsTreeModel().addComponentNode(compNode);
        trComponents.getComponentsTreeModel().reload();
        trComponents.expandAll();
    }


    /**
     * @return Returns path to actual component model implementation
     */
    public String getPathToCompImpl() {
        return pathToCompImpl;
    }


    /**
     * @param pathToCompImpl Path to new component model implementation
     */
    public void setPathToCompImpl(final String pathToCompImpl) {
        this.pathToCompImpl = pathToCompImpl;
    }


    /**
     * @return Return name of actual component model implementation
     */
    public String getNameOfCompImpl() {
        return nameOfCompImpl;
    }


    /**
     * @param nameOfCompImpl Name of new component model implementation
     */
    public void setNameOfCompImpl(final String nameOfCompImpl) {
        this.nameOfCompImpl = nameOfCompImpl;
        this.setTitle(localization.getProperty("titleGUI") + " - " + nameOfCompImpl);
    }


    /**
     * @return Reference to efp client
     */
    public static EfpClient getEfpClient() {
        return EfpClientImpl.getClient();
    }



    /**
     * Action listener for button for changing component model implementation.
     *
     * Date: 7.2.2011
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class ChangingComponentImplActionListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            ComponentTypeDialog componentTypeDialog = new ComponentTypeDialog();

            List<ComponentImplItem> listCompImplItems =
                ComponentImplsProperties.getComponentLoaders();

            if (listCompImplItems.size() == 0) {
                throw new GuiRTException(localization.getProperty("ERR_PROPERTY_LOADERS"));
            }

            componentTypeDialog.fillListComponentType(listCompImplItems);
            componentTypeDialog.setVisible(true);

            if (componentTypeDialog.getIsDialogConfirm()
                    && !nameOfCompImpl.equals(componentTypeDialog.getNameOfCompImpl())) {

                nameOfCompImpl = componentTypeDialog.getNameOfCompImpl();
                pathToCompImpl = componentTypeDialog.getPathToCompImpl();

                AssignmentGUI.this.setTitle(
                        localization.getProperty("titleGUI") + " - " + nameOfCompImpl);

                trComponents.getComponentsTreeModel().clear();
            }
        }

    }



    /**
     * Action listener for combobox with GRs.
     *
     * Date: 3.2.2011
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class CmbGRsActionListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            JComboBox cmb = (JComboBox) e.getSource();

            if (cmb.getSelectedItem() == null) {
                 efpsList.getEfpsListModel().deleteItems();
            }

            if (cmb.getSelectedItem() instanceof GR) {
                 efpsList.getEfpsListModel().fillingWithData(((GR) cmb.getSelectedItem()).getId());

                AppParametersProperties.saveParameter(
                        "last_selected_gr", "" + ((GR) cmb.getSelectedItem()).getId());
            }
       }
    }
}
