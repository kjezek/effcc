package cz.zcu.kiv.efps.assignment.gui;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Contains transfered data during drag&drop operation.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class TransferableObject extends DefaultMutableTreeNode implements Transferable {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Array with supported data flavor. */
    private static DataFlavor[] flavors = {DataFlavor.stringFlavor};

    /** Transfered data. */
    private String data;


    /**
     * Constructor.
     * @param data Transfered data
     */
    public TransferableObject(final String data) {
        this.data = data;
    }

    /**
     * Returns supported types of objects for transfer.
     * @return Array with data flavors.
     */
    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }


    /**
     * Returns transfered data.
     * @param flavor Data flavor
     * @return Transfered data
     * @exception UnsupportedFlavorException It's required unsupported data flavor.
     * @exception IOException Error in writing and reading transfered data.
     */
    public Object getTransferData(final DataFlavor flavor)
            throws UnsupportedFlavorException, IOException {

        Object returnObject = null;

        for (int index = 0; index < flavors.length; index++) {
            if (flavor.equals(flavors[index])) {
                returnObject = data;
            }
        }

        if (returnObject == null) {
            throw new UnsupportedFlavorException(flavor);
        }

        return returnObject;
    }


    /**
     * Test if this transferable tree node supports concrete flavor.
     * @param flavor Tested data flavor
     * @return True if supports else false
     */
    public boolean isDataFlavorSupported(final DataFlavor flavor) {
        boolean returnValue = false;

        for (int i = 0, n = flavors.length; i < n; i++) {
            if (flavor.equals(flavors[i])) {
                returnValue = true;
                break;
            }
        }

        return returnValue;
    }
}
