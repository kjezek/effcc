package cz.zcu.kiv.efps.assignment.gui.componentstree;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.assigningefpdialog.AssignmentEfpDialog;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForTrees;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.FeatureNode;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * Tree which contains opened components with their features and assigned efps.
 *
 * Date: 30.12.2010
 * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentsTree extends JTree {

    /** Serial Version. */
    private static final long serialVersionUID = 1L;
    /** Properties file with localization. */
    private Localization localization;

    /**
     * Default constructor.
     * @param dataModel Data model for component tree with features
     */
    public ComponentsTree(final ComponentsTreeModel dataModel) {
        super(dataModel);
        localization = new Localization("componentTree");
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setEditable(false);
        setShowsRootHandles(false);
        setRootVisible(false);

        setCellRenderer(new CellRendererForTrees());
        addMouseListener(new ComponentTreeMouseAdapter());

        new DropTarget(this, new TreeDropTargetListener());
    }


    /**
     * DropTargetListener for dropping nodes with efp.
     *
     * Date: 30.12.2010
     * @author Jan Svab<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class TreeDropTargetListener implements DropTargetListener {

        @Override
        public void dragEnter(final DropTargetDragEvent dropTargetDragEvent) { }
        @Override
        public void dragExit(final DropTargetEvent dropTargetEvent) { }
        @Override
        public void dragOver(final DropTargetDragEvent dropTargetDragEvent) { }
        @Override
        public void dropActionChanged(final DropTargetDragEvent dropTargetDragEvent) { }

        @Override
        public synchronized void drop(final DropTargetDropEvent dropTargetDropEvent) {
            Point location = dropTargetDropEvent.getLocation();
            TreePath path = getPathForLocation(location.x, location.y);

            if (path == null) {
                return;
            }

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();

            if (node == null) {
                return;
            }

            FeatureNode targetFeatureNode = null;

            if (node.getUserObject() instanceof EFP) {
                targetFeatureNode = (FeatureNode) node.getParent();
            }

            if (node.getUserObject() instanceof EfpAssignedValue) {
                targetFeatureNode = (FeatureNode) node.getParent().getParent();
            }

            if (node instanceof FeatureNode) {
                targetFeatureNode = (FeatureNode) node;
            } else if (node instanceof ComponentNode) {
                dropTargetDropEvent.rejectDrop();
                throw new GuiRTException(localization.getProperty("ERR_ADD_EFP_ROOT"));
            }


            if (targetFeatureNode != null) {
                try {
                    Transferable tr = dropTargetDropEvent.getTransferable();
                    dropTargetDropEvent.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
                    Object userObject = tr.getTransferData(DataFlavor.stringFlavor);

                    EFP efp = EfpSerialiser.deserialize(EFP.class, (String) userObject);

                    AssignmentEfpDialog assignEfpDg = new AssignmentEfpDialog(
                            targetFeatureNode,
                            ComponentsTree.this,
                            efp,
                            ((ComponentNode) path.getPathComponent(1)).getComponent());
                    assignEfpDg.setVisible(true);

                    dropTargetDropEvent.dropComplete(true);

                } catch (UnsupportedFlavorException ex) {
                    dropTargetDropEvent.rejectDrop();
                    throw new RuntimeException(
                            "Operation Drag&Drop failed. (UnsupportedFlavorException)");
                } catch (IOException ex) {
                    dropTargetDropEvent.rejectDrop();
                    throw new RuntimeException("Operation Drag&Drop failed. (IOException)");
                }
            } else {
                dropTargetDropEvent.rejectDrop();
                throw new GuiRTException(localization.getProperty("ERR_ADD_EFP"));
            }
        }
    }


    /**
     * Expands all nodes.
     */
    public void expandAll() {
        for (int i = 0; i < this.getRowCount(); i++) {
            this.expandRow(i);
        }
    }

    /**
     * @return A data model of tree.
     */
    public ComponentsTreeModel getComponentsTreeModel() {
        return (ComponentsTreeModel) getModel();
    }
}
