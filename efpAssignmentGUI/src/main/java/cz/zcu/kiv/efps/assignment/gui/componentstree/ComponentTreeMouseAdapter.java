package cz.zcu.kiv.efps.assignment.gui.componentstree;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import cz.zcu.kiv.efps.assignment.gui.nodetypes.ComponentNode;
import cz.zcu.kiv.efps.assignment.gui.nodetypes.FeatureNode;


/**
 * Mouse listener for component tree.
 *
 * Date: 8.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ComponentTreeMouseAdapter extends MouseAdapter {

    @Override
    public void mousePressed(final MouseEvent e) {
        if (e.isPopupTrigger()) {
            showPop(e);
        }
    }


    @Override
    public void mouseReleased(final MouseEvent e) {
        if (e.isPopupTrigger()) {
            showPop(e);
        }
    }


    /**
     * Shows Popup menu on the supported object.
     * @param e Reference to mouse event
     */
    private void showPop(final MouseEvent e) {
        ComponentsTree componentsTree = (ComponentsTree) e.getSource();
        TreePath path = componentsTree.getPathForLocation(e.getX(), e.getY());

        if (path == null) {
            return;
        }

        Object lastObject = path.getLastPathComponent();

        if (!(lastObject instanceof DefaultMutableTreeNode)
                || lastObject instanceof FeatureNode) {
            return;
        }

        JPopupMenu popupMenu = null;

        //selected node with component
        if (lastObject instanceof ComponentNode) {
            popupMenu =
                new PopupMenuComponent((ComponentNode) lastObject, componentsTree);
        }

        // selected node is node with EFP or EFP assigned value
        if (lastObject instanceof DefaultMutableTreeNode
                && !(lastObject instanceof ComponentNode)) {
            //finding parent feature node
            DefaultMutableTreeNode featureNode = (DefaultMutableTreeNode) lastObject;

            while (!(featureNode instanceof FeatureNode)) {
                featureNode = (DefaultMutableTreeNode) featureNode.getParent();

                if (featureNode == null) {
                    return;
                }
            }

            //finding parent component node
            DefaultMutableTreeNode componentNode = (DefaultMutableTreeNode) lastObject;

            while (!(componentNode instanceof ComponentNode)) {
                componentNode = (DefaultMutableTreeNode) componentNode.getParent();

                if (componentNode == null) {
                    return;
                }
            }

            popupMenu = new PopupMenuAssignments(
                    ((ComponentNode) componentNode).getComponent(),
                    ((FeatureNode) featureNode).getFeature(),
                    (DefaultMutableTreeNode) path.getLastPathComponent(),
                    componentsTree);
        }

        popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
}
