package cz.zcu.kiv.efps.assignment.gui.efpslist;

import java.util.List;

import javax.swing.DefaultListModel;

import cz.zcu.kiv.efps.assignment.gui.AssignmentGUI;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Data model for list with EFP items.
 *
 * Date: 1.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class EfpsListModel extends DefaultListModel {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** ID of GR which from are EFPs. */
    private int currentGRId;



    /**
     * Default constructor.
     */
    public EfpsListModel() {
        currentGRId = -1;
    }


    /**
     * Clears data model.
     */
    public void deleteItems() {
        this.clear();
        currentGRId = -1;
    }


    /**
     * Fills data model with EFP items from concrete GR.
     * @param grId ID of the GR.
     */
    public void fillingWithData(final int grId) {
        if (grId == currentGRId) {
            return;
        }

        this.clear();
        List<EFP> efps = AssignmentGUI.getEfpClient().findEfpForGr(grId);

        for (EFP efp : efps) {
            this.addElement(efp);
        }

        currentGRId = grId;
    }
}
