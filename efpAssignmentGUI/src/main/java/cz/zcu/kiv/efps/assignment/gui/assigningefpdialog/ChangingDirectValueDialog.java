package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.gui.values.EfpValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.FactoryValueGUI;
import cz.zcu.kiv.efps.assignment.gui.values.IllegalValueException;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Dialog for changing value of EFP assignment with EFP direct value.
 *
 * Date: 5.3.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ChangingDirectValueDialog extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private static Localization localization;

    /** X-coordinate of the frame. */
    private static final int WINDOW_X = 100;
    /** Y-coordinate of the frame. */
    private static final int WINDOW_Y = 100;
    /** Width of the frame. */
    private static final int WINDOW_WIDTH = 300;
    /** Height of the frame. */
    private static final int WINDOW_HEIGHT = 300;

    /** GUI for changing EFP value. */
    private EfpValueGUI efpValueGUI;
    /** A new EfpValue. */
    private EfpValueType newEfpValue;


    /**
     * Create the dialog.
     * @param efp A EFP
     * @param oldEfpValue A old EFP value
     */
    public ChangingDirectValueDialog(final EFP efp, final EfpValueType oldEfpValue) {
        super(null, ModalityType.APPLICATION_MODAL);
        localization = new Localization("editDialogs");

        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        newEfpValue = null;
        efpValueGUI = createPanelDirectValue(efp, oldEfpValue);

        initialization();
    }


    /**
     * Initialization of frame.
     */
    private void initialization() {
        setTitle(localization.getProperty("dialogDirectValue"));
        setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
        getContentPane().setLayout(new BorderLayout());

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton btnChange = new JButton(localization.getProperty("btnOK"));
        btnChange.setActionCommand(localization.getProperty("btnOK"));
        btnChange.addActionListener(new ConfirmActionListener());
        buttonPane.add(btnChange);
        getRootPane().setDefaultButton(btnChange);

        JButton btnCancel = new JButton(localization.getProperty("btnStorno"));
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                setVisible(false);
            }
        });
        btnCancel.setActionCommand(localization.getProperty("btnStorno"));
        buttonPane.add(btnCancel);

        getContentPane().add(efpValueGUI, BorderLayout.CENTER);
    }


    /** Action listener for confirm the dialog. */
    private class ConfirmActionListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            try {
                newEfpValue = efpValueGUI.getValue();
            } catch (IllegalValueException e1) {
                throw new GuiRTException(e1);
            }
            setVisible(false);
        }
    }


    /**
     * @return A new EFP value.
     */
    public EfpValueType getNewEfpValue() {
        return newEfpValue;
    }


    /**
     * Creates panel for writing direct value to EFP.
     * @param efp A EFP
     * @param oldEfpValue A old EFP value
     * @return A GUI panel for changing EFP value
     */
    private static EfpValueGUI createPanelDirectValue(
            final EFP efp, final EfpValueType oldEfpValue) {

        JPanel pnlDirectValAssign = new JPanel();
        pnlDirectValAssign.setLayout(new BorderLayout(0, 0));

        EfpValueGUI efpValueGUI = FactoryValueGUI.getValueGUI(efp.getValueType());
        efpValueGUI.setValue(oldEfpValue);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 0));
        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
                localization.getProperty("lblDirectVal"), TitledBorder.LEADING, TitledBorder.TOP,
                null, new Color(0, 0, 0)));
        panel.add(efpValueGUI, BorderLayout.NORTH);


        pnlDirectValAssign.add(panel, BorderLayout.CENTER);

        return efpValueGUI;
    }
}
