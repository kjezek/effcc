package cz.zcu.kiv.efps.assignment.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible coplex value. <br>
 * Date: 10. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpComplexGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** Map of position and new gui value. */
    private Map<Integer, EfpValueGUI> gui;

    /** Map of position and new textfield of name of value. */
    private Map<Integer, JTextField> tfs;

    /** Map of position and new label of name. */
    private Map<Integer, JLabel> lbls;

    /** Map of position and button for delete. */
    private Map<Integer, JButton> btns;

    /** ComboBox for adding new value. */
    private JComboBox cmbNewValue;

    /** Button for adding new value. */
    private JButton btnAdd;

    /** Position gui. (posincrement) */
    private int y = 1;

    /**
     * Create the panel.
     */
    EfpComplexGUI() {
        super();
        setBorder(new TitledBorder(bundle.getProperty("titleBorderComplex")));
        gui = new HashMap<Integer, EfpValueGUI>();
        btns = new HashMap<Integer, JButton>();
        tfs = new HashMap<Integer, JTextField>();
        lbls = new HashMap<Integer, JLabel>();

        init();

    }

    /**
     * Initialization gui.
     */
    private void init() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        cmbNewValue = new JComboBox();
        cmbNewValue.setModel(new DefaultComboBoxModel(EfpValueGUI.getNamesOfValues()));
        cmbNewValue.setToolTipText(bundle.getProperty("toolTipComplex"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 3;
        gbc.insets = new Insets(5, 5, 0, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.0;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(cmbNewValue, gbc);

        btnAdd = new JButton(bundle.getProperty("btnAddComplex"));
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                addValue();
            }
        });
        gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.EAST;
        gbc.insets = new Insets(5, 5, 0, 5);
        gbc.weightx = 0.0;
        gbc.gridx = 3;
        gbc.gridy = 0;
        add(btnAdd, gbc);
    }

    /**
     * Add gui for new value according parametr.
     * @param value value which will be added
     * @param name name of value
     */
    private void addValue(final String name, final EfpValueType value) {
        EfpValueGUI newValue = FactoryValueGUI.getValueGUI(value.getClass());
        newValue.setValue(value);

        addGui(newValue);
        tfs.get(y - 1).setText(name);
        this.validate();
        this.repaint();
    }

    /**
     * Add gui for new value according combobox.
     */
    private void addValue() {
        EfpValueGUI newValue = FactoryValueGUI.getValueGUI(EfpValueGUI
                .getTypeByName(cmbNewValue.getSelectedItem().toString()));
        addGui(newValue);
        this.getRootPane().validate();
        this.getRootPane().repaint();
    }

    /**
     * Create GUI fo new value.
     * @param newValue new gui value
     */
    private void addGui(final EfpValueGUI newValue) {
        JTextField tf = new JTextField();
        tfs.put(y, tf);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.gridx = 1;
        gbc.gridy = y;
        add(tf, gbc);

        JLabel lab = new JLabel(bundle.getProperty("lblNameComplex"));
        lbls.put(y, lab);
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 0.0;
        gbc.gridx = 0;
        gbc.gridy = y;
        add(lab, gbc);

        gui.put(y, newValue);
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 2.0;
        gbc.gridx = 2;
        gbc.gridy = y;
        add(newValue, gbc);

        JButton deleteBtn = new JButton(bundle.getProperty("btnDeleteComplex"));
        deleteBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                for (Entry<Integer, JButton> i : btns.entrySet()) {
                    if (i.getValue() == e.getSource()) {
                        deleteValue(i.getKey());
                        break;
                    }
                }

            }
        });
        btns.put(y, deleteBtn);
        gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.EAST;
        gbc.weightx = 0.0;
        gbc.gridx = 3;
        gbc.gridy = y;
        add(deleteBtn, gbc);
        y++;

    }

    /**
     * Delete value on position y.
     * @param y position deleted value
     */
    private void deleteValue(final int y) {
        this.remove(gui.get(y));
        this.remove(btns.get(y));
        this.remove(tfs.get(y));
        this.remove(lbls.get(y));
        gui.remove(y);
        btns.remove(y);
        tfs.remove(y);
        lbls.remove(y);
        this.getRootPane().validate();
        this.getRootPane().repaint();
    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        Map<String, EfpValueType> items = new HashMap<String, EfpValueType>();
        for (int i = 0; i < y; i++) {
            if (gui.containsKey(i)) {
                if (tfs.get(i).getText().equals("")) {
                    throw new IllegalValueException(
                            bundle.getProperty("ERR_FILL_NAME_COMPLEX"));

                }
                if (items.containsKey(tfs.get(i).getText())) {
                    throw new IllegalValueException(
                            bundle.getProperty("ERR_DUPLICITY_NAME_COMPLEX"));
                }
                items.put(tfs.get(i).getText(), gui.get(i).getValue());
            }
        }
        return new EfpComplex(items);
    }

    @Override
    public void setValue(final EfpValueType value) {
        for (Entry<String, EfpValueType> val : ((EfpComplex) value).getItems().entrySet()) {
            addValue(val.getKey(), val.getValue());
        }
        this.validate();
        this.repaint();
    }

    /* (non-Javadoc)
     *
     * @see values.EfpValueGUI#lock() */
    @Override
    public void lock() {
        for (int i = 0; i < y; i++) {
            if (gui.containsKey(i)) {
                gui.get(i).lock();
                tfs.get(i).setEditable(false);

                this.remove(btns.get(i));
                btns.remove(i);
            }
        }
        this.remove(cmbNewValue);
        this.remove(btnAdd);
    }
}
