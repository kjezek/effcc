package cz.zcu.kiv.efps.assignment.gui;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;


/**
 * Localization class.
 *
 * Date: 24.11.2010
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class Localization {

    /** Path to properties. */
    private static final String PATH = "localization.";
    /** Resource of labels. */
    private ResourceBundle bundle;


    /**
     * Construct localization.
     * @param propertiesFile name of propeties file
     */
    public Localization(final String propertiesFile) {
        try {
            bundle = ResourceBundle.getBundle(PATH + propertiesFile);
        } catch (Exception e) {
            bundle = ResourceBundle.getBundle(PATH + propertiesFile, Locale.ENGLISH);
        }
    }

    /**
     * Return label from properties file.
     * @param key Key of label.
     * @return Value of label.
     */
    public String getProperty(final String key) {
        String value = (String) bundle.getString(key);
        try {
            return new String(value.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("No support encoding in properties file!");
        }
    }
}

