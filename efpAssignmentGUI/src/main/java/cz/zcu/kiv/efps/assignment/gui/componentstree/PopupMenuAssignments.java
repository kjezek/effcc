package cz.zcu.kiv.efps.assignment.gui.componentstree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import cz.zcu.kiv.efps.assignment.api.ComponentEFPModifier;
import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.assigningefpdialog.AssignmentEfpDialog;
import cz.zcu.kiv.efps.assignment.gui.assigningefpdialog.ChangingDirectValueDialog;
import cz.zcu.kiv.efps.assignment.gui.assigningefpdialog.ChangingNamedValueDialog;
import cz.zcu.kiv.efps.assignment.gui.assigningefpdialog.EfpMathFormulaDialog;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.assignment.types.Feature;
import cz.zcu.kiv.efps.assignment.values.EfpAssignedValue;
import cz.zcu.kiv.efps.assignment.values.EfpDirectValue;
import cz.zcu.kiv.efps.assignment.values.EfpFormulaValue;
import cz.zcu.kiv.efps.assignment.values.EfpNamedValue;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * Popup menu for edit of EFP assignments in component tree.
 *
 * Date: 8.2.2011
 *
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class PopupMenuAssignments extends JPopupMenu {
    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** Component modifier which owns editing node. */
    private ComponentEFPModifier component;
    /** Editing node. */
    private DefaultMutableTreeNode editingNode;
    /** Feature which owns editing node. */
    private Feature feature;
    /** Editing component tree. */
    private ComponentsTree componentsTree;

    /**
     * Constructor for basic setting.
     *
     * @param component
     *            Reference to component modifier which owns the editing node.
     * @param editingNode
     *            Reference to editing node.
     * @param feature
     *            Reference to feature which owns the editing node.
     * @param compTree
     *            Editing component tree
     */
    public PopupMenuAssignments(final ComponentEFPModifier component, final Feature feature,
            final DefaultMutableTreeNode editingNode, final ComponentsTree compTree) {

        localization = new Localization("componentTree");

        compTree.setSelectionPath(new TreePath(editingNode.getPath()));
        JMenuItem mniDel = new JMenuItem(localization.getProperty("lblDelete"));
        if (editingNode.getUserObject() instanceof EFP) {
            mniDel.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                    "images/folder_delete.png")));
        }

        if (editingNode.getUserObject() instanceof EfpAssignedValue) {
            mniDel.setIcon(new ImageIcon(getClass().getClassLoader().getResource(
                    "images/delete.png")));

            JMenuItem mniEdit = new JMenuItem(localization.getProperty("lblEdit"));
            mniEdit.addActionListener(new EditAction());
            mniEdit.setIcon(new ImageIcon(getClass().getClassLoader()
                    .getResource("images/edit.png")));
            add(mniEdit);
        }

        mniDel.addActionListener(new DeleteAction());
        add(mniDel);

        this.component = component;
        this.editingNode = editingNode;
        this.feature = feature;
        this.componentsTree = compTree;
    }

    /**
     * Action listener for edit EFP assigned value.
     *
     * Date: 8.2.2011
     *
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class EditAction implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            if (!(editingNode.getUserObject() instanceof EfpAssignedValue)) {
                throw new GuiRTException(localization.getProperty("ERR_WRONG_OBJECT"));
            }

            ComponentsTreeModel dataModel = (ComponentsTreeModel) componentsTree.getModel();
            EfpAssignedValue efpAssignedValue = (EfpAssignedValue) editingNode.getUserObject();
            EFP efp = (EFP) ((DefaultMutableTreeNode) editingNode.getParent()).getUserObject();

            JDialog editDialog = null;

            if (efpAssignedValue instanceof EfpNamedValue) {
                LrAssignment oldLrAssign = ((EfpNamedValue) efpAssignedValue).getLrAssignment();
                editDialog = new ChangingNamedValueDialog(oldLrAssign);
                editDialog.setVisible(true);

                LrAssignment lrAssignment = ((ChangingNamedValueDialog) editDialog)
                        .getSelectedLrAssignment();

                if (lrAssignment == null || lrAssignment.equals(oldLrAssign)) {
                    return;
                }

                EfpNamedValue newAssignedValue = createEfpNamedValue(feature, lrAssignment);
                component.changeEfpValue(feature, efp, efpAssignedValue, newAssignedValue);

                editingNode.setUserObject(newAssignedValue);

            } else if (efpAssignedValue instanceof EfpDirectValue) {
                EfpValueType oldEfpValue = ((EfpDirectValue) efpAssignedValue).getValue();

                editDialog = new ChangingDirectValueDialog(efp, oldEfpValue);
                editDialog.setVisible(true);

                EfpValueType newEfpValue = ((ChangingDirectValueDialog) editDialog)
                        .getNewEfpValue();

                if (newEfpValue == null) {
                    return;
                }

                EfpDirectValue newAssignedValue = new EfpDirectValue(newEfpValue);
                component.changeEfpValue(feature, efp, efpAssignedValue, newAssignedValue);

                editingNode.setUserObject(newAssignedValue);

            } else if (efpAssignedValue instanceof EfpFormulaValue) {
                String oldFormula = ((EfpFormulaValue) efpAssignedValue).getFormula();

                editDialog = new EfpMathFormulaDialog(oldFormula, component);
                String newFormula = ((EfpMathFormulaDialog) editDialog).getMathFormula();

                if (newFormula.equals(oldFormula)) {
                    return;
                }

                EfpFormulaValue newAssignedValue = new EfpFormulaValue(newFormula);
                component.changeEfpValue(feature, efp, efpAssignedValue, newAssignedValue);

                editingNode.setUserObject(newAssignedValue);
            }

            dataModel.nodeStructureChanged(editingNode);
            editDialog.dispose();
        }
    }

    /**
     * Action listener for delete concrete node.
     *
     * Date: 8.2.2011
     *
     * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
     */
    private class DeleteAction implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {

            ComponentsTreeModel dataModel = (ComponentsTreeModel) componentsTree.getModel();

            if (editingNode.getUserObject() instanceof EFP) {
                EFP efp = (EFP) editingNode.getUserObject();

                int n = JOptionPane.showConfirmDialog(null,
                        localization.getProperty("confirmDelEfp"),
                        localization.getProperty("confirmDelDlgTitle"), JOptionPane.YES_NO_OPTION);

                if (n == 0) {
                    if (editingNode.getChildCount() == 0) {
                        component.removeEFP(feature, efp, null);
                    } else {
                        for (int index = 0; index < editingNode.getChildCount(); index++) {
                            DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) editingNode
                                    .getChildAt(index);

                            if (childNode.getUserObject() instanceof EfpAssignedValue) {
                                component.removeEFP(feature, efp,
                                        (EfpAssignedValue) childNode.getUserObject());
                            }
                        }
                    }

                    TreeNode changedNode = editingNode.getParent();
                    dataModel.removeNodeFromParent(editingNode);
                    dataModel.nodeStructureChanged(changedNode);
                }
            }

            if (editingNode.getUserObject() instanceof EfpAssignedValue) {
                int n = JOptionPane.showConfirmDialog(null,
                        localization.getProperty("confirmDelAssignedValue"),
                        localization.getProperty("confirmDelDlgTitle"), JOptionPane.YES_NO_OPTION);

                if (n == 0) {
                    EFP efp = (EFP) ((DefaultMutableTreeNode) editingNode.getParent())
                            .getUserObject();

                    component.removeEFP(feature, efp,
                            (EfpAssignedValue) editingNode.getUserObject());

                    TreeNode changedNode = (DefaultMutableTreeNode) editingNode.getParent();
                    dataModel.removeNodeFromParent(editingNode);

                    // remove node with EFP if it doesn't have any child with assigned efp value
                    if (((DefaultMutableTreeNode) changedNode).getChildCount() == 0) {
                        editingNode = (DefaultMutableTreeNode) changedNode;
                        changedNode = changedNode.getParent();
                        dataModel.removeNodeFromParent(editingNode);
                    }

                    dataModel.nodeStructureChanged(changedNode);
                }
            }
        }
    }

    /**
     * Creates instance of EfpNamedValue.
     *
     * @param editingFeature
     *            A editing feature
     * @param lrAssignment
     *            A LR assignment for new EFP named value
     * @return Created EFP named value
     */
    public static EfpNamedValue createEfpNamedValue(final Feature editingFeature,
            final LrAssignment lrAssignment) {

        EfpNamedValue efpNamedValue = null;

        if (lrAssignment instanceof LrSimpleAssignment) {
            efpNamedValue = new EfpNamedValue((LrSimpleAssignment) lrAssignment);

        } else if (lrAssignment instanceof LrDerivedAssignment) {
            efpNamedValue = (EfpNamedValue) AssignmentEfpDialog.setAssignmentWithLrDerivedAssign(
                    editingFeature, (LrDerivedAssignment) lrAssignment);


        } else {
            throw new RuntimeException("Unknown type of LR assignment.");
        }

        return efpNamedValue;
    }
}
