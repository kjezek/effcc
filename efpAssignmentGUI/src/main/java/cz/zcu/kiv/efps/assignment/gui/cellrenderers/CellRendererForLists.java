package cz.zcu.kiv.efps.assignment.gui.cellrenderers;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.EFP.EfpType;

/**
 * List cell renderer.
 *
 * Date: 2.2.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class CellRendererForLists extends DefaultListCellRenderer {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(final JList list, final Object value,
            final int index, final boolean iss, final boolean chf) {

        super.getListCellRendererComponent(list, value, index, iss, chf);

        if (value instanceof EFP) {
            this.setText(((EFP) value).getName());
            ImageIcon image = null;

            if (((EFP) value).getType() == EfpType.SIMPLE) {
                image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/efpSimpleIcon.png"));
            } else {
                image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/efpDerivedIcon.png"));
            }


            setIcon(image);
        }

        if (value instanceof LrAssignment) {
            LrAssignment lrAssign = (LrAssignment) value;
            this.setText(lrAssign.getValueName() + " = " + lrAssign.getLabel());
            ImageIcon image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/LrAssignmentIcon.png"));
            setIcon(image);
        }

        if (value instanceof GR) {
            GR gr = (GR) value;
            this.setText(gr.toString());
            ImageIcon image = new ImageIcon(getClass().getClassLoader().getResource(
                        "images/GrIcon.png"));
            setIcon(image);
        }

        return this;
    }
}
