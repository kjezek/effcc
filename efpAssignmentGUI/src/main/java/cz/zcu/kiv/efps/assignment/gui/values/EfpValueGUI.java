package cz.zcu.kiv.efps.assignment.gui.values;

import java.util.Arrays;

import javax.swing.JPanel;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpRatio;
import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * Interface for visible GUI of value. <br>
 * Date: 9. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public abstract class EfpValueGUI extends JPanel {

    /** Array of all class which instances could be a value. */
    private static final Class<?>[] VALUE_TYPES = new Class<?>[] {EfpNumber.class,
            EfpNumberInterval.class, EfpString.class, EfpBoolean.class, EfpComplex.class,
            EfpEnum.class, EfpRatio.class, EfpSet.class};


    /**
     * Return names of types value.
     * @return Array names types of value;
     */
    public static String[] getNamesOfValues() {
        String[] valueTypes = new String[VALUE_TYPES.length];
        int i = 0;
        for (Class<?> cl : VALUE_TYPES) {
            valueTypes[i++] = getNameOfType(cl);
        }
        Arrays.sort(valueTypes);
        return valueTypes;
    }

    /**
     * Return a human-readable name of class.
     * @param cl Class
     * @return a human-readable name of class
     */
    public static String getNameOfType(final Class<?> cl) {
        return (String) cl.getSimpleName();
    }

    /**
     * Return class by its name.
     * @param name Name of class.
     * @return a class by name
     */
    public static Class<?> getTypeByName(final String name) {
        for (Class<?> cl : VALUE_TYPES) {
            if (getNameOfType(cl).equals(name)) {
                return cl;
            }
        }
        return null;
    }

    /** Serial version. */
    private static final long serialVersionUID = -5659228144373442304L;

    /** Properties file with localization. */
    protected Localization bundle;

    /**
     * Constructor of value.
     */
    protected EfpValueGUI() {
        bundle = new Localization("values");
    }

    /**
     * Return a value from gui.
     * @return Value
     * @throws IllegalValueException if value is not valid.
     */
    public abstract EfpValueType getValue() throws IllegalValueException;

    /**
     * Set the value to GUI.
     * @param value Value which will be set
     */
    public abstract void setValue(EfpValueType value);

    /**
     * The value will cannot edit.
     */
    public abstract void lock();
}
