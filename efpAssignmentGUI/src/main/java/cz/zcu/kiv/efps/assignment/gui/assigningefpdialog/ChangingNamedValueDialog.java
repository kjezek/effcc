package cz.zcu.kiv.efps.assignment.gui.assigningefpdialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;

import cz.zcu.kiv.efps.assignment.gui.Localization;
import cz.zcu.kiv.efps.assignment.gui.cellrenderers.CellRendererForLists;
import cz.zcu.kiv.efps.assignment.gui.exceptions.GuiRTException;
import cz.zcu.kiv.efps.types.lr.LrAssignment;

/**
 * Dialog for changing value of EFP assignment with EFP named value. It allows
 * select new value from local register from which is old value.
 *
 * Date: 5.3.2011
 * @author Jan Šváb<a href="jansvab10@centrum.cz">jansvab10@centrum.cz</a>
 */
public class ChangingNamedValueDialog extends JDialog {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** Properties file with localization. */
    private Localization localization;

    /** X-coordinate of the frame. */
    private static final int WINDOW_X = 100;
    /** Y-coordinate of the frame. */
    private static final int WINDOW_Y = 100;
    /** Width of the frame. */
    private static final int WINDOW_WIDTH = 300;
    /** Height of the frame. */
    private static final int WINDOW_HEIGHT = 300;

    /** A list with LR assignments. */
    private JList lstLRAssignments;
    /** Data model of the list with LR assignments. */
    private ListLrAssignModel lrAssignmentsDataModel;

    /** A reference to last selected LR assignment before confirm the dialog. */
    private LrAssignment selectedLrAssignment;



    /**
     * Create the dialog.
     * @param oldLrAssignment A old LR assignment.
     */
    public ChangingNamedValueDialog(final LrAssignment oldLrAssignment) {
        super(null, ModalityType.APPLICATION_MODAL);
        localization = new Localization("editDialogs");
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        selectedLrAssignment = null;
        lrAssignmentsDataModel = new ListLrAssignModel();
        int selectedItem = lrAssignmentsDataModel.setData(
                oldLrAssignment.getLr(), oldLrAssignment.getEfp(), oldLrAssignment);

        initialization();

        lstLRAssignments.setSelectedIndex(selectedItem);
    }


    /**
     * Initialization of frame.
     */
    private void initialization() {
        setTitle(localization.getProperty("dialogNamedValue"));
        setBounds(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
        getContentPane().setLayout(new BorderLayout());

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton btnChange = new JButton(localization.getProperty("btnOK"));
        btnChange.setActionCommand(localization.getProperty("btnOK"));
        btnChange.addActionListener(new ConfirmActionListener());
        buttonPane.add(btnChange);
        getRootPane().setDefaultButton(btnChange);

        JButton btnCancel = new JButton(localization.getProperty("btnStorno"));
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                setVisible(false);
            }
        });
        btnCancel.setActionCommand(localization.getProperty("btnStorno"));
        buttonPane.add(btnCancel);

        lstLRAssignments = new JList();
        lstLRAssignments.setCellRenderer(new CellRendererForLists());
        lstLRAssignments.setModel(lrAssignmentsDataModel);
        getContentPane().add(lstLRAssignments, BorderLayout.CENTER);
    }


    /** Action listener for confirm the dialog. */
    private class ConfirmActionListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            if (lstLRAssignments.getSelectedIndex() == -1) {
                throw new GuiRTException(localization.getProperty("ERROR_NO_LRASSIGN"));
            }

            selectedLrAssignment = (LrAssignment) lstLRAssignments.getSelectedValue();
            setVisible(false);
        }
    }


    /**
     * @return Last selected LR assignment
     */
    public LrAssignment getSelectedLrAssignment() {
        return selectedLrAssignment;
    }
}
