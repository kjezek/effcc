package cz.zcu.kiv.efps.assignment.gui.values;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpRatio;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * JPanel for visible ratio value. <br>
 * Date: 10. 11. 2010
 *
 * @author Martin Štulc
 *         <a href="mailto:martin.stulc@gmail.com">martin.stulc@gmail.com</a>
 */
public class EfpRatioGUI extends EfpValueGUI {

    /** Serial version. */
    private static final long serialVersionUID = 4964402100428368300L;

    /** JSpinner for value of the ratio 0-100. */
    private JTextField tfValue;

    /** Label of name of type. */
    private JLabel lblType;

    /**
     * Create the panel.
     */
    EfpRatioGUI() {
        super();
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.weightx = 0.0;
        lblType = new JLabel(bundle.getProperty("lblRatio"));
        add(lblType, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.weightx = 1.0;
        tfValue = new JTextField();
        tfValue.setToolTipText(bundle.getProperty("toolTipRatio"));
        add(tfValue, gbc);
    }

    @Override
    public EfpValueType getValue() throws IllegalValueException {
        if (tfValue.getText().equals("")) {
            throw new IllegalValueException(
                    bundle.getProperty("ERR_FILL_VALUE_RATIO"));
        }
        try {
            double value = Double.parseDouble(tfValue.getText());
            EfpRatio efpr = new EfpRatio(value);
            return efpr;
        } catch (NumberFormatException e1) {
            throw new IllegalValueException(
                    bundle.getProperty("ERR_NOT_NUMBER_RATIO") + "\n\""
                            + tfValue.getText() + "\"");
        } catch (IllegalArgumentException e) {
            throw new IllegalValueException(bundle.getProperty("ERR_NOT_SCALE_RATIO")
                    + "\n\"" + tfValue.getText() + "\"");

        }
    }

    @Override
    public void setValue(final EfpValueType value) {
        tfValue.setText(((EfpNumber) value).getLabel());
    }

    /* (non-Javadoc)
     *
     * @see values.EfpValueGUI#lock() */
    @Override
    public void lock() {
        tfValue.setEditable(false);
    }
}
