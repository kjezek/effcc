/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto;


/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LrAssignmentDTO {

    /** ID. */
    private Integer id;

    /** A value stored for this type. */
    private String value;

    /** A value name for this assignment. */
    private String valueName;

    /** lr DTO. */
    private LrDTO lr;

    /** EFP DTO. */
    private EfpDTO efp;

    /** A logical formula for derived properties. */
    private String logicalFormula;


    /**
     * @return the lrDTO
     */
    public LrDTO getLr() {
        return lr;
    }
    /**
     * @return the efpDTO
     */
    public EfpDTO getEfp() {
        return efp;
    }
    /**
     * @param lrDTO the lrDTO to set
     */
    public void setLr(final LrDTO lrDTO) {
        this.lr = lrDTO;
    }
    /**
     * @param efpDTO the efpDTO to set
     */
    public void setEfp(final EfpDTO efpDTO) {
        this.efp = efpDTO;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @return the valueName
     */
    public String getValueName() {
        return valueName;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }
    /**
     * @param value the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }
    /**
     * @param valueName the valueName to set
     */
    public void setValueName(final String valueName) {
        this.valueName = valueName;
    }


    /**
     * @return the logicalFormula
     */
    public String getLogicalFormula() {
        return logicalFormula;
    }
    /**
     * @param logicalFormula the logicalFormula to set
     */
    public void setLogicalFormula(final String logicalFormula) {
        this.logicalFormula = logicalFormula;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((lr == null) ? 0 : lr.hashCode());
        result = prime * result
                + ((valueName == null) ? 0 : valueName.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LrAssignmentDTO other = (LrAssignmentDTO) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (lr == null) {
            if (other.lr != null) {
                return false;
            }
        } else if (!lr.equals(other.lr)) {
            return false;
        }
        if (valueName == null) {
            if (other.valueName != null) {
                return false;
            }
        } else if (!valueName.equals(other.valueName)) {
            return false;
        }
        return true;
    }





}
