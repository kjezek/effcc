package cz.zcu.kiv.efps.registry.server.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import cz.zcu.kiv.efps.registry.server.beans.LrBackingBean;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.UserService;
import cz.zcu.kiv.efps.registry.server.service.impl.ChoosenObject;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTypes;
import cz.zcu.kiv.efps.registry.server.tools.EfpUtils;

/**
 * Controller for LR views handling.
 * @author Bc. Tomas Kohout
 * @since 12.07.2011
 */
@Controller("lrController")
public class LrController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(LrController.class);

  /**
   * Show LR object in left object window.
   * @param grId String
   * @param choosenObjectId String
   * @param packedListString String
   * @param ajaxEnabled String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lrObject",
      params = {
          ControllerConst.MAIN_PARAM_GR_ID,
          ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
          ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
          ControllerConst.LR_CONTROLLER_AJAX_ENABLED },
      method = RequestMethod.GET)
  public ModelAndView lrObject(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString,
      @RequestParam(ControllerConst.LR_CONTROLLER_AJAX_ENABLED) final String ajaxEnabled)
  throws EFPControllerException {

    LOGGER.info("LrController.lrObject(" + choosenObjectId + ")");

    GrEntity grEntity = null;
    int grIdNum = 0;
    if (grId != null) {
      grIdNum = Integer.valueOf(grId);
    } else {
      grEntity = grService.findAll().get(0);
      if (grEntity != null) {
        grIdNum = grEntity.getId();
      }
    }

    // Construction of Model and View
    ModelAndView mav = new ModelAndView();
    if (ControllerConst.AJAX_ENABLED.equals(ajaxEnabled.toLowerCase())) {
      mav.setViewName("object_properties_ajax");
    } else {
      mav.setViewName("main");
    }

    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);

    EntityTree entityTree = null;
    if (grIdNum != 0) {
      grEntity = grService.findById(grIdNum);
      if (packedList.isEmpty()) {
        entityTree = entityTreeService.createEntityTree(grIdNum);
      } else {
        entityTree = entityTreeService.createEntityTree(
            grIdNum,
            packedList);
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    }

    // gr entity list
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    if (grEntities != null && !grEntities.isEmpty()) {
      if (grEntity != null) {
        mav.addObject(
            ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    }

    //prepare choosen object
    ChoosenObject choosenObject = entityTreeService.getChoosenObject(grId, EntityTypes.LR.getName(),
        choosenObjectId, entityTree, packedListString);
    if (choosenObject != null) {
      entityTree.selectObject(choosenObject.getType(), choosenObject);
      mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT, choosenObject);
    }

    // send packet list
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
        packedListString);

    return mav;
  }

  /**
   * Show LR object in left object window.
   * @param grId String
   * @param choosenObjectId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lrObjectGr2",
      params = {
          ControllerConst.MAIN_PARAM_GR_ID,
          ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID },
      method = RequestMethod.GET)
  public ModelAndView lrObjectGr2(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId)
  throws EFPControllerException {

    LOGGER.info("LrController.lrObject(" + choosenObjectId + ")");

    GrEntity grEntity = null;
    int grIdNum = 0;
    if (grId != null) {
      grIdNum = Integer.valueOf(grId);
    } else {
      grEntity = grService.findAll().get(0);
      if (grEntity != null) {
        grIdNum = grEntity.getId();
      }
    }

    // Construction of Model and View
    ModelAndView mav = new ModelAndView();
    mav.setViewName("object_properties_ajax2");

    List<Integer> packedList = new LinkedList<Integer>();

    EntityTree entityTree = null;
    if (grIdNum != 0) {
      grEntity = grService.findById(grIdNum);
      if (packedList.isEmpty()) {
        entityTree = entityTreeService.createEntityTree(grIdNum);
      } else {
        entityTree = entityTreeService.createEntityTree(
            grIdNum,
            packedList);
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    }

    // gr entity list
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    if (grEntities != null && !grEntities.isEmpty()) {
      if (grEntity != null) {
        mav.addObject(
            ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    }

    //prepare choosen object
    ChoosenObject choosenObject = entityTreeService.getChoosenObject(grId, EntityTypes.LR.getName(),
        choosenObjectId, entityTree, "");
    if (choosenObject != null) {
      entityTree.selectObject(choosenObject.getType(), choosenObject);
      mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT, choosenObject);
    }

    // send packet list
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST, "");

    return mav;
  }

  /**
   * New LR.
   * @param grId Id of parent GR
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newLr",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR },
      method = RequestMethod.GET)
  public ModelAndView newLr(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    List<LrEntity> lrEntitiesForGr = new LinkedList<LrEntity>();
    lrEntitiesForGr = lrService.findLrForGr(Integer.valueOf(grId));

    ModelAndView mav = new ModelAndView("lr_screen_new");

    LrEntity lrEntity = new LrEntity();
    lrEntity.setGr(grEntity);

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    EntityTree lrTree = entityTreeService.createEntityTree(Integer.valueOf(grId));

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrEntitiesForGr);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, convert(lrEntity));
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, lrTree);

    return mav;
  }

  /**
   * Handling of newLr form.
   * @param id String
   * @param name String
   * @param parent String
   * @param parentId String
   * @param grId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newLrForm",
      params = {
          ControllerConst.LR_CONTROLLER_PARAM_ID,
          ControllerConst.LR_CONTROLLER_PARAM_NAME,
          ControllerConst.LR_CONTROLLER_PARAM_PARENT,
          ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID,
          ControllerConst.LR_CONTROLLER_PARAM_GR_ID
          },
      method = RequestMethod.POST)
  public ModelAndView newLrForm(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_ID) final String id,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT) final String parent,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID) final String parentId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_GR_ID) final String grId
      )
  throws EFPControllerException {

    LOGGER.info("LrController.newLrForm(): Creating new LR: " + name);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    LrEntity lrEntity = new LrEntity();
    lrEntity.setName(name);
    if (!parentId.equals("")) {
      lrEntity.setParent(lrService.findById(Integer.valueOf(parentId)));
    } else {
      lrEntity.setParent(null);
    }
    lrEntity.setGr(grService.findById(Integer.valueOf(grId)));
    lrEntity.setAssignments(new LinkedList<LrAssignmentEntity>());

    try {
      lrService.save(lrEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while save new LR.", ex);
    }

    ModelAndView mav = new ModelAndView("lr_screen_new_success");
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ID, lrEntity.getId());
    return mav;
  }

  /**
   * Create new LR from form values and return response for AJAX handling.
   * @param grId String
   * @param id String
   * @param name String
   * @param parent String
   * @param parentId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
  @RequestMapping(
      value = "/newLrFormAjax",
      params = {
          ControllerConst.LR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_ID,
          ControllerConst.LR_CONTROLLER_PARAM_NAME,
          ControllerConst.LR_CONTROLLER_PARAM_PARENT,
          ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID },
      method = RequestMethod.POST)
  public ModelAndView newLrFormAjax(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_ID) final String id,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT) final String parent,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID) final String parentId)
  throws EFPControllerException {

    LOGGER.info("LrController.newLrForm(): Creating new LR: " + name);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    LrEntity lrEntity = new LrEntity();
    lrEntity.setName(name);
    if (!parentId.equals("")) {
      lrEntity.setParent(lrService.findById(Integer.valueOf(parentId)));
    } else {
      lrEntity.setParent(null);
    }
    lrEntity.setGr(grService.findById(Integer.valueOf(grId)));
    lrEntity.setAssignments(new LinkedList<LrAssignmentEntity>());

    try {
      lrService.save(lrEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while save new LR.", ex);
    }

    ModelAndView mav = new ModelAndView("new_lr_success");
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity);
    return mav;
  }

  /**
   * Test of LR. If it is part of LrAssignment, it cannot be deleted directly.
   * @param lrId String
   * @return {@link ModelAndView} TRUE/FALSE
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteLrAllowed",
      params = { ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteLrAllowed(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    List<LrAssignmentEntity> assignments = lrAssignmentService.findLrAssignmentsWithoutParent(
        Integer.valueOf(lrId));

    if (assignments != null && !assignments.isEmpty()) {
      return new ModelAndView("delete_denied");
    }

    return new ModelAndView("empty_mav");
  }

  /**
   * Convert {@link LrBackingBean} to {@link LrEntity}.
   * @param bb {@link LrBackingBean}
   * @return {@link LrEntity}
   */
  private LrEntity convert(final LrBackingBean bb) {
    LrEntity lr = new LrEntity();
    if (bb != null) {
      lr.setName(bb.getName());
      if (bb.getParent() != null && !bb.getParent().equals("")) {
        lr.setParent(lrService.findById(Integer.valueOf(bb.getParent())));
      } else {
        lr.setParent(null);
      }
      lr.setGr(grService.findById(bb.getGrId()));
    }
    lr.setAssignments(new LinkedList<LrAssignmentEntity>());
    return lr;
  }

  /**
   * Only LR handling screen.
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lrScreen",
      params = {
          ControllerConst.MAIN_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView lrScreen(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    LOGGER.info("LrController.lrScreen()");

    GrEntity grEntity = null;
    if (grId == null || (grId != null && grId.equals(""))) {
      List<GrEntity> grEntities = grService.findAll();
      if (grEntities != null && !grEntities.isEmpty()) {
        grEntity = grEntities.get(0);
      } else {
        throw new EFPControllerException("Empty GR registries", null);
        // TODO nothing to show
      }
    } else {
      grEntity = grService.findById(Integer.valueOf(grId));
    }

    List<LrEntity> lrEntitiesForGr = new LinkedList<LrEntity>();
    lrEntitiesForGr = lrService.findLrForGr(grEntity.getId());

    ModelAndView mav = new ModelAndView("lr_screen");

    LrEntity lrEntity = null;
    if (lrId == null || lrId.equals("")) {
      if (!lrEntitiesForGr.isEmpty()) {
        lrEntity = lrEntitiesForGr.get(0);
      } else {
        LOGGER.warn("Empty LR LIST in: " + grId);
      }
    } else {
      lrEntity = lrService.findById(Integer.valueOf(lrId));
    }

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    EntityTree lrTree = entityTreeService.createEntityTree(grEntity.getId());

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_LR,
        lrEntity != null ? lrEntity.getId() : "");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrEntitiesForGr);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity != null ? lrEntity : "");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, lrTree);
    if (lrEntity != null) {
      mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_PARENT, lrEntity.getParent() != null
        ? lrEntity.getParent().getName() : "");
      mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID, lrEntity.getParent() != null
        ? lrEntity.getParent().getId() : "");
    }

    return mav;
  }

  /**
   * First screen for LR (show list to choose).
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lr1",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView lr1() throws EFPControllerException {

    List<GrEntity> grEntities = grService.findAll();

    Map<Integer, Set<LrEntity>> lrMap = new HashMap<Integer, Set<LrEntity>>();

    // LR are sorted by natural order by LR.name
    for (GrEntity gr : grEntities) {
      Set<LrEntity> lrList = new TreeSet<LrEntity>(new Comparator<LrEntity>() {

        @Override
        public int compare(final LrEntity o1, final LrEntity o2) {
          return o1.getName().compareTo(o2.getName());
        }

      });

      lrList.addAll(lrService.findLrForGr(gr.getId()));
      lrMap.put(gr.getId(), lrList);
    }

    ModelAndView mav = new ModelAndView("lr_1");
    mav.addObject(ControllerConst.LR_CONTROLLER_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_MAP, lrMap);
    return mav;
  }

  /**
   * Show details about LR.
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lr2",
      params = { ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView lr2(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    boolean isComponentDeveloper = true;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        if (!ControllerConst.COMPONENT_DEVELOPER_ROLE.equals(role.getName())) {
          isComponentDeveloper = false;
        }
        index++;
      }
    }

    EntityTree tree = entityTreeService.createEntityTreeLR(Integer.valueOf(lrId));

    LrEntity lr = lrService.findById(Integer.valueOf(lrId));

    List<LrAssignmentEntity> assignments = lrAssignmentService.findLrAssignments(
        Integer.valueOf(lrId));

    Collections.sort(assignments, new Comparator<LrAssignmentEntity>() {

      @Override
      public int compare(final LrAssignmentEntity o1, final LrAssignmentEntity o2) {
        return o1.getEfp().getId().compareTo(o2.getEfp().getId());
      }
    });

    List<LrEntity> lrList = new ArrayList<LrEntity>();

    for (LrEntity lrE : lrService.findLrForGr(lr.getGr().getId())) {
      if (lrE.getId().intValue() != lr.getId().intValue()) {
        lrList.add(lrE);
      }
    }

    ModelAndView mav = new ModelAndView("lr_2");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, tree);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lr);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, lr.getGr());
    mav.addObject(ControllerConst.IS_COMPONENT_DEVELOPER, isComponentDeveloper);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS, assignments);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrList);
    return mav;
  }

  /**
   * Convert {@link LrEntity} to {@link LrBackingBean}.
   * @param lrEntity {@link LrEntity}
   * @return {@link LrBackingBean}
   */
  private LrBackingBean convert(final LrEntity lrEntity) {
    LrBackingBean bb = new LrBackingBean();
    if (lrEntity != null) {
      if (lrEntity.getId() != null) {
        bb.setId(lrEntity.getId());
      }
      bb.setName(lrEntity.getName());
      bb.setParent(lrEntity.getParent() != null
          ? String.valueOf(lrEntity.getParent().getId()) : "");
      bb.setGrId(lrEntity.getGr().getId());
    }
    return bb;
  }

  /**
   * Delete Local registry.
   * @param lrId String
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/deleteLr", params = { ControllerConst.LR_CONTROLLER_PARAM_LR_ID })
  public String deleteLr(@RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID)
      final String lrId) throws EFPControllerException {

    LOGGER.info("Deleting local registry: " + lrId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      lrService.deleteById(Integer.valueOf(lrId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting LR (" + lrId + ").", ex);
    }

    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "="
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "="
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=";
  }

  /**
   * Delete selected LR and return response for AJAX handling.
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteLrAjax",
      params = { ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteLrAjax(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    LOGGER.info("Deleting local registry: " + lrId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      lrService.deleteById(Integer.valueOf(lrId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting LR (" + lrId + ").", ex);
    }

    ModelAndView mav = new ModelAndView("delete_lr_success");
    return mav;
  }

  /**
   * Redirect to new LR screen.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/lr0",
      params = { ControllerConst.LR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView lr0(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    // Prepare tree of substructures (LRs)
    List<LrEntity> lrEntities = lrService.findLrForGr(grEntity.getId());

    EntityTree lrTree = entityTreeService.createEntityTree(grEntity.getId());

    ModelAndView mav = new ModelAndView("lr_0");
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.GR_CONTROLLER_LR_LIST, lrEntities);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, lrTree);
    return mav;
  }

  /**
   * Edit LR entity.
   * @param grId String
   * @param choosenObjectId String
   * @param packedListString String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/editLr",
      params = {
        ControllerConst.MAIN_PARAM_GR_ID,
        ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
        ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST },
      method = RequestMethod.GET)
  public ModelAndView editLr(@RequestParam(ControllerConst.MAIN_PARAM_GR_ID)
      final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID)
      final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString)
  throws EFPControllerException {

    LOGGER.info("LrController.editLr lr_id: " + choosenObjectId);

    LrEntity lrEntity = lrService.findById(Integer.valueOf(choosenObjectId));

    if (lrEntity == null) {
      throw new EFPControllerException("", null);
    }

    List<LrEntity> lrEntitiesForGr = new LinkedList<LrEntity>();

    // remove this register
    for (LrEntity lr : lrService.findLrForGr(Integer.valueOf(grId))) {
      if (lr.getId() != lrEntity.getId()) {
        lrEntitiesForGr.add(lr);
      }
    }

    LrBackingBean lrBackingBean = new LrBackingBean(
        lrEntity.getId(),
        lrEntity.getName(),
        lrEntity.getParent() != null ? String.valueOf(lrEntity.getParent().getId()) : null,
        lrEntity.getGr().getId());

    ModelAndView mav = new ModelAndView("editLr");
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITY, lrBackingBean);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrEntitiesForGr);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID, choosenObjectId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST, packedListString);

    return mav;
  }

  /**
   * Edit LR Form. Handling form.
   * @param lrBackingBean backing bean object
   * @param result binding result
   * @param status Session status
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "editLrForm", method = RequestMethod.POST)
  public String editLrForm(
      final LrBackingBean lrBackingBean,
      final BindingResult result,
      final SessionStatus status) throws EFPControllerException {

    LOGGER.info("LrController.editLrForm");

    LrEntity lrEntity = lrService.findById(lrBackingBean.getId());

    if (lrEntity == null) {
      throw new EFPControllerException("Updated LrEtity does not exist in database.", null);
    }

    lrEntity.setName(lrBackingBean.getName());

    LrEntity parent = lrService.findById(Integer.valueOf(lrBackingBean.getParent()).intValue());

    lrEntity.setParent(parent);

    GrEntity grParent = grService.findById(lrBackingBean.getGrId());

    lrEntity.setGr(grParent);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      lrService.update(lrEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating "
          + lrBackingBean.getName() + " LR.", ex);
    }

    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "=" + lrEntity.getGr().getId()
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + EntityTypes.LR + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "=" + lrEntity.getId()
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=";
  }

  /**
   * Handle update LR in LR_SCREEN.
   * @param id String
   * @param name String
   * @param parentParam String
   * @param parentId String
   * @param grId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "editLrFormAjax", params = {
      ControllerConst.LR_CONTROLLER_PARAM_ID,
      ControllerConst.LR_CONTROLLER_PARAM_NAME,
      ControllerConst.LR_CONTROLLER_PARAM_PARENT,
      ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID,
      ControllerConst.LR_CONTROLLER_PARAM_GR_ID
  }, method = RequestMethod.POST)
  public ModelAndView editLrFormAjax(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_ID) final String id,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT) final String parentParam,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_PARENT_ID) final String parentId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_GR_ID) final String grId
      ) throws EFPControllerException {

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    LrEntity lrEntity = lrService.findById(Integer.valueOf(id));

    if (lrEntity == null) {
      throw new EFPControllerException("Edited LrEntity is not in Database.", null);
    }

    lrEntity.setName(name);

    LrEntity parent = null;
    try {
      parent = lrService.findById(Integer.valueOf(parentId));
    } catch (NumberFormatException ex) {
      // nothing, parent is empty => NULL
      LOGGER.info("Empty parent LREntity");
    }

    if (parent != null) {
      lrEntity.setParent(parent);
    }

    GrEntity grParent = grService.findById(Integer.valueOf(grId));

    lrEntity.setGr(grParent);

    try {
      lrService.update(lrEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating "
          + name + " LR.", ex);
    }

    ModelAndView mav = new ModelAndView("edit_lr_result");
    return mav;
  }

  /**
   * Select GR in LrScreen.
   * @param grSelect String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGrScreen",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrScreen(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT) final String grSelect)
  throws EFPControllerException {

    return "redirect:/client/lr/lrScreen?"
        + ControllerConst.MAIN_PARAM_GR_ID
        + "=" + grSelect
        + "&" + ControllerConst.LR_CONTROLLER_PARAM_LR_ID
        + "=";
  }

  /**
   * Select GR in LrScreen.
   * @param grSelect String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGrScreenNew",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrScreenNew(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT) final String grSelect)
  throws EFPControllerException {

    return "redirect:/client/lr/newLr?"
        + ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR
        + "=" + grSelect;
  }

  /**
   * Select LR in LrScreen.
   * @param lrSelect String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectLr",
      params = { ControllerConst.LR_CONTROLLER_LR_SELECT },
      method = RequestMethod.GET)
  public String selectLr(
      @RequestParam(ControllerConst.LR_CONTROLLER_LR_SELECT) final String lrSelect)
  throws EFPControllerException {

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrSelect));

    GrEntity grEntity = lrEntity.getGr();

    return "redirect:/client/lr/lrScreen?"
        + ControllerConst.MAIN_PARAM_GR_ID
        + "=" + grEntity.getId()
        + "&" + ControllerConst.LR_CONTROLLER_PARAM_LR_ID
        + "=" + lrEntity.getId();
  }

  /**
   * Run wizard for creating LR in defined GR.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/wizardLr",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR },
      method = RequestMethod.GET)
  public ModelAndView wizardLr(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    List<LrEntity> lrEntitiesForGr = new LinkedList<LrEntity>();
    lrEntitiesForGr = lrService.findLrForGr(Integer.valueOf(grId));

    LrEntity lrEntity = new LrEntity();
    lrEntity.setGr(grEntity);

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    EntityTree lrTree = entityTreeService.createEntityTree(Integer.valueOf(grId));

    ModelAndView mav = new ModelAndView("wizard_lr");

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrEntitiesForGr);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, convert(lrEntity));
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, lrTree);

    return mav;
  }

  //=============================================================================================
  // Dependency injection
  //=============================================================================================

  /** EntityTreeService. */
  private EntityTreeService entityTreeService;
  /**
   * DI setter.
   * @param entityTreeService {@link EntityTreeService}
   */
  @Autowired
  public void setEntityTreeService(final EntityTreeService entityTreeService) {
    this.entityTreeService = entityTreeService;
  }

  /** GrService. */
  private GrService grService;
  /**
   * DI setter.
   * @param grService {@link GrService}
   */
  @Autowired
  public void setGrService(final GrService grService) {
    this.grService = grService;
  }

  /**
   * Service for LR handling.
   */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }

  /** {@link LrAssignmentService}. */
  private LrAssignmentService lrAssignmentService;

  /**
   * DI setter.
   * @param lrAssignmentService {@link LrAssignmentService}
   */
  @Autowired
  public void setLrAssignmentService(final LrAssignmentService lrAssignmentService) {
    this.lrAssignmentService = lrAssignmentService;
  }

  /** UserService. */
  private UserService userService;
  /**
   * DI setter.
   * @param userService {@link UserService}
   */
  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }

  /** TxManager for Hibernate. */
  private HibernateTransactionManager transactionManager;
  /**
   * DI setter.
   * @param transactionManager {@link HibernateTransactionManager}
   */
  @Autowired
  public void setTransactionManager(final HibernateTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }
}
