package cz.zcu.kiv.efps.registry.server.tools;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Utilities for EfpServer.
 * @author Bc. Tomas Kohout
 * @since 26.07.2011
 */
public final class EfpUtils {

  /**
   * Not default constructor.
   */
  private EfpUtils() { }

  /**
   * Encode packedList into String.
   * @param packedList {@link List}
   * @return String
   */
  public static String encodePackedList(final List<Integer> packedList) {
    String result = "";
    for (int i = 0; i < packedList.size(); i++) {
      if (i < packedList.size() - 1) {
        result = result + String.valueOf(packedList.get(i)) + ",";
      } else {
        result = result + String.valueOf(packedList.get(i));
      }
    }
    return result;
  }

  /**
   * Process packedList String with packed entities.
   * @param packedListString String
   * @return {@link List}
   */
  public static List<Integer> processPackedListString(final String packedListString) {
    List<Integer> packedList = new LinkedList<Integer>();
    if (packedListString != null && !packedListString.equals("")) {
      for (String s : Arrays.asList(packedListString.split(","))) {
        packedList.add(Integer.valueOf(s));
      }
    }
    return packedList;
  }
}
