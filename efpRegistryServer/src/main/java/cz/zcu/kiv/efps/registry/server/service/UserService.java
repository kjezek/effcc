package cz.zcu.kiv.efps.registry.server.service;

import cz.zcu.kiv.efps.registry.server.entities.UserEntity;

/**
 * Service for UserEntity handling.
 * @author Bc. Tomas Kohout
 * @since 02.03.2012
 */
public interface UserService extends GenericService<UserEntity, Integer> {

  /**
   * Check login name in DB.
   * @param login String
   * @return boolean
   */
  boolean loginExist(String login);

  /**
   * Get actual logged user (UserEntity).
   * @return {@link UserEntity}
   */
  UserEntity getLoggedUser();

  /**
   * Find User by login.
   * @param login String
   * @return {@link UserEntity}
   */
  UserEntity getUserByLogin(String login);
}
