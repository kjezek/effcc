package cz.zcu.kiv.efps.registry.server.beans;

/**
 * Backing bean for LR forms.
 * @author Bc. Tomas Kohout
 * @since 26.08.2011
 */
public class LrBackingBean {

  /** ID of LR. */
  private int id;
  /** Name of LR. */
  private String name;
  /** ID of parent LR object. */
  private String parent;
  /** Id of parent GR. */
  private int grId;

  /**
   * Empty constructor.
   */
  public LrBackingBean() { }

  /**
   * Constructor with parameters.
   * @param id int
   * @param name String
   * @param parent String
   * @param grId {@link Integer}
   */
  public LrBackingBean(final int id, final String name, final String parent, final int grId) {
    this.id = id;
    this.name = name;
    this.parent = parent;
    this.grId = grId;
  }

  /**
   * Getter.
   * @return int
   */
  public int getId() {
    return id;
  }

  /**
   * Setter.
   * @param id int
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Getter.
   * @return String
   */
  public String getName() {
    return name;
  }

  /**
   * Setter.
   * @param name String
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Getter.
   * @return String
   */
  public String getParent() {
    return parent;
  }

  /**
   * Setter.
   * @param parent String
   */
  public void setParent(final String parent) {
    this.parent = parent;
  }

  /**
   * Getter.
   * @return int
   */
  public int getGrId() {
    return grId;
  }

  /**
   * Setter.
   * @param grId int
   */
  public void setGrId(final int grId) {
    this.grId = grId;
  }
}
