package cz.zcu.kiv.efps.registry.server.controllers;

/**
 * Common exception for Controllers.
 * @author Bc. Tomas Kohout
 */
public class EFPControllerException extends Exception {

  /** Generated UID. */
  private static final long serialVersionUID = 7191730266349926612L;

  /**
   * Public constructor.
   * @param message String
   * @param cause Exception
   */
  public EFPControllerException(final String message, final Exception cause) {
    super(message, cause);
  }
}
