package cz.zcu.kiv.efps.registry.server.dao.impl;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.EfpDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;

/**
 * EFP DAO impl.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Repository(value = "efpDao")
public class EfpDaoImpl extends AbstractHibernateDao<EfpEntity, Integer> implements
        EfpDao {

    /** {@inheritDoc} */
    @Override
    public EfpEntity findByName(final String name, final Integer grID) {
        Criteria crit = createCriteria()
        .add(Restrictions.eq("name", name))
        .createCriteria("gr")
        .add(Restrictions.eq("id", grID));

        return findUniqueByCriteria(crit);
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpEntity> findEfpForGr(final Integer grId) {
        Criteria crit = createCriteria()
        .createCriteria("gr")
        .add(Restrictions.eq("id", grId));

        return findByCriteria(crit);
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpEntity> findByValuName(final Integer grId, final String valueName) {
        Criteria crit = createCriteria()
            .createCriteria("valueNames")
            .add(Restrictions.eq("name", valueName))
            .createCriteria("gr")
            .add(Restrictions.eq("id", grId));

        return findByCriteria(crit);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    public List<EfpEntity> getNotAssignedEfpList() {

      Session session = getSession();

      String queryString = "SELECT ID, GAMMA, NAME, UNIT, VALUETYPE, GR_ID"
        + " FROM EFP WHERE ID NOT IN"
        + " (SELECT EFP_ID FROM LR_ASSIGNMENT) ORDER BY GR_ID ASC";

      SQLQuery query = session.createSQLQuery(queryString)
        .addEntity(EfpEntity.class);

      return (List<EfpEntity>) query.list();
    }

}
