package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;

/**
 * Choosen object for left object view panel.
 * @author Bc. Tomas Kohout
 * @since 29.06.2011
 */
public class ChoosenObject {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(ChoosenObject.class);

  /** Type of object. */
  private EntityTypes type;
  /** LrEntity of choosen object. */
  private LrEntity lrEntity;
  /** Efp entity of choosen object. */
  private EfpEntity efpEntity;
  /** Background image for choosen object. */
  private String backgroundImage;
  /** Child entities. */
  private List<LrEntity> childLrEntities;
  /** Value names. */
  private List<ValueNameEntity> valueNames;
  /** {@link LrAssignmentEntity}. */
  private LrAssignmentEntity lrAssignmentEntity;
  /** Is null in session. */
  private boolean nullValue;

  /**
   * Public constructor.
   * @param type {@link EntityTypes}
   * @param lrEntity {@link LrEntity}
   * @param efpEntity {@link EfpEntity}
   * @param childLrEntities {@link List}
   * @param valueNames {@link List}
   * @param lrAssignmentEntity {@link LrAssignmentEntity}
   */
  public ChoosenObject(
      final EntityTypes type,
      final LrEntity lrEntity,
      final EfpEntity efpEntity,
      final List<LrEntity> childLrEntities,
      final List<ValueNameEntity> valueNames,
      final LrAssignmentEntity lrAssignmentEntity) {
    switch(type) {
      case LR:
        this.type = EntityTypes.LR;
        this.lrEntity = lrEntity;
        this.efpEntity = null;
        this.backgroundImage = "lr2.gif";
        this.childLrEntities = childLrEntities;
        this.valueNames = null;
        this.lrAssignmentEntity = null;
        break;
      case ASSIGNMENT:
        this.type = EntityTypes.ASSIGNMENT;
        this.lrEntity = lrEntity;
        this.efpEntity = efpEntity;
        this.backgroundImage = "efp.gif";
        this.childLrEntities = null;
        this.valueNames = valueNames;
        this.lrAssignmentEntity = lrAssignmentEntity;
        break;
      case VALUE:
        this.type = EntityTypes.VALUE;
        this.lrEntity = lrEntity;
        this.efpEntity = efpEntity;
        this.backgroundImage = "val2.gif";
        this.childLrEntities = null;
        this.valueNames = valueNames;
        this.lrAssignmentEntity = lrAssignmentEntity;
        break;
      default:
        LOGGER.error("Unknown Entity type.");
        break;
    }
    this.nullValue = false;
  }

  /**
   * Null constructor.
   */
  public ChoosenObject() {
    this.nullValue = true;
  }

  /**
   * Get Entity type.
   * @return {@link EntityTypes}
   */
  public EntityTypes getType() {
    return type;
  }

  /**
   * Get LR entity.
   * @return {@link LrEntity}
   */
  public LrEntity getLrEntity() {
    return lrEntity;
  }

  /**
   * Get Efp Entity.
   * @return {@link EfpEntity}
   */
  public EfpEntity getEfpEntity() {
    return efpEntity;
  }

  /**
   * Get background image.
   * @return String
   */
  public String getBackgroundImage() {
    return backgroundImage;
  }

  /**
   * Get child entities, if it is possible.
   * @return List of {@link LrEntity}
   */
  public List<LrEntity> getChildLrEntities() {
    return childLrEntities;
  }

  /**
   * Get value names if it is possible.
   * @return List of {@link ValueNameEntity}
   */
  public List<ValueNameEntity> getValueNames() {
    return valueNames;
  }

  /**
   * Get {@link LrAssignmentEntity} if it is possible.
   * @return {@link LrAssignmentEntity} or null.
   */
  public LrAssignmentEntity getLrAssignmentEntity() {
    return lrAssignmentEntity;
  }

  /**
   * Is null value.
   * @return boolean
   */
  public boolean isNullValue() {
    return nullValue;
  }

  /**
   * Get ID of choosen object.
   * @return String
   */
  public String getObjectId() {
    String id = null;
    switch(type) {
      case LR:
        id = String.valueOf(lrEntity != null ? lrEntity.getId() : "");
        break;
      case ASSIGNMENT:
        id = String.valueOf(lrAssignmentEntity != null ? lrAssignmentEntity.getId() : "");
        break;
      case VALUE:
        id = String.valueOf(lrAssignmentEntity != null ? lrAssignmentEntity.getValueName() : "");
        break;
      default:
        break;
    }
    return id;
  }
}
