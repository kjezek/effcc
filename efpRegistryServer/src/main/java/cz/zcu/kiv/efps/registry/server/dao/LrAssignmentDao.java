/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;

/**
 * DAO.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrAssignmentDao extends
    GenericHibernateDao<LrAssignmentEntity, Integer> {

    /**
     * Method finds all assignments for an EFP and LR.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignmentEntity> findLrAssignments(Integer efpId, Integer lrId);

    /**
     * Method finds assignment for EFP, LR and a value name.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @param name the value name
     * @return an assignment
     */
    LrAssignmentEntity findLrAssignment(Integer efpId, Integer lrId, String name);


}
