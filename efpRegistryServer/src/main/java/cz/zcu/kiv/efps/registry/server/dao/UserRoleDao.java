package cz.zcu.kiv.efps.registry.server.dao;

import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;

/**
 * UserRoleDao - DB handling.
 * @author Bc. Tomas Kohout
 * @since 11.03.2012
 */
public interface UserRoleDao extends GenericHibernateDao<UserRoleEntity, Integer> {

  /**
   * Is it GR assigned to component developer?.
   * @param grId Integer
   * @return boolean value
   */
  boolean isAssignedToComponentDeveloper(final Integer grId);
}
