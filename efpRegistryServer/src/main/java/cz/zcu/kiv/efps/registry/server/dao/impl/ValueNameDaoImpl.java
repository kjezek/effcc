/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.ValueNameDao;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Repository(value = "valueNameDao")
public class ValueNameDaoImpl extends
        AbstractHibernateDao<ValueNameEntity, Integer> implements ValueNameDao {

    /** {@inheritDoc} */
    @Override
    public ValueNameEntity findByNameAndGr(final String name, final Integer grId) {
        Criteria crit = createCriteria()
        .add(Restrictions.eq("name", name))
        .createCriteria("gr")
        .add(Restrictions.eq("id", grId));

        return findUniqueByCriteria(crit);
    }

    /** {@inheritDoc} */
    @Override
    public List<ValueNameEntity> findByGr(final Integer grId) {
        Criteria crit = createCriteria()
        .createCriteria("gr").add(Restrictions.eq("id", grId));

        return findByCriteria(crit);
    }

    /** {@inheritDoc} */
    public List<ValueNameEntity> getNotAssignedValueList() {

      Session session = getSession();

      String queryString = "SELECT ID, NAME, GR_ID"
        + " FROM VALUE_NAME WHERE ID NOT IN"
        + " (SELECT VALUE_NAME_ID FROM LR_ASSIGNMENT) ORDER BY GR_ID ASC";

      SQLQuery query = session.createSQLQuery(queryString)
        .addEntity(ValueNameEntity.class);

      return (List<ValueNameEntity>) query.list();
    }
}
