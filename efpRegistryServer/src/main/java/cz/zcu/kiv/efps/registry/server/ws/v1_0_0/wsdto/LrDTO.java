/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto;


/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LrDTO {


    /** ID. */
    private Integer id;
    /** A human readable name of this LR. */
    private String name;
    /** A string ID. */
    private String idStr;

    /** A link to parent LR. */
    private LrDTO parent;

    /** A link to GR this LR is related to. */
    private GrDTO gr;

    /**
     * @return the gr
     */
    public GrDTO getGr() {
        return gr;
    }

    /**
     * @param gr the gr to set
     */
    public void setGr(final GrDTO gr) {
        this.gr = gr;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the string ID
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @return the parent
     */
    public LrDTO getParent() {
        return parent;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param idStr the string ID
     */
    public void setIdStr(final String idStr) {
        this.idStr = idStr;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(final LrDTO parent) {
        this.parent = parent;
    }

    /** {@inheritDoc}  */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc}  */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LrDTO other = (LrDTO) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (idStr == null) {
            if (other.idStr != null) {
                return false;
            }
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }



}
