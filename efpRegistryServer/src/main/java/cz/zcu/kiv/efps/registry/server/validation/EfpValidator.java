/**
 *
 */
package cz.zcu.kiv.efps.registry.server.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;

/**
 * This validates EFP entity.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpValidator implements Validator {


    /** {@inheritDoc} */
    @Override
    public boolean supports(final Class<?> clazz) {
        return clazz.isAssignableFrom(EfpEntity.class);
    }

    /** {@inheritDoc} */
    @Override
    public void validate(final Object target, final Errors errors) {

        EfpEntity entity = (EfpEntity) target;
        String efpName = entity.getName();

        if (!efpName.matches("\\w+")) {
            errors.rejectValue("name", "efp.name.invalid",
                    "EFP name may contain only letters, digits"
                    + " or character: '_'");
        }
    }

}
