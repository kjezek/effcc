/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto;


/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class GrDTO {

    /** ID. */
    private Integer id;
    /** A human readable name of GR. */
    private String name;
    /** A string ID of GR. */
    private String idStr;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the string id
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param idStr the string id to set
     */
    public void setIdStr(final String idStr) {
        this.idStr = idStr;
    }

    /** {@inheritDoc}  */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc}  */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GrDTO other = (GrDTO) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }


}
