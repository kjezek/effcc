/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;


/**
 * A service for EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface EfpService extends GenericService<EfpEntity, Integer> {


    /**
     * It finds an EFP via its name and GR.
     * A name of the EFP is unique for a GR.
     * @param name the name
     * @param grID the id of the GR
     * @return an EFP
     */
    EfpEntity findByName(String name, Integer grID);

    /**
     * Finds a list of EFPs for GR.
     * @param grId gr which EFPs belong to
     * @return the list of EFPs
     */
    List<EfpEntity> findEfpForGr(Integer grId);

    /**
     * This method finds a list of EFPs by a value name
     * assigned to the EFP.
     * @param grId gr ID
     * @param valueName the value name
     * @return a list of EFPs which have assigned the value name
     */
    List<EfpEntity> findByValuName(Integer grId, String valueName);

    /**
     * Get list of not assigned EFPs.
     * @return {@link List}
     */
    List<EfpEntity> getNotAssignedEfpList();
}
