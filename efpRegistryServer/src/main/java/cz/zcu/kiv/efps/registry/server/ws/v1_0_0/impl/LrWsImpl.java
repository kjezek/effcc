/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.tools.EntityConvertor;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrWs;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrDTO;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(endpointInterface = "cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrWs",
        targetNamespace = LrWs.NAMESPACE)
@Component(value = "lrWs")
public class LrWsImpl implements LrWs {

    /** A LR service reference. */
    @Resource(name = "lrService")
    private LrService lrService;

    /** A LR assignment service reference. */
    @Resource(name = "lrAssignmentService")
    private LrAssignmentService lrAssignmentService;

    /** An entity values converter. */
    @Resource(name = "entityConvertor")
    private EntityConvertor entityConvertor;

    /** A value name service reference. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** {@inheritDoc} */
    @Override
    public LrDTO getLrById(final Integer id) {
        return entityConvertor.convert(lrService.getById(id));
    }

    /** {@inheritDoc} */
    @Override
    public LrDTO createLr(final LrDTO newLr) {
        LrEntity lrEntity = entityConvertor.convert(newLr);
        lrService.save(lrEntity);
        return entityConvertor.convert(lrEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrDTO> findLrForGr(final Integer grId) {
        return entityConvertor.convertLrs(lrService.findLrForGr(grId));
    }

    /** {@inheritDoc} */
    @Override
    public void updateLr(final LrDTO modifiedLR) {
        LrEntity lrEntity = entityConvertor.convert(modifiedLR);
        lrService.update(lrEntity);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentDTO getLrAssignmentById(final Integer id) {
        LrAssignmentEntity lrAssignment = lrAssignmentService.getById(id);
        return entityConvertor.convert(lrAssignment);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentDTO createLrAssignment(final LrAssignmentDTO lrAssignmentDTO) {
        ValueNameEntity valueName = valueNameService.getByEfp(
                lrAssignmentDTO.getValueName(), lrAssignmentDTO.getEfp().getId());

        LrAssignmentEntity lrAssignmentEntity = entityConvertor.convert(lrAssignmentDTO,
                valueName);
        lrAssignmentService.save(lrAssignmentEntity);
        return entityConvertor.convert(lrAssignmentEntity);
    }

    /** {@inheritDoc} */
    @Override
    public void updateLrAssignment(final LrAssignmentDTO lrAssignmentDTO) {
        ValueNameEntity valueName = valueNameService.getByEfp(
                lrAssignmentDTO.getValueName(), lrAssignmentDTO.getEfp().getId());

        LrAssignmentEntity lrAssignmentEntity = entityConvertor.convert(lrAssignmentDTO,
                valueName);
        lrAssignmentService.update(lrAssignmentEntity);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteLrAssignment(final Integer lrAssignmentId) {
        LrAssignmentEntity lrAssignmentEntity = lrAssignmentService
                .findById(lrAssignmentId);

        lrAssignmentService.delete(lrAssignmentEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentDTO> findLrAssignments(final Integer efpId, final Integer lrId) {
        List<LrAssignmentEntity> lrAssignmentEntity = lrAssignmentService
                .findLrAssignments(efpId, lrId);

        return entityConvertor.convertLrAssignments(lrAssignmentEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentDTO> findAllLrAssignments(final Integer lrId) {
        List<LrAssignmentEntity> lrAssignmentEntity = lrAssignmentService
                .findLrAssignments(lrId);

        return entityConvertor.convertLrAssignments(lrAssignmentEntity);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentDTO findLrAssignment(final Integer efpId, final Integer lrId,
            final String name) {

        LrAssignmentEntity lrAssignmentEntity = lrAssignmentService.findLrAssignment(
                efpId, lrId, name);

        LrAssignmentDTO result = null;
        if (lrAssignmentEntity != null) {
            result = entityConvertor.convert(lrAssignmentEntity);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public void deleteLR(final Integer lrId) {
        LrEntity lrEntity = lrService.getById(lrId);
        lrService.delete(lrEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentDTO> generateLrAssignment(final EfpDTO efp, final LrDTO lr,
            final String fce, final Double from, final Double to, final Double step) {
        List<EfpValueType> values = lrAssignmentService.generateLrAssignment(fce, from,
                to, step);
        List<LrAssignmentDTO> result = entityConvertor.convertValues(values, efp, lr);
        return result;
    }

}
