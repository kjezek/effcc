/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.LrDao;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Repository(value = "lrDao")
public class LrDaoImpl extends AbstractHibernateDao<LrEntity, Integer>
        implements LrDao {

    /** {@inheritDoc} */
    @Override
    public List<LrEntity> findLrForGr(final Integer grId) {
        Criteria crit = createCriteria()
        .createCriteria("gr")
        .add(Restrictions.eq("id", grId));

        return findByCriteria(crit);
    }

}
