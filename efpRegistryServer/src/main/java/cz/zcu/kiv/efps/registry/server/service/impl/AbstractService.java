/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.io.Serializable;
import java.util.List;

import cz.zcu.kiv.efps.registry.server.dao.GenericDao;
import cz.zcu.kiv.efps.registry.server.service.GenericService;

/**
 * A convenient abstract service.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 * @param <T> type of a persistent entity
 * @param <ID> type of the ID
 * @param <V> type of the DAO
 *
 */
public abstract class AbstractService<V extends GenericDao<T, ID>, T, ID extends Serializable>
    implements  GenericService<T, ID> {

    /**
     * returns a concrete DAO implementation.
     * @return the DAO implementation
     */
    protected abstract V getDao();

    /** {@inheritDoc} */
    @Override
    public T getById(final ID id) {
        return getDao().getById(id);
    }

    /** {@inheritDoc} */
    @Override
    public T findById(final ID id) {
        return getDao().findById(id);
    }

    /** {@inheritDoc} */
    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    /** {@inheritDoc} */
    @Override
    public void save(final T entity) {
        getDao().save(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void saveWithoutLogin(final T entity) {
        getDao().save(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void update(final T entity) {
        getDao().update(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void saveOrUpdate(final T entity) {
        getDao().saveOrUpdate(entity);
     }

    /** {@inheritDoc} */
    @Override
    public void delete(final T entity) {
        getDao().delete(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteById(final ID id) {
        getDao().deleteById(id);
    }



}
