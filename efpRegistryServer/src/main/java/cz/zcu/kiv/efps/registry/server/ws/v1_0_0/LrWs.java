/** */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrDTO;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(targetNamespace = LrWs.NAMESPACE)
public interface LrWs {

    /** Target namespace. */
    String NAMESPACE = "http://v1_0_0.ws.server.registry.efps.kiv.zcu.cz";

    /**
     * It gets LR by its ID.
     *
     * @param id
     *            the ID
     * @return the found LR
     */
    LrDTO getLrById(@WebParam(name = "id") Integer id);

    /**
     * It creates new LR attaching it to existing GR.
     *
     * @param newLr
     *            new LR
     * @return a created DTO
     */
    LrDTO createLr(@WebParam(name = "newLr") LrDTO newLr);

    /**
     * Finds a list of LRs for GR.
     *
     * @param grId
     *            existing GR
     * @return the found list
     */
    List<LrDTO> findLrForGr(@WebParam(name = "gr") Integer grId);

    /**
     * Modifies existing LR.
     *
     * @param modifiedLR
     *            an object containing new values
     */
    void updateLr(@WebParam(name = "modifiedLr") LrDTO modifiedLR);

    /**
     * Gets LR Assignment by its ID.
     * @param id the ID
     * @return the LR assignment for the ID
     */
    LrAssignmentDTO getLrAssignmentById(@WebParam(name = "id") Integer id);

    /**
     * Creates a new assignment.
     * @param lrAssignment the new assignment
     * @return the created assignment
     */
    LrAssignmentDTO createLrAssignment(
            @WebParam(name = "lrAssignment") LrAssignmentDTO lrAssignment);

    /**
     * Updates an assignment.
     * @param lrAssignment the assignment to update
     */
    void updateLrAssignment(@WebParam(name = "lrAssignment") LrAssignmentDTO lrAssignment);

    /**
     * Permanently deletes an assignment.
     * @param lrAssignmentId the assignment's ID to delete
     */
    void deleteLrAssignment(@WebParam(name = "lrAssignmentId") Integer lrAssignmentId);

    /**
     * Method finds all assignments for an EFP and LR.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignmentDTO> findLrAssignments(@WebParam(name = "efpId") Integer efpId,
            @WebParam(name = "lrId") Integer lrId);

    /**
     * Method finds assignment for EFP, LR and a value name.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @param name the value name
     * @return an assignment
     */
    LrAssignmentDTO findLrAssignment(@WebParam(name = "efpId") Integer efpId,
            @WebParam(name = "lrId") Integer lrId, @WebParam(name = "name") String name);

    /**
     * Method finds all assignments for LR.
     * @param lrId local registry ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignmentDTO> findAllLrAssignments(@WebParam(name = "lrId") Integer lrId);

    /**
     * This method deletes LR with the given ID.
     * @param lrId the ID
     */
    void deleteLR(@WebParam(name = "lrId") Integer lrId);

    /**
     * Method compute values of lrAssignment under fce.
     * @param efp extra-functional property
     * @param lr local register
     * @param fce function
     * @param from lower limit of interval
     * @param to upper limit of interval
     * @param step step between the limits
     * @return a list of computed assignments
     */
    List<LrAssignmentDTO> generateLrAssignment(@WebParam(name = "efp") EfpDTO efp,
            @WebParam(name = "lr") LrDTO lr,
            @WebParam(name = "fce") java.lang.String fce,
            @WebParam(name = "from") Double from, @WebParam(name = "to") Double to,
            @WebParam(name = "step") Double step);

}
