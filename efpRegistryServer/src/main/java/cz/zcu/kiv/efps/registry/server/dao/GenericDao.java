package cz.zcu.kiv.efps.registry.server.dao;

import java.io.Serializable;
import java.util.List;

/**
 * A generic DAO interface.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T> type of an entity which this interface works on
 * @param <ID> type of an ID of the entity
 */
public interface GenericDao<T, ID extends Serializable> {


    /**
     * Gets an entity by ID.
     * @param id the ID
     * @param lock reading with locks
     * @return the found entity or an exception
     */
    T getById(ID id, boolean lock);


    /**
     * Gets an entity by ID.
     * @param id the ID
     * @return the found entity or null
     */
    T getById(ID id);

    /**
     * Gets an entity by ID.
     * @param id the ID
     * @return the found entity
     */
    T findById(ID id);


    /**
     *
     * @return all entities in database (be careful, it may be a lot of instances)
     */
    List<T> findAll();

    /**
     * Finds records by an example.
     * @param exampleInstance an example instance
     * @param excludeProperty which properties to exclude
     * @return found records
     */
    List<T> findByExample(T exampleInstance, String[] excludeProperty);


    /**
     * Persists an entity.
     * @param entity the entity
     */
    void save(T entity);


    /**
     * Update an entity.
     * @param entity the entity
     */
    void update(T entity);


    /**
     * Persits or updates an entity.
     * @param entity the entity
     */
    void saveOrUpdate(T entity);


    /**
     * Deletes an entity.
     * @param entity the entity
     */
    void delete(T entity);

    /**
     * Deletes an entity by ID.
     * @param id the ID of the entity
     */
    void deleteById(ID id);

}
