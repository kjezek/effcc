package cz.zcu.kiv.efps.registry.server.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity class for table UsersRole.
 * @author Bc. Tomas Kohout
 * @since 26.02.2012
 */
@Entity
@Table(name = "users_role")
public class UserRoleEntity implements Serializable {

  /** Serialized UID. */
  private static final long serialVersionUID = 2764285089276411711L;

  /** ID. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  /** NAME. */
  @Column(nullable = false, unique = true)
  private String name;

  /** DESCRIPTION. */
  @Column
  private String description;

  /** List of users whit this role. */
  @ManyToMany
  @JoinTable(
      name = "users_roles",
      uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "role_id" }) },
      joinColumns = { @JoinColumn(name = "role_id", nullable = false, updatable = false) },
      inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) })
  private List<UserEntity> usersWithRole;

  //======================================================================================
  // getters, setters

  /**
   * Get ID.
   * @return {@link Integer}
   */
  public Integer getId() {
    return id;
  }

  /**
   * Set ID.
   * @param id {@link Integer}
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * Get NAME.
   * @return String name
   */
  public String getName() {
    return name;
  }

  /**
   * set NAME.
   * @param name String
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Get DESCRIPTION.
   * @return String
   */
  public String getDescription() {
    return description;
  }

  /**
   * Set DESCRIPTION.
   * @param description String
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * Get list of users with this role.
   * @return {@link List}
   */
  public List<UserEntity> getUsersWithRole() {
    return usersWithRole;
  }

  /**
   * Set List of users with this role.
   * @param usersWithRole {@link List}
   */
  public void setUsersWithRole(final List<UserEntity> usersWithRole) {
    this.usersWithRole = usersWithRole;
  }
}
