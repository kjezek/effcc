/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.GrDao;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Repository(value = "grDao")
public class GrDaoImpl extends AbstractHibernateDao<GrEntity, Integer> implements GrDao {

  /** logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(GrDaoImpl.class);

  /** {@inheritDoc} */
  public void removeGRAssignToUser(final Integer grId, final Integer userId) {
    Session session = getSession();

    String queryString = "DELETE FROM ASSIGNED_GR WHERE GR_ID = "
      + grId + " AND USER_ID = " + userId;

    SQLQuery query = session.createSQLQuery(queryString);

    int res = query.executeUpdate();
    LOGGER.info("Deleted " + res + "rows");
  }

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  public List<GrEntity> getAssignedGRs(final Integer userId) {

    Session session = getSession();

    List<GrEntity> result = new ArrayList<GrEntity>();

    String queryString = "SELECT gr.ID, gr.NAME, gr.IDSTR FROM GR gr"
        + " LEFT JOIN ASSIGNED_GR ass ON ass.GR_ID = gr.ID"
        + " WHERE ass.USER_ID = " + userId;

    SQLQuery query = session.createSQLQuery(queryString)
      .addEntity(GrEntity.class);

    result = (List<GrEntity>) query.list();

    return result;
  }
}
