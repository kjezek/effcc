package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;

/**
 * Tree structure of ViewEntities.
 * @author Bc. Tomas Kohout
 * @since 12.06.2011
 */
public class EntityTree {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EntityTree.class);

  /** List of ViewEntities for view. */
  private List<ViewEntity> entities = null;
  /** Cache for LRs. */
  private Map<Integer, ViewEntity> lrCache = null;
  /** Cache fro EFPs. */
  private Map<String, List<ViewEntity>> efpCache = null;
  /** Cache for child assignments. */
  private Map<Integer, List<LrAssignmentEntity>> childAssignments = null;

  /**
   * Public constructor.
   */
  public EntityTree() {
    entities = new LinkedList<ViewEntity>();
    childAssignments = new HashMap<Integer, List<LrAssignmentEntity>>();
    lrCache = new HashMap<Integer, ViewEntity>();
    efpCache = new HashMap<String, List<ViewEntity>>();
  }

  /**
   * New REST implementation.
   */

  /**
   * Select object in a tree.
   * @param type {@link EntityTypes}
   * @param choosenObject {@link ChoosenObject}
   */
  public void selectObject(final EntityTypes type, final ChoosenObject choosenObject) {
    switch(type) {
      case LR:
        if (lrCache.containsKey(choosenObject.getLrEntity().getId())) {
          // reset all entities to not selected value (ensure)
          for (ViewEntity ve : entities) {
            ve.setSelected(false);
          }
          lrCache.get(choosenObject.getLrEntity().getId()).setSelected(true); // quick search LR
        } else {
          LOGGER.warn("LR entity marked for selection is not in a tree.");
        }
        break;
      case ASSIGNMENT:
        String key = String.valueOf(choosenObject.getLrEntity().getId())
          + "_" + String.valueOf(choosenObject.getEfpEntity().getId());
        if (efpCache.containsKey(key)) {
          // reset all entities to not selected value (ensure)
          for (ViewEntity ve : entities) {
            ve.setSelected(false);
          }
          for (ViewEntity ve : efpCache.get(key)) {
            ve.setSelected(true);
          }
        } else {
          LOGGER.warn("EFP entity marked for selection is not in a tree.");
        }
        break;
      case VALUE:
        String key2 = String.valueOf(choosenObject.getLrEntity().getId())
          + "_" + String.valueOf(choosenObject.getEfpEntity().getId());
        if (efpCache.containsKey(key2)) {
          // reset all entities to not selected value (ensure)
          for (ViewEntity ve : entities) {
            ve.setSelected(false);
          }
          for (ViewEntity ve : efpCache.get(key2)) {
            if (choosenObject.getLrAssignmentEntity().getValue().equals(
                ve.getAssignment().getValue())) {
              ve.setSelected(true);
              break;
            }
          }
        } else {
          LOGGER.warn("Value entity marked for selection is not in a tree.");
        }
        break;
        default:
          LOGGER.error("Unknown Entity type.");
          break;
    }
  }

  //=============================================================================================
  // Getters, Setters
  //=============================================================================================

  /**
   * Entities.
   * @return {@link List}
   */
  public List<ViewEntity> getEntities() {
    return entities;
  }

  /**
   * Get child assignments.
   * @return {@link Map}
   */
  public Map<Integer, List<LrAssignmentEntity>> getChildAssignments() {
    return childAssignments;
  }

  /**
   * Get LR cache.
   * @return {@link Map}
   */
  public Map<Integer, ViewEntity> getLrCache() {
    return lrCache;
  }

  /**
   * Get EFP cache.
   * @return {@link Map}
   */
  public Map<String, List<ViewEntity>> getEfpCache() {
    return efpCache;
  }

  //=============================================================================================
  // Dependency injection
  //=============================================================================================
}
