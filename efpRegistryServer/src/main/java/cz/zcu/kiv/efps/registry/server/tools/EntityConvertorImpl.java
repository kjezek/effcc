/**
 *
 */
package cz.zcu.kiv.efps.registry.server.tools;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.GrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrDTO;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * An utility class converting database entities.
 * to EFP types an vice versa
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Component(value = "entityConvertor")
public class EntityConvertorImpl implements EntityConvertor {

    /** {@inheritDoc} */
    @Override
    public EfpDTO convert(final EfpEntity entity) {
        EfpDTO result = new EfpDTO();

        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setUnit(entity.getUnit());
        result.setGamma(entity.getGamma());
        result.setGr(convert(entity.getGr()));
        result.setValueType(entity.getValueType());

        if (entity.getValueNames() != null) {
            List<String> names = convertValueNames(entity.getValueNames());
            result.setValueNames(names);

        }

        if (entity.getDerivedFromEfps() != null) {
            result.setDerivedFromEfps(convertEfps(entity.getDerivedFromEfps()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public EfpEntity convert(final EfpDTO dto, final List<ValueNameEntity> names) {
        EfpEntity result = new EfpEntity();

        result.setId(dto.getId());
        result.setName(dto.getName());
        result.setUnit(dto.getUnit());
        result.setValueNames(names);
        result.setGamma(dto.getGamma());
        result.setGr(convert(dto.getGr()));
        result.setValueType(dto.getValueType());

        if (dto.getDerivedFromEfps() != null) {
            result.setDerivedFromEfps(convertEfpsEntity(dto.getDerivedFromEfps()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpDTO> convertEfps(final List<EfpEntity> entities) {
        List<EfpDTO> result = new LinkedList<EfpDTO>();

        for (final EfpEntity entity : entities) {
            result.add(convert(entity));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpEntity> convertEfpsEntity(final List<EfpDTO> dtos) {
        List<EfpEntity> result = new LinkedList<EfpEntity>();

        for (final EfpDTO dto : dtos) {
            result.add(convert(dto, null));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentDTO convert(final LrAssignmentEntity entity) {
        LrAssignmentDTO result = new LrAssignmentDTO();

        result.setId(entity.getId());
        result.setValue(entity.getValue());
        result.setValueName(entity.getValueName().getName());
        result.setEfp(convert(entity.getEfp()));
        result.setLr(convert(entity.getLr()));
        result.setLogicalFormula(entity.getLogicalFormula());

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentDTO> convertLrAssignments(
            final List<LrAssignmentEntity> entities) {

        List<LrAssignmentDTO> result = new LinkedList<LrAssignmentDTO>();

        for (LrAssignmentEntity entity : entities) {
            result.add(convert(entity));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentEntity convert(final LrAssignmentDTO dto,
            final ValueNameEntity valueName) {
        LrAssignmentEntity result = new LrAssignmentEntity();

        result.setId(dto.getId());
        result.setValue(dto.getValue());
        result.setValueName(valueName);
        result.setEfp(convert(dto.getEfp(), null)); // refenere for value names is not needed here
        result.setLr(convert(dto.getLr()));
        result.setLogicalFormula(dto.getLogicalFormula());

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> convertValueNames(final List<ValueNameEntity> entities) {

        List<String> result = new LinkedList<String>();

        for (final ValueNameEntity entity : entities) {
            result.add(entity.getName());
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public GrDTO convert(final GrEntity entity) {
        GrDTO result = new GrDTO();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setIdStr(entity.getIdStr());

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<GrDTO> convertGrs(final List<GrEntity> entities) {
        List<GrDTO> result = new LinkedList<GrDTO>();

        for (final GrEntity entity : entities) {
            result.add(convert(entity));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public GrEntity convert(final GrDTO dto) {
        GrEntity result = new GrEntity();
        result.setId(dto.getId());
        result.setName(dto.getName());
        result.setIdStr(dto.getIdStr());

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrDTO convert(final LrEntity entity) {
        LrDTO result = new LrDTO();

        result.setGr(convert(entity.getGr()));
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setIdStr(entity.getIdStr());

        if (entity.getParent() != null) {
            // Recursively convet parent.
            result.setParent(convert(entity.getParent()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrEntity convert(final LrDTO dto) {

        LrEntity result = new LrEntity();

        result.setGr(convert(dto.getGr()));
        result.setId(dto.getId());
        result.setName(dto.getName());
        result.setIdStr(dto.getIdStr());

        if (dto.getParent() != null) {
            result.setParent(convert(dto.getParent()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrDTO> convertLrs(final List<LrEntity> entities) {
        List<LrDTO> result = new LinkedList<LrDTO>();

        for (LrEntity entity : entities) {
            result.add(convert(entity));
        }

        return result;
    }

    /** {@inheritDoc} */
    public List<LrAssignmentDTO> convertValues(final List<EfpValueType> values,
            final EfpDTO efp, final LrDTO lr) {
        List<LrAssignmentDTO> assignments = new ArrayList<LrAssignmentDTO>();
        for (EfpValueType value : values) {
            LrAssignmentDTO dto = new LrAssignmentDTO();
            dto.setEfp(efp);
            dto.setLr(lr);
            dto.setValue(value.serialise());
            // TODO [stulc] LrWs setValueName
            // dto.setValueName(valueName);

            assignments.add(dto);
        }
        return assignments;
    }

}
