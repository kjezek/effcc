/**
 *
 */
package cz.zcu.kiv.efps.registry.server.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Entity
@Table(name = "value_name",
         uniqueConstraints = {@UniqueConstraint(columnNames = { "name", "gr_id" }) }
        )
public class ValueNameEntity {

    /** ID. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /** A name of the value. */
    @Column(nullable = false)
    private String name;


    /** A name is valid for GR. */
    @ManyToOne
    @JoinColumn(name = "gr_id", nullable = false)
    private GrEntity gr;


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the gr
     */
    public GrEntity getGr() {
        return gr;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param gr the gr to set
     */
    public void setGr(final GrEntity gr) {
        this.gr = gr;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ValueNameEntity other = (ValueNameEntity) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }



}
