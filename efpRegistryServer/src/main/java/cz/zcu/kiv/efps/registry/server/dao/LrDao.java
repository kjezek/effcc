/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrEntity;

/**
 * DAO for LR entity.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrDao extends GenericHibernateDao<LrEntity, Integer>  {

    /**
     * Finds a list of LRs for GR.
     *
     * @param grId
     *            existing GR ID
     * @return the found list
     */
    List<LrEntity> findLrForGr(Integer grId);

}
