package cz.zcu.kiv.efps.registry.server.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.service.UserRoleService;
import cz.zcu.kiv.efps.registry.server.service.UserService;

/**
 * Controller for user handling.
 * @author Bc. Tomas Kohout
 * @since 11.03.2012
 */
@Controller("userController")
public class UserController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  /**
   * Test login existence (warn user by AJAX).
   * @param login String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/checkLogin",
      params = { ControllerConst.USER_CONTROLLER_LOGIN },
      method = RequestMethod.GET)
  public ModelAndView checkLogin(
      @RequestParam(ControllerConst.USER_CONTROLLER_LOGIN) final String login)
  throws EFPControllerException {

    String view = "login_ok";
    if (userService.loginExist(login)) {
      view = "login_exist";
    }

    ModelAndView mav = new ModelAndView(view);
    return mav;
  }

  /**
   * Go to registration form screen (Create new user).
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/registration", params = { }, method = RequestMethod.GET)
  public ModelAndView registration() throws EFPControllerException {

    List<UserRoleEntity> roles = userRoleService.findAll();

    ModelAndView mav = new ModelAndView("reg_0");
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    return mav;
  }

  /**
   * New user form.
   * @param login String
   * @param password String
   * @param passwordCheck String
   * @param name String
   * @param email String
   * @param phone String
   * @param roles String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newUser",
      params = {
          ControllerConst.USER_CONTROLLER_LOGIN,
          ControllerConst.USER_CONTROLLER_PASSWORD,
          ControllerConst.USER_CONTROLLER_PASSWORD_CHECK,
          ControllerConst.USER_CONTROLLER_NAME,
          ControllerConst.USER_CONTROLLER_EMAIL,
          ControllerConst.USER_CONTROLLER_PHONE,
          ControllerConst.USER_CONTROLLER_ROLES
          },
      method = RequestMethod.POST)
  public ModelAndView newUser(
      @RequestParam(ControllerConst.USER_CONTROLLER_LOGIN) final String login,
      @RequestParam(ControllerConst.USER_CONTROLLER_PASSWORD) final String password,
      @RequestParam(ControllerConst.USER_CONTROLLER_PASSWORD_CHECK) final String passwordCheck,
      @RequestParam(ControllerConst.USER_CONTROLLER_NAME) final String name,
      @RequestParam(ControllerConst.USER_CONTROLLER_EMAIL) final String email,
      @RequestParam(ControllerConst.USER_CONTROLLER_PHONE) final String phone,
      @RequestParam(ControllerConst.USER_CONTROLLER_ROLES) final String roles)
  throws EFPControllerException {

    UserEntity user = null;

    if (!userService.loginExist(login)) {

      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      user = new UserEntity();
      user.setLogin(login);
      user.setPassword("" + passwordEncoder.encodePassword(password,
          systemWideSaltSource.getSystemWideSalt()));
      user.setName(name);
      user.setEmail(email);
      user.setPhone(phone);

      String[] rolesArray = roles.split(",");
      List<UserRoleEntity> roleEntities = new ArrayList<UserRoleEntity>();

      for (String roleId : rolesArray) {
        UserRoleEntity role = userRoleService.findById(Integer.valueOf(roleId));
        if (role != null) {
          roleEntities.add(role);
        }
      }
      user.setUsersRoles(roleEntities);

      try {
        userService.saveWithoutLogin(user);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (AccessDeniedException de) {
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while saving "
            + user.getLogin() + " USER.", ex);
      }
    }

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    StringBuilder rolez = new StringBuilder();

    // Get only assigned GRs
    int index = 1;
    if (user != null) {
      for (GrEntity gr : user.getAssignedGrList()) {
        grEntities.add(gr);
      }
      for (UserRoleEntity role : user.getUsersRoles()) {
        rolez.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          rolez.append(",");
        }
        index++;
      }
    }

    ModelAndView mav = new ModelAndView("guid");
    /*
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, rolez);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    */
    return mav;
  }

  /**
   * Show edit user form.
   * @param login String
   * @param backAction String previous use case
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/reg1",
      params = {
          ControllerConst.USER_CONTROLLER_LOGIN,
          ControllerConst.BACK_ACTION },
      method = RequestMethod.GET)
  public ModelAndView reg1(
      @RequestParam(ControllerConst.USER_CONTROLLER_LOGIN) final String login,
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction)
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();

    if (!user.getLogin().equals(login)) {
      throw new EFPControllerException("You are logged as other user than you want to edit", null);
    }

    boolean isAdmin = false;
    List<UserRoleEntity> roles = new LinkedList<UserRoleEntity>();
    for (UserRoleEntity role : user.getUsersRoles()) {
      if (ControllerConst.ADMIN_ROLE.equals(role.getName())) {
        isAdmin = true;
      }
      roles.add(role);
    }

    // get user complete list
    List<UserEntity> users = null;
    Map<Integer, String> user2Roles = new HashMap<Integer, String>();
    if (isAdmin) {
      users = userService.findAll();
      for (UserEntity us : users) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        boolean first = true;
        for (UserRoleEntity r : us.getUsersRoles()) {
          if (!first) {
            sb.append(", ");
          }
          sb.append(r.getDescription());
          first = false;
        }
        sb.append(")");
        user2Roles.put(us.getId(), sb.toString());
      }
    }

    List<UserRoleEntity> userRoles = userRoleService.findAll();

    StringBuilder rolesS = new StringBuilder();

    // Get only assigned GRs
    int index = 1;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        rolesS.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          rolesS.append(",");
        }
        index++;
      }
    }

    ModelAndView mav = new ModelAndView("reg_1");
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.EDITED_USER, user);
    mav.addObject(ControllerConst.IS_AMDMIN, isAdmin);
    if (isAdmin) {
      mav.addObject(ControllerConst.USERS, users);
      mav.addObject(ControllerConst.USER_2_ROLES, user2Roles);
    }
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, userRoles);
    mav.addObject(ControllerConst.USER_ROLES_LOGGED, rolesS);
    mav.addObject(ControllerConst.USER_CONTROLLER_ROLES2, roles);
    mav.addObject(ControllerConst.BACK_ACTION, backAction);
    return mav;
  }

  /**
   * Show edit user form.
   * @param userId String
   * @param login String
   * @param backAction String previous use case
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/reg1",
      params = {
          ControllerConst.USER_CONTROLLER_LOGIN,
          ControllerConst.BACK_ACTION,
          ControllerConst.USER_ID },
      method = RequestMethod.GET)
  public ModelAndView reg1(
      @RequestParam(ControllerConst.USER_CONTROLLER_LOGIN) final String login,
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction,
      @RequestParam(ControllerConst.USER_ID) final String userId)
  throws EFPControllerException {

    UserEntity editedUser = userService.findById(Integer.valueOf(userId));

    List<UserRoleEntity> roles = new LinkedList<UserRoleEntity>();
    for (UserRoleEntity role : editedUser.getUsersRoles()) {
      roles.add(role);
    }

    ModelAndView mav = reg1(login, backAction);
    mav.addObject(ControllerConst.EDITED_USER, editedUser);
    mav.addObject(ControllerConst.USER_CONTROLLER_ROLES2, roles);
    return mav;
  }

  /**
   * Edit user (handling of form).
   * @param login String
   * @param password String
   * @param passwordCheck String
   * @param name String
   * @param email String
   * @param phone String
   * @param roles String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/editUser",
      params = {
          ControllerConst.USER_CONTROLLER_LOGIN,
          ControllerConst.USER_CONTROLLER_PASSWORD,
          ControllerConst.USER_CONTROLLER_PASSWORD_CHECK,
          ControllerConst.USER_CONTROLLER_NAME,
          ControllerConst.USER_CONTROLLER_EMAIL,
          ControllerConst.USER_CONTROLLER_PHONE,
          ControllerConst.USER_CONTROLLER_ROLES
      },
      method = RequestMethod.POST)
  public ModelAndView editUser(
      @RequestParam(ControllerConst.USER_CONTROLLER_LOGIN) final String login,
      @RequestParam(ControllerConst.USER_CONTROLLER_PASSWORD) final String password,
      @RequestParam(ControllerConst.USER_CONTROLLER_PASSWORD_CHECK) final String passwordCheck,
      @RequestParam(ControllerConst.USER_CONTROLLER_NAME) final String name,
      @RequestParam(ControllerConst.USER_CONTROLLER_EMAIL) final String email,
      @RequestParam(ControllerConst.USER_CONTROLLER_PHONE) final String phone,
      @RequestParam(ControllerConst.USER_CONTROLLER_ROLES) final String roles)
  throws EFPControllerException {

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    UserEntity user = userService.getUserByLogin(login);

    user.setName(name);
    if (password != null && !password.equals("")) {
      user.setPassword("" + passwordEncoder.encodePassword(password,
          systemWideSaltSource.getSystemWideSalt()));
    }
    user.setEmail(email);
    user.setPhone(phone);

    String[] rolesArray = roles.split(",");
    List<UserRoleEntity> roleEntities = new ArrayList<UserRoleEntity>();

    for (String roleId : rolesArray) {
      UserRoleEntity role = userRoleService.findById(Integer.valueOf(roleId));
      if (role != null) {
        roleEntities.add(role);
      }
    }
    user.setUsersRoles(roleEntities);

    try {
      userService.update(user);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
      // commit, because component developer needs to save it too
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while saving "
          + user.getLogin() + " USER.", ex);
    }

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    StringBuilder rolez = new StringBuilder();

    // Get only assigned GRs
    int index = 1;
    if (user != null) {
      for (GrEntity gr : user.getAssignedGrList()) {
        grEntities.add(gr);
      }
      for (UserRoleEntity role : user.getUsersRoles()) {
        rolez.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          rolez.append(",");
        }
        index++;
      }
    }

    ModelAndView mav = new ModelAndView("gr_1");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, rolez);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    return mav;
  }

  //===================================================================================

  /** UserService. */
  private UserService userService;
  /**
   * DI setter.
   * @param userService {@link UserService}
   */
  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }

  /** UserRoleService. */
  private UserRoleService userRoleService;
  /**
   * DI setter.
   * @param userRoleService {@link UserRoleService}
   */
  @Autowired
  public void setUserRoleService(final UserRoleService userRoleService) {
    this.userRoleService = userRoleService;
  }

  /** PasswordEncoder. */
  private PasswordEncoder passwordEncoder;
  /**
   * DI setter.
   * @param passwordEncoder {@link PasswordEncoder}
   */
  @Autowired
  public void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  /** SystemWideSaltSource. */
  private SystemWideSaltSource systemWideSaltSource;
  /***
   * DI setter.
   * @param systemWideSaltSource {@link SystemWideSaltSource}
   */
  @Autowired
  public void setSystemWideSaltSource(final SystemWideSaltSource systemWideSaltSource) {
    this.systemWideSaltSource = systemWideSaltSource;
  }

  /** TxManager for Hibernate. */
  private HibernateTransactionManager transactionManager;
  /**
   * DI setter.
   * @param transactionManager {@link HibernateTransactionManager}
   */
  @Autowired
  public void setTransactionManager(final HibernateTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }
}
