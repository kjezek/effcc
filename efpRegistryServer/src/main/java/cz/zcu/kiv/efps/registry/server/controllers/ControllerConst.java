package cz.zcu.kiv.efps.registry.server.controllers;


/**
 * Constants for controllers.
 * @author Bc. Tomas Kohout
 * @since 12.07.2011
 */
public final class ControllerConst {

  /**
   * Not public or default constructor.
   */
  private ControllerConst() { }

  /**
   * New impl for REST.
   */

  /** gr_id. */
  public static final String MAIN_PARAM_GR_ID = "gr_id";
  /** choosen_object_type. */
  public static final String MAIN_PARAM_CHOOSEN_OBJECT_TYPE = "choosen_object_type";
  /** choosen_object_id. */
  public static final String MAIN_PARAM_CHOOSEN_OBJECT_ID = "choosen_object_id";

  /** grlist. */
  public static final String MAIN_CONTROLLER_OBJECT_GR_ENTITIES = "grlist";
  /** value_types_classes. */
  public static final String EFP_CONTROLLER_VALUE_TYPES_CLASSES = "value_types_classes";
  /** lrlist. */
  public static final String MAIN_CONTROLLER_OBJECT_LR_ENTITIES = "lrlist";
  /** entityTree. */
  public static final String MAIN_CONTROLLER_OBJECT_ENTITY_TREE = "entityTree";
  /** gr_select. */
  public static final String MAIN_CONTROLLER_OBJECT_GR_SELECT = "gr_select";
  /** choosen_gr. */
  public static final String MAIN_CONTROLLER_OBJECT_CHOOSEN_GR = "choosen_gr";
  /** choosen_lr. */
  public static final String MAIN_CONTROLLER_OBJECT_CHOOSEN_LR = "choosen_lr";
  /** lr_id. */
  public static final String MAIN_CONTROLLER_PARAM_LR_ID = "lr_id";
  /** packed_list. */
  public static final String MAIN_CONTROLLER_PARAM_PACKED_LIST = "packed_list";
  /** choosenObject. */
  public static final String MAIN_PARAM_CHOOSEN_OBJECT = "choosenObject";
  /** efp_id. */
  public static final String VALUE_CONTROLLER_PARAM_EFP_ID = "efp_id";
  /** lr_id. */
  public static final String VALUE_CONTROLLER_PARAM_LR_ID = "lr_id";
  /** value_name. */
  public static final String VALUE_CONTROLLER_PARAM_VALUE_NAME = "value_name";
  /** name. */
  public static final String VALUE_CONTROLLER_PARAM_NAME = "name";
  /** Delimiter for compound IDs. */
  public static final String ID_DELIMITER = "<.>";
  /** GrId. */
  public static final String GR_CONTROLLER_PARAM_GR_ID = "gr_id";
  /** name. */
  public static final String GR_CONTROLLER_PARAM_NAME = "name";
  /** LrId. */
  public static final String LR_CONTROLLER_PARAM_LR_ID = "lr_id";
  /** LrEntity. */
  public static final String LR_CONTROLLER_PARAM_LR_ENTITY = "lr_entity";
  /** LrEntities. */
  public static final String LR_CONTROLLER_PARAM_LR_ENTITIES = "lr_entities";
  /** EfpId. */
  public static final String EFP_CONTROLLER_PARAM_EFP_ID = "efp_id";
  /** ValueId. */
  public static final String VALUE_CONTROLLER_PARAM_VALUE_ID = "value_id";
  /** value_object. */
  public static final String VALUE_CONTROLLER_VALUE_OBJECT = "value_object";
  /** GrObject. */
  public static final String GR_CONTROLLER_GR_OBJECT = "gr_object";
  /** LrTree. */
  public static final String GR_CONTROLLER_LR_TREE = "lr_tree";
  /** LrList. */
  public static final String GR_CONTROLLER_LR_LIST = "lr_list";
  /** wizard. */
  public static final String COMMON_CONTROLLER_WIZARD = "wizard";
  /** lr_object. */
  public static final String LR_CONTROLLER_LR_OBJECT = "lr_object";
  /** lr_select. */
  public static final String LR_CONTROLLER_LR_SELECT = "lr_select";
  /** assignment_id. */
  public static final String EFP_CONTROLLER_ASSIGNMENT_ID = "assignment_id";
  /** assignment_object. */
  public static final String EFP_CONTROLLER_ASSIGNMENT_OBJECT = "assignment_object";
  /** values. */
  public static final String EFP_CONTROLLER_VALUES = "values";
  /** gr_id. */
  public static final String EFP_CONTROLLER_GR_SELECT = "gr_select";
  /** lr_select. */
  public static final String EFP_CONTROLLER_LR_SELECT = "lr_select";
  /** choosen_lr. */
  public static final String EFP_CONTROLLER_CHOOSEN_LR = "choosen_lr";
  /** efp_entities. */
  public static final String EFP_CONTROLLER_EFP_ENTITIES = "efp_entities";
  /** na_efp_list. */
  public static final String EFP_CONTROLLER_NOT_ASSIGNED_EFP_LIST = "na_efp_list";
  /** na_value_list. */
  public static final String VALUE_CONTROLLER_NOT_ASSIGNED_VALUE_LIST = "na_value_list";
  /** choosen_efp. */
  public static final String EFP_CONTROLLER_CHOOSEN_EFP = "choosen_efp";
  /** efp_object. */
  public static final String EFP_CONTROLLER_EFP_OBJECT = "efp_object";
  /** value_object. */
  public static final String EFP_CONTROLLER_VALE_OBJECT = "value_object";
  /** values. */
  public static final String VALUE_CONTROLLER_VALUES = "values";
  /** choosen_value. */
  public static final String VALUE_CONTROLLER_CHOOSEN_VALUE = "choosen_value";
  /** value_select. */
  public static final String VALUE_CONTROLLER_VALUE_SELECT = "value_select";
  /** name. */
  public static final String EFP_CONTROLLER_VALUE_NAME = "name";
  /** ajax_enabled. */
  public static final String LR_CONTROLLER_AJAX_ENABLED = "ajax_enabled";
  /** ajax_enabled. */
  public static final String AJAX_ENABLED = "true";
  /** ajax_disabled. */
  public static final String AJAX_DISABLED = "false";
  /** id. */
  public static final String LR_CONTROLLER_PARAM_ID = "id";
  /** name. */
  public static final String LR_CONTROLLER_PARAM_NAME = "name";
  /** parent. */
  public static final String LR_CONTROLLER_PARAM_PARENT = "parent";
  /** parent_id. */
  public static final String LR_CONTROLLER_PARAM_PARENT_ID = "parent_id";
  /** parent_id. */
  public static final String LR_CONTROLLER_PARAM_GR_ID = "gr_id";
  /** gr_entities. */
  public static final String LR_CONTROLLER_GR_ENTITIES = "gr_entities";
  /** lr_map. */
  public static final String LR_CONTROLLER_LR_MAP = "lr_map";
  /** assignments. */
  public static final String EFP_CONTROLLER_ASSIGNMENTS = "assignments";
  /** lr_list. */
  public static final String EFP_CONTROLLER_LR_LIST = "lr_list";
  /** efp_names. */
  public static final String EFP_CONTROLLER_EFP_NAMES = "efp_names";
  /** value_names. */
  public static final String EFP_CONTROLLER_VALUE_NAMES = "value_names";
  /** efp_list. */
  public static final String EFP_CONTROLLER_EFP_LIST = "efp_list";
  /** parent (checked).*/
  public static final String EFP_CONTROLLER_PARENT = "parent";
  /** assignment_result. */
  public static final String EFP_CONTROLLER_TEST_RESULT = "assignment_result";
  /** EFP id. */
  public static final String EFP_CONTROLLER_PARAM_ID = "id";
  /** EFP name. */
  public static final String EFP_CONTROLLER_PARAM_NAME = "name";
  /** EFP gamma. */
  public static final String EFP_CONTROLLER_PARAM_GAMMA = "gamma";
  /** EFP unit. */
  public static final String EFP_CONTROLLER_PARAM_UNIT = "unit";
  /** EFP value type. */
  public static final String EFP_CONTROLLER_PARAM_VALUE_TYPE = "value_type";
  /** gr_name. */
  public static final String EFP_CONTROLLER_PARAM_GR_NAME = "gr_name";
  /** EFP gr_id. */
  public static final String EFP_CONTROLLER_PARAM_GR_ID = "gr_id";
  /** efp. */
  public static final String EFP_CONTROLLER_EFP = "efp";
  /** value. */
  public static final String EFP_CONTROLLER_VALUE = "value";
  /** efp_id. */
  public static final String EFP_CONTROLLER_EFP_ID = "efp_id";
  /** gr_id. */
  public static final String EFP_CONTROLLER_GR_ID = "gr_id";
  /** EFP lr_id. */
  public static final String EFP_CONTROLLER_PARAM_LR_ID = "lr_id";
  /** EFP value_id. */
  public static final String EFP_CONTROLLER_PARAM_VALUE_ID = "value_id";
  /** assignments. */
  public static final String VALUE_CONTROLLER_ASSIGNMENTS = "assignments";
  /** back_action. */
  public static final String BACK_ACTION = "back_action";
  /** value_object. */
  public static final String EFP_CONTROLLER_VALUE_OBJECT = "value_object";
  /** user_roles. */
  public static final String USER_CONTROLLER_USER_ROLES = "user_roles";
  /** login. */
  public static final String USER_CONTROLLER_LOGIN = "login";
  /** password. */
  public static final String USER_CONTROLLER_PASSWORD = "password";
  /** password_check. */
  public static final String USER_CONTROLLER_PASSWORD_CHECK = "password_check";
  /** name. */
  public static final String USER_CONTROLLER_NAME = "name";
  /** email. */
  public static final String USER_CONTROLLER_EMAIL = "email";
  /** phone. */
  public static final String USER_CONTROLLER_PHONE = "phone";
  /** roles. */
  public static final String USER_CONTROLLER_ROLES = "roles";
  /** roles2. */
  public static final String USER_CONTROLLER_ROLES2 = "roles2";
  /** logged_user. */
  public static final String LOGGED_USER = "logged_user";
  /** is_component_developer. */
  public static final String IS_COMPONENT_DEVELOPER = "is_component_developer";
  /** is_admin. */
  public static final String IS_AMDMIN = "is_admin";
  /** users. */
  public static final String USERS = "users";
  /** user_2_roles. */
  public static final String USER_2_ROLES = "user_2_roles";
  /** gr_2_developer_map. */
  public static final String GR_ASSIGNED_TO_DEVLOPER_MAP = "gr_2_developer_map";
  /** user_id. */
  public static final String USER_ID = "user_id";
  /** edited_user. */
  public static final String EDITED_USER = "edited_user";
  /** lr_list_size. */
  public static final String LR_LIST_SIZE = "lr_list_size";
  /** gr_2_assigned. */
  public static final String GR_2_ASSIGNED = "gr_2_assigned";
  /** user_roles_orig. */
  public static final String USER_ROLES_LOGGED = "user_roles_logged";

  /**********************************************************************************/

  /** Assignment is OK, can be created. */
  public static final int ASSIGNMENT_CAN_BE_CREATED = 0;
  /** Assignment has duplicty in DB. */
  public static final int ASSIGNMENT_DUPLICITY = 1;
  /** Assignment - something is missing. */
  public static final int ASSIGNMENT_MISSING = 2;

  /** ComponentDeveloper. */
  public static final String COMPONENT_DEVELOPER_ROLE = "ROLE_COMPONENT_DEVELOPER";
  /** DomainExpert. */
  public static final String DOMAIN_EXPERT_ROLE = "ROLE_DOMAIN_EXPERT";
  /** Admin. */
  public static final String ADMIN_ROLE = "ROLE_ADMIN";
}
