/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.tools.EntityConvertor;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrWs;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.GrDTO;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(
        endpointInterface = "cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrWs",
        targetNamespace = GrWs.NAMESPACE)
@Component(value = "grWs")
public class GrWsImpl implements GrWs {

    /** A reference to GR service. */
    @Resource(name = "grService")
    private GrService grService;

    /** An entity values converter. */
    @Resource(name = "entityConvertor")
    private EntityConvertor entityConvertor;

    /** A reference to a Value name service. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** {@inheritDoc} */
    @Override
    public GrDTO getGrById(final Integer id) {
        GrEntity grEntity = grService.getById(id);

        return entityConvertor.convert(grEntity);
    }

    /** {@inheritDoc} */
    @Override
    public GrDTO createGr(final GrDTO grDTO) {

        GrEntity grEntity = entityConvertor.convert(grDTO);
        grService.save(grEntity);

        return entityConvertor.convert(grEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<GrDTO> findAllGrs() {
        return entityConvertor.convertGrs(grService.findAll());
    }

    /** {@inheritDoc} */
    @Override
    public void updateGr(final GrDTO modifiedGr) {
        GrEntity grModifiedEntity = entityConvertor.convert(modifiedGr);
        grService.update(grModifiedEntity);
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getAllValueNames(final Integer id) {

        List<ValueNameEntity> names = valueNameService.findByGr(id);
        return entityConvertor.convertValueNames(names);
    }

    /** {@inheritDoc} */
    @Override
    public void updateValueName(final Integer id, final String oldValue, final String newValue) {

        ValueNameEntity name = valueNameService.findByName(oldValue, id);
        if (name == null) {
            throw new IllegalArgumentException(
                    "There is no Value Name for '" + oldValue + " and grId = "
                    + id);
        }

        name.setName(newValue);

        // let hibernate store it.
    }

    /** {@inheritDoc} */
    @Override
    public void deleteValueName(final Integer id, final String valueName) {
        ValueNameEntity name = valueNameService.findByName(valueName, id);

        if (name == null) {
            throw new IllegalArgumentException(
                    "There is no Value Name for '" + valueName + " and grId = "
                    + id);
        }

        valueNameService.delete(name);
    }

    /** {@inheritDoc} */
    @Override
    public void createValueName(final Integer id, final String valueName) {

        GrEntity grEntity = grService.getById(id);
        ValueNameEntity name = new ValueNameEntity();
        name.setGr(grEntity);
        name.setName(valueName);

        valueNameService.save(name);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteGR(final Integer grId) {
        GrEntity grEntity = grService.getById(grId);
        grService.delete(grEntity);
    }

}
