package cz.zcu.kiv.efps.registry.server.controllers;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cz.zcu.kiv.efps.registry.server.beans.EfpBean;
import cz.zcu.kiv.efps.registry.server.beans.ValueNameBean;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.UserService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.service.impl.ChoosenObject;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTypes;
import cz.zcu.kiv.efps.registry.server.tools.EfpUtils;

/**
 * Controller for handling with EFP (Assignments).
 */
@Controller("efpController")
public class EfpController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EfpController.class);

  /**
   * Method for showing EFP object in left object window.
   * @param grId String
   * @param lrId String
   * @param choosenObjectId String
   * @param packedListString String
   * @param ajaxEnabled String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/efpObject", params = {
      ControllerConst.MAIN_PARAM_GR_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
      ControllerConst.LR_CONTROLLER_AJAX_ENABLED },
  method = RequestMethod.GET)
  public ModelAndView efpObject(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString,
      @RequestParam(ControllerConst.LR_CONTROLLER_AJAX_ENABLED) final String ajaxEnabled)
  throws EFPControllerException {

    LOGGER.info("EfpController.efpObject(" + grId + "," + lrId + ","
        + choosenObjectId + ",[" + packedListString + "])");

    GrEntity grEntity = null;
    int grIdNum = 0;
    if (grId != null) {
      grIdNum = Integer.valueOf(grId);
    } else {
      grEntity = grService.findAll().get(0);
      if (grEntity != null) {
        grIdNum = grEntity.getId();
      }
    }

    // Construction of Model and View
    ModelAndView mav = new ModelAndView();
    if (ControllerConst.AJAX_ENABLED.equals(ajaxEnabled.toLowerCase())) {
      mav.setViewName("object_properties_ajax");
    } else {
      mav.setViewName("main");
    }

    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);

    EntityTree entityTree = null;
    if (grIdNum != 0) {
      grEntity = grService.findById(grIdNum);
      if (packedList.isEmpty()) {
        entityTree = entityTreeService.createEntityTree(grIdNum);
      } else {
        entityTree = entityTreeService.createEntityTree(
            grIdNum,
            packedList);
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    }

    // gr entity list
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    if (grEntities != null && !grEntities.isEmpty()) {
      if (grEntity != null) {
        mav.addObject(
            ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    }

    //prepare choosen object
    ChoosenObject choosenObject = entityTreeService.getChoosenObject(
        grId, EntityTypes.ASSIGNMENT.getName(),
        lrId + ControllerConst.ID_DELIMITER + choosenObjectId, entityTree, packedListString);
    if (choosenObject != null) {
      entityTree.selectObject(choosenObject.getType(), choosenObject);
      mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT, choosenObject);
    }

    // send packet list
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
        packedListString);

    return mav;
  }

  /**
   * Delete EFP.
   * @param efpId String
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/deleteEfp", params = { ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID })
  public String deleteEfp(@RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID)
      final String efpId) throws EFPControllerException {

    LOGGER.info("Deleting EFP: " + efpId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    // TODO delete assignments for EFP_ID

    try {
      efpService.deleteById(Integer.valueOf(efpId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting EFP (" + efpId + ").", ex);
    }

    // TODO  keep screen on LR
    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "="
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "="
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=";
  }

  /**
   * Delete not assigned Efp.
   * @param efpId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteEfpAjax",
      params = { ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteEfpAjax(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId)
  throws EFPControllerException {

    LOGGER.info("Deleting EFP: " + efpId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    // deleting only not assigned EFP - it is OK

    try {
      efpService.deleteById(Integer.valueOf(efpId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting EFP (" + efpId + ").", ex);
    }

    ModelAndView mav = new ModelAndView("efp_1");
    mav.addObject(ControllerConst.EFP_CONTROLLER_NOT_ASSIGNED_EFP_LIST,
        efpService.getNotAssignedEfpList());
    return mav;
  }

  /**
   * Delete assignment.
   * @param assignmentId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteAssignmentAjax",
      params = { ControllerConst.EFP_CONTROLLER_ASSIGNMENT_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteAssignmentAjax(
      @RequestParam(ControllerConst.EFP_CONTROLLER_ASSIGNMENT_ID) final String assignmentId)
  throws EFPControllerException {

    LrAssignmentEntity assignment = lrAssignmentService.findById(Integer.valueOf(assignmentId));

    if (assignment != null) {

      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      LOGGER.info("Deleting LRAssignment(ID=)" + assignmentId);

      try {
        lrAssignmentService.delete(assignment);
        transactionManager.commit(txStatus);
      } catch (AccessDeniedException de) {
        transactionManager.rollback(txStatus);
        return new ModelAndView("denied");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while deleting LR_ASSIGNMENT ("
            + assignmentId + ").", ex);
      }
    } else {
      LOGGER.warn("Assignment for deleting (" + assignmentId + ") was not found");
    }

    ModelAndView mav = new ModelAndView("delete_assignment_result");
    return mav;
  }

  /**
   * Show specific EFP screen.
   * <br /> Only EFP assignments handling.
   * @param grId String
   * @param efpId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/efpScreen",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
                 ControllerConst.VALUE_CONTROLLER_PARAM_EFP_ID },
      method = RequestMethod.GET)
  public ModelAndView efpScreen(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_EFP_ID) final String efpId)
  throws EFPControllerException {
    ModelAndView mav = new ModelAndView("efp_screen");

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    EntityTree entityTree = null;
    GrEntity grEntity = null;
    if (grId == null || (grId != null && grId.equals(""))) {
      if (grEntities != null && !grEntities.isEmpty()) {
        grEntity = grEntities.get(0);
      }
    } else {
      grEntity = grService.findById(Integer.valueOf(grId));
    }
    entityTree = entityTreeService.createEntityTree(grEntity.getId());

    List<LrEntity> lrEntitiesForGr = lrService.findLrForGr(grEntity.getId());

    // EFPs
    List<EfpEntity> efpEntities = new ArrayList<EfpEntity>();
    efpEntities.addAll(efpService.findEfpForGr(grEntity.getId()));

    // EFP object
    EfpEntity efpEntity = null;
    if (efpId != null && !efpId.equals("")) {
      efpEntity = efpService.findById(grEntity.getId());
    } else {
      if (!efpEntities.isEmpty()) {
        efpEntity = efpEntities.get(0);   // default is first from list
      }
    }

    if (efpEntity == null) {
      efpEntity = new EfpEntity();
    }

    // load values
    List<ValueNameEntity> values = new ArrayList<ValueNameEntity>();
    if (efpEntity != null) {
      values = efpEntity.getValueNames();
    }

    String chosenEfpId = efpId;
    if (chosenEfpId == null || chosenEfpId.equals("")) {
      chosenEfpId = String.valueOf(efpEntity.getId());
    }

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LR_CONTROLLER_PARAM_LR_ENTITIES, lrEntitiesForGr);
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_ENTITIES, efpEntities);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_OBJECT, efpEntity);
    mav.addObject(ControllerConst.EFP_CONTROLLER_CHOOSEN_EFP, chosenEfpId);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUES, values);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALE_OBJECT, new ValueNameBean());

    return mav;
  }

  /**
   * Show EFP screen for choosing EFP from list.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/efp1",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView efp1() throws EFPControllerException {

    ModelAndView mav = new ModelAndView("efp_1");
    mav.addObject(ControllerConst.EFP_CONTROLLER_NOT_ASSIGNED_EFP_LIST,
        efpService.getNotAssignedEfpList());
    return mav;
  }

  /**
   * Show assignments.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/efp2",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView efp2() throws EFPControllerException {

    List<LrAssignmentEntity> assignments = lrAssignmentService.findAll();

    ModelAndView mav = new ModelAndView("efp_2");
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS, assignments);
    return mav;
  }

  /**
   * Detail of EFP.
   * @param efpId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/efp3",
      params = { ControllerConst.EFP_CONTROLLER_EFP_ID },
      method = RequestMethod.GET)
  public ModelAndView efp3(
      @RequestParam(ControllerConst.EFP_CONTROLLER_EFP_ID) final String efpId)
  throws EFPControllerException {

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    boolean isComponentDeveloper = true;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        if (!ControllerConst.COMPONENT_DEVELOPER_ROLE.equals(role.getName())) {
          isComponentDeveloper = false;
        }
        index++;
      }
    }

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    // find accessible value types
    List<String> classes = null;
    try {
      classes = parseValueTypes();
    } catch (Exception ex) {
      LOGGER.warn("Cannot load datatypes", ex);
    }

    ModelAndView mav = new ModelAndView("efp_3");
    mav.addObject(ControllerConst.IS_COMPONENT_DEVELOPER, isComponentDeveloper);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    if (classes != null && !classes.isEmpty()) {
      mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_TYPES_CLASSES, classes);
    }
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_OBJECT, efpEntity);
    return mav;
  }

  /**
   * Select GR in efp_screen.
   * @param grId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(
      value = "/selectGrEfpScreen",
      params = { ControllerConst.EFP_CONTROLLER_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrEfpScreen(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId)
  throws EFPControllerException {

    return "redirect:/client/efp/efpScreen?gr_id=" + grId
      + "&efp_id=";
  }
  */

  /**
   * Select GR in efp_screen.
   * @param grId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGrEfpScreenNew",
      params = { ControllerConst.EFP_CONTROLLER_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrEfpScreenNew(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId)
  throws EFPControllerException {

    return "redirect:/client/efp/newEFP?gr_id=" + grId;
  }

  /**
   * Select EFP in efp_screen.
   * @param grId Striong
   * @param efpId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(
      value = "/selectEfp",
      params = {
          ControllerConst.EFP_CONTROLLER_GR_SELECT,
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID },
      method = RequestMethod.GET)
  public String selectAssignment(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId)
  throws EFPControllerException {
    return "redirect:/client/efp/efpScreen?gr_id=" + grId
    + "&efp_id=" + efpId;
  }
  */

  /**
   * EFP handling form.
   * @param efpObject {@link EfpEntity}
   * @param result {@link BindingResult}
   * @param status {@link SessionStatus}
   * @return String redirect
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(value = "/editEFPFormScreen", method = RequestMethod.POST)
  public String editEFPFormScreen(
      final EfpEntity efpObject,
      final BindingResult result,
      final SessionStatus status) throws EFPControllerException {

    LOGGER.info("EfpController.editEFPFormScreen");

    EfpEntity efpEntity = null;
    if (efpObject != null) {

      efpEntity = efpService.findById(efpObject.getId());

      if (efpEntity == null) {
        throw new EFPControllerException(
            "Cannot find EFP id=(" + efpObject.getId() + ") in DB. ", null);
      }

      // update attributes
      efpEntity.setName(efpObject.getName());
      efpEntity.setValueType(efpObject.getValueType());

      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      try {
        efpService.update(efpEntity);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while updating EFP "
            + "(" + efpEntity.getId() + ").", ex);
      }
    }

    return "redirect:/client/efp/efpScreen?gr_id=" + efpEntity.getGr().getId()
    + "&efp_id=" + efpEntity.getId();
  }
  */

  /**
   * New EFP handling.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(
      value = "/newEFP",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView newEFP(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    EfpEntity efp = new EfpEntity();

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    EfpBean bean = new EfpBean();
    bean.setGrId(Integer.valueOf(grId));
    bean.setEfpEntity(efp);

    ModelAndView mav = new ModelAndView("efp_screen_new");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_OBJECT, bean);
    return mav;
  }
  */

  /**
   * Create new EFP without assign.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/efp0",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView efp0(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        index++;
      }
    }

    /*
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    */

    // find accessible value types
    List<String> classes = null;
    try {
      classes = parseValueTypes();
    } catch (Exception ex) {
      LOGGER.warn("Cannot load datatypes", ex);
    }

    ModelAndView mav = new ModelAndView("efp_0");
    //mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    if (classes != null && !classes.isEmpty()) {
      mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_TYPES_CLASSES, classes);
    }
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    return mav;
  }

  /**
   * Get value types from XML config file.
   * @return {@link List}
   * @throws Exception e
   */
  private List<String> parseValueTypes() throws Exception {
    List<String> result = new ArrayList<String>();
    URL url = EfpController.class.getResource("/value_types.xml");
    File file = new File(url.getPath());
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(file);
    doc.getDocumentElement().normalize();
    NodeList nodeList = doc.getDocumentElement().getElementsByTagName("type");
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node element = (Node) nodeList.item(i);
      if (element.getNodeType() == Node.ELEMENT_NODE) {
        result.add(element.getFirstChild().getNodeValue());
      }
    }
    return result;
  }

  /**
   * Handling form new EFP.
   * @param efpObject {@link EfpBean}
   * @param result {@link BindingResult}
   * @param status {@link SessionStatus}
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newEFPFormScreen",
      params = { },
      method = RequestMethod.POST)
  public String newEFPFormScreen(
      final EfpBean efpObject,
      final BindingResult result,
      final SessionStatus status) throws EFPControllerException {

    EfpEntity efpEntity = efpObject.getEfpEntity();
    GrEntity grEntity = grService.findById(efpObject.getGrId());
    efpEntity.setGr(grEntity);

    LOGGER.info("Inserting EFP: " + efpEntity.getName());

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      efpService.save(efpEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting EFP "
          + "(" + efpEntity.getId() + ").", ex);
    }

    return "redirect:/client/efp/efpScreen?gr_id=" + efpObject.getGrId()
    + "&efp_id=" + efpEntity.getId();
  }

  /**
   * Handle AJAX form for new EFP.
   * @param name String
   * @param gamma String
   * @param unit String
   * @param valueType String
   * @param grName String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newEFPAjax",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_VALUE_TYPE,
          ControllerConst.EFP_CONTROLLER_PARAM_UNIT,
          ControllerConst.EFP_CONTROLLER_PARAM_GAMMA,
          ControllerConst.EFP_CONTROLLER_PARAM_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView newEFPAjax(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME) final String grName,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_VALUE_TYPE) final String valueType,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_UNIT) final String unit,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GAMMA) final String gamma,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    LOGGER.info("Creating new EFP ...");

    EfpEntity efpEntity = new EfpEntity();
    efpEntity.setName(name);
    efpEntity.setGamma(gamma);
    efpEntity.setUnit(unit);
    Class<?> clazz = null;
    try {
      clazz = Class.forName(valueType);
    } catch (ClassNotFoundException noex) {
      LOGGER.error("", noex);
    }
    if (clazz != null) {
      efpEntity.setValueType(clazz);
    }

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));
    efpEntity.setGr(grEntity);

    LOGGER.info("Inserting EFP: " + efpEntity.getName());

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      efpService.save(efpEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting EFP "
          + "(" + efpEntity.getId() + ").", ex);
    }

    ModelAndView mav = new ModelAndView("new_efp_result");
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP, efpEntity);
    return mav;
  }

  /**
   * Handle AJAX form for edit EFP.
   * @param efpId String
   * @param name String
   * @param gamma String
   * @param unit String
   * @param valueType String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/editEFPAjax",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID,
          //ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_VALUE_TYPE,
          ControllerConst.EFP_CONTROLLER_PARAM_UNIT,
          ControllerConst.EFP_CONTROLLER_PARAM_GAMMA,
          ControllerConst.EFP_CONTROLLER_PARAM_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView editEFPAjax(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId,
      //@RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME) final String grName,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_VALUE_TYPE) final String valueType,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_UNIT) final String unit,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GAMMA) final String gamma,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    LOGGER.info("Creating new EFP ...");

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));
    efpEntity.setName(name);
    efpEntity.setGamma(gamma);
    efpEntity.setUnit(unit);
    Class<?> clazz = null;
    try {
      clazz = Class.forName(valueType);
    } catch (ClassNotFoundException noex) {
      LOGGER.error("", noex);
    }
    if (clazz != null) {
      efpEntity.setValueType(clazz);
    }

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));
    efpEntity.setGr(grEntity);

    LOGGER.info("Updating EFP: " + efpEntity.getName());

    try {
      efpService.update(efpEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating EFP "
          + "(" + efpEntity.getId() + ").", ex);
    }

    ModelAndView mav = new ModelAndView("edit_efp_result");
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP, efpEntity);
    return mav;
  }

  /**
   * New value - Handle AJAX form on assignment screen.
   * @param name String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newValueAJAX",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView newValueAJAX(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    ValueNameEntity valueEntity = new ValueNameEntity();
    valueEntity.setName(name);

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));
    valueEntity.setGr(grEntity);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      valueNameService.save(valueEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting Value "
          + "(" + valueEntity.getId() + ").", ex);
    }

    ModelAndView mav = new ModelAndView("new_value_result");
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE, valueEntity);
    return mav;
  }

  /**
   * New Lr Assignment - handling form on assignment screen.
   * @param lrId String
   * @param efpId String
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newLrAssignment",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_LR_ID,
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID,
          ControllerConst.EFP_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.POST)
  public ModelAndView newLrAssignment(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    LrAssignmentEntity assignment = new LrAssignmentEntity();

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));
    assignment.setLr(lrEntity);

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));
    assignment.setEfp(efpEntity);

    ValueNameEntity valueNameEntity = valueNameService.findById(Integer.valueOf(valueId));
    assignment.setValueName(valueNameEntity);

    assignment.setValue(valueNameEntity.getName());
    // TODO + logical formula

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      lrAssignmentService.save(assignment);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting LR Assignment "
          + "(" + assignment.getId() + ").", ex);
    }

    List<LrAssignmentEntity> assignments = new ArrayList<LrAssignmentEntity>();
    assignments = lrAssignmentService.findLrAssignments(Integer.valueOf(lrId));

    ModelAndView mav = new ModelAndView("new_assignment_result");
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS, assignments);
    return mav;
  }

  /**
   * Handle form create assignment.
   * @param lrId String
   * @param efpId String
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/createAssignmentAjax",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_LR_ID,
          ControllerConst.EFP_CONTROLLER_EFP_ID,
          ControllerConst.EFP_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.POST)
  public ModelAndView createAssignmentAjax(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    LrAssignmentEntity assignment = new LrAssignmentEntity();

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));
    assignment.setLr(lrEntity);

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));
    assignment.setEfp(efpEntity);

    ValueNameEntity valueNameEntity = valueNameService.findById(Integer.valueOf(valueId));
    assignment.setValueName(valueNameEntity);

    assignment.setValue(valueNameEntity.getName());
    // TODO + logical formula

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      lrAssignmentService.save(assignment);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT EFP ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting LR Assignment "
          + "(" + assignment.getId() + ").", ex);
    }

    ModelAndView mav = new ModelAndView("ass_1_ok");
    return mav;
  }

  /**
   * New ValueName for EFP.
   * @param valueNameBean {@link ValueNameBean}
   * @param result {@link BindingResult}
   * @param status {@link SessionStatus}
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newValueForEfpForm",
      params = { },
      method = RequestMethod.POST)
  public String newValueForEfpForm(
      final ValueNameBean valueNameBean,
      final BindingResult result,
      final SessionStatus status)
  throws EFPControllerException {

    ValueNameEntity valueNameEntity = valueNameBean.getValueNameEntity();

    if (valueNameEntity != null) {

      LOGGER.info("Inserting ValueName (" + valueNameEntity.getName() + ")");

      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      GrEntity gr = grService.findById(Integer.valueOf(valueNameBean.getGrId()));
      valueNameEntity.setGr(gr);

      // save valueName
      try {
        valueNameService.save(valueNameEntity);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while inserting ValueName "
            + "(" + valueNameEntity.getName() + ").", ex);
      }

      EfpEntity efpEntity = efpService.findById(Integer.valueOf(valueNameBean.getEfpId()));

      // add ValueName to EFP
      efpEntity.getValueNames().add(valueNameEntity);

      // save it
      txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());
      try {
        efpService.update(efpEntity);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while updating EfpEntity "
            + "(" + efpEntity.getId() + ").", ex);
      }

      // make assignment
      LrAssignmentEntity assignment = new LrAssignmentEntity();
      assignment.setValue(valueNameEntity.getName()); // TODO check
      assignment.setValueName(valueNameEntity);
      assignment.setEfp(efpEntity);
      LrEntity lrEntity = lrService.findById(Integer.valueOf(valueNameBean.getLrId()));
      assignment.setLr(lrEntity);

      // save it
      txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());
      try {
        lrAssignmentService.save(assignment);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while inserting LrAssignment "
            + "(" + assignment.getValue() + ").", ex);
      }
    }

    return "redirect:/client/efp/efpScreen?gr_id=" + valueNameBean.getGrId()
    + "&efp_id=" + valueNameBean.getEfpId();
  }

  /**
   * New Value for EFP (AJAX processing).
   * <br /><i>without page refresh</i>
   * @param grId String
   * @param efpId String
   * @param name String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newValueForEfpFormAJAX",
      params = {
          ControllerConst.EFP_CONTROLLER_VALUE_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID,
          ControllerConst.VALUE_CONTROLLER_PARAM_LR_ID,
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView newValueForEfpFormAJAX(
      @RequestParam(ControllerConst.EFP_CONTROLLER_VALUE_NAME) final String name,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    ValueNameEntity valueNameEntity = null;
    valueNameEntity = valueNameService.findByName(name, Integer.valueOf(grId));
    if (valueNameEntity == null) {
      valueNameEntity = new ValueNameEntity();

      // set values
      valueNameEntity.setName(name);
      GrEntity gr = grService.findById(Integer.valueOf(grId));
      valueNameEntity.setGr(gr);
    }

    LOGGER.info("Inserting(updating) ValueName (" + valueNameEntity.getName() + ")");

    TransactionStatus txStatus = transactionManager
        .getTransaction(new DefaultTransactionDefinition());

    // save valueName
    try {
      valueNameService.saveOrUpdate(valueNameEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting ValueName "
          + "(" + valueNameEntity.getName() + ").", ex);
    }

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));

    // add ValueName to EFP
    efpEntity.getValueNames().add(valueNameEntity);
    List<ValueNameEntity> values = efpEntity.getValueNames();

    // save it
    txStatus = transactionManager
        .getTransaction(new DefaultTransactionDefinition());
    try {
      efpService.update(efpEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating EfpEntity "
          + "(" + efpEntity.getId() + ").", ex);
    }

    // make assignment
    LrAssignmentEntity assignment = new LrAssignmentEntity();
    assignment.setValue(valueNameEntity.getName()); // TODO check
    assignment.setValueName(valueNameEntity);
    assignment.setEfp(efpEntity);
    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));
    assignment.setLr(lrEntity);

    // save it
    txStatus = transactionManager
        .getTransaction(new DefaultTransactionDefinition());
    try {
      lrAssignmentService.save(assignment);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException(
          "Exception while inserting LrAssignment " + "("
              + assignment.getValue() + ").", ex);
    }

    ModelAndView mav = new ModelAndView("efp_values_ajax_list");
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUES, values);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);

    return mav;
  }

  /**
   * Show delete LR assignment screen.
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteAssignmentScreen",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteAssignmentScreen(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    LrEntity lrEntity = null;
    if (lrId == null || (lrId != null && lrId.equals(""))) {
      List<LrEntity> lrs = lrService.findLrForGr(Integer.valueOf(grId));
      if (lrs != null && !lrs.isEmpty()) {
        lrEntity = lrs.get(0);
      }
    } else {
      lrEntity = lrService.findById(Integer.valueOf(lrId));
    }

    ModelAndView mav = new ModelAndView("delete_assignment");

    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grService.findAll());
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_LR_ENTITIES,
        lrService.findLrForGr(Integer.valueOf(grId)));
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_LR, lrEntity.getId());
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS,
        lrAssignmentService.findLrAssignments(lrEntity.getId()));

    return mav;
  }

  /**
   * Delete value for EFP (Delete with assignments).
   * @param efpId String
   * @param valueId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteValueEfpScreen",
      params = {
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID,
          ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.GET)
  public String deleteValueEfpScreen(
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));

    // delete assignments before
    List<LrAssignmentEntity> assignments = lrAssignmentService.findAll();
    for (LrAssignmentEntity assignment : assignments) {
      if (assignment.getEfp().getId().intValue() == Integer.parseInt(efpId)
          && assignment.getValueName().getId().intValue() == Integer.parseInt(valueId)) {

        TransactionStatus txStatus = transactionManager.getTransaction(
            new DefaultTransactionDefinition());

        try {
          lrAssignmentService.delete(assignment);
          transactionManager.commit(txStatus);
          LOGGER.info("*** DB COMMIT ***");
        } catch (Exception ex) {
          if (!txStatus.isCompleted()) {
            transactionManager.rollback(txStatus);
          }
          throw new EFPControllerException("Exception while deleting LrAssignment "
              + "(" + assignment.getValue() + ").", ex);
        }

      }
    }

    // delete from EFP names
    ValueNameEntity valueForDelete = null;
    for (ValueNameEntity valueName : efpEntity.getValueNames()) {
      if (valueName.getId().intValue() == Integer.parseInt(valueId)) {
        valueForDelete = valueName;
      }
    }
    efpEntity.getValueNames().remove(valueForDelete);
    // save change
    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      efpService.update(efpEntity);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating EfpEntity "
          + "(" + efpEntity.getId() + ").", ex);
    }

    // delete Value
    ValueNameEntity valueNameEntity = valueNameService.findById(Integer.valueOf(valueId));
    if (valueNameEntity != null) {
      txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      try {
        valueNameService.delete(valueNameEntity);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while deleting ValueName "
            + "(" + valueNameEntity.getName() + ").", ex);
      }
    }

    return "redirect:/client/efp/efpScreen?gr_id=" + efpEntity.getGr().getId()
    + "&efp_id=" + efpId;
  }

  /**
   * Assignment screen (Drag & Drop for assignments).
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/assignmentScreen",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID
          },
      method = RequestMethod.GET)
  public ModelAndView assignmentScreen(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    LrEntity lrEntity = null;
    if (lrId == null || (lrId != null && lrId.equals(""))) {
      List<LrEntity> lrs = lrService.findLrForGr(Integer.valueOf(grId));
      if (lrs != null && !lrs.isEmpty()) {
        lrEntity = lrs.get(0);
      }
    } else {
      lrEntity = lrService.findById(Integer.valueOf(lrId));
    }

    List<EfpEntity> efpList = new ArrayList<EfpEntity>();
    List<ValueNameEntity> valueList = new ArrayList<ValueNameEntity>();

    if (lrEntity != null) {
      efpList = efpService.findEfpForGr(lrEntity.getGr().getId());
      valueList = valueNameService.findByGr(lrEntity.getGr().getId());
    }

    ModelAndView mav = new ModelAndView("assignment_screen");
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_ENTITIES, efpList);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUES, valueList);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grService.findAll());
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_LR_ENTITIES,
        lrService.findLrForGr(Integer.valueOf(grId)));
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_LR, lrEntity.getId());
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS,
        lrAssignmentService.findLrAssignments(lrEntity.getId()));
    return mav;
  }

  /**
   * New assignment.
   * @param backAction String what is back action (view for back action)
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/ass0",
      params = { ControllerConst.BACK_ACTION },
      method = RequestMethod.GET)
  public ModelAndView ass0(
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction)
  throws EFPControllerException {

    List<LrEntity> lrList = lrService.findAll();
    List<ValueNameEntity> valueList = valueNameService.findAll();
    List<EfpEntity> efpList = efpService.findAll();

    ModelAndView mav = new ModelAndView("ass_0");
    mav.addObject(ControllerConst.EFP_CONTROLLER_LR_LIST, lrList);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_NAMES, valueList);
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_LIST, efpList);
    mav.addObject(ControllerConst.BACK_ACTION, backAction);
    return mav;
  }

  /**
   * Create ASSIGNMENT for existing not assigned EFP.
   * @param grId String (after choosing LR)
   * @param efpId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/ass1",
      params = {
          ControllerConst.EFP_CONTROLLER_GR_ID,
          ControllerConst.EFP_CONTROLLER_EFP_ID },
      method = RequestMethod.GET)
  public ModelAndView ass1(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_ID) final String grId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_EFP_ID) final String efpId)
  throws EFPControllerException {

    List<LrEntity> lrList = lrService.findLrForGr(Integer.valueOf(grId));

    List<ValueNameEntity> valueList = null;
    if (grId != null && !grId.equals("")) {
      valueList = valueNameService.findByGr(Integer.valueOf(grId));
    } else {
      valueList = valueNameService.findAll();
    }

    EfpEntity efpEntity = efpService.findById(Integer.valueOf(efpId));

    ModelAndView mav = new ModelAndView("ass_1");
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_OBJECT, efpEntity);
    mav.addObject(ControllerConst.EFP_CONTROLLER_LR_LIST, lrList);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_NAMES, valueList);
    return mav;
  }

  /**
   * Prepare screen for creating assignment (choose EFP and VALUE).
   * @param backAction String
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/ass3",
      params = {
          ControllerConst.BACK_ACTION,
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView ass3(
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        index++;
      }
    }

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    List<EfpEntity> listEFP = efpService.findEfpForGr(Integer.valueOf(grId));

    List<ValueNameEntity> valueList = null;
    if (grId != null && !grId.equals("")) {
      valueList = valueNameService.findByGr(Integer.valueOf(grId));
    } else {
      valueList = valueNameService.findAll();
    }

    ModelAndView mav = new ModelAndView("ass_3");
    mav.addObject(ControllerConst.BACK_ACTION, backAction);
    mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_LIST, listEFP);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_NAMES, valueList);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    return mav;
  }

  /**
   * Prepare screen for creating assignment (choose EFP and VALUE).
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/ass3",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID },
      method = RequestMethod.GET)
  public ModelAndView ass3(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId)
  throws EFPControllerException {
    return ass3("gr/gr2?gr_id=" + grId, grId, lrId);
  }

  /**
   * Delete LR assignment and return result.
   * @param assignmentId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteAssignment",
      params = { ControllerConst.EFP_CONTROLLER_ASSIGNMENT_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteAssignment(
      @RequestParam(ControllerConst.EFP_CONTROLLER_ASSIGNMENT_ID) final String assignmentId)
  throws EFPControllerException {

    ModelAndView mav = null;

    LrAssignmentEntity assignment = lrAssignmentService.findById(Integer.valueOf(assignmentId));

    if (assignment != null) {

      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      try {
        lrAssignmentService.delete(assignment);
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (AccessDeniedException de) {
        transactionManager.rollback(txStatus);
        return new ModelAndView("denied");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while deleting LR assignment ("
            + assignmentId + ").", ex);
      }

      mav = new ModelAndView("delete_assignment_result");

    } else {
      mav = null;
    }

    return mav;
  }

  /**
   * Get LR Assignments list (enable/disable parent LRs).
   * @param lrId String
   * @param parent String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/getAssignments",
      params = {
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID,
          ControllerConst.EFP_CONTROLLER_PARENT },
      method = RequestMethod.GET)
  public ModelAndView getAssignments(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARENT) final String parent)
  throws EFPControllerException {

    boolean showParent = false;
    if (parent != null && parent.equals("checked")) {
      showParent = true;
    }

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));

    List<LrAssignmentEntity> assignments = new ArrayList<LrAssignmentEntity>();
    List<LrAssignmentEntity> assignmentsTemp = new ArrayList<LrAssignmentEntity>();
    if (lrEntity != null) {
      assignmentsTemp = lrAssignmentService.findLrAssignments(lrEntity.getId());
    }

    // show assignments only from THIS LR. - Filter others
    if (!showParent) {
      for (LrAssignmentEntity assignment : assignmentsTemp) {
        if (assignment.getLr().getId().intValue() == lrEntity.getId().intValue()) {
          assignments.add(assignment);
        }
      }
    } else {
      assignments.addAll(assignmentsTemp);
    }

    ModelAndView mav = new ModelAndView("assignment_list");
    mav.addObject(ControllerConst.EFP_CONTROLLER_ASSIGNMENTS, assignments);
    mav.addObject(ControllerConst.LR_CONTROLLER_LR_OBJECT, lrEntity);
    mav.addObject(ControllerConst.EFP_CONTROLLER_PARENT, showParent ? "checked" : "");
    return mav;
  }

  /**
   * Test LR assignment before saving - event on form - AJAX.
   * @param lrId String
   * @param efpId String
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/testAssignment",
      params = {
          ControllerConst.LR_CONTROLLER_PARAM_LR_ID,
          ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID,
          ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.GET)
  public ModelAndView testAssignment(
      @RequestParam(ControllerConst.LR_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    // 1 - this assignment exists in DB (duplicity)
    // 0 - OK, can be created
    // 2 - Something is missing
    int result = ControllerConst.ASSIGNMENT_CAN_BE_CREATED;

    boolean missing = false;
    if (efpId == null || (efpId != null && efpId.equals(""))) {
      result = ControllerConst.ASSIGNMENT_MISSING;
      missing = true;
    }
    if (valueId == null || (valueId != null && valueId.equals(""))) {
      result = ControllerConst.ASSIGNMENT_MISSING;
      missing = true;
    }

    if (!missing) {

      LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));
      List<LrAssignmentEntity> assignmentsTemp = new ArrayList<LrAssignmentEntity>();
      List<LrAssignmentEntity> assignments = new ArrayList<LrAssignmentEntity>();
      if (lrEntity != null) {
        assignmentsTemp = lrAssignmentService.findLrAssignments(lrEntity
            .getId());
        for (LrAssignmentEntity assignment : assignmentsTemp) {
          if (assignment.getLr().getId().intValue() == lrEntity.getId()
              .intValue()) {
            assignments.add(assignment);
          }
        }

        // test
        for (LrAssignmentEntity assignment : assignments) {
          if (assignment.getEfp().getId().intValue() == Integer.parseInt(efpId)
              && assignment.getValueName().getId().intValue() == Integer
                  .parseInt(valueId)) {
            result = ControllerConst.ASSIGNMENT_DUPLICITY;
            break;
          }
        }
      }

    }

    ModelAndView mav = new ModelAndView("test_assignment");
    mav.addObject(ControllerConst.EFP_CONTROLLER_TEST_RESULT, result);
    return mav;
  }

  /**
   * Run wizard for EFP.
   * @param grId String
   * @param lrId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/wizardEfp",
      params = {
          ControllerConst.EFP_CONTROLLER_GR_SELECT,
          ControllerConst.EFP_CONTROLLER_CHOOSEN_LR },
      method = RequestMethod.GET)
  public ModelAndView wizardEfp(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId,
      @RequestParam(ControllerConst.EFP_CONTROLLER_CHOOSEN_LR) final String lrId)
  throws EFPControllerException {
    // TODO
    ModelAndView mav = new ModelAndView("wizard_efp");
    return mav;
  }

  /**
   * Select GR on assignment screen.
   * @param grId String
   * @return redirect String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGrAssignmentScreen",
      params = { ControllerConst.EFP_CONTROLLER_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrAssignmentScreen(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId)
  throws EFPControllerException {
    return "redirect:/client/efp/assignmentScreen?gr_id=" + grId + "&lr_id=";
  }

  /**
   * Select GR on delete assignment screen.
   * @param grId String
   * @return redirect String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGrDeleteAssignment",
      params = { ControllerConst.EFP_CONTROLLER_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrDeleteAssignment(
      @RequestParam(ControllerConst.EFP_CONTROLLER_GR_SELECT) final String grId)
  throws EFPControllerException {
    return "redirect:/client/efp/deleteAssignmentScreen?gr_id=" + grId + "&lr_id=";
  }

  /**
   * Select LR on assignment screen.
   * @param lrId String
   * @return redirect String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectLrAssignmentScreen",
      params = { ControllerConst.EFP_CONTROLLER_LR_SELECT },
      method = RequestMethod.GET)
  public String selectLrAssignmentScreen(
      @RequestParam(ControllerConst.EFP_CONTROLLER_LR_SELECT) final String lrId)
  throws EFPControllerException {

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));

    return "redirect:/client/efp/assignmentScreen?gr_id="
      + lrEntity.getGr().getId() + "&lr_id=" + lrId;
  }

  /**
   * Select LR on delete assignment screen.
   * @param lrId String
   * @return redirect String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectLrDeleteAssignment",
      params = { ControllerConst.EFP_CONTROLLER_LR_SELECT },
      method = RequestMethod.GET)
  public String selectLrDeleteAssignment(
      @RequestParam(ControllerConst.EFP_CONTROLLER_LR_SELECT) final String lrId)
  throws EFPControllerException {

    LrEntity lrEntity = lrService.findById(Integer.valueOf(lrId));

    return "redirect:/client/efp/deleteAssignmentScreen?gr_id="
      + lrEntity.getGr().getId() + "&lr_id=" + lrId;
  }

  //=============================================================================================
  // Dependency injection
  //=============================================================================================

  /** GrService. */
  private GrService grService;
  /**
   * DI setter.
   * @param grService {@link GrService}
   */
  @Autowired
  public void setGrService(final GrService grService) {
    this.grService = grService;
  }

  /** EntityTreeService. */
  private EntityTreeService entityTreeService;
  /**
   * DI setter.
   * @param entityTreeService {@link EntityTreeService}
   */
  @Autowired
  public void setEntityTreeService(final EntityTreeService entityTreeService) {
    this.entityTreeService = entityTreeService;
  }

  /** Service for LR handling. */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }

  /** LrAssignmentService. */
  private LrAssignmentService lrAssignmentService;
  /**
   * DI setter.
   * @param lrAssignmentService {@link LrAssignmentService}
   */
  @Autowired
  public void setLrAssignmentService(final LrAssignmentService lrAssignmentService) {
    this.lrAssignmentService = lrAssignmentService;
  }

  /** TxManager for Hibernate. */
  private HibernateTransactionManager transactionManager;
  /**
   * DI setter.
   * @param transactionManager {@link HibernateTransactionManager}
   */
  @Autowired
  public void setTransactionManager(final HibernateTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  /** ValueNameService. */
  private ValueNameService valueNameService;

  /**
   * DI setter.
   * @param valueNameService {@link ValueNameService}
   */
  @Autowired
  public void setValueNameService(final ValueNameService valueNameService) {
    this.valueNameService = valueNameService;
  }

  /** EfpService. */
  private EfpService efpService;
  /**
   * DI setter.
   * @param efpService {@link EfpService}
   */
  @Autowired
  public void setEfpService(final EfpService efpService) {
    this.efpService = efpService;
  }

  /** UserService. */
  private UserService userService;
  /**
   * DI setter.
   * @param userService {@link UserService}
   */
  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }
}
