package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrEntity;

/**
 * Service for LR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrService extends GenericService<LrEntity, Integer> {


    /**
     * Finds a list of LRs for GR.
     *
     * @param grId
     *            existing GR ID
     * @return the found list
     */
    List<LrEntity> findLrForGr(Integer grId);

}
