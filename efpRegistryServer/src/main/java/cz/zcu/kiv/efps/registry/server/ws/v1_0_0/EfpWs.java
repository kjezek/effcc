/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;

/**
 * A web service providing operations
 * on EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(targetNamespace = EfpWs.NAMESPACE)
public interface EfpWs {

    /** Target namespace. */
    String NAMESPACE = "http://v1_0_0.ws.server.registry.efps.kiv.zcu.cz";

    /**
     *
     * @param name a name of an EFP
     * @param grId id of GR
     * @return the EFP or null
     *
     */
    EfpDTO findEfpByName(
            @WebParam(name = "efpName") String name,
            @WebParam(name = "grID") Integer grId);

    /**
     * Finds a list of EFPs for GR.
     * @param grId gr which EFPs belong to
     * @return the list of EFPs
     */
    List<EfpDTO> findEfpForGr(Integer grId);


    /**
     * @param efp an EFP to create
     * @return a created EFP (with assigned ID)
     */
    EfpDTO createEfp(@WebParam(name = "efp") EfpDTO efp);

    /**
     * Updates an EFP.
     * @param efp the EFP to be updated
     */
    void updateEfp(@WebParam(name = "efp") EfpDTO efp);

    /**
     * It deletes EFP from DB.
     * @param efpId the id of the efp.
     */
    void deleteEfp(@WebParam(name = "efpId") Integer efpId);

    /**
     * This method finds a list of EFP which a value name is used in.
     * @param grId ID of  GR
     * @param valueName the value to find EFPs for.
     * @return a list of records.
     */
    List<EfpDTO> findByAssignedValueName(
            @WebParam(name = "grId") Integer grId,
            @WebParam(name = "valueName") String valueName);
}
