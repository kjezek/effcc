package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.UserRoleDao;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.service.UserRoleService;

/**
 * Implementation of UserRoleService.
 * @author Bc. Tomas Kohout
 * @since 11.03.2012
 */
@Service(value = "userRoleService")
public class UserRoleServiceImpl extends AbstractService<UserRoleDao, UserRoleEntity, Integer>
implements UserRoleService {

  /** UserRoleDao. */
  @Resource(name = "userRoleDao")
  private UserRoleDao userRoleDao;

  /** {@inheritDoc} */
  @Override
  protected UserRoleDao getDao() {
    return userRoleDao;
  }

  /** {@inheritDoc} */
  public boolean isAssignedToComponentDeveloper(final Integer grId) {
    return userRoleDao.isAssignedToComponentDeveloper(grId);
  }

  /** {@inheritDoc} */
  public UserRoleEntity findByName(final String name) {
    UserRoleEntity roleExample = new UserRoleEntity();
    roleExample.setName(name);
    List<UserRoleEntity> roles = userRoleDao.findByExample(roleExample, new String[] {});
    return roles.isEmpty() ? null : roles.get(0);
  }
}
