/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.GrDao;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.service.GrService;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service(value = "grService")
public class GrServiceImpl extends AbstractService<GrDao, GrEntity, Integer>
        implements GrService {

    /** A reference to GR DAO. */
    @Resource(name = "grDao")
    private GrDao grDao;

    /** {@inheritDoc} */
    @Override
    public GrDao getDao() {
        return grDao;
    }

    /** {@inheritDoc} */
    public void removeGRToUser(final Integer grId, final Integer userId) {
      grDao.removeGRAssignToUser(grId, userId);
    }

    /** {@inheritDoc} */
    public List<GrEntity> getAssignedGRs(final Integer userId) {
      return grDao.getAssignedGRs(userId);
    }

}
