package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.impl.ChoosenObject;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;
import cz.zcu.kiv.efps.registry.server.service.impl.ViewEntity;

/**
 * Service for EntityTree manipulation.
 * @author Bc. Tomas Kohout
 * @since 31.07.2011
 */
public interface EntityTreeService {

  /**
   * Creates EntityTree for View.
   * @param grId gr ID
   * @return {@link EntityTree}
   */
  EntityTree createEntityTree(
      final Integer grId);

  /**
   * Creates EntityTree for View under specific LR.
   * @param lrId String root ID
   * @return {@link EntityTree}
   */
  EntityTree createEntityTreeLR(final Integer lrId);

  /**
   * Creates EntityTree with packing.
   * @param grId GR ID
   * @param packedList packed list
   * @return {@link EntityTree}
   */
  EntityTree createEntityTree(
      final Integer grId,
      final List<Integer> packedList);

  /**
   * Convert List of ViewEntities.
   * @param entities {@link List}
   * @return {@link List}
   */
  List<LrEntity> convert(final List<ViewEntity> entities);

  /**
   * Get choosen object or null from parameters.
   * @param grId gr
   * @param choosenObjectType String
   * @param choosenObjectId String
   * @param entityTree {@link EntityTree}
   * @param packedListString String
   * @return {@link ChoosenObject}
   */
  ChoosenObject getChoosenObject(
      final String grId,
      final String choosenObjectType,
      final String choosenObjectId,
      final EntityTree entityTree,
      final String packedListString);
}
