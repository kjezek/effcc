package cz.zcu.kiv.efps.registry.server.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Entity
@Table(name = "efp",
     uniqueConstraints = {@UniqueConstraint(columnNames = { "name", "gr_id" }) }
)
public class EfpEntity  {

    /** ID. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /** A name of the EFP. */
    @Column(nullable = false)
    private String name;
    /** A measuring unit. */
    private String unit;
    /** A user defined gamma. */
    private String gamma;
    /** A value type of this EFP.  */
    private Class<?> valueType;

    /** A list of properties this EFP is derived from. */
    @ManyToMany
    @JoinTable(name = "derived_efp",
            uniqueConstraints = {@UniqueConstraint(columnNames
                    = {"derived_efp_id", "efp_id" }) },
            joinColumns = {@JoinColumn(name = "derived_efp_id") },
            inverseJoinColumns = {@JoinColumn(name = "efp_id") })
    private List<EfpEntity> derivedFromEfps;

    /** A link to GR. */
    @ManyToOne
    @JoinColumn(name = "gr_id", nullable = false)
    private GrEntity gr;

    /** Value names for LRs. */
    @ManyToMany
    @JoinTable(name = "efp_names",
            inverseJoinColumns = {@JoinColumn(name = "name_id", nullable = true) })
    private List<ValueNameEntity> valueNames;

    /**
     * @return the derivedFromEfps
     */
    public List<EfpEntity> getDerivedFromEfps() {
        return derivedFromEfps;
    }

    /**
     * @param derivedFromEfps the derivedFromEfps to set
     */
    public void setDerivedFromEfps(final List<EfpEntity> derivedFromEfps) {
        this.derivedFromEfps = derivedFromEfps;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** Max length for name. */
    private static final int NAME_CONSTANT_MAX_LENGTH = 14;
    /** Start position for second part in name. */
    private static final int NAME_CONSTANT_SECOND_POSITION = 13;

    /**
     * Get optimized name with formatting.
     * @return String
     */
    public String getOptimizedName() {
      if (name.length() <= NAME_CONSTANT_MAX_LENGTH) {
        return name;
      } else {
        String firstPart = name.substring(0, NAME_CONSTANT_SECOND_POSITION);
        String secondPart = name.substring(NAME_CONSTANT_SECOND_POSITION);
        return firstPart + "<br />" + secondPart;
      }
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @return the gamma
     */
    public String getGamma() {
        return gamma;
    }

    /**
     * @return the valueNames
     */
    public List<ValueNameEntity> getValueNames() {
        return valueNames;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(final String unit) {
        this.unit = unit;
    }

    /**
     * @param gamma the gamma to set
     */
    public void setGamma(final String gamma) {
        this.gamma = gamma;
    }

    /**
     * @param valueNames the valueNames to set
     */
    public void setValueNames(final List<ValueNameEntity> valueNames) {
        this.valueNames = valueNames;
    }

    /**
     * @return the gr
     */
    public GrEntity getGr() {
        return gr;
    }

    /**
     * @param gr the gr to set
     */
    public void setGr(final GrEntity gr) {
        this.gr = gr;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }



    /**
     * @return the valueType
     */
    public Class<?> getValueType() {
        return valueType;
    }

    /**
     * @param valueType the valueType to set
     */
    public void setValueType(final Class<?> valueType) {
        this.valueType = valueType;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpEntity other = (EfpEntity) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /** Class name max length. */
    private static final int CLASS_NAME_CONSTANT_LENGTH = 12;

    /**
     * Get prepared simple name with formatting.
     * @return String
     */
    public String getPreparedValueType() {
      String result = getValueType().getSimpleName();
      if (result.length() > CLASS_NAME_CONSTANT_LENGTH) {
        result = result.substring(0, CLASS_NAME_CONSTANT_LENGTH) + ".";
      }
      return result;
    }
}
