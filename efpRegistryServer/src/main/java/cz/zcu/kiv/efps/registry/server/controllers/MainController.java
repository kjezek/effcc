package cz.zcu.kiv.efps.registry.server.controllers;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.impl.ChoosenObject;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;
import cz.zcu.kiv.efps.registry.server.tools.EfpUtils;

/**
 * Main controller for hadndling a tree in a view.
 * @author Bc. Tomas Kohout
 * @since 12.07.2011
 */
@Controller("mainController")
public class MainController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

  /**
   * Show basic menu - guidepost.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/guidepost", params = { }, method = RequestMethod.GET)
  public ModelAndView guidepost() throws EFPControllerException {
    ModelAndView mav = new ModelAndView("guid");
    return mav;
  }

  /**
   * Show login page.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/login", params = { }, method = RequestMethod.GET)
  public ModelAndView login() throws EFPControllerException {
    ModelAndView mav = new ModelAndView("guid");
    return mav;
  }

  /**
   * Show denied page, after trying some denied action (service method security) .
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/denied",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView denied() throws EFPControllerException {
    ModelAndView mav = new ModelAndView("denied");
    return mav;
  }

  /**
   * Get a tree for center window.
   * @param grId global registry id.
   * @param choosenObjectType (GR, LR, EFP, VAL)
   * @param choosenObjectId ID of choosen object
   * @param packedListString String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/tree", params = {
      ControllerConst.MAIN_PARAM_GR_ID,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST },
      method = RequestMethod.GET)
  public ModelAndView tree(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE) final String choosenObjectType,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString)
  throws EFPControllerException {
    GrEntity grEntity = null;
    int grIdNum = 0;
    if (grId != null && !grId.equals("")) {
      grIdNum = Integer.valueOf(grId);
    } else if (!grService.findAll().isEmpty()) {
      grEntity = grService.findAll().get(0);
      if (grEntity != null) {
        grIdNum = grEntity.getId();
      }
    } else {
      // TODO empty GR
      LOGGER.info("Empty GR list.");
    }

    // Construction of Model and View
    ModelAndView mav = new ModelAndView();
    mav.setViewName("main");

    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);

    EntityTree entityTree = null;
    if (grIdNum != 0) {
      grEntity = grService.findById(grIdNum);
      if (packedList.isEmpty()) {
        entityTree = entityTreeService.createEntityTree(grIdNum);
      } else {
        entityTree = entityTreeService.createEntityTree(
            grIdNum,
            packedList);
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    }

    // gr entity list
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    if (grEntities != null && !grEntities.isEmpty()) {
      if (grEntity != null) {
        mav.addObject(
            ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    }

    //prepare choosen object
    ChoosenObject choosenObject = entityTreeService.getChoosenObject(grId, choosenObjectType,
        choosenObjectId, entityTree, packedListString);
    if (choosenObject != null) {
      entityTree.selectObject(choosenObject.getType(), choosenObject);
      mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT, choosenObject);
    }

    // send packet list
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
        packedListString);

    return mav;
  }

  /**
   * Main method without parameter (first call).
   * @return calls main method with parameter.
   * @throws EFPControllerException e
   */
  @RequestMapping
  public ModelAndView main() throws EFPControllerException {
    return tree(null, null, null, null);
  }

  /**
   * Handling of combo-box with GR choosing.
   * @param grSelect gr_select
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/chooseGr",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT },
      method = RequestMethod.GET)
  public String chooseGr(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT)
      final String grSelect)
    throws EFPControllerException {
    LOGGER.info("method chooseGr: parameter gr_select=" + grSelect);
    GrEntity grEntity = grService.getById(Integer.valueOf(grSelect));
    return "redirect:/client/main/tree?"
      + ControllerConst.MAIN_PARAM_GR_ID
      + "=" + grEntity.getId()
      + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
      + "=" + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
      + "=" + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
      + "=";
  }

  /**
   * Make a change in a tree (pack LR).
   * @param grId global registry id.
   * @param choosenObjectType (GR, LR, EFP, VAL)
   * @param choosenObjectId ID of choosen object
   * @param lrId lr_id (LR for pack)
   * @param packedListString String
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(
  value = "/packLr",
  params = {
      ControllerConst.MAIN_PARAM_GR_ID,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST },
  method = RequestMethod.GET)
  public String packLr(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE) final String choosenObjectType,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString)
  throws EFPControllerException {

    LOGGER.info("method packLr: " + lrId);

    // process packedListString
    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);
    packedList.add(Integer.valueOf(lrId));

    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "=" + grId
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + choosenObjectType + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "=" + choosenObjectId
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=" + EfpUtils.encodePackedList(packedList);
  }

  /**
   * Make a change in a tree (unpack LR).
   * @param grId global registry id.
   * @param choosenObjectType (GR, LR, EFP, VAL)
   * @param choosenObjectId ID of choosen object
   * @param lrId lr_id (LR for pack)
   * @param packedListString String
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(
  value = "/unpackLr",
  params = {
      ControllerConst.MAIN_PARAM_GR_ID,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE,
      ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID,
      ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST },
  method = RequestMethod.GET)
  public String unpackLr(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE) final String choosenObjectType,
      @RequestParam(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID) final String choosenObjectId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString)
  throws EFPControllerException {

    LOGGER.info("method unpackLr: " + lrId);

    // process packedListString
    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);
    packedList.remove(Integer.valueOf(lrId));

    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "=" + grId
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + choosenObjectType + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "=" + choosenObjectId
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=" + EfpUtils.encodePackedList(packedList);
  }

  /**
   * ****************************************************************************************
   * End of new REST implementation.
   */

  //=============================================================================================
  // Getters, Setters
  //=============================================================================================


  //=============================================================================================
  // Dependency injection
  //=============================================================================================

  /** Service for EntityTree handling. */
  private EntityTreeService entityTreeService;
  /**
   * DI setter.
   * @param entityTreeService {@link EntityTreeService}
   */
  @Autowired
  public void setEntityTreeService(final EntityTreeService entityTreeService) {
    this.entityTreeService = entityTreeService;
  }

  /** Service for GR handling. */
  private GrService grService;
  /**
   * DI setter.
   * @param grService {@link GrService}
   */
  @Autowired
  public void setGrService(final GrService grService) {
    this.grService = grService;
  }

  /** Service for LR handling. */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }
}
