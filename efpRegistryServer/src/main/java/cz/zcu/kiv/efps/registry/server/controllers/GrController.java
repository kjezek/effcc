package cz.zcu.kiv.efps.registry.server.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.UserRoleService;
import cz.zcu.kiv.efps.registry.server.service.UserService;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;

/**
 * Controller for Global register handling.
 * @author Bc. Tomas Kohout
 * @since 09.08.2011
 */
@Controller(value = "grController")
public class GrController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(GrController.class);

  /**
   * Create new Global register.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newGr",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView newGr() throws EFPControllerException {

    ModelAndView mav = new ModelAndView("gr_screen_new");

    mav.addObject(ControllerConst.COMMON_CONTROLLER_WIZARD, true);

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, new GrEntity());
    if (grEntities != null && !grEntities.isEmpty()) {
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntities.get(0).getId());
    }

    return mav;
  }

  /**
   * New GR - handling form.
   * @param name String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
  @RequestMapping(
      value = "/newGrForm",
      params = { ControllerConst.GR_CONTROLLER_PARAM_NAME },
      method = RequestMethod.POST)
  public ModelAndView newGrForm(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_NAME) final String name)
  throws EFPControllerException {

    LOGGER.info("GrController.newGrForm: Creating " + name + " GR.");

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    GrEntity grObject = new GrEntity();
    grObject.setName(name);

    try {
      grService.save(grObject);

      // need to assign GR to Admin (? and Domain Expert)
      UserRoleEntity adminRole = userRoleService.findByName(ControllerConst.ADMIN_ROLE);
      for (UserEntity us : adminRole.getUsersWithRole()) {
        us.getAssignedGrList().add(grObject);
        userService.update(us);
      }

      //own user assignment + unique test
      UserEntity ownUser = userService.getLoggedUser();
      if (ownUser != null && !ownUser.getAssignedGrList().contains(grObject)) {
        ownUser.getAssignedGrList().add(grObject);
        userService.update(ownUser);
      }

      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while save new GR.", ex);
    }

    ModelAndView mav = new ModelAndView("gr_screen_new_success");
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grObject);
    return mav;
  }

  /**
   * Delete Global registry.
   * @param grId String
   * @return String
   * @throws EFPControllerException e
   */
  @Deprecated
  @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
  @RequestMapping(value = "/deleteGr", params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID })
  public String deleteGr(@RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID)
      final String grId) throws EFPControllerException {

    LOGGER.info("Deleting global registry: id=" + grId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      grService.deleteById(Integer.valueOf(grId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting GR (" + grId + ").", ex);
    }

    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "="
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "="
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=";
  }

  /**
   * Is GR allowed to delete.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteGrAllowed",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteGrAllowed(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity gr = grService.findById(Integer.valueOf(grId));

    if (gr != null) {
      List<LrEntity> lrList = lrService.findLrForGr(gr.getId());
      if (lrList != null && lrList.size() > 0) {
        return new ModelAndView("delete_denied");
      }
    }

    return new ModelAndView("empty_mav");
  }

  /**
   * Delete GR and return response for AJAX handling.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
  @RequestMapping(
      value = "/deleteGrAjax",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteGrAjax(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    LOGGER.info("Deleting global registry: id=" + grId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {

      GrEntity gr = grService.getById(Integer.valueOf(grId));

      List<UserEntity> users = userService.findAll();
      for (UserEntity user : users) {
        user.getAssignedGrList().remove(gr);
      }

      grService.deleteById(Integer.valueOf(grId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting GR (" + grId + ").", ex);
    }

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    ModelAndView mav = new ModelAndView("gr_screen_new_success");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    return mav;
  }

  /**
   * Only GR handling screen.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/grScreen",
      params = { ControllerConst.MAIN_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView grScreen(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    LOGGER.info("Showing GR Screen ...");

    ModelAndView mav = new ModelAndView("gr_screen");

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    String gr = null;
    GrEntity grEntity = null;
    if (grId == null || (grId != null && grId.equals(""))) {
      if (grEntities != null && !grEntities.isEmpty()) {
        grEntity = grEntities.get(0);
        gr = String.valueOf(grEntity.getId());
      } else {
        throw new EFPControllerException("Empty GR registries", null);
        // TODO nothing to show
      }
    }

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, gr == null ? grId : gr);
    if (grId != null && !grId.equals("")) {
      mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grService
        .findById(Integer.valueOf(grId)));
    } else {
      mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    }

    return mav;
  }

  /**
   * First GR screen - Choose GR for work in or create a new one.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/gr1",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView gr1()
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();

    StringBuilder roles = new StringBuilder();

    // Get only assigned GRs
    int index = 1;
    boolean isComponentDeveloper = true;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        if (!ControllerConst.COMPONENT_DEVELOPER_ROLE.equals(role.getName())) {
          isComponentDeveloper = false;
        }
        index++;
      }
    }

    ModelAndView mav = new ModelAndView("gr_1");

    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES,
        grService.getAssignedGRs(user.getId()));
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.IS_COMPONENT_DEVELOPER, isComponentDeveloper);

    return mav;
  }

  /**
   * Go to GR SCREEN of chosen GR.
   * @param backAction Back view screen
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/gr2",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.BACK_ACTION },
      method = RequestMethod.GET)
  public ModelAndView gr2(
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    boolean isComponentDeveloper = true;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        if (!ControllerConst.COMPONENT_DEVELOPER_ROLE.equals(role.getName())) {
          isComponentDeveloper = false;
        }
        index++;
      }
    }

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    // Prepare tree of substructures (LRs)
    List<LrEntity> lrEntities = lrService.findLrForGr(grEntity.getId());

    EntityTree lrTree = entityTreeService.createEntityTree(grEntity.getId());

    ModelAndView mav = new ModelAndView("gr_2");
    mav.addObject(ControllerConst.IS_COMPONENT_DEVELOPER, isComponentDeveloper);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.GR_CONTROLLER_LR_LIST, lrEntities);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, lrTree);
    mav.addObject(ControllerConst.BACK_ACTION, backAction);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.LR_LIST_SIZE, lrTree.getEntities().size());
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES,
        grService.getAssignedGRs(user.getId()));
    return mav;
  }

  /**
   * Go to GR SCREEN of chosen GR.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/gr2",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView gr2(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {
    return gr2("gr/gr1", grId);
  }

  /**
   * Assign GR to Component Developer.
   * @param grId String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/assignGRToDeveloper",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public void assignGRToDeveloper(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {
    // TODO rework -> rework
    //assignGRToRole(Integer.valueOf(grId), ControllerConst.COMPONENT_DEVELOPER_ROLE);
  }

  /**
   * Assign GR to specified role.
   * @param grId String
   * @param userId Integer
   * @throws EFPControllerException e
   */
  private void assignGRToUser(final Integer grId, final Integer userId)
  throws EFPControllerException {

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    GrEntity gr = grService.findById(grId);
    if (gr != null) {
      UserEntity user = userService.findById(userId);
      if (user != null) {
        if (!isAlreadyAssigned(user, gr)) {
          user.getAssignedGrList().add(gr);
        } else {
          return; // already assigned
        }

        try {
          grService.update(gr);
          transactionManager.commit(txStatus);
          LOGGER.info("*** DB COMMIT ***");
        } catch (Exception ex) {
          if (!txStatus.isCompleted()) {
            transactionManager.rollback(txStatus);
          }
          throw new EFPControllerException("Exception while updating "
              + gr.getName() + " GR.", ex);
        }
      }
    }
  }

  /**
   * Remove assignment GR for this user.
   * @param grId String
   * @param userId String
   * @throws EFPControllerException e
   */
  private void removeGRToUser(final Integer grId, final Integer userId)
  throws EFPControllerException {

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    GrEntity gr = grService.findById(grId);
    if (gr != null) {
      UserEntity user = userService.findById(userId);
      if (user != null) {

        try {
          grService.removeGRToUser(grId, userId);
          //grService.update(gr);
          transactionManager.commit(txStatus);
          LOGGER.info("*** DB COMMIT ***");
        } catch (Exception ex) {
          if (!txStatus.isCompleted()) {
            transactionManager.rollback(txStatus);
          }
          throw new EFPControllerException("Exception while updating "
              + gr.getName() + " GR.", ex);
        }
      }
    }
  }


  /**
   * Test if GR is assigned to this role.
   * @param user {@link UserEntity}
   * @param grEntity {@link GrEntity}
   * @return boolean
   */
  private boolean isAlreadyAssigned(final UserEntity user, final GrEntity grEntity) {
    for (GrEntity gr : user.getAssignedGrList()) {
      if (gr.getId().intValue() == grEntity.getId().intValue()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Choose GR.
   * @param grSelect String
   * @return String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/selectGr",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGr(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT) final String grSelect)
  throws EFPControllerException {
    return "redirect:/client/gr/grScreen?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "=" + grSelect;
  }

  /**
   * Edit GR - Handling input form.
   * @param grObject {@link GrEntity}
   * @param result {@link BindingResult}
   * @param status {@link SessionStatus}
   * @return String redirect
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/editGrForm",
      params = { },
      method = RequestMethod.POST)
  public String editGrForm(
      final GrEntity grObject,
      final BindingResult result,
      final SessionStatus status) throws EFPControllerException {

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      grService.update(grObject);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating "
          + grObject.getName() + " GR.", ex);
    }

    return "redirect:/client/gr/grScreen?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "=" + grObject.getId();
  }

  /**
   * Edit GR and show response for AJAX handling.
   * @param id String
   * @param name String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/editGrFormAjax",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.GR_CONTROLLER_PARAM_NAME },
          method = RequestMethod.POST)
  public ModelAndView editGrFormAjax(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String id,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_NAME) final String name)
  throws EFPControllerException {

    GrEntity grObject = grService.findById(Integer.valueOf(id));
    grObject.setName(name);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      grService.update(grObject);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while updating "
          + grObject.getName() + " GR.", ex);
    }

    ModelAndView mav = new ModelAndView("edit_gr_success");
    return mav;
  }

  /**
   * Run wizard (start with GR).
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/runWizard",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView runWizard(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    LOGGER.info("Showing GR Screen ...");

    ModelAndView mav = new ModelAndView("gr_screen_new");

    mav.addObject(ControllerConst.COMMON_CONTROLLER_WIZARD, true);

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grId);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, new GrEntity());

    return mav;
  }

  /**
   * Run wizard for GR (Add new GR and continue to LR).
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/wizardGr",
      params = { },
      method = RequestMethod.GET)
  public ModelAndView wizardGr() throws EFPControllerException {
    ModelAndView mav = new ModelAndView("wizard_gr");
    return mav;
  }

  /**
   * Show gr_3 view (Assign GR to user).
   * @param backAction String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/gr3",
      params = {
          ControllerConst.BACK_ACTION,
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView gr3(
      @RequestParam(ControllerConst.BACK_ACTION) final String backAction,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        index++;
      }
    }

    List<UserEntity> users = userService.findAll();
    Map<Integer, Map<Integer, Boolean>> gr2Assigned = new HashMap<Integer, Map<Integer, Boolean>>();
    Map<Integer, List<UserRoleEntity>> user2Roles = new HashMap<Integer, List<UserRoleEntity>>();
    for (UserEntity us : users) {

      List<UserRoleEntity> rs = new ArrayList<UserRoleEntity>();
      for (UserRoleEntity role : us.getUsersRoles()) {
        rs.add(role);
      }
      user2Roles.put(us.getId(), rs);

      Map<Integer, Boolean> gr2AssignedInner = new HashMap<Integer, Boolean>();
      for (GrEntity gr : us.getAssignedGrList()) {
        if (gr.getId().intValue() == grEntity.getId().intValue()) {
          gr2AssignedInner.put(gr.getId(), Boolean.TRUE);
        } else {
          gr2AssignedInner.put(gr.getId(), Boolean.FALSE);
        }
      }
      gr2Assigned.put(us.getId(), gr2AssignedInner);
    }

    ModelAndView mav = new ModelAndView("gr_3");
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.USERS, users);
    mav.addObject(ControllerConst.GR_2_ASSIGNED, gr2Assigned);
    mav.addObject(ControllerConst.USER_2_ROLES, user2Roles);
    mav.addObject(ControllerConst.BACK_ACTION, backAction);
    return mav;
  }

  /**
   * Assign GR to specified user.
   * @param grId String
   * @param userId String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/assignGRToUser",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.USER_ID },
      method = RequestMethod.GET)
  public void assignGRToUser(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.USER_ID) final String userId)
  throws EFPControllerException {

    assignGRToUser(Integer.valueOf(grId), Integer.valueOf(userId));

  }

  /**
   * Remove assignment user for this GR.
   * @param grId String
   * @param userId String
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/removeGRToUser",
      params = {
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID,
          ControllerConst.USER_ID },
      method = RequestMethod.GET)
  public void removeGRToUser(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.USER_ID) final String userId)
  throws EFPControllerException {

    removeGRToUser(Integer.valueOf(grId), Integer.valueOf(userId));

  }

  //=============================================================================================
  // Getters, Setters
  //=============================================================================================

  //=============================================================================================
  // Dependency injection
  //=============================================================================================
  /** GrService. */
  private GrService grService;
  /**
   * DI setter.
   * @param grService {@link GrService}
   */
  @Autowired
  public void setGrService(final GrService grService) {
    this.grService = grService;
  }

  /** LrService. */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }

  /** EntityTreeService. */
  private EntityTreeService entityTreeService;
  /**
   * DI setter.
   * @param entityTreeService {@link EntityTreeService}
   */
  @Autowired
  public void setEntityTreeService(final EntityTreeService entityTreeService) {
    this.entityTreeService = entityTreeService;
  }

  /** UserService. */
  private UserService userService;
  /**
   * DI setter.
   * @param userService {@link UserService}
   */
  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }

  /** UserRoleService. */
  private UserRoleService userRoleService;
  /**
   * DI setter.
   * @param userRoleService {@link UserRoleService}
   */
  @Autowired
  public void setUserRoleService(final UserRoleService userRoleService) {
    this.userRoleService = userRoleService;
  }

  /** TxManager for Hibernate. */
  private HibernateTransactionManager transactionManager;
  /**
   * DI setter.
   * @param transactionManager {@link HibernateTransactionManager}
   */
  @Autowired
  public void setTransactionManager(final HibernateTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }
}
