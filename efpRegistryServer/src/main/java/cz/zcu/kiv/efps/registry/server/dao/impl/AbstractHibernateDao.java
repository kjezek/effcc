package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

import cz.zcu.kiv.efps.registry.server.dao.GenericHibernateDao;

/**
 * A base class for hibernate DAOs.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T>
 * @param <ID>
 */
public abstract class AbstractHibernateDao<T, ID extends Serializable>
        implements GenericHibernateDao<T, ID> {

    /** A session factory. */
    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    /** A persistent class this instance serves for. */
    private Class<T> persistentClass;

    /** Cretase an instance. */
    @SuppressWarnings("unchecked")
    public AbstractHibernateDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     *
     * @return a session
     */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     *
     * @return the persistent class
     */
    protected Class<T> getPersistentClass() {
        return persistentClass;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public T getById(final ID id) {
        T result = (T) getSession().get(getPersistentClass(), id);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public T getById(final ID id, final boolean lock) {
        if (lock) {
            return (T) getSession().get(getPersistentClass(), id,
                    LockMode.UPGRADE);
        } else {
            return getById(id);
        }
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public T findById(final ID id) {
        return (T) getSession().load(getPersistentClass(), id);
    }

    /** {@inheritDoc} */
    @Override
    public void save(final T entity) {
        getSession().save(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void update(final T entity) {
        getSession().update(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void saveOrUpdate(final T entity) {
        getSession().saveOrUpdate(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void delete(final T entity) {
        getSession().delete(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteById(final ID id) {
        getSession().delete(findById(id));
    }

    /** {@inheritDoc} */
    @Override
    public List<T> findAll() {
        return findByCriteria();
    }

    /**
     * Use this inside subclasses as a convenience method.
     * @param criterion a list of criterions
     * @return selected records.
     */
    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(final Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }

    /**
     * Creates a hibernate criteria.
     * @return the criteria
     */
    protected Criteria createCriteria() {
        return getSession().createCriteria(getPersistentClass());
    }

    /**
     * Finds entities by criteria.
     * @param criteria the criteria
     * @return the found entities
     */
    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(final Criteria criteria) {
        return criteria.list();
    }

    /**
     * Finds an entity by criteria.
     * @param criteria the criteria
     * @return the found entity or null
     */
    protected T findUniqueByCriteria(final Criteria criteria) {
        List<T> results = findByCriteria(criteria);

        if (results.size() > 1) {
            throw new IllegalArgumentException(
                    "An input criteria results in more than one entity.");
        }

        if (results.isEmpty()) {
            return null;
        }

        return results.get(0);
    }

    /**
     * This method will execute an HQL query and return the number of affected
     * entities.
     * @param query the query
     * @param namedParams params for the query
     * @param params values for params
     * @return number of affected records.
     */
    protected int executeQuery(final String query, final String[] namedParams,
            final Object[] params) {
        Query q = getSession().createQuery(query);

        if (namedParams != null) {
            for (int i = 0; i < namedParams.length; i++) {
                q.setParameter(namedParams[i], params[i]);
            }
        }

        return q.executeUpdate();
    }

    /**
     * This method will execute an HQL query and return the number of affected
     * entities.
     * @param query the query
     * @return number of affected records.
     */
    protected int executeQuery(final String query) {
        return executeQuery(query, null, null);
    }


    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> findByExample(final T exampleInstance, final String[] excludeProperty) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Example example = Example.create(exampleInstance).excludeZeroes()
                .enableLike().ignoreCase();
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    /** {@inheritDoc} */
    @Override
    public void flush() {
        getSession().flush();
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        getSession().clear();
    }


}
