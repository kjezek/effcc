package cz.zcu.kiv.efps.registry.server.dao.impl;

import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.UserDao;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;

/**
 * DaoImpl for UserEntity.
 * @author Bc. Tomas Kohout
 * @since 02.03.2012
 */
@Repository(value = "userDao")
public class UserDaoImpl extends AbstractHibernateDao<UserEntity, Integer>
implements UserDao {

}
