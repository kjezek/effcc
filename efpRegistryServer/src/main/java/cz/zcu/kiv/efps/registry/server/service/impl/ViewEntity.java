package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;

/**
 * ViewEntity is temporary structure for viewing GR and LR entities in a tree (EntityTree).
 * @author Bc. Tomas Kohout
 * @since 12.06.2011
 */
public interface ViewEntity {

  /**
   * Get list of child viewEntities.
   * @param usePack handle with packing/unpacking tree
   * @return list
   */
  List<ViewEntity> getChildEntities(boolean usePack);

  /**
   * Packed view or unpacked view of this Entity.
   * @return boolean
   */
  boolean isPacked();

  /**
   * Set packed value.
   * @param packed boolen
   */
  void setPacked(boolean packed);

  /**
   * Get Entity type.
   * @return {@link EntityTypes}
   */
  EntityTypes getType();

  /**
   * Setter for type.
   * @param type {@link EntityTypes}
   */
  void setType(EntityTypes type);

  /**
   * Get Concrete LrEntity.
   * @return {@link LrEntity}
   */
  LrEntity getLrEntity();

  /**
   * Get concrete EFP.
   * @return {@link LrAssignmentEntity}
   */
  LrAssignmentEntity getAssignment();

  /**
   * Get level in a tree.
   * @return int
   */
  int getLevel();

  /**
   * Set level.
   * @param level int
   */
  void setLevel(int level);

  /**
   * Is selected entity?
   * @return boolean
   */
  boolean isSelected();

  /**
   * Set selected.
   * @param selected boolean
   */
  void setSelected(boolean selected);
}
