/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface GrDao extends GenericHibernateDao<GrEntity, Integer>  {

  /**
   * Remove assign GR for user.
   * @param grId Integer
   * @param userId Integer
   */
  void removeGRAssignToUser(Integer grId, Integer userId);

  /**
   * Get assigned GRs to specified user.
   * @param userId Integer
   * @return {@link List}
   */
  List<GrEntity> getAssignedGRs(Integer userId);
}
