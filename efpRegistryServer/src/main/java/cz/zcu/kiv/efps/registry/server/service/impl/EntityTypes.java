package cz.zcu.kiv.efps.registry.server.service.impl;

/**
 * Enumeration for Entity types.
 * @author Bc. Tomas Kohout
 */
public enum EntityTypes {

  /** Global registry. */
  GR("gr"),
  /** Local registry. */
  LR("lr"),
  /** Assignment (EFP). */
  ASSIGNMENT("assignment"),
  /** Value. */
  VALUE("value");

  /** Name of type. */
  private String name;

  /**
   * Constructor.
   * @param name String
   */
  private EntityTypes(final String name) {
    this.name = name;
  }

  /**
   * Get name of type.
   * @return String
   */
  public String getName() {
    return this.name;
  }

  /**
   * Get EntityType by name of type.
   * @param name String
   * @return {@link EntityTypes}
   */
  public static EntityTypes getByName(final String name) {
    EntityTypes type = null;
    if (GR.name.equals(name)) {
      type = GR;
    } else
    if (LR.name.equals(name)) {
      type = LR;
    }
    if (ASSIGNMENT.name.equals(name)) {
      type = ASSIGNMENT;
    }
    if (VALUE.name.equals(name)) {
      type = VALUE;
    }
    return type;
  }
}
