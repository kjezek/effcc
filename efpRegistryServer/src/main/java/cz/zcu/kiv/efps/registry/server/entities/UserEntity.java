package cz.zcu.kiv.efps.registry.server.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity class for table User.
 * @author Bc. Tomas Kohout
 * @since 26.02.2012
 */
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

  /** Serial UID. */
  private static final long serialVersionUID = 5312295179644533752L;

  /** ID. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  /** LOGIN. */
  @Column(nullable = false, unique = true, length = 20)
  private String login;

  /** PASSWORD. */
  @Column
  private String password;

  /** NAME. */
  @Column
  private String name;

  /** EMAIL. */
  @Column
  private String email;

  /** PHONE. */
  @Column
  private String phone;

  /** A list of roles of user. */
  @ManyToMany
  @JoinTable(
      name = "users_roles",
      uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "role_id" }) },
      joinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) },
      inverseJoinColumns = { @JoinColumn(name = "role_id", nullable = false, updatable = false) })
  private List<UserRoleEntity> usersRoles = new ArrayList<UserRoleEntity>();

  /** List of GRs assigned to this user. */
  @ManyToMany
  @JoinTable(
      name = "assigned_gr",
      uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "gr_id" }) },
      joinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) },
      inverseJoinColumns = { @JoinColumn(name = "gr_id", nullable = false, updatable = false) })
  private List<GrEntity> assignedGrList = new ArrayList<GrEntity>();

  //==========================================================================================
  // getters, setters

  /**
   * Get ID.
   * @return {@link Integer}
   */
  public Integer getId() {
    return id;
  }

  /**
   * Set ID.
   * @param id {@link Integer}
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * get LOGIN.
   * @return String
   */
  public String getLogin() {
    return login;
  }

  /**
   * Set LOGIN.
   * @param login String
   */
  public void setLogin(final String login) {
    this.login = login;
  }

  /**
   * Get PASSWORD.
   * @return String
   */
  public String getPassword() {
    return password;
  }

  /**
   * Set PASSWORD.
   * <br />Should be encoded to SHA1. Don't use this method by hand.
   * @param password String
   */
  public void setPassword(final String password) {
    this.password = password;
  }

  /**
   * Get NAME.
   * @return String
   */
  public String getName() {
    return name;
  }

  /**
   * Set NAME.
   * @param name String
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Get EMAIL.
   * @return String
   */
  public String getEmail() {
    return email;
  }

  /**
   * Set EMAIL.
   * @param email String
   */
  public void setEmail(final String email) {
    this.email = email;
  }

  /**
   * get PHONE.
   * @return String
   */
  public String getPhone() {
    return phone;
  }

  /**
   * set PHONE.
   * @param phone String
   */
  public void setPhone(final String phone) {
    this.phone = phone;
  }

  /**
   * Get List of roles of this user.
   * @return {@link List}
   */
  public List<UserRoleEntity> getUsersRoles() {
    return usersRoles;
  }

  /**
   * Set Roles.
   * @param usersRoles {@link List}
   */
  public void setUsersRoles(final List<UserRoleEntity> usersRoles) {
    this.usersRoles = usersRoles;
  }

  /**
   * Get list of GR entities which are assigned (delegated) to this role.
   * @return {@link List}
   */
  public List<GrEntity> getAssignedGrList() {
    return assignedGrList;
  }

  /**
   * Set List of GR entities which are assigned to this role.
   * @param assignedGrList {@link List}
   */
  public void setAssignedGrList(final List<GrEntity> assignedGrList) {
    this.assignedGrList = assignedGrList;
  }
}
