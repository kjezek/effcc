/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * LR Assignment service.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrAssignmentService extends GenericService<LrAssignmentEntity, Integer> {

    /**
     * Method finds all assignments for an EFP and LR.
     * The resulted assignments are read from LR
     * with the given ID and also from all its parents.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignmentEntity> findLrAssignments(Integer efpId, Integer lrId);

    /**
     * Method finds all assignments LR.
     * The resulted assignments are read from LR
     * with the given ID and also from all its parents.
     * @param lrId local registry ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignmentEntity> findLrAssignments(Integer lrId);

    /**
     * Method finds all assignments LR.
     * The resulted assignments are read from LR
     * with the given ID. Not find in parents.
     * @param lrId String
     * @return {@link List}
     */
    List<LrAssignmentEntity> findLrAssignmentsWithoutParent(Integer lrId);

    /**
     * Method finds assignment for EFP, LR and a value name.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @param name the value name
     * @return an assignment
     */
    LrAssignmentEntity findLrAssignment(Integer efpId, Integer lrId, String name);

    /**
     * Method compute values of lrAssignment under fce.
     * @param fce function
     * @param from lower limit of interval
     * @param to upper limit of interval
     * @param step step between the limits
     * @return a list of computed lrAssignments
     */
    List<EfpValueType> generateLrAssignment(final String fce, final double from,
            final double to, final double step);

}
