package cz.zcu.kiv.efps.registry.server.controllers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cz.zcu.kiv.efps.registry.server.beans.ValueNameBean;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.UserService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.service.impl.ChoosenObject;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTree;
import cz.zcu.kiv.efps.registry.server.service.impl.EntityTypes;
import cz.zcu.kiv.efps.registry.server.tools.EfpUtils;

/**
 * Controller for handling Value views.
 * @author Bc. Tomas Kohout
 * @since 12.07.2011
 */
@Controller("valueController")
public class ValueController {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(ValueController.class);

  /**
   * Show value object in left window.
   * @param grId String
   * @param efpId String
   * @param lrId String
   * @param valueName String
   * @param packedListString String
   * @param ajaxEnabled String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/valueObject", params = {
      ControllerConst.MAIN_PARAM_GR_ID,
      ControllerConst.VALUE_CONTROLLER_PARAM_EFP_ID,
      ControllerConst.VALUE_CONTROLLER_PARAM_LR_ID,
      ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_NAME,
      ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
      ControllerConst.LR_CONTROLLER_AJAX_ENABLED })
  public ModelAndView valueObject(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_EFP_ID) final String efpId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_LR_ID) final String lrId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_NAME) final String valueName,
      @RequestParam(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST)
      final String packedListString,
      @RequestParam(ControllerConst.LR_CONTROLLER_AJAX_ENABLED) final String ajaxEnabled)
  throws EFPControllerException {
    LOGGER.info("ValueController.valueObject(efp_id: "
        + efpId + ", lr_id: " + lrId + ", value_name: " + valueName + ")");

    GrEntity grEntity = null;
    int grIdNum = 0;
    if (grId != null) {
      grIdNum = Integer.valueOf(grId);
    } else {
      grEntity = grService.findAll().get(0);
      if (grEntity != null) {
        grIdNum = grEntity.getId();
      }
    }

    // Construction of Model and View
    ModelAndView mav = new ModelAndView();
    if (ControllerConst.AJAX_ENABLED.equals(ajaxEnabled.toLowerCase())) {
      mav.setViewName("object_properties_ajax");
    } else {
      mav.setViewName("main");
    }

    List<Integer> packedList = EfpUtils.processPackedListString(packedListString);

    EntityTree entityTree = null;
    if (grIdNum != 0) {
      grEntity = grService.findById(grIdNum);
      if (packedList.isEmpty()) {
        entityTree = entityTreeService.createEntityTree(grIdNum);
      } else {
        entityTree = entityTreeService.createEntityTree(
            grIdNum,
            packedList);
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_ENTITY_TREE, entityTree);
    }

    // gr entity list
    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();
    if (grEntities != null && !grEntities.isEmpty()) {
      if (grEntity != null) {
        mav.addObject(
            ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
      }
      mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    }

    //prepare choosen object
    ChoosenObject choosenObject = entityTreeService.getChoosenObject(
        grId, EntityTypes.VALUE.getName(),
        efpId + ControllerConst.ID_DELIMITER + lrId
        + ControllerConst.ID_DELIMITER + valueName, entityTree, packedListString);
    if (choosenObject != null) {
      entityTree.selectObject(choosenObject.getType(), choosenObject);
      mav.addObject(ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT, choosenObject);
    }

    // send packet list
    mav.addObject(ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST,
        packedListString);

    return mav;
  }

  /**
   * Delete value.
   * @param valueId String
   * @return String
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(value = "/deleteValue",
      params = { ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID })
  public String deleteValue(@RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID)
      final String valueId) throws EFPControllerException {

    LOGGER.info("Deleting Value: " + valueId);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      valueNameService.deleteById(Integer.valueOf(valueId));
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while deleting EFP Value (" + valueId + ").", ex);
    }

    // TODO  keep screen on LR
    return "redirect:/client/main/tree?"
    + ControllerConst.MAIN_PARAM_GR_ID
    + "="
    + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_TYPE
    + "=" + "&" + ControllerConst.MAIN_PARAM_CHOOSEN_OBJECT_ID
    + "="
    + "&" + ControllerConst.MAIN_CONTROLLER_PARAM_PACKED_LIST
    + "=";
  }
  */

  /**
   * Go to Value Screen.
   * @param grId String
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/valueScreen",
      params = {
          ControllerConst.MAIN_PARAM_GR_ID,
          ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.GET)
  public ModelAndView valueScreen(
      @RequestParam(ControllerConst.MAIN_PARAM_GR_ID) final String grId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    List<GrEntity> grEntities = grService.findAll();

    ValueNameBean valueNameBean = new ValueNameBean();

    GrEntity grEntity = null;
    if (grId == null || (grId != null && grId.equals(""))) {
      if (grEntities != null && !grEntities.isEmpty()) {
        grEntity = grEntities.get(0);
      }
    } else {
      grEntity = grService.findById(Integer.valueOf(grId));
    }

    List<ValueNameEntity> valuesForGr = valueNameService.findByGr(grEntity.getId());

    ValueNameEntity valueNameEntity = new ValueNameEntity();
    if (valueId != null && !valueId.equals("")) {
      valueNameEntity = valueNameService.findById(Integer.valueOf(valueId));
      valueNameBean.setGrId(String.valueOf(valueNameEntity.getGr().getId()));
    } else {
      if (!valuesForGr.isEmpty()) {
        valueNameEntity = valuesForGr.get(0);
      } else {
        valueNameBean.setGrId(String.valueOf(grEntity.getId()));
      }
    }

    valueNameBean.setValueNameEntity(valueNameEntity);

    ModelAndView mav = new ModelAndView("value_screen");
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_CHOOSEN_GR, grEntity.getId());
    mav.addObject(ControllerConst.VALUE_CONTROLLER_CHOOSEN_VALUE,
        String.valueOf(valueNameEntity.getId()));
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.EFP_CONTROLLER_VALE_OBJECT, valueNameBean);
    mav.addObject(ControllerConst.VALUE_CONTROLLER_VALUES, valuesForGr);

    return mav;
  }

  /**
   * Select GR in Value Screen.
   * @param grId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(
      value = "/selectGrValueScreen",
      params = { ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT },
      method = RequestMethod.GET)
  public String selectGrValueScreen(
      @RequestParam(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_SELECT) final String grId)
  throws EFPControllerException {

    return "redirect:/client/value/valueScreen?gr_id=" + grId + "&value_id=";
  }
  */

  /**
   * Select value for Value Screen.
   * @param valueId String
   * @return String redirect
   * @throws EFPControllerException e
   */
  /*
  @RequestMapping(
      value = "/selectValueScreen",
      params = { ControllerConst.VALUE_CONTROLLER_VALUE_SELECT },
      method = RequestMethod.GET)
  public String selectValueScreen(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_VALUE_SELECT) final String valueId)
  throws EFPControllerException {

    ValueNameEntity valueNameEntity = valueNameService.findById(Integer.valueOf(valueId));

    return "redirect:/client/value/valueScreen?gr_id="
    + valueNameEntity.getGr().getId() + "&value_id=" + valueId;
  }
  */

  /**
   * Show unassigned list of values.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/val1", params = { }, method = RequestMethod.GET)
  public ModelAndView val1() throws EFPControllerException {

    ModelAndView mav = new ModelAndView("val_1");
    mav.addObject(ControllerConst.VALUE_CONTROLLER_NOT_ASSIGNED_VALUE_LIST,
        valueNameService.getNotAssignedValueList());

    return mav;
  }

  /**
   * Show assigned list of values.
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(value = "/val2", params = { }, method = RequestMethod.GET)
  public ModelAndView val2() throws EFPControllerException {

    List<LrAssignmentEntity> assignments = lrAssignmentService.findAll();

    ModelAndView mav = new ModelAndView("val_2");
    mav.addObject(ControllerConst.VALUE_CONTROLLER_ASSIGNMENTS, assignments);
    return mav;
  }

  /**
   * Delete ValueName and return result.
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/deleteValueAjax",
      params = { ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.GET)
  public ModelAndView deleteValueAjax(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    ValueNameEntity value = valueNameService.findById(Integer.valueOf(valueId));

    if (value != null) {
      TransactionStatus txStatus = transactionManager.getTransaction(
          new DefaultTransactionDefinition());

      try {
        valueNameService.deleteById(Integer.valueOf(valueId));
        transactionManager.commit(txStatus);
        LOGGER.info("*** DB COMMIT ***");
      } catch (AccessDeniedException de) {
        transactionManager.rollback(txStatus);
        return new ModelAndView("denied");
      } catch (Exception ex) {
        if (!txStatus.isCompleted()) {
          transactionManager.rollback(txStatus);
        }
        throw new EFPControllerException("Exception while deleting EFP Value ("
            + valueId + ").", ex);
      }
    } else {
      LOGGER.warn("Cannot delete EFP value (" + valueId + "). It is not in DB.");
    }

    ModelAndView mav = new ModelAndView("delete_value_result");
    return mav;
  }

  /**
   * Create new value.
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/val0",
      params = { ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView val0(
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        index++;
      }
    }

    ModelAndView mav = new ModelAndView("val_0");
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.GR_CONTROLLER_GR_OBJECT, grEntity);
    return mav;
  }

  /**
   * Create new value - form handling.
   * @param name String
   * @param grName String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/newValueAjax",
      params = {
          ControllerConst.VALUE_CONTROLLER_PARAM_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView newValueAjax(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_NAME) final String name,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME) final String grName,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    ValueNameEntity value = new ValueNameEntity();
    value.setName(name);
    value.setGr(grEntity);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      valueNameService.save(value);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting EFP Value ("
          + name + ").", ex);
    }

    ModelAndView mav = new ModelAndView("new_value_result");
    return mav;
  }

  /**
   * Show value detail.
   * @param valueId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/val3",
      params = { ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID },
      method = RequestMethod.GET)
  public ModelAndView val3(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId)
  throws EFPControllerException {

    UserEntity user = userService.getLoggedUser();
    StringBuilder roles = new StringBuilder();
    int index = 1;
    boolean isComponentDeveloper = true;
    if (user != null) {
      for (UserRoleEntity role : user.getUsersRoles()) {
        roles.append(role.getDescription());
        if (index < user.getUsersRoles().size()) {
          roles.append(",");
        }
        if (!ControllerConst.COMPONENT_DEVELOPER_ROLE.equals(role.getName())) {
          isComponentDeveloper = false;
        }
        index++;
      }
    }

    ValueNameEntity value = valueNameService.findById(Integer.valueOf(valueId));

    List<GrEntity> grEntities = new LinkedList<GrEntity>();
    grEntities = grService.findAll();

    List<LrAssignmentEntity> assignments = new ArrayList<LrAssignmentEntity>();
    List<LrAssignmentEntity> allAssignments = lrAssignmentService.findAll();
    for (LrAssignmentEntity ass : allAssignments) {
      if (ass.getValueName().getId().intValue() == value.getId().intValue()) {
        assignments.add(ass);
      }
    }

    ModelAndView mav = new ModelAndView("val_3");
    mav.addObject(ControllerConst.IS_COMPONENT_DEVELOPER, isComponentDeveloper);
    mav.addObject(ControllerConst.USER_CONTROLLER_USER_ROLES, roles);
    mav.addObject(ControllerConst.LOGGED_USER, user);
    mav.addObject(ControllerConst.VALUE_CONTROLLER_VALUE_OBJECT, value);
    mav.addObject(ControllerConst.MAIN_CONTROLLER_OBJECT_GR_ENTITIES, grEntities);
    mav.addObject(ControllerConst.VALUE_CONTROLLER_ASSIGNMENTS, assignments);
    return mav;
  }

  /**
   * Edit new value - form handling.
   * @param valueId String
   * @param name String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/editValueAjax",
      params = {
          ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID,
          ControllerConst.VALUE_CONTROLLER_PARAM_NAME,
          //ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME,
          ControllerConst.EFP_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.POST)
  public ModelAndView editValueAjax(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId,
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_NAME) final String name,
      //@RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_NAME) final String grName,
      @RequestParam(ControllerConst.EFP_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    GrEntity grEntity = grService.findById(Integer.valueOf(grId));

    ValueNameEntity value = valueNameService.findById(Integer.valueOf(valueId));
    value.setName(name);
    value.setGr(grEntity);

    TransactionStatus txStatus = transactionManager.getTransaction(
        new DefaultTransactionDefinition());

    try {
      valueNameService.update(value);
      transactionManager.commit(txStatus);
      LOGGER.info("*** DB COMMIT ***");
    } catch (AccessDeniedException de) {
      transactionManager.rollback(txStatus);
      return new ModelAndView("denied");
    } catch (Exception ex) {
      if (!txStatus.isCompleted()) {
        transactionManager.rollback(txStatus);
      }
      throw new EFPControllerException("Exception while inserting EFP Value ("
          + name + ").", ex);
    }

    ModelAndView mav = new ModelAndView("edit_value_result");
    return mav;
  }

  /**
   * Assign value to LR and EFP from specific GR.
   * @param valueId String
   * @param grId String
   * @return {@link ModelAndView}
   * @throws EFPControllerException e
   */
  @RequestMapping(
      value = "/ass2",
      params = {
          ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID,
          ControllerConst.GR_CONTROLLER_PARAM_GR_ID },
      method = RequestMethod.GET)
  public ModelAndView ass2(
      @RequestParam(ControllerConst.VALUE_CONTROLLER_PARAM_VALUE_ID) final String valueId,
      @RequestParam(ControllerConst.GR_CONTROLLER_PARAM_GR_ID) final String grId)
  throws EFPControllerException {

    List<LrEntity> listLR = lrService.findLrForGr(Integer.valueOf(grId));
    List<EfpEntity> listEFP = efpService.findEfpForGr(Integer.valueOf(grId));

    ValueNameEntity value = valueNameService.findById(Integer.valueOf(valueId));

    ModelAndView mav = new ModelAndView("ass_2");
      mav.addObject(ControllerConst.EFP_CONTROLLER_EFP_LIST, listEFP);
      mav.addObject(ControllerConst.EFP_CONTROLLER_LR_LIST, listLR);
      mav.addObject(ControllerConst.EFP_CONTROLLER_VALUE_OBJECT, value);
    return mav;
  }

  //=============================================================================================
  // Getters, Setters
  //=============================================================================================

  //=============================================================================================
  // Dependency injection
  //=============================================================================================
  /** GrService. */
  private GrService grService;
  /**
   * DI setter.
   * @param grService {@link GrService}
   */
  @Autowired
  public void setGrService(final GrService grService) {
    this.grService = grService;
  }

  /** EntityTreeService. */
  private EntityTreeService entityTreeService;
  /**
   * DI setter.
   * @param entityTreeService {@link EntityTreeService}
   */
  @Autowired
  public void setEntityTreeService(final EntityTreeService entityTreeService) {
    this.entityTreeService = entityTreeService;
  }

  /** Service for LR handling. */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }

  /** ValueNameService. */
  private ValueNameService valueNameService;
  /**
   * DI setter.
   * @param valueNameService {@link ValueNameService}
   */
  @Autowired
  public void setValueNameService(final ValueNameService valueNameService) {
    this.valueNameService = valueNameService;
  }

  /** LrAssignmentService. */
  private LrAssignmentService lrAssignmentService;
  /**
   * DI setter.
   * @param lrAssignmentService {@link LrAssignmentService}
   */
  @Autowired
  public void setLrAssignmentService(final LrAssignmentService lrAssignmentService) {
    this.lrAssignmentService = lrAssignmentService;
  }

  /** TxManager for Hibernate. */
  private HibernateTransactionManager transactionManager;
  /**
   * DI setter.
   * @param transactionManager {@link HibernateTransactionManager}
   */
  @Autowired
  public void setTransactionManager(final HibernateTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  /** EfpService. */
  private EfpService efpService;
  /**
   * DI setter.
   * @param efpService {@link EfpService}
   */
  @Autowired
  public void setEfpService(final EfpService efpService) {
    this.efpService = efpService;
  }

  /** UserService. */
  private UserService userService;
  /**
   * DI setter.
   * @param userService {@link UserService}
   */
  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }
}
