/**
 *
 */
package cz.zcu.kiv.efps.registry.server.tools;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.GrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrDTO;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * An utility class converting database entities.
 * to EFP types an vice versa
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface EntityConvertor {

    /**
     * Converts an database entity to the EFP type.
     * @param entity the database entity
     * @return the DTO type.
     */
    EfpDTO convert(final EfpEntity entity);

    /**
     * Converts DTO entity to DB entity.
     * @param dto the DTO
     * @param names attached value names
     * @return  the db entity
     */
    EfpEntity convert(EfpDTO dto, List<ValueNameEntity> names);

    /**
     * Converts an database entity to the EFP type.
     * @param entities the database entity
     * @return the DTO type.
     */
    List<EfpDTO> convertEfps(final List<EfpEntity> entities);

    /**
     * Converts an database entity to the EFP type.
     * @param dtos the DTO
     * @return the database type.
     */
    List<EfpEntity> convertEfpsEntity(final List<EfpDTO> dtos);

    /**
     * Converts DB entity to DTO.
     * @param entity the db entity
     * @return the DTO
     */
    LrAssignmentDTO convert(final LrAssignmentEntity entity);

    /**
     * Converts DB entity to DTO.
     * @param entity the db entity
     * @return the DTO
     */
    List<LrAssignmentDTO> convertLrAssignments(final List<LrAssignmentEntity> entity);

    /**
     * Converts DTO entity to DB entity.
     * @param dto the DTO
     * @param valueName TODO
     * @return  the db entity
     */
    LrAssignmentEntity convert(final LrAssignmentDTO dto, ValueNameEntity valueName);

    /**
     * Converts DB entity to DTO.
     * @param entities the db entity
     * @return the DTO
     */
    List<String> convertValueNames(List<ValueNameEntity> entities);


    /**
     * Converts DB entity to DTO.
     * @param entity the db entity
     * @return the DTO
     */
    GrDTO convert(final GrEntity entity);

    /**
     * Converts an database entity to the EFP type.
     * @param entities the database entity
     * @return the EFP type.
     */
    List<GrDTO> convertGrs(final List<GrEntity> entities);

    /**
     * Converts DTO entity to DB entity.
     * @param dto the DTO
     * @return  the db entity
     */
    GrEntity convert(final GrDTO dto);

    /**
     * Converts DB entity to DTO.
     * @param entity the db entity
     * @return the DTO
     */
    LrDTO convert(LrEntity entity);

    /**
     * Converts an database entity to the EFP type.
     * @param entities the database entity
     * @return the EFP type.
     */
    List<LrDTO> convertLrs(List<LrEntity> entities);

    /**
     * Converts DTO entity to DB entity.
     * @param dto the DTO
     * @return  the db entity
     */
    LrEntity convert(LrDTO dto);

    /**
     * Converts values to the LrAssignmentDTO.
     * @param values the values
     * @param efp an efp
     * @param lr a local register
     * @return the EFP type.
     */
    List<LrAssignmentDTO> convertValues(List<EfpValueType> values, EfpDTO efp, LrDTO lr);

}
