/**
 *
 */
package cz.zcu.kiv.efps.registry.server.validation;

import java.util.List;
import java.util.Locale;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

/**
 * This calls a validation before intercepted
 * methods are invoked.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class ValidationInterceptor  {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Messages. */
    @Autowired
    private ReloadableResourceBundleMessageSource messages;

    /** a list of validators. */
    private List<Validator> validators;

    /**
     * This method is called to validate parameters.
     * on all service methods
     * @param jp the join point
     */
    public void validateMethod(final JoinPoint jp) {

        logger.debug("Validating join-point: " + jp);

        Object[] args = jp.getArgs();

        // go through arguments and validate
        for (Object arg : args) {
            DataBinder binder = new DataBinder(arg);
            for (Validator validator : validators) {
                if (validator.supports(arg.getClass())) {

                    logger.trace("Validating " + arg + " with " + validator);

                    binder.setValidator(validator);
                    binder.validate();

                }
            }
            if (binder.getBindingResult().hasErrors()) {
                String errorMessage = createErrorMessage(binder.getBindingResult());
                throw new IllegalArgumentException(errorMessage);
            }
        }
    }


    /**
     * It creates error messages from the input errors.
     * @param bindingResult the errors
     * @return the error message
     */
    private String createErrorMessage(final BindingResult bindingResult) {

        StringBuffer result = new StringBuffer();

        for (Object error : bindingResult.getAllErrors()) {
            FieldError objectError = (FieldError) error;

            result.append("Invalid value: '").append(
                    bindingResult.getRawFieldValue(objectError.getField()))
                    .append("'");

            String errorCode = objectError.getCode();
            // TODO [kjezek, B] it would be better to have a client locale here
            String errorMessage = messages.getMessage(
                    errorCode, null, objectError.getDefaultMessage(), Locale.getDefault());

            result.append(", Error: ");
            result.append(errorMessage);
            result.append(", ");
        }

        return result.toString();
    }


    /**
     * @param validators the validators to set
     */
    public void setValidators(final List<Validator> validators) {
        this.validators = validators;
    }


}
