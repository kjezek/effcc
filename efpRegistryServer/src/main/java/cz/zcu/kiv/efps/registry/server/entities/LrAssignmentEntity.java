/**
 *
 */
package cz.zcu.kiv.efps.registry.server.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Entity
@Table(name = "lr_assignment",
        uniqueConstraints = {
        @UniqueConstraint(columnNames = { "value_name_id", "lr_id", "efp_id" }) })
public class LrAssignmentEntity {

    /** ID. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /** A value stored for this type. */
    @Column(nullable = false)
    private String value;

    /** A value name for this assignment. */
    @ManyToOne
    @JoinColumn(name = "value_name_id", nullable = false)
    private ValueNameEntity valueName;

    /** A link to LR this value is assigned to. */
    @ManyToOne
    @JoinColumn(name = "lr_id", nullable = false)
    private LrEntity lr;

    /** A link to EFP this value is assigned to. */
    @ManyToOne
    @JoinColumn(name = "efp_id", nullable = false)
    private EfpEntity efp;

    /** A logical formula for derived properties. */
    @Column(name = "logical_formula")
    private String logicalFormula;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @return the valueName
     */
    public ValueNameEntity getValueName() {
        return valueName;
    }
    /**
     * @return the lr
     */
    public LrEntity getLr() {
        return lr;
    }
    /**
     * @return the efp
     */
    public EfpEntity getEfp() {
        return efp;
    }
    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }
    /**
     * @param value the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }
    /**
     * @param valueName the valueName to set
     */
    public void setValueName(final ValueNameEntity valueName) {
        this.valueName = valueName;
    }
    /**
     * @param lr the lr to set
     */
    public void setLr(final LrEntity lr) {
        this.lr = lr;
    }
    /**
     * @param efp the efp to set
     */
    public void setEfp(final EfpEntity efp) {
        this.efp = efp;
    }
    /**
     * @return the logicalFormula
     */
    public String getLogicalFormula() {
        return logicalFormula;
    }
    /**
     * @param logicalFormula the logicalFormula to set
     */
    public void setLogicalFormula(final String logicalFormula) {
        this.logicalFormula = logicalFormula;
    }


    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((lr == null) ? 0 : lr.hashCode());
        result = prime * result
                + ((valueName == null) ? 0 : valueName.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LrAssignmentEntity other = (LrAssignmentEntity) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (lr == null) {
            if (other.lr != null) {
                return false;
            }
        } else if (!lr.equals(other.lr)) {
            return false;
        }
        if (valueName == null) {
            if (other.valueName != null) {
                return false;
            }
        } else if (!valueName.equals(other.valueName)) {
            return false;
        }
        return true;
    }

    public String getPreparedValue() {
      return (EfpSerialiser.deserialize(EfpValueType.class, value)).getLabel();
    }

}
