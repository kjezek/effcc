/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface ValueNameService extends
        GenericService<ValueNameEntity, Integer> {

    /**
     * Finds  a record.
     * @param name the name
     * @param grId the GR
     * @return the record
     */
    ValueNameEntity findByName(String name, Integer grId);

    /**
     * Finds  a record.
     * @param name the name
     * @param grId the GR
     * @return the record
     */
    List<ValueNameEntity> findByNames(List<String> name, Integer grId);


    /**
     * Finds  all records.
     * @param grId the GR
     * @return the record
     */
    List<ValueNameEntity> findByGr(Integer grId);


    /**
     * it gets a value name by efp and the name of the value name.
     * @param name the name
     * @param efpId the ID of the EFP.
     * @return the record or exception it the record does not exist
     */
    ValueNameEntity getByEfp(String name, Integer efpId);

    /**
     * Creates a record detached from the hibernate session.
     * @param name the name
     * @param grId the GR
     * @return the record
     */
    ValueNameEntity create(String name, Integer grId);

    /**
     * Finds or creates a record.
     * @param name the name
     * @param grId the GR
     * @return the record
     */
    ValueNameEntity findOrCreate(String name, Integer grId);

    /**
     * Get all not assigned values.
     * @return {@link List}
     */
    List<ValueNameEntity> getNotAssignedValueList();
}
