package cz.zcu.kiv.efps.registry.server.dao;

import cz.zcu.kiv.efps.registry.server.entities.UserEntity;

/**
 * DAO for UserEntity handling.
 * @author Bc. Tomas Kohout
 * @since 02.03.2012
 */
public interface UserDao extends GenericHibernateDao<UserEntity, Integer> {

}
