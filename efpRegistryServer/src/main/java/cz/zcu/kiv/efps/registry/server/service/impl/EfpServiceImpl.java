/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.EfpDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service(value = "efpService")
public class EfpServiceImpl
    extends AbstractService<EfpDao, EfpEntity, Integer>
    implements EfpService {

    /** EFP DAO dependency. */
    @Resource(name = "efpDao")
    private EfpDao efpDao;

    /** {@inheritDoc} */
    @Override
    public EfpDao getDao() {
        return efpDao;
    }

    /** {@inheritDoc} */
    @Override
    public EfpEntity findByName(final String name, final Integer grID) {
        return efpDao.findByName(name, grID);
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpEntity> findEfpForGr(final Integer grId) {
        return efpDao.findEfpForGr(grId);
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpEntity> findByValuName(final Integer grId, final String valueName) {
        return efpDao.findByValuName(grId, valueName);
    }

    /** {@inheritDoc} */
    public List<EfpEntity> getNotAssignedEfpList() {
      return efpDao.getNotAssignedEfpList();
    }

}
