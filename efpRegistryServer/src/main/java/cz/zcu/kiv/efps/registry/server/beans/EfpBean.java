package cz.zcu.kiv.efps.registry.server.beans;

import java.io.Serializable;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;

/**
 * Assignment bean object for handling forms.
 * @author Bc. Tomas Kohout
 * @since 25.10.2011
 */
public class EfpBean implements Serializable {

  /** serial UID. */
  private static final long serialVersionUID = 3735143062901457358L;

  /** {@link EfpEntity} object. */
  private EfpEntity efpEntity;
  /** ID of parent GR. */
  private int grId;

  /**
   * Getter.
   * @return {@link EfpEntity}
   */
  public EfpEntity getEfpEntity() {
    return efpEntity;
  }

  /**
   * Setter.
   * @param efpEntity {@link EfpEntity}
   */
  public void setEfpEntity(final EfpEntity efpEntity) {
    this.efpEntity = efpEntity;
  }

  /**
   * Getter.
   * @return {@link Integer}
   */
  public int getGrId() {
    return grId;
  }

  /**
   * Setter.
   * @param grId {@link Integer}
   */
  public void setGrId(final int grId) {
    this.grId = grId;
  }
}
