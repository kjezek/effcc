/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.LrDao;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.LrService;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service(value = "lrService")
public class LrServiceImpl extends AbstractService<LrDao, LrEntity, Integer>
implements LrService {

    /** A reference to LR DAO. */
    @Resource(name = "lrDao")
    private LrDao lrDao;

    /** {@inheritDoc} */
    @Override
    public LrDao getDao() {
        return lrDao;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrEntity> findLrForGr(final Integer grId) {
        return lrDao.findLrForGr(grId);
    }

}
