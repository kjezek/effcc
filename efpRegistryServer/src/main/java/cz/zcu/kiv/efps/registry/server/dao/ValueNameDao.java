/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface ValueNameDao extends GenericHibernateDao<ValueNameEntity, Integer>  {


    /**
     * Finds  a name.
     * @param name the name
     * @param grID GR ID
     * @return a record
     */
    ValueNameEntity findByNameAndGr(String name, Integer grID);

    /**
     * Finds  all names.
     * @param grID GR ID
     * @return a record
     */
    List<ValueNameEntity> findByGr(Integer grID);

    /**
     * Gel all not assigned values.
     * @return {@link List}
     */
    List<ValueNameEntity> getNotAssignedValueList();
}
