/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.GrDao;
import cz.zcu.kiv.efps.registry.server.dao.ValueNameDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service(value = "valueNameService")
public class ValueNameServiceImpl extends
        AbstractService<ValueNameDao, ValueNameEntity, Integer> implements
        ValueNameService {

    /** DAO. */
    @Resource(name = "valueNameDao")
    private ValueNameDao valueNameDao;

    /** DAO. */
    @Resource(name = "grDao")
    private GrDao grDao;

    /** Service. */
    @Resource(name = "efpService")
    private EfpService efpService;

    /** {@inheritDoc} */
    @Override
    protected ValueNameDao getDao() {
        return valueNameDao;
    }

    /** {@inheritDoc} */
    @Override
    public ValueNameEntity findByName(final String name, final Integer grId) {
        return valueNameDao.findByNameAndGr(name, grId);
    }

    /** {@inheritDoc} */
    @Override
    public ValueNameEntity getByEfp(final String name, final Integer efpId) {
        EfpEntity efpEntity = efpService.getById(efpId);

        for (ValueNameEntity valueName : efpEntity.getValueNames()) {
            if (name.equals(valueName.getName())) {
                return valueName;
            }
        }

        throw new IllegalArgumentException("A value name '" + name
                + "' is not valid for the " + efpEntity.getName() + " extra-functional property. "
                + "Available names are: " + efpEntity.getValueNames());
    }


    /** {@inheritDoc} */
    @Override
    public ValueNameEntity create(final String name, final Integer grId) {
        GrEntity gr = grDao.getById(grId);
        ValueNameEntity entity = new ValueNameEntity();
        entity.setName(name);
        entity.setGr(gr);

        return entity;
    }


    /** {@inheritDoc} */
    @Override
    public ValueNameEntity findOrCreate(final String name, final Integer grId) {
        ValueNameEntity entity = findByName(name, grId);

        if (entity == null) {
            entity = create(name, grId);
            save(entity);
        }
        return entity;
    }

    /** {@inheritDoc} */
    public List<ValueNameEntity> findByGr(final Integer grId) {
        return valueNameDao.findByGr(grId);
    }

    /** {@inheritDoc} */
    @Override
    public List<ValueNameEntity> findByNames(final List<String> names, final Integer grId) {
        List<ValueNameEntity> entities = new LinkedList<ValueNameEntity>();

        for (String name : names) {

            ValueNameEntity nameEntity = findByName(name, grId);

            if (nameEntity == null) {
                throw new IllegalArgumentException("The value name '" + name + "'"
                        + " for GR with ID: " + grId + " does not exist.");
            }

            entities.add(nameEntity);
        }

        return entities;
    }

    /** {@inheritDoc} */
    public List<ValueNameEntity> getNotAssignedValueList() {
      return valueNameDao.getNotAssignedValueList();
    }

}
