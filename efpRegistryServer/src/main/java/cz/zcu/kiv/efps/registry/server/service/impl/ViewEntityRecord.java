package cz.zcu.kiv.efps.registry.server.service.impl;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.LrService;

/**
 * Concrete ViewEntity in a tree.
 * @author Bc. Tomas Kohout
 * @since 12.06.2011
 */
public class ViewEntityRecord implements ViewEntity, Serializable {

  /** Generated UID. */
  private static final long serialVersionUID = -3343488611386234277L;

  /** LrService handling. */
  private LrService lrService;
  /** LREntity if it is LR. */
  private LrEntity lrEntity = null;
  /** LRAssignment, if it is assignment. */
  private LrAssignmentEntity assignment = null;
  /** Flag for packed option. */
  private boolean packed;
  /** Type of entity. */
  private EntityTypes type;
  /** Flag for selected option. */
  private boolean selected;
  /** Level in the tree. */
  private int level = 0;

  /**
   * Public constructor.
   * @param lrEntity {@link LrEntity}
   * @param assignment {@link LrAssignmentEntity}
   * @param lrService {@link LrService}
   */
  public ViewEntityRecord(
      final LrEntity lrEntity,
      final LrAssignmentEntity assignment,
      final LrService lrService) {
    this.lrService = lrService;
    this.lrEntity = lrEntity;
    this.assignment = assignment;
    if (lrEntity != null) {
      type = EntityTypes.LR;
    } else {
      type = EntityTypes.ASSIGNMENT;
    }
  }

  /**
   * Get child entities of Entity.
   * @param usePack boolean (logic uses packing or not)
   * @return {@link List} of {@link ViewEntity}
   */
  @Override
  public List<ViewEntity> getChildEntities(final boolean usePack) {
    if (usePack && (packed || lrEntity == null)) {
      return new LinkedList<ViewEntity>();
    } else {
      List<ViewEntity> childs = new LinkedList<ViewEntity>();
      for (LrEntity lr : lrService.findAll()) {
        if (lr.getParent() != null && lr.getParent().getId().equals(lrEntity.getId())) {
          ViewEntity viewEntity = new ViewEntityRecord(lr, null, lrService);
          viewEntity.setPacked(false);
          childs.add(viewEntity);
        }
      }
      return childs;
    }
  }

  @Override
  public boolean isPacked() {
    return packed;
  }

  @Override
  public void setPacked(final boolean packed) {
    this.packed = packed;
  }

  @Override
  public EntityTypes getType() {
    return type;
  }

  @Override
  public LrEntity getLrEntity() {
    return lrEntity;
  }

  @Override
  public LrAssignmentEntity getAssignment() {
    return assignment;
  }

  @Override
  public void setType(final EntityTypes type) {
    this.type = type;
  }

  @Override
  public int getLevel() {
    return level;
  }

  @Override
  public void setLevel(final int level) {
    this.level = level;
  }

  /**
   * Get selected.
   * @return boolean
   */
  public boolean isSelected() {
    return selected;
  }

  /**
   * Set selected.
   * @param selected boolean
   */
  public void setSelected(final boolean selected) {
    this.selected = selected;
  }
}
