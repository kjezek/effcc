/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.impl;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.tools.EntityConvertor;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpWs;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.EfpDTO;


/**
 * A web service providing operations
 * on EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(
        endpointInterface = "cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpWs",
        targetNamespace = EfpWs.NAMESPACE)
@Component(value = "efpWs")
public class EfpWSImpl implements EfpWs {

    /** Business methods. */
    @Resource(name = "efpService")
    private EfpService efpService;

    /** Business methods. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** An entity values converter. */
    @Resource(name = "entityConvertor")
    private EntityConvertor entityConvertor;

    /** {@inheritDoc} */
    @Override
    public EfpDTO findEfpByName(final String name, final Integer grID) {
        EfpEntity entity = efpService.findByName(name, grID);

        EfpDTO result = null;
        if (entity != null) {
            result = entityConvertor.convert(entity);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpDTO> findEfpForGr(final Integer grId) {
        List<EfpEntity> entities = efpService.findEfpForGr(grId);
        return entityConvertor.convertEfps(entities);
    }

    /** {@inheritDoc} */
    @Override
    public EfpDTO createEfp(final EfpDTO efp) {
        List<ValueNameEntity> names = new LinkedList<ValueNameEntity>();

        // find or create names
        if (efp.getValueNames() != null) {
            names = valueNameService.findByNames(efp.getValueNames(), efp.getGr().getId());
        }

        EfpEntity entity = entityConvertor.convert(efp, names);
        efpService.save(entity);
        return entityConvertor.convert(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void updateEfp(final EfpDTO efp) {
        List<ValueNameEntity> names = new LinkedList<ValueNameEntity>();

        // find or create names
        if (efp.getValueNames() != null) {
            names = valueNameService.findByNames(efp.getValueNames(), efp.getGr().getId());
        }

        EfpEntity entity = entityConvertor.convert(efp, names);
        efpService.update(entity);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteEfp(final Integer efpId) {

        EfpEntity entity = efpService.getById(efpId);
        efpService.delete(entity);
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpDTO> findByAssignedValueName(final Integer grId, final String valueName) {
        List<EfpEntity> entities = efpService.findByValuName(grId, valueName);

        List<EfpDTO> dtos = new LinkedList<EfpDTO>();
        for (EfpEntity entity : entities) {
            dtos.add(entityConvertor.convert(entity));
        }

        return dtos;
    }


}
