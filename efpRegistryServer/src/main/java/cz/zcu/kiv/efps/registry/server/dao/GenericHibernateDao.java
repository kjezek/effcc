/**
 *
 */
package cz.zcu.kiv.efps.registry.server.dao;

import java.io.Serializable;

/**
 * A generic hibernate DAO interface.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T> type of an entity which this interface works on
 * @param <ID> type of an ID of the entity
 */
public interface GenericHibernateDao<T, ID extends Serializable>
    extends GenericDao<T, ID> {

    /**
     * Flush the current session.
     */
    void flush();

    /**
     * Clear the current session.
     */
    void clear();
}
