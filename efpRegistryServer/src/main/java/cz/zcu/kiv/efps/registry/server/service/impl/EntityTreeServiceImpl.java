package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.controllers.ControllerConst;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.EntityTreeService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;

/**
 * Implementation of EntityTreeService.
 * @author Bc. Tomas Kohout
 * @since 31.07.2011
 */
@Service(value = "entityTreeService")
public class EntityTreeServiceImpl implements EntityTreeService {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EntityTreeServiceImpl.class);

  @Override
  public EntityTree createEntityTree(final Integer grId) {
    EntityTree tree = new EntityTree();

    // child LR entities
    List<LrEntity> lrEntities = lrService.findLrForGr(grId);
    if (lrEntities != null && !lrEntities.isEmpty()) {
      int level = 0;
      for (LrEntity lr : lrEntities) {
        if (lr.getParent() == null) {
          ViewEntity viewEntity = new ViewEntityRecord(lr, null, lrService);
          viewEntity.setPacked(false);
          viewEntity.setType(EntityTypes.LR);
          viewEntity.setLevel(level);
          viewEntity.setSelected(false);
          tree.getEntities().add(viewEntity);
          tree.getLrCache().put(lr.getId(), viewEntity);

          // sub childs
          addChildEntities(viewEntity, tree, level, new LinkedList<Integer>());

          tree.getChildAssignments().put(lr.getId(), lr.getAssignments());

          // EFPs
          for (LrAssignmentEntity assignment : lr.getAssignments()) {
            ViewEntity ve = new ViewEntityRecord(null, assignment, lrService);
            ve.setPacked(false);
            ve.setType(EntityTypes.ASSIGNMENT);
            viewEntity.setLevel(0);
            viewEntity.setSelected(false);
            tree.getEntities().add(ve);

            String key = String.valueOf(lr.getId())
              + "_" + String.valueOf(assignment.getEfp().getId());
            if (tree.getEfpCache().containsKey(key)) {
              tree.getEfpCache().get(key).add(ve);
            } else {
              List<ViewEntity> list = new LinkedList<ViewEntity>();
              list.add(ve);
              tree.getEfpCache().put(key, list);
            }
          }
        }
      }
    }

    return tree;
  }

  public EntityTree createEntityTreeLR(final Integer lrId) {
    EntityTree tree = new EntityTree();

    LrEntity root = lrService.findById(lrId);

    int level = 0;

    if (root != null) {
      for (LrEntity lr : lrService.findLrForGr(root.getGr().getId())) {
        if (lr.getParent() != null && lr.getParent().getId() == lrId) {

          ViewEntity viewEntity = new ViewEntityRecord(lr, null, lrService);
          viewEntity.setPacked(false);
          viewEntity.setType(EntityTypes.LR);
          viewEntity.setLevel(level);
          viewEntity.setSelected(false);
          tree.getEntities().add(viewEntity);
          tree.getLrCache().put(lr.getId(), viewEntity);

          addChildEntities(viewEntity, tree, level, new LinkedList<Integer>());
        }
      }
    }

    return tree;
  }

  /**
   * Add child entities to tree.
   * @param viewEntity {@link ViewEntity}
   * @param tree {@link EntityTree}
   * @param level int
   * @param packedList {@link List}
   */
  private void addChildEntities(
      final ViewEntity viewEntity,
      final EntityTree tree,
      final int level,
      final List<Integer> packedList) {
    int localLevel = level;
    int levelForEFP = level;
    List<ViewEntity> childs = viewEntity.getChildEntities(true);
    if (childs != null && !childs.isEmpty()) {
      localLevel++;
      for (ViewEntity child : childs) {
        ViewEntity ve = new ViewEntityRecord(child.getLrEntity(), null, lrService);
        if (packedList.contains(child.getLrEntity().getId())) {
          ve.setPacked(true);
        } else {
          ve.setPacked(false);
        }
        viewEntity.setSelected(false);
        ve.setLevel(localLevel);
        ve.setType(EntityTypes.LR);
        tree.getEntities().add(ve);
        tree.getLrCache().put(child.getLrEntity().getId(), ve);

        // sub childs
        if (!ve.isPacked()) {
          addChildEntities(child, tree, localLevel, packedList);
        }
      }
    }

    List<LrAssignmentEntity> assignments = viewEntity.getLrEntity().getAssignments();
    Collections.sort(assignments, new Comparator<LrAssignmentEntity>() {

      @Override
      public int compare(final LrAssignmentEntity o1, final LrAssignmentEntity o2) {
        if (!o1.getEfp().getId().equals(o2.getEfp().getId())) {
          return o1.getEfp().getId().compareTo(o2.getEfp().getId());
        }
        return 0;
      }
    });

    tree.getChildAssignments().put(viewEntity.getLrEntity().getId(), assignments);

    // EFPs
    if (!viewEntity.isPacked() && levelForEFP != 0) {
      for (LrAssignmentEntity assignment : viewEntity.getLrEntity()
          .getAssignments()) {
        ViewEntity v = new ViewEntityRecord(null, assignment, lrService);
        v.setPacked(false);
        v.setType(EntityTypes.ASSIGNMENT);
        v.setLevel(levelForEFP);
        viewEntity.setSelected(false);
        tree.getEntities().add(v);

        String key = String.valueOf(viewEntity.getLrEntity().getId())
          + "_" + String.valueOf(assignment.getEfp().getId());
        if (tree.getEfpCache().containsKey(key)) {
          tree.getEfpCache().get(key).add(v);
        } else {
          List<ViewEntity> list = new LinkedList<ViewEntity>();
          list.add(v);
          tree.getEfpCache().put(key, list);
        }
      }
    }
  }

  @Override
  public EntityTree createEntityTree(
      final Integer grId,
      final List<Integer> packedList) {

    EntityTree tree = new EntityTree();

    // child LR entities
    List<LrEntity> lrEntities = lrService.findLrForGr(grId);
    if (lrEntities != null && !lrEntities.isEmpty()) {
      int level = 0;
      for (LrEntity lr : lrEntities) {
        if (lr.getParent() == null) {
          ViewEntity viewEntity = new ViewEntityRecord(lr, null, lrService);
          if (packedList.contains(lr.getId())) {
            viewEntity.setPacked(true);
          } else {
            viewEntity.setPacked(false);
          }
          viewEntity.setSelected(false);
          viewEntity.setType(EntityTypes.LR);
          viewEntity.setLevel(level);
          tree.getEntities().add(viewEntity);
          tree.getLrCache().put(lr.getId(), viewEntity);

          // sub childs
          if (!viewEntity.isPacked()) {
            addChildEntities(viewEntity, tree, level, packedList);
          }

          tree.getChildAssignments().put(lr.getId(), lr.getAssignments());

          // EFPs
          if (!viewEntity.isPacked()) {
            for (LrAssignmentEntity assignment : lr.getAssignments()) {
              ViewEntity ve = new ViewEntityRecord(null, assignment, lrService);
              ve.setPacked(false);
              ve.setType(EntityTypes.ASSIGNMENT);
              viewEntity.setLevel(0);
              viewEntity.setSelected(false);
              tree.getEntities().add(ve);

              String key = String.valueOf(lr.getId())
                + "_" + String.valueOf(assignment.getEfp().getId());
              if (tree.getEfpCache().containsKey(key)) {
                tree.getEfpCache().get(key).add(ve);
              } else {
                List<ViewEntity> list = new LinkedList<ViewEntity>();
                list.add(ve);
                tree.getEfpCache().put(key, list);
              }
            }
          }
        }
      }
    }

    return tree;
  }

  /**
   * Get choosen object or null from parameters.
   * @param grId gr
   * @param choosenObjectType String
   * @param choosenObjectId String
   * @param entityTree {@link EntityTree}
   * @param packedListString String
   * @return {@link ChoosenObject}
   */
  public ChoosenObject getChoosenObject(
      final String grId,
      final String choosenObjectType,
      final String choosenObjectId,
      final EntityTree entityTree,
      final String packedListString) {
    ChoosenObject result = null;
    if (choosenObjectType == null || choosenObjectId == null) {
      return result;
    }
    EntityTypes type = EntityTypes.getByName(choosenObjectType.toLowerCase());
    if (type != null) {
      LrEntity lrEntity = null;
      String[] ids = null;
      switch (type) {
        case GR:
          // nothing, not selectable from a tree
          break;
        case LR:
          lrEntity = lrService.findById(Integer.valueOf(choosenObjectId));
          if (lrEntity != null) {

            ViewEntity foundEntity = entityTree.getLrCache().get(Integer.valueOf(choosenObjectId));

            if (foundEntity == null) {
              foundEntity = new ViewEntityRecord(lrEntity, null, lrService);
            }

            if (foundEntity != null) {
              result = new ChoosenObject(
                  type,
                  lrEntity,
                  null,
                  convert(foundEntity.getChildEntities(false)),
                  null,
                  null);
            }
          }
          break;
        case ASSIGNMENT:
          // choosenObjectId has both IDs
          ids = choosenObjectId.split(ControllerConst.ID_DELIMITER);
          lrEntity = lrService.findById(Integer.valueOf(ids[0]));
          EfpEntity efpEntity = efpService.findById(Integer.valueOf(ids[1]));

          result = new ChoosenObject(
              EntityTypes.ASSIGNMENT,
              lrEntity,
              efpEntity,
              null,
              efpEntity.getValueNames(),
              null);
          break;
        case VALUE:
          ids = choosenObjectId.split(ControllerConst.ID_DELIMITER);
          String efpId = ids[0];
          String lrId = ids[1];
          String valueName = ids[2];
          LrAssignmentEntity assignmentEntity = lrAssignmentService.findLrAssignment(
              Integer.valueOf(efpId),
              Integer.valueOf(lrId),
              valueName);

          lrEntity = lrService.findById(Integer.valueOf(lrId));

          EfpEntity efpEntityInner = efpService.findById(Integer.valueOf(efpId));

          if (assignmentEntity != null) {
            result = new ChoosenObject(
                EntityTypes.VALUE,
                lrEntity,
                efpEntityInner,
                null,
                null,
                assignmentEntity);
          } else {
            LOGGER.error("Cannot find LrAssignment: ("
                + efpId + "," + lrId + "," + valueName + ")");
          }
          break;
        default:
          // nothing
          break;
      }
    }
    return result;
  }

  @Override
  public List<LrEntity> convert(final List<ViewEntity> entities) {
    List<LrEntity> result = new LinkedList<LrEntity>();

    for (ViewEntity ve : entities) {
      result.add(ve.getLrEntity());
    }

    return result;
  }

  /** LrService. */
  private LrService lrService;
  /**
   * DI setter.
   * @param lrService {@link LrService}
   */
  @Autowired
  public void setLrService(final LrService lrService) {
    this.lrService = lrService;
  }

  /** EfpService. */
  private EfpService efpService;
  /**
   * EfpService.
   * @param efpService {@link EfpService}
   */
  @Autowired
  public void setEfpService(final EfpService efpService) {
    this.efpService = efpService;
  }

  /** LrAssignmentService. */
  private LrAssignmentService lrAssignmentService;
  /**
   * DI setter.
   * @param lrAssignmentService {@link LrAssignmentService}
   */
  @Autowired
  public void setLrAssignmentService(final LrAssignmentService lrAssignmentService) {
    this.lrAssignmentService = lrAssignmentService;
  }
}
