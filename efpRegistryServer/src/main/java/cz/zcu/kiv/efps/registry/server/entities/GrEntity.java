/**
 *
 */
package cz.zcu.kiv.efps.registry.server.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Entity
@Table(name = "gr")
public class GrEntity implements Serializable {

  private static final long serialVersionUID = 7529565442092908271L;

    /** ID. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /** A human readable name of GR. */
    @Column(nullable = false, unique = true)
    private String name;
    /** A string ID of GR. */
    @Column(nullable = false, unique = true)
    private String idStr;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the string ID
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param idStr the string iD
     */
    public void setIdStr(final String idStr) {
        this.idStr = idStr;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode()) + ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GrEntity other = (GrEntity) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }

        if (idStr == null) {
            if (other.idStr != null) {
                return false;
            }
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }


}
