/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.GrDTO;

/**
 * A web service for GR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@WebService(targetNamespace = GrWs.NAMESPACE)
public interface GrWs {

    /** Target namespace. */
    String NAMESPACE = "http://v1_0_0.ws.server.registry.efps.kiv.zcu.cz";

    /**
     * Finds GR.
     * @param id ID of GR
     * @return found GR
     */
    GrDTO getGrById(@WebParam(name = "id") Integer id);

    /**
     * Creates new GR.
     * @param grDTO not existing GR
     * @return created object.
     */
    GrDTO createGr(@WebParam(name = "grDTO") GrDTO grDTO);

    /**
     * Finds all GRs.
     * @return all GRs
     */
    List<GrDTO> findAllGrs();

    /**
     * Updated GR.
     * @param modifiedGr GR with new value(s)
     */
    void updateGr(@WebParam(name = "modifiedFr") GrDTO modifiedGr);

    /**
     * This method reads all value names available for this GR.
     * @param id id of GR
     * @return the list of names
     */
    List<String> getAllValueNames(@WebParam(name = "grId") Integer id);

    /**
     * This method allows to change a value name.
     * @param oldValue an old name
     * @param newValue a new name
     * @param id id of GR
     */
    void updateValueName(
            @WebParam(name = "grId") Integer id,
            @WebParam(name = "oldValue") String oldValue,
            @WebParam(name = "newValue") String newValue);

     /**
      * This method deletes one value name.
      * @param valueName the value name
     * @param id id of GR
      */
    void deleteValueName(
            @WebParam(name = "grId") Integer id,
            @WebParam(name = "valueName") String valueName);

    /**
     * This method creates one value name.
     * @param valueName the value name
    * @param id id of GR
     */
   void createValueName(
           @WebParam(name = "grId") Integer id,
           @WebParam(name = "valueName") String valueName);

   /**
    * This method deletes GR with the given ID.
    * @param grId the ID
    */
   void deleteGR(@WebParam(name = "grId") Integer grId);


}
