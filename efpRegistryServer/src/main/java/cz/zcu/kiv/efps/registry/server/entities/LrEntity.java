/**
 *
 */
package cz.zcu.kiv.efps.registry.server.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Entity
@Table(name = "lr",
         uniqueConstraints = {@UniqueConstraint(columnNames = { "name", "gr_id" }) }
        )
public class LrEntity {

    /** ID. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /** A human readable name of this LR. */
    @Column(nullable = false)
    private String name;
    /** A string ID of LR. */
    @Column(nullable = true)
    private String idStr;

    /** A link to parent LR. */
    @ManyToOne
    @JoinColumn(name = "parent_lr_id")
    private LrEntity lrParent;

    /** A link to GR this LR is related to. */
    @ManyToOne
    @JoinColumn(name = "gr_id", nullable = false)
    private GrEntity gr;

    /** A list of assignments. */
    @OneToMany(mappedBy = "lr")
    private List<LrAssignmentEntity> assignments;

    /**
     * @return the gr
     */
    public GrEntity getGr() {
        return gr;
    }

    /**
     * @param gr the gr to set
     */
    public void setGr(final GrEntity gr) {
        this.gr = gr;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** Max length of name. */
    private static final int NAME_MAX_LENGTH = 14;
    /** Index for split. */
    private static final int NAME_DELIMITER_INDEX = 13;

    /**
     * Get optimized name for view.
     * @return String
     */
    public String getOptimizedName() {
      if (name.length() <= NAME_MAX_LENGTH) {
        return name;
      } else {
        String firstPart = name.substring(0, NAME_DELIMITER_INDEX);
        String secondPart = name.substring(NAME_DELIMITER_INDEX);
        return firstPart + "<br />" + secondPart;
      }
    }

    /**
     * Get String Id (Usefull for JSTL).
     * @return String
     */
    public String getStringId() {
      return String.valueOf(id);
    }

    /**
     * @return the parent
     */
    public LrEntity getParent() {
        return lrParent;
    }

    /**
     * @return the string ID
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(final LrEntity parent) {
        this.lrParent = parent;
    }

    /**
     * @return the assignments
     */
    public List<LrAssignmentEntity> getAssignments() {
        return assignments;
    }

    /**
     * @param assignments the assignments to set
     */
    public void setAssignments(final List<LrAssignmentEntity> assignments) {
        this.assignments = assignments;
    }

    /**
     * @param idStr the string iD
     */
    public void setIdStr(final String idStr) {
        this.idStr = idStr;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LrEntity other = (LrEntity) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }

        if (idStr == null) {
            if (other.idStr != null) {
                return false;
            }
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }


}
