package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.dao.LrAssignmentDao;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;

/**
 * DAO Impl.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Repository(value = "lrAssignmentDao")
public class LrAssignmentDaoImpl extends
        AbstractHibernateDao<LrAssignmentEntity, Integer> implements
        LrAssignmentDao {

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentEntity> findLrAssignments(final Integer efpId, final Integer lrId) {
        Criteria crit = createCriteria();

        Criteria critLr = crit.createCriteria("lr");
        critLr.add(Restrictions.eq("id", lrId));

        Criteria critEfp = crit.createCriteria("efp");
        critEfp.add(Restrictions.eq("id", efpId));

        return findByCriteria(crit);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentEntity findLrAssignment(final Integer efpId, final Integer lrId,
            final String name) {

        Criteria crit = createCriteria();

        Criteria valName = crit.createCriteria("valueName");
        valName .add(Restrictions.eq("name", name));

        Criteria critLr = crit.createCriteria("lr");
        critLr.add(Restrictions.eq("id", lrId));

        Criteria critEfp = crit.createCriteria("efp");
        critEfp.add(Restrictions.eq("id", efpId));

        return (LrAssignmentEntity) crit.uniqueResult();
    }

}
