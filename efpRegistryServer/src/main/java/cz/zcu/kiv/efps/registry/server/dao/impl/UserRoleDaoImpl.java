package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import cz.zcu.kiv.efps.registry.server.controllers.ControllerConst;
import cz.zcu.kiv.efps.registry.server.dao.UserRoleDao;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;

/**
 * Implementation of UserRoleDao.
 * @author Bc. Tomas Kohout
 * @since 11.03.2012
 */
@Repository(value = "userRoleDao")
public class UserRoleDaoImpl extends AbstractHibernateDao<UserRoleEntity, Integer>
implements UserRoleDao {

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  public boolean isAssignedToComponentDeveloper(final Integer grId) {
    Session session = getSession();

    String queryString = "SELECT gr.ID, gr.NAME FROM GR gr"
    + " LEFT JOIN ASSIGNED_GR ass_gr ON ass_gr.GR_ID = gr.ID"
    + " LEFT JOIN USERS_ROLE us_role ON us_role.ID = ass_gr.ROLE_ID"
    + " WHERE gr.ID = " + grId
    + " AND us_role.NAME = '"
    + ControllerConst.COMPONENT_DEVELOPER_ROLE + "'";

    SQLQuery query = session.createSQLQuery(queryString)
      .addEntity(GrEntity.class);

    return !((List<GrEntity>) query.list()).isEmpty();
  }
}
