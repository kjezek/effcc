package cz.zcu.kiv.efps.registry.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import cz.zcu.kiv.efps.registry.server.dao.UserDao;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;

/**
 * Dao for database authentication.
 * @author Bc. Tomas Kohout
 * @since 11.02.2012
 */
public class HibernateAuthenticationDaoImpl implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(final String username)
      throws UsernameNotFoundException, DataAccessException {

    User userFromDB = null;

    UserEntity example = new UserEntity();
    example.setLogin(username);

    List<UserEntity> users = userDao.findByExample(example, new String[] {});

    if (!users.isEmpty()) {
      UserEntity userEntity = users.get(0);   // should be only one (unique login)
      if (userEntity != null) {

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (UserRoleEntity role : userEntity.getUsersRoles()) {
          // maybe some better identifier
          GrantedAuthority ga = new GrantedAuthorityImpl(role.getName());
          authorities.add(ga);
        }

        userFromDB = new User(
            username,
            userEntity.getPassword(),
            true,
            true,
            true,
            true,
            authorities);

      } else {
        throw new UsernameNotFoundException("User " + username + "doesn't exist.");
      }
    } else {
      throw new UsernameNotFoundException("User " + username + "doesn't exist.");
    }

    return userFromDB;
  }

  //=======================================================================================
  /** {@link UserDao}. */
  private UserDao userDao;
  /**
   * DI setter.
   * @param userDao {@link UserDao}
   */
  @Autowired
  public void setUserDao(final UserDao userDao) {
    this.userDao = userDao;
  }
}
