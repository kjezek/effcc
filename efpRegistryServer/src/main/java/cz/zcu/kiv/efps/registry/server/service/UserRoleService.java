package cz.zcu.kiv.efps.registry.server.service;

import cz.zcu.kiv.efps.registry.server.entities.UserRoleEntity;

/**
 * Service for UserRole handling.
 * @author Bc. Tomas Kohout
 * @since 11.03.2012
 */
public interface UserRoleService extends GenericService<UserRoleEntity, Integer> {

  /**
   * Is it GR assigned to component developer?.
   * @param grId Integer
   * @return boolean value
   */
  boolean isAssignedToComponentDeveloper(Integer grId);

  /**
   * Find UserRole by role name.
   * @param name String
   * @return {@link UserRoleEntity}
   */
  UserRoleEntity findByName(String name);
}
