package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.LrAssignmentDao;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * LR Assignment Service Impl.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Service(value = "lrAssignmentService")
public class LrAssignmentServiceImpl extends
        AbstractService<LrAssignmentDao, LrAssignmentEntity, Integer> implements
        LrAssignmentService {

    /** LR Assignment DAO. */
    @Resource(name = "lrAssignmentDao")
    private LrAssignmentDao lrAssignmentDao;

    /** LR service. */
    @Resource(name = "lrService")
    private LrService lrService;

    /** {@inheritDoc} */
    @Override
    protected LrAssignmentDao getDao() {
        return lrAssignmentDao;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentEntity> findLrAssignments(final Integer efpId,
            final Integer lrId) {

        List<LrAssignmentEntity> result = new LinkedList<LrAssignmentEntity>();

        LrEntity lr = lrService.getById(lrId);

        // go through current LR and all parent LRs
        while (lr != null) {
            List<LrAssignmentEntity> assignments = lrAssignmentDao.findLrAssignments(
                    efpId, lr.getId());

            // remove assignments already being in the collection.
            removeAssignments(assignments, result);
            result.addAll(assignments);

            lr = lr.getParent(); // go to the parent
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentEntity> findLrAssignments(final Integer lrId) {
        List<LrAssignmentEntity> result = new LinkedList<LrAssignmentEntity>();

        LrEntity lr = lrService.getById(lrId);

        // go through current LR and all parent LRs
        while (lr != null) {
            List<LrAssignmentEntity> assignments = lr.getAssignments();

            // remove assignments already being in the collection.
            removeAssignments(assignments, result);
            result.addAll(assignments);

            lr = lr.getParent(); // go to the parent
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentEntity findLrAssignment(final Integer efpId, final Integer lrId,
            final String name) {

        LrEntity lr = lrService.getById(lrId);

        LrAssignmentEntity result = null;

        // go through current LR and all parent LRs
        while (result == null && lr != null) {

            result = lrAssignmentDao.findLrAssignment(efpId, lr.getId(), name);

            lr = lr.getParent(); // go to the parent
        }

        return result;
    }

    /**
     * It filters assignments along the template.
     * @param assignments a list to filter
     * @param remove a template items being removed from the assignments
     */
    private void removeAssignments(final List<LrAssignmentEntity> assignments,
            final List<LrAssignmentEntity> remove) {

        CollectionUtils.filter(assignments, new Predicate() {

            @Override
            public boolean evaluate(final Object object) {

                //  go through assignments and filter those with the same names.
                for (LrAssignmentEntity item : remove) {
                    if (item.getValueName().equals(
                            ((LrAssignmentEntity) object).getValueName())) {

                        // the current value will be removed
                        return false;
                    }
                }

                // the current value will NOT be removed
                return true;
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    public List<EfpValueType> generateLrAssignment(final String fce, final double from,
            final double to, final double step) {
        // TODO [stulc] implementet sever List<LrAssignment> generateLrAssignment
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignmentEntity> findLrAssignmentsWithoutParent(final Integer lrId) {
      List<LrAssignmentEntity> result = new LinkedList<LrAssignmentEntity>();

      LrEntity lr = lrService.getById(lrId);

      // go through current LR
      List<LrAssignmentEntity> assignments = lr.getAssignments();

      // remove assignments already being in the collection.
      removeAssignments(assignments, result);
      result.addAll(assignments);

      lr = lr.getParent(); // go to the parent

      return result;
    }

}
