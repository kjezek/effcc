package cz.zcu.kiv.efps.registry.server.beans;

import java.io.Serializable;

import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;

/**
 * Bean class for handling forms (ValueName entities).
 * @author Bc. Tomas Kohout
 * @since 28.10.2011
 */
public class ValueNameBean implements Serializable {

  /** serial UID. */
  private static final long serialVersionUID = 2264598721817095992L;

  /** ValueNameEntity. */
  private ValueNameEntity valueNameEntity;
  /** GrId. */
  private String grId;
  /** EfpId. */
  private String efpId;
  /** LR id. */
  private String lrId;

  /**
   * Getter.
   * @return {@link ValueNameEntity}
   */
  public ValueNameEntity getValueNameEntity() {
    return valueNameEntity;
  }

  /**
   * Setter.
   * @param valueNameEntity  {@link ValueNameEntity}
   */
  public void setValueNameEntity(final ValueNameEntity valueNameEntity) {
    this.valueNameEntity = valueNameEntity;
  }

  /**
   * Getter.
   * @return String
   */
  public String getGrId() {
    return grId;
  }

  /**
   * Setter.
   * @param grId String
   */
  public void setGrId(final String grId) {
    this.grId = grId;
  }

  /**
   * Getter.
   * @return String
   */
  public String getEfpId() {
    return efpId;
  }

  /**
   * Setter.
   * @param efpId String
   */
  public void setEfpId(final String efpId) {
    this.efpId = efpId;
  }

  /**
   * Getter.
   * @return String
   */
  public String getLrId() {
    return lrId;
  }

  /**
   * Setter.
   * @param lrId String
   */
  public void setLrId(final String lrId) {
    this.lrId = lrId;
  }
}
