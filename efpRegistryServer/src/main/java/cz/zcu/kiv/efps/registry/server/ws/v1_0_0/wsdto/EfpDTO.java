/**
 *
 */
package cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto;

import java.util.List;

/**
 * A DTO object for EFPs.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpDTO {

    /** ID. */
    private Integer id;
    /** A name of the EFP. */
    private String name;
    /** A measuring unit. */
    private String unit;
    /** A user defined gamma. */
    private String gamma;
    /** A value type of this EFP. */
    private Class<?> valueType;

    /** A link to GR. */
    private GrDTO gr;

    /** A list of properties this EFP is derived from. */
    private List<EfpDTO> derivedFromEfps;

    /** Value names for LRs. */
    private List<String> valueNames;

    /**
     * @return the derivedFromEfps
     */
    public List<EfpDTO> getDerivedFromEfps() {
        return derivedFromEfps;
    }

    /**
     * @param derivedFromEfps the derivedFromEfps to set
     */
    public void setDerivedFromEfps(final List<EfpDTO> derivedFromEfps) {
        this.derivedFromEfps = derivedFromEfps;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @return the gamma
     */
    public String getGamma() {
        return gamma;
    }

    /**
     * @return the valueNames
     */
    public List<String> getValueNames() {
        return valueNames;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(final String unit) {
        this.unit = unit;
    }

    /**
     * @param gamma the gamma to set
     */
    public void setGamma(final String gamma) {
        this.gamma = gamma;
    }

    /**
     * @param valueNames the valueNames to set
     */
    public void setValueNames(final List<String> valueNames) {
        this.valueNames = valueNames;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }


    /**
     * @return the gr
     */
    public GrDTO getGr() {
        return gr;
    }

    /**
     * @param gr the gr to set
     */
    public void setGr(final GrDTO gr) {
        this.gr = gr;
    }



    /**
     * @return the valueType
     */
    public Class<?> getValueType() {
        return valueType;
    }

    /**
     * @param valueType the valueType to set
     */
    public void setValueType(final Class<?> valueType) {
        this.valueType = valueType;
    }

    /** {@inheritDoc}  */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc}  */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpDTO other = (EfpDTO) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }


}
