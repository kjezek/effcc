package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import cz.zcu.kiv.efps.registry.server.dao.UserDao;
import cz.zcu.kiv.efps.registry.server.entities.UserEntity;
import cz.zcu.kiv.efps.registry.server.service.UserService;

/**
 * UserServiceImpl.
 * @author Bc. Tomas Kohout
 * @since 02.03.2012
 */
@Service(value = "userService")
public class UserServiceImpl extends AbstractService<UserDao, UserEntity, Integer>
implements UserService {

  /** Reference to UserDao. */
  @Resource(name = "userDao")
  private UserDao userDao;

  /** {@inheritDoc} */
  @Override
  protected UserDao getDao() {
    return userDao;
  }

  /** {@inheritDoc} */
  public boolean loginExist(final String login) {
    UserEntity user = new UserEntity();
    user.setLogin(login);

    List<UserEntity> list = userDao.findByExample(user, new String[] {});

    return !list.isEmpty();
  }

  /** {@inheritDoc} */
  public UserEntity getLoggedUser() {

    SecurityContext context = SecurityContextHolder.getContext();

    if (context != null && context.getAuthentication() != null) {
      UserEntity example = new UserEntity();
      if (context.getAuthentication().getPrincipal() instanceof User) {
        User user = (User) context.getAuthentication().getPrincipal();
        example.setLogin(user.getUsername());
      } else {
        return null; // anonymous
      }

      List<UserEntity> list = userDao.findByExample(example, new String[] {});

      if (list != null && !list.isEmpty()) {
        return list.get(0); //should be only one user
      }
    }

    return null;
  }

  /** {@inheritDoc} */
  public UserEntity getUserByLogin(final String login) {

    UserEntity res = null;

    UserEntity example = new UserEntity();
    example.setLogin(login);

    List<UserEntity> users = userDao.findByExample(example, new String[] { });
    if (users != null && !users.isEmpty()) {
      res = users.get(0);
    }

    return res;
  }
}
