/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.entities.GrEntity;

/**
 * A service on GR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface GrService extends GenericService<GrEntity, Integer> {

  /**
   * Remove assign GR to user.
   * @param grId String
   * @param userId String
   */
  void removeGRToUser(Integer grId, Integer userId);

  /**
   * Get list of assigned GRs for specified user.
   * @param userId Integer
   * @return {@link List}
   */
  List<GrEntity> getAssignedGRs(Integer userId);

}
