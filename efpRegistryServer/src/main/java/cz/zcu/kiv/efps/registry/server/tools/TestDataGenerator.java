/**
 *
 */
package cz.zcu.kiv.efps.registry.server.tools;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;

/**
 * This class creates and stores test data into
 * DB. It server only for test purposes.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
//@Component(value = "testDataGenerator")
public class TestDataGenerator {

    /** A service under test. */
    @Resource(name = "lrAssignmentService")
    private LrAssignmentService lrAssignmentService;

    /** A service used for this test. */
    @Resource(name = "efpService")
    private EfpService efpService;

    /** A service used for this test. */
    @Resource(name = "grService")
    private GrService grService;

    /** A service used for this test. */
    @Resource(name = "lrService")
    private LrService lrService;

    /** A service used for this test. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** Common test entity. */
    private GrEntity gr;

    /** Common test entity. */
    private EfpEntity efp1;

    /** Common test entity. */
    private EfpEntity efp2;

    /** Common test entity. */
    private EfpEntity derivedEfp;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigSlow;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigQuick;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigAverage;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigVerySlow;

    /** Common test entity. */
    private LrEntity lr;

    /** Common test entity. */
    private LrEntity parentLr;

    /** Common test entity. */
    private ValueNameEntity nameSlow;

    /** Common test entity. */
    private ValueNameEntity nameQuick;

    /** Common test entity. */
    private ValueNameEntity nameAverage;

    /** Common test entity. */
    private ValueNameEntity nameVerySlow;

    /** Common test entity. */
    private ValueNameEntity nameVeryQuick;

    /**
     * It generates test data.
     */
    @PostConstruct
    public void generate() {
        final String ut = new SimpleDateFormat("DD.MM.yy-hh.mm.ss.SS").format(new Date());

        gr = new GrEntity();
        gr.setName("Education systems" + ut);
        grService.save(gr);

        nameSlow = new ValueNameEntity();
        nameSlow.setGr(gr);
        nameSlow.setName("slow");
        valueNameService.save(nameSlow);

        nameQuick = new ValueNameEntity();
        nameQuick.setGr(gr);
        nameQuick.setName("quick");
        valueNameService.save(nameQuick);

        nameAverage = new ValueNameEntity();
        nameAverage.setGr(gr);
        nameAverage.setName("average");
        valueNameService.save(nameAverage);

        nameVerySlow = new ValueNameEntity();
        nameVerySlow.setGr(gr);
        nameVerySlow.setName("very slow");
        valueNameService.save(nameVerySlow);

        nameVeryQuick = new ValueNameEntity();
        nameVeryQuick.setGr(gr);
        nameVeryQuick.setName("very quick");
        valueNameService.save(nameVeryQuick);

        efp1 = new EfpEntity();
        efp1.setName("EFP1");
        efp1.setGr(gr);
        efp1.setValueNames(
                Arrays.asList(nameVeryQuick, nameQuick, nameAverage, nameSlow, nameVerySlow));
        efp1.setValueType(EfpNumberInterval.class);
        efpService.save(efp1);

        efp2 = new EfpEntity();
        efp2.setName("EFP2");
        efp2.setGr(gr);
        efp2.setValueType(EfpNumber.class);
        efpService.save(efp2);

        derivedEfp = new EfpEntity();
        derivedEfp.setName("Derived EFP");
        derivedEfp.setGr(gr);
        derivedEfp.setDerivedFromEfps(Arrays.asList(efp1, efp2));
        derivedEfp.setValueType(EfpEnum.class);
        efpService.save(derivedEfp);

        parentLr = new LrEntity();
        parentLr.setName("High performance LR.");
        parentLr.setGr(gr);
        lrService.save(parentLr);

        lr = new LrEntity();
        lr.setName("High performance LR - specialised.");
        lr.setGr(gr);
        lr.setParent(parentLr);
        lrService.save(lr);

        lrAssigSlow = new LrAssignmentEntity();
        lrAssigSlow.setEfp(efp1);
        lrAssigSlow.setLr(parentLr);
        lrAssigSlow.setValue(new EfpNumberInterval(20, 30).serialise());
        lrAssigSlow.setValueName(nameSlow);
        lrAssignmentService.save(lrAssigSlow);

        lrAssigAverage = new LrAssignmentEntity();
        lrAssigAverage.setEfp(efp1);
        lrAssigAverage.setLr(parentLr);
        lrAssigAverage.setValue(new EfpNumberInterval(10, 20).serialise());
        lrAssigAverage.setValueName(nameAverage);
        lrAssignmentService.save(lrAssigAverage);

        lrAssigQuick = new LrAssignmentEntity();
        lrAssigQuick.setEfp(efp1);
        lrAssigQuick.setLr(parentLr);
        lrAssigQuick.setValue(new EfpNumberInterval(0, 10).serialise());
        lrAssigQuick.setValueName(nameQuick);
        lrAssignmentService.save(lrAssigQuick);

        lrAssigQuick = new LrAssignmentEntity();
        lrAssigQuick.setEfp(efp1);
        lrAssigQuick.setLr(lr); // inherited LR
        lrAssigQuick.setValue(new EfpNumberInterval(10, 30).serialise());
        lrAssigQuick.setValueName(nameQuick);
        lrAssignmentService.save(lrAssigQuick);

        lrAssigVerySlow = new LrAssignmentEntity();
        lrAssigVerySlow.setEfp(efp1);
        lrAssigVerySlow.setLr(lr); // inherited LR
        lrAssigVerySlow.setValue(new EfpNumberInterval(30, 100).serialise());
        lrAssigVerySlow.setValueName(nameVerySlow);
        lrAssignmentService.save(lrAssigVerySlow);
    }
}
