/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.security.access.annotation.Secured;

import cz.zcu.kiv.efps.registry.server.controllers.ControllerConst;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 * @param <T> type of a persistent entity
 * @param <ID> type of the ID
 *
 */
public interface GenericService<T, ID extends Serializable> {


    /**
     * Gets an entity by ID.
     * @param id the ID
     * @return the found entity or an exception
     */
    T getById(ID id);

    /**
     * Gets an entity by ID.
     * @param id the ID
     * @return the found entity or null
     */
    T findById(ID id);


    /**
     *
     * @return all entities in database (be careful, it may be a lot of instances)
     */
    List<T> findAll();

    /**
     * Persists an entity.
     * @param entity the entity
     */
    @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
    void save(T entity);

    /**
     * Persists an entity.
     * Login is not required.
     * @param entity the entity
     */
    void saveWithoutLogin(T entity);


    /**
     * Update an entity.
     * @param entity the entity
     */
    @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
    void update(T entity);


    /**
     * Persits or updates an entity.
     * @param entity the entity
     */
    @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
    void saveOrUpdate(T entity);


    /**
     * Deletes an entity.
     * @param entity the entity
     */
    @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
    void delete(T entity);

    /**
     * Deletes an entity by ID.
     * @param id the ID of the entity
     */
    @Secured({ ControllerConst.ADMIN_ROLE, ControllerConst.DOMAIN_EXPERT_ROLE })
    void deleteById(ID id);
}
