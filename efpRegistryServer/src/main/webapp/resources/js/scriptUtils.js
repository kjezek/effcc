/**
 * JavaScript functions for checking forms and other utilities.
 * @author Bc. Tomáš Kohout
 */

/**
 * Open target in new window - XHTML do not use target="_blank"
 */
function bar(url) {
  wasOpen  = false;
  win = window.open(url);
  return (typeof(win)=='object')?true:false;
}

function confirmGrDelete() {
  $(document).ready(function(){
        $(".modalInput").overlay({
              mask: {
                  color: '#aaa',
                  loadSpeed: 'fast',
                  opacity: 0.5
              },
              speed: 'fast',
              closeOnClick: true
        });
        $('.modalInput').click(function(){
            var modal = $(this).attr('rel');
            var action = $(this).attr('rev');
            /* set action on yes button */
            $(modal).find('.yes').eq(0).attr('href', action);
        });
  });
}

function confirmDelete(text, ref)
{
  var returnValue = window.confirm(text);
  if(returnValue == true) {
    window.location=ref;
  } else {
    window.location="#";
  }
}


/**
 * Check user form in registration.
 * Test filled inputs and password check.
 */
function testData()
{
  var login = document.getElementById("login");
  var name = document.getElementById("name");
  var surname = document.getElementById("surname");
  var email = document.getElementById("email");
  var password = document.getElementById("password");
  var password2 = document.getElementById("password2");
  var birth = document.getElementById("birth");
  var regx = /^\d\d\.\d\d\.19\d\d$/;
  var returnval = true;
  var errorBirth = false;
  var errorFill = false;
  var dtArr;

  if(login.value == "")
  {
    errorFill = true;
    returnval = false;
    login.style["background"]="#FFFF66";
  }
  if(name.value == "")
  {
    errorFill = true;
    returnval = false;
    name.style["background"]="#FFFF66";
  }
  if(surname.value == "")
  {
    errorFill = true;
    returnval = false;
    surname.style["background"]="#FFFF66";
  }
  if(email.value == "")
  {
    errorFill = true;
    returnval = false;
    email.style["background"]="#FFFF66";
  }
  if(password.value == "")
  {
    errorFill = true;
    returnval = false;
    password.style["background"]="#FFFF66";
  }
  if(password2.value == "")
  {
    errorFill = true;
    returnval = false;
    password2.style["background"]="#FFFF66";
  }
  if(birth.value == "")
  {
    errorFill = true;
    returnval = false;
    birth.style["background"]="#FFFF66";
  }

  if(password.value != password2.value)
     {
       alert("Potvrzení hesla se neshoduje se zadaným heslem.");
       returnval = false;
     }

     if(regx.test(birth.value))
     {
       dtArr = birth.value.split(".");
       if((parseInt(dtArr[0]) > 31) || (parseInt(dtArr[1]) > 12))
       {
         returnval = false;
         errorBirth = true;
       }
     }
     else
     {
       returnval = false;
       errorBirth = true;
     }

     if(errorFill)
     {
       alert("Musí být vyplněny všechny žluté položky.");
     }

     if(errorBirth)
     {
       birth.style["background"]="#FFFF66";
       alert("Špatně zadáno datum narození.");
     }

  return returnval;
}

/**
 * Check create Story form.
 * Using REGEXP EXPRESSIONS for transformations by template.
 */
function testCreateStory()
{
  var blog = document.getElementById("blogID");
  var caption = document.getElementById("caption");
  var content = document.getElementById("content");

  var errorFill = false;
  var returnval = true;
  var str;

  if(blog.value == "") {
    errorFill = true;
    returnval = false;
    blog.style["background"]="#FFFF66";
  }

  if(caption.value == "") {
    errorFill = true;
    returnval = false;
    caption.style["background"]="#FFFF66";
  }

  if(content.value == "") {
    errorFill = true;
    returnval = false;
    content.style["background"]="#FFFF66";
  } else {
    var rexp = new RegExp("<p>", "g");
    var rexpend = new RegExp("</p>", "g");
    var matchp = content.value.match(rexp);
    var matchpend = content.value.match(rexpend);
    alert(matchp.length);
    alert(matchpend.length);
    if(matchp.length != matchpend.length) {
      alert("Každý odstavec musíte ukončit pomocí </p>.");
      content.style["background"]="#FFFF66";
      returnval = false;
    }
    /** Check HTML tags. */
    var re = new RegExp("</*[a-zA-Z /]*[^p]>", "g");
    var re2 = new RegExp("<p>", "g");
    str = content.value.replace(re, "");
    str = str.replace(re2, "<p class='subcontent'>");
  }

  if(errorFill) {
    alert("Žlutě označená pole musí být vyplněná.");
  }

  if((returnval)&&(!errorFill)) {
    content.value = str;
  }

  return returnval;
}