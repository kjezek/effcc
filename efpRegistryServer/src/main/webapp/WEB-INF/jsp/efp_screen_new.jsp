<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <form:form modelAttribute="efp_object" action="${pageContext.request.contextPath}/client/efp/newEFPFormScreen" method="post">
        <form:hidden path="grId" value="${choosen_gr}" />
        <table border="0" class="attributes">
        <tr>
          <th colspan="2" style="height: 80px;"><spring:message code="efp.client.efpScreen.content.help" /></th>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="efpEntity.id" class="caption">Id:</form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="efpEntity.id" class="disabled" onfocus="disabled=true;" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="efpEntity.name" class="caption"><spring:message code="efp.client.efp_screen.name"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="efpEntity.name" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="efpEntity.gamma" class="caption"><spring:message code="efp.client.efp_screen.gamma"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="efpEntity.gamma" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="efpEntity.unit" class="caption"><spring:message code="efp.client.efp_screen.unit"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="efpEntity.unit" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="efpEntity.valueType" class="caption"><spring:message code="efp.client.efp_screen.valueType"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="efpEntity.valueType" class="text-field" /></td>
        </tr>
        <tr>
          <td></td>
          <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
        </tr>
        </table>
      </form:form>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.efpScreen.chooseGr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectGrEfpScreenNew" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">${efp_object.efpEntity.name}</h3>
        <h5 class="grScreen"><spring:message code="efp.client.efpScreen.efp" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.efpScreen.efp_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <a href="${pageContext.request.contextPath}/client/efp/newEFP?gr_id=${choosen_gr}" title="" class="menuButtonSelected">
          <spring:message code="efp.client.efpScreen.newEFP" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/efpScreen?gr_id=${choosen_gr}&efp_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.updateEFP" />
        </a>
        <a href="" title="" class="menuButtonDisabled" onclick="return false">
          <spring:message code="efp.client.efpScreen.deleteEFP" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/assignmentScreen?gr_id=${choosen_gr}&lr_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.newLrAssignment" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/deleteAssignmentScreen?gr_id=${choosen_gr}&lr_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.deleteLrAssignment" />
        </a>
        <a href="${pageContext.request.contextPath}/client/main/tree?gr_id=${gr_object.id}&choosen_object_type=&choosen_object_id=&packed_list=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.grScreen.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

