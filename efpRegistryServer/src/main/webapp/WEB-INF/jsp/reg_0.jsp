<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      var options = {
          target:        '#ajaxTarget',
          beforeSubmit:  showRequest,
          success:       showResponse
      };

      $('#new_form').ajaxForm(options);

      $('#back').click(function() {
        window.location = $(this).attr('name');
      });
    });

    function showRequest(formData, jqForm, options) {

      // login check
      var login = $('#login').fieldValue();

      $.get($('#login_check').attr('name')+login, function(data) {
        if(data != null && data != '') {
          alert(data);
          return false;
        }
      });

      // password check
      var password = $('#password').fieldValue();
      var passwordCheck = $('#password_check').fieldValue();

      if($.trim(password) != $.trim(passwordCheck)) {
        alert($('#not_match').text());
        return false;
      }

      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      window.location = $('#next').attr('name');
    }
    </script>
  </head>
  <body>
    <div id="main">
      <div id="ajaxTarget" style="display: none;"></div>
      <button id="next" name="${pageContext.request.contextPath}/client/main/guidepost" style="display: none;">next</button>
      <button id="login_check" name="${pageContext.request.contextPath}/client/user/checkLogin?login=" style="display: none;"></button>
      <p id="not_match" style="display: none;">
        <spring:message code="efp.client.reg0.passwordMatch" />
      </p>
      <img src="${pageContext.request.contextPath}/client/resources/graphics/banner-free.png" height="96" width="806" alt="" title="" />
      <div id="content">
        <form id="new_form" action="${pageContext.request.contextPath}/client/user/newUser" method="post">
          <table class="attributes" style="width: 700px;">
            <tr>
              <td><button id="back" name="${pageContext.request.contextPath}/client/main/login" type="button" class="black">&lt;&lt;</button></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th colspan="3" style="height: 80px;"><spring:message code="efp.client.reg0.newUser" /></th>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption"><spring:message code="efp.client.reg0.login"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="login" name="login" class="text-field" style="width: 250px;" /></td>
              <td rowspan="6" class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/login-paper2.png); background-repeat: none;">
                <table>
                  <tr>
                    <th><spring:message code="efp.client.reg0.userRoles" /></th>
                  </tr>
                  <c:forEach var="role" items="${user_roles}">
                    <tr>
                      <td>${role.description}</td>
                      <c:choose>
                        <c:when test="${role.name eq 'ROLE_COMPONENT_DEVELOPER'}">
                          <td><input type="checkbox" name="roles" value="${role.id}" checked="checked" /></td>
                        </c:when>
                      </c:choose>
                    </tr>
                  </c:forEach>
                </table>
              </td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.password"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="password" id="password" name="password" class="password" style="width: 250px;" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.passwordCheck"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="password" id="password_check" name="password_check" class="password" style="width: 250px;" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.name"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="name" name="name" class="text-field" style="width: 250px;" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.email"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="email" name="email" class="text-field" style="width: 250px;" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.phone"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="phone" name="phone" class="text-field" style="width: 250px;" /></td>
            </tr>
            <tr>
              <td style="text-align: right;">
                <button type="reset" class="black">Reset</button>
              </td>
              <td style="text-align: left;">
                <button type="submit" class="black"><spring:message code="efp.client.req0.registration" /></button>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>