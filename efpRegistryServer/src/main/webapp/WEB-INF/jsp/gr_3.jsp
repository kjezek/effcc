<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

      $('.edit-button').click(function() {
        window.location = $(this).attr('id');
      });

      $('#logout-button').click(function() {
        window.location = $(this).attr('name');
      });

      $('#back').click(function() {
        window.location = $(this).attr('name');
      });

      $(".gr_line_assign").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true
      });

      var dragged_gr = null;
      $(".gr_line_assign").draggable({
        start: function() {
          dragged_gr = $(this);
        }
      });

      $(".user_strip").droppable({
        drop: function(source) {
          // call assignGrToUser method
          var url = $('#assign_url').attr('title');
          $.get(url+$(this).attr('id'), function() {
            // nothing
          });
          // mark user
          $(this).removeClass('user_strip');
          $(this).addClass('user_strip_marked');
          $(this).find('.gr_icon').show();
          $(this).find('.delete-button').show();
        }
      });

      $(".user_strip_marked").droppable({
          drop: function(source) {
            // call assignGrToUser method
            var url = $('#assign_url').attr('title');
            $.get(url+$(this).attr('id'), function() {
              // nothing
            });
            // mark user
            $(this).removeClass('user_strip');
            $(this).addClass('user_strip_marked');
            $(this).find('.gr_icon').show();
            $(this).find('.delete-button').show();
          }
        });

      $('#user_list').mousewheel(function(event, delta) {
        $("#user_list").scrollTop($("#user_list").scrollTop() - (delta*15));
      });

      $('#up').click(function() {
        $("#user_list").scrollTop($("#user_list").scrollTop() - (15));
      });

      $('#down').click(function() {
        $("#user_list").scrollTop($("#user_list").scrollTop() + (15));
      });

      $('.delete-button').click(function() {

        $.get($(this).attr('id'), function() {
          // nothing
        });

        $(this).parent().addClass('user_strip');
        $(this).parent().removeClass('user_strip_marked');
        $(this).parent().find('.gr_icon').hide();
        $(this).hide();
      });

    });
    </script>
  </head>

  <body>
    <div id="main">
      <p id="assign_url" style="display: none;" title="${pageContext.request.contextPath}/client/gr/assignGRToUser?gr_id=${gr_object.id}&user_id=" />
      <p id="remove_assign_url" style="display: none;" title="${pageContext.request.contextPath}/client/gr/removeGRToUser?gr_id=${gr_object.id}&user_id=" />
      <!-- Global register -->
      <div id="global_register_left">
        <img src="${pageContext.request.contextPath}/client/resources/graphics/gr-icon.png" width="36" height="32" alt="" title="" style="display: inline;" />
        <a href="${gr_object.id}" id="gr_register" class="gr_line_assign" onclick="return false;" style="text-decoration: none;">
          ${gr_object.name}
        </a>
        <hr />
        <p class="wizard">
          <spring:message code="efp.client.gr3.help" />
        </p>
        <p class="wizard">
          <spring:message code="efp.client.gr3.help2" />
        </p>
      </div>
      <!-- User list -->
      <div id="user_list">
        <c:forEach var="user" items="${users}">
          <c:choose>
            <c:when test="${gr_2_assigned[user.id][gr_object.id]}">
              <div class="user_strip_marked" style="display: block; cursor: default;" id="${user.id}">
              <img class="gr_icon" src="${pageContext.request.contextPath}/client/resources/graphics/gr-icon.png" width="18" height="16" alt="" title="" />
              ${user.name} (${user.login})
              <c:forEach var="r" items="${user_2_roles[user.id]}">
                <c:choose>
                  <c:when test="${r.name eq 'ROLE_ADMIN'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/admin.png" width="32" height="32" alt="admin" title="<spring:message code='efp.client.gr3.admin' />" />
                  </c:when>
                  <c:when test="${r.name eq 'ROLE_DOMAIN_EXPERT'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/expert.png" width="32" height="32" alt="domain expert" title="<spring:message code='efp.client.gr3.domain_expert' />" />
                  </c:when>
                  <c:when test="${r.name eq 'ROLE_COMPONENT_DEVELOPER'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/developer.png" width="32" height="32" alt="component developer", title="<spring:message code='efp.client.gr3.component_developer' />" />
                  </c:when>
                </c:choose>
              </c:forEach>
              <button class="delete-button" title="<spring:message code='efp.client.gr3.remove_assign' />" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/gr/removeGRToUser?gr_id=${gr_object.id}&user_id=${user.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="remove_assign" title="<spring:message code='efp.client.gr3.remove_assign' />" width="16" height="16 "/></button>
              </div>
            </c:when>
            <c:otherwise>
              <div class="user_strip" style="display: block; cursor: default;" id="${user.id}">
              <img class="gr_icon" style="display: none;" src="${pageContext.request.contextPath}/client/resources/graphics/gr-icon.png" width="18" height="16" alt="" title="" />
              ${user.name} (${user.login})
              <c:forEach var="r" items="${user_2_roles[user.id]}">
                <c:choose>
                  <c:when test="${r.name eq 'ROLE_ADMIN'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/admin.png" width="32" height="32" alt="admin" title="<spring:message code='efp.client.gr3.admin' />" />
                  </c:when>
                  <c:when test="${r.name eq 'ROLE_DOMAIN_EXPERT'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/expert.png" width="32" height="32" alt="domain expert" title="<spring:message code='efp.client.gr3.domain_expert' />" />
                  </c:when>
                  <c:when test="${r.name eq 'ROLE_COMPONENT_DEVELOPER'}">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/developer.png" width="32" height="32" alt="component developer", title="<spring:message code='efp.client.gr3.component_developer' />" />
                  </c:when>
                </c:choose>
              </c:forEach>
              <button class="delete-button" title="<spring:message code='efp.client.gr3.remove_assign' />" style="cursor: pointer; display: none;" id="${pageContext.request.contextPath}/client/gr/removeGRToUser?gr_id=${gr_object.id}&user_id=${user.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="remove_assign" title="<spring:message code='efp.client.gr3.remove_assign' />" width="16" height="16 "/></button>
              </div>
            </c:otherwise>
          </c:choose>

        </c:forEach>
      </div>
      <div style="position: absolute; top: 100px; left: 550px; cursor: pointer;">
        <img id="up" src="${pageContext.request.contextPath}/client/resources/graphics/up.png" width="50" height="50" alt="" title="" />
      </div>
      <div style="position: absolute; top: 650px; left: 550px; cursor: pointer;">
        <img id="down" src="${pageContext.request.contextPath}/client/resources/graphics/down.jpg" width="50" height="50" alt="" title="" />
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">GR</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td style="width: 500px; text-align: left;">
              <button id="back" name="${pageContext.request.contextPath}/client/${back_action}" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
              <h1 id="title_gr_name">${gr_object.name}</h1>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=gr/gr1"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
        </table>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

