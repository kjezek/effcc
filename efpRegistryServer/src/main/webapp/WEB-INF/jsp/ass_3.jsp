<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">

      var dragged_efp;
      var dragged_value;

      $(document).ready(function() {

        var options = {
          target:        '#ajaxTarget',
          beforeSubmit:  showRequest,
          success:       showResponse
        };

        $('#create_form').ajaxForm(options);

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

        $(".tree_value").draggable({
          appendTo: 'body',
          containment: 'window',
          scroll: false,
          helper: 'clone',
          opacity: .8,
          revert: true
        });

        $(".tree_value").draggable({
          start: function() {
            dragged_value = $(this);
          }
        });

        $(".tree_value").draggable({
          revert: true
        });

        $(".tree_efp").draggable({
          appendTo: 'body',
          containment: 'window',
          scroll: false,
          helper: 'clone',
          opacity: .8,
          revert: true
        });

        $(".tree_efp").draggable({
          start: function() {
            dragged_efp = $(this);
          }
        });

        $(".tree_efp").draggable({
          revert: true
        });

        $("#value_input").droppable({
          drop: function(source) {
            if(dragged_value != null) {
              $("#value_input").val(dragged_value.text());
              $("#value_id").val(dragged_value.attr('href'));

              testAssignment();
            }
          }
        });

        $("#efp_input").droppable({
          drop: function(source) {
            if(dragged_efp != null) {
              $("#efp_input").val(dragged_efp.text());
              $("#efp_id").val(dragged_efp.attr('href'));

              testAssignment();
            }
          }
        });

        var showHelp = true;
        $('#value_input').focus(function() {
          if(showHelp) {
            alert($('#help').text());
            showHelp = false;
          }
        });

        var showHelp2 = true;
        $('#efp_input').focus(function() {
          if(showHelp2) {
            alert($('#help').text());
            showHelp2 = false;
          }
        });

        $('#efp_table').mousewheel(function(event, delta) {
          $("#efp_table").scrollTop($("#efp_table").scrollTop() - (delta*15));
        });

        $('#value_table').mousewheel(function(event, delta) {
          $("#value_table").scrollTop($("#value_table").scrollTop() - (delta*15));
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id')
        });

        $('#logout-button').click(function() {
          window.location = $(this).attr('name')
        });

      });

      function showRequest(formData, jqForm, options) {
        var queryString = $.param(formData);
        return true;
      }

      function showResponse(responseText, statusText, xhr, $form)  {
        window.location = $('#create_assignment').attr('name');
      }

      function testAssignment() {
        if(dragged_efp != null && dragged_value != null) {
          $.get($('#result_target').attr('title')+$('#lr_id').val()+'&efp_id='+dragged_efp.attr('href')+'&value_id='+dragged_value.attr('href'), function(data) {
            $('#result_target').html(data);
          });
        }
      }
    </script>
  </head>
  <body>
    <div id="main">
      <div id="content_down_lr1" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <p id="help" style="display: none;">
          <spring:message code="efp.client.ass_1.help" />
        </p>
        <!-- Assign EFP into assignment -->
        <table>
        <tr><td>
        <table>
          <tr>
            <th class="inside-boxed" style="width: 350px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              EFP
            </th>
          </tr>
        </table>
        </td><td>
        <table>
          <tr>
            <th class="inside-boxed" style="width: 350px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.values" />
            </th>
          </tr>
        </table>
        </td></tr><tr><td>
        <!-- EFP -->
        <div id="efp_table" style="width: 350px;">
        <table>
          <c:forEach var="efp" items="${efp_list}">
            <tr>
              <td style="width: 200px;">
                <p style="display: inline;">
                  <a onclick="return false;" href="${efp.id}" class="tree_efp">${efp.name}</a>
                </p>
              </td>
            </tr>
          </c:forEach>
        </table>
        </div>
        </td><td>
        <!-- values -->
        <div id="value_table" style="width: 350px;">
        <table>
          <c:forEach var="value" items="${value_names}">
            <tr>
              <td style="width: 350px;">
                <p style="display: inline;">
                  <a onclick="return false;" href="${value.id}" class="tree_value">${value.name}</a>
                </p>
              </td>
            </tr>
          </c:forEach>
        </table>
        </div>
        </td></tr>
        <tr><td colspan="2">
        <!-- Assignment target -->
        <form id="create_form" action="${pageContext.request.contextPath}/client/efp/createAssignmentAjax" method="post">
        <table>
          <tr>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              LR
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              EFP
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.value" />
            </th>
          </tr>
          <tr>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="lr_input" type="text" class="text-field" value="${lr_object.name}" style="width: 250px;" />
              <input id="lr_id" name="lr_id" type="text" value="${lr_object.id}" style="display: none;" />
            </td>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="efp_input" type="text" class="text-field" value="" style="width: 250px;" />
              <input id="efp_id" name="efp_id" type="text" value="" style="display: none;" />
            </td>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="value_input" type="text" class="text-field" value="" style="width: 250px;" />
              <input id="value_id" name="value_id" type="text" value="" style="display: none;" />
            </td>
          </tr>
          <tr>
            <td id="result_target" title="${pageContext.request.contextPath}/client/efp/testAssignment?lr_id="></td>
            <td colspan="3" style="text-align: right;">
              <button id="create_assignment" name="${pageContext.request.contextPath}/client/gr/gr2?gr_id=${gr_object.id}&back_action=gr/gr1" type="submit" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.ass_1.create" />
              </button>
            </td>
          </tr>
        </table>
        </form>
        </td></tr>
        </table>
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">EFP->${efp_object.name}</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/gr/gr2?gr_id=${gr_object.id}&back_action=gr/gr1" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=efp/ass3?gr_id=${gr_object.id}&lr_id=${lr_object.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
        </table>
        <div id="ajaxTarget">
        </div>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
