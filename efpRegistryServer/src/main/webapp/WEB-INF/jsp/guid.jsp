<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#registration').click(function() {
          window.location = $(this).attr('name');
        });
      });

      function submitenter(myfield,e) {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;
        else return true;

        if (keycode == 13) {
          myfield.form.submit();
          return false;
        } else return true;
      }
    </script>
  </head>

  <body>
    <div id="main">
      <img src="${pageContext.request.contextPath}/client/resources/graphics/banner.png" height="96" width="806" alt="" title="" />
      <!-- h1 class="title2" style="width: 800px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.main.guidepost.title" /></h1>-->
      <div id="content" class="help_content" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/login-paper2.png); background-repeat: none;">
        <form name="f" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
          <fieldset class="attributes-login">
            <legend class="attributes"><spring:message code="efp.client.main.guidepost.login_operator" /></legend>
            <table border="0">
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption"><spring:message code="efp.client.main.guidepost.login" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png);"><input id="j_username" name="j_username" type="text" class="text-field" style="width: 150px;"/></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png);"><label class="caption"><spring:message code="efp.client.main.guidepost.password" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png);"><input id="j_password" name="j_password" type="password" class="text-field" style="width: 150px;" onkeypress="return submitenter(this,event)" /></td>
              </tr>
              <tr>
                <td><button id="registration" name="${pageContext.request.contextPath}/client/user/registration" type="button" class="black"><spring:message code="efp.client.guid.registrate" /></button></td>
                <td><button type="submit" class="black" title="<spring:message code='efp.client.guid.login' />">&gt;&gt;</button></td>
              </tr>
            </table>
        </fieldset>
        </form>
        <c:choose>
          <c:when test="${param.result eq 'failed'}">
            <p class=wizard style="color: red; height: 30px; padding-left: 10px;">
              <spring:message code="efp.client.guid.loginFailed" />
            </p>
          </c:when>
          <c:when test="${param.result eq 'denied'}">
            <p class=wizard style="color: red; height: 30px; padding-left: 10px;">
              <spring:message code="efp.client.guid.acessDenied" />
            </p>
          </c:when>
          <c:otherwise>
            <p class=wizard style="color: red; height: 30px;">
            </p>
          </c:otherwise>
        </c:choose>
        <div style="margin-top: 110px; height: 140px;">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/vigs79.jpg" alt="" title="" width="105" height="140" style="float: left;"/>
          <p class="news">
            <spring:message code="efp.client.guid.news1" />
          </p>
          <p class="news">
            <spring:message code="efp.client.guid.news2" />
          </p>
          <p class="news">
            <spring:message code="efp.client.guid.news3" />
          </p>
        </div>
        <hr class="news"/>
        <div>
          <img src="${pageContext.request.contextPath}/client/resources/graphics/spring-logo.jpg" alt="" title="" width="144" height="84" style="display: inline;"/>
          <img src="${pageContext.request.contextPath}/client/resources/graphics/hibernate-logo.jpg" alt="" title="" width="144" height="84" style="display: inline;"/>
          <img src="${pageContext.request.contextPath}/client/resources/graphics/ajax-logo.jpg" alt="" title="" width="144" height="84" style="display: inline;"/>
          <img src="${pageContext.request.contextPath}/client/resources/graphics/maven-logo.jpg" alt="" title="" width="144" height="84" style="display: inline;" />
        </div>
        <!--
        <fieldset class="attributes">
          <legend class="attributes"><spring:message code="efp.client.main.guidepost.anonymous" /></legend>
        <table border="0" class="centered">
          <tr>
            <td style="padding: 10px;">
              <a href="${pageContext.request.contextPath}/client/gr/gr1" title="<spring:message code='efp.client.grScreen.gr_desc' />" class="menuButtonGrey" style="width: 150px;"><spring:message code="efp.client.main.guidepost.gr" />
                <span style="display: none;">
                  <spring:message code="efp.client.main.guidepost.gr_help" />
                </span>
                <img src="${pageContext.request.contextPath}/client/resources/graphics/gr_snap.png" width="400" height="300" alt="" style="display: none;" />
              </a>
            </td>
            <td style="padding: 10px;">
              <a href="${pageContext.request.contextPath}/client/lr/lr1" title="<spring:message code='efp.client.lrScreen.lr_desc' />" class="menuButtonGrey" style="width: 150px;"><spring:message code="efp.client.main.guidepost.lr" />
                <span style="display: none;">
                  <spring:message code="efp.client.main.guidepost.lr_help" />
                </span>
                <img src="${pageContext.request.contextPath}/client/resources/graphics/lr_snap.png" width="400" height="300" alt="" style="display: none;" />
              </a>
            </td>
          </tr>
          <tr>
            <td style="padding: 10px;">
              <a href="${pageContext.request.contextPath}/client/efp/efp1" title="<spring:message code='efp.client.efpScreen.efp_desc' />" class="menuButtonGrey" style="width: 150px;"><spring:message code="efp.client.main.guidepost.efp" />
                <span style="display: none;">
                  <spring:message code="efp.client.main.guidepost.efp_help" />
                </span>
                <img src="${pageContext.request.contextPath}/client/resources/graphics/efp_snap.png" width="400" height="300" alt="" style="display: none;" />
              </a>
            </td>
            <td style="padding: 10px;">
              <a href="${pageContext.request.contextPath}/client/value/val1" class="menuButtonGrey" style="width: 150px;"><spring:message code="efp.client.main.guidepost.value" />
                <span style="display: none;">
                  <spring:message code="efp.client.main.guidepost.value_help" />
                </span>
                <img src="${pageContext.request.contextPath}/client/resources/graphics/value_snap.png" width="400" height="300" alt="" style="display: none;" />
              </a>
            </td>
          </tr>
          <tr>
            <td style="padding: 10px;">
              <a href="" class="menuButtonGrey" style="width: 150px;"><spring:message code="efp.client.main.guidepost.tree" />
                <span style="display: none;">
                  <spring:message code="efp.client.main.guidepost.tree_help" />
                </span>
                <img src="${pageContext.request.contextPath}/client/resources/graphics/tree_snap.png" width="400" height="300" alt="" style="display: none;" />
              </a>
            </td>
          </tr>
        </table>
        </fieldset>
        -->
      </div>
      <!--
      <div id="object_properties" style="border: none; top: 60px;">
        <p class="wizard">
          <spring:message code="efp.client.main.guidepost.help_login" />
        </p>
        <p class="wizard" style="margin-top: 120px;">
          <spring:message code="efp.client.main.guidepost.help_anonymous" />
        </p>
      </div>
      -->
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>