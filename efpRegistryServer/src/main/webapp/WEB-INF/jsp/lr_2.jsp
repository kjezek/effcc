<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

      var options = {
        target:        '#message_bar',
        beforeSubmit:  showRequest,
        success:       showResponse
      };

      var options2 = {
        target:        '#message_bar',
        beforeSubmit:  showRequest,
        success:       showResponse2
      };

      $('#editLrAjax').ajaxForm(options);

      //$('#newLrAjax').ajaxForm(options2);

      $('#back').click(function() {
        parent.history.back();
        //window.location = $(this).attr('name');
      });

      $(".tree_lr").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true
      });

      var dragged_lr;
      $(".tree_lr").draggable({
        start: function() {
          dragged_lr = $(this);
        }
      });

      $("#parent").droppable({
        drop: function(source) {
          $("#parent").val(dragged_lr.text());
          var splitted = dragged_lr.attr('href').split('choosen_object_id=');
          $("#parent_id").val(splitted[1]);
        }
      });

      $("#loading").ajaxStart(function(){
          $(this).show();
       }).ajaxStop(function(){
          $(this).hide();
       });

      var selectedLr;
      var selectedLrName;

      $(".tree_lr").click( function(){
        selectedLr = $(this).attr('href').substring($(this).attr('href').lastIndexOf('=')+1, $(this).attr('href').length);
        selectedLrName = $(this).text();
        //send request
        $.get($(this).attr("href"), function( data ){
          //callback function
          $("#target_lr").replaceWith(data);
          var nav_content = $('.schema_nav').text();
          if(nav_content.lastIndexOf('LR->') != -1) {
            nav_content = nav_content.substring(0, nav_content.lastIndexOf('->'));
          }
          nav_content = nav_content+'->'+selectedLrName;
          $('.schema_nav').text(nav_content);
        });
        //stop
        return false;
      });

      $('#delete').click(function() {
          if(selectedLr != null && selectedLr != '') {
            $.get($(this).attr('name')+selectedLr, function(data) {
              $('#message_bar').html(data);
              location.reload();
            });
          } else {
            alert($('#no_selection').text());
          }
        });

        $('#next').click(function() {
          if(selectedLr != null && selectedLr != '') {
            window.location = $(this).attr('name')+selectedLr;
          } else {
            alert($('#no_selection').text());
          }
        });

        $('#new_lr').click(function() {
          $('#editGrAjax').hide();
          $('#newLrAjax').show();
          $(this).hide();
          $('#edit_gr').show();
        });

        $('#edit_gr').click(function() {
          $('#newLrAjax').hide();
          $('#editGrAjax').show();
          $(this).hide();
          $('#new_lr').show();
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id')
        });

        $('#logout-button').click(function() {
          window.location = $(this).attr('name')
        });

        $('#edit_lr').click(function() {
          $('#editLrAjax').toggle();
          $('#edit_help').toggle();
        });

        $('#content_down').mousewheel(function(event, delta) {
          $("#content_down").scrollTop($("#content_down").scrollTop() - (delta*15));
        });

        $('.gr_line').hover(function() {

          $('.like_input_id_sel').removeClass('like_input_id_sel').addClass('like_input_id');
          $('.like_input_normal_sel').removeClass('like_input_normal_sel').addClass('like_input_normal');

          $(this).find('.like_input_id').removeClass('like_input_id').addClass('like_input_id_sel');
          $(this).find('.like_input_normal').removeClass('like_input_normal').addClass('like_input_normal_sel');
        });

        $('#parent').click(function() {
          $('#parent_combo').toggle();
        });

        $('.gr_black').click(function() {
          $('#parent').val($(this).text());
          $('#parent_id').val($(this).attr('name'));
          $('#parent_combo').hide('slow');
        });

        $('#parent_combo').mousewheel(function(event, delta) {
          $("#parent_combo").scrollTop($("#parent_combo").scrollTop() - (delta*15));
        });

        $('.delete-button').click(function() {
          var returnValue = window.confirm($('#confirm_delete').text());
          if(returnValue == true) {
            $.get($(this).attr('id'), function(data) {

              location.reload();
            });
          }
        });
    });

    function showRequest(formData, jqForm, options) {
      var queryString = $.param(formData);
      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      $('#message_bar').html(responseText);
      $('#message_bar').fadeIn('slow');
    }

    function showResponse2(responseText, statusText, xhr, $form)  {
      $('#message_bar').html(responseText);
      $('#message_bar').fadeIn('slow');
      location.reload();
    }
    </script>
  </head>

  <body>
    <div id="main">
      <p id="confirm_delete" style="display: none;"><spring:message code="efp.client.lr_2.confirm_delete" /></p>
      <div id="loading" style="background-image:url('${pageContext.request.contextPath}/client/resources/graphics/transparentbg.png')">
        <p><img src="${pageContext.request.contextPath}/client/resources/graphics/ajax-loader.gif"/><br /><spring:message code="efp.client.main.loading" /></p>
      </div>
      <div id="window_small_2" style="background-color: #FFFFFF; border: none;">
        <p id="edit_help" class="wizard">
          <spring:message code="efp.client.lr_2.edit_lr.help" />
        </p>
        <form id="editLrAjax" action="${pageContext.request.contextPath}/client/lr/editLrFormAjax" method="post" style="display: none;">
          <fieldset class="attributes">
            <legend class="attributes"><spring:message code="efp.client.lrScreen.edit" /></legend>
            <table border="0">
              <tr class="inside-boxed">
                <td style="display: none;"><input id="gr_id" name="gr_id" type="hidden" value="${gr_object.id}" /></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">id</label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="id" name="id" type="text" class="disabled" value="${lr_object.id}" onfocus="disabled=true;" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" value="${lr_object.name}" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.gr_2.parent" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="parent" name="parent" type="text" class="text-field" value="${lr_object.parent.name}" /></td>
                <td><input id="parent_id" name="parent_id" type="hidden" value="${lr_object.parent.id}" /></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right;"><button type="submit" class="black" style="margin-top: 5px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
              </tr>
          </table>
          </fieldset>
        </form>
        <div id="parent_combo" style="display: none;">
          <a class="gr_black" name="">&nbsp;</a>  <!-- empty row for setting ROOT -->
          <c:forEach var="lr" items="${lr_entities}">
            <a class="gr_black" name="${lr.id}">${lr.name}</a>
          </c:forEach>
        </div>
      </div>
      <div id="content_down" style=" border: none; background-repeat: no-repeat; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid-buttons.gif); overflow: hidden;">

        <table class="centered" style="background-color: #FFFFFF; margin-top: 20px; padding: 10px;">
          <tr>
            <td colspan="2">
              <h2 id="title_gr_name"><spring:message code="efp.client.lr_2.lr" />: ${lr_object.name}</h2>
            </td>
          </tr>
          <tr>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              EFP
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.value" />
            </th>
          </tr>
          <c:set var="efp" value="" />
          <c:forEach var="assignment" items="${assignments}">
          <tr class="gr_line">
            <c:choose>
              <c:when test="${efp == '' || (efp != '' && efp != assignment.efp.id)}">
                <td class="like_input_normal" style="width: 300px; border: none; border-top: 1px solid #000000;">${assignment.efp.name}</td>
                <td class="like_input_normal" style="width: 300px; border: none; border-top: 1px solid #000000;">${assignment.valueName.name}</td>
              </c:when>
              <c:otherwise>
                <td></td>
                <td class="like_input_normal" style="width: 300px; border: none;">${assignment.valueName.name}</td>
              </c:otherwise>
            </c:choose>
            <c:if test="${!is_component_developer}">
              <td class="delete-button" id="${pageContext.request.contextPath}/client/efp/deleteAssignmentAjax?assignment_id=${assignment.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="<spring:message code='efp.client.lr_2.delete_help' />" width="16" height="16 "/></td>
            </c:if>
            <c:set var="efp" value="${assignment.efp.id}" />
          </tr>
          </c:forEach>
        </table>

        <div id="target_lr" style="display: none;">
        </div>

        <p id="no_selection" style="display: none;"><spring:message code="efp.client.gr_1.no_selection" /></p>

      </div>
      <div id="object_properties_small">
        <p class="wizard">
          <spring:message code="efp.client.lr_2.help" />
        </p>
        <button id="edit_lr" class="black"><spring:message code="efp.client.lr_2.edit_lr" /></button>
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">GR->${gr_object.name}.LR->${lr_object.name}</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/${back_action}" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=lr/lr2?lr_id=${lr_object.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
          <tr>
            <td>
              <p id="message_bar" class="wizard" style="color: green;"></p>
            </td>
          </tr>
        </table>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

