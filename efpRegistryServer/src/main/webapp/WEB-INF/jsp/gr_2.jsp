<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

      var options = {
        target:        '#message_bar',
        beforeSubmit:  showRequest,
        success:       showResponse
      };

      var options2 = {
        target:        '#message_bar',
        beforeSubmit:  showRequest,
        success:       showResponse2
      };

      $('#editGrAjax').ajaxForm(options);

      $('#newLrAjax').ajaxForm(options2);

      $('#back').click(function() {
        window.location = $(this).attr('name');
      });

      $(".tree_lr").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true
      });

      var dragged_lr;
      $(".tree_lr").draggable({
        start: function() {
          dragged_lr = $(this);
        }
      });

      $("#parent").droppable({
        drop: function(source) {
          $("#parent").val(dragged_lr.text());
          var splitted = dragged_lr.attr('href').split('choosen_object_id=');
          $("#parent_id").val(splitted[1]);
        }
      });

      $("#loading").ajaxStart(function(){
          $(this).show();
       }).ajaxStop(function(){
          $(this).hide();
       });

      var selectedLr;
      var selectedLrName;

      $(".tree_lr").click( function(){
        selectedLr = $(this).attr('href').substring($(this).attr('href').lastIndexOf('=')+1, $(this).attr('href').length);
        selectedLrName = $(this).text();
        //send request
        $.get($(this).attr("href"), function( data ){
          //callback function
          //replaceWith
          $("#target_lr").replaceWith(data);
          $("#target_lr").css('overflow-y', 'hidden');

          $('#target_lr').mousewheel(function(event, delta) {
            $("#target_lr").scrollTop($("#target_lr").scrollTop() - (delta*15));
          });

          var nav_content = $('.schema_nav').text();
          if(nav_content.lastIndexOf('LR->') != -1) {
            nav_content = nav_content.substring(0, nav_content.lastIndexOf('->'));
          }
          nav_content = nav_content+'->'+selectedLrName;
          $('.schema_nav').text(nav_content);

          $('#next').show();
          $('#assignment').show();
        });
        //stop
        return false;
      });

        $('.delete').click(function() {

          var url = $(this).attr('href');
          var splitted = $(this).attr('href').split('lr_id=');
          var lr_id = splitted[1];

          if(lr_id != null && lr_id != '') {

            //test LR allowed
            $.get($('#delete_test').attr('title')+lr_id, function(res) {
              if(res != 'denied delete') {
                $.get(url, function(data) {
                  $('#message_bar').html(data);
                  //delay(1000);
                  location.reload();
                });
              } else {
                // Lr is part of LrAssignment, cannot delete it
                alert($('#delete_test_denied').text());
              }
            });
          } else {
            alert($('#no_selection').text());
          }
        });

        $('#next').click(function() {
          if(selectedLr != null && selectedLr != '') {
            window.location = $(this).attr('name')+selectedLr;
          } else {
            alert($('#no_selection').text());
          }
        });

        $('#assignment').click(function() {
          if(selectedLr != null && selectedLr != '') {
            window.location = $(this).attr('name')+selectedLr;
          } else {
            alert($('#no_selection').text());
          }
        });

        $('#new_lr').click(function() {
          $('#editGrAjax').hide();
          $('#newLrAjax').show();
          $(this).hide();
          $('#edit_gr').show();
        });

        $('#edit_gr').click(function() {
          $('#newLrAjax').hide();
          $('#editGrAjax').show();
          $(this).hide();
          $('#new_lr').show();
        });

        var show_help = true;
        $('#parent').focus(function() {
          if(show_help) {
            alert($('#parent_help').text());
            show_help = false;
          }
        });

        $('#parent').change(function() {
          if($(this).text() == '') {
            $("#parent_id").val('');
          }
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id');
        });

        $('#logout-button').click(function() {
          window.location = $(this).attr('name');
        });

        $('#content_down').mousewheel(function(event, delta) {
          $("#content_down").scrollTop($("#content_down").scrollTop() - (delta*15));
        });

        $('#create_efp').click(function() {
          window.location = $(this).attr('name');
        });

        $('#create_value').click(function() {
          window.location = $(this).attr('name');
        });

        $('#gr_select').click(function() {
          $('#gr_combo').toggle('slow');
        });

        $(document).keypress(function(e) {
          if (e.keyCode == 27) { $('#gr_combo').hide(); }
        });

        $('.gr_black').click(function() {
          $('#gr_select').val($(this).text());
          $('#gr_combo').hide('slow');

          window.location = $('#gr_select_refresh').attr('title')+$(this).attr('name');
        });

        $('#gr_combo').mousewheel(function(event, delta) {
          $("#gr_combo").scrollTop($("#gr_combo").scrollTop() - (delta*15));
        });

        $('#target_lr').mousewheel(function(event, delta) {
          $("#target_lr").scrollTop($("#target_lr").scrollTop() - (delta*15));
        });

        $('#assign_gr').click(function() {
          window.location = $('#assign_gr_url').attr('title');
        });
    });

    function showRequest(formData, jqForm, options) {
      var queryString = $.param(formData);
      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      $('#message_bar').html(responseText);
      $('#messageBar').fadeIn('slow');
    }

    function showResponse2(responseText, statusText, xhr, $form)  {
      $('#newLrAjax').resetForm();
      $('#message_bar').html(responseText);
      $('#message_bar').fadeIn('slow');
      location.reload();
    }
    </script>
  </head>

  <body>
    <div id="main">
      <p id="assign_gr_url" title="${pageContext.request.contextPath}/client/gr/gr3?back_action=gr/gr2?gr_id=${gr_object.id}&gr_id=${gr_object.id}" style="display: none;"/>
      <p id="delete_test" title="${pageContext.request.contextPath}/client/lr/deleteLrAllowed?lr_id=" style="display: none;"/>
      <p id="delete_test_denied" style="display: none;"><spring:message code="efp.client.gr_2.delete_lr_denied" /></p>
      <p id="gr_select_refresh" title="${pageContext.request.contextPath}/client/gr/gr2?back_action=gr/gr1&gr_id=" />
      <div id="loading" style="background-image:url('${pageContext.request.contextPath}/client/resources/graphics/transparentbg.png')">
        <p><img src="${pageContext.request.contextPath}/client/resources/graphics/ajax-loader.gif"/><br /><spring:message code="efp.client.main.loading" /></p>
      </div>
      <c:if test="${!is_component_developer}">
      <div id="window_small_2" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png);">
        <form id="editGrAjax" action="${pageContext.request.contextPath}/client/gr/editGrFormAjax" method="post" style="display: none;">
          <fieldset class="attributes">
            <legend class="attributes"><spring:message code="efp.client.grScreen.edit" /></legend>
            <table border="0">
              <tr class="inside-boxed">
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">id</label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="gr_id" name="gr_id" type="text" class="disabled" value="${gr_object.id}" onfocus="disabled=true;" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" value="${gr_object.name}" /></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right;"><button type="submit" class="black" style="margin-top: 5px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
              </tr>
          </table>
          </fieldset>
        </form>
        <form id="newLrAjax" action="${pageContext.request.contextPath}/client/lr/newLrFormAjax" method="post">
          <fieldset class="attributes">
            <legend class="attributes"><spring:message code="efp.client.lrScreen.new" /></legend>
            <table border="0">
              <tr class="inside-boxed">
                <td style="display: none;"><input id="gr_id" name="gr_id" type="hidden" value="${gr_object.id}" /></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">id</label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="id" name="id" type="text" class="disabled" value="" onfocus="disabled=true;" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" value="" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.gr_2.parent" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="parent" name="parent" type="text" class="text-field" value="" /></td>
                <td><input id="parent_id" name="parent_id" type="hidden" value="" /></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right;"><button type="submit" class="black" style="margin-top: 5px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
              </tr>
          </table>
          </fieldset>
        </form>
      </div>
      </c:if>
      <c:if test="${is_component_developer}">
        <div id="window_small_2" style="background-color: #FFFFFF;">
          <p class="wizard">
            <spring:message code="efp.client.gr_2.component.developer.help" />
          </p>
        </div>
      </c:if>
      <div id="content_down" style="overflow-y: hidden; border: none; background-repeat: no-repeat; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid-buttons2.gif);">
        <table style="padding-top: 10px; padding-left: 20px;">
          <c:forEach var="entity" items="${entityTree.entities}">
            <c:if test="${entity.type.name == 'lr'}">
              <tr>
                <td>
                  <p style="display: inline;">
                    <c:forEach var="i" begin="2" end="${entity.level}">
                      &nbsp;&nbsp;&nbsp;&nbsp;
                    </c:forEach>
                    <c:if test="${entity.lrEntity.parent != null}">
                      <img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" />
                    </c:if>
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/LrIcon.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" />&nbsp;
                    <a href="${pageContext.request.contextPath}/client/lr/lrObjectGr2?gr_id=${gr_object.id}&choosen_object_id=${entity.lrEntity.id}" class="tree_lr">${entity.lrEntity.name}</a>
                    <c:if test="${!is_component_developer}">
                      <a class="delete" onclick="return false;" href="${pageContext.request.contextPath}/client/lr/deleteLrAjax?lr_id=${entity.lrEntity.id}" title="<spring:message code='efp.client.lr.delete' />">
                        <img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="" width="16" height="16 "/>
                      </a>
                    </c:if>
                  </p>
                </td>
              </tr>
            </c:if>
          </c:forEach>
        </table>

        <div id="target_lr" style="overflow-y: hidden;">
          <c:choose>
            <c:when test="${lr_list_size == '0'}">
              <p style="padding: 10px; font-family: verdana;"><spring:message code="efp.client.gr2.properties.empty.help" /></p>
            </c:when>
            <c:otherwise>
              <p style="padding: 10px; font-family: verdana;"><spring:message code="efp.client.gr2.properties.help" /></p>
            </c:otherwise>
          </c:choose>
        </div>

        <c:choose>
          <c:when test="${is_component_developer}">

          </c:when>
          <c:otherwise>
            <div id="efp_bar" style="border: none;">
              <table>
                <tr>
                  <td>
                    <button id="create_efp" class="menuButtonGrey" style="cursor: pointer; width: 120px; position: relative; left: 60px;" name="${pageContext.request.contextPath}/client/efp/efp0?gr_id=${gr_object.id}"><spring:message code="efp.client.gr2.create_efp" /></button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button id="create_value" class="menuButtonGrey" style="cursor: pointer; width: 120px; position: relative; left: 50px; top: 1px;" name="${pageContext.request.contextPath}/client/value/val0?gr_id=${gr_object.id}"><spring:message code="efp.client.gr2.create_value" /></button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button id="assignment" class="menuButtonGrey" style="cursor: pointer; width: 120px; position: relative; left: 30px; top: 1px; display: none;" name="${pageContext.request.contextPath}/client/efp/ass3?back_action=gr/gr2?gr_id=${gr_object.id}&gr_id=${gr_object.id}&lr_id=" type="button"><spring:message code="efp.client.gr_2.assignment" /></button>
                  </td>
                </tr>
                <c:if test="${lr_list_size != '0'}">
                <tr>
                  <td>
                    <button id="next" class="menuButtonGrey"  style="cursor: pointer; width: 120px; display: none;" name="${pageContext.request.contextPath}/client/lr/lr2?back_action=gr/gr2?gr_id=${gr_object.id}&lr_id=" type="button"><spring:message code="efp.client.gr_2.next_lr_1" /></button>
                  </td>
                </tr>
                </c:if>
                <tr>
                  <td>
                    <img id="assign_gr" style="position: absolute; top: -100px; left: 60px; cursor: pointer;" src="${pageContext.request.contextPath}/client/resources/graphics/assign_gr.gif" title="<spring:message code='efp.client.gr_2.assign_gr' />" alt="" width="60", height="60" />
                  </td>
                </tr>
              </table>
              <p id="no_selection" style="display: none;"><spring:message code="efp.client.gr_2.no_selection" /></p>
            </div>
          </c:otherwise>
        </c:choose>
        <p id="no_selection" style="display: none;"><spring:message code="efp.client.gr_1.no_selection" /></p>
      </div>
      <c:if test="${!is_component_developer}">
      <div id="object_properties_small">
        <p class="wizard">
          <spring:message code="efp.client.gr_2.edit_help" />
        </p>
        <p class="wizard">
          <spring:message code="efp.client.gr_2.new_help" />
        </p>
        <p id="parent_help" class="wizard" style="display: none;">
          <spring:message code="efp.client.gr_2.parent_help" />
        </p>
        <button id="new_lr" name="" type="button" class="black" style="display: none;"><spring:message code="efp.client.gr_2.new_lr" /></button>
        <button id="edit_gr" name="" type="button" class="black"><spring:message code="efp.client.gr_2.edit_gr" /></button>
      </div>
      </c:if>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">GR->${gr_object.name}.LR</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/${back_action}" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=gr/gr2?gr_id=${gr_object.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
          <tr>
            <td style="width: 500px; height: 40px;">
              <input type="text" id="gr_select" value="${gr_object.name}" style="border: 1px solid black; font-family: verdana; font-weight: bold; width: 250px; font-size: medium; cursor: pointer;"/>
              <div id="gr_combo" style="display: none;">
                <c:forEach var="gr" items="${grlist}">
                  <a class="gr_black" name="${gr.id}">${gr.name}</a>
                </c:forEach>
              </div>
            </td>
            <td>
              <p id="message_bar" class="wizard" style="color: green;"></p>
            </td>
          </tr>
        </table>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

