<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">

    var dragged_efp = null;
    var dragged_value = null;
     // prepare the form when the DOM is ready
     $(document).ready(function() {

       var optionsEfp = {
         //target:        '#efp_table_new',   // target element(s) to be updated with server response
         beforeSubmit:  showRequestEfp,  // pre-submit callback
         success:       showResponseEfp  // post-submit callback
       };

       var optionsValue = {
         //target:        '#value_table_new',   // target element(s) to be updated with server response
         beforeSubmit:  showRequestValue,  // pre-submit callback
         success:       showResponseValue  // post-submit callback
       };

       var optionsAssignment = {
         target:        '#assignment_target_table',   // target element(s) to be updated with server response
         beforeSubmit:  showRequestAssignment,  // pre-submit callback
         success:       showResponseAssignment  // post-submit callback
       };

       // bind form using 'ajaxForm'
       $('#new_efp_ajax_form').ajaxForm(optionsEfp);
       $('#new_value_ajax_form').ajaxForm(optionsValue);
       $('#assignment_ajax_form').ajaxForm(optionsAssignment);

      $('#scrollbar1').tinyscrollbar();
      $('#scrollbar2').tinyscrollbar();

      $('#parent_box').click(function() {
          $.get($(this).attr('name')+$(this).attr('checked'), function(data) {
            $('#assignment_target_table').html(data);
          });
        });

      $('.ref').hover(function() {
        $(this).addClass('ref-hover');
      }, function() {
        $(this).removeClass('ref-hover');
      });

      $('#new_efp').click(function() {
        $('.ref').toggle();
          //$('#scrollbar1').hide();
          $('#new_efp_ajax_form').toggle();
          $('#scrollbar2').toggle();
      });

      $('#efp_back').click(function() {
        $('#new_efp_ajax_form').toggle();
        $('#scrollbar2').toggle();
        $('.ref').toggle();
      });

      $('#new_value').click(function() {
          $('.ref').toggle();
          $('#scrollbar1').toggle();
          //$('#scrollbar2').hide();
          $('#new_value_ajax_form').toggle();
        });

      $('#value_back').click(function() {
        $('#new_value_ajax_form').toggle();
        $('#scrollbar1').toggle();
        $('.ref').toggle();
      });

      //var dragged_efp = null;
      //var dragged_value = null;

      $(".drag_efp").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true

      });

      $(".drag_efp").draggable({
        start: function() {
          dragged_efp = $(this);
        }
      });

      $(".drag_efp").draggable({
        revert: true
      });

      $(".drag_value").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true
      });

      $(".drag_value").draggable({
        start: function() {
          dragged_value = $(this);
        }
      });

      $(".drag_value").draggable({
        revert: true
      });

      $("#trash").droppable({
        drop: function(source) {
           if(dragged_efp != null) {
             $("#target_efp").val(dragged_efp.text());
             $("#efp_id").val(dragged_efp.attr('href'));
           }
           if(dragged_value != null) {
             $("#target_value").val(dragged_value.text());
             $("#value_id").val(dragged_value.attr('href'));
           }

           // test assignment
           $.get($('#result_target').attr('title')+$('#efp_id').val()+'&value_id='+$('#value_id').val(), function(data) {
             $('#result_target').html(data);
           });

        }

      });

        $('#assignment_submit').click(function() {
          $('#assignment_ajax_form').submit();
        });

      });

     function showRequestEfp(formData, jqForm, options) {
         var queryString = $.param(formData);
         return true;
     }

     function showResponseEfp(responseText, statusText, xhr, $form)  {
         $('#new_efp_ajax_form').resetForm();
         dragged_efp = null;
         dragged_value = null;
         $('#efp_table_new').fadeIn('slow');
         location.reload();
     }

     function showRequestValue(formData, jqForm, options) {
         var queryString = $.param(formData);
         return true;
     }

     function showResponseValue(responseText, statusText, xhr, $form)  {
         $('#new_value_ajax_form').resetForm();
         dragged_efp = null;
         dragged_value = null;
         $('#value_table_new').fadeIn('slow');
         location.reload();
     }

     function showRequestAssignment(formData, jqForm, options) {
         var queryString = $.param(formData);
         return true;
     }

     function showResponseAssignment(responseText, statusText, xhr, $form)  {
         $('#assignment_target_table').fadeIn('slow');
         $('#target_efp').val('');
         $('#target_value').val('');
         $('#efp_id').val('');
         $('#value_id').val('');
     }
    </script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <table border="0">
          <tr>
            <td>
              <div id="scrollbar1">
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                  <div class="viewport">
                    <div class="overview">
                      <h5 class="grScreen" style="padding-bottom: 5px;"><spring:message code="efp.client.efpScreen.chooseEfp" /></h5>
                      <table id="efp_table" border="0">
                        <tr id="efp_table_new">
                        </tr>
                        <c:forEach var="efp" items="${efp_entities}">
                        <tr>
                          <td>
                            <a href="${efp.id}" class="drag_efp" onclick="return false;" style="border: 1px solid black; margin: 1px;">${efp.name}</a>
                          </td>
                        </tr>
                      </c:forEach>
                    </table>
                  </div>
                </div>
              </div>
              <span id="new_efp" class="ref"><spring:message code="efp.client.efpScreen.newEFP" /></span>
            </td>
            <td>
              <form id="new_efp_ajax_form" action="${pageContext.request.contextPath}/client/efp/newEFPAjax" method="post" style="display: none;">
              <table border="0" class="attributes">
                <tr>
                  <th colspan="2" style="width: 200px; height: 80px;"><spring:message code="efp.client.efpScreen.newEFP" /></th>
                </tr>
                <tr>
                  <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">Id:</label></td>
                  <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="id" name="id" type="text" class="disabled" onfocus="disabled=true;" /></td>
                </tr>
                <tr>
                  <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.name"/></label></td>
                  <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
                </tr>
                <tr>
                  <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.gamma"/></label></td>
                  <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="gamma" name="gamma" type="text" class="text-field" /></td>
                </tr>
                <tr>
                  <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.unit"/></label></td>
                  <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="unit" name="unit" type="text" class="text-field" /></td>
                </tr>
                <tr>
                  <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.valueType"/></label></td>
                  <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="valueType" name="valueType" type="text" class="text-field" /></td>
                </tr>
                <tr>
                  <td><button id="efp_back" type="button" class="black" style="margin-top: 30px;"><spring:message code="efp.client.assignmentScreen.back" /></button></td>
                  <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
                  <td><input id="gr_id" name="gr_id" type="hidden" value="${choosen_gr}" /></td>
                </tr>
              </table>
              </form>
              <form id="new_value_ajax_form" action="${pageContext.request.contextPath}/client/efp/newValueAJAX" method="post" style="display: none;">
                <table border="0" class="attributes">
                  <tr>
                    <th colspan="2" style="height: 60px;"><spring:message code="efp.client.efpScreen.new" /></th>
                  </tr>
                  <tr>
                    <td class="inside-boxed" style="width: 80px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.name"/></label></td>
                    <td class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
                  </tr>
                  <tr>
                    <td>
                      <input type="hidden" id="gr_id" name="gr_id" value="${choosen_gr}" />
                      <button id="value_back" type="button" class="black" style="margin-top: 30px;"><spring:message code="efp.client.assignmentScreen.back" /></button>
                    </td>
                    <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.efpScreen.newValue.confirmForm" /></button></td>
                  </tr>
                </table>
              </form>
            </td>
            <td>
              <div id="scrollbar2">
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                  <div class="viewport">
                    <div class="overview">
                      <h5 class="grScreen" style="padding-bottom: 5px;"><spring:message code="efp.client.valueScreen.chooseValue" /></h5>
                      <table id="value_table" border="0">
                      <tr id="value_table_new">
                      </tr>
                      <c:forEach var="value" items="${values}">
                        <tr>
                          <td>
                            <a href="${value.id}" onclick="return false;" class="drag_value" style="border: 1px solid black; margin: 1px;">${value.name}</a>
                          </td>
                        </tr>
                      </c:forEach>
                    </table>
                  </div>
                </div>
              </div>
              <span id="new_value" class="ref"><spring:message code="efp.client.valueScreen.newValue" /></span>
            </td>
          </tr>
        </table>
        <hr />
        <div style="text-align: center; width: 650px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
          <span class="box"><spring:message code="efp.client.assignmentScreen.assignment" /></span>
        </div>
        <div id="trash">
          <form id="assignment_ajax_form" action="${pageContext.request.contextPath}/client/efp/newLrAssignment" method="post">
          <table border="0" class="attributes">
            <tr>
              <th style="width: 200px;"><h5 class="grScreen">Lr</h5></th>
              <th style="width: 200px;"><h5 class="grScreen">Efp</h5></th>
              <th style="width: 200px;"><h5 class="grScreen"><spring:message code="efp.client.valueScreen.value" /></h5></th>
            </tr>
            <tr>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="lr_name" name="lr_name" type="text" value="${lr_object.name}" /></td>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="target_efp" name="efp_name" type="text" value="" /></td>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="target_value" name="value_name" type="text" value="" /></td>
            </tr>
            <tr>
              <td><input id="lr_id" name="lr_id" type="hidden" value="${lr_object.id}" /></td>
              <td><input id="efp_id" name="efp_id" type="hidden" value="" /></td>
              <td><input id="value_id" name="value_id" type="hidden" value="" /></td>
            </tr>
            <tr>
              <td></td>
              <td><span id="assignment_submit" class="ref"><spring:message code="efp.client.assignmentScreen.new_assignment" /></span></td>
              <td id="result_target" title="${pageContext.request.contextPath}/client/efp/testAssignment?lr_id=${lr_object.id}&efp_id="></td>
            </tr>
          </table>
          </form>
        </div>
        <div style="text-align: center; width: 650px;">
          <span class="box"><spring:message code="efp.client.assignmentScreen.show_parent" /></span>
          <input id="parent_box" name="${pageContext.request.contextPath}/client/efp/getAssignments?lr_id=${lr_object.id}&parent=" type="checkbox" checked="checked" />
        </div>
        <table id="assignment_target_table" border="1" class="attributes">
          <c:forEach var="assignment" items="${assignments}">
            <tr>
              <td style="width: 200px; text-align: left;">${assignment.lr.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.efp.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.valueName.name}</td>
            </tr>
          </c:forEach>
        </table>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.lrScreen.chooseGr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectGrAssignmentScreen" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.lrScreen.chooseLr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectLrAssignmentScreen" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="lr_select" name="lr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="lr" items="${lrlist}">
                <c:choose>
                  <c:when test="${choosen_lr == lr.id}">
                    <option selected="selected" value="${lr.id}">${lr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${lr.id}">${lr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.efpScreen.efp" /></h3>
        <h5 class="grScreen"><spring:message code="efp.client.assignmentScreen.assignment" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.assignmentScreen.assignment_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <h3><spring:message code="efp.client.assignmentScreen.new_assignment" /></h3>
        <p class="wizard">
          <spring:message code="efp.client.assignmentScreen.new_assignment.help" />
        </p>
        <h3><spring:message code="efp.client.assignmentScreen.assignment_list" /></h3>
        <p class="wizard">
          <spring:message code="efp.client.assignmentScreen.assignment_list.help" />
        </p>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

