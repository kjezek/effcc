<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        $(".menuButtonGrey").mouseover(function() {
          var source = $(this).find('span');
          var image = $(this).find('img');
          $("#content").toggle();
          $("#help").text(source.text());
          $("#help").toggle();
          $("#help_snap").find('img').attr('src',(image.attr('src')));
          $("#help_snap").toggle();
        });

        $(".menuButtonGrey").mouseout(function() {
            $("#help").toggle();
            $("#help_snap").toggle();
            $("#content").toggle();
          });
      });
    </script>
  </head>

  <body>
    <div id="main">
      <h1 class="title2" style="width: 800px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.main.guidepost.title" /></h1>
      <div id="content" class="help_content">
        <p>
          <spring:message code="efp.client.main.guidepost.content1" />
        </p>
        <p>
          <spring:message code="efp.client.main.guidepost.content2" />
        </p>
        <p>
          <spring:message code="efp.client.main.guidepost.content3" />
        </p>
      </div>
      <table border="0">
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/gr/wizardGr" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.wizard" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.wizard_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/tree_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/gr/grScreen?gr_id=" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.gr" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.gr_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/gr_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/lr/lrScreen?gr_id=&lr_id=" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.lr" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.lr_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/lr_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/efp/efpScreen?gr_id=&efp_id=" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.efp" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.efp_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/efp_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/value/valueScreen?gr_id=&value_id=" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.value" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.value_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/value_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="${pageContext.request.contextPath}/client/main/main" class="menuButtonGrey"><spring:message code="efp.client.main.guidepost.tree" />
              <span style="display: none;">
                <spring:message code="efp.client.main.guidepost.tree_help" />
              </span>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/tree_snap.png" width="400" height="300" alt="" style="display: none;" />
            </a>
          </td>
        </tr>
      </table>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>