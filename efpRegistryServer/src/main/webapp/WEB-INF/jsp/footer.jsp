<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<table>
  <tr>
    <td>
      <a href="http://www.kiv.zcu.cz" onclick="return !bar(this.href);">
        <img src="${pageContext.request.contextPath}/client/resources/graphics/kiv-logo_cb.png" height="50" width="250" alt="kiv" />
      </a>
    </td>
    <td>
      <span class="footer">
        <a href="${pageContext.request.contextPath}/client/resources/docs/presentation-2011-04-21.pdf" onclick="return !bar(this.href);" class="footer" title="EFP?">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/pdf.jpg" height="50" width="50" alt="pdf" title="" class="footer" />
        </a>
      </span>
    </td>
    <td>
      <p class="footer2">Tomáš Kohout@2012<br />
      <spring:message code="efp.client.last.actualization" /> 12.05.2012</p>
    </td>
  </tr>
</table>
