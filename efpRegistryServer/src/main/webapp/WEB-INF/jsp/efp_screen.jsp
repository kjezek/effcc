<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
     // prepare the form when the DOM is ready
     $(document).ready(function() {
        var options = {
        target:        '#addTargetAJAX',   // target element(s) to be updated with server response
        beforeSubmit:  showRequest,  // pre-submit callback
        success:       showResponse  // post-submit callback
          };

         // bind form using 'ajaxForm'
         $('#addFormAJAX').ajaxForm(options);

      });

     // pre-submit callback (validation?)
     function showRequest(formData, jqForm, options) {
         // formData is an array; here we use $.param to convert it to a string to display it
         // but the form plugin does this for you automatically when it submits the data
         var queryString = $.param(formData);

         // jqForm is a jQuery object encapsulating the form element.  To access the
         // DOM element for the form do this:
         // var formElement = jqForm[0];

         // here we could return false to prevent the form from being submitted;
         // returning anything other than false will allow the form submit to continue
         return true;
     }

     // post-submit callback
     function showResponse(responseText, statusText, xhr, $form)  {
         // for normal html responses, the first argument to the success callback
         // is the XMLHttpRequest object's responseText property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'xml' then the first argument to the success callback
         // is the XMLHttpRequest object's responseXML property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'json' then the first argument to the success callback
         // is the json data object returned by the server

         $('#addFormAJAX').resetForm();
         $('#addTargetAJAX').fadeIn('slow');
     }
    </script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
      <c:if test="${(efp_object.id != null) and (efp_object.id != 0)}">
        <form:form modelAttribute="efp_object" action="${pageContext.request.contextPath}/client/efp/editEFPFormScreen" method="post">
        <table border="0" class="attributes">
        <tr>
          <th colspan="2" style="height: 60px;"><spring:message code="efp.client.efpScreen.content.help" /></th>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="id" class="caption">Id</form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="id" class="disabled" onfocus="disabled=true;" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="name" class="caption"><spring:message code="efp.client.efp_screen.name"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="name" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="gamma" class="caption"><spring:message code="efp.client.efp_screen.gamma"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="gamma" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="unit" class="caption"><spring:message code="efp.client.efp_screen.unit"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="unit" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="valueType" class="caption"><spring:message code="efp.client.efp_screen.valueType"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="valueType" class="text-field" /></td>
        </tr>
        <tr>
          <td></td>
          <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
        </tr>
        <tr>
          <td colspan="2">
          <h6 class="object-properties"><spring:message code="efp.client.efpScreen.values" /></h6>
            <table id="addTargetAJAX" border="0" class="centered">
              <c:forEach var="value" items="${values}">
                <tr>
                  <td></td>
                  <td class="inside-boxed" style="text-align: left; padding-left: 5px; padding-right: 5px;">
                    <a href="${pageContext.request.contextPath}/client/value/valueScreen?gr_id=${choosen_gr}&value_id=${value.id}" title="<spring:message code='efp.client.assignment.edit' />" class="object-properties">${value.name}</a>
                  </td>
                  <td>
                    <a href="" title="<spring:message code='efp.client.assingment.delete' />" class="image" onclick="confirmDelete('<spring:message code="efp.client.efpScreen.assignment.confirm_delete" />', '${pageContext.request.contextPath}/client/efp/deleteValueEfpScreen?efp_id=${choosen_efp}&value_id=${value.id}');"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="delete" title="" /></a>
                  </td>
                </tr>
              </c:forEach>
            </table>
          </td>
        </tr>
        </table>
      </form:form>
      <!-- Add value -->
      <form id="addFormAJAX" action="${pageContext.request.contextPath}/client/efp/newValueForEfpFormAJAX" method="post">
        <table border="0" class="attributes">
          <tr>
            <th colspan="2" style="height: 60px;"><spring:message code="efp.client.efpScreen.new" /></th>
          </tr>
          <tr>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.name"/></label></td>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
          </tr>
          <tr>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.lr"/></label></td>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <select id="lr_id" name="lr_id" class="text-field">
                <c:forEach var="lr" items="${lr_entities}">
                  <option value="${lr.id}">${lr.name}</option>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <input type="hidden" id="gr_id" name="gr_id" value="${choosen_gr}" />
              <input type="hidden" id="efp_id" name="efp_id" value="${choosen_efp}" />
            </td>
            <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.efpScreen.newValue.confirmForm" /></button></td>
          </tr>
        </table>
      </form>
      </c:if>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.efpScreen.chooseGr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectGrEfpScreen" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.efpScreen.chooseEfp" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectEfp" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
          <input id="gr_select" name="gr_select" type="hidden" value="${choosen_gr}"/>
            <select id="efp_id" name="efp_id" class="chooser" onchange="this.form.submit()">
              <c:forEach var="efp" items="${efp_entities}">
                <c:choose>
                  <c:when test="${choosen_efp eq efp.id}">
                    <option selected="selected" value="${efp.id}">${efp.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${efp.id}">${efp.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">${efp_object.name}</h3>
        <h5 class="grScreen"><spring:message code="efp.client.efpScreen.efp" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.efpScreen.efp_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">

        <a href="${pageContext.request.contextPath}/client/efp/newEFP?gr_id=${choosen_gr}" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.newEFP" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/efpScreen?gr_id=${choosen_gr}&efp_id=" title="" class="menuButtonSelected">
          <spring:message code="efp.client.efpScreen.updateEFP" />
        </a>
        <a href="" title="" class="menuButtonGrey" onclick="confirmDelete('<spring:message code="efp.client.efpScreen.confirm_delete" />', '${pageContext.request.contextPath}/client/efp/deleteEfp?lr_id=${choosen_lr}');">
          <spring:message code="efp.client.efpScreen.deleteEFP" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/assignmentScreen?gr_id=${choosen_gr}&lr_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.newLrAssignment" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/deleteAssignmentScreen?gr_id=${choosen_gr}&lr_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.efpScreen.deleteLrAssignment" />
        </a>
        <a href="${pageContext.request.contextPath}/client/main/tree?gr_id=${gr_object.id}&choosen_object_type=&choosen_object_id=&packed_list=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.grScreen.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

