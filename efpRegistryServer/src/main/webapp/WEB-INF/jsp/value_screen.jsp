<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
      <c:if test="${(value_object.valueNameEntity.id != null) and (value_object.valueNameEntity.id != 0)}">
        <form:form modelAttribute="value_object" action="${pageContext.request.contextPath}/client/value/editValueFormScreen" method="post">
        <table border="0" class="attributes">
        <tr>
          <th colspan="2" style="height: 60px;"><spring:message code="efp.client.valueScreen.content.help" /></th>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="valueNameEntity.id" class="caption">Id</form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="valueNameEntity.id" class="disabled" onfocus="disabled=true;" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="valueNameEntity.name" class="caption"><spring:message code="efp.client.value_screen.name"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="valueNameEntity.name" class="text-field" /></td>
        </tr>
        <tr>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="grId" class="caption"><spring:message code="efp.client.value_screen.gr"/></form:label></td>
          <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
            <form:select path="grId" class="text-field">
              <form:options items="${grlist}" itemValue="id" itemLabel="name" />
            </form:select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
        </tr>
        </table>
      </form:form>
      </c:if>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser_thin"><spring:message code="efp.client.efpScreen.chooseGr" /></h5>
        <form action="${pageContext.request.contextPath}/client/value/selectGrValueScreen" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
        <h5 class="chooser_thin"><spring:message code="efp.client.valueScreen.chooseValue" /></h5>
        <form action="${pageContext.request.contextPath}/client/value/selectValueScreen" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="value_select" name="value_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="value" items="${values}">
                <c:choose>
                  <c:when test="${choosen_value eq value.id}">
                    <option selected="selected" value="${value.id}">${value.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${value.id}">${value.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">${value_object.valueNameEntity.name}</h3>
        <h5 class="grScreen"><spring:message code="efp.client.valueScreen.value" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.valueScreen.value_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <a href="${pageContext.request.contextPath}/client/value/newValue?gr_id=${choosen_gr}" title="" class="menuButtonGrey">
          <spring:message code="efp.client.valueScreen.newValue" />
        </a>
        <a href="${pageContext.request.contextPath}/client/value/valueScreen?gr_id=${choosen_gr}&value_id=" title="" class="menuButtonSelected">
          <spring:message code="efp.client.valueScreen.updateValue" />
        </a>
        <a href="" title="" class="menuButtonGrey" onclick="confirmDelete('<spring:message code="efp.client.valueScreen.confirm_delete" />', '${pageContext.request.contextPath}/client/value/deleteValue?value_id=${choosen_value}');">
          <spring:message code="efp.client.valueScreen.deleteValue" />
        </a>
        <a href="${pageContext.request.contextPath}/client/main/tree?gr_id=${gr_object.id}&choosen_object_type=gr&choosen_object_id=${choosen_gr}&packed_list=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.grScreen.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

