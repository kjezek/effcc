<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
              <div class="lr_name" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/${choosenObject.backgroundImage}); background-repeat: no-repeat; background-position: 50% 50%; background-color: #D6D6D6; margin: 5px;">
                <h6 class="object-properties"><spring:message code="efp.client.assignment.efp_name" /></h6>
                <h5 class="object-properties" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png); opacity: 0.8;">${choosenObject.efpEntity.optimizedName}</h5>
                <a href="${pageContext.request.contextPath}/client/efp/editEfp?efp_id=${choosenObject.efpEntity.id}" title="<spring:message code='efp.client.assignment.edit' />" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit.png" width="16" height="16" alt="edit" title="" /></a>
                <a href="" title="<spring:message code='efp.client.assingment.delete' />" class="image" onclick="confirmDelete('<spring:message code="efp.client.assignment.confirm_delete" />', '${pageContext.request.contextPath}/client/efp/deleteEfp?efp_id=${choosenObject.efpEntity.id}');"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="delete" title="" /></a>
              </div>
              <hr class="object-properties" />
              <h6 class="object-properties"><spring:message code="efp.client.assignment.values" /></h6>
              <div class="object-properties">
                <ul>
                  <c:forEach var="assignment" items="${entityTree.childAssignments[choosenObject.lrEntity.id]}">
                    <c:if test="${assignment.efp.id eq choosenObject.efpEntity.id}">
                      <li><a href="${pageContext.request.contextPath}/client/value/valueObject?gr_id=${choosen_gr}&efp_id=${assignment.efp.id}&lr_id=${assignment.lr.id}&value_name=${assignment.valueName.name}&packed_list=${packed_list}&ajax_enabled=false" title="${assignment.preparedValue}" class="object-properties">${assignment.valueName.name}</a></li>
                    </c:if>
                  </c:forEach>
                </ul>
              </div>
              <hr class="object-properties" />
              <h6 class="object-properties"><spring:message code="efp.client.assignment.details" /></h6>
              <table class="details" border="0">
                <tr>
                  <th><spring:message code="efp.client.assignment.value" /></th>
                  <th><spring:message code="efp.client.assignment.type" /></th>
              </tr>
              <c:forEach var="assignment" items="${entityTree.childAssignments[choosenObject.lrEntity.id]}">
                <c:if test="${assignment.efp.id eq choosenObject.efpEntity.id}">
                  <tr>
                  <td>${assignment.valueName.name}</td>
                  <td width="20">${assignment.efp.preparedValueType}</td>
                  </tr>
                </c:if>
              </c:forEach>
            </table>