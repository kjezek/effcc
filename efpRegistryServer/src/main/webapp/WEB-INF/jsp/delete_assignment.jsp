<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
     // prepare the form when the DOM is ready
     $(document).ready(function() {

      $('#parent_box').click(function() {
          $.get($(this).attr('name')+$(this).attr('checked'), function(data) {
            $('#assignment_target_table').html(data);
          });
        });

      $('.ref').hover(function() {
        $(this).addClass('ref-hover');
      }, function() {
        $(this).removeClass('ref-hover');
      });

      $('.ref').click(function() {
        var splitted = $(this).attr('id').split('=');
        var answer = confirm($(this).attr('title')+' (id='+splitted[1]+')?');
        if(answer) {
          $.get($(this).attr('id'), function(data) {
            if(data.toLowerCase() == 'ok') {
              $(this).parent().parent().remove();
              location.reload();
            }
          });
        }
      });

    });
    </script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <div style="text-align: center; width: 650px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
          <span class="box"><spring:message code="efp.client.assignmentScreen.assignment" /></span>
        </div>
        <table id="assignment_target_table" border="1" class="attributes">
          <c:forEach var="assignment" items="${assignments}">
            <tr>
              <td style="width: 200px; text-align: left;">${assignment.lr.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.efp.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.valueName.name}</td>
              <td><span id="${pageContext.request.contextPath}/client/efp/deleteAssignment?assignment_id=${assignment.id}" title="<spring:message code='efp.client.deleteAssignment.deleteConfirmation' />" class="ref"><spring:message code="efp.client.deleteAssignment.delete" /></span></td>
            </tr>
          </c:forEach>
        </table>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.lrScreen.chooseGr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectGrDeleteAssignment" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
        <h5 class="chooser_thin" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.lrScreen.chooseLr" /></h5>
        <form action="${pageContext.request.contextPath}/client/efp/selectLrDeleteAssignment" method="get" style="margin-top: 2px;">
          <p style="display: inline;">
            <select id="lr_select" name="lr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="lr" items="${lrlist}">
                <c:choose>
                  <c:when test="${choosen_lr == lr.id}">
                    <option selected="selected" value="${lr.id}">${lr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${lr.id}">${lr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.efpScreen.efp" /></h3>
        <h5 class="grScreen"><spring:message code="efp.client.assignmentScreen.assignment" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.assignmentScreen.assignment_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <h3><spring:message code="efp.client.deleteAssignment.delete_assignment" /></h3>
        <p class="wizard">
          <spring:message code="efp.client.deleteAssignment.delete_assignment.help" />
        </p>
        <a href="${pageContext.request.contextPath}/client/efp/efpScreen?gr_id=${choosen_gr}&efp_id=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.deleteAssignment.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
