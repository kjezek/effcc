<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
              <div class="lr_name" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/${choosenObject.backgroundImage}); background-repeat: no-repeat; background-position: 50% 50%; background-color: #FFFFFF; margin: 5px;">
                <h5 class="object-properties">${choosenObject.lrEntity.optimizedName}</h5>
              </div>
              <!-- <hr class="object-properties" />-->
              <h6 class="object-properties">EFP <spring:message code="efp.client.val_3.assignments" />:</h6>
              <c:set var="tmp_efp" value="${null}" />
              <c:forEach var="assignment" items="${entityTree.childAssignments[choosenObject.lrEntity.id]}">
                <c:if test="${tmp_efp == null || (tmp_efp != null && !(tmp_efp eq assignment.efp.id))}">
                  <c:if test="${tmp_efp != null}">
                    <hr />
                  </c:if>
                <a href="${pageContext.request.contextPath}/client/efp/efp3?efp_id=${assignment.efp.id}" title="<spring:message code='efp.client.lr.show_efp' />" class="object-properties-efp" style="padding-left: 5px;">${assignment.efp.name}</a>
                </c:if>
                <br />&nbsp;&nbsp;&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" /><a href="${pageContext.request.contextPath}/client/value/val3?value_id=${assignment.valueName.id}" title="<spring:message code='efp.client.lr.value' />" class="object-properties-efp">${assignment.valueName.name}</a>
                <c:set var="tmp_efp" value="${assignment.efp.id}" />
              </c:forEach>