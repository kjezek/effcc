<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        var options = {
          target:        '#ajaxTarget',
          beforeSubmit:  showRequest,
          success:       showResponse
        };

        $('#create_form').ajaxForm(options);

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

        $(".tree_lr").draggable({
          appendTo: 'body',
          containment: 'window',
          scroll: false,
          helper: 'clone',
          opacity: .8,
          revert: true
        });

        $(".tree_value").draggable({
          appendTo: 'body',
          containment: 'window',
          scroll: false,
          helper: 'clone',
          opacity: .8,
          revert: true
        });

        var dragged_lr;
        $(".tree_lr").draggable({
          start: function() {
            dragged_lr = $(this);
          }
        });

        var dragged_value;
        $(".tree_value").draggable({
          start: function() {
            dragged_value = $(this);
          }
        });

        $("#lr_input").droppable({
          drop: function(source) {
            $("#lr_input").val(dragged_lr.text());
            $("#lr_id").val(dragged_lr.attr('href'));
          }
        });

        $("#value_input").droppable({
          drop: function(source) {
            $("#value_input").val(dragged_value.text());
            $("#value_id").val(dragged_value.attr('href'));
          }
        });

        var showHelp = true;
        $('#lr_input').focus(function() {
          if(showHelp) {
            alert($('#help').text());
            showHelp = false;
          }
        });

        var showHelp2 = true;
        $('#value_input').focus(function() {
          if(showHelp2) {
            alert($('#help').text());
            showHelp2 = false;
          }
        });

      });

      function showRequest(formData, jqForm, options) {
        var queryString = $.param(formData);
        return true;
      }

      function showResponse(responseText, statusText, xhr, $form)  {
        window.location = $('#create_assignment').attr('name');
      }
    </script>
  </head>
  <body>
    <div id="main">
      <div id="content_down_lr1" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <p id="help" style="display: none;">
          <spring:message code="efp.client.ass_1.help" />
        </p>
        <!-- Assign EFP into assignment -->
        <table>
        <tr><td>
        <table>
          <tr>
            <th class="inside-boxed" style="width: 350px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.lrs" />
            </th>
          </tr>
        </table>
        </td><td>
        <table>
          <tr>
            <th class="inside-boxed" style="width: 350px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.values" />
            </th>
          </tr>
        </table>
        </td></tr><tr><td>
        <!-- LR -->
        <div id="lr_table">
        <table>
          <c:forEach var="lr" items="${lr_list}">
            <tr>
              <td style="width: 350px;">
                <p style="display: inline;">
                  <a onclick="return false;" href="${lr.id}" class="tree_lr">${lr.name}</a>
                </p>
              </td>
            </tr>
          </c:forEach>
        </table>
        </div>
        </td><td>
        <!-- values -->
        <div id="value_table">
        <table>
          <c:forEach var="value" items="${value_names}">
            <tr>
              <td style="width: 350px;">
                <p style="display: inline;">
                  <a onclick="return false;" href="${value.id}" class="tree_value">${value.name}</a>
                </p>
              </td>
            </tr>
          </c:forEach>
        </table>
        </div>
        </td></tr>
        <tr><td colspan="2">
        <!-- Assignment target -->
        <form id="create_form" action="${pageContext.request.contextPath}/client/efp/createAssignmentAjax" method="post">
        <table>
          <tr>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              LR
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              EFP
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.ass_1.value" />
            </th>
          </tr>
          <tr>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="lr_input" type="text" class="text-field" value="" style="width: 250px;" />
              <input id="lr_id" name="lr_id" type="text" value="" style="display: none;" />
            </td>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="efp_input" type="text" class="text-field" value="${efp_object.name}" style="width: 250px;" />
              <input id="efp_id" name="efp_id" type="text" value="${efp_object.id}" style="display: none;" />
            </td>
            <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <input id="value_input" type="text" class="text-field" value="" style="width: 250px;" />
              <input id="value_id" name="value_id" type="text" value="" style="display: none;" />
            </td>
          </tr>
          <tr>
            <td colspan="3" style="text-align: right;">
              <button id="create_assignment" name="${pageContext.request.contextPath}/client/efp/efp2" type="submit" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.ass_1.create" />
              </button>
            </td>
          </tr>
        </table>
        </form>
        </td></tr>
        </table>
      </div>
      <div id="wizard">
        <table>
          <tr>
            <td class="schema_nav">EFP->${efp_object.name}</td>
            <c:choose>
              <c:when test="${operator != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />tokos (admin)</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/efp/efp1" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
          </tr>
        </table>
        <div id="ajaxTarget">
        </div>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
