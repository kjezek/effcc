<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
  <h3 class="wizard"><spring:message code="efp.client.wizard.wizard" /></h3>
  <p class="wizard">
    <spring:message code="efp.client.wizard.help" />
  </p>
  <a href="${pageContext.request.contextPath}/client/gr/wizardGr" class="menuButtonGrey" style="width: 200px;">
    <spring:message code="efp.client.wizard.startGR" />
  </a>
