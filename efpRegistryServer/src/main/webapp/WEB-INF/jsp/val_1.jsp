<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        var selectedValue = '';
        var selectedValueName = '';
        var selectedGr = '';
        $('.gr_line').hover(function() {
          if($(this).find('.like_input_id').text() != '') {
            selectedValue = $(this).find('.like_input_id').text();
            selectedValueName = $(this).find('.like_input_lr').text();
            selectedGr = $(this).find('.like_input_normal').attr('id');

            $('.like_input_id_sel').removeClass('like_input_id_sel').addClass('like_input_id');
            $('.like_input_lr_sel').removeClass('like_input_lr_sel').addClass('like_input_lr');
            $('.like_input_normal_sel').removeClass('like_input_normal_sel').addClass('like_input_normal');

            $(this).find('.like_input_id').removeClass('like_input_id').addClass('like_input_id_sel');
            $(this).find('.like_input_lr').removeClass('like_input_lr').addClass('like_input_lr_sel');
            $(this).find('.like_input_normal').removeClass('like_input_normal').addClass('like_input_normal_sel');

            var nav_content = $('.schema_nav').text();
            if(nav_content.indexOf('->') != -1) {
              nav_content = nav_content.substring(0, nav_content.indexOf('->'));
            }
            nav_content = nav_content+'->'+selectedValueName;
            $('.schema_nav').text(nav_content);
          }
        });

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

        $('.delete-button').click(function() {
          var returnValue = window.confirm($('#confirm_delete').text());
          if(returnValue == true) {
            $.get($(this).attr('id'), function(data) {
              window.location.reload();
            });
          }
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id');
        });

        $('#show_assignments').click(function() {
          window.location = $(this).attr('name');
        });

        $('.assign-button').click(function() {
          window.location = $(this).attr('id')+selectedGr;
        });

        $('#new').click(function() {
          window.location = $(this).attr('name');
        });

      });
    </script>
  </head>
  <body>
    <div id="main">
      <p id="confirm_delete" style="display: none;"><spring:message code="efp.client.value.confirm_delete" /></p>
      <div id="lr_buttons">
        <table class="centered">
          <tr>
            <td>
              <button id="new" name="${pageContext.request.contextPath}/client/value/val0" type="button" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.val_1.newValue" />
              </button>
            </td>
            <td>
              <button id="show_assignments" name="${pageContext.request.contextPath}/client/value/val2" type="button" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.val_1.show_assignments" />
              </button>
            </td>
          </tr>
        </table>
        <p id="no_selection" style="display: none;"><spring:message code="efp.client.val_1.no_selection" /></p>
      </div>
      <div id="content_down_lr1" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <!-- Not assigned Values -->
        <table class="centered">
          <tr>
            <th></th>
            <th colspan="3" class="inside-boxed" style="text-align: center;background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.val_1.unassignments" />
            </th>
          </tr>
          <tr>
            <th></th>
            <th class="inside-boxed" style="width: 100px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">id</th>
            <th class="inside-boxed" style="width: 350px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.val_1.name" />
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.val_1.from_gr" />
            </th>
          </tr>
          <c:forEach var="value" items="${na_value_list}">
            <tr class="gr_line">
              <td class="assign-button" title="<spring:message code='efp.client.val_1.assign' />" id="${pageContext.request.contextPath}/client/value/ass2?value_id=${value.id}&gr_id="><img src="${pageContext.request.contextPath}/client/resources/graphics/assign.jpg" alt="assign" title="" width="16" height="16 "/></td>
              <td class="like_input_id" style="width: 100px;">${value.id}</td>
              <td class="like_input_lr" style="width: 350px;">${value.name}</td>
              <td class="like_input_normal" style="width: 250px;" id="${value.gr.id}">${value.gr.name}</td>
              <td class="edit-button" id="${pageContext.request.contextPath}/client/value/val3?value_id=${value.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="detail" title="" width="16" height="16 "/></td>
              <td class="delete-button" id="${pageContext.request.contextPath}/client/value/deleteValueAjax?value_id=${value.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="" width="16" height="16 "/></td>
            </tr>
          </c:forEach>
        </table>
      </div>
      <div id="wizard">
        <table>
          <tr>
            <td class="schema_nav">VALUE</td>
            <c:choose>
              <c:when test="${operator != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />tokos (admin)</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/main/guidepost" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
          </tr>
        </table>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
