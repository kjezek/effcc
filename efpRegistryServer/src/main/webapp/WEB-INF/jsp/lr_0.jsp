<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

      var options = {
        target:        '#ajaxTarget',
        beforeSubmit:  showRequest,
        success:       showResponse
      };

      $('#newLrAjax').ajaxForm(options);

      $('#back').click(function() {
        window.location = $(this).attr('name');
      });

      $(".tree_lr").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        opacity: .8,
        revert: true
      });

      var dragged_lr;
      $(".tree_lr").draggable({
        start: function() {
          dragged_lr = $(this);
        }
      });

      $("#parent").droppable({
        drop: function(source) {
          $("#parent").val(dragged_lr.text());
          var splitted = dragged_lr.attr('href').split('choosen_object_id=');
          $("#parent_id").val(splitted[1]);
        }
      });

      $("#loading").ajaxStart(function(){
          $(this).show();
       }).ajaxStop(function(){
          $(this).hide();
       });

      var selectedLr;
      var selectedLrName;

        var show_help = true;
        $('#parent').focus(function() {
          if(show_help) {
            alert($('#parent_help').text());
            show_help = false;
          }
        });

        $('#parent').change(function() {
          if($(this).text() == '') {
            $("#parent_id").val('');
          }
        });
    });

    function showRequest(formData, jqForm, options) {
      var queryString = $.param(formData);
      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      $('#newLrAjax').resetForm();
      $('#ajaxTarget').fadeIn('slow');
      var splitted = responseText.split('lr_id=');
      window.location = $('#detail').attr('title')+splitted[1];
    }
    </script>
  </head>

  <body>
    <div id="main">
      <p id="detail" title="${pageContext.request.contextPath}/client/lr/lr2?lr_id=" style="display: none;"></p>
      <div id="loading" style="background-image:url('${pageContext.request.contextPath}/client/resources/graphics/transparentbg.png')">
        <p><img src="${pageContext.request.contextPath}/client/resources/graphics/ajax-loader.gif"/><br /><spring:message code="efp.client.main.loading" /></p>
      </div>
      <div id="window_small_2" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
        <form id="newLrAjax" action="${pageContext.request.contextPath}/client/lr/newLrFormAjax" method="post">
          <fieldset class="attributes">
            <legend class="attributes"><spring:message code="efp.client.lrScreen.new" /></legend>
            <table border="0">
              <tr class="inside-boxed">
                <td style="display: none;"><input id="gr_id" name="gr_id" type="hidden" value="${gr_object.id}" /></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">id</label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="id" name="id" type="text" class="disabled" value="" onfocus="disabled=true;" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" value="" /></td>
              </tr>
              <tr>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.gr_2.parent" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="parent" name="parent" type="text" class="text-field" value="" /></td>
                <td><input id="parent_id" name="parent_id" type="hidden" value="" /></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right;"><button type="submit" class="black" style="margin-top: 5px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
              </tr>
          </table>
          </fieldset>
        </form>
      </div>
      <div id="content_down" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <table>
          <c:forEach var="entity" items="${entityTree.entities}">
            <c:if test="${entity.type.name == 'lr'}">
              <tr>
                <td>
                  <p style="display: inline;">
                    <c:forEach var="i" begin="2" end="${entity.level}">
                      &nbsp;&nbsp;&nbsp;&nbsp;
                    </c:forEach>
                    <c:if test="${entity.lrEntity.parent != null}">
                      <img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" />
                    </c:if>
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/LrIcon.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" />&nbsp;
                    <a onclick="return false;" href="${pageContext.request.contextPath}/client/lr/lrObjectGr2?gr_id=${gr_object.id}&choosen_object_id=${entity.lrEntity.id}" class="tree_lr">${entity.lrEntity.name}</a>
                  </p>
                </td>
              </tr>
            </c:if>
          </c:forEach>
        </table>

        <div id="target_lr" style="display: none;">
        </div>

      </div>
      <div id="object_properties_small">
        <p class="wizard">
          <spring:message code="efp.client.lr_0.new_help" />
        </p>
        <p id="parent_help" class="wizard" style="display: none;">
          <spring:message code="efp.client.gr_2.parent_help" />
        </p>
      </div>
      <div id="wizard">
        <table>
          <tr>
            <td class="schema_nav">GR->${gr_object.name}.LR</td>
            <c:choose>
              <c:when test="${operator != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />tokos (admin)</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/lr/lr1" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
          </tr>
          <tr>
            <td>
              <p id="ajaxTarget" class="wizard" style="color: green;"></p>
            </td>
          </tr>
        </table>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

