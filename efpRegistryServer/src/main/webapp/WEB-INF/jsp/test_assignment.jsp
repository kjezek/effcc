<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>
  <c:if test="${assignment_result eq '0'}">
    <span id="result" class="result-green"><spring:message code="efp.client.assignmentScreen.result_assignment.ok" /></span>
  </c:if>
  <c:if test="${assignment_result eq '1'}">
    <span id="result" class="result-red"><spring:message code="efp.client.assignmentScreen.result_assignment.duplicity" /></span>
  </c:if>
  <c:if test="${assignment_result eq '2'}">
    <span id="result" class="result-green"></span>
  </c:if>
