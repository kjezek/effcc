<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
      <div id="object_properties">
        <c:choose>
          <c:when test="${choosenObject == null || choosenObject.nullValue}">
            <h5 class="object-properties"><spring:message code="efp.client.main.empty_lr" /></h5>
          </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${choosenObject.type.name eq 'lr'}">
              <%@ include file="lr.jsp" %>
            </c:when>
            <c:when test="${choosenObject.type.name eq 'assignment'}">
              <%@ include file="assignment.jsp" %>
            </c:when>
            <c:when test="${choosenObject.type.name eq 'value'}">
              <%@ include file="value.jsp" %>
            </c:when>
          </c:choose>
        </c:otherwise>
        </c:choose>
      </div>