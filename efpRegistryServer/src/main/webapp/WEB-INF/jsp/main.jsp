<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        $("#loading").ajaxStart(function(){
            $(this).show();
         }).ajaxStop(function(){
            $(this).hide();
         });

        $(".tree").click( function(){
          //send request
          $.get($(this).attr("href"), function( data ){
            //callback function
            $("#object_properties").replaceWith(data);
          });
          //stop
          return false;
        });

      });
    </script>
  </head>

  <body>
    <div id="main">
      <div id="loading" style="background-image:url('${pageContext.request.contextPath}/client/resources/graphics/transparentbg.png')">
        <p><img src="${pageContext.request.contextPath}/client/resources/graphics/ajax-loader.gif"/><br /><spring:message code="efp.client.main.loading" /></p>
      </div>
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <c:forEach var="entity" items="${entityTree.entities}">
          <c:set var="selected_class" value="" />
          <c:if test="${entity.selected}">
            <c:set var="selected_class" value="marked" />
          </c:if>
          <c:if test="${entity.type.name == 'lr'}">
            <p class="${selected_class}">
              <c:forEach var="i" begin="2" end="${entity.level}">
              &nbsp;&nbsp;&nbsp;&nbsp;
              </c:forEach>
              <c:if test="${entity.lrEntity.parent != null}">
                <img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" />
              </c:if>
              <c:choose>
                <c:when test="${entity.packed}">
                  <a href="${pageContext.request.contextPath}/client/main/unpackLr?gr_id=${choosen_gr}&choosen_object_type=${choosenObject.type}&choosen_object_id=${choosenObject.objectId}&lr_id=${entity.lrEntity.id}&packed_list=${packed_list}" title="+" class="dir-icon"><img src="${pageContext.request.contextPath}/client/resources/graphics/LrIcon.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" /></a>&nbsp;
                    <a href="${pageContext.request.contextPath}/client/lr/lrObject?gr_id=${choosen_gr}&choosen_object_id=${entity.lrEntity.id}&packed_list=${packed_list}&ajax_enabled=true"
                       title="<spring:message code='efp.client.main.tree.lr' />" class="tree">${entity.lrEntity.name}</a>
                </c:when>
                <c:otherwise>
                  <a href="${pageContext.request.contextPath}/client/main/packLr?gr_id=${choosen_gr}&choosen_object_type=${choosenObject.type}&choosen_object_id=${choosenObject.objectId}&lr_id=${entity.lrEntity.id}&packed_list=${packed_list}" title="-" class="dir-icon"><img src="${pageContext.request.contextPath}/client/resources/graphics/LrIcon.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" /></a>&nbsp;
                    <a href="${pageContext.request.contextPath}/client/lr/lrObject?gr_id=${choosen_gr}&choosen_object_id=${entity.lrEntity.id}&packed_list=${packed_list}&ajax_enabled=true"
                       title="<spring:message code='efp.client.main.tree.lr' />" class="tree">${entity.lrEntity.name}</a>
                </c:otherwise>
              </c:choose>
            </p>
          </c:if>
          <c:if test="${entity.type.name == 'assignment'}">
            <p class="${selected_class}">
              <c:forEach var="i" begin="1" end="${entity.level}">
              &nbsp;&nbsp;&nbsp;&nbsp;
              </c:forEach>
              <img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" /><img src="${pageContext.request.contextPath}/client/resources/graphics/LrAssignmentIcon.png" height="16" width="16" alt="efp_icon" title="" class="tree-icon" />&nbsp;
                <a href="${pageContext.request.contextPath}/client/efp/efpObject?gr_id=${choosen_gr}&lr_id=${entity.assignment.lr.id}&choosen_object_id=${entity.assignment.efp.id}&packed_list=${packed_list}&ajax_enabled=true" title="EFP" class="tree">${entity.assignment.efp.name}</a> - <a href="${pageContext.request.contextPath}/client/value/valueObject?gr_id=${choosen_gr}&efp_id=${entity.assignment.efp.id}&lr_id=${entity.assignment.lr.id}&value_name=${entity.assignment.valueName.name}&packed_list=${packed_list}&ajax_enabled=true" title="<spring:message code='efp.client.main.value_name' />" class="tree">${entity.assignment.valueName.name}</a>
            </p>
          </c:if>
        </c:forEach>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.choose" /></h5>
        <form action="${pageContext.request.contextPath}/client/main/chooseGr" method="get">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
        <a href="${pageContext.request.contextPath}/client/gr/grScreen?gr_id=${choosen_gr}" title="<spring:message code='efp.client.main.edit' />" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit.png" width="16" height="16" alt="edit" title="" /></a>
        <a href="" title="<spring:message code='efp.client.main.delete' />" onclick="confirmDelete('<spring:message code="efp.client.delete_gr_question" />', '${pageContext.request.contextPath}/client/gr/deleteGr?gr_id=${choosen_gr}');" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="delete" title="" /></a>
      </div>
      <div id="gr_properties">
        <%@ include file="wizard.jsp" %>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <c:choose>
          <c:when test="${choosenObject == null || choosenObject.nullValue}">
            <h5 class="object-properties" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.main.empty_lr" /></h5>
          </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${choosenObject.type.name eq 'lr'}">
              <%@ include file="lr.jsp" %>
            </c:when>
            <c:when test="${choosenObject.type.name eq 'assignment'}">
              <%@ include file="assignment.jsp" %>
            </c:when>
            <c:when test="${choosenObject.type.name eq 'value'}">
              <%@ include file="value.jsp" %>
            </c:when>
          </c:choose>
        </c:otherwise>
        </c:choose>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

