<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        var options = {
          target:        '#message_bar',
          beforeSubmit:  showRequest,
          success:       showResponse
        };

        $('#new_form').ajaxForm(options);

        $(".tree_lr").draggable({
          appendTo: 'body',
          containment: 'window',
          scroll: false,
          helper: 'clone',
          opacity: .8,
          revert: true
        });

        var dragged_gr;
        $(".tree_lr").draggable({
          start: function() {
            dragged_gr = $(this);
          }
        });

        $("#gr_name").droppable({
          drop: function(source) {
            $("#gr_name").val(dragged_gr.text());
            $("#gr_id").val(dragged_gr.attr('href'));
          }
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id')
        });

        $('#logout-button').click(function() {
          window.location = $(this).attr('name')
        });

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

      });

      function showRequest(formData, jqForm, options) {
        var queryString = $.param(formData);
        return true;
      }

      function showResponse(responseText, statusText, xhr, $form)  {
        window.location = $('#back').attr('name');
      }
    </script>
  </head>
  <body>
    <div id="main">
      <div id="content_down_lr1" style="border: none; background-repeat: no-repeat; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid-buttons.gif);">
        <!-- Create new EFP -->
        <form id="new_form" style="padding-top: 40px;" action="${pageContext.request.contextPath}/client/value/newValueAjax" method="post">
          <table border="0" class="attributes" style="width: 700px;">
          <tr>
            <th colspan="2" style="height: 80px;"><spring:message code="efp.client.val_0.new_value" /></th>
          </tr>
          <tr>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.efp_screen.name"/></label></td>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input type="text" id="name" name="name" class="text-field" style="width: 400px;" /></td>
          </tr>
          <tr>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">GR</label></td>
            <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input type="text" id="gr_name" name="gr_name" class="text-field" style="width: 400px;" value="${gr_object.name}" /></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="hidden" id="gr_id" name="gr_id" value="${gr_object.id}" /></td>
          </tr>
          <tr>
            <td></td>
            <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
          </tr>
          </table>
        </form>
        <!-- <p class="wizard"><spring:message code="efp.client.val_0.help" /></p>-->
        <!--
        <table style="padding: 10px;">
          <c:set var="col_index" value="0"/>
          <tr>
          <c:forEach var="gr" items="${grlist}">
            <c:choose>
              <c:when test="${col_index < 3}">
                <td style="width: 250px;">
                  <p style="display: inline;">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/gr.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" />&nbsp;
                    <a onclick="return false;" href="${gr.id}" class="tree_lr">${gr.name}</a>
                  </p>
                </td>
              </c:when>
              <c:otherwise>
                </tr>
                <tr>
                  <td style="width: 250px;">
                  <p style="display: inline;">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/gr.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" />&nbsp;
                    <a onclick="return false;" href="${gr.id}" class="tree_lr">${gr.name}</a>
                  </p>
                </td>
                <c:set var="col_index" value="0"/>
              </c:otherwise>
            </c:choose>
            <c:set var="col_index" value="${col_index+1}"/>
          </c:forEach>
          </tr>
        </table>
        -->
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">VALUE</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/gr/gr2?gr_id=${gr_object.id}&back_action=gr/gr1" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=value/val0?gr_id=${gr_object.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
        </table>
        <div id="message_bar">
        </div>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
