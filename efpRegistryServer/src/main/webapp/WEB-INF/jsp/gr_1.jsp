<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

      var options = {
        target:        '#ajaxTarget',
        beforeSubmit:  showRequest,
        success:       showResponse
      };

      $('#addGrAjax').ajaxForm(options);

      $('#scrollbar3').tinyscrollbar();

      var selectedGr;
      var selectedGrName;

      $('.gr_line').hover(function() {
        if($(this).find('.like_input_id').text() != '') {
          selectedGr = $(this).find('.like_input_id').text();
          selectedGrName = $(this).find('.like_input_normal').text();
          $('.like_input_id_sel').removeClass('like_input_id_sel').addClass('like_input_id');
          $('.like_input_normal_sel').removeClass('like_input_normal_sel').addClass('like_input_normal');

          $(this).find('.like_input_id').removeClass('like_input_id').addClass('like_input_id_sel');
          $(this).find('.like_input_normal').removeClass('like_input_normal').addClass('like_input_normal_sel');

          var nav_content = $('.schema_nav').text();
          if(nav_content.indexOf('->') != -1) {
            nav_content = nav_content.substring(0, nav_content.indexOf('->'));
          }
          nav_content = nav_content+'->'+selectedGrName;
          $('.schema_nav').text(nav_content);

          $('#title_gr_name').text(selectedGrName);
        }
      });

      $('#back').click(function() {
        window.location = $(this).attr('name');
      });

      $('#edit_user').click(function() {
        window.location = $(this).attr('name');
      });

      $('.delete-button').click(function() {
        var returnValue = window.confirm($('#confirm_delete').text());
        var splitted = $(this).attr('id').split('gr_id=');
        var delete_url = $(this).attr('id');
        if(returnValue == true) {
          $.get($('#delete_test').attr('title')+splitted[1], function(res) {
            if(res != 'denied delete') {
              $.get(delete_url, function(data) {
                $('#ajaxTarget').html(data);
                location.reload();
              });
            } else {
              //GR is not empty - cannot delete it
              alert($('#delete_test_denied').text());
            }

          });
        }
      });

      $('.edit-button').click(function() {
        window.location = $(this).attr('id')
      });

      $('#logout-button').click(function() {
        window.location = $(this).attr('name')
      });

      $('#content_down').mousewheel(function(event, delta) {
        $("#content_down").scrollTop($("#content_down").scrollTop() - (delta*15));
      });

      $('.role_checkbox').change(function() {
        if($(this).is(':checked')) {  //only for assign (unassign is not allowed)
          $.get($('#assign').attr('title')+$(this).attr('id'), function(data) {
            location.reload();
          });
        } else {
          $(this).attr('checked', true);
          //$(this).attr('disabled', 'disabled');
        }
      });

      $('.assign-button').click(function() {
        window.location = $(this).attr('id');
      });

    });

    function showRequest(formData, jqForm, options) {
      var queryString = $.param(formData);
      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      $('#addGrAjax').resetForm();
      $('#ajaxTarget').fadeIn('slow');
      var splitted = responseText.split('gr_id:');
      window.location = $('#detail').attr('title')+splitted[1];
      return true;
    }
    </script>
  </head>

  <body>
    <div id="main">
      <p id="assign" title="${pageContext.request.contextPath}/client/gr/assignGRToDeveloper?gr_id=" style="display: none;"/>
      <p id="detail" title="${pageContext.request.contextPath}/client/gr/gr2?back_action=gr/gr1&gr_id=" style="display: none;"></p>
      <p id="confirm_delete" style="display: none;"><spring:message code="efp.client.delete_gr_question" /></p>
      <p id="delete_test" title="${pageContext.request.contextPath}/client/gr/deleteGrAllowed?gr_id=" style="display: none;"/>
      <p id="delete_test_denied" style="display: none;"><spring:message code="efp.client.gr_1.delete_denied" /></p>
      <c:if test="${!is_component_developer}">
      <div id="window_small" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png);">
        <form id="addGrAjax" action="${pageContext.request.contextPath}/client/gr/newGrForm" method="post">
          <fieldset class="attributes">
            <legend class="attributes"><spring:message code="efp.client.grScreen.new" /></legend>
            <table border="0">
              <tr class="inside-boxed">
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
                <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right;"><button type="submit" class="black" style="margin-top: 5px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
              </tr>
          </table>
          </fieldset>
        </form>
      </div>
      </c:if>
      <c:set var="content_down_class" value="content_down"/>
      <c:if test="${fn:length(grlist) > 8}">
        <c:set var="content_down_class" value="content_down_long"/>
      </c:if>
      <div id="${content_down_class}" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <fieldset class="attributes" style="background-color: #FFFFFF; margin-top: 20px;">
          <legend class="attributes"><spring:message code="efp.client.choose" /></legend>
          <table id="ajaxTarget">
            <tr>
              <th></th>
              <th class="inside-boxed" style="width: 150px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">id</th>
              <th class="inside-boxed" style="width: 400px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.gr_1.name" /></th>
              <!--
              <c:if test="${!is_component_developer}">
                <th></th>
                <th></th>
                <th><img src="${pageContext.request.contextPath}/client/resources/graphics/developer.png" width="33" height="32" alt="" title="component developer" /></th>
              </c:if>
              -->
            </tr>
            <c:forEach var="gr" items="${grlist}">
              <tr class="gr_line">
                <td><img src="${pageContext.request.contextPath}/client/resources/graphics/gr-icon.png" width="36" height="32" alt="" title="" /></td>
                <td class="like_input_id" style="width: 150px; border: none;">${gr.id}</td>
                <td class="like_input_normal" style="width: 400px; border: none;">${gr.name}</td>
                <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/gr/gr2?back_action=gr/gr1&gr_id=${gr.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="" width="16" height="16 "/></td>
                <c:if test="${!is_component_developer}">
                  <td class="delete-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/gr/deleteGrAjax?gr_id=${gr.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="" width="16" height="16 "/></td>
                </c:if>
                <!-- Do not show, if user is only component developer (He cannot manage GR assigns) -->
                <c:if test="${!is_component_developer}">
                  <td class="assign-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/gr/gr3?back_action=gr/gr1&gr_id=${gr.id}" title="<spring:message code='efp.client.gr_1.assign_to_user' />">
                    <img src="${pageContext.request.contextPath}/client/resources/graphics/assign.jpg" width="16" height="16" alt="assign" title="" />
                  </td>
                </c:if>
              </tr>
            </c:forEach>
          </table>
        </fieldset>

      </div>
      <div id="object_properties_small">
        <c:choose>
          <c:when test="${is_component_developer}">
            <p class="wizard">
              <spring:message code="efp.client.gr_1.cannot_create_help" />
            </p>
          </c:when>
          <c:otherwise>
            <p class="wizard">
              <spring:message code="efp.client.gr_1.create_help" />
            </p>
          </c:otherwise>
        </c:choose>
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav">GR</td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td style="width: 500px;">
              <h1 id="title_gr_name"></h1>
            </td>
            <c:if test="${logged_user != null}">
              <td class="edit-button" style="cursor: pointer;" id="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=gr/gr1"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="<spring:message code='efp.client.reg1.editUser' />" width="16" height="16 "/></td>
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
        </table>
      </div>
      <c:if test="${fn:length(grlist) <= 8}">
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
      </c:if>
    </div>
  </body>
</html>

