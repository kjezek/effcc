<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
     // prepare the form when the DOM is ready
     $(document).ready(function() {
        var options = {
        target:        '#ajaxTarget',   // target element(s) to be updated with server response
        beforeSubmit:  showRequest,  // pre-submit callback
        success:       showResponse  // post-submit callback
          };

         // bind form using 'ajaxForm'
         $('#addGrAjax').ajaxForm(options);

      });

     // pre-submit callback (validation?)
     function showRequest(formData, jqForm, options) {
         // formData is an array; here we use $.param to convert it to a string to display it
         // but the form plugin does this for you automatically when it submits the data
         var queryString = $.param(formData);

         // jqForm is a jQuery object encapsulating the form element.  To access the
         // DOM element for the form do this:
         // var formElement = jqForm[0];

         // here we could return false to prevent the form from being submitted;
         // returning anything other than false will allow the form submit to continue
         return true;
     }

     // post-submit callback
     function showResponse(responseText, statusText, xhr, $form)  {
         // for normal html responses, the first argument to the success callback
         // is the XMLHttpRequest object's responseText property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'xml' then the first argument to the success callback
         // is the XMLHttpRequest object's responseXML property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'json' then the first argument to the success callback
         // is the json data object returned by the server

         $('#addGrAjax').resetForm();
         $('#ajaxTarget').fadeIn('slow');
         var splitted = responseText.split('<p style="display:none">');
         var fin = splitted[1].split('</p>');
         $('#ref').toggle();
         $('#ref').attr('href', $('#ref').attr('href')+fin[0]);
     }
    </script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <div id="ajaxTarget">
        </div>

        <form id="addGrAjax" action="${pageContext.request.contextPath}/client/gr/newGrForm" method="post">
          <table border="0" class="attributes">
            <tr>
              <th colspan="2" style="height: 80px;"><spring:message code="efp.client.grScreen.content.help.new" /></th>
            </tr>
            <tr class="inside-boxed">
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.grScreen.content.name" /></label></td>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
            </tr>
            <tr>
              <td colspan="2"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.grScreen.confirm_changes" /></button></td>
            </tr>
            <tr>
              <td colspan="2"><p class="explain"><spring:message code="efp.client.grScreen.explain" /></p></td>
            </tr>
          </table>
        </form>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <!-- Selection is disabled in NEW GR mode -->
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.grScreen.new" /></h3>
        <h5 class="grScreen"><spring:message code="efp.client.grScreen.gr" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.grScreen.gr_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <h3><spring:message code="efp.client.wizardGr.step" /></h3>
        <p class="wizard">
          <spring:message code="efp.client.wizardGr.help" />
        </p>
        <a id="ref" style="display:none" href="${pageContext.request.contextPath}/client/lr/wizardLr?choosen_gr=" title="<spring:message code='efp.client.wizardGr.continue.title' />" class="menuButtonGrey">
          <spring:message code="efp.client.wizardGr.continue" />
        </a>
        <a href="${pageContext.request.contextPath}" title="<spring:message code='efp.client.wizardGr.back.desc' />" class="menuButtonGrey">
          <spring:message code="efp.client.wizardGr.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

