<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
     // prepare the form when the DOM is ready
     $(document).ready(function() {

       var options = {
         target:        '#ajaxTarget',   // target element(s) to be updated with server response
         beforeSubmit:  showRequest,  // pre-submit callback
         success:       showResponse  // post-submit callback
       };

       // bind form using 'ajaxForm'
       $('#addLrAjax').ajaxForm(options);

      $("#clean").click(function() {
        $("#parent_id").attr('value', '');
        $("#parent").attr('value', '');
        return false;
      });

      $(".tree_lr").draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
          opacity: .8,
          revert: true
        });

      var dragged_lr;
      $(".tree_lr").draggable({
            start: function() {
              dragged_lr = $(this);
            }
          });

        $("#parent").droppable({
          drop: function(source) {
            $("#parent").attr('value', dragged_lr.text());
            $("#parent_id").attr('value', dragged_lr.attr('href'));
          }
        });

      });

     // pre-submit callback (validation?)
     function showRequest(formData, jqForm, options) {
         // formData is an array; here we use $.param to convert it to a string to display it
         // but the form plugin does this for you automatically when it submits the data
         var queryString = $.param(formData);

         // jqForm is a jQuery object encapsulating the form element.  To access the
         // DOM element for the form do this:
         // var formElement = jqForm[0];

         // here we could return false to prevent the form from being submitted;
         // returning anything other than false will allow the form submit to continue
         return true;
     }

     // post-submit callback
     function showResponse(responseText, statusText, xhr, $form)  {
         // for normal html responses, the first argument to the success callback
         // is the XMLHttpRequest object's responseText property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'xml' then the first argument to the success callback
         // is the XMLHttpRequest object's responseXML property

         // if the ajaxForm method was passed an Options Object with the dataType
         // property set to 'json' then the first argument to the success callback
         // is the json data object returned by the server

         $('#addLrAjax').resetForm();
         $('#ajaxTarget').fadeIn('slow');
         var splitted = responseText.split('<p style="display:none">');
         var fin = splitted[1].split('</p>');
         $('#ref').toggle();
         $('#ref').attr('href', $('#ref').attr('href')+fin[0]);
     }
    </script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <div id="ajaxTarget">
        </div>

        <table border="0">
          <tr>
            <td>
              <!-- form with values -->
              <form id="addLrAjax" action="${pageContext.request.contextPath}/client/lr/newLrForm" method="post">
                <table border="0" class="attributes" style="width: 200px;">
                  <tr>
                    <th colspan="2" style="height: 80px;"><spring:message code="efp.client.lrScreen.content.help.new" /></th>
                  </tr>
                  <tr>
                    <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption">Id:</label></td>
                    <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="id" name="id" type="text" class="disabled" onfocus="disabled=true;" /></td>
                  </tr>
                  <tr>
                    <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.editLr.name"/></label></td>
                    <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><input id="name" name="name" type="text" class="text-field" /></td>
                  </tr>
                  <tr>
                    <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><label class="caption"><spring:message code="efp.client.editLr.parent" /></label></td>
                    <td class="inside-boxed" style="text-align: right; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
                      <input id="parent" name="parent" type="text" class="text-field" onclick="return false;" onkeypress="return false;" />
                      <input id="parent_id" name="parent_id" type="hidden" class="text-field" />
                      <a id="clean" href="" title="<spring:message code='efp.client.lrScreen.clean' />" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="edit" title="" /></a>
                    </td>
                  </tr>
                  <tr>
                    <td><input id="gr_id" name="gr_id" type="hidden" value="${gr_object.id}" /></td>
                    <td align="left"><button type="submit" class="black" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <p class="explain"><spring:message code="efp.client.lrScreen.explain" /></p>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <p class="explain"><spring:message code="efp.client.lrScreen.explain2" /></p>
                    </td>
                  </tr>
                </table>
              </form>
            </td>
            <td>
              <!-- tree with LRs -->
              <div style="height: 400px; width: 250px; overflow: auto;">
              <table border="0">
                <tr>
                  <th style="height: 80px;">
                    <spring:message code="efp.client.lrScreen.lrTree" />
                  </th>
                </tr>
                <c:forEach var="entity" items="${entityTree.entities}">
                  <c:if test="${entity.type.name == 'lr'}">
                  <tr>
                    <td>
                      <p>
                        <c:forEach var="i" begin="2" end="${entity.level}">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        </c:forEach>
                        <c:if test="${entity.lrEntity.parent != null}">
                          <img src="${pageContext.request.contextPath}/client/resources/graphics/tree-line.gif" height="16" width="16" alt="tree-line" title="" class="tree-icon" />
                        </c:if>
                        <img src="${pageContext.request.contextPath}/client/resources/graphics/LrIcon.png" height="16" width="16" alt="lr_icon" title="" class="tree-icon" />&nbsp;
                        <a onclick="return false;" href="${entity.lrEntity.id}" class="tree_lr">${entity.lrEntity.name}</a>
                      </p>
                    </td>
                  </tr>
                  </c:if>
                </c:forEach>
              </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.lrScreen.new" /></h3>
        <h5 class="grScreen"><spring:message code="efp.client.lrScreen.lr" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.lrScreen.lr_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <h3><spring:message code="efp.client.wizardLr.step" /></h3>
        <p class="wizard">
          <spring:message code="efp.client.wizardLr.help" />
        </p>
        <a id="ref" style="display:none" href="${pageContext.request.contextPath}/client/efp/assignmentScreen?gr_id=${gr_object.id}&lr_id=" title="<spring:message code='efp.client.wizardLr.continue.title' />" class="menuButtonGrey">
          <spring:message code="efp.client.wizardLr.continue" />
        </a>
        <a href="${pageContext.request.contextPath}" title="<spring:message code='efp.client.wizardLr.back.desc' />" class="menuButtonGrey">
          <spring:message code="efp.client.wizardLr.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

