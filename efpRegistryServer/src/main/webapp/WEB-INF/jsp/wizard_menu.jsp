        <img src="${pageContext.request.contextPath}/client/resources/graphics/welcome.png" height="96" width="96" alt="<spring:message code='efp.client.main.wizard' />" title="<spring:message code='efp.client.main.wizard' />" style="display: inline;" />
        <img src="${pageContext.request.contextPath}/client/resources/graphics/wizard.gif" height="96" width="710" alt="<spring:message code='efp.client.main.wizard' />" title="<spring:message code='efp.client.main.wizard' />" />
        <a href="${pageContext.request.contextPath}/client/gr/grScreen?gr_id=${choosen_gr}" title="<spring:message code='efp.client.main.gr_changes' />" id="gr-button" class="wizard">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/gr2_complex.gif" height="60" width="120" alt="GR" title="<spring:message code='efp.client.main.gr_desc' />" />
        </a>
        <a href="${pageContext.request.contextPath}/client/lr/lrScreen?gr_id=${choosen_gr}&lr_id=" title="<spring:message code='efp.client.main.lr_changes' />" id="lr-button" class="wizard">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/lr2_complex.gif" height="60" width="120" alt="GR" title="<spring:message code='efp.client.main.lr_desc' />" />
        </a>
        <a href="${pageContext.request.contextPath}/client/efp/efpScreen?gr_id=${choosen_gr}&efp_id=" title="<spring:message code='efp.client.main.efp_changes' />" id="efp-button" class="wizard">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/efp2_complex.gif" height="60" width="120" alt="GR" title="<spring:message code='efp.client.main.efp_desc' />" />
        </a>
        <a href="${pageContext.request.contextPath}/client/value/valueScreen?gr_id=${choosen_gr}&value_id=" title="<spring:message code='efp.client.main.value_changes' />" id="val-button" class="wizard">
          <img src="${pageContext.request.contextPath}/client/resources/graphics/val2_complex.gif" height="60" width="120" alt="GR" title="<spring:message code='efp.client.main.value_desc' />" />
        </a>