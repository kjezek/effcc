<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        var selectedEfpName = '';
        var assignmentId = '';

        $('.gr_line').hover(function() {
          if($(this).find('.like_input_id').text() != '') {
            assignmentId = $(this).find('.like_input_id').text();
            selectedEfpName = $(this).find('.like_input_lr').text();

            $('.like_input_id_sel').removeClass('like_input_id_sel').addClass('like_input_id');
            $('.like_input_lr_sel').removeClass('like_input_lr_sel').addClass('like_input_lr');
            $('.like_input_normal_sel').removeClass('like_input_normal_sel').addClass('like_input_normal');

            $(this).find('.like_input_id').removeClass('like_input_id').addClass('like_input_id_sel');
            $(this).find('.like_input_lr').removeClass('like_input_lr').addClass('like_input_lr_sel');
            $(this).find('.like_input_normal').removeClass('like_input_normal').addClass('like_input_normal_sel');

            var nav_content = $('.schema_nav').text();
            if(nav_content.indexOf('->') != -1) {
              nav_content = nav_content.substring(0, nav_content.indexOf('->'));
            }
            nav_content = nav_content+'->'+selectedEfpName;
            $('.schema_nav').text(nav_content);
          }
        });

        $('.delete-button').click(function() {
          var returnValue = window.confirm($('#confirm_delete').text());
            if(returnValue == true) {
            $.get($(this).attr('id'), function(data) {
              $('#result_target').html(data);
              window.location.reload();
            });
          }
        });

        $('#new').click(function() {
          window.location = $(this).attr('name');
        });

        $('#show_unassignments').click(function() {
          window.location = $(this).attr('name');
        });

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

      });
    </script>
  </head>
  <body>
    <div id="main">
      <p id="confirm_delete" style="display: none;"><spring:message code="efp.client.deleteAssignment.deleteConfirmation" /></p>
      <div id="lr_buttons">
        <table class="centered">
          <tr>
            <td>
              <button id="new" name="${pageContext.request.contextPath}/client/efp/ass0?back_action=efp/efp2" type="button" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.efp_2.new" />
              </button>
            </td>
            <td>
              <button id="show_unassignments" name="${pageContext.request.contextPath}/client/efp/efp1" type="button" class="black" style="margin-top: 5px;">
                <spring:message code="efp.client.efp_2.unassigned" />
              </button>
            </td>
          </tr>
        </table>
        <p id="no_selection" style="display: none;"><spring:message code="efp.client.efp_1.no_selection" /></p>
      </div>
      <div id="content_down_lr1" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <!-- assigned EFPs -->
        <table class="centered">
          <tr>
            <th colspan="3" class="inside-boxed" style="text-align: center;background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.efp_2.assigned" />
            </th>
          </tr>
          <tr>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.efp_2.efp" />
            </th>
            <th class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.efp_2.value" />
            </th>
            <th class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              LR
            </th>
          </tr>
          <c:forEach var="assignment" items="${assignments}">
            <tr class="gr_line">
              <td class="like_input_id" style="width: 100px; display: none;">${assignment.id}</td>
              <td class="like_input_lr" style="width: 250px;">${assignment.efp.name}</td>
              <td class="like_input_normal" style="width: 200px;">${assignment.valueName.name}</td>
              <td class="like_input_normal" style="width: 250px;">${assignment.lr.name}</td>
              <td class="delete-button" id="${pageContext.request.contextPath}/client/efp/deleteAssignmentAjax?assignment_id=${assignment.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="" width="16" height="16 "/></td>
            </tr>
          </c:forEach>
        </table>
      </div>
      <div id="wizard">
        <table>
          <tr>
            <td class="schema_nav">ASSIGNMENT</td>
            <c:choose>
              <c:when test="${operator != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />tokos (admin)</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/efp/efp1" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
          </tr>
          <tr>
            <td id="result_target"></td>
          </tr>
        </table>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>
