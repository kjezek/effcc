<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
              <div class="lr_name" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/${choosenObject.backgroundImage}); background-repeat: no-repeat; background-position: 50% 50%; background-color: #D6D6D6; margin: 5px;">
                <h6 class="object-properties"><spring:message code="efp.client.lr.name" /></h6>
                <h5 class="object-properties" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png); opacity: 0.8;">${choosenObject.lrEntity.optimizedName}</h5>
                <a href="${pageContext.request.contextPath}/client/lr/editLr?gr_id=${choosen_gr}&choosen_object_id=${choosenObject.lrEntity.id}&packed_list=${packed_list}" title="<spring:message code='efp.client.lr.edit' />" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit.png" width="16" height="16" alt="edit" title="" /></a>
                <a href="" title="<spring:message code='efp.client.lr.delete' />" class="image" onclick="confirmDelete('<spring:message code="efp.client.lr.confirm_delete" />', '${pageContext.request.contextPath}/client/lr/deleteLr?lr_id=${choosenObject.lrEntity.id}');"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="delete" title="" /></a>
              </div>
              <hr class="object-properties" />
              <h6 class="object-properties"><spring:message code="efp.client.lr.child_repos" /></h6>
              <div class="object-properties">
                <ul>
                  <c:forEach var="child_lr" items="${choosenObject.childLrEntities}">
                    <li><a href="${pageContext.request.contextPath}/client/lr/lrObject?gr_id=${choosen_gr}&choosen_object_id=${child_lr.id}&packed_list=${packed_list}&ajax_enabled=false" title="<spring:message code='efp.client.lr.show_lr' />" class="object-properties">${child_lr.name}</a></li>
                  </c:forEach>
                </ul>
              </div>
              <hr class="object-properties" />
              <h6 class="object-properties">EFP:</h6>
              <c:set var="tmp_efp" value="${null}" />
              <c:forEach var="assignment" items="${entityTree.childAssignments[choosenObject.lrEntity.id]}">
                <c:if test="${tmp_efp == null || (tmp_efp != null && !(tmp_efp eq assignment.efp.id))}">
                  <c:if test="${tmp_efp != null}">
                    <hr />
                  </c:if>
                <a href="${pageContext.request.contextPath}/client/efp/efpObject?gr_id=${choosen_gr}&lr_id=${choosenObject.lrEntity.id}&choosen_object_id=${assignment.efp.id}&packed_list=${packed_list}&ajax_enabled=false" title="<spring:message code='efp.client.lr.show_efp' />" class="object-properties-efp">${assignment.efp.name}</a>
                </c:if>
                <br />&nbsp;&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/client/value/valueObject?gr_id=${choosen_gr}&efp_id=${assignment.efp.id}&lr_id=${assignment.lr.id}&value_name=${assignment.valueName.name}&packed_list=${packed_list}&ajax_enabled=false" title="<spring:message code='efp.client.lr.value' />" class="object-properties-efp" style="background-color: #D6D6D6;">${assignment.valueName.name}</a>
                <c:set var="tmp_efp" value="${assignment.efp.id}" />
              </c:forEach>