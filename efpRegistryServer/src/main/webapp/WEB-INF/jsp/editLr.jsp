<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository - Edit LR</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
  </head>
  <body>
    <div id="main">
      <h1 class="title"><spring:message code="efp.client.editLr.title" /></h1>
      <a href="${pageContext.request.contextPath}/client/lr/lrObject?gr_id=${choosen_gr}&choosen_object_id=${choosen_object_id}&packed_list=${packed_list}" title="<spring:message code='efp.client.editLr.back_to_tree' />" class="image"><img src="${pageContext.request.contextPath}/client/resources/graphics/back.png" width="16" height="16" alt="<spring:message code='efp.client.editLr.back_to_tree' />" title="" /></a>
      <form:form modelAttribute="lr_entity" action="${pageContext.request.contextPath}/client/lr/editLrForm" method="post">
        <table border="0">
        <tr>
          <td><form:label path="id" class="caption">Id:</form:label></td>
          <td><form:input path="id" class="disabled" onfocus="disabled=true;" /></td>
          <td rowspan="3" valign="middle" style="padding-left: 30px;">
            <img src="${pageContext.request.contextPath}/client/resources/graphics/lr2.gif" height="60" width="60" alt="GR" title="" />
          </td>
        </tr>
        <tr>
          <td><form:label path="name" class="caption"><spring:message code="efp.client.editLr.name"/></form:label></td>
          <td><form:input path="name" class="text-field" /></td>
        </tr>
        <tr>
          <td><form:label path="parent" class="caption"><spring:message code="efp.client.editLr.parent" /></form:label></td>
          <td>
            <form:select path="parent" class="text-field">
              <form:option value="" class="text-field">&nbsp;</form:option>
              <form:options items="${lr_entities}" itemValue="id" itemLabel="name" />
            </form:select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td align="left"><button type="submit" class="green" style="margin-top: 30px;"><spring:message code="efp.client.editLr.confirm_changes" /></button></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="3">
            <p class="explain"><spring:message code="efp.client.lrScreen.explain" /></p>
          </td>
        </tr>
        </table>
      </form:form>
    </div>
  </body>
</html>