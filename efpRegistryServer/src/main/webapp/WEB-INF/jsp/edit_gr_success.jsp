<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>
<p id="ajaxTarget" class="wizard" style="color: #00FF00; font-weight: bold;">
  <spring:message code="efp.client.gr_2.edit_success" />
</p>
