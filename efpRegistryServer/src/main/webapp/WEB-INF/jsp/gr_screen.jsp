<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
  </head>

  <body>
    <div id="main">
      <div id="window" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <form:form modelAttribute="gr_object" action="${pageContext.request.contextPath}/client/gr/editGrForm" method="post">
          <table border="0" class="attributes">
            <tr>
              <th colspan="2" style="height: 80px;"><spring:message code="efp.client.grScreen.content.help" /></th>
            </tr>
            <tr>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="id" class="caption">Id:</form:label></td>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="id" class="disabled" onfocus="disabled=true;" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:label path="name" class="caption"><spring:message code="efp.client.grScreen.content.name" /></form:label></td>
              <td class="inside-boxed" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><form:input path="name" class="text-field" /><form:hidden path="id" /></td>
            </tr>
            <tr>
              <td></td>
              <td align="left"><button type="submit" class="black" style="margin-top: 30px;">
                <spring:message code="efp.client.grScreen.confirm_changes" />
              </button></td>
            </tr>
            <tr>
              <td colspan="2"><p class="explain"><spring:message code="efp.client.grScreen.explain" /></p></td>
            </tr>
          </table>
        </form:form>
      </div>
      <div id="gr_chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/gr2.png);">
        <h5 class="chooser" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);"><spring:message code="efp.client.choose" /></h5>
        <form action="${pageContext.request.contextPath}/client/gr/selectGr" method="get">
          <p style="display: inline;">
            <select id="gr_select" name="gr_select" class="chooser" onchange="this.form.submit()">
              <c:forEach var="gr" items="${grlist}">
                <c:choose>
                  <c:when test="${choosen_gr eq gr.id}">
                    <option selected="selected" value="${gr.id}">${gr.name}</option>
                  </c:when>
                  <c:otherwise>
                    <option value="${gr.id}">${gr.name}</option>
                  </c:otherwise>
                </c:choose>
              </c:forEach>
            </select>
          </p>
        </form>
      </div>
      <div id="gr_properties">
        <h3 class="grScreen" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">${gr_object.name}</h3>
        <h5 class="grScreen"><spring:message code="efp.client.grScreen.gr" /></h5>
        <p class="wizard">
          <spring:message code="efp.client.grScreen.gr_desc" />
        </p>
      </div>
      <div id="wizard">
        <%@ include file="wizard_menu.jsp" %>
      </div>
      <div id="object_properties">
        <a href="${pageContext.request.contextPath}/client/gr/newGr" title="" class="menuButtonGrey">
          <spring:message code="efp.client.grScreen.new" />
        </a>
        <a href="${pageContext.request.contextPath}/client/gr/grScreen?gr_id=${gr_object.id}" title="" class="menuButtonSelected">
          <spring:message code="efp.client.grScreen.edit" />
        </a>
        <a href="" title="" class="menuButtonGrey" onclick="confirmDelete('<spring:message code="efp.client.delete_gr_question" />', '${pageContext.request.contextPath}/client/gr/deleteGr?gr_id=${choosen_gr}');">
          <spring:message code="efp.client.grScreen.delete" />
        </a>
        <a href="${pageContext.request.contextPath}/client/main/tree?gr_id=${gr_object.id}&choosen_object_type=gr&choosen_object_id=${choosen_gr}&packed_list=" title="" class="menuButtonGrey">
          <spring:message code="efp.client.grScreen.back" />
        </a>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>

