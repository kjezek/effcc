<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/tinyscrollbar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        var selectedLr = '';
        var selectedLrName = '';
        var selectedGr = '';

        $('.gr_line').hover(function() {
          if($(this).find('.like_input_id').text() != '') {
            selectedLr = $(this).find('.like_input_id').text();
            selectedLrName = $(this).find('.like_input_lr').text();
            if(selectedLrName == '') {
              selectedLrName = $(this).find('.like_input_lr_colored').text();
            }
            selectedGr = $(this).find('.like_input_gr_id').text();
            if(selectedGr == '') {
              selectedGr = $(this).find('.like_input_gr_id_colored').text();
            }
            $('.like_input_lr_sel_colored').removeClass('like_input_lr_sel_colored').addClass('like_input_lr_colored');
            $('.like_input_lr_sel').removeClass('like_input_lr_sel').addClass('like_input_lr');
            $('.like_input_normal_sel_colored').removeClass('like_input_normal_sel_colored').addClass('like_input_normal_colored');
            $('.like_input_normal_sel').removeClass('like_input_normal_sel').addClass('like_input_normal');

            $(this).find('.like_input_lr_colored').removeClass('like_input_lr_colored').addClass('like_input_lr_sel_colored');
            $(this).find('.like_input_lr').removeClass('like_input_lr').addClass('like_input_lr_sel');
            $(this).find('.like_input_normal_colored').removeClass('like_input_normal_colored').addClass('like_input_normal_sel_colored');
            $(this).find('.like_input_normal').removeClass('like_input_normal').addClass('like_input_normal_sel');

            var nav_content = $('.schema_nav').text();
            if(nav_content.indexOf('->') != -1) {
              nav_content = nav_content.substring(0, nav_content.indexOf('->'));
            }
            nav_content = nav_content+'->'+selectedLrName;
            $('.schema_nav').text(nav_content);
          }
        });

        $('#back').click(function() {
          window.location = $(this).attr('name');
        });

        $('.delete-button').click(function() {
          var returnValue = window.confirm($('#confirm_delete').text());
          if(returnValue == true) {
            $.get($(this).attr('id'), function(data) {
              $('#ajaxTarget').html(data);
                location.reload();
            });
          }
        });

        $('.edit-button').click(function() {
          window.location = $(this).attr('id');
        });

        $('.add-button').click(function() {
          if(selectedGr == '') {
            alert($('#no_selection_gr').text());
          } else {
            window.location = $(this).attr('id')+selectedGr;
          }
        });

      });
    </script>
  </head>
  <body>
    <div id="main">
      <p id="confirm_delete" style="display: none;"><spring:message code="efp.client.lr.confirm_delete" /></p>
      <div id="lr_buttons">
        <p id="no_selection" style="display: none;"><spring:message code="efp.client.gr_2.no_selection" /></p>
        <p id="no_selection_gr" style="display: none;"><spring:message code="efp.client.lr_1.no_selection_gr" /></p>
      </div>
      <div id="content_down_lr1" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/grid2.jpg);">
        <table class="centered">
          <tr>
            <th></th>
            <th class="inside-boxed" style="width: 150px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.lr_1.lr" />
            </th>
            <th class="inside-boxed" style="width: 150px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/title_back.png);">
              <spring:message code="efp.client.lr_1.from_gr" />
            </th>
          </tr>
          <!-- Group by GR -->
          <c:set var="colored" value="true" />
          <c:forEach var="gr" items="${gr_entities}">
            <!-- Show all LRs in the same GR -->
            <c:forEach var="lr" items="${lr_map[gr.id]}">
              <tr class="gr_line">
                <td class="add-button" title="<spring:message code='efp.client.lr_1.add' />" id="${pageContext.request.contextPath}/client/lr/lr0?gr_id="><img src="${pageContext.request.contextPath}/client/resources/graphics/plus.gif" alt="add" title="" width="16" height="16 "/></td>
                <td class="like_input_id" style="display: none;">${lr.id}</td>
                <td class="like_input_gr_id" style="display: none;">${gr.id}</td>
                <c:choose>
                  <c:when test="${colored eq true}">
                    <td class="like_input_lr_colored">${lr.name}</td>
                    <td class="like_input_normal_colored" style="width: 150px;">${gr.name}</td>
                  </c:when>
                  <c:otherwise>
                    <td class="like_input_lr">${lr.name}</td>
                    <td class="like_input_normal" style="width: 150px;">${gr.name}</td>
                  </c:otherwise>
                </c:choose>
                <td class="edit-button" id="${pageContext.request.contextPath}/client/lr/lr2?lr_id=${lr.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/edit-button.png" alt="edit" title="" width="16" height="16 "/></td>
                <td class="delete-button" id="${pageContext.request.contextPath}/client/lr/deleteLrAjax?lr_id=${lr.id}"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete-button.png" alt="delete" title="" width="16" height="16 "/></td>
              </tr>
            </c:forEach>
            <c:choose>
              <c:when test="${colored eq true}">
                <c:set var="colored" value="false" />
              </c:when>
              <c:otherwise>
                <c:set var="colored" value="true" />
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </table>
      </div>
      <div id="wizard">
        <table>
          <tr>
            <td class="schema_nav">LR</td>
            <c:choose>
              <c:when test="${operator != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />tokos (admin)</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back" name="${pageContext.request.contextPath}/client/main/guidepost" type="button" class="black" style="margin-top: 5px;">&lt;&lt;</button>
            </td>
          </tr>
        </table>
      </div>
      <div id="footer">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>