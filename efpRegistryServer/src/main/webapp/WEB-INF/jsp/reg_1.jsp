<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="EFP Repository" />
    <meta name="copyright" content="TomasKohout@2011" />
    <meta name="keywords" content="EFP, repository, storage, component, software, OSGi" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="author" content="Bc. Tomáš Kohout" />
    <title>EFP Repository</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_basic.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/resources/css/styles_print.css" type="text/css" media="print" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-1.7.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery.form.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/scriptUtils.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/resources/js/jQuery_mousewheel_plugin.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      var options = {
          target:        '#ajaxTarget',
          beforeSubmit:  showRequest,
          success:       showResponse
      };

      $('#edit_form').ajaxForm(options);

      $('#back').click(function() {
        parent.history.back();
        //window.location = $(this).attr('name');
      });

      $('#back_gr1').click(function() {
        window.location = $(this).attr('name');
      });

      $('#logout-button').click(function() {
        window.location = $(this).attr('name');
      });

      $('.gr_black').click(function() {
        $('#user_select').val($(this).text());
        $('#user_combo').hide('slow');

        window.location = $('#user_select_refresh').attr('title')+$(this).attr('name');
      });

      $('#user_select').click(function() {
        $('#user_combo').toggle('slow');
      });

      $(document).keypress(function(e) {
        if (e.keyCode == 27) { $('#user_combo').hide(); }
      });

      $('#user_combo').mousewheel(function(event, delta) {
        $("#user_combo").scrollTop($("#gr_combo").scrollTop() - (delta*15));
      });

    });

    function showRequest(formData, jqForm, options) {

      // password check
      var password = $('#password').fieldValue();
      var passwordCheck = $('#password_check').fieldValue();

      if($.trim(password) != $.trim(passwordCheck)) {
        alert($('#not_match').text());
        return false;
      }

      return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
      window.location.reload();
      //parent.history.back();
      //window.location = $('#next').attr('name');
    }
    </script>
  </head>
  <body>
    <div id="main">
      <p id="user_select_refresh" title="${pageContext.request.contextPath}/client/user/reg1?login=${logged_user.login}&back_action=gr/gr1&user_id=" style="display: none;" />
      <div id="ajaxTarget" style="display: none;"></div>
      <button id="next" name="${pageContext.request.contextPath}/client/gr/gr1" style="display: none;">next</button>
      <p id="not_match" style="display: none;">
        <spring:message code="efp.client.reg0.passwordMatch" />
      </p>
      <img src="${pageContext.request.contextPath}/client/resources/graphics/banner-free.png" height="96" width="806" alt="" title="" />
      <div id="content">
        <c:if test="${is_admin}">
          <!-- Combo for choose users (ADMIN)-->
          <input type="text" id="user_select" value="${edited_user.login} - ${edited_user.name} ${user_2_roles[edited_user.id]}" style="border: 1px solid black; font-family: verdana; font-weight: bold; width: 700px; font-size: medium; cursor: pointer; margin-top: 5px;"/>
          <div id="user_combo" style="display: none; margin-top: 5px;">
            <c:forEach var="user" items="${users}">
              <a class="gr_black" name="${user.id}">${user.login} - ${user.name} ${user_2_roles[user.id]}</a>
            </c:forEach>
          </div>
        </c:if>
        <form id="edit_form" action="${pageContext.request.contextPath}/client/user/editUser" method="post">
          <table class="attributes" style="width: 700px;">
            <tr>
              <td><button id="back" name="${pageContext.request.contextPath}/client/${back_action}" type="button" class="black">&lt;</button></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th colspan="3" style="height: 80px;"><spring:message code="efp.client.reg1.editUser" /></th>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption"><spring:message code="efp.client.reg0.login"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;">
                <input type="text" id="login" name="login" onfocus="disabled=true;" class="disabled" style="width: 250px;" value="${edited_user.login}" />
              </td>
              <td rowspan="6" class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/login-paper2.png); background-repeat: none;">
                <table>
                  <tr>
                    <th><spring:message code="efp.client.reg0.userRoles" /></th>
                  </tr>
                  <c:forEach var="role" items="${user_roles}">
                    <tr>
                      <td>${role.description}</td>
                      <c:set var="contains" value="false" />
                      <c:forEach var="r" items="${roles2}">
                        <c:if test="${r.id == role.id}">
                          <c:set var="contains" value="true" />
                        </c:if>
                      </c:forEach>
                      <c:choose>
                        <c:when test="${contains}">
                          <td><input type="checkbox" name="roles" value="${role.id}" checked="checked" /></td>
                        </c:when>
                        <c:otherwise>
                          <c:choose>
                            <c:when test="${is_admin}">
                              <td><input type="checkbox" name="roles" value="${role.id}" /></td>
                            </c:when>
                            <c:otherwise>
                              <td><input type="checkbox" name="roles" value="${role.id}" disabled="disabled" /></td>
                            </c:otherwise>
                          </c:choose>
                        </c:otherwise>
                      </c:choose>
                    </tr>
                  </c:forEach>
                </table>
              </td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.password"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="password" id="password" name="password" class="password" style="width: 250px;" value=""/></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.passwordCheck"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="password" id="password_check" name="password_check" class="password" style="width: 250px;" value="" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.name"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="name" name="name" class="text-field" style="width: 250px;" value="${edited_user.name}" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.email"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="email" name="email" class="text-field" style="width: 250px;" value="${edited_user.email}" /></td>
            </tr>
            <tr>
              <td class="inside-boxed" style="width: 200px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><label class="caption" style="width: 200px;"><spring:message code="efp.client.reg0.phone"/></label></td>
              <td class="inside-boxed" style="width: 250px; background-image: url(${pageContext.request.contextPath}/client/resources/graphics/background-login.png); background-repeat: none;"><input type="text" id="phone" name="phone" class="text-field" style="width: 250px;" value="${edited_user.phone}" /></td>
            </tr>
            <tr>
              <td style="text-align: right;">
                <button type="reset" class="black">Reset</button>
              </td>
              <td style="text-align: left;">
                <button type="submit" class="black"><spring:message code="efp.client.reg1.edit" /></button>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div id="wizard" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/banner-free.png);">
        <table>
          <tr>
            <td class="schema_nav"></td>
            <c:choose>
              <c:when test="${logged_user != null}">
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.operator_desc" /><br />${logged_user.login} (${user_roles_logged})</td>
              </c:when>
              <c:otherwise>
                <td rowspan="2" class="operator"><spring:message code="efp.client.operator.anonymous_desc" /></td>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td>
              <button id="back_gr1" name="${pageContext.request.contextPath}/client/${back_action}" type="button" class="black" style="margin-top: 5px;" title="">&lt;&lt;</button>
            </td>
            <c:if test="${logged_user != null}">
              <td><button id="logout-button" name="${pageContext.request.contextPath}/j_spring_security_logout" class="black"><spring:message code="efp.client.operator.logout"/></button></td>
            </c:if>
          </tr>
        </table>
      </div>
      <div id="footer" style="background-image: url(${pageContext.request.contextPath}/client/resources/graphics/footer-background.png); background-repeat: none; background-position: center;">
        <%@ include file="footer.jsp" %>
      </div>
    </div>
  </body>
</html>