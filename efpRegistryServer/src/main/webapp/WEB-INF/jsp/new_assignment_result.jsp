<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% request.setCharacterEncoding("utf-8"); %>
<% response.setCharacterEncoding("utf-8"); %>
          <c:forEach var="assignment" items="${assignments}">
            <tr>
              <td style="width: 200px; text-align: left;">${assignment.lr.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.efp.name}</td>
              <td style="width: 200px; text-align: left;">${assignment.valueName.name}</td>
            </tr>
          </c:forEach>