<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
      <table border="0" class="centered">
              <c:forEach var="value" items="${values}">
                <tr>
                  <td></td>
                  <td class="inside-boxed" style="text-align: left; padding-left: 5px; padding-right: 5px;">
                    <a href="${pageContext.request.contextPath}/client/value/valueScreen?gr_id=${choosen_gr}&value_id=${value.id}" title="<spring:message code='efp.client.assignment.edit' />" class="object-properties">${value.name}</a>
                  </td>
                  <td>
                    <a href="" title="<spring:message code='efp.client.assingment.delete' />" class="image" onclick="confirmDelete('<spring:message code="efp.client.efpScreen.assignment.confirm_delete" />', '${pageContext.request.contextPath}/client/efp/deleteValueEfpScreen?efp_id=${choosen_efp}&value_id=${value.id}');"><img src="${pageContext.request.contextPath}/client/resources/graphics/delete.png" width="16" height="16" alt="delete" title="" /></a>
                  </td>
                </tr>
              </c:forEach>
            </table>