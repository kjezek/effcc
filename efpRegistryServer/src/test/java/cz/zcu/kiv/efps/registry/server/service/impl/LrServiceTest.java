/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.zcu.kiv.efps.registry.server.dao.LrDao;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrService;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LrServiceTest extends AbstractTestSupport {

    /** Service.  */
    @Resource(name = "lrService")
    private LrService lrService;

    /** DAO.  */
    @Resource(name = "lrDao")
    private LrDao lrDao;

    /** Service.  */
    @Resource(name = "grService")
    private GrService grService;

    /** A test GR. */
    private GrEntity gr;

    @Before
    public void setUp() {
        gr = new GrEntity();
        gr.setName("Education systems");
        gr.setIdStr("ES");

        grService.save(gr);

        lrDao.flush();
        lrDao.clear();
    }


    @Test
    public void testSave() {

        LrEntity newLr = new LrEntity();
        newLr.setName("High performance LR.");
        newLr.setGr(gr);

        lrService.save(newLr);

        Assert.assertNotNull(newLr.getId());
        Assert.assertEquals(gr, newLr.getGr());
    }

    @Test
    public void testSaveAttachParent() {
        LrEntity newLr = new LrEntity();
        newLr.setName("High performance LR.");
        newLr.setGr(gr);

        LrEntity parentLr = new LrEntity();
        parentLr.setName("Parent.");
        parentLr.setGr(gr);

        lrService.save(parentLr);
        newLr.setParent(parentLr);
        lrService.save(newLr);

        Assert.assertNotNull(newLr.getId());
        Assert.assertNotNull(newLr.getParent());
        Assert.assertEquals(parentLr, newLr.getParent());
        Assert.assertEquals(gr, parentLr.getGr());
    }

    @Test
    public void testFindLrForGr() {
        LrEntity newLr = new LrEntity();
        newLr.setName("High performance LR.");
        newLr.setGr(gr);

        LrEntity parentLr = new LrEntity();
        parentLr.setName("Parent.");
        parentLr.setGr(gr);

        lrService.save(parentLr);
        newLr.setParent(parentLr);
        lrService.save(newLr);

        List<LrEntity> list = lrService.findLrForGr(gr.getId());

        Assert.assertTrue(list.contains(parentLr));
        Assert.assertTrue(list.contains(parentLr));
    }

    @Test
    public void testUpdate() {

        LrEntity newLr = new LrEntity();
        newLr.setName("High performance LR.");
        newLr.setGr(gr);
        lrService.save(newLr);

        newLr.setName("Modified name");
        lrService.update(newLr);

        Assert.assertEquals(newLr, lrService.getById(newLr.getId()));

    }

    @Test(expected = Exception.class)
    public void testUpdateNonExisting() {
        LrEntity newLr = new LrEntity();
        newLr.setName("Modified name");
        newLr.setGr(gr);
        lrService.update(newLr);

    }

}
