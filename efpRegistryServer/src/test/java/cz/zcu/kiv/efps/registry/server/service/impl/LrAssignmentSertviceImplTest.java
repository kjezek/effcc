/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.zcu.kiv.efps.registry.server.dao.LrDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrAssignmentEntity;
import cz.zcu.kiv.efps.registry.server.entities.LrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.LrAssignmentService;
import cz.zcu.kiv.efps.registry.server.service.LrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.LrDTO;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LrAssignmentSertviceImplTest extends AbstractTestSupport {

    /** A service under test. */
    @Resource(name = "lrAssignmentService")
    private LrAssignmentService lrAssignmentService;

    /** A service used for this test. */
    @Resource(name = "efpService")
    private EfpService efpService;

    /** A service used for this test. */
    @Resource(name = "grService")
    private GrService grService;

    /** A service used for this test. */
    @Resource(name = "lrService")
    private LrService lrService;

    /** A service used for this test. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** LR dao. */
    @Resource(name = "lrDao")
    private LrDao lrDao;

    /** Common test entity. */
    private GrEntity gr;

    /** Common test entity. */
    private EfpEntity efp1;

    /** Common test entity. */
    private EfpEntity efp2;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigEfp1;

    /** Common test entity. */
    private LrAssignmentEntity lrAssigEfp2;

    /** Common test entity. */
    private LrEntity lr;

    /** Common test entity. */
    private ValueNameEntity name;

    @Before
    public void tearUp() {
        gr = new GrEntity();
        gr.setName("Education systems");
        gr.setIdStr("ES");
        grService.save(gr);

        efp1 = new EfpEntity();
        efp1.setName("EFP1");
        efp1.setGr(gr);
        efpService.save(efp1);

        efp2 = new EfpEntity();
        efp2.setName("EFP2");
        efp2.setGr(gr);
        efpService.save(efp2);

        lr = new LrEntity();
        lr.setName("High performance LR.");
        lr.setGr(gr);
        lrService.save(lr);

        name = new ValueNameEntity();
        name.setGr(gr);
        name.setName("slow");
        valueNameService.save(name);

        lrAssigEfp1 = new LrAssignmentEntity();
        lrAssigEfp1.setEfp(efp1);
        lrAssigEfp1.setLr(lr);
        lrAssigEfp1.setValueName(name);
        lrAssigEfp1.setValue("10");
        lrAssignmentService.save(lrAssigEfp1);


        lrAssigEfp2 = new LrAssignmentEntity();
        lrAssigEfp2.setEfp(efp2);
        lrAssigEfp2.setLr(lr);
        lrAssigEfp2.setValueName(name);
        lrAssigEfp2.setValue("10");
        lrAssignmentService.save(lrAssigEfp2);

        lrDao.flush();
        lrDao.clear();
    }

    @Test
    public void testFindLrAssignments() {

        List<LrAssignmentEntity> assigns =
        lrAssignmentService.findLrAssignments(efp1.getId(), lr.getId());

        Assert.assertTrue(assigns.contains(lrAssigEfp1));
        Assert.assertFalse(assigns.contains(lrAssigEfp2));
    }

    @Test
    public void testFindAllLrAssignments() {

        List<LrAssignmentEntity> assigns =
        lrAssignmentService.findLrAssignments(lr.getId());

        Assert.assertTrue(assigns.contains(lrAssigEfp1));
        Assert.assertTrue(assigns.contains(lrAssigEfp2));
    }

    @Test
    public void testFindLrAssignment() {
        LrAssignmentEntity assign =
            lrAssignmentService.findLrAssignment(efp1.getId(), lr.getId(), "slow");

        Assert.assertEquals("slow", assign.getValueName().getName());

        assign = lrAssignmentService
            .findLrAssignment(efp1.getId(), lr.getId(), "xyz");

        Assert.assertNull(assign);
    }
}
