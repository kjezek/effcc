package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import cz.zcu.kiv.efps.registry.server.service.impl.ValueNameServiceImpl;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.zcu.kiv.efps.registry.server.dao.GrDao;
import cz.zcu.kiv.efps.registry.server.dao.ValueNameDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;

/**
*
* This is a junit test for ValueNameServiceImpl.
* This test was generated at
* @author
*/
public class ValueNameServiceImplTest extends AbstractTestSupport  {

    /** A service under test. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** DAO.  */
    @Resource(name = "valueNameDao")
    private ValueNameDao valueNameDao;

    /** A service used in tests. */
    @Resource(name = "efpService")
    private EfpService efpService;


    /** A service under test. */
    @Resource(name = "grService")
    private GrService grService;

    /** An entity used in tests. */
    private GrEntity gr;

    /** An entity used in tests. */
    private ValueNameEntity name;

    /** Common test entity. */
    private EfpEntity efp1;



    @Before
    public void setUp() {
        gr = new GrEntity();
        gr.setName("Education systems");
        gr.setIdStr("ES");

        grService.save(gr);

        name = new ValueNameEntity();
        name.setName("slow");
        name.setGr(gr);
        valueNameService.save(name);

        efp1 = new EfpEntity();
        efp1.setName("EFP1");
        efp1.setGr(gr);
        efp1.setValueNames(Arrays.asList(name));
        efpService.save(efp1);

        valueNameDao.flush();
        valueNameDao.clear();
    }

    /**
     * @see cz.zcu.kiv.efps.registry.server.service.impl.ValueNameServiceImpl#findByName
     */
    @Test
    public void testFindByName() {
        ValueNameEntity result = valueNameService.findByName("slow", gr.getId());

        assertNotNull(result);
        assertEquals(name.getName(), result.getName());
    }

    /**
     * @see cz.zcu.kiv.efps.registry.server.service.impl.ValueNameServiceImpl#getByEfp
     */
    @Test
    public void testGetByEfp() {
        ValueNameEntity result = valueNameService.getByEfp("slow", efp1.getId());
        assertNotNull(result);
        assertEquals(name.getName(), result.getName());
    }

    /**
     * @see cz.zcu.kiv.efps.registry.server.service.impl.ValueNameServiceImpl#create
     */
    @Test
    public void testCreate() {
        ValueNameEntity result = valueNameService.create("high", gr.getId());

        assertNotNull(result);
        assertEquals("high", result.getName());
    }

    /**
     * @see cz.zcu.kiv.efps.registry.server.service.impl.ValueNameServiceImpl#findOrCreate
     */
    @Test
    public void testFindOrCreate() {

        // returns existing
        ValueNameEntity result = valueNameService.getByEfp("slow", efp1.getId());
        assertNotNull(result);
        assertEquals(name.getName(), result.getName());

        // creates new
        ValueNameEntity result2 = valueNameService.create("high", gr.getId());
        assertNotNull(result2);
        assertEquals("high", result2.getName());
    }



}
