/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.zcu.kiv.efps.registry.server.dao.GrDao;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.wsdto.GrDTO;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class GrServiceImplTest extends AbstractTestSupport {

    /** A service under test. */
    @Resource(name = "grService")
    private GrService grService;

    /** GR DAO. */
    @Resource(name = "grDao")
    private GrDao grDao;

    @Before
    public void setUp() {
        GrEntity gr = new GrEntity();
        gr.setName("Education systems");
        gr.setIdStr("ES");

        grService.save(gr);

        grDao.flush();
        grDao.clear();
    }

    @Test
    public void testUpdate() {

        GrEntity grOld = grService.findAll().get(0);
        grOld.setName("NEW GR1");

        grService.update(grOld);
        GrEntity grCheck = grService.getById(grOld.getId());

        Assert.assertEquals(grOld.getName(), grCheck.getName());
    }

}
