package cz.zcu.kiv.efps.registry.server.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.zcu.kiv.efps.registry.server.dao.EfpDao;
import cz.zcu.kiv.efps.registry.server.entities.EfpEntity;
import cz.zcu.kiv.efps.registry.server.entities.GrEntity;
import cz.zcu.kiv.efps.registry.server.entities.ValueNameEntity;
import cz.zcu.kiv.efps.registry.server.service.EfpService;
import cz.zcu.kiv.efps.registry.server.service.GrService;
import cz.zcu.kiv.efps.registry.server.service.ValueNameService;

/**
 * EFP Service Test.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpServiceImplTest extends AbstractTestSupport {

    /** A service used in tests. */
    @Resource(name = "efpService")
    private EfpService efpService;

    /** EFP DAO. */
    @Resource(name = "efpDao")
    private EfpDao efpDao;

    /** A service used in tests. */
    @Resource(name = "grService")
    private GrService grService;

    /** A service used in tests. */
    @Resource(name = "valueNameService")
    private ValueNameService valueNameService;

    /** Common test entity. */
    private GrEntity gr;

    /** Common test entity. */
    private EfpEntity efp1;

    /** Common test entity. */
    private EfpEntity efp2;

    @Before
    public void tearUp() {
        gr = new GrEntity();
        gr.setName("Education systems");
        gr.setIdStr("ES");
        grService.save(gr);

        ValueNameEntity valueName = new ValueNameEntity();
        valueName.setGr(gr);
        valueName.setName("slow");
        valueNameService.save(valueName);

        efp1 = new EfpEntity();
        efp1.setName("EFP1");
        efp1.setGr(gr);
        efp1.setValueNames(Arrays.asList(valueName));
        efpService.save(efp1);

        efp2 = new EfpEntity();
        efp2.setName("EFP2");
        efp2.setGr(gr);
        efpService.save(efp2);

        efpDao.flush();
        efpDao.clear();
    }

    /**
     * Test creates an EFP and then it tries to find it.
     */
    @Test
    public void testFindEFP() {

        EfpEntity result = efpService.findByName("EFP1", gr.getId());

        Assert.assertNotNull(result);
        Assert.assertEquals("EFP1", result.getName());
        Assert.assertEquals(new Integer(gr.getId()), result.getGr().getId());
    }

    @Test
    public void testFindEfpForGr() {

        List<EfpEntity> efps = efpService.findEfpForGr(gr.getId());

        Assert.assertTrue(efps.contains(efp1));
        Assert.assertTrue(efps.contains(efp2));
    }

    @Test
    public void testFindByValuName() {
        List<EfpEntity> efps = efpService.findByValuName(gr.getId(), "slow");

        Assert.assertTrue(efps.contains(efp1));
        Assert.assertFalse(efps.contains(efp2));
    }

}
