/**
 *
 */
package cz.zcu.kiv.efps.registry.server.service.impl;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * A base test class.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {
                "/applicationContext.xml",
                "/persistence.xml",
                "/test-placeholders.xml"
                })

@TransactionConfiguration
@Transactional
public abstract class AbstractTestSupport {

}
