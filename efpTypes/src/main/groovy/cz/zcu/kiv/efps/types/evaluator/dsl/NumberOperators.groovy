package cz.zcu.kiv.efps.types.evaluator.dsl

import cz.zcu.kiv.efps.types.datatypes.EfpNumber
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval

/**
 * This is a category that overrides math operators for java.lang.Number
 * to allow to operate with efp numbers or intervals.
 *
 * Every overridden operator returns EfpNumber or EfpInterval instance (based on an input parameter).
 *
 * Date: 6.6.2010
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
class NumberOperators {

  public static EfpNumber plus(Number number, EfpNumber efpNumber) {
    return new EfpNumber(number.toDouble()) + efpNumber;
  }

  public static EfpNumber minus(Number number, EfpNumber efpNumber) {
    return new EfpNumber(number.toDouble()) - efpNumber;
  }

  public static EfpNumber multiply(Number number, EfpNumber efpNumber) {
    return new EfpNumber(number.toDouble()) * efpNumber;
  }

  public static EfpNumber div(Number number, EfpNumber efpNumber) {
    return new EfpNumber(number.toDouble()) / efpNumber;
  }

  public static EfpNumberInterval plus(Number number, EfpNumberInterval efpNumber) {
    return new EfpNumberInterval(number.toDouble()) + efpNumber;
  }

  public static EfpNumberInterval minus(Number number, EfpNumberInterval efpNumber) {
    return new EfpNumberInterval(number.toDouble()) - efpNumber;
  }

  public static EfpNumberInterval multiply(Number number, EfpNumberInterval efpNumber) {
    return new EfpNumberInterval(number.toDouble()) * efpNumber;
  }

  public static EfpNumberInterval div(Number number, EfpNumberInterval efpNumber) {
    return new EfpNumberInterval(number.toDouble()) / efpNumber;
  }

}
