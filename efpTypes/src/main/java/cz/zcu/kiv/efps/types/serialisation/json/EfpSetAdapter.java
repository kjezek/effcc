/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation.json;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleSet;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleType;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * EFP Set - json adapter.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpSetAdapter
    implements JsonDeserializer<EfpSet> {

    /** {@inheritDoc} */
    @Override
    public EfpSet deserialize(
            final JsonElement json,
            final Type typeOfT,
            final JsonDeserializationContext context)  {

        List<EfpValueType> result = new LinkedList<EfpValueType>();
        List<EfpSimpleType<?>> simpleResult = new LinkedList<EfpSimpleType<?>>();

        JsonElement array = json.getAsJsonObject().get("efpSet");
        Iterator<JsonElement> iterator = array.getAsJsonArray().iterator();

        while (iterator.hasNext()) {
            JsonElement item = iterator.next();
            EfpValueType efpType = (EfpValueType) context.deserialize(item, EfpValueType.class);
            result.add(efpType);

            if (efpType instanceof EfpSimpleType) {
                simpleResult.add((EfpSimpleType<?>) efpType);
            }
        }

        EfpSet set;

        // if there are only simple results, create a simple set.
        if (simpleResult.size() == result.size()) {
            set = new EfpSimpleSet(simpleResult.toArray(new EfpSimpleType<?>[simpleResult.size()]));
        } else {
            set = new EfpSet(result.toArray(new EfpValueType[result.size()]));
        }

        return set;
    }

}
