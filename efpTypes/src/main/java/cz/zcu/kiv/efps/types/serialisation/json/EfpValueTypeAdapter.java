/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * Json - EfpValueType adapter.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpValueTypeAdapter implements JsonSerializer<EfpValueType>,
        JsonDeserializer<EfpValueType> {

    /** {@inheritDoc} */
    @Override
    public EfpValueType deserialize(final JsonElement json, final Type typeOfT,
            final JsonDeserializationContext context) {



        if (json.isJsonObject()) {

            Type type = null;

            JsonObject jsonObject = json.getAsJsonObject();
            if (jsonObject.has("efpBoolean")) {
                type = EfpBoolean.class;
            } else if (jsonObject.has("efpComplex")) {
                type = EfpComplex.class;
            } else if (jsonObject.has("efpEnum")) {
                type = EfpEnum.class;
            } else if (jsonObject.has("efpNumber")) {
                type = EfpNumber.class;
            } else if (jsonObject.has("efpString")) {
                type = EfpString.class;
            } else if (jsonObject.has("efpSet")) {
                type = EfpSet.class;
            } else if (jsonObject.has("low")) {
                type = EfpNumberInterval.class;
            }

            return context.deserialize(json, type);
        }

        return null;
    }

    /** {@inheritDoc} */
    @Override
    public JsonElement serialize(final EfpValueType src, final Type typeOfSrc,
            final JsonSerializationContext context) {

        return context.serialize(src, src.getClass());
    }

}
