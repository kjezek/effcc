package cz.zcu.kiv.efps.types.properties;

import cz.zcu.kiv.efps.types.gr.GR;


/**
 * A representation of a simple extra-functional property.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class SimpleEFP extends EFP {


    /** A type of this property - used for serialisation. */
    @SuppressWarnings("unused")
    private EfpType efpType = EfpType.SIMPLE;

    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private SimpleEFP() { }

    /**
     * Creates an extra-functional property.
     * @param id an ID for persisting purposes
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param gr a GR this property is assigned to
     */
    public SimpleEFP(final Integer id, final String name, final Class<?> valueType, final Meta meta,
            final Gamma gamma, final GR gr) {
        super(id, name, valueType, meta, gamma, gr);
    }

    /**
     * Creates an extra-functional property.
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param gr a GR this property is assigned to
     */
    public SimpleEFP(final String name,
            final Class<?> valueType, final Meta meta, final Gamma gamma,
            final GR gr) {
        super(name, valueType, meta, gamma, gr);
    }

    /** {@inheritDoc} */
    @Override
    public EfpType getType() {
        return EfpType.SIMPLE;
    }


}
