package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * This class implements a default comparing function for numbers.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class NumberGamma implements Gamma {

    /** {@inheritDoc} */
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        return (int) (((EfpNumber) o1).doubleValue()
                - ((EfpNumber) o2).doubleValue());
    }
}
