/**
 *
 */
package cz.zcu.kiv.efps.types.lr;


import java.util.List;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.AssignedValueFinder;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * An assignment of a derived value for an EFP in LR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class LrDerivedAssignment extends AbstractLrAssignment {


    /** An evaluator deriving a derived value. */
    private LrDerivedValueEvaluator evaluator;

    /**
     * @param id An ID serving for persisting purposes.
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param lr Link to LR
     * @param evaluator an evaluator deriving a derived value.
     */
    public LrDerivedAssignment(
            final Integer id,
            final EFP efp,
            final String valueName,
            final LrDerivedValueEvaluator evaluator,
            final LR lr) {

        super(id, efp, valueName, lr);
        this.evaluator = evaluator;
    }

    /**
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param lr Link to LR
     * @param evaluator an evaluator deriving a derived value.
     */
    public LrDerivedAssignment(
            final EFP efp,
            final String valueName,
            final LrDerivedValueEvaluator evaluator,
            final LR lr) {

        this(null, efp, valueName, evaluator, lr);
    }


    /**
     * This method derives a value for this derived assignment.
     * @param finder a callback interface which returns
     * @param assignments a list of deriving assignments
     * @return a computed derived value
     */
    public EfpValueType deriveValue(final AssignedValueFinder finder, List<LrAssignment> assignments) {
        return evaluator.evaluate(finder, assignments);
    }


    /**
     *
     * @return a derived value evaluator
     */
    public LrDerivedValueEvaluator getEvaluator() {
        return evaluator;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentType getAssignmentType() {
        return LrAssignmentType.DERIVED;
    }


    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "LrDerivedAssignment [getEfp()= +"  + getEfp() + ", getValueName()="
                + getValueName() + ", getLr()=" + getLr() + ", getEfpValue()=]";
    }

    @Override
    public String getLabel() {
        return evaluator.serialise();
    }


}
