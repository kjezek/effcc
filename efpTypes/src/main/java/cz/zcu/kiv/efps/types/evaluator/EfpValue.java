/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This class represents a pair with
 * EFP and its value.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpValue {

    /** EFP. */
    private EFP efp;

    /** A value realed to the EFP. */
    private EfpValueType value;

    /**
     * @param efp EFP
     * @param value a value related to the EFP
     */
    public EfpValue(final EFP efp, final EfpValueType value) {
        super();
        this.efp = efp;
        this.value = value;
    }

    /**
     * @return the efp
     */
    public EFP getEfp() {
        return efp;
    }

    /**
     * @return the value
     */
    public EfpValueType getValue() {
        return value;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpValue other = (EfpValue) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "EfpValue [efp=" + efp + ", value=" + value + "]";
    }



}
