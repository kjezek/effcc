package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * The default gamma function for strings. It results a zero when
 * two strings are literally equal. In other cases it cannot decide.
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class StringGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        if (o1 != null && o1.equals(o2)) {
            return 0;
        } else {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule.");
        }
    }
}
