package cz.zcu.kiv.efps.types.evaluator;

import cz.zcu.kiv.efps.types.lr.LrAssignment;

/**
 * This interface is implemented to compute
 * a rule deriving a derived EFP from simple ones.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrdDerivingRuleEvaluator {

    /**
     * Execute logical formula computing a derived property.
     * @param values pre-computed results of all participating EFPs.
     * @return a result of the execution of the formula
     * @param lrParticipatingAssignments LR assignments participating in the logical formula
     */
    Object evaluate(final EfpValue[] values,
            final LrAssignment... lrParticipatingAssignments);

}
