package cz.zcu.kiv.efps.types.datatypes;

import java.util.*;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.EnumGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;
//import org.apache.commons.lang.StringUtils;

/**
 * EFP enumeration type.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek <a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpEnum extends AbstractComparableType
    implements EfpSimpleType<String[]> {

    /** Items stored in this enumeration. */
    private String[] efpEnum;

    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpEnum() {

    }

    /**
     * Creates an instance.
     * @param items values to be stored in this enumeration.
     */
    public EfpEnum(final String... items) {
        this.efpEnum = items;
    }

    /**
     * Returns stored enumerations.
     * @return the list of enumerations.
     */
    public final String[] getItems() {
        return efpEnum;
    }

    /**
     * Returns a subenum following input names.
     * @param names the names to match with names of this enum
     * @return createdsub enum
     */
    public final EfpEnum getSubEnum(final String... names) {

        List<String> nameList = Arrays.asList(names);
        List<String> resultItems = new LinkedList<String>();
        for (final String item : efpEnum) {
            if (nameList.contains(item)) {
                resultItems.add(item);
            }
        }

        return new EfpEnum(resultItems.toArray(new String[resultItems.size()]));
    }

    /** {@inheritDoc} */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EfpEnum efpEnumObject = (EfpEnum) o;

        if (!Arrays.equals(efpEnum, efpEnumObject.efpEnum)) {
            return false;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public final int hashCode() {
        return efpEnum != null ? Arrays.hashCode(efpEnum) : 0;
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new EnumGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {

        StringBuffer buffer = new StringBuffer();
        buffer.append("enum {");
        for (String item : efpEnum) {
            buffer.append(item);
            buffer.append(", ");
        }
        buffer.append("}");

        return buffer.toString();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        if (efpEnum.length == 1) {
            return prefixNames + "=" + efpEnum[0];
        }

        String filter = "&";

        for (String val : efpEnum) {
            filter += " (" + prefixNames + "=" + val + ")";
        }

        return filter;
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();

        StringBuilder buffer = new StringBuilder();
        for (String item : efpEnum) {
            if (buffer.length() != 0) {
                buffer.append(",");
            }

            buffer.append(item);
        }

        map.put(prefixNames + ":List<String>", "\"" + buffer + "\"");
        return map;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public final String[] getValue() {
        return efpEnum;
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }

}
