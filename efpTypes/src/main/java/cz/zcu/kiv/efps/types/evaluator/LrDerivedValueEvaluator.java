/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import java.util.List;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LrAssignment;

/**
 * This interface is implemented to evaluate a value
 * of a derived EFP according to set of other EFPs and their values.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrDerivedValueEvaluator {


    /**
     * Execute logical formula computing a derived property.
     * @param finder a finder is used for loading concrete values
     * @param assignments the list of deriving assignments
     * for EFPs used in the deriving formula
     * @return a result of the execution of the formula
     */
    EfpValueType evaluate(AssignedValueFinder finder, List<LrAssignment> assignments);

    /**
     * This method serialises a formula stored in the evaluator.
     * @return the string representation of a formula
     */
    String serialise();
}
