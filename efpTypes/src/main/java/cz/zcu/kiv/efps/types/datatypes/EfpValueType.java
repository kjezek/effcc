package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.serialisation.SerialisableEfp;

import java.util.Map;

/**
 * A top level interface representing a data type of an extra-functional property.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public interface EfpValueType
    extends Comparable<EfpValueType>, SerialisableEfp {

    /**
     * The method sets a user defined gamma (comparing) function.
     * A comparing of two data types is performed along this function.
     * If the user defined gamma function is not set, a default gamma
     * function is used.
     * @param userGamma the user defined gamma function
     */
    void setUserGamma(Gamma userGamma);

    /**
     * This method returns a default comparing function.
     * @return the default function
     */
    Gamma getDefaultGamma();

    /**
     * Return a human-readable description of this data type.
     * @return a string for showing to users.
     */
    String getLabel();

    /**
     * Converts value into LDAP filter string.
     * @param prefixNames a prefix of names of value parts
     * @return a converted value in LDAP filter string
     */
    String convertIntoLDAPFilter(String prefixNames);

    /**
     * Converts value into map of parameters, for example for provide capabilities in OSGi.
     * @param prefixNames a prefix of names of value parts
     * @return A map with parameters of values. The key is name of parameter, value is part of value.
     */
    Map<String, String> convertIntoParameters(String prefixNames);
}
