package cz.zcu.kiv.efps.types.gr;


/**
 * The main class representing Global Registry.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek <a href="mailto:kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class GR {

    /** ID of GR. */
    private Integer id;
    /** A name of GR. */
    private String name;
    /** A string ID of GR */
    private String idStr;

    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private GR() { }

    /**
     * @param id ID of this GR
     * @param name a name of this GR
     * @param idStr a string ID of this GR
     */
    public GR(final Integer id, final String name, final String idStr) {
        super();
        this.id = id;
        this.name = name;
        this.idStr = idStr;
    }

    /**
     * @param name a name of this GR
     * @param idStr a string ID of this GR
     */
    public GR(final String name, String idStr) {
        super();
        this.name = name;
        this.idStr = idStr;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the string id
     */
    public String getIdStr() {
        return idStr;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode())+ ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GR other = (GR) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }

        if (idStr == null) {
            if (other.idStr != null) {
                return false;
            }
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "GR [id=" + id + ", name=" + name + ", idStr=" + idStr +"]";
    }



}
