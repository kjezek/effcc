/**
 *
 */
package cz.zcu.kiv.efps.types.datatypes;

import java.util.ArrayList;
import java.util.List;

/**
 * An restriction of the class {@link EfpSet}
 * allowing to hold only simple types.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpSimpleSet extends EfpSet implements EfpSimpleType<Object[]> {

    /**
     */
    private EfpSimpleSet() {

    }

    /**
     * Crates an instance.
     * @param items values wrapped in the set.
     */
    public EfpSimpleSet(final EfpSimpleType<?>... items) {
        super(items);
    }

    /** {@inheritDoc} */
    @Override
    public final Object[] getValue() {

        List<Object> items = new ArrayList<Object>();

        List<EfpValueType> types = super.getItems();
        for (final EfpValueType type : types) {
            // each item must be of the simple type
            items.add(((EfpSimpleType<?>) type).getValue());
        }

        return items.toArray();
    }




}
