package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.BooleanGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

import java.util.HashMap;
import java.util.Map;


/**
 * EFP Boolean data type.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpBoolean extends AbstractComparableType implements EfpSimpleType<Boolean> {

    /** A wrapped boolean value. */
    private Boolean efpBoolean;

    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpBoolean() {
    }

    /**
     * Cretes an instance.
     * @param value wrapped Boolean value
     */
    public EfpBoolean(final Boolean value) {
        this.efpBoolean = value;
    }

    /**
     * Return a wrapped Boolean value.
     * @return the value
     */
    public final Boolean getValue() {
        return efpBoolean;
    }

    /** {@inheritDoc} */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }

        EfpBoolean that;

        if (o instanceof EfpBoolean) {
            that = (EfpBoolean) o;
        } else  if (o instanceof Boolean) {
            that = new EfpBoolean((Boolean) o);
        } else {
            return false;
        }


        if (efpBoolean != null ? !efpBoolean.equals(that.efpBoolean) : that.efpBoolean != null) {
            return false;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public final int hashCode() {
        return efpBoolean != null ? efpBoolean.hashCode() : 0;
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new BooleanGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {
        return efpBoolean.toString();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        if (efpBoolean) {
            return prefixNames + "=true";
        } else {
            return prefixNames + "=false";
        }
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> parameters = new HashMap<String, String>();

        if (efpBoolean) {
            parameters.put(prefixNames, "true");
        } else {
            parameters.put(prefixNames, "false");
        }

        return parameters;
    }

    /** {@inheritDoc} */
    @Override
    public final  String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }
}
