package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * The default gamma function for boolean types. It compares
 * two instances as equal when they hold the same values.
 * In any other cases it cannot decide a result.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class BooleanGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        if (o1 != null && o1.equals(o2)) {
            return 0;
        } else {
            // by default, true is better then false
            EfpBoolean boolean1 = (EfpBoolean) o1;
            EfpBoolean boolean2 = (EfpBoolean) o2;

            return boolean1.getValue().compareTo(boolean2.getValue());
        }
    }
}
