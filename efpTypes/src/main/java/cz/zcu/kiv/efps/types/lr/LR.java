package cz.zcu.kiv.efps.types.lr;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;
import cz.zcu.kiv.efps.types.serialisation.SerialisableEfp;

/**
 * The main class representing Local Registry.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek <a href="mailto:kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class LR implements SerialisableEfp {

    /** ID. */
    private Integer id;
    /** A human readable name of this LR. */
    private String name;
    /** A string ID. */
    private String idStr;

    /** A reference to global registry. */
    private GR gr;
    /** A reference to local registry. */
    private LR parentLR;


    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private LR() { }

    /**
     * @param name a name of this LR
     * @param gr A reference to global registry
     * @param parentLR A reference to local registry
     * @param idStr A string ID of this LR
     */
    public LR(final String name, final GR gr, final LR parentLR, final String idStr) {
        super();
        this.name = name;
        this.gr = gr;
        this.parentLR = parentLR;
        this.idStr = idStr;
    }

    /**
     * @param id an unique ID
     * @param name a name of this LR
     * @param gr A reference to global registry
     * @param parentLR A reference to local registry
     * @param idStr A string ID of this LR
     */
    public LR(final Integer id, final String name, final GR gr, final LR parentLR, final String idStr) {
        super();
        this.id = id;
        this.name = name;
        this.gr = gr;
        this.parentLR = parentLR;
        this.idStr = idStr;
    }

    /**
     * @return A reference to global registry
     */
    public final GR getGr() {
        return gr;
    }

    /**
     * @return A reference to local registry
     */
    public final LR getParentLR() {
        return parentLR;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the string id
     */
    public String getIdStr() {return idStr; }


    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "LR [id=" + id + ", name=" + name + ", idStr=" + idStr +", gr=" + gr + ", parentLR="
                + parentLR + "]";
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((idStr == null) ? 0 : idStr.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LR other = (LR) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (idStr == null) {
            if (other.idStr != null) {
                return false;
            }
        } else if (!idStr.equals(other.idStr)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }






}
