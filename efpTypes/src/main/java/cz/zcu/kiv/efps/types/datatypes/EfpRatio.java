package cz.zcu.kiv.efps.types.datatypes;

/**
 * A percentile EFP type.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpRatio extends EfpNumber {

    /** A max value. */
    private static final int MAX_VALUE = 100;

    /** A serialized ID.  */
    private static final long serialVersionUID = 2558792484704492597L;

    /**
     * Creates a new ratio object.
     * @param value value must be between 0-100 percent
     */
    public EfpRatio(final double value) {
        super(value);

        if (value < 0 || value > MAX_VALUE) {
            throw new IllegalArgumentException("Value must be between 0-100. "
                     + "The set value has been " + value);
        }

    }

}
