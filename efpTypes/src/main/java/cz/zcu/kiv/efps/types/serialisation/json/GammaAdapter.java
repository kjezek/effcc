/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class GammaAdapter  implements JsonDeserializer<Gamma>,
        JsonSerializer<Gamma> {

    /** {@inheritDoc} */
    @Override
    public JsonElement serialize(
            final Gamma src,
            final Type typeOfSrc,
            final JsonSerializationContext context) {

        return context.serialize(src, src.getClass());
    }

    /** {@inheritDoc} */
    @Override
    public Gamma deserialize(
            final JsonElement json,
            final Type typeOfT,
            final JsonDeserializationContext context) {

        Gamma result = null;

        if (json.getAsJsonObject().has("function")) {
            result = new UserGamma(json.getAsJsonObject().get("function").getAsString());
        }

        return result;
    }

}
