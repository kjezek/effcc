/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class ClassAdapter
    implements JsonSerializer<Class<?>>,
    JsonDeserializer<Class<?>> {

    /**  {@inheritDoc} */
    @Override
    public Class<?> deserialize(final JsonElement json, final Type typeOfT,
            final JsonDeserializationContext context) throws JsonParseException {

        String className = json.getAsString();

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    /**  {@inheritDoc} */
    @Override
    public JsonElement serialize(final Class<?> src, final Type typeOfSrc,
            final JsonSerializationContext context) {

        return new JsonPrimitive(src.getName());
    }

}
