package cz.zcu.kiv.efps.types.datatypes;

/**
 * An interface for EFP simple type.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 *
 * @param <T> a type this simple type wraps.
 */
public interface EfpSimpleType<T> extends EfpValueType {

    /**
     * Returns a simple value of the type.
     * @return a java type value wrapped in the simple type.
     */
    T getValue();
}
