/**
 *
 */
package cz.zcu.kiv.efps.types.tools;


/**
 * A class supporting running of groovy scripts.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class GroovyScripts {

    /** A string used for connecting name of a feature and a property. */
    public static final String DELIMITER = "_";

    /** A regex used for filtering disallowed characters. */
    public static final String FORBIDDEN_CHARACTERS = "\\W";

    /** a private constructor. */
    private GroovyScripts() { }

    // TODO [kjezek, C] the script should not be hard coded, think of better way.
    /** A groovy script to evaluate math formulas. */
    public static final String GROOVY_SCRIPT =
        // support for math functions
        "import cz.zcu.kiv.efps.types.evaluator.dsl.NumberOperators; "
        // one may directly add any EFP type
        + "import cz.zcu.kiv.efps.types.datatypes.*; "
        //+ "import static java.lang.Math.*; "
        + "import static cz.zcu.kiv.efps.types.evaluator.dsl.EfpMath.*; "

        + "use (NumberOperators.class) {" // use category
        + "  return %s" // a formula goes here
        + "} ";



    /**
     * This method converts any string to may be safely used
     * in the groovy script as an identifier.
     * @param input the input string
     * @return a modified string.
     */
    public static String scriptSafeString(final String input) {
        return input.replaceAll(FORBIDDEN_CHARACTERS, DELIMITER);
    }

    /**
     * It work like a {@link String#replaceAll(String, String)} but
     * a pattern is not a regex. It is a plain text.
     * @param pattern the pattern
     * @param input an input string
     * @param replacement the replacement
     * @return a new modified string
     */
    public static String replaceAll(
            final String input,
            final String pattern,
            final String replacement) {

        String result = input;
        while (result.contains(pattern) && !pattern.equals(replacement)) {
            result = result.replace(pattern, replacement);
        }

        return result;
    }

}
