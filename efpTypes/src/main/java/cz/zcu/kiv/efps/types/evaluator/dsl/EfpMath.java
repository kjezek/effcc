/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator.dsl;

import cz.zcu.kiv.efps.types.datatypes.EfpNumber;

/**
 * An implementation of <code>java.util.Math</code>
 * for EFP numbers.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpMath {

    /** Private constructor. */
    private EfpMath() { }

    /**
     * This method delegates its computation to {@link Math#pow(double, double)}.
     * @param a the same as the delegated method in {@link Math}
     * @param b the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber pow(final Number a, final Number b) {
        return new EfpNumber(Math.pow(a.doubleValue(), b.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#abs(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber abs(final Number a) {
        return new EfpNumber(Math.abs(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#acos(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber acos(final Number a) {
        return new EfpNumber(Math.acos(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#asin(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber asin(final Number a) {
        return new EfpNumber(Math.asin(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#atan(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber atan(final Number a) {
        return new EfpNumber(Math.atan(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#atan2(double, double)}.
     * @param a the same as the delegated method in {@link Math}
     * @param b the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber atan2(final Number a, final Number b) {
        return new EfpNumber(Math.atan2(a.doubleValue(), b.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#cbrt(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber cbrt(final Number a) {
        return new EfpNumber(Math.cbrt(a.doubleValue()));
    }


    /**
     * This method delegates its computation to {@link Math#ceil(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber ceil(final Number a) {
        return new EfpNumber(Math.ceil(a.doubleValue()));
    }


    /**
     * This method delegates its computation to {@link Math#cos(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber cos(final Number a) {
        return new EfpNumber(Math.cos(a.doubleValue()));
    }


    /**
     * This method delegates its computation to {@link Math#cosh(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber cosh(final Number a) {
        return new EfpNumber(Math.cosh(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#exp(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber exp(final Number a) {
        return new EfpNumber(Math.exp(a.doubleValue()));
    }


    /**
     * This method delegates its computation to {@link Math#expm1(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber expm1(final Number a) {
        return new EfpNumber(Math.expm1(a.doubleValue()));
    }


    /**
     * This method delegates its computation to {@link Math#floor(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber floor(final Number a) {
        return new EfpNumber(Math.floor(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#hypot(double, double)}.
     * @param a the same as the delegated method in {@link Math}
     * @param b the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber hypot(final Number a, final Number b) {
        return new EfpNumber(Math.hypot(a.doubleValue(), b.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#log(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber ln(final Number a) {
        return new EfpNumber(Math.log(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#log10(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber log(final Number a) {
        return new EfpNumber(Math.log10(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#log1p(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber log1p(final Number a) {
        return new EfpNumber(Math.log1p(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#max(double, double)}.
     * @param a the same as the delegated method in {@link Math}
     * @param b the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber max(final Number a, final Number b) {
        return new EfpNumber(Math.max(a.doubleValue(), b.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#min(double, double)}.
     * @param a the same as the delegated method in {@link Math}
     * @param b the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber min(final Number a, final Number b) {
        return new EfpNumber(Math.min(a.doubleValue(), b.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#random()}.
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber random() {
        return new EfpNumber(Math.random());
    }

    /**
     * This method delegates its computation to {@link Math#rint(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber rint(final Number a) {
        return new EfpNumber(Math.rint(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#round(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber round(final Number a) {
        return new EfpNumber(Math.round(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#signum(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber signum(final Number a) {
        return new EfpNumber(Math.signum(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#sin(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber sin(final Number a) {
        return new EfpNumber(Math.sin(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#sinh(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber sinh(final Number a) {
        return new EfpNumber(Math.sinh(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#sqrt(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber sqrt(final Number a) {
        return new EfpNumber(Math.sqrt(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#tan(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber tan(final Number a) {
        return new EfpNumber(Math.tan(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#tanh(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber tanh(final Number a) {
        return new EfpNumber(Math.tanh(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#toDegrees(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber toDegrees(final Number a) {
        return new EfpNumber(Math.toDegrees(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#toRadians(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber toRadians(final Number a) {
        return new EfpNumber(Math.toRadians(a.doubleValue()));
    }

    /**
     * This method delegates its computation to {@link Math#ulp(double)}.
     * @param a the same as the delegated method in {@link Math}
     * @return the same as the delegated method in {@link Math}
     */
    public static EfpNumber ulp(final Number a) {
        return new EfpNumber(Math.ulp(a.doubleValue()));
    }

}
