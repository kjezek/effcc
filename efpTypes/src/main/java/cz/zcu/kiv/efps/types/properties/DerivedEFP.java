package cz.zcu.kiv.efps.types.properties;

import java.util.Arrays;
import java.util.List;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.serialisation.json.EfpAdapter;

/**
 * A representation of derived extra-functional property.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class DerivedEFP extends EFP {

    /** A list of properties this derived property is derived from. */
    private List<EFP> efps;

    /** A type of this property - used for serialisation. */
    @SuppressWarnings("unused")
    private EfpType efpType = EfpType.DERIVED;

    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private DerivedEFP() { }

    /**
     * Creates an extra-functional property.
     * @param id an ID for persisting purposes
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param  efps a list of properties this derived property is derived from
     * @param gr a GR this property is assigned to
     */
    public DerivedEFP(
            final Integer id,
            final String name,
            final Class<?> valueType,
            final Meta meta,
            final Gamma gamma,
            final GR gr,
            final EFP... efps) {

        super(id, name, valueType, meta, gamma, gr);
        this.efps = Arrays.asList(efps);
    }

    /**
     * Creates an extra-functional property.
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param gr a GR this property is assigned to
     * @param  efps a list of properties this derived property is derived from
     */
    public DerivedEFP(final String name, final Class<?> valueType, final Meta meta,
            final Gamma gamma, final GR gr,
            final EFP... efps) {
        this(null, name, valueType, meta, gamma, gr, efps);
    }


    /**
     * Returns extra-functional properties from which this property is composed.
     * @return a list of composing extra-functional properties.
     */
    public final List<EFP> getEfps() {
        return efps;
    }

    /** {@inheritDoc} */
    @Override
    public EfpType getType() {
        return EfpType.DERIVED;
    }
}
