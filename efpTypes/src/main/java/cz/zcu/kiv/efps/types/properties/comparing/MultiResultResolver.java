package cz.zcu.kiv.efps.types.properties.comparing;

/**
 * This class collects results from a sequential comparing of values.
 * It first stores results for positive and negative values separately.
 * These results may be then used for evaluating which of the result (positive or negative)
 * is greater.
 *
 * Date: 17.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class MultiResultResolver {

    /** Number of positive comparing. */
    private int pos = 0;
    /** Number of negative comparing. */
    private int neg = 0;
    /** A result of all comparing. */
    private int result = 0;

    /**
     * Compares two types and store the comparing result.
     * @param type1 first type
     * @param type2 second type
     * @param <T> a type to be compared.
     */
    public final <T> void compare(final Comparable<T> type1, final T type2) {

        int cmpRes = type1.compareTo(type2);
        result += cmpRes;
        pos += (cmpRes > 0) ? cmpRes : 0;
        neg += (cmpRes < 0) ? cmpRes : 0;
    }

    /**
     * @return A result of all comparing
     */
    public final int getResult() {
        return result;
    }

    /**
     * @return Number of positive comparing
     */
    public final int getPos() {
        return pos;
    }

    /**
     * @return Number of negative comparing
     */
    public final int getNeg() {
        return neg;
    }
}
