/**
 *
 */
package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;

import java.util.Map;

/**
 * A convenient implementation of {@link AbstractComparableType}.
 * This class basically wraps another type and delegates
 * calls of its methods to calls of the wrapped class.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class DefaultComparableType extends AbstractComparableType {

    /** A wrapped type. */
    private EfpValueType wrappedType;

    /**
     * Creates an instance.
     * @param wrappedType a type that is wrapped by this class.
     */
    public DefaultComparableType(final EfpValueType wrappedType) {
        super(wrappedType);
        this.wrappedType = wrappedType;
    }

    /** {@inheritDoc}*/
    @Override
    public final Gamma getDefaultGamma() {
        return wrappedType.getDefaultGamma();
    }

    /** {@inheritDoc}*/
    @Override
    public final String getLabel() {
        return wrappedType.getLabel();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        return wrappedType.convertIntoLDAPFilter(prefixNames);
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        return wrappedType.convertIntoParameters(prefixNames);
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return wrappedType.serialise();
    }

}
