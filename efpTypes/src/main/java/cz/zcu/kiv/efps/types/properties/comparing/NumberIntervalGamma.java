package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * The default gamma function for number intervals.
 *
 * Date: 20.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class NumberIntervalGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        int result = -1;
        EfpNumberInterval int1 = (EfpNumberInterval) o1;
        EfpNumberInterval int2 = (EfpNumberInterval) o2;

        if (int1.getLow().equals(int2.getLow())
                && int1.getHigh().equals(int2.getHigh())) {

            result = 0;
        } else
        if (Math.signum(int1.getLow().compareTo(int2.getLow()))
                == Math.signum(int1.getHigh().compareTo(int2.getHigh()))) {

            result = int1.getLow().compareTo(int2.getLow());
        }

        // [TODO, kjezek] -  other cases not finished yet

        return result;
    }
}
