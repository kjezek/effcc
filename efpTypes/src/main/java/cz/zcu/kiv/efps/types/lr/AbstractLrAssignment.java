/**
 *
 */
package cz.zcu.kiv.efps.types.lr;

import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * A base class covering an assignment of a value to an EFP in LR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class AbstractLrAssignment implements LrAssignment {

    /** An ID serving for persisting purposes. */
    private Integer id;
    /** An EFP a value is assigned to. */
    private EFP efp;
    /** Name of the assigned value.  */
    private String valueName;
    /** Link to LR.  */
    private LR lr;


    /**
     * @param id An ID serving for persisting purposes.
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param efpValue A value stored in LR
     * @param lr Link to LR
     */
    public AbstractLrAssignment(
            final Integer id,
            final EFP efp,
            final String valueName,
            final LR lr) {

        super();
        this.id = id;
        this.efp = efp;
        this.valueName = valueName;
        this.lr = lr;
    }

    /**
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param efpValue A value stored in LR
     * @param lr Link to LR
     */
    public AbstractLrAssignment(
            final EFP efp,
            final String valueName,
            final LR lr) {

    }

    /**
     * @return An EFP a value is assigned to.
     */
    public final EFP getEfp() {
        return efp;
    }

    /**
     * @return Name of the assigned value.
     */
    public final String getValueName() {
        return valueName;
    }

    /**
     * @return Link to LR.
     */
    public final LR getLr() {
        return lr;
    }

    @Override
    public String toString() {
        return "AbstractLrAssignment [id=" + id + ", efp=" + efp
                + ", valueName=" + valueName + ", lr=" + lr + "]";
    }

    /** {@inheritDoc} */
    @Override
    public Integer getId() {
        return id;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result + ((lr == null) ? 0 : lr.hashCode());
        result = prime * result
                + ((valueName == null) ? 0 : valueName.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractLrAssignment other = (AbstractLrAssignment) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (lr == null) {
            if (other.lr != null) {
                return false;
            }
        } else if (!lr.equals(other.lr)) {
            return false;
        }
        if (valueName == null) {
            if (other.valueName != null) {
                return false;
            }
        } else if (!valueName.equals(other.valueName)) {
            return false;
        }
        return true;
    }



}
