package cz.zcu.kiv.efps.types.datatypes;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.ComplexTypeGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * A representation of a complex type of an extra-functional property.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpComplex extends AbstractComparableType {

    /** A map of the stored values. */
    private Map<String, EfpValueType> efpComplex;

    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpComplex() {

    }

    /**
     * Creates an instance.
     * @param items a map that is used to fill in this complex type.
     */
    public EfpComplex(final Map<String, EfpValueType> items) {
        this.efpComplex = new HashMap<String, EfpValueType>(items);
    }

    /**
     * Returns a stored values.
     * @return the stored values.
     */
    public final Map<String, EfpValueType> getItems() {
        return new HashMap<String, EfpValueType>(efpComplex);
    }

    /** {@inheritDoc} */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EfpComplex that = (EfpComplex) o;

        if (efpComplex != null ? !efpComplex.equals(that.efpComplex) : that.efpComplex != null) {
            return false;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public final int hashCode() {
        return efpComplex != null ? efpComplex.hashCode() : 0;
    }

    /**
     * The method returns all types stored in this complex type.
     * @return a collection of values
     */
    public final Collection<EfpValueType> getTypes() {
        return efpComplex.values();
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new ComplexTypeGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {

        StringBuffer buffer = new StringBuffer();

        buffer.append("complex {");
        for (Map.Entry<String, EfpValueType> entry : efpComplex.entrySet()) {
            buffer.append(entry.getKey());
            buffer.append(" : ");
            buffer.append(entry.getValue());
            buffer.append(", ");
        }
        buffer.append("}");

        return buffer.toString();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        String filter = "&";

        for (Map.Entry<String, EfpValueType> entry : efpComplex.entrySet()) {
            filter += " (" + entry.getValue().convertIntoLDAPFilter(prefixNames
                    + "_" + entry.getKey()) + ")";
        }

        return filter;
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();

        for (Map.Entry<String, EfpValueType> entry : efpComplex.entrySet()) {
            Map<String, String> subMap = entry.getValue().convertIntoParameters(prefixNames
                    + "_" +  entry.getKey());
            map.putAll(subMap);
        }

        return map;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }

}
