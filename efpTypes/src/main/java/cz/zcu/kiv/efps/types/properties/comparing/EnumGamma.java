package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * It is a default gamma function for enumerations.
 * It evaluates each item of the underlying items and evaluates partial results.
 * The result is a result of each evaluation unless they hold different values.
 * In a case of different partial results, the function cannot decide.
 *
 * Date: 17.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EnumGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        MultiResultResolver result = new MultiResultResolver();

        EfpEnum enum1 = (EfpEnum) o1;
        EfpEnum enum2 = (EfpEnum) o2;

        if (enum1.getItems().length != enum2.getItems().length) {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule. "
                    + "Enums have different count of items.");
        }

        for (int i = 0; i < enum1.getItems().length; i++) {
            String value1 = enum1.getItems()[i];
            String value2 = enum2.getItems()[i];

            result.compare(value1, value2);
        }


        if (result.getPos() != 0 && result.getNeg() != 0) {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule. "
                    + "The comparing of each item has found "
                    + "a combination of positive and negative results. "
                    + "The comparing is ambiguous.");
        }

        return result.getResult();
    }
}
