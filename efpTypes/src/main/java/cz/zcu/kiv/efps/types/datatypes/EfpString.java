package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.StringGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

import java.util.HashMap;
import java.util.Map;

/**
 * An efp String.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpString extends AbstractComparableType implements EfpSimpleType<String> {

    /** A wrapped string. */
    private String efpString;

    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpString() {
    }

    /**
     * Create a new string data type.
     * @param text a text
     */
    public EfpString(final String text) {
        this.efpString = text;
    }

    /** {@inheritDoc} */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        EfpString efpStringObject;

        if (o instanceof EfpString) {
            efpStringObject = (EfpString) o;
        } else if (o instanceof String) {
            efpStringObject = new EfpString(String.valueOf(o));
        } else {
            return false;
        }


        if (efpString != null
                ? !efpString.equals(efpStringObject.efpString)
                : efpStringObject.efpString != null) {

            return false;
        }

        return true;
    }



    /** {@inheritDoc} */
    @Override
    public final int hashCode() {
        return efpString != null ? efpString.hashCode() : 0;
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new StringGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {
        return efpString;
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        return prefixNames + "=" + efpString + "";
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(prefixNames + ":String", "\"" + efpString + "\"");

        return map;
    }

    /** {@inheritDoc} */
    @Override
    public final String getValue() {
        return efpString;
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getLabel();
    }
}

