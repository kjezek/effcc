/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;


import groovyjarjarantlr.StringUtils;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * This is a helper class allowing
 * helpful operation upon the logical formulas.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LogicalFormulaHelper {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** A logical formula. */
    private String logicalFormula;

    /**
     * @param logicalFormula a logical formula
     */
    public LogicalFormulaHelper(
            final String logicalFormula) {

        this.logicalFormula = logicalFormula;
    }

    /**
     * This method finds which assignments are actually used
     * in the formula according to input EFPs.
     * @param efps a list of EFPs which are being find in the formula
     * @return a list of assignments.
     */
    public List<EfpLrAssignment> findUsedAssignments(final List<EFP> efps) {


        List<EfpLrAssignment> result = new LinkedList<EfpLrAssignment>();

        if (logger.isDebugEnabled()) {
            logger.debug("Determining if EFPs " + efps + " are used in the formula "
                    + logicalFormula);
        }

        // go through all efps and names
        for (EFP efp : efps) {

            if (logicalFormula != null && logicalFormula.contains(efp.getName())) {

                if (logger.isTraceEnabled()) {
                    logger.trace("EFP " + efp + " is included in the formula. ");
                }

                EfpLrAssignment efplr = new EfpLrAssignment(efp);

                for (String valName : efp.getMeta().getNames()) {

                    String name = efp.getName() + GroovyScripts.DELIMITER + valName;
                    if (logicalFormula.contains(name)) {

                        if (logger.isTraceEnabled()) {
                            logger.trace("LR name  " + name + " is included in the formula. ");
                        }

                        efplr.getLrValueNames().add(valName);
                    }
                }

                result.add(efplr);
            }
        }

        return result;
    }
}
