/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpAdapter implements JsonDeserializer<EFP>, JsonSerializer<EFP> {

    /** {@inheritDoc} */
    @Override
    public JsonElement serialize(
            final EFP src,
            final Type typeOfSrc,
            final JsonSerializationContext context) {

        return context.serialize(src, src.getClass());
    }

    /** {@inheritDoc} */
    @Override
    public EFP deserialize(
            final JsonElement json,
            final Type typeOfT,
            final JsonDeserializationContext context) {

        String efpType = json.getAsJsonObject().get("efpType").getAsString();

        EFP result = null;

        if (EFP.EfpType.DERIVED.name().equals(efpType)) {
            result = context.deserialize(json, DerivedEFP.class);
        }

        if (EFP.EfpType.SIMPLE.name().equals(efpType)) {
            result = context.deserialize(json, SimpleEFP.class);
        }

        return result;
    }

}
