/**
 *
 */
package cz.zcu.kiv.efps.types.properties.comparing;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleType;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * This class allows to define a user gamma function.
 *
 * The script is defined as a groovy script. The script
 * is evaluated and it must result in an integer value
 * determining the result of the comparing.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class UserGamma implements Gamma {

    /** A name of the first argument in the script.  */
    public static final String FIRST_EFP_VALUE_NAME = "first";

    /** A name of the second argument in the script.  */
    public static final String SECOND_EFP_VALUE_NAME = "second";

    /** A 'return' keyword. */
    private static final String RETURN = "return";

    // TODO [kjezek, C] the script should not be hard coded, think of better way.
    /** A groovy script to evaluate math formulas. */
    private static final String GROOVY_SCRIPT =
        "import cz.zcu.kiv.efps.types.datatypes.*; "
        //+ "import static java.lang.Math.*; "
        + "  %s %s" // a formula goes here
        + "";

    /** A user defined gamma function. */
    private String function;

    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private UserGamma() { }

    /**
     * Creates a user defined gamma function.
     *
     * @param function a function comparing two EFPs. The result of the
     * function must be a type of Integer
     */
    public UserGamma(final String function) {
        this.function = function;
    }


    /** {@inheritDoc} */
    @Override
    public int compare(final EfpValueType o1, final EfpValueType o2) {

        Object value1 = o1;
        Object value2 = o2;

        // Simple types are changed to
        // java types.
        // It allows a combination of
        // java types with simple EFP types
        // Eg. math operations, comparing
        if (o1 instanceof EfpSimpleType) {
            value1 = ((EfpSimpleType<?>) o1).getValue();
        }
        if (o2 instanceof EfpSimpleType) {
            value2 = ((EfpSimpleType<?>) o2).getValue();
        }

        Binding binding = new Binding();
        binding.setVariable(FIRST_EFP_VALUE_NAME, value1);
        binding.setVariable(SECOND_EFP_VALUE_NAME, value2);

        GroovyShell groovyShell = new GroovyShell(binding);

        // determine whether the word 'return'
        // is in the formula
        String returnKeyword = RETURN;
        if (function.contains(RETURN)) { // return is already in the function
            returnKeyword = "";
        }

        String script = String.format(GROOVY_SCRIPT, returnKeyword, function);
        // it is up to a user the formula is a correct groovy script.
        Object result = groovyShell.evaluate(script);

        // comparing  may result only to Integer
        if (!(result instanceof Number)) {
            throw new IllegalArgumentException("The function '" + function + "' "
                    + "does not results to Integer. Such comparing function "
                    + "is not allowd.");
        }

        return ((Number) result).intValue();
    }

    /**
     * @return the function
     */
    public String getFunction() {
        return function;
    }


    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((function == null) ? 0 : function.hashCode());
        return result;
    }


    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserGamma other = (UserGamma) obj;
        if (function == null) {
            if (other.function != null) {
                return false;
            }
        } else if (!function.equals(other.function)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "UserGamma [function=" + function + "]";
    }




}
