/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This interface severs as a callback for
 * finding a concrete value assigned to an EFP.
 *
 * It is used when a derived value is computed. Typically
 * an EFP has assigned a deriving rule concerning
 * other EFPs. However, values of these EFPs are known
 * only as the EFPs are applied on components.
 *
 * It that phase this interface is used to load
 * concrete values assigned for EFPs on a component.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface AssignedValueFinder {


    /**
     * It returns a value assigned for an EFP.
     * @param efp the EFP
     * @return the value
     */
    EfpValueType findValue(EFP efp);
}
