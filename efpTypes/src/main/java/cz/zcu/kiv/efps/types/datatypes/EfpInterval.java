package cz.zcu.kiv.efps.types.datatypes;

/**
 * This interface represents an interval that may be assigned
 * to an extra-functional property.
 *
 * Date: 6.6.2010
 * @param <T> type of the inreval.
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public interface EfpInterval<T> extends EfpValueType {

    /**
     * Returns a low value of the interval.
     * @return a low value
     */
    T getLow();

    /**
     * Returns a high value of the interval.
     * @return a high value
     */
    T getHigh();

}
