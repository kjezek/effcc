/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation;

/**
 * This interface should be implemented by all
 * classes allowing to serialize their content
 * into a plain string.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface SerialisableEfp {

    /**
     * This method serializes the value into
     * a string.
     * @return the serialized string
     */
    String serialise();

}
