/**
 *
 */
package cz.zcu.kiv.efps.types.lr;

import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * A base interface covering an assignment of a value to an EFP in LR.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface LrAssignment {

    /** Possible types of assignment in LR. */
    enum LrAssignmentType {
        /** an assignment for simple EFPs. */
        SIMPLE,
        /** an assignment for derived properties. */
        DERIVED
        }

    /**
     * @return An EFP a value is assigned to.
     */
    EFP getEfp();

    /**
     * @return Name of the assigned value.
     */
    String getValueName();

    /**
     * @return Link to LR.
     */
    LR getLr();

    /**
     *
     * @return a type of a concrete assignment.
     */
    LrAssignmentType getAssignmentType();

    /**
     * @return An ID serving for persisting purposes.
     */
    Integer getId();

    /**
     * A human readable label of the assignment.
     * @return A human readable label of the assignment.
     */
    String getLabel();

}
