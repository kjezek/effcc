/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import java.util.LinkedList;
import java.util.List;

import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * This class contains an EFP
 * and its LR assignments.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpLrAssignment {

    /** EFP. */
    private EFP efp;

    /** A list of LR names related to the EFP. */
    private List<String> lrValueNames = new LinkedList<String>();

    /**
     * @param efp EFP.
     */
    public EfpLrAssignment(final EFP efp) {
        super();
        this.efp = efp;
    }

    /**
     * @return the efp
     */
    public EFP getEfp() {
        return efp;
    }

    /**
     * @return the lrAssignments
     */
    public List<String> getLrValueNames() {
        return lrValueNames;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efp == null) ? 0 : efp.hashCode());
        result = prime * result
                + ((lrValueNames == null) ? 0 : lrValueNames.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EfpLrAssignment other = (EfpLrAssignment) obj;
        if (efp == null) {
            if (other.efp != null) {
                return false;
            }
        } else if (!efp.equals(other.efp)) {
            return false;
        }
        if (lrValueNames == null) {
            if (other.lrValueNames != null) {
                return false;
            }
        } else if (!lrValueNames.equals(other.lrValueNames)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "EfpLrAssignment [efp=" + efp + ", lrAssignments="
                + lrValueNames + "]";
    }



}
