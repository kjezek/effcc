package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * It is an abstract implementation of comparable data types. It allows
 * to compare two instances of one data type by default rule or by
 * a user defined comparing function.
 *
 * Date: 17.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public abstract class AbstractComparableType implements EfpValueType {

    /** A user defined comparing function. */
    private Gamma userGamma;

    /** An optional wrapped type. */
    private EfpValueType wrappedType;

    /**
     * A simple constructor that does not provide a wrapped type.
     */
    public AbstractComparableType() {
        this(null);
    }

    /**
     * Crates an instance with providing wrapped EFP type.
     * Once the wrapped type is provide, the calls of the method <code>compareTo</code>
     * perform on this type rather than <code>this</code> class.
     * @param wrappedType the wrapped type.
     */
    public AbstractComparableType(final EfpValueType wrappedType) {
        super();
        this.wrappedType = wrappedType;
    }



    /**
     * The method sets a user defined gamma (comparing) function.
     * A comparing of two data types is performed along this function.
     * If the user defined gamma function is not set, a default gamma
     * function is used.
     * @param userGamma the user defined gamma function
     */
    public final void setUserGamma(final Gamma userGamma) {
        this.userGamma = userGamma;
    }

    /** {@inheritDoc} */
    @Override
    public final int compareTo(final EfpValueType o) {

        Gamma gamma = getDefaultGamma();

        if (userGamma != null) {
            gamma = userGamma;
        }

        EfpValueType comparingType;

        if (wrappedType == null) {
            comparingType = this;
        } else {
            comparingType = wrappedType;
        }

        return gamma.compare(comparingType, o);
    }
}
