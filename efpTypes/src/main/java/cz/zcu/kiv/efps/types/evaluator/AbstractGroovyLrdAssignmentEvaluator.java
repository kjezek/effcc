/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleType;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * This class evaluates logical formula.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public abstract class AbstractGroovyLrdAssignmentEvaluator implements LrdDerivingRuleEvaluator {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** A formula to be evaluated as a part of a Groovy script. */
    private String formula;


    /** A callback interface to find values for deriving EFPs. */
    private AssignedValueFinder finder;

    /**
     * A constructor.
     * @param formula a formula to be evaluated as a part of a groovy script.
     * @param finder A callback interface to find values for deriving EFP
     */
    public AbstractGroovyLrdAssignmentEvaluator(
            final String formula,
            final AssignedValueFinder finder) {
        this.formula = formula;
        this.finder = finder;
    }

    /**
     * This method will return a groovy script
     * in which a formula will be put and the script will be evaluated.
     *
     * The script should contain one pair of the '%s' characters to mark
     * the place where the formula will be put.
     * @return a string representation of the groovy script
     */
    public abstract String getGroovyScript();


    /** {@inheritDoc} */
    @Override
    public Object evaluate(
            final EfpValue[] values,
            final LrAssignment... lrParticipatingAssignments) {

        if (logger.isDebugEnabled()) {
            logger.debug("Evaluating formula " + formula + " for values "
                    + values + " and other LR assignments " + lrParticipatingAssignments);
        }

        Binding binding = new Binding();
        String safeFormula = formula;
        // bind results of other properties in formula.
        for (EfpValue value : values) {

            // a name must match the name in the math formula
            String valueName = value.getEfp().getName();
            valueName = GroovyScripts.scriptSafeString(valueName);

            if (logger.isTraceEnabled()) {
                logger.trace("EFP name " + value.getEfp().getName() + " "
                        + "translated as " + valueName);
            }

            String safeValueName = GroovyScripts.scriptSafeString(valueName);
            safeFormula = GroovyScripts.replaceAll(safeFormula, valueName, safeValueName);

            EfpValueType efpType = value.getValue();

            Object javaType = retunJavaOrEfpType(efpType);

            if (logger.isTraceEnabled()) {
                logger.trace("Setting value " + javaType + " for variable " + valueName);
            }

            binding.setVariable(valueName, javaType);
        }

        // bind named values from LR
        for (final LrAssignment partAssig : lrParticipatingAssignments) {

            String propertyName = partAssig.getEfp().getName();
            String valueName = partAssig.getValueName();
            EfpValueType efpType;

            if (LrAssignment.LrAssignmentType.SIMPLE == partAssig.getAssignmentType()) {
                efpType = ((LrSimpleAssignment) partAssig).getEfpValue();
            } else {
                efpType = ((LrDerivedAssignment) partAssig).deriveValue(finder,
                        Arrays.asList(lrParticipatingAssignments));

            }

            if (efpType == null) {
                throw new IllegalArgumentException("A nested assignment returned a null value."
                        + " Cannot evaluate formula " + formula
                        + " A problematic assginment is: " + partAssig);
            }


            if (logger.isTraceEnabled()) {
                logger.trace("Binding related LR Assignment "
                        + "EFP: " + propertyName + ", value name: " + valueName
                        + "Value: " + efpType);
            }
            Object javaType = retunJavaOrEfpType(efpType);


            binding.setVariable(
                    propertyName + GroovyScripts.DELIMITER + valueName,
                    javaType);
        }

        GroovyShell groovyShell = new GroovyShell(binding);

        if (logger.isDebugEnabled()) {
            logger.debug("Formula is " + safeFormula);
        }

        String script = String.format(GroovyScripts.GROOVY_SCRIPT, safeFormula);

        if (logger.isDebugEnabled()) {
            logger.debug("Excuting script " + script);
        }


        // it is up to a user the formula is a correct groovy script.
        Object result = groovyShell.evaluate(script);

        if (logger.isDebugEnabled()) {
            logger.debug("Result of the evaluation " + result);
        }

        return result;
    }

    /**
     * This method gets an EFP type and converts it to a Java type
     * if it is possible. Otherwise it returns the original type.
     * @param efpType the EFP type
     * @return java type or the same EFP type
     */
    private static Object retunJavaOrEfpType(final EfpValueType efpType) {
        Object javaType = null;

        // for the purposes of comparing
        // EfpTypes with Java types
        // we must convert EfpTypes to java types.
        // It basically means unwrap a value in the simple type
        // Complex type are kept without change, because
        // they are in essence incomparable with java types.
        if (efpType instanceof EfpSimpleType) {
            javaType = ((EfpSimpleType<?>) efpType).getValue();
        } else {
            javaType = efpType;
        }

        return javaType;
    }

}
