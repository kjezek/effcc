/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * This kind of evaluator computes constraint
 * upon a value. The semantics is that
 * an expected value is valid as long as
 * a constraint holds.
 *
 * The constraint is expressed as a logical formula
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class LrConstraintEvaluator implements LrDerivedValueEvaluator {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** An object allowing to compute a deriving rule. */
    private LrdDerivingRuleEvaluator rule;

    /** A logical rule. */
    private String logicalRule;


    /** A list of deriving EFPs. */
    private List<EFP> efps;

    /** A value to be returned if a logical rule is fullfiled. */
    private EfpValueType expectedValue;

    /**
     * It creates a new evaluator.
     * @param logicalRule a logical formula conditioning a validity
     * of an expected
     * @param expectedValue the expected value
     * @param efps a list of EFPs used in the formula
     * @param assignments a list of other LR Assignments used in the formula
     */
    public LrConstraintEvaluator(
            final String logicalRule,
            final EfpValueType expectedValue,
            final List<EFP> efps) {

        this.logicalRule = logicalRule;
        this.efps = efps;
        this.expectedValue = expectedValue;
    }

    /** {@inheritDoc} */
    @Override
    public EfpValueType evaluate(final AssignedValueFinder finder,
            final List<LrAssignment> assignments) {

        rule = new AbstractGroovyLrdAssignmentEvaluator(logicalRule, finder) {
            @Override
            public String getGroovyScript() {
                return GroovyScripts.GROOVY_SCRIPT;
            }
        };

        List<EfpValue> values = new LinkedList<EfpValue>();

        for (EFP efp : efps) {
            EfpValueType type = finder.findValue(efp);

            // A sub-value to compute the formula is missing
            // ->> the result of the whole computation is also null.
            if (type == null) {
                return null;
            }

            values.add(new EfpValue(efp, type));
        }

        Object condition = rule.evaluate(
                values.toArray(new EfpValue[values.size()]),
                assignments.toArray(new LrAssignment[assignments.size()]));

        if (!(condition instanceof Boolean)) {
            throw new IllegalArgumentException("A logical expression "
                    + logicalRule + " does not result in Boolean. ");
        }

        EfpValueType result = null;

        if (Boolean.TRUE.equals(condition)) {
            result = expectedValue;

            logger.debug("A logical formula " + logicalRule
                    + " is fullfilled. Returning value: {}", result);
        }

        return result;
    }

    /**
     * @return the rule
     */
    public LrdDerivingRuleEvaluator getRule() {
        return rule;
    }

    /**
     * @return the logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * @return the logicalRule
     */
    public String getLogicalRule() {
        return logicalRule;
    }

    /**
     * @return the efps
     */
    public List<EFP> getEfps() {
        return efps;
    }

    /**
     * @return the expectedValue
     */
    public EfpValueType getExpectedValue() {
        return expectedValue;
    }

    @Override
    public String serialise() {
        return getLogicalRule();
    }


}