package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * The default gamma function for set types.
 * It evaluates each item of the underlying items.
 * The result is a result of each evaluation unless they hold different values.
 * In a case of different partial results, the function cannot decide.

 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class SetGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        MultiResultResolver result = new MultiResultResolver();

        EfpSet set1 = (EfpSet) o1;
        EfpSet set2 = (EfpSet) o2;

        if (set1.getItems().size() != set2.getItems().size()) {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule. "
                    + "Enums have different count of items.");
        }

        for (int i = 0; i < set1.getItems().size(); i++) {
            EfpValueType value1 = set1.getItems().get(i);
            EfpValueType value2 = set2.getItems().get(i);

            result.compare(value1, value2);
        }


        if (result.getPos() != 0 && result.getNeg() != 0) {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule. "
                    + "The comparing of each item has found "
                    + "a combination of positive and negative results. "
                    + "The comparing is ambiguous.");
        }

        return result.getResult();
    }

}
