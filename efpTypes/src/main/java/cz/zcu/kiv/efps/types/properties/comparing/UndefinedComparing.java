package cz.zcu.kiv.efps.types.properties.comparing;

/**
 * An exception thrown for undefined comparing.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class UndefinedComparing extends RuntimeException {


    /**
     *  A serilalised ID.
     */
    private static final long serialVersionUID = 2086447285777654162L;


    /** Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link RuntimeException#initCause}.
     *
     * @param   text   the detail message. The detail message is saved for
     *          later retrieval by the {@link RuntimeException#getMessage()} method.
     */
    public UndefinedComparing(final String text) {
        super(text);
    }
}
