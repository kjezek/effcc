package cz.zcu.kiv.efps.types.lr;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * The class represents an assignment of one context dependent
 * value to a local registry.
 *
 * Date: 16.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public final class LrSimpleAssignment extends AbstractLrAssignment {

    /** A value assigned to this EFP assignment. */
    private EfpValueType efpValue;

    /**
     * @param id An ID serving for persisting purposes.
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param efpValue A value stored in LR
     * @param lr Link to LR
     */
    public LrSimpleAssignment(
            final Integer id,
            final EFP efp,
            final String valueName,
            final EfpValueType efpValue,
            final LR lr) {

        super(id, efp, valueName, lr);

        this.efpValue = efpValue;
    }
    /**
     *
     * @param efp An EFP a value is assigned to
     * @param valueName Name of the assigned value
     * @param efpValue A value stored in LR
     * @param lr Link to LR
     */
    public LrSimpleAssignment(
            final EFP efp,
            final String valueName,
            final EfpValueType efpValue,
            final LR lr) {

        this(null, efp, valueName, efpValue, lr);
    }

    /**
     * An EFP value assigned in this assignment.
     * @return a value
     */
    public EfpValueType getEfpValue() {
        return efpValue;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignmentType getAssignmentType() {
        return LrAssignmentType.SIMPLE;
    }


    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "LrSimpleAssignment [getEfp()=" + getEfp() + ", getValueName()="
                + getValueName() + ", getLr()=" + getLr() + ", getEfpValue()="
                + getEfpValue() + "]";
    }
    @Override
    public String getLabel() {
        return getEfpValue().getLabel();
    }
}
