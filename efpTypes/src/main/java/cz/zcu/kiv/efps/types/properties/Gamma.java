package cz.zcu.kiv.efps.types.properties;

import java.util.Comparator;

import cz.zcu.kiv.efps.types.datatypes.EfpValueType;

/**
 * An interface for gamma comparing functions.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public interface Gamma extends Comparator<EfpValueType> {

}
