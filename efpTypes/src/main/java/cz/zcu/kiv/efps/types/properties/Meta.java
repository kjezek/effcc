package cz.zcu.kiv.efps.types.properties;

import java.util.Arrays;
import java.util.List;

/**
 * This class represents a block of meta-information
 * attached to an extra-functional property.
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class Meta {

    /** Measuring unit on the property. */
    private String unit;
    /** Names of the assigned values.*/
    private List<String> names;

    /** A private constructor for serialization. */
    @SuppressWarnings("unused")
    private Meta() { }

    /**
     * Creates an instance.
     * @param unit measuring unit on the property
     * @param names names of the assigned values.
     */
    public Meta(final String unit, final String... names) {
        this.unit = unit;
        this.names = Arrays.asList(names);
    }

    /**
     *
     * @return Measuring unit on the property.
     */
    public final String getUnit() {
        return unit;
    }

    /**
     *
     * @return Names of the assigned values
     */
    public final List<String> getNames() {
        return names;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((names == null) ? 0 : names.hashCode());
        result = prime * result + ((unit == null) ? 0 : unit.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Meta other = (Meta) obj;
        if (names == null) {
            if (other.names != null) {
                return false;
            }
        } else if (!names.equals(other.names)) {
            return false;
        }
        if (unit == null) {
            if (other.unit != null) {
                return false;
            }
        } else if (!unit.equals(other.unit)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "Meta [unit=" + unit + ", names=" + names + "]";
    }



}
