package cz.zcu.kiv.efps.types.properties.comparing;

import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * The default gamma function for complex types.
 * It evaluates each item of the underlying items.
 * The result is a result of each evaluation unless they hold different values.
 * In a case of different partial results, the function cannot decide.
 *
 * Date: 17.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class ComplexTypeGamma implements Gamma {

    /** {@inheritDoc} */
    @Override
    public final int compare(final EfpValueType o1, final EfpValueType o2) {

        MultiResultResolver result = new MultiResultResolver();

        EfpComplex type1 = (EfpComplex) o1;
        EfpComplex type2 = (EfpComplex) o2;

        for (String key1 : type1.getItems().keySet()) {

            EfpValueType value1 = type1.getItems().get(key1);
            EfpValueType value2 = type2.getItems().get(key1);

            if (value2 == null) {
                throw new UndefinedComparing("Values "
                        + o1 + " and " + o2 + " cannot be compared "
                        + "by the implicit rule. "
                        + "An item with the name " + key1 + " is not in both of them.");
            }

            result.compare(value1, value2);
        }

        if (result.getPos() != 0 && result.getNeg() != 0) {
            throw new UndefinedComparing("Values "
                    + o1 + " and " + o2 + " cannot be compared "
                    + "by the implicit rule. "
                    + "The comparing of each item has found "
                    + "a combination of positive and negative results. "
                    + "The comparing is ambiguous.");
        }

        return result.getResult();
    }
}
