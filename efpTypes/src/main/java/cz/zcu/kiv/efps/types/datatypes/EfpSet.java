package cz.zcu.kiv.efps.types.datatypes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.SetGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * A set of EFP values.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpSet extends AbstractComparableType {

    /** A set of other EFP values. */
    private List<EfpValueType> efpSet;


    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpSet() {
        super();
    }

    /**
     * Creates an instance.
     * @param items items that will be added to this set.
     */
    public EfpSet(final EfpValueType... items) {
        this.efpSet = Arrays.asList(items);
    }

    /**
     *
     * @return a set of EFP values.
     */
    public List<EfpValueType> getItems() {
        return efpSet;
    }



    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((efpSet == null) ? 0 : efpSet.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (!getClass().isAssignableFrom(obj.getClass())) {
            return false;
        }

        EfpSet other = (EfpSet) obj;
        if (efpSet == null) {
            if (other.efpSet != null) {
                return false;
            }
        } else if (!efpSet.equals(other.efpSet)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new SetGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {

        StringBuffer buffer = new StringBuffer();
        buffer.append("set {");
        for (EfpValueType item : efpSet) {
            buffer.append(item.getLabel());
            buffer.append(", ");
        }
        buffer.append("}");

        return buffer.toString();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        String filter = "&";

        int iVal = 1;
        for (EfpValueType val : efpSet) {
            filter += " (" + val.convertIntoLDAPFilter(prefixNames + "_" + iVal) + ")";
            iVal++;
        }

        return filter;
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();

        int iVal = 1;
        for (EfpValueType val : efpSet) {
            Map<String, String> subMap = val.convertIntoParameters(prefixNames + "_" + iVal);
            map.putAll(subMap);
            iVal++;
        }
        return map;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }
}
