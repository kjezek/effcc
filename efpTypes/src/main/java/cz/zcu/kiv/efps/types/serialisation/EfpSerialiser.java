/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.serialisation.json.ClassAdapter;
import cz.zcu.kiv.efps.types.serialisation.json.EfpAdapter;
import cz.zcu.kiv.efps.types.serialisation.json.EfpSetAdapter;
import cz.zcu.kiv.efps.types.serialisation.json.EfpValueTypeAdapter;
import cz.zcu.kiv.efps.types.serialisation.json.GammaAdapter;

/**
 * This tool class contain a set of methods allowing
 * to serialize and deserialize all EPP value types
 * to a string form allowing to save and load
 * it to/from database, text file, etc.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EfpSerialiser {

    /** This constructor is private. */
    private EfpSerialiser() { }

    /**
     * The method serializes the EFP type.
     * @param serialisableEfp the EFP type
     * @return the serialized value
     */
    public static String serialize(final SerialisableEfp serialisableEfp) {
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(EfpValueType.class, new EfpValueTypeAdapter());
        gsonBuilder.registerTypeAdapter(Gamma.class, new GammaAdapter());
        gsonBuilder.registerTypeAdapter(EFP.class, new EfpAdapter());
        gsonBuilder.registerTypeAdapter(Class.class, new ClassAdapter());

        Gson gson = gsonBuilder.create();

        return gson.toJson(serialisableEfp);
    }

    /**
     * The method deserialize the EFP type.
     * @param <T> a type of the EFP type
     * @param valueType an instance representing the type of the EFP type
     * @param serialisedString string representation of the value
     * @return the EFP type.
     */
    public static <T extends SerialisableEfp> T deserialize(
            final Class<T> valueType, final String serialisedString) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(EfpValueType.class, new EfpValueTypeAdapter());
        gsonBuilder.registerTypeAdapter(EfpSet.class, new EfpSetAdapter());
        gsonBuilder.registerTypeAdapter(Gamma.class, new GammaAdapter());
        gsonBuilder.registerTypeAdapter(EFP.class, new EfpAdapter());
        gsonBuilder.registerTypeAdapter(Class.class, new ClassAdapter());

        Gson gson = gsonBuilder.create();

        return gson.fromJson(serialisedString, valueType);
    }


}
