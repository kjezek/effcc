package cz.zcu.kiv.efps.types.properties;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;
import cz.zcu.kiv.efps.types.serialisation.SerialisableEfp;


/**
 * The top level class in extra-functional property classes.
 * It represents a general extra-functional property.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public abstract class EFP implements SerialisableEfp {

    /** A type of the property. */
    public enum EfpType {
        /** A simple property. */
        SIMPLE,
        /** A derived property. */
        DERIVED
        }

    /** an persisting ID.  */
    private Integer id;
    /** A name of the property. */
    private String name;
    /** A data type of the property. */
    private Class<?> valueType;
    /** A block of meta-information. */
    private Meta meta;
    /** A comparing function. */
    private Gamma gamma;
    /** A GR this property is assigned to. */
    private GR gr;


    /** A private constructor for serialization. */
    protected EFP() { }

    /**
     * Creates an extra-functional property.
     * @param id an ID for persisting purposes
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param gr a GR this property is assigned to
     */
    public EFP(final Integer id, final String name, final Class<?> valueType, final Meta meta,
            final Gamma gamma, final GR gr) {
        super();

        this.id = id;
        this.name = name;
        this.valueType = valueType;
        this.meta = meta;
        this.gamma = gamma;
        this.gr = gr;
    }

    /**
     * Creates an extra-functional property.
     * @param name a name of the property
     * @param valueType a data type of the property
     * @param meta a block of meta-information
     * @param gamma a comparing function
     * @param gr a GR this property is assigned to
     */
    public EFP(final String name, final  Class<?> valueType,
            final Meta meta, final Gamma gamma, final GR gr) {
        this(null, name, valueType, meta, gamma, gr);
    }

    /**
     *
     * @return a type of this property.
     */
    public abstract EfpType getType();

    /**
     *
     * @return a name of the property
     */
    public final String getName() {
        return name;
    }

    /**
     *
     * @return a data type of the property
     */
    public final Class<?> getValueType() {
        return valueType;
    }

    /**
     *
     * @return a block of meta-information
     */
    public final Meta getMeta() {
        return meta;
    }

    /**
     *
     * @return a comparing function
     */
    public final Gamma getGamma() {
        return gamma;
    }

    /**
     * @return the gr
     */
    public GR getGr() {
        return gr;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }


    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gr == null) ? 0 : gr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EFP other = (EFP) obj;
        if (gr == null) {
            if (other.gr != null) {
                return false;
            }
        } else if (!gr.equals(other.gr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "EFP [id=" + id + ", name=" + name + ", valueType=" + valueType
                + ", meta=" + meta + ", gamma=" + gamma + ", gr=" + gr + "]";
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }




}
