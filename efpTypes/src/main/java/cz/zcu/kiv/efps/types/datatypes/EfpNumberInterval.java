package cz.zcu.kiv.efps.types.datatypes;

import net.sourceforge.interval.ia_math.IAMath;
import net.sourceforge.interval.ia_math.RealInterval;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.NumberIntervalGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

import java.util.HashMap;
import java.util.Map;

/**
 * An EFP numbers interval.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpNumberInterval extends AbstractComparableType implements EfpInterval<EfpNumber> {

    /** a low bound of the interval. */
    private EfpNumber low;
    /** a high bound of the interval. */
    private EfpNumber high;


    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpNumberInterval() {
        super();
    }

    /**
     * Constructs a new interval.
     * @param low beginning of the interval
     * @param high ending of the interval
     */
    public EfpNumberInterval(final double low, final double high) {
        this.low = new EfpNumber(low);
        this.high = new EfpNumber(high);
    }

    /**
     * Constructs a new interval.
     * @param value it is beginning and ending of the interval
     */
    public EfpNumberInterval(final double value) {
        this.low =  new EfpNumber(value);
        this.high =  new EfpNumber(value);
    }

    /**
     * Constructs a new interval.
     * @param low beginning of the interval
     * @param high ending of the interval
     */
    public EfpNumberInterval(final EfpNumber low, final EfpNumber high) {
        this.low = low;
        this.high = high;
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval plus(final EfpNumberInterval efp) {

        RealInterval thisInterval = toRealInterval();
        RealInterval inputInterval = efp.toRealInterval();

        return toEfpInterval(IAMath.add(thisInterval, inputInterval));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval minus(final EfpNumberInterval efp) {

        RealInterval thisInterval = toRealInterval();
        RealInterval inputInterval = efp.toRealInterval();

        return toEfpInterval(IAMath.sub(thisInterval, inputInterval));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval multiply(final EfpNumberInterval efp) {

        RealInterval thisInterval = toRealInterval();
        RealInterval inputInterval = efp.toRealInterval();

        return toEfpInterval(IAMath.mul(thisInterval, inputInterval));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval div(final EfpNumberInterval efp) {

        RealInterval thisInterval = toRealInterval();
        RealInterval inputInterval = efp.toRealInterval();

        return toEfpInterval(IAMath.div(thisInterval, inputInterval));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval plus(final EfpNumber efp) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(efp.doubleValue(), efp.doubleValue());
        return plus(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval minus(final EfpNumber efp) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(efp.doubleValue(), efp.doubleValue());
        return minus(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval multiply(final EfpNumber efp) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(efp.doubleValue(), efp.doubleValue());
        return multiply(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval div(final EfpNumber efp) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(efp.doubleValue(), efp.doubleValue());
        return div(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval plus(final Number number) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(number.doubleValue());
        return plus(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval minus(final Number number) {
        EfpNumberInterval inputInterval =
            new EfpNumberInterval(number.doubleValue());
        return minus(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval multiply(final Number number) {
        EfpNumberInterval inputInterval = new EfpNumberInterval(number.doubleValue());
        return multiply(inputInterval);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval div(final Number number) {
        EfpNumberInterval inputInterval = new EfpNumberInterval(number.doubleValue());
        return div(inputInterval);
    }

    /**
     * Rounds this interval with the given precision and returns a new interval.
     * @param precision number of digits
     * @return a new interval
     */
    public final EfpNumberInterval round(final int precision) {
        return new EfpNumberInterval(low.round(precision), high.round(precision));
    }

    /** {@inheritDoc} */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EfpNumberInterval that = (EfpNumberInterval) o;

        if (high != null ? !high.equals(that.high) : that.high != null) {
            return false;
        }
        if (low != null ? !low.equals(that.low) : that.low != null) {
            return false;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public final int hashCode() {
        int result = low != null ? low.hashCode() : 0;
        result = 31 * result + (high != null ? high.hashCode() : 0);
        return result;
    }

    /**
     * Translates EFP interval into third-party RealInterval.
     * @return new interval
     */
    protected final RealInterval toRealInterval() {
        return new RealInterval(low.doubleValue(), high.doubleValue());
    }

    /**
     * Translates RealInterval to EFP interval.
     * @param realInterval RealInterval
     * @return new interval
     */
    protected final EfpNumberInterval toEfpInterval(final RealInterval realInterval) {
        return new EfpNumberInterval(realInterval.lo(), realInterval.hi());
    }

    /**
     * @return A low bound of the interval
     */
    public final EfpNumber getLow() {
        return low;
    }

    /**
     * @return A high bound of the interval
     */
    public final EfpNumber getHigh() {
        return high;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new NumberIntervalGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {
        return "[" + low + "; " + high + "]";
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        return "& (" + prefixNames + "_min>=" + low.getLabel() + ") "
                + "(" + prefixNames + "_max<=" + high.getLabel() + ")";
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(prefixNames + "_min:Double", low.getLabel());
        map.put(prefixNames + "_max:Double", high.getLabel());

        return map;
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }

}
