package cz.zcu.kiv.efps.types.datatypes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.NumberGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * The EFP type for numbers.
 *
 * Date: 4.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpNumber extends Number implements EfpValueType, EfpSimpleType<Number> {

    /** A precision of displayed number. */
    private static final int DISPLAY_PRECISION = 3;

    /** A serialized ID. */
    private static final long serialVersionUID = -412860590012358926L;

    /** A number value wrapped in this EFP type. */
    private double efpNumber;

    /** A user gamma. */
    private Gamma userGamma;


    /**
     *
     */
    @SuppressWarnings("unused")
    private EfpNumber() {    }

    /**
     * Creates an instance.
     * @param value a value wrapped in this EFP type.
     */
    public EfpNumber(final double value) {
        this.efpNumber = value;
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber plus(final EfpNumber number) {
        return new EfpNumber(efpNumber + number.efpNumber);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber minus(final EfpNumber number) {
        return new EfpNumber(efpNumber - number.efpNumber);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber div(final EfpNumber number) {
        return new EfpNumber(efpNumber / number.efpNumber);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber multiply(final EfpNumber number) {
        return new EfpNumber(efpNumber * number.efpNumber);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval plus(final EfpNumberInterval efp) {
        return new EfpNumberInterval(efpNumber, efpNumber).plus(efp);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval minus(final EfpNumberInterval efp) {
        return new EfpNumberInterval(efpNumber, efpNumber).minus(efp);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval multiply(final EfpNumberInterval efp) {
        return new EfpNumberInterval(efpNumber, efpNumber).multiply(efp);
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param efp an operand
     * @return the result of the operation
     */
    public final EfpNumberInterval div(final EfpNumberInterval efp) {
        return new EfpNumberInterval(efpNumber, efpNumber).div(efp);
     }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber plus(final Number number) {
        return plus(new EfpNumber(number.doubleValue()));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber minus(final Number number) {
        return minus(new EfpNumber(number.doubleValue()));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber multiply(final Number number) {
        return multiply(new EfpNumber(number.doubleValue()));
    }

    /**
     * Overrides an operator for usage in Groovy.
     * @param number an operand
     * @return the result of the operation
     */
    public final EfpNumber div(final Number number) {
        return div(new EfpNumber(number.doubleValue()));
    }

    /**
     * Rounds this number with the given precision and returns a new number.
     * @param precision number of digits
     * @return a new number
     */
    public final EfpNumber round(final int precision) {
        BigDecimal tmpNumber = new BigDecimal(efpNumber);
        tmpNumber = tmpNumber.setScale(precision, RoundingMode.HALF_UP);

        return new EfpNumber(tmpNumber.doubleValue());
    }


    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EfpNumber)) {
            return false;
        }

        EfpNumber efpNumberObject = (EfpNumber) o;

        if (Double.compare(efpNumberObject.efpNumber, efpNumber) != 0) {
            return false;
        }

        return true;
    }

    @Override
    public final int hashCode() {
        long temp = efpNumber != +0.0d ? Double.doubleToLongBits(efpNumber) : 0L;
        return (int) (temp ^ (temp >>> 32));
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public final Gamma getDefaultGamma() {
        return new NumberGamma();
    }

    /** {@inheritDoc} */
    @Override
    public final String getLabel() {
        return new EfpNumber(efpNumber).round(DISPLAY_PRECISION).getValue().toString();
    }

    @Override
    public String convertIntoLDAPFilter(final String prefixNames) {
        return prefixNames + "=" + efpNumber;
    }

    @Override
    public Map<String, String> convertIntoParameters(final String prefixNames) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(prefixNames + ":Double", "" + efpNumber);

        return map;
    }

    /** {@inheritDoc} */
    @Override
    public final int compareTo(final EfpValueType o) {
        EfpValueType wrapper = new DefaultComparableType(this);
        wrapper.setUserGamma(userGamma);
        return wrapper.compareTo(o);
    }

    /** {@inheritDoc} */
    @Override
    public final void setUserGamma(final Gamma userGamma) {
        this.userGamma = userGamma;
    }

    /** {@inheritDoc} */
    @Override
    public final int intValue() {
        return (int) efpNumber;
    }

    /** {@inheritDoc} */
    @Override
    public final long longValue() {
        return (long) efpNumber;
    }

    /** {@inheritDoc} */
    @Override
    public final float floatValue() {
        return (float) efpNumber;
    }

    /** {@inheritDoc} */
    @Override
    public final double doubleValue() {
        return efpNumber;
    }

    /** {@inheritDoc} */
    @Override
    public final Number getValue() {
        return doubleValue();
    }

    /** {@inheritDoc} */
    @Override
    public String serialise() {
        return EfpSerialiser.serialize(this);
    }


}
