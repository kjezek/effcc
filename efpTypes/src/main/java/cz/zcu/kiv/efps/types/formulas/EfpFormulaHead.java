package cz.zcu.kiv.efps.types.formulas;

import java.util.LinkedList;
import java.util.List;

import cz.zcu.kiv.efps.types.properties.EFP;


/**
 * This class represents a head of an extra-functional a  formula.
 * It covers an output of the formula and a list of inputs.
 *
 * Date: 20.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class EfpFormulaHead {

    private EFP output;
    private List<EFP> input;

    public EfpFormulaHead(EFP output, List<EFP> input) {
        this.output = output;
        this.input = new LinkedList<EFP>(input);
    }

    public EFP getOutput() {
        return output;
    }

    public List<EFP> getInput() {
        return new LinkedList<EFP>(input);
    }
}
