package cz.zcu.kiv.efps.types.evaluator

import org.junit.Test
import cz.zcu.kiv.efps.types.datatypes.EfpInterval
import org.junit.Before
import cz.zcu.kiv.efps.types.datatypes.EfpNumber
import cz.zcu.kiv.efps.types.evaluator.dsl.NumberOperators
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval

/**
 *
 *
 * Date: 4.6.2010

 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */

class EvalTest {

  private EfpInterval interval1;
  private EfpInterval interval2;

  private EfpNumber number1;

  @Before
  public void setUp() {
    interval1 = new EfpNumberInterval(10, 20);
    interval2 = new EfpNumberInterval(30, 40);
    number1 = new EfpNumber(50);
  }


  @Test
  public void testBasicFormula() {

    EfpNumberInterval result = interval1 + interval2;

    assert new EfpNumberInterval(40, 60) == result.round(0);
  }

  @Test
  public void testNumbersAndIntervals() {

    EfpNumberInterval result = interval1 + number1 - interval2;

    assert new EfpNumberInterval(20, 40) == result.round(0);
  }

  @Test
  public void testEfpNumberIntervalWithJavaTypes() {

    use (NumberOperators.class) {
      // java number BEFORE efp value
      EfpNumberInterval result = 2 * interval1;
      assert new EfpNumberInterval(20, 40) == result.round(0);

      result = 2 +  interval1;
      assert new EfpNumberInterval(12, 22) == result.round(0);

      result = 100 - interval1;
      assert new EfpNumberInterval(80, 90) == result.round(0);

      // java number AFTER efp value
      result = interval1 * 2;
      assert new EfpNumberInterval(20, 40) == result.round(0);

      result = interval1 + 2;
      assert new EfpNumberInterval(12, 22) == result.round(0);

      result = interval1 - 10;
      assert new EfpNumberInterval(0, 10) == result.round(0);
    }
  }

  public void testEfpNumberWithJavaTypes() {

    use (NumberOperators.class) {
      //java number BEFORE efp value
      EfpNumber result = 2 * number1;
      assert new EfpNumber(100) == result.round(0);

      result = 2 + number1;
      assert new EfpNumber(52) == result.round(0);

      result = 200 - number1;
      assert new EfpNumber(150) == result.round(0);

      result = 100 / number1;
      assert new EfpNumber(2) == result.round(0);

      // java number AFTER efp value
      result =  number1 * 2;
      assert new EfpNumber(100) == result.round(0);

      result = number1 + 2;
      assert new EfpNumber(52) == result.round(0);

      result = number1 - 20;
      assert new EfpNumber(30) == result.round(0);

      result = number1 / 5;
      assert new EfpNumber(10) == result.round(0);
    }
  }
}
