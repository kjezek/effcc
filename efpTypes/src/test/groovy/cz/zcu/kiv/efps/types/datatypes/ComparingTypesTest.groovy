/**
 *
 */
package cz.zcu.kiv.efps.types.datatypes;

import org.junit.Test;
import cz.zcu.kiv.efps.types.datatypes.*;

/**
 * Test comparing of java types.
 * It practically means the test of overriding the <code>compareTo</code> method
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
class ComparingTypesTest {

	@Test
	public void testNumberTypes() {

		// epf types
		assert new EfpNumber(10) > new EfpNumber(5);
		assert new EfpNumber(10) < new EfpNumber(15);
		assert new EfpNumber(10) >= new EfpNumber(10);
		assert new EfpNumber(10) <= new EfpNumber(10);
		assert new EfpNumber(10) == new EfpNumber(10);
		assert new EfpNumber(10) <=> new EfpNumber(10) == 0;

		// efp and java types
		assert new EfpNumber(10) > 5;
		assert new EfpNumber(10) < 15;
		assert new EfpNumber(10) >= 10;
		assert new EfpNumber(10) <= 10;
		assert new EfpNumber(10) == 10;
		assert new EfpNumber(10) <=> 10 == 0;

		// the opposite direction
		assert 10 > new EfpNumber(5);
		assert 10 < new EfpNumber(15);
		assert 10 >= new EfpNumber(10);
		assert 10 <= new EfpNumber(10);
		assert 10 == new EfpNumber(10);
		assert 10 <=> new EfpNumber(10) == 0;
	}

	@Test
	public void testNumberIntervalTypes() {

      assert new EfpNumberInterval(10, 11) > new EfpNumberInterval(1, 2);
      assert new EfpNumberInterval(10, 11) < new EfpNumberInterval(100, 200);
      assert new EfpNumberInterval(10, 11) <=> new EfpNumberInterval(10, 11) == 0;
	}

}
