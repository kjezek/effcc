package cz.zcu.kiv.efps.types.datatypes

import org.junit.Test

/**
 * Test comparing of types to equality.
 * It practically means the test of overriding the <code>equal/code> method
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
class EqualsTypesTest {

  @Test
  void testNumbers() {

    // efp types
    assert new EfpNumber(10) == new EfpNumber(10)

    // efp types with java types
    assert new EfpNumber(10) == 10
    assert 10 == new EfpNumber(10)
  }

  @Test
  void testStrings() {
    assert new EfpString("efp") == new EfpString("efp");
    assert new EfpString("efp").getValue() == "efp";
    assert "efp" == new EfpString("efp").getValue();
  }

  @Test
  void testBoolean() {
    assert new EfpBoolean(true) == new EfpBoolean(true)
    assert true == new EfpBoolean(true).getValue()
    assert new EfpBoolean(true).getValue() == true
  }

  @Test
  void testComplexType() {
    def ct1 = new EfpComplex(
            [
                    "number": new EfpNumber(1),
                    "string" : new EfpString("efp"),
                    "boolean" : new EfpBoolean(true),
                    "numberInterval" : new EfpNumberInterval(10, 20),
                    "set" : new EfpSet(new EfpBoolean(true), new EfpString("abc")),
                    "enum" : new EfpEnum("a", "b", "cdef")

            ]
    );

    def ct2 = new EfpComplex(
            [
                    "enum" : new EfpEnum("a", "b", "cdef"),
                    "number": new EfpNumber(1),
                    "string" : new EfpString("efp"),
                    "boolean" : new EfpBoolean(true),
                    "numberInterval" : new EfpNumberInterval(10, 20),
                    "set" : new EfpSet(new EfpBoolean(true), new EfpString("abc"))

            ]
    );

    assert ct1 == ct2
  }

}
