package cz.zcu.kiv.efps.types.datatypes;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * EfpIntervalTest Tester.
 *
 * @author Kamil Ježek
 * @since <pre>06/04/2010</pre>
 * @version 1.0
 */
public class EfpNumberTest {

    private EfpNumberInterval firstInt;

    private EfpNumber firstNum;
    private EfpNumber secondNum;

    private Number javaNumber;

    @Before
    public void setUp() throws Exception {
        firstInt = new EfpNumberInterval(10, 20);

        firstNum = new EfpNumber(10);
        secondNum = new EfpNumber(20);

        javaNumber = 20;
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     *
     * Method: testPlus()
     *
     */
     @Test
    public void testTestPlus() {
        EfpNumber resultNumber = firstNum.plus(secondNum);
        Assert.assertEquals(
                new EfpNumber(30),
                resultNumber);

        EfpNumber resultNumberWitJNumber = firstNum.plus(javaNumber);
        Assert.assertEquals(
                new EfpNumber(30),
                resultNumberWitJNumber);

        EfpNumberInterval resultInterval = firstNum.plus(firstInt).round(0);
        Assert.assertEquals(
                new EfpNumberInterval(20, 30),
                resultInterval);
    }

    /**
     *
     * Method: testMinus()
     *
     */
     @Test
    public void testTestMinus()  {
        EfpNumber resultNumber = firstNum.minus(secondNum);
        Assert.assertEquals(
                new EfpNumber(-10),
                resultNumber);

        EfpNumber resultNumberWitJNumber = firstNum.minus(javaNumber);
        Assert.assertEquals(
                new EfpNumber(-10),
                resultNumberWitJNumber);


        EfpNumberInterval resultInterval = firstNum.minus(firstInt).round(0);
        Assert.assertEquals(
                new EfpNumberInterval(-10, 0),
                resultInterval);
    }

    /**
     *
     * Method: testMultiply()
     *
     */
     @Test
    public void testTestMultiply()  {
        EfpNumber resultNumber = firstNum.multiply(secondNum);
        Assert.assertEquals(
                new EfpNumber(200),
                resultNumber);

        EfpNumber resultNumberWitJNumber = firstNum.multiply(javaNumber);
        Assert.assertEquals(
                new EfpNumber(200),
                resultNumberWitJNumber);


        EfpNumberInterval resultInterval = firstNum.multiply(firstInt).round(0);
        Assert.assertEquals(
                new EfpNumberInterval(100, 200),
                resultInterval);
    }

    /**
     *
     * Method: testDiv()
     *
     */
     @Test
    public void testTestDiv()  {
        EfpNumber resultNumber = firstNum.div(secondNum);
        Assert.assertEquals(
                new EfpNumber(0.5),
                resultNumber);

        EfpNumber resultNumberWitJNumber = firstNum.div(javaNumber);
        Assert.assertEquals(
                new EfpNumber(0.5),
                resultNumberWitJNumber);

        EfpNumberInterval resultInterval = firstNum.div(firstInt).round(1);
        Assert.assertEquals(
                new EfpNumberInterval(0.5, 1),
                resultInterval);
    }


}


