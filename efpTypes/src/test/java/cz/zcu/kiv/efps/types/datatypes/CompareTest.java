package cz.zcu.kiv.efps.types.datatypes;

import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.comparing.UndefinedComparing;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * The purpose of this class is to test a variety of comparing methods that compares efp data types.
 * It tests implicit gamma functions.
 *
 * Date: 17.6.2010
 *
 * @author Kamil Ježek<a href="kjezek@kiv.zcu.cz">kjezek@kiv.zcu.cz</a>
 */
public class CompareTest {


    @Test
    public final void testCmpNumbers() {
        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        Assert.assertTrue(num1.compareTo(num2) < 0);
        Assert.assertTrue(num2.compareTo(num1) > 0);
        Assert.assertTrue(num1.compareTo(num1) == 0);
    }

    @Test
    public final void testNumberInterval() {
        EfpNumberInterval int1 = new EfpNumberInterval(10, 20);
        EfpNumberInterval int2 = new EfpNumberInterval(10, 20);

        Assert.assertEquals(0, int1.compareTo(int2));
        Assert.assertEquals(0, int2.compareTo(int1));
        Assert.assertEquals(0, int1.compareTo(int1));
    }

    @Test
    public final void testBoolean() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        Assert.assertTrue(bool1.compareTo(bool1) == 0);
        Assert.assertTrue(bool1.compareTo(bool2) == 0);
    }

    @Test()
    public final void testBooleanFail() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(false);

        Assert.assertTrue(bool1.compareTo(bool2) > 0);
    }

    @Test
    public final void testString() {
        EfpString string1 = new EfpString("hello");
        EfpString string2 = new EfpString("hello");

        Assert.assertTrue(string1.compareTo(string2) == 0);
        Assert.assertTrue(string1.compareTo(string1) == 0);
    }

    @Test(expected = UndefinedComparing.class)
    public final void testStringFail() {
        EfpString string1 = new EfpString("hello");
        EfpString string2 = new EfpString("good morning");

        string1.compareTo(string2);
    }

    @Test
    public final void testEnum() {
        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpEnum enum2 = new EfpEnum("a", "b", "c");

        Assert.assertTrue(enum1.compareTo(enum2) == 0);
        Assert.assertTrue(enum1.compareTo(enum1) == 0);
    }

    @Test(expected = UndefinedComparing.class)
    public final void testEnumFail() {
        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpEnum enum2 = new EfpEnum("b", "a", "c");

        enum1.compareTo(enum2);
    }

    @Test
    public final void testSet() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpEnum enum2 = new EfpEnum("a", "b", "c");

        EfpSet set1 = new EfpSet(bool1, num1, enum1);
        EfpSet set2 = new EfpSet(bool2, num2, enum2);

        Assert.assertTrue(set1.compareTo(set2) < 0);
        Assert.assertTrue(set2.compareTo(set1) > 0);
        Assert.assertTrue(set1.compareTo(set1) == 0);
    }

    @Test(expected = UndefinedComparing.class)
    public final void testSetFail() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        EfpSet set1 = new EfpSet(bool1, num1, num2);
        EfpSet set2 = new EfpSet(bool2, num2, num1);

        set1.compareTo(set2);
    }

    @Test
    public final void testComplexType() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpEnum enum2 = new EfpEnum("b", "b", "c");

        Map<String, EfpValueType> map1 = new HashMap<String, EfpValueType>();
        map1.put("val1", bool1);
        map1.put("val2", num1);
        map1.put("val3", enum1);

        Map<String, EfpValueType> map2 = new HashMap<String, EfpValueType>();
        map2.put("val1", bool2);
        map2.put("val2", num2);
        map2.put("val3", enum2);

        EfpComplex ct1 = new EfpComplex(map1);
        EfpComplex ct2 = new EfpComplex(map2);

        Assert.assertTrue(ct1.compareTo(ct2) < 0);
        Assert.assertTrue(ct2.compareTo(ct1) > 0);
        Assert.assertTrue(ct1.compareTo(ct1) == 0);
    }

    @Test(expected = UndefinedComparing.class)
    public final void testComplexTypeFailOnValues() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        Map<String, EfpValueType> map1 = new HashMap<String, EfpValueType>();
        map1.put("val1", bool1);
        map1.put("val2", num1);
        map1.put("val3", num2);

        Map<String, EfpValueType> map2 = new HashMap<String, EfpValueType>();
        map2.put("val1", bool2);
        map2.put("val2", num2);
        map2.put("val3", num1);

        EfpComplex ct1 = new EfpComplex(map1);
        EfpComplex ct2 = new EfpComplex(map2);

        ct1.compareTo(ct2);
    }

    @Test(expected = UndefinedComparing.class)
    public final void testComplexTypeFailOnKeys() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(true);

        EfpNumber num1 = new EfpNumber(10);
        EfpNumber num2 = new EfpNumber(20);

        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpEnum enum2 = new EfpEnum("b", "b", "c");

        Map<String, EfpValueType> map1 = new HashMap<String, EfpValueType>();
        map1.put("val1 - different Key", bool1);
        map1.put("val2", num1);
        map1.put("val3", enum1);

        Map<String, EfpValueType> map2 = new HashMap<String, EfpValueType>();
        map2.put("val1", bool2);
        map2.put("val2", num2);
        map2.put("val3", enum2);

        EfpComplex ct1 = new EfpComplex(map1);
        EfpComplex ct2 = new EfpComplex(map2);

        Assert.assertTrue(ct1.compareTo(ct2) < 0);
        Assert.assertTrue(ct2.compareTo(ct1) > 0);
        Assert.assertTrue(ct1.compareTo(ct1) == 0);
    }

    @Test
    public final void testUserDefinedGamma() {
        Gamma fruitGamma = new FruitGamma();
        EfpString str1 = new EfpString("melon");
        str1.setUserGamma(fruitGamma);
        EfpString str2 = new EfpString("apple");
        str2.setUserGamma(fruitGamma);

        Assert.assertTrue(str1.compareTo(str2) > 0);
        Assert.assertTrue(str2.compareTo(str1) < 0);
        Assert.assertTrue(str1.compareTo(str1) == 0);
        Assert.assertTrue(str2.compareTo(str2) == 0);
    }


    private final class FruitGamma implements Gamma {

        private Map<String, Integer> indexes = new HashMap<String, Integer>();

        private FruitGamma() {
            indexes.put("apple", 1);
            indexes.put("orange", 2);
            indexes.put("banana", 3);
            indexes.put("melon", 4);
        }

        @Override
        public int compare(final EfpValueType o1, final EfpValueType o2) {

            EfpString s1 = (EfpString) o1;
            EfpString s2 = (EfpString) o2;

            return indexes.get(s1.getValue()) - indexes.get(s2.getValue());
        }
    }

}
