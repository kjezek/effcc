package cz.zcu.kiv.efps.types.datatypes;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * EfpNumberInterval Tester.
 *
 * @author Kamil Ježek
 * @since <pre>06/04/2010</pre>
 * @version 1.0
 */
public class EfpIntervalTest  {

    private EfpNumberInterval one;
    private EfpNumberInterval two;
    private EfpNumber number;
    private Number javaNumber;

    @Before
    public void setUp() throws Exception {
        one = new EfpNumberInterval(10, 20);
        two = new EfpNumberInterval(30, 40);
        number = new EfpNumber(50);
        javaNumber = 50;
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     *
     * Method: plus(EfpNumberInterval efp)
     *
     */
     @Test
    public void testPlus() {
        EfpNumberInterval result = one.plus(two).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(40, 60),
                result);

        EfpNumberInterval resultNumber = one.plus(number).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(60, 70),
                resultNumber);

        EfpNumberInterval resultNumberWithJType = one.plus(javaNumber).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(60, 70),
                resultNumberWithJType);
    }

    /**
     *
     * Method: minus(EfpNumberInterval efp)
     *
     */
     @Test
    public void testMinus() {
        EfpNumberInterval result = one.minus(two).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(-30, -10),
                result);

        EfpNumberInterval resultNumber = one.minus(number).round(0);
        Assert.assertEquals(
                new EfpNumberInterval(-40, -30),
                resultNumber);

        EfpNumberInterval resultNumberWithJType = one.minus(javaNumber).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(-40, -30),
                resultNumberWithJType);

    }

    /**
     *
     * Method: multiply(EfpNumberInterval efp)
     *
     */
     @Test
    public void testMultiply()  {
        EfpNumberInterval result = one.multiply(two).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(300, 800),
                result);

        EfpNumberInterval resultNumber = one.multiply(number).round(0);
        Assert.assertEquals(
                new EfpNumberInterval(500, 1000),
                resultNumber);

        EfpNumberInterval resultNumberWithJType = one.multiply(javaNumber).round(0);

        Assert.assertEquals(
                new EfpNumberInterval(500, 1000),
                resultNumberWithJType);
    }

    /**
     *
     * Method: div(EfpNumberInterval efp)
     *
     */
     @Test
    public void testDiv() {
        EfpNumberInterval result = one.div(two).round(2);

        Assert.assertEquals(
                new EfpNumberInterval(0.25, 0.67),
                result);

        EfpNumberInterval resultNumber = one.div(number).round(2);
        Assert.assertEquals(
                new EfpNumberInterval(0.2, 0.4),
                resultNumber);

        EfpNumberInterval resultNumberWithJType = one.div(javaNumber).round(2);

        Assert.assertEquals(
                new EfpNumberInterval(0.2, 0.4),
                resultNumberWithJType);
    }

    /**
     *
     * Method: equals(Object o)
     *
     */
     @Test
    public void testEquals() {

        Assert.assertEquals(one, one);
    }



}


