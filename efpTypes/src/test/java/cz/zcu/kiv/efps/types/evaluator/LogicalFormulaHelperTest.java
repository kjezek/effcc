package cz.zcu.kiv.efps.types.evaluator;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import static org.junit.Assert.*;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LogicalFormulaHelper;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;

/**
*
* This is a junit test for LogicalFormulaHelper
* This test was generated at
* @author
*/

public class LogicalFormulaHelperTest  {

    /** LR. */
    private LR lr = new LR(1, "High Performance LR", null, null, "HP");

    /**
     * @see cz.zcu.kiv.efps.types.evaluator.LogicalFormulaHelper#findUsedAssignments
     */
    @Test
    public void testFindUsedAssignments() {

        EFP time = new SimpleEFP("time", null, new Meta("s", "slow", "average", "high"), null, null);
        EFP memory = new SimpleEFP("memory", null, new Meta("kb", "slow", "average", "high"), null, null);

        EFP machine = new DerivedEFP("machine", null, new Meta("", "slow", "average", "high"), null, null,
                time, memory);

        EFP invalid = new SimpleEFP("invalid", null, null, null, null);

        String formula = "time <= time_slow && memory == 200";
        LogicalFormulaHelper helper = new LogicalFormulaHelper(formula);

        List<EfpLrAssignment> efpLrs = helper.findUsedAssignments(
                Arrays.asList(time, machine, memory, invalid));

        for (EfpLrAssignment efpLr : efpLrs) {
            Assert.assertFalse(efpLr.getEfp().equals(invalid));
            Assert.assertTrue(efpLr.getEfp().equals(time)
                    || efpLr.getEfp().equals(memory)
                    || efpLr.getEfp().equals(machine));
        }

    }



}
