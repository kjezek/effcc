package cz.zcu.kiv.efps.types.datatypes;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * The purpose of this class is to test a variety of conversion data types into special strings.
 *
 * Date: 29.4.2014
 *
 * @author Jan Svab<a href="jansvab10@gmail.com">jansvab10@gmail.com</a>
 */
public class ConversionToStringTest {

    @Test
    public final void testNumberInterval() {
        EfpNumberInterval int1 = new EfpNumberInterval(10.2, 20.8);

        Assert.assertEquals("& (value_min>=10.2) (value_max<=20.8)", int1.convertIntoLDAPFilter("value"));

        Map<String, String> parameters = int1.convertIntoParameters("value");
        Assert.assertEquals("10.2", parameters.get("value_min:Double"));
        Assert.assertEquals("20.8", parameters.get("value_max:Double"));
    }

    @Test
    public final void testBoolean() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpBoolean bool2 = new EfpBoolean(false);

        Assert.assertEquals("value=true", bool1.convertIntoLDAPFilter("value"));
        Assert.assertEquals("value=false", bool2.convertIntoLDAPFilter("value"));

        Map<String, String> parameters1 = bool1.convertIntoParameters("value");
        Map<String, String> parameters2 = bool2.convertIntoParameters("value");

        Assert.assertEquals("true", parameters1.get("value"));
        Assert.assertEquals("false", parameters2.get("value"));
    }

    @Test
    public final void testString() {
        EfpString string1 = new EfpString("hello");

        Assert.assertEquals("value=hello", string1.convertIntoLDAPFilter("value"));

        Map<String, String> parameters = string1.convertIntoParameters("value");
        Assert.assertEquals("\"hello\"", parameters.get("value:String"));
    }

    @Test
    public final void testEnum() {
        EfpEnum enum1 = new EfpEnum("a", "b", "c");

        Assert.assertEquals("& (value=a) (value=b) (value=c)", enum1.convertIntoLDAPFilter("value"));

        Map<String, String> parameters = enum1.convertIntoParameters("value");
        Assert.assertEquals("\"a,b,c\"", parameters.get("value:List<String>"));
    }

    @Test
    public final void testSet() {
        EfpBoolean bool1 = new EfpBoolean(true);
        EfpNumber num1 = new EfpNumber(10);
        EfpEnum enum1 = new EfpEnum("a", "b", "c");

        EfpSet set1 = new EfpSet(bool1, num1, enum1);

        Assert.assertEquals("& (value_1=true) (value_2=10.0) (& (value_3=a) (value_3=b) (value_3=c))",
                set1.convertIntoLDAPFilter("value"));

        Map<String, String> parameters = set1.convertIntoParameters("value");
        Assert.assertEquals("true", parameters.get("value_1"));
        Assert.assertEquals("10.0", parameters.get("value_2:Double"));
        Assert.assertEquals("\"a,b,c\"", parameters.get("value_3:List<String>"));
    }

    @Test
    public final void testComplexType() {
        EfpBoolean bool1 = new EfpBoolean(false);
        EfpNumber num1 = new EfpNumber(10);
        EfpEnum enum1 = new EfpEnum("a", "b", "c");
        EfpNumberInterval int1 = new EfpNumberInterval(10.2, 20.8);

        Map<String, EfpValueType> map1 = new HashMap<String, EfpValueType>();
        map1.put("valA", bool1);
        map1.put("valB", num1);
        map1.put("valC", enum1);
        map1.put("valD", int1);

        EfpComplex ct1 = new EfpComplex(map1);

        String filter = ct1.convertIntoLDAPFilter("value");
        Assert.assertTrue(filter.contains("(value_valA=false)"));
        Assert.assertTrue(filter.contains("(value_valB=10.0)"));
        Assert.assertTrue(filter.contains("(& (value_valC=a) (value_valC=b) (value_valC=c))"));
        Assert.assertTrue(filter.contains("(& (value_valD_min>=10.2) (value_valD_max<=20.8))"));

        Map<String, String> parameters = ct1.convertIntoParameters("value");
        Assert.assertEquals("false", parameters.get("value_valA"));
        Assert.assertEquals("10.0", parameters.get("value_valB:Double"));
        Assert.assertEquals("\"a,b,c\"", parameters.get("value_valC:List<String>"));
        Assert.assertEquals("10.2", parameters.get("value_valD_min:Double"));
        Assert.assertEquals("20.8", parameters.get("value_valD_max:Double"));
    }
}
