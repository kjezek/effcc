/**
 *
 */
package cz.zcu.kiv.efps.types.evaluator;

import junit.framework.Assert;

import org.junit.Test;

import cz.zcu.kiv.efps.types.evaluator.AbstractGroovyLrdAssignmentEvaluator;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.tools.GroovyScripts;

/**
 * Tests of evaluations of groovy expressions. It tests also evaluation of
 * overridden operators from the EFP Types module.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public final class EvalScriptTest {

    /**
     * The test of comparing of data types. It depends on overriding of
     * <code>compareTo</code> functions on data types.
     */
    @Test
    public void testComapring() {

        // efp numbers
        Assert.assertTrue(boolExpr("new EfpNumber(10) < new EfpNumber(20)"));
        Assert.assertTrue(boolExpr("new EfpNumber(10) <= new EfpNumber(10)"));
        Assert.assertTrue(boolExpr("new EfpNumber(30) > new EfpNumber(20)"));
        Assert.assertTrue(boolExpr("new EfpNumber(20) >= new EfpNumber(20)"));
        Assert.assertTrue(boolExpr("new EfpNumber(20).compareTo(new EfpNumber(20)) == 0"));

        Assert.assertTrue(boolExpr("105 < 106"));

        // efp number intervals
        Assert.assertTrue(boolExpr("new EfpNumberInterval(10, 20) < new EfpNumberInterval(15, 25)"));
        Assert.assertTrue(boolExpr("new EfpNumberInterval(10, 20) < new EfpNumberInterval(30, 40)"));
        Assert.assertTrue(boolExpr("new EfpNumberInterval(10, 20) < new EfpNumberInterval(20, 40)"));
        Assert.assertTrue(boolExpr("new EfpNumberInterval(10, 20) <= new EfpNumberInterval(10, 20)"));

        // efp number with java numbers
        Assert.assertTrue(boolExpr("new EfpNumber(10) < 20"));
        Assert.assertTrue(boolExpr("new EfpNumber(10) <= 10"));
        Assert.assertTrue(boolExpr("new EfpNumber(30) > 20"));
        Assert.assertTrue(boolExpr("new EfpNumber(20) >= 20"));
        Assert.assertTrue(boolExpr("new EfpNumber(30).compareTo(30) == 0"));

        // java numbers with efp numbers
        Assert.assertTrue(boolExpr("10 < new EfpNumber(20)"));
        Assert.assertTrue(boolExpr("10 <= new EfpNumber(10)"));
        Assert.assertTrue(boolExpr("30 > new EfpNumber(20)"));
        Assert.assertTrue(boolExpr("20 >= new EfpNumber(20)"));
    }

    /**
     * The test of comparing equality. It depends on overriding of
     * <code>equals</code> functions on data types.
     */
    @Test
    public void testEquals() {

        // numbers
        Assert.assertTrue(boolExpr("new EfpNumber(10) == new EfpNumber(10)"));
        // intervals
        Assert.assertTrue(boolExpr("new EfpNumberInterval(10, 20) == new EfpNumberInterval(10, 20)"));

        // other efp types
        Assert.assertTrue(boolExpr("new EfpString('hello') == new EfpString('hello')"));
        Assert.assertTrue(boolExpr("new EfpBoolean(true) == new EfpBoolean(true)"));
        Assert.assertTrue(boolExpr("new EfpString('hello') == new EfpString('hello')"));

        // efp types with Java types
        Assert.assertTrue(boolExpr("new EfpString('hello').getValue() == 'hello'"));
        Assert.assertTrue(boolExpr("new EfpBoolean(true).getValue() == true"));
        Assert.assertTrue(boolExpr("new EfpString('hello').getValue() == 'hello'"));

        // Java types with efp types
        Assert.assertTrue(boolExpr("'hello' == new EfpString('hello').getValue()"));
        Assert.assertTrue(boolExpr("true == new EfpBoolean(true).getValue()"));
        Assert.assertTrue(boolExpr("'hello' == new EfpString('hello').getValue()"));
    }

    /**
     * Evaluates a logical formula.
     *
     * @param expression
     *            the formula
     * @return boolean result
     */
    private Boolean boolExpr(final String expression) {

        return (Boolean) new AbstractGroovyLrdAssignmentEvaluator(expression, null) {

            @Override
            public String getGroovyScript() {
                return GroovyScripts.GROOVY_SCRIPT;
            }
        }.evaluate(new EfpValue[0], new LrAssignment[0]);
    }

}
