/**
 *
 */
package cz.zcu.kiv.efps.types.properties.comparing;

import org.junit.Assert;
import org.junit.Test;

import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.properties.Gamma;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class UserGammaTest {

    /**
     * Test with simple types.
     */
    @Test
    public void testCompareSimpleTypes() {

        EfpValueType val1 = new EfpNumber(10);
        EfpValueType val2 = new EfpNumber(20);

        Gamma gamma = new UserGamma("first - second");

        Assert.assertTrue(gamma.compare(val1, val2) < 0);
        Assert.assertTrue(gamma.compare(val2, val1) > 0);
        Assert.assertTrue(gamma.compare(val1, val1) == 0);

        gamma = new UserGamma(
                "if (first > second) return 1 "
                + " else if (first < second) return -1 "
                + "else return 0");

        Assert.assertTrue(gamma.compare(val1, val2) < 0);
        Assert.assertTrue(gamma.compare(val2, val1) > 0);
        Assert.assertTrue(gamma.compare(val1, val1) == 0);
    }

    /**
     * Test with intervals.
     */
    @Test
    public void testCompareIntervals() {

        EfpValueType val1 = new EfpNumberInterval(10, 40);
        EfpValueType val2 = new EfpNumberInterval(50, 60);

        // compares only lower boundaries
        Gamma gamma = new UserGamma("(first - second).getLow()");

        Assert.assertTrue(gamma.compare(val1, val2) < 0);
        Assert.assertTrue(gamma.compare(val2, val1) > 0);
    }

    /**
     * Test of practical usage of user gamma.
     */
    @Test
    public void testCompletScenario() {
        EfpValueType val1 = new EfpNumber(10);
        EfpValueType val2 = new EfpNumber(20);

        Gamma gamma = new UserGamma("first - second");
        val1.setUserGamma(gamma);
        val2.setUserGamma(gamma);

        Assert.assertTrue(val1.compareTo(val2) < 0);
        Assert.assertTrue(val2.compareTo(val1) > 0);
        Assert.assertTrue(val1.compareTo(val1) == 0);
    }

}
