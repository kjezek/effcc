/**
 *
 */
package cz.zcu.kiv.efps.types.serialisation;

import java.util.HashMap;
import java.util.Map;

import javax.naming.LinkRef;

import org.junit.Assert;
import org.junit.Test;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpComplex;
import cz.zcu.kiv.efps.types.datatypes.EfpEnum;
import cz.zcu.kiv.efps.types.datatypes.EfpNumber;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpSet;
import cz.zcu.kiv.efps.types.datatypes.EfpSimpleSet;
import cz.zcu.kiv.efps.types.datatypes.EfpString;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * Tests for serializer.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class EfpValueSerialiserTest {


    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseString() {

        EfpString value = new EfpString("efp");
        String str = value.serialise();
        EfpString newValue
            = EfpSerialiser.deserialize(EfpString.class, str);

        Assert.assertEquals(value, newValue);
    }


    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseStringWitGamma() {

        EfpString value = new EfpString("efp");
        value.setUserGamma(new UserGamma("return -1"));
        String str = value.serialise();
        EfpString newValue
            = EfpSerialiser.deserialize(EfpString.class, str);

        Assert.assertEquals(value, newValue);
        Assert.assertTrue(newValue.compareTo(newValue) == -1);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseSimpleSet() {

        EfpSimpleSet value = new EfpSimpleSet(new EfpString("hello"),
                new EfpNumber(10),
                new EfpBoolean(true),
                new EfpSimpleSet(
                        new EfpString("x"),
                        new EfpString("y"),
                        new EfpString("z")
                        )
        );
        String str = value.serialise();
        EfpSimpleSet newValue
            = EfpSerialiser.deserialize(EfpSimpleSet.class, str);

        Assert.assertEquals(value, newValue);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseEnum() {

        EfpEnum value = new EfpEnum("x", "y", "z");
        String str = value.serialise();
        EfpEnum newValue
            = EfpSerialiser.deserialize(EfpEnum.class, str);

        Assert.assertEquals(value, newValue);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseNumberInterval() {

        EfpNumberInterval value = new EfpNumberInterval(10, 20);
        String str = value.serialise();
        EfpNumberInterval newValue
            = EfpSerialiser.deserialize(EfpNumberInterval.class, str);

        Assert.assertEquals(value, newValue);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseComplex() {

        EfpSimpleSet set = new EfpSimpleSet(new EfpString("hello"),
                new EfpNumber(10),
                new EfpBoolean(true),
                new EfpSimpleSet(
                        new EfpString("x"),
                        new EfpString("y"),
                        new EfpString("z")
                        )
        );
        Map<String, EfpValueType> data = new HashMap<String, EfpValueType>();
        data.put("numberInterval", new EfpNumberInterval(10, 20));
        data.put("set", set);
        data.put("enum", new EfpEnum("x", "y", "z"));

        EfpComplex value = new EfpComplex(data);

        String str = value.serialise();
        EfpComplex newValue
            = EfpSerialiser.deserialize(EfpComplex.class, str);

        Assert.assertEquals(value, newValue);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseEFP() {

        EFP efp = new SimpleEFP(
                1, "simple EFP", EfpNumber.class,
                new Meta("kb", "high", "low"),
                new UserGamma("formula"),
                new GR(2, "gr", "gr"));

        String str = efp.serialise();

        EFP newEFP = EfpSerialiser.deserialize(EFP.class, str);

        Assert.assertEquals(efp, newEFP);
        Assert.assertEquals(efp.getMeta(), newEFP.getMeta());
        Assert.assertEquals(efp.getGr(), newEFP.getGr());
        Assert.assertEquals(efp.getGamma(), newEFP.getGamma());
        Assert.assertEquals(efp.getValueType(), newEFP.getValueType());
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testSerialiseDerivedEFP() {

        EFP efp1 = new SimpleEFP(1, "simple EFP 1", EfpNumber.class, null, null, null);
        EFP efp2 = new SimpleEFP(1, "simple EFP 2", EfpNumber.class, null, null, null);

        EFP efp = new DerivedEFP(
                1, "derived EFP", EfpNumber.class,
                null, null, null, efp1, efp2);

        String str = efp.serialise();

        DerivedEFP newEFP = EfpSerialiser.deserialize(DerivedEFP.class, str);

        Assert.assertEquals(efp.getValueType(), newEFP.getValueType());
        Assert.assertEquals(efp, newEFP);
        Assert.assertNull(newEFP.getMeta());
        Assert.assertNull(newEFP.getGr());
        Assert.assertNull(newEFP.getGamma());
        Assert.assertTrue(newEFP.getEfps().size() == 2);
    }

    /**
     * Test to serialize and deserialize EFP type.
     */
    @Test
    public void testLr() {

        LR lr = new LR("lr name", new GR("gr name", "grname"), null, "LN");

        String str = lr.serialise();

        LR newLr = EfpSerialiser.deserialize(LR.class, str);

        Assert.assertEquals(lr, newLr);
        Assert.assertEquals(lr.getGr(), newLr.getGr());
    }
}
