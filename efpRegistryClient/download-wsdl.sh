#!/bin/sh

ADDRESS=http://localhost:8080/efpRegistryServer/ws
DEST=src/main/wsdl

wget $ADDRESS/efp?wsdl -O $DEST/efp.wsdl
wget $ADDRESS/lr?wsdl -O $DEST/lr.wsdl
wget $ADDRESS/gr?wsdl -O $DEST/gr.wsdl
