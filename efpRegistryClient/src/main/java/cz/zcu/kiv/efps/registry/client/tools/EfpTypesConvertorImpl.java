/**
 *
 */
package cz.zcu.kiv.efps.registry.client.tools;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.client.EfpClient;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrDTO;
import cz.zcu.kiv.efps.types.datatypes.EfpValueType;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import cz.zcu.kiv.efps.types.serialisation.EfpSerialiser;

/**
 * A class converting data types.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Component(value = "efpTypesConvertor")
public class EfpTypesConvertorImpl implements EfpTypesConvertor {

    /** Dependency. */
    @Resource(name = "efpClient")
    private EfpClient efpClient;

    /** {@inheritDoc} */
    @Override
    public EFP convert(final EfpDTO dto) {
        EFP result;

        String[] names;

        if (dto.getValueNames() == null) {
            names = new String[] {};
        } else {
            names = dto.getValueNames().toArray(
                    new String[dto.getValueNames().size()]);
        }

        // obtain mete
        Meta meta = new Meta(dto.getUnit(), names);

        // obtain gamma
        Gamma gamma;
        if (StringUtils.trimToNull(dto.getGamma()) == null) {
            gamma = null;
        } else {
            gamma = new UserGamma(dto.getGamma());
        }

        GR gr = convert(dto.getGr());
        Integer id = dto.getId();

        Class<?> valueType = null;
        try {
            valueType = Class.forName(dto.getValueType());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        // create simple or derived instance
        if (dto.getDerivedFromEfps() == null
                || dto.getDerivedFromEfps().isEmpty()) {

            result = new SimpleEFP(id, dto.getName(), valueType, meta, gamma, gr);
        } else {

            List<EFP> derivingEFPs = new LinkedList<EFP>();

            // convert all deriving properties
            for (final EfpDTO derivingEFP : dto.getDerivedFromEfps()) {
                derivingEFPs.add(convert(derivingEFP));
            }

            result = new DerivedEFP(id, dto.getName(), valueType, meta, gamma, gr,
                    derivingEFPs.toArray(new EFP[derivingEFPs.size()]));
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public EfpDTO convert(final EFP efp) {
        EfpDTO result = new EfpDTO();

        Meta meta = efp.getMeta();
        if (meta != null) {
            result.setUnit(meta.getUnit());
            result.getValueNames().addAll(meta.getNames());
        }

        if (efp.getType() == EFP.EfpType.DERIVED) {
            List<EfpDTO> deriving = convertEfps(((DerivedEFP) efp).getEfps());
            result.getDerivedFromEfps().addAll(deriving);
        }

        result.setId(efp.getId());
        result.setName(efp.getName());
        result.setGr(convert(efp.getGr()));
        result.setValueType(efp.getValueType().getName());

        if (efp.getGamma() != null) {
            result.setGamma(((UserGamma) efp.getGamma()).getFunction());
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public List<EfpDTO> convertEfps(final List<EFP> efps) {
        List<EfpDTO> result = new LinkedList<EfpDTO>();

        for (final EFP efp : efps) {
            result.add(convert(efp));
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public List<EFP> convertEfpDTOs(final List<EfpDTO> dtos) {
        List<EFP> result = new LinkedList<EFP>();

        for (final EfpDTO dto : dtos) {
            result.add(convert(dto));
        }

        return result;
    }



    /** {@inheritDoc} */
    @Override
    public LrAssignmentDTO convert(final LrAssignment lrAssignment) {
        LrAssignmentDTO result = new LrAssignmentDTO();

       result.setId(lrAssignment.getId());
       result.setValueName(lrAssignment.getValueName());
       result.setEfp(convert(lrAssignment.getEfp()));
       result.setLr(convert(lrAssignment.getLr()));

       if (lrAssignment.getAssignmentType() == LrAssignment.LrAssignmentType.DERIVED) {

           LrDerivedValueEvaluator eval = ((LrDerivedAssignment) lrAssignment).getEvaluator();
           // TODO [kjezek, A] I deal only with a logical formula here
           LrConstraintEvaluator logFormEval = (LrConstraintEvaluator) eval;
           result.setLogicalFormula(logFormEval.getLogicalRule());
           result.setValue(logFormEval.getExpectedValue().serialise());
       } else {
           result.setValue(((LrSimpleAssignment) lrAssignment).getEfpValue().serialise());
       }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignment convert(final LrAssignmentDTO dto) {

        Integer id = dto.getId();
        EfpValueType value = EfpSerialiser.deserialize(EfpValueType.class, dto.getValue());
        String valueName = dto.getValueName();
        EFP efp = convert(dto.getEfp());
        LR lr = convert(dto.getLr());

        LrAssignment result;
        if (StringUtils.trimToNull(dto.getLogicalFormula()) == null
                && EFP.EfpType.SIMPLE.equals(efp.getType())) {

            result = new LrSimpleAssignment(id, efp, valueName, value, lr);
        } else {
            // TODO [kjezek, A] I deal only with a logical formula here
            DerivedEFP derEfp = (DerivedEFP) efp;

            LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                    dto.getLogicalFormula(),
                    value,
                    derEfp.getEfps());

            result = new LrDerivedAssignment(
                    id, efp, valueName, eval, lr);

        }

         return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignment> convertAssignDTOs(final List<LrAssignmentDTO> dtos) {
        List<LrAssignment> result = new LinkedList<LrAssignment>();

        for (LrAssignmentDTO dto : dtos) {
            result.add(convert(dto));
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public GR convert(final GrDTO gr) {
        return new GR(gr.getId(), gr.getName(), gr.getIdStr());
    }

    /** {@inheritDoc} */
    @Override
    public List<GR> convertGRDTOs(final List<GrDTO> grDTOs) {
        List<GR> result = new LinkedList<GR>();

        for (GrDTO grDTO : grDTOs) {
            result.add(convert(grDTO));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public GrDTO convert(final GR gr) {
        GrDTO grDTO = new GrDTO();
        grDTO.setId(gr.getId());
        grDTO.setName(gr.getName());
        grDTO.setIdStr(gr.getIdStr());
        return grDTO;
    }

    /** {@inheritDoc} */
    @Override
    public LR convert(final LrDTO dto) {

        LR result = null;
        if (dto != null) {
            result = new LR(
                    dto.getId(),
                    dto.getName(),
                    convert(dto.getGr()),
                    convert(dto.getParent()),
                    dto.getIdStr()
                    );
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public LrDTO convert(final LR lrType) {

        LrDTO result = new LrDTO();

        result.setGr(convert(lrType.getGr()));
        result.setId(lrType.getId());
        result.setName(lrType.getName());
        result.setIdStr(lrType.getIdStr());

        if (lrType.getParentLR() != null) {
            result.setParent(convert(lrType.getParentLR()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LR> convertLrDTOs(final List<LrDTO> dtos) {
        List<LR> result = new LinkedList<LR>();

        for (LrDTO dto : dtos) {
            result.add(convert(dto));
        }

        return result;
    }


}
