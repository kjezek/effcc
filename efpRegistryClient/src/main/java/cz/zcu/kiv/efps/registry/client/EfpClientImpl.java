/**
 *
 */
package cz.zcu.kiv.efps.registry.client;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import cz.zcu.kiv.efps.registry.client.tools.EfpTypesConvertor;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpWs;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrWs;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrWs;
import cz.zcu.kiv.efps.types.evaluator.EfpLrAssignment;
import cz.zcu.kiv.efps.types.evaluator.LogicalFormulaHelper;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * A client class accessing EFP data from a remote repository.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@Component(value = "efpClient")
public final class EfpClientImpl implements EfpClient {

    /** A self instance for the singleton. */
    private static EfpClient self;

    /** A reference to remote EFP methods. */
    @Resource(name = "efpWs")
    private EfpWs efpWs;

    /** A reference to remote GR methods. */
    @Resource(name = "grWs")
    private GrWs grWs;

    /** A reference to remote LR methods. */
    @Resource(name = "lrWs")
    private LrWs lrWs;

    /** A types converter. */
    @Resource(name = "efpTypesConvertor")
    private EfpTypesConvertor efpTypesConvertor;

    /**
     * Returns a singleton instance.
     *
     * @return the instance.
     */
    public static EfpClient getClient() {

        if (self == null) {

            ApplicationContext appCtx = new ClassPathXmlApplicationContext(
                    "/applicationContext.xml", "/placeholders.xml");

            self = (EfpClient) appCtx.getBean("efpClient");
        }

        return self;
    }

    /** {@inheritDoc} */
    @Override
    public EFP findEfpByName(final String name, final Integer grId) {
        EfpDTO efpDto = efpWs.findEfpByName(name, grId);

        EFP efp = null;
        if (efpDto != null) {
            efp = efpTypesConvertor.convert(efpDto);
        }

        return efp;
    }

    @Override
    public List<EFP> findEfpForGr(final Integer grId) {
        List<EfpDTO> efpDtos = efpWs.findEfpForGr(grId);
        return efpTypesConvertor.convertEfpDTOs(efpDtos);
    }

    /** {@inheritDoc} */
    @Override
    public EFP create(final EFP efp) {
        EfpDTO efpDTO = efpTypesConvertor.convert(efp);
        EfpDTO createdEfp = efpWs.createEfp(efpDTO);
        return efpTypesConvertor.convert(createdEfp);
    }

    /** {@inheritDoc} */
    @Override
    public void update(final EFP efp) {
        EfpDTO efpDTO = efpTypesConvertor.convert(efp);
        efpWs.updateEfp(efpDTO);
    }

    /** {@inheritDoc} */
    @Override
    public GR getGrById(final Integer id) {
        GrDTO grDTO = grWs.getGrById(id);
        return efpTypesConvertor.convert(grDTO);
    }

    /** {@inheritDoc} */
    @Override
    public GR create(final GR gr) {
        return efpTypesConvertor.convert(grWs.createGr(efpTypesConvertor.convert(gr)));
    }

    /** {@inheritDoc} */
    @Override
    public List<GR> findGrAll() {
        return efpTypesConvertor.convertGRDTOs(grWs.findAllGrs());
    }

    /** {@inheritDoc} */
    @Override
    public void update(final GR modifiedGr) {
        GrDTO modifiedGrDTO = efpTypesConvertor.convert(modifiedGr);
        grWs.updateGr(modifiedGrDTO);
    }

    /** {@inheritDoc} */
    @Override
    public LR getLrById(final Integer id) {
        return efpTypesConvertor.convert(lrWs.getLrById(id));
    }

    /** {@inheritDoc} */
    @Override
    public LR create(final LR newLr) {
        LrDTO lrDTO = efpTypesConvertor.convert(newLr);

        return efpTypesConvertor.convert(lrWs.createLr(lrDTO));
    }

    /** {@inheritDoc} */
    @Override
    public List<LR> findLrForGr(final GR gr) {
        return efpTypesConvertor.convertLrDTOs(lrWs.findLrForGr(gr.getId()));
    }

    /** {@inheritDoc} */
    @Override
    public void update(final LR modifiedLR) {
        LrDTO lrDTO = efpTypesConvertor.convert(modifiedLR);
        lrWs.updateLr(lrDTO);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignment getLrAssignmentById(final Integer id) {
        LrAssignmentDTO lrAssignmentDTO = lrWs.getLrAssignmentById(id);
        return efpTypesConvertor.convert(lrAssignmentDTO);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignment> findLrAssignments(final Integer efpId, final Integer lrId) {
        List<LrAssignmentDTO> lrAssigns = lrWs.findLrAssignments(efpId, lrId);
        return efpTypesConvertor.convertAssignDTOs(lrAssigns);

    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignment> findLrAssignments(final Integer lrId) {
        List<LrAssignmentDTO> lrAssigns = lrWs.findAllLrAssignments(lrId);
        return efpTypesConvertor.convertAssignDTOs(lrAssigns);

    }

    /** {@inheritDoc} */
    @Override
    public LrAssignment create(final LrAssignment lrAssignment) {
        LrAssignmentDTO lrAssDTO = efpTypesConvertor.convert(lrAssignment);

        return efpTypesConvertor.convert(lrWs.createLrAssignment(lrAssDTO));
    }

    /** {@inheritDoc} */
    @Override
    public void update(final LrAssignment lrAssignment) {
        LrAssignmentDTO lrAssDTO = efpTypesConvertor.convert(lrAssignment);
        lrWs.updateLrAssignment(lrAssDTO);
    }

    /** {@inheritDoc} */
    @Override
    public void delete(final LrAssignment lrAssignment) {
        lrWs.deleteLrAssignment(lrAssignment.getId());
    }

    /** {@inheritDoc} */
    @Override
    public void deleteEfp(final EFP efp) {
        efpWs.deleteEfp(efp.getId());
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getAllValueNames(final Integer grId) {
        return grWs.getAllValueNames(grId);
    }

    /** {@inheritDoc} */
    @Override
    public void updateValueName(final Integer grId, final String oldValue,
            final String newValue) {
        grWs.updateValueName(grId, oldValue, newValue);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteValueName(final Integer grId, final String valueName) {
        grWs.deleteValueName(grId, valueName);
    }

    /** {@inheritDoc} */
    @Override
    public void createValueName(final Integer grId, final String valueName) {
        grWs.createValueName(grId, valueName);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteLr(final Integer lrId) {
        lrWs.deleteLR(lrId);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteGr(final Integer grId) {
        grWs.deleteGR(grId);
    }

    /** {@inheritDoc} */
    @Override
    public List<EFP> findByAssignedValueName(final Integer grId, final String valueName) {
        List<EfpDTO> efpDtos = efpWs.findByAssignedValueName(grId, valueName);
        return efpTypesConvertor.convertEfpDTOs(efpDtos);
    }

    /** {@inheritDoc} */
    @Override
    public LrAssignment findLrAssignment(final Integer efpId, final Integer lrId,
            final String name) {

        LrAssignmentDTO lrAssign = lrWs.findLrAssignment(efpId, lrId, name);

        LrAssignment result = null;

        if (lrAssign != null) {
            result = efpTypesConvertor.convert(lrAssign);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignment> generateLrAssignment(final EFP efp, final LR lr,
            final String fce, final double from, final double to, final double step) {
        List<LrAssignmentDTO> lrAssignmentDTO = lrWs.generateLrAssignment(
                efpTypesConvertor.convert(efp), efpTypesConvertor.convert(lr), fce, from,
                to, step);

        return efpTypesConvertor.convertAssignDTOs(lrAssignmentDTO);
    }

    /** {@inheritDoc} */
    @Override
    public List<LrAssignment> findLrAssignmentsInFormula(
            final String formula,
            final List<EFP> efps, final LR lr) {

        LogicalFormulaHelper helper = new LogicalFormulaHelper(formula);
        List<EfpLrAssignment> efpLrs = helper.findUsedAssignments(efps);

        List<LrAssignment> lrAssignments = new ArrayList<LrAssignment>();

        for (EfpLrAssignment efpLr : efpLrs) {

            for (String valueName : efpLr.getLrValueNames()) {
                LrAssignment relatedAssignment = findLrAssignment(efpLr.getEfp().getId(), lr.getId(), valueName);

                lrAssignments.add(relatedAssignment);
            }

        }

        return lrAssignments;
    }
}
