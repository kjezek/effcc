package cz.zcu.kiv.efps.registry.client.tools;

import java.util.List;

import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.EfpDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.GrDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrAssignmentDTO;
import cz.zcu.kiv.efps.registry.server.ws.v1_0_0.LrDTO;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * An interface converting data types.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface EfpTypesConvertor {

    /**
     * Converts a DTO entity to the EFP type.
     *
     * @param dto
     *            the DTO entity
     * @return the EFP type.
     */
    EFP convert(final EfpDTO dto);

    /**
     * Converts an EFP type to the DTO type.
     *
     * @param efp
     *            the EFP type
     * @return the DTO type.
     */
    EfpDTO convert(final EFP efp);

    /**
     * Converts EFP types to the DTO types.
     *
     * @param efps
     *            the EFP types
     * @return the DTO types.
     */
    List<EfpDTO> convertEfps(final List<EFP> efps);

    /**
     * Converts DTO types to the EFP types.
     * @param dtos the DTO
     * @return the DTO types.
     */
    List<EFP> convertEfpDTOs(final List<EfpDTO> dtos);

    /**
     * Converts DTO to EFP type.
     * @param dto the DTO
     * @return  the EFP type
     */
    LrAssignmentDTO convert(final LrAssignment dto);

    /**
     * Converts DTO to EFP type.
     * @param dto the DTO
     * @return  the EFP type
     */
    LrAssignment convert(final LrAssignmentDTO dto);

    /**
     * Converts DTO to EFP type.
     * @param dto the DTO
     * @return  the EFP type
     */
    List<LrAssignment> convertAssignDTOs(final List<LrAssignmentDTO> dto);

    /**
     * Converts an DTO to the EFP type.
     *
     * @param gr
     *            the DTO type
     * @return the EFP type.
     */
    GR convert(final GrDTO gr);

    /**
     * Converts an DTO to the EFP type.
     *
     * @param grDTOs
     *            the DTO
     * @return the EFP type.
     */
    List<GR> convertGRDTOs(final List<GrDTO> grDTOs);

    /**
     * Converts an EFP type to the DTO type.
     *
     * @param gr
     *            the EFP type
     * @return the DTO type.
     */
    GrDTO convert(final GR gr);

    /**
     * Converts an DTO to the EFP type.
     *
     * @param dto
     *            the DTO type
     * @return the EFP type.
     */
    LR convert(final LrDTO dto);

    /**
     * Converts an EFP type  to the DTO.
     *
     * @param lrType
     *            the EFP type
     * @return the DTO type.
     */
    LrDTO convert(final LR lrType);

    /**
     * Converts an DTO to the EFP type.
     *
     * @param dtos
     *            the database entity
     * @return the EFP type.
     */
    List<LR> convertLrDTOs(final List<LrDTO> dtos);

}
