package cz.zcu.kiv.efps.registry.client;

import java.util.List;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

/**
 * A main access point to obtain EFP data from
 * the EFP repository.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface EfpClient {

    /**
     * Finds an EFP by name and GR.
     * @param name a name of an EFP
     * @param grId id of GR
     * @return the EFP or null
     *
     */
    EFP findEfpByName(String name, Integer grId);

    /**
     * Finds a list of EFPs for GR.
     * @param grId gr which EFPs belong to
     * @return the list of EFPs
     */
    List<EFP> findEfpForGr(Integer grId);

    /**
     * @param efp an EFP to create
     * @return a created EFP (with assigned ID)
     */
    EFP create(EFP efp);

    /**
     * Updates an EFP.
     * @param efp the EFP to be updated
     */
    void update(EFP efp);

    /**
     * Finds GR.
     * @param id ID of GR
     * @return found GR
     */
    GR getGrById(final Integer id);

    /**
     * Creates new GR.
     * @param gr not existing GR
     * @return EFP type
     */
    GR create(final GR gr);

    /**
     * Finds all GRs.
     * @return all GRs
     */
    List<GR> findGrAll();

    /**
     * Updated GR.
     * @param modifiedGr GR with new value(s)
     */
    void update(final GR modifiedGr);

    /**
     * It gets LR by its ID.
     * @param id the ID
     * @return the found LR
     */
    LR getLrById(Integer id);

    /**
     * It creates new LR attaching it to existing GR.
     * @param newLr new LR
     * @return EFP type
     */
    LR create(LR newLr);

    /**
     * Finds a list of LRs for GR.
     * @param gr existing GR
     * @return the found list
     */
    List<LR> findLrForGr(GR gr);

    /**
     * Modifies existing LR.
     * @param modifiedLR an object containing new values
     */
    void update(LR modifiedLR);

    /**
     * Method finds all assignments for an EFP and LR.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignment> findLrAssignments(Integer efpId, Integer lrId);

    /**
     * Method finds assignment for EFP, LR and a value name.
     * @param lrId local registry ID
     * @param efpId extra-functional property ID
     * @param name the value name
     * @return an assignment
     */
    LrAssignment findLrAssignment(Integer efpId, Integer lrId, String name);

    /**
     * Method finds all assignments for concrete LR.
     *
     * @param lrId local registry ID
     * @return a list of assignments for the pair of LR and EFP
     */
    List<LrAssignment> findLrAssignments(Integer lrId);

    /**
     * Gets LR Assignment by its ID.
     * @param id the ID
     * @return the LR assignment for the ID
     */
    LrAssignment getLrAssignmentById(Integer id);

    /**
     * It goes through formula and loads all assignments
     * used in the formula.
     * @param formula the formula
     * @param efps EFPs used in the formula
     * @param lr LR
     * @return a list of assignments used in the formula
     */
    List<LrAssignment> findLrAssignmentsInFormula(
            String formula,
            List<EFP> efps, LR lr);

    /**
     * Creates a new assignment.
     * @param lrAssignment the new assignment
     * @return the created assignment
     */
    LrAssignment create(LrAssignment lrAssignment);


    /**
     * Updates an assignment.
     * @param lrAssignment the assignment to update
     */
    void update(LrAssignment lrAssignment);

    /**
     * Permanently deletes an assignment.
     * @param lrAssignment the assignment to delete
     */
    void delete(LrAssignment lrAssignment);

    /**
     * Permanently deletes an EFP.
     * @param efp the EFP to delete
     */
    void deleteEfp(EFP efp);

    /**
     * This method reads all value names available for this GR.
     * @param grId GR
     * @return the list of names
     */
    List<String> getAllValueNames(Integer grId);

    /**
     * This method allows to change a value name.
     * @param oldValue an old name
     * @param newValue a new name
     * @param grId GR
     */
    void updateValueName(Integer grId, String oldValue, String newValue);

    /**
     * This method deletes one value name.
     * @param valueName the value name
     * @param grId GR
     */
    void deleteValueName(Integer grId, String valueName);

    /**
     * This method creates one value name.
     * @param valueName the value name
     * @param grId GR
     */
    void createValueName(Integer grId, String valueName);

    /**
     * This method deletes LR with a given ID.
     * @param lrId the ID of LR to be deleted
     */
    void deleteLr(Integer lrId);

    /**
     * This method deletes GR with a given ID.
     * @param grId the ID of GR to be deleted
     */
    void deleteGr(Integer grId);

    /**
     * This method finds a list of EFP which a value name is used in.
     * @param grId ID of GR
     * @param valueName the value to find EFPs for.
     * @return a list of records.
     */
    List<EFP> findByAssignedValueName(Integer grId, String valueName);

    /**
     * This method compute values of lrAssignment under fce.
     * @param efp extra-functional property
     * @param lr local register
     * @param fce function
     * @param from lower limit of interval
     * @param to upper limit of interval
     * @param step step between the limits
     * @return a list of computed lrAssignments
     */
    List<LrAssignment> generateLrAssignment(EFP efp, LR lr, String fce, double from,
            double to, double step);

}
