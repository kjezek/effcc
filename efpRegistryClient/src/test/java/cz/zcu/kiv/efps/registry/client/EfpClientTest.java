/**
 *
 */
package cz.zcu.kiv.efps.registry.client;

import cz.zcu.kiv.efps.types.datatypes.EfpBoolean;
import cz.zcu.kiv.efps.types.datatypes.EfpNumberInterval;
import cz.zcu.kiv.efps.types.datatypes.EfpString;
import cz.zcu.kiv.efps.types.evaluator.LrConstraintEvaluator;
import cz.zcu.kiv.efps.types.evaluator.LrDerivedValueEvaluator;
import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.lr.LrDerivedAssignment;
import cz.zcu.kiv.efps.types.lr.LrSimpleAssignment;
import cz.zcu.kiv.efps.types.properties.DerivedEFP;
import cz.zcu.kiv.efps.types.properties.EFP;
import cz.zcu.kiv.efps.types.properties.Gamma;
import cz.zcu.kiv.efps.types.properties.Meta;
import cz.zcu.kiv.efps.types.properties.SimpleEFP;
import cz.zcu.kiv.efps.types.properties.comparing.UserGamma;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {
                "/applicationContext.xml",
                "/placeholders.xml"}
        )
@Ignore  // ignore for not as it needs a test server
public class EfpClientTest {

    @Resource(name = "efpClient")
    private EfpClient efpClient;

    /** A test GR. */
    private GR grEdu;
    /** A test GR. */
    private GR grAuto;

    /** A test LR. */
    private LR parentLr;

    /** A test LR. */
    private LR lr;

    /** A test LR. */
    private LR lr2;

    /** A test EFP. */
    private EFP simpleEfp1;

    /** A test EFP. */
    private EFP simpleEfp2;

    /** A test EFP. */
    private EFP derivedEfp;

    /** Simple LR Asignment. */
    private LrAssignment lrSimpleAssig;

    /** Simple LR Derived. */
    private LrAssignment lrDerivedAssig;

    /** Simple LR Derived. */
    private LrAssignment lrDerivedAssigWithEmptyFormula;

    /** Unique name for one test run. */
    private String uniqueName;

    @Before
    public void setUp() {

        uniqueName = new SimpleDateFormat("dd.MM.yyyy-HH.mm.ss.SS").format(new Date());

        grEdu = new GR("Education Systems" + uniqueName, "ES" + uniqueName);
        grAuto = new GR("Automotive" + uniqueName, "A" + uniqueName);

        grEdu = efpClient.create(grEdu);
        grAuto = efpClient.create(grAuto);

        parentLr = new LR("Parent LR", grEdu, null, "PLR" + uniqueName);
        parentLr = efpClient.create(parentLr);

        lr = new LR("LR", grEdu, parentLr, "LR" + uniqueName);
        lr2 = new LR("LR2", grEdu, null, "LR2" + uniqueName);

        lr = efpClient.create(lr);
        lr2 = efpClient.create(lr2);

        efpClient.createValueName(grEdu.getId(), "low");
        efpClient.createValueName(grEdu.getId(), "high");
        efpClient.createValueName(grEdu.getId(), "medium");
        efpClient.createValueName(grEdu.getId(), "delete");

        efpClient.createValueName(grAuto.getId(), "ok");
        efpClient.createValueName(grAuto.getId(), "quick");

        Meta meta = new Meta("kb", "low" , "high", "medium");

        simpleEfp1 = new SimpleEFP(
                "EFP1", EfpString.class, meta,
                new UserGamma("second - first"), grEdu);

        simpleEfp1 =  efpClient.create(simpleEfp1);

        simpleEfp2 = new SimpleEFP(
                "EFP2", EfpString.class, meta, null, grEdu);

        simpleEfp2 =  efpClient.create(simpleEfp2);

        derivedEfp = new DerivedEFP(
                "EFPderived", EfpString.class, meta, null, grEdu, simpleEfp1, simpleEfp2);

        derivedEfp =  efpClient.create(derivedEfp);

        lrSimpleAssig = new LrSimpleAssignment(
                simpleEfp1, "low", new EfpNumberInterval(10, 20), lr);

        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "a + b + c", new EfpString("good"), Arrays.asList(simpleEfp1, simpleEfp2));
        lrDerivedAssig = new LrDerivedAssignment(
                derivedEfp, "high", eval, lr);

        LrDerivedValueEvaluator emptyEval = new LrConstraintEvaluator(
                null, new EfpString("good"), Arrays.asList(simpleEfp1, simpleEfp2));
        lrDerivedAssigWithEmptyFormula = new LrDerivedAssignment(
                derivedEfp, "low", emptyEval, lr);

        lrSimpleAssig = efpClient.create(lrSimpleAssig);
        lrDerivedAssig = efpClient.create(lrDerivedAssig);
        lrDerivedAssigWithEmptyFormula = efpClient.create(lrDerivedAssigWithEmptyFormula);
    };

//    @After
//    public void tearDown() {
//        // clean up
//        efpClient.delete(lrSimpleAssig);
//        efpClient.delete(lrDerivedAssig);
//    }

    @Test
    public void testGetClient() {
        EfpClient efpLocalClient = EfpClientImpl.getClient();

        Assert.assertNotNull(efpLocalClient);
    }

    @Test
    public void testFindEfpByName() {

        EFP efp = efpClient.findEfpByName(simpleEfp1.getName(), grEdu.getId());
        Assert.assertEquals(simpleEfp1, efp);
    }

    @Test
    public void testFindNonExistingEfp() {

        EFP efp = efpClient.findEfpByName("this EFP is not in the DB", -1);
        Assert.assertNull(efp);
    }


    @Test
    public void testFindEfpForNonExistingGr() {

        List<EFP> efps = efpClient.findEfpForGr(-1);
        Assert.assertTrue(efps.size() == 0);
    }

    @Test
    public void testFindEfpForGr() {

        List<EFP> efps = efpClient.findEfpForGr(grEdu.getId());

        Assert.assertTrue(efps.contains(simpleEfp1));
        Assert.assertTrue(efps.contains(simpleEfp2));
        Assert.assertTrue(efps.contains(derivedEfp));
    }

    @Test
    public void testCreateEfp() {

        Meta meta = new Meta("slow", "ok", "quick");
        EFP efp = new SimpleEFP("XYZ", EfpString.class, meta, null, grAuto);
        EFP createdEfp = efpClient.create(efp);

        Assert.assertNotNull(createdEfp.getId());
        Assert.assertEquals(efp, createdEfp);
        Assert.assertEquals(efp.getGr(), createdEfp.getGr());
        Assert.assertEquals(efp.getMeta(), createdEfp.getMeta());
        Assert.assertNull(createdEfp.getGamma());
    }

    @Test
    public void testUpdateEfpSimpleVal() {
        EFP efp = new SimpleEFP(
                simpleEfp1.getId(),
                "Averynewname",
                simpleEfp1.getValueType(),
                simpleEfp1.getMeta(),
                simpleEfp1.getGamma(),
                simpleEfp1.getGr()
                );

        efpClient.update(efp);

        Assert.assertNotNull(
                efpClient.findEfpByName("Averynewname" , simpleEfp1.getGr().getId()));
    }

    @Test
    public void testUpdateEfpGr() {
        EFP efp = new SimpleEFP(
                simpleEfp1.getId(),
                simpleEfp1.getName(),
                simpleEfp1.getValueType(),
                null,
                simpleEfp1.getGamma(),
                grAuto
                );

        efpClient.update(efp);

        Assert.assertNotNull(efpClient.findEfpByName(simpleEfp1.getName(), grAuto.getId()));
    }

    @Test
    public void testUpdateEfpMeta() {

        Meta meta = new Meta("meters");
        EFP efp = new SimpleEFP(
                simpleEfp1.getId(),
                simpleEfp1.getName(),
                simpleEfp1.getValueType(),
                meta,
                simpleEfp1.getGamma(),
                simpleEfp1.getGr()
                );

        efpClient.update(efp);

        EFP updatedEFP = efpClient.findEfpByName(simpleEfp1.getName(), simpleEfp1.getGr().getId());

        Assert.assertEquals(meta, updatedEFP.getMeta());
    }

    @Test
    public void testUpdateEfpGamma() {

        Gamma gamma = new UserGamma("a + b + c");
        EFP efp = new SimpleEFP(
                simpleEfp1.getId(),
                simpleEfp1.getName(),
                simpleEfp1.getValueType(),
                simpleEfp1.getMeta(),
                gamma,
                simpleEfp1.getGr()
                );

        efpClient.update(efp);

        EFP updatedEFP = efpClient.findEfpByName(simpleEfp1.getName(), simpleEfp1.getGr().getId());

        Assert.assertEquals(gamma, updatedEFP.getGamma());
    }

    @Test
    public void testGetById() {
        GR loadedGR = efpClient.getGrById(grEdu.getId());
        Assert.assertEquals(grEdu, loadedGR);
    }

    @Test
    public void testCreateGr() {
        GR grHosp = new GR("Hospitals" + uniqueName, "H" + uniqueName);
        GR grCreated = efpClient.create(grHosp);

        Assert.assertNotNull(grCreated.getId());
        Assert.assertEquals(grHosp, grCreated);
    }

    @Test
    public void testFindAll() {
        List<GR> grList = efpClient.findGrAll();

        Assert.assertTrue(grList.contains(grEdu));
        Assert.assertTrue(grList.contains(grAuto));
    }

    @Test
    public void testUpdateGr() {

        GR modifiedGR = new GR(grEdu.getId(), "XXX" + uniqueName, "XXXREG" + uniqueName);
        efpClient.update(modifiedGR);

        GR loadedGR = efpClient.getGrById(grEdu.getId());
        Assert.assertEquals(modifiedGR, loadedGR);
    }

    @Test
    public void testGetLrByLd() {

        LR lrLoaded = efpClient.getLrById(lr.getId());

        Assert.assertEquals(lr, lrLoaded);
    }

    @Test
    public void testFindLrForNonExistingGr() {

        GR gr = new GR(-1, "non existing GR", "NEGR");
        List<LR> lrList = efpClient.findLrForGr(gr);

        Assert.assertEquals(0, lrList.size());
    }

    @Test
    public void testFindLrForGr() {

        List<LR> lrList = efpClient.findLrForGr(grEdu);

        Assert.assertTrue(lrList.contains(lr));
        Assert.assertTrue(lrList.contains(lr2));
        Assert.assertTrue(lrList.contains(parentLr));
    }

    @Test
    public void testUpdateLrName() {

        LR updatedLr  = new LR(lr.getId(), "XYZ", lr.getGr(), lr.getParentLR(), lr.getIdStr());
        efpClient.update(updatedLr);
        LR loadedLR = efpClient.getLrById(lr.getId());

        Assert.assertEquals(updatedLr.getName(), loadedLR.getName());
    }

    @Test
    public void testUpdateLrParent() {

        LR updatedLr  = new LR(lr.getId(), lr.getName(), lr.getGr(), lr2, lr.getIdStr());
        efpClient.update(updatedLr);
        LR loadedLR = efpClient.getLrById(lr.getId());

        Assert.assertEquals(updatedLr.getParentLR(), loadedLR.getParentLR());
        Assert.assertEquals(lr2, loadedLR.getParentLR());
    }

    @Test
    public void testUpdateLrGR() {

        LR updatedLr  = new LR(lr.getId(), lr.getName(), grAuto, lr.getParentLR(), lr.getIdStr());
        efpClient.update(updatedLr);
        LR loadedLR = efpClient.getLrById(lr.getId());

        Assert.assertEquals(updatedLr.getGr(), loadedLR.getGr());
        Assert.assertEquals(grAuto, loadedLR.getGr());

    }

    @Test
    public void testCreateLrAssignment() {
        LrSimpleAssignment lrAssignment = new LrSimpleAssignment(
                simpleEfp2, "high", new EfpBoolean(true), parentLr);

        LrSimpleAssignment lrAssignCreated =  (LrSimpleAssignment) efpClient.create(lrAssignment);

        Assert.assertNotNull(lrAssignCreated.getId());
        Assert.assertEquals(lrAssignment.getEfp(), lrAssignCreated.getEfp());
        Assert.assertEquals(lrAssignment.getValueName(), lrAssignCreated.getValueName());
        Assert.assertEquals(lrAssignment.getEfpValue(), lrAssignCreated.getEfpValue());
        Assert.assertEquals(lrAssignment.getLr(), lrAssignCreated.getLr());
    }

    @Test
    public void testUpdateLrAssignment() {
        LrDerivedValueEvaluator eval = new LrConstraintEvaluator(
                "a + b + c", new EfpString("good"), Arrays.asList(simpleEfp1, simpleEfp2));

        LrDerivedAssignment lrAssignment = new LrDerivedAssignment(
                lrSimpleAssig.getId(),
                derivedEfp, "medium" , eval, lr2);

        efpClient.update(lrAssignment);

        LrDerivedAssignment lrAssignUpdated = (LrDerivedAssignment) efpClient.getLrAssignmentById(lrAssignment.getId());

        Assert.assertEquals(lrAssignment, lrAssignUpdated);
        Assert.assertEquals(lrAssignment.getEfp(), lrAssignUpdated.getEfp());
        Assert.assertEquals(lrAssignment.getValueName(), lrAssignUpdated.getValueName());
        Assert.assertEquals(lrAssignment.getEvaluator().serialise(), lrAssignUpdated.getEvaluator().serialise());
        Assert.assertEquals(lrAssignment.getLr(), lrAssignUpdated.getLr());
    }

    @Test(expected = Exception.class)
    public void testDeleteLrAssignment() {

        efpClient.delete(lrSimpleAssig);
        efpClient.getLrAssignmentById(lrSimpleAssig.getId());
    }

    @Test
    public void findNonExistingLrAssignments() {
        List<LrAssignment> items = efpClient.findLrAssignments(-1, -1);

        Assert.assertEquals(0, items.size());
    }

    @Test
    public void findLrAssignments() {

        List<LrAssignment> items = efpClient.findLrAssignments(simpleEfp1.getId(), lr.getId());

        for (LrAssignment item : items) {
            Assert.assertEquals(LrAssignment.LrAssignmentType.SIMPLE, item.getAssignmentType());
        }

        Assert.assertTrue(items.contains(lrSimpleAssig));
        Assert.assertFalse(items.contains(lrDerivedAssig));
    }

    @Test
    public void findLrAssignmentByName() {

        LrAssignment item = efpClient
            .findLrAssignment(simpleEfp1.getId(), lr.getId(), "low");

        Assert.assertEquals("low", item.getValueName());
    }


    @Test
    public void testfindLrAssignmentsTypes() {

        List<LrAssignment> items = efpClient.findLrAssignments(simpleEfp1.getId(), lr.getId());
        for (LrAssignment item : items) {
            Assert.assertEquals(LrAssignment.LrAssignmentType.SIMPLE, item.getAssignmentType());
        }
        Assert.assertTrue(items.contains(lrSimpleAssig));
        Assert.assertFalse(items.contains(lrDerivedAssig));
        Assert.assertFalse(items.contains(lrDerivedAssigWithEmptyFormula));

        items = efpClient.findLrAssignments(derivedEfp.getId(), lr.getId());
        for (LrAssignment item : items) {
            Assert.assertEquals(LrAssignment.LrAssignmentType.DERIVED, item.getAssignmentType());
        }

        Assert.assertFalse(items.contains(lrSimpleAssig));
        Assert.assertTrue(items.contains(lrDerivedAssig));
        Assert.assertTrue(items.contains(lrDerivedAssigWithEmptyFormula));

    }


    @Test
    public void findNonExistingAllLrAssignments() {

        List<LrAssignment> items = efpClient.findLrAssignments(-1);
        Assert.assertEquals(0, items.size());
    }

    @Test
    public void findAllLrAssignments() {

        List<LrAssignment> items = efpClient.findLrAssignments(lr.getId());

        Assert.assertTrue(items.contains(lrSimpleAssig));
        Assert.assertTrue(items.contains(lrDerivedAssig));
    }

    @Test
    public void testDeleteEfp() {

        EFP efp = new SimpleEFP(
                "EFPtobedeleted", EfpString.class, null, null, grEdu);
        efp =  efpClient.create(efp);
        Assert.assertNotNull(efp);

        efpClient.deleteEfp(efp);

        List<EFP> efps = efpClient.findEfpForGr(efp.getGr().getId());
        Assert.assertFalse(efps.contains(efp));
    }

    @Test
    public void testGetAllValueNames() {
        List<String> values = efpClient.getAllValueNames(grEdu.getId());

        Assert.assertTrue(values.contains("low"));
        Assert.assertTrue(values.contains("high"));
        Assert.assertTrue(values.contains("medium"));
    }

    @Test
    public void testUpdateValueName() {
        efpClient.updateValueName(grEdu.getId(), "low", "abcd");

        List<String> values = efpClient.getAllValueNames(grEdu.getId());

        Assert.assertFalse(values.contains("low"));
        Assert.assertTrue(values.contains("high"));
        Assert.assertTrue(values.contains("medium"));
        Assert.assertTrue(values.contains("abcd"));
    }

    @Test
    public void testDeleteValueName() {
        efpClient.deleteValueName(grEdu.getId(), "delete");

        List<String> values = efpClient.getAllValueNames(grEdu.getId());

        Assert.assertTrue(values.contains("low"));
        Assert.assertTrue(values.contains("high"));
        Assert.assertFalse(values.contains("delete"));
    }

    @Test
    public void testCreateValueName() {
        efpClient.createValueName(grEdu.getId(), "xyz");

        List<String> values = efpClient.getAllValueNames(grEdu.getId());

        Assert.assertTrue(values.contains("low"));
        Assert.assertTrue(values.contains("high"));
        Assert.assertTrue(values.contains("medium"));
        Assert.assertTrue(values.contains("xyz"));
    }

    @Test
    public void testDeleteLr() {
        LR lr = new LR("LR - deleting test", grEdu, null, "LRDT");
        lr = efpClient.create(lr);

        efpClient.deleteLr(lr.getId());
        List<LR> lrs = efpClient.findLrForGr(grEdu);

        Assert.assertFalse(lrs.contains(lr));
    }

    @Test
    public void testDeleteGr() {
        GR gr = new GR("GR -deleting test " + uniqueName, "GR");
        gr = efpClient.create(gr);

        efpClient.deleteGr(gr.getId());
        List<GR> grs = efpClient.findGrAll();

        Assert.assertFalse(grs.contains(gr));
    }

    @Test
    public void testFindByAssignedValueName() {
        List<EFP> efps = efpClient.findByAssignedValueName(grEdu.getId(), "low");

        Assert.assertTrue(efps.contains(simpleEfp1));
        Assert.assertTrue(efps.contains(simpleEfp2));
        Assert.assertTrue(efps.contains(derivedEfp));

        efps = efpClient.findByAssignedValueName(grEdu.getId(), "delete");
        Assert.assertEquals(0, efps.size());
    }


}
