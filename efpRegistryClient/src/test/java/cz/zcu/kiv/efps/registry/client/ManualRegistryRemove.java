package cz.zcu.kiv.efps.registry.client;

import cz.zcu.kiv.efps.types.gr.GR;
import cz.zcu.kiv.efps.types.lr.LR;
import cz.zcu.kiv.efps.types.lr.LrAssignment;
import cz.zcu.kiv.efps.types.properties.EFP;

import java.util.LinkedList;
import java.util.List;

/**
 * Utility class to remove registries
 *
 * Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ManualRegistryRemove
{
    private static EfpClient client = EfpClientImpl.getClient();

    public static void main(String[] args) {

        List<GR> grs = client.findGrAll();

        List<EFP> derivedEfps = new LinkedList<EFP>();
        List<EFP> simpleEfps = new LinkedList<EFP>();
        List<GR> selectedGrs = new LinkedList<GR>();

        for (GR gr : grs) {

            String grName = gr.getName();

            if (grName.startsWith("Automotive")
                    || grName.startsWith("Education System")
                    || grName.startsWith("Hospitals")
                    || grName.startsWith("XXX")) {


                deleteLrs(gr, client);

                for (EFP efp : client.findEfpForGr(gr.getId())) {

                    // delete simple EFPs first
                    if (efp.getType().equals(EFP.EfpType.DERIVED)) {
                        derivedEfps.add(efp);
                    } else {
                        simpleEfps.add(efp);
                    }
                }

                selectedGrs.add(gr);
            }
        }

        // EFPs are scattered across GRs, se we must delete all EFPs before deleting GRs
        for (EFP efp : derivedEfps) {
            client.deleteEfp(efp);
        }

        for (EFP efp : simpleEfps) {
            client.deleteEfp(efp);
        }

        for (GR gr : selectedGrs) {

            // delete value names first
            for (String valueName : client.getAllValueNames(gr.getId())) {
                client.deleteValueName(gr.getId(), valueName);
            }

            System.out.println("" + gr);
            client.deleteGr(gr.getId());
        }
    }

    private static void deleteLrs(GR gr, EfpClient client) {

        List<LR> deleteLrs = client.findLrForGr(gr);
        List<LR> postponedLrs = new LinkedList<LR>();

        if (deleteLrs.isEmpty()) {
            return;
        }

        // find LRs that must be postponed because
        // they are parents of other LRs
        for (LR lr : deleteLrs) {

            // postpone parents of a LR
            if (lr.getParentLR() != null) {
                postponedLrs.add(lr.getParentLR());
            }
        }

        // delete all LRs that are not postponed
        for (LR lr : deleteLrs) {
            if (!postponedLrs.contains(lr)) {


                // delete assignments first
                for (LrAssignment lrAssignment : client.findLrAssignments(lr.getId())) {
                    client.delete(lrAssignment);
                }

                client.deleteLr(lr.getId());
            }
        }

        deleteLrs(gr, client);
    }

}
